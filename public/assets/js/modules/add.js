var AddModule = function () {
	var webroot;

	var createFormValidator = function() {
		$("#frmAddModule").validate({
			ignore: [],
			rules: {
				name: {
					required : true,
					noSpace  : true
				},				
				introduction:{
					required: function(textarea) {
				        CKEDITOR.instances[textarea.id].updateElement(); // update textarea
				        var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
				        return editorcontent.length === 0;
				    }
				},
				/*moduleImg: {
					//required: true,
                	extension: "jpeg|jpg|png"
				},*/
				instruction:{
					required: function(textarea) {
				        CKEDITOR.instances[textarea.id].updateElement(); // update textarea
				        var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
				        return editorcontent.length === 0;
				    }
				},
				learning_outcome:{
					required: function(textarea) {
				        CKEDITOR.instances[textarea.id].updateElement(); // update textarea
				        var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
				        return editorcontent.length === 0;
				    }
				},
				moduleImg: {
					required : function(){
						var img_val = $("#moduleUpImg").val();  
						if(img_val) return false;
					},
                	extension: "jpeg|jpg|png"
				},
				meta_keywords:{
					required: function(textarea) {
				        CKEDITOR.instances[textarea.id].updateElement(); // update textarea
				        var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
				        return editorcontent.length === 0;
				    }
				},
				meta_title:{
					required : true,
					noSpace  : true
				},
				meta_description:{
					required: function(textarea) {
				        CKEDITOR.instances[textarea.id].updateElement(); // update textarea
				        var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
				        return editorcontent.length === 0;
				    }
				},
				learning_activities:{
					required: function(textarea) {
				        CKEDITOR.instances[textarea.id].updateElement(); // update textarea
				        var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
				        return editorcontent.length === 0;
				    }
				},
				doc_name1: {
					required : true,
					noSpace  : true
				},
				doc_type1: {
					required : true,
					notEqualTo: 0
				},
				doc1 : {
					required : true,
                	extension: "doc|docx|pdf|doc|docx|pdf|odt|xls|xlsx|ppt|pptx" 
				}	
											
			},
			messages: {
				name: {
					required: "Please enter name"					          
				},				
				introduction : {
					required:"Please enter introduction content"
				},
				instruction : {
					required:"Please enter instruction content"
				},
				learning_outcome : {
					required:"Please enter instruction content"
				},
				moduleImg: {
					required : "Please select module image"
				},				
				meta_keywords : {
					required:"Please enter meta keywords"
				},
				meta_title : {
					required:"Please enter meta title"
				},
				meta_description : {
					required:"Please enter meta keywords"
				},
				learning_activities : {
					required:"Please enter learning activities"
				},
				doc_name1 : {
					required:"Please enter document name"
				},
				doc_type1 : {
					required:"Please select document type"
				},
				doc1 : {
					required:"Please select document"
				}
			},   
			submitHandler: function (form) { 
				submitAddDocs();
				return true
			}
			/*errorPlacement: function(error,element) {
			    console.log(error);
			}*/
		});
	}

	var submitAddDocs = function() {
		//alert('sss');
		var c = $("#docDataCnt").val();
		var error=0;
		for(var i=2; i <=c; i++) {
			//alert(i);
			var doc_name = $("#doc_name"+i).val();
			if(doc_name==""){
				$("#doc_name"+i).after('<label id="name-error" class="error" for="name">Please enter name of document</label>');
				$("#doc_name"+i).focus();
				error++;
			}
			var doc_type = $("#doc_type"+i).val();
			if(doc_type==""){
				$("#doc_type"+i).after('<label id="name-error" class="error" for="name">Please select document type</label>');
				$("#doc_type"+i).focus();
				error++;
			}
			var doc = $("#doc"+i).val();
			var doc_ext = doc.split('.').reverse()[0].toLowerCase();
			var exts = ['doc','docx','pdf','odt','xls','xlsx','ppt','pptx'];
			if(doc==""){
				$("#doc"+i).after('<label id="name-error" class="error" for="name">Please select document</label>');
				$("#doc"+i).focus();
				error++;
			}else if($.inArray(doc_ext, exts) == -1) {
			    //alert('invalid extension!');
			    $("#doc"+i).after('<label id="name-error" class="error" for="name">Invalid file format</label>');
				$("#doc"+i).focus();
				error++;
			}
		}
		if(error > 0)
			return false;
		else
			return true;

	}
	var init = function (dataVariable) {
		webroot = dataVariable.ajax_call_root;	

		jQuery.validator.addMethod("noSpace", function(value, element) { 
			return value == '' || value.trim().length != 0;  
		}, "Space is not allow");

		jQuery.validator.addMethod("emailExt", function(value, element, param) {
			return value.match(/^[a-zA-Z0-9_\.%\+\-]+@[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,}$/);
		},"Please enter valide email address");

		createFormValidator();
		//getActiveCKEDITOR();
	}		

	return {
		init:init
	}

}();