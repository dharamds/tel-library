
 $("#gradebook_create_form_validation").validate({
            rules: {
                category_id: {
                    required : true
                },
                name: {
                    required : true
                },
                template: {
                    required : true
                },
                description: {
                    required : true
                },
                  
            },
            messages: {
                 category_id: {
                    required : "Please select category"
                },
                name: {
                    required : "Please enter name"
                },
                template: {
                    required : "Please enter template"
                },
                description: {
                    required : "Please enter description"
                },
            },   
            submitHandler: function (form) { 
                $("#gradebook_create_form_validation").submit();
            }
        });