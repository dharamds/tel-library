var AddQuizQuestion = function () {
    var webroot,recordID;

    var get_selected_section_part = function(recordID,sectionID){
        $.ajax({
            url      :   webroot+"quiz/get_selected_section_part",
            method   :   "POST",
            data     : {'quizID':recordID,'sectionID':sectionID},
            dataType : "html",        
            success: function (result) { 
                $("#selected_section_part").html(result);
            }
        });
        return false; 
    }

    var init = function (dataVariable) {
        webroot = dataVariable.ajax_call_root;
        recordID = dataVariable.recordID;
        get_selected_section_part(recordID,$('#selected_que_section_id option:selected').val());

        $("#selected_que_section_id").on('change', function(){
            get_selected_section_part(recordID,this.value);
        });

       
        $(".click_to_finish").on('click', function(){
            window.location.href = webroot+"quiz";
        });

        $(".click_to_back").on('click', function(){
            $("#quizPanelTab li").removeClass("active");
            $("#tab-metadata").addClass('active');
            getViewTemplate('metadata',recordID); 
        });

    /*$(".scroll-top").on("click", function(){
    $('body,html').animate({
    scrollTop: 0
    }, 500);
    }); */


    }   

    return {
        init:init
    }

}();