var AddQuizContent = function () {
    var webroot,recordID;


    var submitQuizContentForm = function() {
        $.ajax({
            url      :   webroot+"quiz/saveContents",
            method   :   "POST",
            data     :  $('#createquiz').serialize() + '&recordID='+recordID,
            dataType : "json",        
            success: function (result) { 
                CommanJS.getDisplayMessgae(result.code,result.message); 
                if(result.code == 200){
                    $("#quizPanelTab li").removeClass("active");
                    $("#tab-setting").addClass('active');
                    getViewTemplate('setting',result.id); 
                }         
            }
        });
    }


  var createFormValidator = function() {
        $("#createquiz").validate({
            ignore: [],
            rules: {
                type: {
                    required  : true
                },
                title: {
                    required : true,
                    noSpace  : true,
                    remote:{
                        url: webroot +"quiz/getCheckExist",
                        type: 'POST',
                        data: {'recordID':recordID}
                    }  
                },
                
                long_title: {
                    noSpace  : true
                },
                quizInstructions:{
                    required: function(textarea) {
                          CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                          var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                          return editorcontent.length === 0;
                      }
                 }/*,
                quizFeedback:{
                    required: function(textarea) {
                          CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                          var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                          return editorcontent.length === 0;
                      }
                 }*/
            },
            messages: {
                type: {
                    required: "Please select type"                             
                }, 
                title: {
                    required: "Please enter title",
                    remote: "Quiz name is already exist"                              
                },
                long_title: {
                    required: "Please enter long title"               
                },
                quizInstructions : {
                    required:"Please enter some content"
                },
                quizFeedback : {
                    required:"Please enter some content"
                }
            },   
            submitHandler: function (form) { 
                submitQuizContentForm();
                return false;
            },
            errorPlacement: function(error, $elem) {
                if ($elem.is('textarea')) {
                    $elem.insertAfter($elem.next('div'));
                }
                error.insertAfter($elem);
            }
        });
    }

    var getActiveCkEditor = function(){
        
        CKEDITOR.replace( 'quizInstructions', {
            toolbar : 'short'
         });
        CKEDITOR.replace( 'quizFeedback', {
            toolbar : 'short'
         });

       
    }

  var init = function (dataVariable) {
    webroot = dataVariable.ajax_call_root;
    recordID = dataVariable.recordID;  

    jQuery.validator.addMethod("noSpace", function(value, element) { 
      return value == '' || value.trim().length != 0;  
    }, "Space is not allow");

    
    getActiveCkEditor();
    createFormValidator();   

    $(".click_to_next").on('click', function(){
        $("#quizPanelTab li").removeClass("active");
        $("#tab-setting").addClass('active');
        getViewTemplate('setting');
    });

    $(".click_to_finish").on('click', function(){
        window.location.href = webroot+"quiz";
    });

    $("input.touchspin4").TouchSpin({
        verticalbuttons: true,
        step: 1,
        min : 0,
        max : 100
    });

    /*$(".scroll-top").on("click", function(){
        $('body,html').animate({
            scrollTop: 0
        }, 500);
    }); */
  }   

  return {
    init:init
  }

}();