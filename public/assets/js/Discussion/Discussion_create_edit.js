var Discussion_create = function (){
	var webroot;
	
	var createFormValidator = function() {
        $("#Discussion_create_form_validation").validate({
            rules: {
                name: {
                    required : true
                },
                description: {
                    required: function(textarea) {
                        CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                        return editorcontent.length === 0;
                    }
                }
            },
            messages: {
                name: {
                    required: "Please enter name" ,
                    noSpace:true
                },
                description: {
                    required: "Please enter description"
                }
            },   
            submitHandler: function (form) { 
                $("#Discussion_create_form_validation").submit();
            }
        });
    }

    var ckeditorinit = function() {
		var config = {
			toolbarGroups: [
				{
					name: "basicstyles",
					groups: ["basicstyles"]
				},
				{
					name: "links",
					groups: ["links"]
				},
				{
					name: "paragraph",
					groups: ["list", "blocks"]
				},
				{
					name: "document",
					groups: ["mode"]
				},
				{
					name: "insert",
					groups: ["insert"]
				},
				{
					name: "styles",
					groups: ["styles"]
				},
				{
					name: "about",
					groups: ["about"]
				}
			],
			removeButtons:
				"Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar"
		};
		// to assign ckeditor
		CKEDITOR.replace("description", config);
    };
    
	var init = function (dataVariable) {
        webroot = dataVariable.ajax_call_root;
        
         jQuery.validator.addMethod("noSpace", function (value, element, param) {
            return value.match(/^(?=.*\S).+$/);
        }, "No space please and don't leave it empty");
        
        createFormValidator();
        ckeditorinit();
		CKEDITOR.replace('description', {
            toolbar: 'short',
        });
	}		

	return {
		init:init,
		createFormValidator:createFormValidator
	}	

}();