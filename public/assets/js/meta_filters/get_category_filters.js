var CatFilters = function (){
	
	var webroot,post_data,selector;
	
	
	
	var __getCheckAllSelect = function(element){
		//console.log(element);
		var numberOfChecked = $("."+element+'_check_fil:checkbox:checked').length;
		var totalCheckboxes = $("."+element+'_check_fil:checkbox').length;
		//$("#"+elementView+' .search_categories').val(numberOfChecked+' selected');
		if(numberOfChecked > 0){
			$("#"+element+' span').text(numberOfChecked);
		}else{
			$("#"+element+' span').text('');
		}
		if(numberOfChecked == totalCheckboxes ){
			 $("#"+element+'_check_fil').prop("checked",true);
		}
	}

	var init = function (dataVariable) {
		webroot = dataVariable.ajax_call_root;
		post_data = dataVariable.post_data;
		/*selector = post_data.selector.replace(' ', '_').toLowerCase();*/
		selector = post_data.selector.split(' ').join('_').toLowerCase();
		
		$("#"+post_data.selector_div+' .search_categories').on('keyup', function () {
			var value = this.value.toLowerCase();
			$("#"+post_data.selector_div+' ul label').filter(function() {
				//$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
				if($(this).text().toLowerCase().indexOf(value) > -1){
					$(this).parent('li').removeClass('li-hidden').addClass('li-visible');
				}else{
					$(this).parent('li').removeClass('li-visible').addClass('li-hidden');
				}
				var numv = $(this).parents("ul").children('li.li-visible').length;
				
				if(numv == 0){
					$("#"+post_data.selector_div+' .no-match').show();
					$("#"+post_data.selector_div+' .no-match span').text('"'+value+'"');
				}else{
					$("#"+post_data.selector_div+' .no-match').hide();
				}
		    });
		});
	
      

        $("#"+selector+"_check_fil").click(function () {
			$("."+selector+"_check_fil").prop('checked', $(this).prop('checked'));
			__getCheckAllSelect(selector);
        });

        $("."+selector+"_check_fil").on('change',function(){
            if (!$(this).prop("checked")){
                $("#"+selector+"_check_fil").prop("checked",false);
            }
            __getCheckAllSelect(selector);
        });

		
	}		

	return {
		init:init
	}	

}();