var AddCourse = function () {
  var webroot;

  var createFormValidator = function() {
    $("#frmAddCourse").validate({
      ignore: [],
      rules: {
        name: {
          required : true,
          noSpace  : true
        },        
        description:{
          required: function(textarea) {
                CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                return editorcontent.length === 0;
            }
        },        
        instruction:{
          required: function(textarea) {
                CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                return editorcontent.length === 0;
            }
        },
        courseImg: {
          required : function(){
            var img_val = $("#courseUpImg").val();  
            if(img_val) return false;
          },          
          extension: "jpeg|jpg|png"
        },
        prerequisites: {
            required : true,
            noSpace  : true
        },
        term: {
          required : true,
          noSpace  : true
        },
        learning_outcome:{
          required: function(textarea) {
                CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                return editorcontent.length === 0;
            }
        },
        learning_activities: {
          required: function(textarea) {
                CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                return editorcontent.length === 0;
            }
        },
        technology_requirments: {
          required: function(textarea) {
                CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                return editorcontent.length === 0;
            }
        },
        competency:{
          required : true,
          noSpace  : true
        },
        literacy:{
          required : true,
          noSpace  : true
        },
        internal_links: {
          required: function(textarea) {
                CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                return editorcontent.length === 0;
            }
        },
        meta_keywords:{
          required: function(textarea) {
                CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                return editorcontent.length === 0;
            }
        },
        meta_title:{
          required : true,
          noSpace  : true
        },
        meta_keywords: {
          required: function(textarea) {
                CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                return editorcontent.length === 0;
            }
        },
        meta_description:{
          required: function(textarea) {
                CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                return editorcontent.length === 0;
            }
        },       
        regular_price: {
          required : true,
          noSpace  : true,
          notEqualTo: 0
        },
        sales_price: {
          required : true,
          noSpace  : true,
          notEqualTo: 0
        },
        docname_1: {
          required:true
        },
        doctype_1: {
          required:true
        },
        doc_1: {
           required : function(){
            var doc_val = $("#courseUpDoc_1").val();  
            if(doc_val) return false;
          },    
           extension: "doc|docx|pdf|doc|docx|pdf|odt|xls|xlsx|ppt|pptx" 
        },
                      
      },
      messages: {
        name: {
          required: "Please enter name"                   
        },        
        description : {
          required:"Please enter description"
        },        
        instruction : {
          required:"Please enter instruction content"
        },
        courseImg: {
          required:"Please select course image"
        },
        prerequisites : {
          required:"Please enter prerequisites"
        },
        term : {
          required:"Please select term"
        },
        learning_outcome : {
          required:"Please enter learning content"
        },
        technology_requirments: {
          required:"Please enter technology requirments content"
        },     
        competency: {
          required:"Please enter competency"
        },  
        literacy: {
          required:"Please enter literacy"
        },     
        internal_links: {
          required:"Please enter internal links"
        },
        meta_title : {
          required:"Please enter meta title"
        },
        meta_keywords : {
          required:"Please enter meta keywords"
        },
        meta_description : {
          required:"Please enter meta description"
        },
        regular_price : {
          required:"Please enter regular price"
        },
        sales_price : {
          required:"Please enter sales price"
        },
        docname_1 : {
          required:"Please enter document name"
        },
        doctype_1 : {
          required:"Please select document type"
        },
        doc_1 : {
          required:"Please select correct file format"
        }
      },   
      /*submitHandler: function (form) { 
        submitAddDocs();
        alert("false");
      },
      errorPlacement: function(error,element) {
          console.log(error);
      }*/
    });
  }

  var submitAddDocs = function() {
    //alert('sss');
    var c = $("#docDataCnt").val();
    var error=0;
    for(var i=2; i <=c; i++) {
      //alert(i);
      var doc_name = $("#doc_name"+i).val();
      if(doc_name==""){
        $("#doc_name"+i).after('<label id="name-error" class="error" for="name">Please enter name of document</label>');
        $("#doc_name"+i).focus();
        error++;
      }
      var doc_type = $("#doc_type"+i).val();
      if(doc_type==""){
        $("#doc_type"+i).after('<label id="name-error" class="error" for="name">Please select document type</label>');
        $("#doc_type"+i).focus();
        error++;
      }
      var doc = $("#doc"+i).val();
      var doc_ext = doc.split('.').reverse()[0].toLowerCase();
      var exts = ['doc','docx','pdf','odt','xls','xlsx','ppt','pptx'];
      if(doc==""){
        $("#doc"+i).after('<label id="name-error" class="error" for="name">Please select document</label>');
        $("#doc"+i).focus();
        error++;
      }else if($.inArray(doc_ext, exts) == -1) {
          //alert('invalid extension!');
          $("#doc"+i).after('<label id="name-error" class="error" for="name">Invalid file format</label>');
        $("#doc"+i).focus();
        error++;
      }
    }
    if(error > 0)
      return false;
    else
      return true;

  }
  var init = function (dataVariable) {
    webroot = dataVariable.ajax_call_root;  

    jQuery.validator.addMethod("noSpace", function(value, element) { 
      return value == '' || value.trim().length != 0;  
    }, "Space is not allow");

    jQuery.validator.addMethod("emailExt", function(value, element, param) {
      return value.match(/^[a-zA-Z0-9_\.%\+\-]+@[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,}$/);
    },"Please enter valide email address");

    createFormValidator();
    //getActiveCKEDITOR();
  }   

  return {
    init:init
  }

}();