$("#poll_form_validation").validate({
    rules: {
        name: "required",
        definitions: "required",
        synonyms_terms: "required",
        variations_terms: "required",
        learning_outcomes: "required",
        literacies: "required",
        competencies: "required"
    },
    messages: {
        name: "Please enter Name",
        definitions: "Please enter Definitions",
        synonyms_terms: "Please enter Synonyms Terms",
        variations_terms: "Please enter Variations Terms",
        literacies: "Please Enter Literacies",
        competencies: "Please enter Competencies"
    }
});