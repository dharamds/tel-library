$(document).ready(function()
{   var i;
    $(".add-row").click(function()
    {  	
        var html  = "";
        html     += "<tr><td><input type='checkbox' name='record[]'></td>";				
        html     += "<td><input class='option_input' type='text' name='que_ans["+i+"][option]'></td>";		
        html     += "<td><input class='answer_input' type='checkbox' name='que_ans["+i+"][answer]'></td></tr>";			

        $("table tbody#emp_details").append(html);
				i++;
    });

    $(".delete-row").click(function()
    {
        var row_count         = $("#emp_details").find('input[name="record[]"]').length;
        var checked_row_count = $('[name="record[]"]:checked').length;

        if(row_count != checked_row_count)
        {
            $("#emp_details").find('input[name="record[]"]').each(function()
            {
                if($(this).is(":checked"))
                {
                    $(this).parents("#emp_details tr").remove();
                }
            });
        }
        else
        {
            alert("All rows can't be deleted");
            return false;
        }
    });	
 

   $('form#que_form_validation').on('submit', function(event) {
      $('.option_input').each(function() {
         $(this).rules("add", 
               {
                  required: true,
                  messages: {
                     required: "Please enter option",
                  }
               });
      });

      $.validator.addMethod("atlistone", function(value, elem, param) {
         return $(".answer_input:checkbox:checked").length > 0;
      },"You must select at least one!");

      $.validator.addMethod("onlyone", function(value, elem, param) {
         return $(".answer_input:checkbox:checked").length <= 1;
      },"You can select only one!");

      $('.answer_input').each(function() {
         $(this).rules("add", 
               {
                  atlistone: true,
                  onlyone: true
               });
      });

      $(this).find('input[type="checkbox"]').each( function () {
        var checkbox = $(this);
        if( checkbox.is(':checked')) {
            checkbox.attr('value','1');
        }
      });

   });


   $("#que_form_validation").validate({
      rules: {
         'que[name]': "required",
         'que[learning_outcomes]': "required",
         'que[literacies]': "required",
         'que[competencies]': "required",
         'que[type]': "required",
         'que[question_type]': "required",
      },
      messages: {
         'que[name]': "Please enter name",
         'que[learning_outcomes]': "Please enter learning outcomes",
         'que[literacies]': "Please enter literacies",
         'que[competencies]': "Please enter competencies",
         'que[type]': "Please select quiz type",
         'que[question_type]': "Please select question type"
      }
   });

   $('select#questiontype').on('change', function() {
      switch (this.value) { 
         case '3': 
         case '5':
            $( "#addbutton" ).removeClass("hide"); 
            $( "#dynamicadd" ).removeClass("hide");
            $("#innertable").find("tr:gt(2)").remove();
            i = 1;
            break;
         case '2':
            $( "#addbutton" ).addClass("hide");
            $( "#truefalsefield" ).removeClass("hide");
            $( "#dynamicadd" ).removeClass("hide");
            i = 2;
            $("#innertable").find("tr:gt(2)").remove();
            break;
         default:
            $( "#dynamicadd" ).addClass("hide");
            $( "#truefalsefield" ).addClass("hide");
            i = 1;
            $("#innertable").find("tr:gt(2)").remove();
      }   
   });
});