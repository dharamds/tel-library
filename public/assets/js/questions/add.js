var AddQuestion = function () {
    var webroot;

    var createFormValidator = function () {
        $("#frmAddQuestion").validate({
            ignore: [],
            rules: {
                question_type: {
                    required: true
                },
                question: {
                    required: function (textarea) {
                        CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                        return editorcontent.length === 0;
                    }
                },
                opts_1: {
                    required: function () {
                        var a = $("#question_type").val();
                        if (a == 2)
                            return true;
                    }
                },
                opts_2: {
                    required: function () {
                        var a = $("#question_type").val();
                        if (a == 2)
                            return true;
                    }
                },
                opts_status: {
                    required: function () {
                        var a = $("#question_type").val();
                        if (a == 2)
                            return true;
                    }
                },
                mopts_1: {
                    //required : true					
                    required: function () {
                        var a = $("#question_type").val();
                        if (a == 3)
                            return true;
                    }
                },
                mopts_2: {
                    required: function () {
                        var a = $("#question_type").val();
                        if (a == 3)
                            return true;
                    }
                },
                'chk_mopts[]': {
                    minlength: 1,
                    required: function () {
                        var a = $("#question_type").val();
                        if (a == 3)
                            return true;
                    }
                }
            },
            messages: {
                question_type: {
                    required: "Please select question type"
                },
                question: {
                    required: "Please enter QUESTION"
                },
                opts_1: {
                    required: "Please enter option"
                },
                opts_2: {
                    required: "Please enter option"
                },
                opts_status: {
                    required: "Please select at-least one correct option"
                },
                mopts_1: {
                    required: "Please enter option value"
                },
                mopts_2: {
                    required: "Please enter option value"
                },
                'chk_mopts[]': {
                    required: "Please select at-least one correct option"
                },
                
                
            },
            errorPlacement: function (error, $elem) {
                if ($elem.is('textarea')) {
                    $elem.insertAfter($elem.next('div'));
                }
                error.insertAfter($elem);
            }
           
            
        });
    }

    var init = function (dataVariable) {
        webroot = dataVariable.ajax_call_root;

        jQuery.validator.addMethod("noSpace", function (value, element) {
            return value == '' || value.trim().length != 0;
        }, "Space is not allow");

        jQuery.validator.addMethod("emailExt", function (value, element, param) {
            return value.match(/^[a-zA-Z0-9_\.%\+\-]+@[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,}$/);
        }, "Please enter valide email address");

        createFormValidator();
        //getActiveCKEDITOR();
    }

    return {
        init: init
    }

}();