var Users = function () {
    var webroot,states;

    var createFormValidator = function () {
        $("#user_section").validate({
            rules: {
                first_name: {
                    required: true,
                    noSpace: true
                },
                last_name: {
                    required: true,
                    noSpace: true
                },
                unique_id: {
                    required: true,

                },
                email: {
                    required: true,
                    emailExt: true
                   
                },
                 password: {
                   //  required: true,
                     minlength: 8,
                   //  noSpace: true
                 },

                 password_confirm: {
                  //   required: true,
                     minlength: 8,
                     equalTo: "#password",
                   //  noSpace: true
                 },
                user_role: {
                    required: true,
                    min: 1
                }
            },
            messages: {
                first_name: {
                    required: "Please enter first name"
                },
                last_name: {
                    required: "Please enter last name"
                },
                unique_id: {
                    required: "Please enter unique id"
                },
                email: {
                    required: "Please enter email",
                    
                },
                phone: {
                    required: "Please enter phone number"
                },
                user_role: {
                    required: "Please select role",
                    min: "Please select role",
                },
                 password: {
                   //  required: "Please enter password",
                     minlength: "Please enter atleast eight characters",
                 },

                 password_confirm: {
                  //   required: "Please confirm password",
                     minlength: "Please enter atleast eight characters",
                     equalTo: "Password do not match ! Enter same password"
                 },

            },
            submitHandler: function (form) {
                //$("#user_section").submit();
               
                return true;
            }
        });
    }


    var autocompleteState = function() {
        $( ".states" ).autocomplete({
            source: states,
            minLength:0,
            select: function (event, ui) {
                getCities(ui.item.value);
            },
            search  : function(){$(this).addClass('loading');},
            open    : function(){$(this).removeClass('loading');}
        }).focus(function(){
            if (this.value == ""){
                $(this).autocomplete("search");
            }
        });
    }
    
    
    var autocompleteCity = function(result) {
        $( ".city" ).autocomplete({
            source: result.data,
            minLength:0,
            search  : function(){$(this).addClass('loading');},
            open    : function(){$(this).removeClass('loading');}
        }).focus(function(){
            if (this.value == ""){
                $(this).autocomplete("search");
            }
        });     
    }

    var getCities = function(state) {
        
        $.ajax({
            url      :  webroot+"containers/schools/get_city_per_state",
            method   :   "POST",
            data     : {state:state},
            dataType : "json",              
            success: function (result) { 
                autocompleteCity(result);
            }
        });
        
    }

    var init = function (dataVariable) {
        webroot = dataVariable.ajax_call_root;
        states  = dataVariable.states;

        jQuery.validator.addMethod("noSpace", function (value, element, param) {
            return value.match(/^(?=.*\S).+$/);
        }, "No space please and don't leave it empty");
        
           jQuery.validator.addMethod("emailExt", function (value, element, param) {
            return value.match(/^[a-zA-Z0-9_\.%\+\-]+@[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,}$/);
        }, "Please enter valid email address");

        jQuery.validator.addMethod("phoneUS", function (phone_number, element) {
            phone_number = phone_number.replace(/\s+/g, "");
            return this.optional(element) || phone_number.length > 9 &&
                    phone_number.match(/^\(?(\d{3})\)?[-\. ]?(\d{3})[-\. ]?(\d{4})$/);
        }, "Invalid phone number");

        createFormValidator();
        autocompleteState();
    }

    return {
        init: init
    }

}();