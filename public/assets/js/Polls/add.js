var AddPolls = function (){
	var webroot;
	
	var createFormValidator = function() {
        $("#frmAddPolls").validate({
            ignore: [],
            rules: {
                title: {
                    required : true
                },
                question:{
                    required: function(textarea) {
                        CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                        return editorcontent.length === 0;
                    }
                },
                poll_response_1:{
                    required: function(textarea) {
                        CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                        return editorcontent.length === 0;
                    }
                },
                poll_response_2:{
                    required: function(textarea) {
                        CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                        return editorcontent.length === 0;
                    }
                }
            },
            messages: {
                title: {
                    required: "Please enter poll title"          
                },
                question: {
                    required: "Please enter question"          
                },
                poll_response_1: {
                    required : "Please enter response"        
                },
                poll_response_2: {
                    required : "Please enter response"        
                }
            },   
            submitHandler: function (form) { 
            return true;                
            }
        });
    }

	var init = function (dataVariable) {
        webroot = dataVariable.ajax_call_root;
		createFormValidator();
        
        /* fetching poll category*/
        $(document).on('keydown', '#category', function() {
            $("#category").typeahead({
                source: function(query, result)
                {
                    $.ajax({
                       url: webroot+"polls/getPollsCategory", 
                       method:"POST",
                       data:{search:query},
                       dataType:"json",
                       success:function(data) {
                       //console.log(data);
                          result($.map(data, function(item){
                             return item;
                          }));
                       },               
                    })
                },
                updater:function (item) {
                    //console.log(item.name);
                    $('.cats').append('<div class="tagItem">'+
                            item.name+'<i class="i fa fa-close" id="'+item.id+'"></i>'+
                            '<input type="hidden" name="cats[]" value="'+item.id+'"></div>');
                }
            });

            // Remove selected
            $(document).on('click','.fa-close', function() {
                $(this).parents('.tagItem').remove();
            });
        });   

        /*Fetching poll tags */
        $(document).on('keydown', '#tag', function() {
            $("#tag").typeahead({
                source: function(query, result)
                {
                    $.ajax({
                        url: webroot+"polls/getPollsTags", 
                        method:"POST",
                        data:{search:query},
                        dataType:"json",
                        success:function(data) {
                            //console.log(data);
                            result($.map(data, function(item){
                                return item;
                            }));
                       },               
                    })
                },
                updater:function (item) {
                    //console.log(item.name);
                    $('.tags').append('<div class="tagItem">'+
                            item.name+'<i class="i fa fa-close" id="'+item.id+'"></i>'+
                            '<input type="hidden" name="tags[]" value="'+item.id+'"></div>');
                }
            });

            // Remove selected
            $(document).on('click','.fa-close', function() {
                $(this).parents('.tagItem').remove();
            });
        }); 

	}		

	return {
		init:init,
		createFormValidator:createFormValidator
	}	

}();