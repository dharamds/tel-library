jQuery.validator.addMethod("noSpace", function(value, element) { 
  return value.indexOf(" ") < 0 && value != ""; 
}, "No space please and don't leave it empty");

jQuery.validator.addMethod("emailExt", function(value, element, param) {
			return value.match(/^[a-zA-Z0-9_\.%\+\-]+@[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,}$/);
		},"Please enter valide email address");


$("#add_email_templates_form_validation").validate({
     
    rules: {
        temp_code: {
            required: true,
            noSpace: true
        },
        subject: {
            required: true
        },
        message: {
            required: true
        },
        from_email: {
            required: true,
            email: true,
           
        },
        from_name: {
            required: true
        },
    },
    messages: {
        temp_code: {
            required: 'Please enter template code',
            noSpace: 'Space is not allowed'
        },
         subject: {
            required: 'Please enter subject'
        },
         message: {
            required: 'Please enter message'
        },
         from_email: {
            required: 'Please enter email'
        },
         from_name: {
            required: 'Please enter name'
        },
    }

});


$("#edit_email_templates_form_validation").validate({
    rules: {
        temp_code: {
            required: true,
            noSpace: true
        },
        subject: {
            required: true
        },
        message: {
            required: true
        },
        from_email: {
            required: true,
            email: true
        },
        from_name: {
            required: true
        }
    },
    messages: {
        temp_code: {
            required: 'Please enter template code',
            noSpace: 'Space is not allowed',
        },
         subject: {
            required: 'Please enter subject'
        },
         message: {
            required: 'Please enter message'
        },
         from_email: {
            required: 'Please enter email'
        },
         from_name: {
            required: 'Please enter name'
        },
    }
});


$('#add_email_templates_form_validation').on('submit', function(e) {
  if($('.editor').summernote('isEmpty')) {
    console.log('contents is empty, fill it!');
    alert('Please Enter Message');
    e.preventDefault();
  }
  else {
    
  }
})

$('#edit_email_templates_form_validation').on('submit', function(e) {
  if($('.editor').summernote('isEmpty')) {
    console.log('contents is empty, fill it!');
    alert('Please Enter Message');
    e.preventDefault();
  }
  else {
    
  }
})










