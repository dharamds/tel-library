var Category_create_edit = function (){
	var webroot;
	
	var createFormValidator = function() {
        $("#frmAddCategory").validate({
            rules: {                
                type: {
                    required : true
                },name: {
                    required : true,
                    noSpace: true
                },
            },
            messages: {
                type: {
                    required: "Please select type"               
                },
                name: {
                    required: "Please enter category name"          
                }
            },   
            submitHandler: function (form) { 
               return true;
            }
        });
    }

	var init = function (dataVariable) {
        webroot = dataVariable.ajax_call_root;
        
          jQuery.validator.addMethod("noSpace", function (value, element, param) {
            return value.match(/^(?=.*\S).+$/);
        }, "No space please and don't leave it empty");
        
		createFormValidator();
	}		

	return {
		init:init,
		createFormValidator:createFormValidator
	}	

}();