var Competencies = function (){
	var webroot;
	
	var createFormValidator = function() {
        $("#frmAddCompetencies").validate({
            rules: {
                name: {
                    required : true
                },
            },
            messages: {                
                name: {
                    required: "Please enter Learning outcome"          
                }
            },   
            submitHandler: function (form) { 
                $("#frmAddCompetencies").submit();
            }
        });
    }

	var init = function (dataVariable) {
        webroot = dataVariable.ajax_call_root;
		createFormValidator();
	}		

	return {
		init:init,
		createFormValidator:createFormValidator
	}	

}();