var LearningStandard = function (){
	var webroot;
	
	var createFormValidator = function() {
        $("#frmAddLearningStandard").validate({
            rules: {
                name: {
                    required : true
                },
            },
            messages: {                
                name: {
                    required: "Please enter Learning outcome"          
                }
            },   
            submitHandler: function (form) { 
                $("#frmAddLearningStandard").submit();
            }
        });
    }

	var init = function (dataVariable) {
        webroot = dataVariable.ajax_call_root;
		createFormValidator();
	}		

	return {
		init:init,
		createFormValidator:createFormValidator
	}	

}();