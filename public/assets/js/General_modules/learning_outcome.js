var LearningOutcome = function (){
	var webroot;
	
	var createFormValidator = function() {
        $("#frmAddLearningOutcome").validate({
            rules: {
                name: {
                    required : true
                },
            },
            messages: {                
                name: {
                    required: "Please enter Learning outcome"          
                }
            },   
            submitHandler: function (form) { 
                $("#frmAddLearningOutcome").submit();
            }
        });
    }

	var init = function (dataVariable) {
        webroot = dataVariable.ajax_call_root;
		createFormValidator();
	}		

	return {
		init:init,
		createFormValidator:createFormValidator
	}	

}();