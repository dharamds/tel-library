var Grades = function () {
    var webroot;

    var createFormValidator = function () {
        $("#add_grades_form_validation").validate({
            rules: {
                name: {
                    required: true,
                    noSpace:true
                   
                },
            },
            messages: {
                name: {
                    required: "Please enter grade name"
                }
            },
            submitHandler: function (form) {
                $("#add_grades_form_validation").submit();
            }
        });
    }

    var init = function (dataVariable) {
        webroot = dataVariable.ajax_call_root;
        
            jQuery.validator.addMethod("noSpace", function (value, element, param) {
            return value.match(/^(?=.*\S).+$/);
        }, "No space please and don't leave it empty");
        
       
        
        
        createFormValidator();
    }

    return {
        init: init
    }

}();