var Skills_create_edit = function (){
	var webroot;
	
	var createFormValidator = function() {
        $("#skills_create_edit_form_validation").validate({
            rules: {
                name: {
                    required : true
                }
            },
            messages: {
                name: {
                    required: "Please enter name"          
                }
            },   
            submitHandler: function (form) { 
                $("#skills_create_edit_form_validation").submit();
            }
        });
    }

	var init = function (dataVariable) {
        webroot = dataVariable.ajax_call_root;
		createFormValidator();

	}		

	return {
		init:init,
		createFormValidator:createFormValidator
	}	

}();