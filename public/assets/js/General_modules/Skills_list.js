var Skills_list = function (){
	var webroot;
	
	var serverside_datatable = function() {
        var table = table = table = $('#memListTable').DataTable({
            // Processing indicator
            "processing": true,
            // DataTables server-side processing mode
            "serverSide": true,
            // Initial no order.
            "iDisplayLength": 10,
            "bPaginate": true,
            "order": [],
            // Load data from an Ajax source
            "ajax": {
                "url": webroot+"general_modules/skills/getLists",
                "type": "POST",
                "data": function(data) {
                    //alert("h");
                },
            },
            //Set column definition initialisation properties
            "columnDefs": [{
                    "targets": [3],
                    "data": null,
                    "render": function(data, type, full, meta) {
                        if (type === 'display') {
                            if (full['status'] == '1') {
                                data = '<a href="'+webroot+'general_modules/skills/update_status/activate/' + full['id'] + '" data-toggle="tooltip" data-placement="top" title="Click to Change Status"><button type="button" class="btn btn-primary-alt btn-sm">Active</button></a>';
                            } else {
                                data = '<a href="'+webroot+'general_modules/skills/update_status/deactivate/' + full['id'] + '" data-toggle="tooltip" data-placement="top" title="Click to Change Status"><button type="button" class="btn btn-danger-alt btn-sm">Inactive</button></a>';
                            }
                        }
                        return data;

                    }

                },
                {
                    "targets": [4],
                    "data": null,
                    "render": function(data, type, full, meta) {
                        if (type === 'display') {
                            data = "<a class='btn btn-success-alt btn-sm' href='"+webroot+"general_modules/skills/edit_skills/" + full['id'] + "'><i class='ti ti-pencil'></i></a>";
                            data += '<a href="'+webroot+'general_modules/skills/delete_skills/' + full['id'] + ' " class="btn btn-danger-alt btn-sm delete_item" ><i class="ti ti-close"></i></a>';
                        }
                        return data;

                    }

                }
            ],
            "columns": [{
                    "data": "sr_no",
                    "autoWidth": true
                },
                {
                    "data": "name",
                    "autoWidth": true
                },
                {
                    "data": "created",
                    "autoWidth": true
                },
                {
                    "data": "status",
                    "autoWidth": true
                }
            ]
        });

        $(document).on('click', '.delete_item', function(e) {
            e.preventDefault();
            var scope = $(this);
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure?',
                buttons: {
                    confirm: function() {
                        $.get(scope.attr("href"), // url
                            function(data, textStatus, jqXHR) { // success callback
                                var obj = JSON.parse(data);
                                if (obj.msg === 'deleted') {
                                    // scope.closest("tr").remove();
                                    table.ajax.reload();  //just reload table
                                    //table.search('').draw();
                                }
                            });
                        return true;
                    },
                    cancel: function() {
                        return true;
                    }
                }
            });
        });
    }

	var init = function (dataVariable) {
        webroot = dataVariable.ajax_call_root;
        serverside_datatable();

	}		

	return {
		init:init,
		
	}	

}();