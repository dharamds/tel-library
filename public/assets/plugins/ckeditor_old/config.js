/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function(config)
{
    // Define changes to default configuration here. For example:
    // config.language = 'fr';
    // config.uiColor = '#AADC6E';
    //config.toolbar = 'MyToolbar';

    config.allowedContent = true;
    
    config.extraPlugins = 'youtube,doksoft_advanced_blocks,doksoft_button,doksoft_file,doksoft_image,doksoft_image_embed,doksoft_include,doksoft_maps,doksoft_preview,doksoft_resize,panelbutton,floatpanel,doksoft_special_symbols,doksoft_table,doksoft_templates,inlinecancel';
    config.toolbar_name = [['youtube'],[ 'doksoft_advanced_blocks'],['doksoft_button'],['doksoft_file'],['doksoft_image'],['doksoft_image_embed'],['doksoft_include'],['doksoft_maps'],['doksoft_preview'],['doksoft_resize'],['doksoft_special_symbols'],['doksoft_table'],['doksoft_templates'],['inlinecancel']];

    config.toolbar_Basic = [
                ['youtube','Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv',
                    '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', '-', 'Anchor', '-', 'TextColor', 'BGColor']
            ];
   
	//config.removePlugins = 'image';
    CKEDITOR.on('dialogDefinition', function(ev) {
        // Take the dialog window name and its definition from the event data.
        var dialogName = ev.data.name;
        var dialogDefinition = ev.data.definition;

        if (dialogName == 'image') {
            dialogDefinition.onShow = function() {
                // This code will open the Advanced tab.
                this.selectPage('Upload');
            };
        }
    });

};
