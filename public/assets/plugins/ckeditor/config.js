/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	
	//config.language = 'fr';
	//config.uiColor = '#AADC6E';
	//config.extraPlugins = 'mathjax';
	 config.extraPlugins = 'eqneditor, telplugin';
	//config.filebrowserBrowseUrl = '/tel_library/public/assets/plugins/ckeditor/plugins/ckfinder/ckfinder.html';
	//config.filebrowserUploadUrl = '/tel_library/public/assets/plugins/ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
	//config.filebrowserUploadUrl = 'http://192.168.1.74:7777/tel_library/media_sources/MediaSources/upload_image';

	config.toolbar_Full =
		[
			{ name: 'document',    items : [ 'Source','-','Save','NewPage','DocProps','Preview','Print','-','Templates' ] },
			{ name: 'clipboard',   items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
			{ name: 'editing',     items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
			{ name: 'forms',       items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
			'/',
			{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
			{ name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
			{ name: 'links',       items : [ 'Link','Unlink','Anchor' ] },
			{ name: 'insert',      items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak' ] },
			'/',
			{ name: 'styles',      items : [ 'Styles','Format','Font','FontSize'] },
			{ name: 'colors',      items : [ 'TextColor','BGColor' ] },
			{ name: 'tools',       items : [ 'Maximize', 'ShowBlocks','-','About' ] }
		];

	config.toolbar_custom = 
		[
			{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
			{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Scayt' ] },
			{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor'] },
			{ name: 'insert', items: [ 'Image', 'Table', 'HorizontalRule', 'SpecialChar' ] },
		];	

	config.toolbar_short = 
		[
			{ name: 'basicstyles', items : ['Source','-','Bold','Italic','Underline','Strike','Subscript','Superscript', 'Blockquote','-','Outdent','Indent', '-', 'NumberedList','BulletedList','-','Link', 'Unlink','-','Table', 'SpecialChar', 'EqnEditor', 'Telplugin']},
			{ name: 'styles', items : ['Styles','Format','Font','FontSize'] },
		];	

	config.toolbar_feedback = 
		[
			{ name: 'basicstyles', items : [ 'Bold','Italic','Underline', '-', 'Blockquote', '-', 'JustifyCenter','JustifyRight','JustifyBlock','-', 'Link','Unlink'] }, 
		];	
};


