﻿CKEDITOR.plugins.add('telplugin',
    {
        init: function (editor) {
            var pluginName = 'telplugin';
            editor.ui.addButton('Telplugin',
                {
                    label: 'Media Gallery',
                    command: 'OpenWindow',

                    icon: CKEDITOR.plugins.getPath('telplugin') + 'mybuttonicon.gif'
                });
            var cmd = editor.addCommand('OpenWindow', { exec: showMyDialog });
        }
    });

function showMyDialog(e) {
    var instances_data = e.title.split(',');
    CommanJS.choose_media_image_file('image_callback', instances_data[1]);
}
function image_callback(image_name, img_full_url, instance_name) {
    var extension = img_full_url.substr(img_full_url.lastIndexOf('.') + 1);
    switch (extension) {
        case 'jpg':
        case 'jpeg':
        case 'png':
        case 'gif':
            CKEDITOR.instances[instance_name.id].insertHtml('<img src="' + img_full_url + '">');
            CKEDITOR.instances[instance_name.id].updateElement();
            break;
        case 'mp3':
        case 'mb':
            CKEDITOR.instances[instance_name.id].insertHtml('<audio controls><source src="' + img_full_url + '"></audio>');
            CKEDITOR.instances[instance_name.id].updateElement();
            break;
        default:
            CKEDITOR.instances[instance_name.id].insertHtml('<img src="' + img_full_url + '">');
            CKEDITOR.instances[instance_name.id].updateElement();
    }


}