<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Auther : Dharam Pardhi
* IDE    : Sublime
* Date   : 2/5/2017
*/
class Common_model extends CI_Model
{
	public function get_permissions()
	{
		$user_id = $this->ion_auth->user()->row()->id;
			if($user_id> 0){
				$this->db->select('*');
				$this->db->from('users_roles');
				$this->db->where(array('users_roles.user_id' => $user_id));
				$query = $this->db->get();	
				$role_ids = array_column($query->result(), 'role_id');
				//pr($role_ids); exit;
				$this->db->select('*');
				$this->db->from('menus_permissions');
				$this->db->join('menus', 'menus.id = menus_permissions.menu_id', 'inner');	
				$this->db->where(['menus.slug' => uri_string()]);
				$this->db->group_start();
				$this->db->where_in('menus_permissions.role_id', $role_ids);
				$this->db->or_where('menus_permissions.user_id', $user_id);
				$this->db->group_end();
				//$this->db->get()->row();	
				//echo $this->db->last_query();
				//exit;
				return $this->db->get()->row();	
			}
	}
	public function add($table,$data)
	{
		$query = $this->db->insert($table,$data);
		return TRUE;
	}
	public function select($table)
	{
		$query = $this->db->get($table);
		return $query->result();
	}

	public function update_data($id,$table)
	{
		$this->db->where($id);
		$query = $this->db->get($table);
		return $query->row();
	}

	public function update($id,$data,$table)
	{
		
		if (empty($id)) return FALSE;

		$this->db->update($table, $data, array('id' => $id));
		
		return TRUE;
	}
	function UpdateDB($table,$Where,$Data)
	{
		
		$this->db->where($Where);
		$Update = $this->db->update($table,$Data);
		if ($Update):
			return true;
		else:
			return false;
		endif;
	}

	public function delete($id,$table)
	{
		$this->db->where($id);
		$this->db->delete($table);
		return TRUE;
	}

	function getAllData($table, $specific='', $row='', $Where='', $order='', $limit='', $groupBy='',$like = '', $where_in = '', $where_ins='')
	{
		// If Condition
		if (!empty($Where)):
			$this->db->where($Where);
		endif;
		// If Condition
		if (!empty($where_in)):
			$this->db->where_in($where_in, $where_ins);
		endif;
		// If Specific Columns are require
		if (!empty($specific)):
			$this->db->select($specific);
		else:
			$this->db->select('*');
		endif;

		if (!empty($groupBy)):
			$this->db->group_by($groupBy);
		endif;
		// if Order
		if (!empty($order)):
			$this->db->order_by($order);
		endif;
		// if limit
		if (!empty($limit)):
			$this->db->limit($limit);
		endif;

		//if like
		if(!empty($like)):
			$this->db->like($like);
		endif;	
		// get Data
		
		//if select row
		if(!empty($row)):
			$GetData = $this->db->get($table);
			return $GetData->row();
		else:
			$GetData = $this->db->get($table);
			return $GetData->result();
		endif;	
	}
function getDataById($table, $fields, $conditons) {
		$this->db->select($fields);
		$this->db->from($table);
        $this->db->where($conditons);
        $query = $this->db->get();
        return $query->row();
	}
	function DJoin($field,$tbl,$jointbl1,$Joinone,$row='',$jointbl3='',$Where='',$order='',$groupy = '',$limit = '', $offset = 0, $query = '')
    {
        $this->db->select($field);
        $this->db->from($tbl);
        $this->db->join($jointbl1,$Joinone);
        if (!empty($jointbl3)):
            foreach ($jointbl3 as $Table => $On):
                $this->db->join($Table,$On);
            endforeach;
        endif;
        // if Group
		if (!empty($groupy)):
			$this->db->group_by($groupy);
		endif;
        if(!empty($order)):
            $this->db->order_by($order);
        endif;
        if(!empty($Where)):
            $this->db->where($Where);
        endif;
        if(!empty($limit)):
            $this->db->limit($limit, $offset);
        endif;
        
        if(!empty($query)):
            $this->db->like($like, $query);
        endif;

        if(!empty($row)):
			$query = $this->db->get();
			return $query->row();
		else:
	        $query=$this->db->get();
	        return $query->result();
	    endif;	    

    }
}