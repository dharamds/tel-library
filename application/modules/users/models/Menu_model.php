<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_model extends CI_Model 
{

	function __construct() {
    parent::__construct();

    $this->tableName = 'menus';
  }
  // get all menu items
  function get_items() {
    $this->db->select('*');
    $this->db->from($this->tableName);
    $this->db->order_by('parent_id');
    $this->db->order_by('number');
    $query = $this->db->get();
    return $query->result_array();
  }

  // get menu as per current user role
  public function getCurrentMenu()
    {
     
      $current_user_id = $this->ion_auth->get_user_id();
      //$current_role_id = $this->ion_auth->get_user_role();
      $current_role_id = $this->ion_auth->user_role_id($current_user_id)->role_id;

     //pr($current_user_id);
     // pr($data); exit;
      $this->db->select($this->tableName.'.*');
      $this->db->from($this->tableName);
      $this->db->join('menus_permissions', 'menus_permissions.menu_id = menus.id', 'inner');
      $this->db->where(['menus_permissions.role_id' => $current_role_id]);
      $this->db->where([$this->tableName.'.status' => 1]);
      $this->db->or_where(['menus_permissions.user_id' => $current_user_id]);
      $this->db->order_by('parent_id');
      $this->db->order_by('number');
      return $this->generateTreeMenu($this->db->get()->result_array());
    }

    function generateTreeMenu($items = array(), $parent_id = 0, $selected = 0){
    $tree = '<ul class="acc-menu">';
    for($i=0, $ni=count($items); $i < $ni; $i++){  
      $select = '';
      if($items[$i]['parent_id'] == $parent_id){

        if($selected == $items[$i]['id']){ $select = 'selected';}
        
        if($items[$i]['parent_id'] > 1){ $url = base_url("/".$items[$i]['slug']);}else{ $url = 'javascript:void(0);';}
        $tree .= '<li class="hasChild"><a href="'.$url.'">';
        $tree .= '<i class="'.$items[$i]['icon'].'"></i> <span>'.$items[$i]['name'].'</span></a>';
        $tree .= $this->generateTreeMenu($items, $items[$i]['id'], $selected);
        $tree .= '</li>';
      }else{

      }

    }
    $tree .= '</ul>';
    return $tree;
  }

      function generateTree($items = array(), $parent_id = 0){
        
        for($i=0, $ni=count($items); $i < $ni; $i++){
          if($items[$i]['parent_id'] == $parent_id){
            $tree .= '<div class="flex-col-md-6"><div class="user-menu-block">';
            $tree .= '<h4>';
            if($items[$i]['id'] > 1){ 

              $role_names = $this->role_names($items[$i]['id']);
            }
            $tree .= $items[$i]['name'].'&nbsp;<div class="pull-right mr-4"><a href="'.base_url('/users/menus/save_menu/'.$items[$i]['id']).'"  class="btn btn-success btn-xs btn-grid"><i class="fa fa-pencil"></i></a><a  href="'.base_url('/users/menus/delete_menu/'.$items[$i]['id']).'" class="delete_confirm btn btn-danger btn-xs btn-grid" ><i class="fa fa-trash"></i></a></div><div class="user-role-name">'.$role_names.'</div>';
          //}else{
        //    $tree .= $items[$i]['name'];
        // }
            $tree .= '</h4>';
            $tree .= $this->generateSubTree($items, $items[$i]['id']);
            $tree .= '</div></div>';
          }
        }
        
        return $tree;
      }

      function generateSubTree($items = array(), $parent_id = 0){
        $tree = '<ul class="list-group">';
        for($i=0, $ni=count($items); $i < $ni; $i++){
          if($items[$i]['parent_id'] == $parent_id){
            $tree .= '<li class="list-group-item">';
            if($items[$i]['id'] > 1){ 

              $role_names = $this->role_names($items[$i]['id']);
            }
            $tree .= $items[$i]['name'].'&nbsp;<div class="pull-right"><a href="'.base_url('/users/menus/save_menu/'.$items[$i]['id']).'"  class="btn btn-success btn-xs btn-grid"><i class="fa fa-pencil"></i></a><a  href="'.base_url('/users/menus/delete_menu/'.$items[$i]['id']).'" class="delete_confirm btn btn-danger btn-xs btn-grid" ><i class="fa fa-trash"></i></a></div><div class="user-role-name">'.$role_names.'</div>';
            $tree .= '</li>';
          }
        }
        $tree .= '</ul>';
        return $tree;
      }


      function role_names($menu_id){ //pr($menu_id);
        $this->db->select('roles.name')
          ->from('menus_permissions')
          ->join('roles', 'menus_permissions.role_id = roles.id', 'inner')
          ->where(['menus_permissions.menu_id' => $menu_id, 'roles.id >' => 1]);
           $query = $this->db->get();
          return$this->convert_multi_array($query->result());
      }

      function convert_multi_array($array) {
        return implode("",array_map(function($a) { return '<span class="btn btn-info btn-xs">'.$a->name.'</span>'; }, $array) );
      }
      

    function generateOptionTree($items = array(), $parent_id = 0, $selected = 0){
    $tree = '';
    for($i=0, $ni=count($items); $i < $ni; $i++){

      $dash = ($items[$i]['parent_id'] == 0) ? '' : str_repeat('-', (2)) .' ';
      $select = '';
      if($items[$i]['parent_id'] == $parent_id){
        if($selected == $items[$i]['id']){ $select = 'selected';}
        $tree .= '<option value="'.$items[$i]['id'].'" '.$select.'>';
        $tree .= $dash.$items[$i]['name'];
        $tree .= $this->generateOptionTree($items, $items[$i]['id'], $selected);
        $tree .= '</option>';
      }else{

      }

    }
    $tree .= '';
    return $tree;
  }

  /**
   * save_menu
   *
   * @param string|bool $name
   * @param string      $description
   * @param array       $additional_data
   *
   * @return int|bool The ID of the inserted role, or FALSE on failure
   * @author aditya menon
   */
  public function save_menu($data = [], $id = NULL)
  {
    if($id)
    {
      // update the new menu
      $this->db->where('id', $id);
      $this->db->update($this->tableName, $data);
      $return = $id;
    }else{ 
      // insert the new menu
      $this->db->insert($this->tableName, $data);
      //pr($data); exit;s
      $this->db->trans_commit();
    $return = $this->db->insert_id();
    }
    return $return;
  }

/**
   * save_menu
   *
   * @param string|bool $name
   * @param string      $description
   * @param array       $additional_data
   *
   * @return int|bool The ID of the inserted role, or FALSE on failure
   * @author aditya menon
   */
  public function save_menu_permission_users( $user_id = null, $menu_id = null)
  {
    //pr($menu_id);
    $this->db->select('*');
    $this->db->where('user_id', $user_id);
    $this->db->where('menu_id', $menu_id);

    //pr($this->db->get('menus_permissions')->row()); exit;


    if($this->db->get('menus_permissions')->row())
    {
      $return = $menu_id.'All ready exit;'.$user_id;
    }else{ 
      $data['menu_id'] = $menu_id;
      $data['user_id'] = $user_id;
      // insert the new menu
      $this->db->insert('menus_permissions', $data);
     $return = 'Added';
    }
    return $return;
  }

/**
   * delete_menu
   *
   * @param string|bool $name
   * @param array       $additional_data
   *
   * @return int|bool The ID of the inserted role, or FALSE on failure
   * @author aditya menon
   */
  public function delete_menu($id = NULL)
  {
    if($id)
    {
      // delete the menu
      $this->db->where('id', $id);
      $this->db->delete($this->tableName);

      // delete the menu associations
      $this->db->where('menu_id', $id);
      $this->db->delete('menus_permissions');
    return $id;
  }
}



} // End of Model Class
