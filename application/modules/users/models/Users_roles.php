<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_roles extends CI_Model 
{

	public function check_group($table,$group_name)
	{
		$this->db->where('name', $group_name);
		$query = $this->db->get($table);
		if ($query->num_rows() > 0)
		{
          return TRUE;
        }
        else
        {
          return FALSE;
        }
	}
	public function update($id,$data,$table)
	{
		if (empty($id)) return FALSE;
		$this->db->where($id);
		$this->db->update($table,$data);
		return TRUE;
	}

	public function get_user_containers($id)
	{
		$this->db->select('*')
				 ->from('roles')
				 ->join('containers_roles', "roles.id = containers_roles.role_id")
				 ->join('containers', "containers.id = containers_roles.container_id")
				 ->where('containers_roles.role_id',$id);
		$query = $this->db->get();
		return $query->result(); 
	}

	public function remove_from_containers($container_ids=false, $role_id=false)
	{
		// role id is required
		if(empty($role_id))
		{
			return FALSE;
		}

		// if privilege id(s) are passed remove privilege from the role(s)
		
		if(!is_array($container_ids))
		{
			$container_ids = array($container_ids);
		}

		foreach($container_ids as $privilege_id)
		{
			$this->db->select('*')
					 ->from('containers_roles')
					 ->join('roles', "roles.id = containers_roles.role_id")
					 ->join('containers', "containers.id = containers_roles.container_id")
					 ->where('containers_roles.role_id',$role_id);
			$this->db->delete();		 	
		}

		return TRUE;
	}
	// get Current user Roles
	
	public function currentUserRole()
		{
			$user_id = $this->ion_auth->user()->row()->id;

			//$user_id = 196;
			
			if($usersData = $this->db->select('roles.*, MIN(roles.id) as id, MIN(roles.name) as name')->join('roles' , 'roles.id = containers_users_roles.role_id')->where(['containers_users_roles.user_id' => $user_id])->group_by('containers_users_roles.user_id')->get('containers_users_roles')->row()){
				return $usersData;
			}else{
				return $this->db->select('*')->join('roles' , 'roles.id = users_roles.role_id')->where(['users_roles.user_id' => $user_id])->get('users_roles')->row();
			}

			
		}
}

/* End of file Users_groups.php */
/* Location: ./application/models/Users_groups.php */