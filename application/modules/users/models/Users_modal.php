<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
Author Salman Iqbal
Company Parexons
Date 26/1/2017
*/

class Users_modal extends CI_Model
{

	function __construct()
	{

		// Set table name
		$this->table = 'users';
		// Set orderable column fields

		$this->column_order = array(null, null, 'active', 'user_name', 'users.email', 'institution', 'course_name', 'course_section_name', 'users.date', 'parent', 'roles');
		// Set searchable column fields

		$this->column_search = array('users.first_name', 'users.last_name', 'users.email', 'containers.name');
		// Set default order

		$this->order = array('users.id' => 'desc');
		$this->user_container_id = get_container_id();
	}

	function set_user_container_id($id)
	{
		$this->user_container_id = $id;
	}

	/*
     * Generate unique user id from the database
     * @param No parameters
     */
	public function generate_unique_user_id()
	{
		return 'TEL' . date('ym') . $this->db->select("MAX($this->table.id) + 1 as id")->from($this->table)->get()->row()->id;
	}

	/*
     * Fetch members data from the database
     * @param $_POST filter data based on the posted parameters
     */
	public function getRows($postData)
	{

		$this->_get_datatables_query($postData);

		if ($postData['length'] != -1) {
			$this->db->limit($postData['length'], $postData['start']);
		}
		$query = $this->db->get();
		$data['result'] = $query->result();
		/*pr($this->db->last_query());
		exit;*/
		$data['total'] = $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total;
		return $data;
	}


	private function _get_users_query($role_id = STUDENT, $postData)
	{

		ini_set("mysql.trace_mode", "0");
		$this->db->select('SQL_CALC_FOUND_ROWS users.id, users.email, users.active, CONCAT_WS(" ", users.first_name, users.middle_name, users.last_name) as user_name, roles.name', false);
		$this->db->select('DATE_FORMAT(' . $this->table . '.date, "%m/%d/%Y") as date');

		$this->db->select('COALESCE(containers.name, \'NA\') as institution');

		$this->db->from($this->table);
		$this->db->join('users_roles', $this->table . '.id = users_roles.user_id');
		$this->db->join('roles', 'roles.id = users_roles.role_id');
		$this->db->join('containers_users_roles', $this->table . '.id = containers_users_roles.user_id', 'left');
		$this->db->join('containers', 'containers.id = containers_users_roles.container_id', 'left');

		$i = 0;
		// loop searchable columns 
		foreach ($this->column_search as $item) {

			// if datatable send POST for search
			if ($postData['search']['value']) {
				// first loop
				if ($i === 0) {
					// open bracket 
					$this->db->group_start();
					//pr( $item);	
					$this->db->like($item, $postData['search']['value'], 'both');
				} else {
					$this->db->or_like($item, $postData['search']['value'], 'both');
				}
				// last loop
				if (count($this->column_search) - 1 == $i) {
					// close bracket
					$this->db->group_end();
				}
			}
			$i++;
		}




		if (isset($postData['active']) && $postData['active'] < 2) {
			$this->db->where('users.active', $postData['active']);
		}
		$this->db->where('users.delete_status', 1);
		$this->db->where('users_roles.role_id', $role_id);
	}



	public function getListings($role_id = STUDENT, $postData)
	{
		$this->_get_users_query($role_id, $postData);

		if ($postData['length'] != -1) {
			$this->db->limit($postData['length'], $postData['start']);
		}
		$query = $this->db->get();

		$data['result'] = $query->result();
		$data['total'] = $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total;
		return $data;
	}


	/*
     * Fetch members data from the database
     * @param $_POST filter data based on the posted parameters
     */
	public function getUsers($role_id = STUDENT)
	{
		$column_search = array('users.first_name', 'users.last_name', 'users.email');

		$this->db->select('users.id, CONCAT_WS(" ", users.first_name, users.middle_name, users.last_name) as name');
		$this->db->from($this->table);
		$this->db->join('users_roles', $this->table . '.id = users_roles.user_id');
		$this->db->join('roles', 'roles.id = users_roles.role_id');
		$search = $this->input->post('query');

		foreach ($this->column_search as $item) {

			// if datatable send POST for search
			if ($postData['search']['value']) {
				// first loop
				if ($i === 0) {
					// open bracket 
					$this->db->group_start();
					//pr( $item);	
					$this->db->like($item, $search, 'after');
				} else {
					$this->db->or_like($item, $search, 'after');
				}
				// last loop
				if (count($this->column_search) - 1 == $i) {
					// close bracket
					$this->db->group_end();
				}
			}
			$i++;
		}



		$this->db->where('users.active', 1);
		$this->db->where('users_roles.role_id', $role_id);
		return  $this->db->get()->result();
	}


	/*
     * Count records based on the filter params
     * @param $_POST filter data based on the posted parameters
     */
	public function countFiltered($postData)
	{
		$this->_get_datatables_query($postData);
		$query = $this->db->get();
		return $query->num_rows();
	}

	/*
     * Perform the SQL queries needed for an server-side processing requested
     * @param $_POST filter data based on the posted parameters
     */
	private function _get_datatables_query($postData)
	{

		ini_set("mysql.trace_mode", "0");
		$this->db->select('SQL_CALC_FOUND_ROWS users.id, users.email, users.date, users.active, CONCAT_WS(" ", users.first_name, users.middle_name, users.last_name) as user_name, "NA" as parent, roles.id as role_id, GROUP_CONCAT(DISTINCT roles.name SEPARATOR "|") as roles,users.phone,users.institutional_unique_id,users.institutional_unique_id,users.cellphone,users.standing_year,users.dob,', false);
		$this->db->select('COALESCE(containers.name, \'NA\') as institution');

		//$this->db->select('COALESCE(course_sections.name, \'NA\') as course_section_name');

		$this->db->select('DATE_FORMAT(' . $this->table . '.date, "%m/%d/%Y") as date');
		if ($postData['order']['0']['column'] == 6) {
			$this->db->select('GROUP_CONCAT(DISTINCT COALESCE(courses.name, \'NA\') SEPARATOR "<br>")  as course_name');
		} else {
			$this->db->select('"..." as course_name');
		}

		if ($postData['order']['0']['column'] == 7) {
			$this->db->select('GROUP_CONCAT(DISTINCT COALESCE(course_sections.name, \'NA\') SEPARATOR "<br>")  as course_section_name');
		} else {
			$this->db->select('"..." as course_section_name');
		}
		$this->db->select('DATE_FORMAT(course_assigned_to_users.created, "%m/%d/%Y") as enrolled_date');
		//$this->db->select("IF(".$this->table.".active = 1, 'Active', 'Inactive') as active");

		$this->db->from($this->table);

		$this->db->join('users_roles', $this->table . '.id = users_roles.user_id');
		$this->db->join('containers_users_roles', $this->table . '.id = containers_users_roles.user_id', 'left');

		if (isset($postData['container_selected']) && $postData['container_selected'] > 0) {
			$this->db->join('roles', 'roles.id = containers_users_roles.role_id');
		} else {
			$this->db->join('roles', 'roles.id = users_roles.role_id');
		}

		// $this->db->join('timezones', 'timezones.id = users.time_zone', 'left');
		$this->db->join('containers', 'containers.id = containers_users_roles.container_id', 'left');

		// $this->db->join('user_parents', $this->table . '.id = user_parents.user_id', 'left');
		// $this->db->join('users as parents', 'parents.id = user_parents.parent_id', 'left');

		//$this->db->join('container_courses', 'container_courses.container_id = containers.id', 'left');


		/*$this->db->join('course_assigned_to_users', 'course_assigned_to_users.container_id = containers.id AND course_assigned_to_users.user_id = users.id', 'left');
		$this->db->join('courses', 'courses.id = course_assigned_to_users.course_id AND course_assigned_to_users.user_id = users.id', 'left');
		$this->db->join('course_sections', 'course_assigned_to_users.section_id = course_sections.id AND course_assigned_to_users.user_id = users.id', 'left');*/

		$this->db->join('course_assigned_to_users', 'course_assigned_to_users.container_id = containers.id AND course_assigned_to_users.user_id = users.id', 'left');
		if ($postData['order']['0']['column'] == 6) {
		$this->db->join('courses', 'courses.id = course_assigned_to_users.course_id AND course_assigned_to_users.user_id = users.id', 'left');
		}
		/*$this->db->join('container_courses', 'container_courses.container_id = containers.id AND container_courses.course_id = courses.id', 'inner');*/
		if ($postData['order']['0']['column'] == 7) {
		$this->db->join('course_sections', 'course_sections.id = course_assigned_to_users.section_id AND course_assigned_to_users.user_id = users.id', 'left');
		}


		$i = 0;
		// loop searchable columns 
		foreach ($this->column_search as $item) {
			// if datatable send POST for search
			if ($postData['search']['value']) {
				// first loop
				if ($i === 0) {
					// open bracket 
					$this->db->group_start();
					//pr( $item);	
					$this->db->like($item, $postData['search']['value'], 'after');
				} else {
					$this->db->or_like($item, $postData['search']['value'], 'after');
				}
				// last loop
				if (count($this->column_search) - 1 == $i) {
					// close bracket
					$this->db->group_end();
				}
			}
			$i++;
		}


		if (isset($postData['check_users'])) {
			$checked_user = array_filter(explode('|', $postData['check_users']));
			if (count($checked_user) > 0) {
				$this->db->where_in('users.id', $checked_user);
			}
		}

		if (isset($postData['active']) && $postData['active'] < 2) {
			$this->db->where('users.active', $postData['active']);
		}
		if (isset($postData['roles_selected']) && $postData['roles_selected'] > 0) {
			$this->db->where('users_roles.role_id', $postData['roles_selected']);
		}
		// condition for container
		if (isset($postData['container_selected']) && $postData['container_selected'] > 0) {
			$this->db->where('containers_users_roles.container_id', $postData['container_selected']);
		}

		// condition for courses
		if (isset($postData['course_selected']) && $postData['course_selected'] > 0) {
			$this->db->where('course_assigned_to_users.course_id', $postData['course_selected']);
		}
		// condition for sections
		if (isset($postData['section_selected']) && $postData['section_selected'] > 0) {
			$this->db->where('course_assigned_to_users.section_id', $postData['section_selected']);
		}

		// set users 
		if ($this->user_container_id > 0) {
			$this->db->where(['containers_users_roles.container_id' => $this->user_container_id]);
		}

		$this->db->where('users.id >', 1);
		$this->db->where('users.delete_status', 1);
		$this->db->group_by('users.id');

		if (isset($postData['order'])) {
			$this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	/*
     * Perform the SQL queries needed for an server-side processing requested
     * @param $_POST filter data based on the posted parameters
     */
	public function get_cources_by_container($container_id)
	{
		$this->db->select('courses.name, courses.id');
		$this->db->from('container_courses');
		$this->db->join('courses', 'courses.id = container_courses.course_id');
		$this->db->where(['container_courses.container_id' => $container_id, 'container_courses.status' => 1, 'courses.delete_status' => 1, 'courses.status' => 1]);
		return $this->db->get()->result();
	}

	public function get_sections_by_course($container_id, $course_id)
	{
		$this->db->select('course_sections.name, course_sections.id');
		$this->db->from('course_sections');
		$this->db->where(['course_sections.institute_id' => $container_id, 'course_sections.course_id' => $course_id, 'course_sections.status' => 1]);
		return $this->db->get()->result();
	}

	// get_groups_by_section

	public function get_groups_by_section($postData)
	{

		$this->db->select('container_groups.name, container_groups.id');
		$this->db->from('container_groups');
		$this->db->where(['container_groups.container_id' => $postData['container_id'], 'container_groups.course_id' => $postData['course_id'], 'container_groups.section_id' => $postData['section_id'], 'container_groups.status' => 1]);
		return $this->db->get()->result();
	}

	// get All Users
	public function all_users()
	{
		$this->db->order_by('id', 'desc');
		$this->db->where(['id > ' => 1]);
		$this->db->limit(10);
		$query = $this->db->get('users');
		return $query->result();
	}

	//Count Users
	public function count_users()
	{
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where(['id > ' => 1]);
		return $this->db->count_all_results();
	}

	public function recent_users()
	{
		$this->db->where('date', date('Y-m-d'));
		$query = $this->db->get('users');

		return $count = $query->num_rows();
	}

	public function weekly_data()
	{
		$this->db->select('id');
		$this->db->from('users');
		$this->db->where('DATE > DATE_SUB(NOW(), INTERVAL 1 WEEK)');
		return $this->db->count_all_results();
	}

	public function get_user_privileges($id)
	{
		$this->db->select('*')
			->from('roles')
			->join('role_perm', "roles.id = role_perm.role_id")
			->join('permissions', "permissions.perm_id = role_perm.perm_id")
			->where('role_perm.role_id', $id);
		$query = $this->db->get();
		return $query->result();
	}
	/////////////////
	public function remove_from_privileges($privilege_ids = false, $role_id = false)
	{
		// role id is required
		if (empty($role_id)) {
			return FALSE;
		}

		// if privilege id(s) are passed remove privilege from the role(s)

		if (!is_array($privilege_ids)) {
			$privilege_ids = array($privilege_ids);
		}

		foreach ($privilege_ids as $privilege_id) {
			$this->db->select('*')
				->from('role_perm')
				->join('roles', "roles.id = role_perm.role_id")
				->join('permissions', "permissions.perm_id = role_perm.perm_id")
				->where('role_perm.role_id', $role_id);
			// ->where('role_perm.perm_id',$privilege_id);
			$this->db->delete();
		}

		return TRUE;
	}
	public function get_role_users($role)
	{
		$this->db->select('email')
			->from('roles')
			->join('users_roles', 'roles.id = users_roles.role_id')
			->join('users', 'users.id = users_roles.user_id')
			->where('roles.id', $role);
		$query = $this->db->get();
		return $query->result();
	}

	/**
	 * get_details_by_email method
	 * @description this function use to get user data
	 * @return array
	 */
	public function get_details_by_email($email)
	{
		$this->db->select('email,id');
		$this->db->where(['users.email' => $email, 'users.delete_status' => 1]);
		$query = $this->db->get("users");
		return $query->row();
	}

	function get_parents()
	{
		$user_id = $this->ion_auth->user()->row()->id;
		$this->db->select('user_parents.id, users.email, users.first_name, users.middle_name, users.last_name')
			->from('user_parents')
			->join('users', 'user_parents.parent_id = users.id', 'inner')
			->where(['user_parents.user_id' => $user_id]);
		$query = $this->db->get();
		return $query->result();
	}

	function remove_parents($id)
	{
		$this->db->where(['id' => $id]);
		$this->db->delete('user_parents');
		return 'Parent removed successfully.';
	}
	function SetCurrentContainer()
	{ }

	public function export_users($postData)
	{
		$this->_get_datatables_query($postData);
		$query = $this->db->get();
		$data['result'] = $query->result_array();
		return $data;
	}

	public function delete_selected_users($postData)
	{
		//$checked_user = array_filter(explode('|',$_POST['selected_user_id']);	
		$checked_user = array_filter(explode('|', $postData));
		$this->db->set('delete_status', 0);
		$this->db->set('active', 0);
		$this->db->where_in('id', $checked_user);
		$this->db->update('users');
		return true;
	}

	public function current_courses($postData)
	{

		$this->_get_datatables_query_current_courses($postData);

		if ($postData['length'] != -1) {
			$this->db->limit($postData['length'], $postData['start']);
		}
		$query = $this->db->get();
		$data['result'] = $query->result();
		//pr($this->db->last_query());
		//exit;
		$data['total'] = $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total;
		return $data;
	}

	private function _get_datatables_query_current_courses($postData)
	{

		ini_set("mysql.trace_mode", "0");
		$this->db->select('SQL_CALC_FOUND_ROWS courses.name as course_name,course_sections.name as section_name,container_groups.name as group_name,container_groups_users.is_leader,CONCAT_WS(" ", users.first_name, users.middle_name, users.last_name) as group_leader', false);
		$this->db->from('course_assigned_to_users');
		$this->db->join('courses', 'courses.id = course_assigned_to_users.course_id');
		$this->db->join('container_groups_users', 'container_groups_users.user_id=course_assigned_to_users.user_id');
		$this->db->join('course_sections', 'course_sections.id = container_groups_users.section_id');
		$this->db->join('container_groups', 'container_groups.id = container_groups_users.container_group_id');
		$this->db->join('users', 'users.id=course_assigned_to_users.user_id');
		$i = 0;
		// loop searchable columns 
		foreach ($this->column_search as $item) {
			// if datatable send POST for search
			if ($postData['search']['value']) {
				// first loop
				if ($i === 0) {
					// open bracket 
					$this->db->group_start();
					//pr( $item);	
					$this->db->like($item, $postData['search']['value'], 'after');
				} else {
					$this->db->or_like($item, $postData['search']['value'], 'after');
				}
				// last loop
				if (count($this->column_search) - 1 == $i) {
					// close bracket
					$this->db->group_end();
				}
			}
			$i++;
		}
		$this->db->where('container_groups_users.user_id', $postData['user_id']);
	}

	public function current_course_group($postData)
	{

		$this->_get_datatables_query_current_course_group($postData);

		if ($postData['length'] != -1) {
			$this->db->limit($postData['length'], $postData['start']);
		}
		$query = $this->db->get();
		$data['result'] = $query->result();
		//pr($this->db->last_query());
		//exit;
		$data['total'] = $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total;
		return $data;
	}

	private function _get_datatables_query_current_course_group($postData)
	{

		ini_set("mysql.trace_mode", "0");
		$this->db->select('SQL_CALC_FOUND_ROWS courses.name as course_name,course_sections.name as section_name,container_groups.name as group_name,DATE_FORMAT(courses.course_expiry, "%m/%d/%Y") as course_expiry,DATE_FORMAT(courses.created, "%m/%d/%Y") as course_created,count(container_groups_users.user_id) as enrolled_students', false);
		$this->db->from('container_groups_users');
		$this->db->join('courses', 'courses.id = container_groups_users.course_id');
		$this->db->join('course_sections', 'course_sections.id = container_groups_users.section_id');
		$this->db->join('container_groups', 'container_groups.id = container_groups_users.container_group_id');
		$i = 0;
		// loop searchable columns 
		foreach ($this->column_search as $item) {
			// if datatable send POST for search
			if ($postData['search']['value']) {
				// first loop
				if ($i === 0) {
					// open bracket 
					$this->db->group_start();
					//pr( $item);	
					$this->db->like($item, $postData['search']['value'], 'after');
				} else {
					$this->db->or_like($item, $postData['search']['value'], 'after');
				}
				// last loop
				if (count($this->column_search) - 1 == $i) {
					// close bracket
					$this->db->group_end();
				}
			}
			$i++;
		}
		$this->db->where('container_groups_users.user_id', $postData['user_id']);
		$this->db->group_by("container_groups_users.container_group_id");
	}

	public function current_institution_group($postData)
	{

		$this->_get_datatables_query_current_institution_group($postData);

		if ($postData['length'] != -1) {
			$this->db->limit($postData['length'], $postData['start']);
		}

		$query = $this->db->get();

		$data['result'] = $query->result();
		$data['total'] = $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total;
		return $data;
	}

	public function _get_datatables_query_current_institution_group($postData)
	{
		//pr($this->user_container_id,"k");
		ini_set("mysql.trace_mode", "0");
		$this->db->select('SQL_CALC_FOUND_ROWS container_groups.name as group_name,CONCAT_WS(" ", users.first_name, users.middle_name, users.last_name) as user_name', false);
		$this->db->from('container_groups');
		$this->db->join('container_groups_users', 'container_groups_users.container_id=container_groups.id');
		$this->db->join('users', 'users.id=container_groups_users.user_id');
		$this->db->where(['container_groups.container_id' => $this->user_container_id, 'container_groups_users.is_leader' => 1]);

		$i = 0;
		// loop searchable columns 
		foreach ($this->column_search as $item) {
			// if datatable send POST for search
			if ($postData['search']['value']) {
				// first loop
				if ($i === 0) {
					// open bracket 
					$this->db->group_start();
					//pr( $item);	
					$this->db->like($item, $postData['search']['value'], 'after');
				} else {
					$this->db->or_like($item, $postData['search']['value'], 'after');
				}
				// last loop
				if (count($this->column_search) - 1 == $i) {
					// close bracket
					$this->db->group_end();
				}
			}
			$i++;
		}
	}

	public function assigned_courses($postData)
	{

		$this->_get_datatables_query_assigned_courses($postData);

		if ($postData['length'] != -1) {
			$this->db->limit($postData['length'], $postData['start']);
		}
		$query = $this->db->get();
		$data['result'] = $query->result();
		// pr($this->db->last_query());
		// exit;
		$data['total'] = $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total;
		return $data;
	}

	private function _get_datatables_query_assigned_courses($postData)
	{

		ini_set("mysql.trace_mode", "0");
		$this->db->select('SQL_CALC_FOUND_ROWS courses.name as course_name,containers.name as container_name,course_sections.name as section_name', false);
		$this->db->from('course_assigned_to_users');
		$this->db->join('courses', 'courses.id = course_assigned_to_users.course_id');
		$this->db->join('containers', 'containers.id = course_assigned_to_users.container_id');
		$this->db->join('course_sections', 'course_sections.id = course_assigned_to_users.section_id');

		$i = 0;
		// loop searchable columns 
		foreach ($this->column_search as $item) {
			// if datatable send POST for search
			if ($postData['search']['value']) {
				// first loop
				if ($i === 0) {
					// open bracket 
					$this->db->group_start();
					//pr( $item);	
					$this->db->like($item, $postData['search']['value'], 'after');
				} else {
					$this->db->or_like($item, $postData['search']['value'], 'after');
				}
				// last loop
				if (count($this->column_search) - 1 == $i) {
					// close bracket
					$this->db->group_end();
				}
			}
			$i++;
		}
		$where = array(
			'course_assigned_to_users.user_id' => $postData['user_id'],
			'courses.status' => 1,
			'courses.delete_status' => 1,
			'course_sections.status' => 1,
			'course_sections.delete_status' => 1,
			'containers.status' => 1,
			'containers.status_delete' => 1,
		);
		$this->db->where($where);
	}

	public function assigned_students($postData)
	{

		$this->_get_datatables_query_assigned_students($postData);

		if ($postData['length'] != -1) {
			$this->db->limit($postData['length'], $postData['start']);
		}
		$query = $this->db->get();
		$data['result'] = $query->result();
		//pr($this->db->last_query());
		//exit;
		$data['total'] = $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total;
		return $data;
	}

	private function _get_datatables_query_assigned_students($postData)
	{

		ini_set("mysql.trace_mode", "0");
		$this->db->select('SQL_CALC_FOUND_ROWS courses.name as course_name,course_sections.name as section_name,container_groups.name as group_name', false);
		$this->db->from('container_groups_users');
		$this->db->join('courses', 'courses.id = container_groups_users.course_id');
		$this->db->join('course_sections', 'course_sections.id = container_groups_users.section_id');
		$this->db->join('container_groups', 'container_groups.id = container_groups_users.container_group_id');
		$i = 0;
		// loop searchable columns 
		foreach ($this->column_search as $item) {
			// if datatable send POST for search
			if ($postData['search']['value']) {
				// first loop
				if ($i === 0) {
					// open bracket 
					$this->db->group_start();
					//pr( $item);	
					$this->db->like($item, $postData['search']['value'], 'after');
				} else {
					$this->db->or_like($item, $postData['search']['value'], 'after');
				}
				// last loop
				if (count($this->column_search) - 1 == $i) {
					// close bracket
					$this->db->group_end();
				}
			}
			$i++;
		}
		$where = array(
			'container_groups_users.user_id' => $postData['user_id'],
			'courses.status' => 1,
			'courses.delete_status' => 1,
			'course_sections.status' => 1,
			'course_sections.delete_status' => 1,
		);
		$this->db->where($where);
	}

	public function student_completed_courses($postData)
	{

		$this->_get_datatables_query_student_completed_courses($postData);

		if ($postData['length'] != -1) {
			$this->db->limit($postData['length'], $postData['start']);
		}
		$query = $this->db->get();
		$data['result'] = $query->result();
		//pr($this->db->last_query());
		//exit;
		$data['total'] = $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total;
		return $data;
	}

	private function _get_datatables_query_student_completed_courses($postData)
	{

		ini_set("mysql.trace_mode", "0");
		$this->db->select('SQL_CALC_FOUND_ROWS courses.name as course_name,course_sections.name as section_name,container_groups.name as group_name', false);
		$this->db->from('container_groups_users');
		$this->db->join('courses', 'courses.id = container_groups_users.course_id');
		$this->db->join('course_sections', 'course_sections.id = container_groups_users.section_id');
		$this->db->join('container_groups', 'container_groups.id = container_groups_users.container_group_id');
		$i = 0;
		// loop searchable columns 
		foreach ($this->column_search as $item) {
			// if datatable send POST for search
			if ($postData['search']['value']) {
				// first loop
				if ($i === 0) {
					// open bracket 
					$this->db->group_start();
					//pr( $item);	
					$this->db->like($item, $postData['search']['value'], 'after');
				} else {
					$this->db->or_like($item, $postData['search']['value'], 'after');
				}
				// last loop
				if (count($this->column_search) - 1 == $i) {
					// close bracket
					$this->db->group_end();
				}
			}
			$i++;
		}
		$where = array(
			'container_groups_users.user_id' => $postData['user_id'],
			'courses.status' => 1,
			'courses.delete_status' => 1,
			'course_sections.status' => 1,
			'course_sections.delete_status' => 1,
		);
		$this->db->where($where);
		//$this->db->where('container_groups_users.user_id', $postData['user_id']);
	}

	public function student_curent_courses($postData)
	{

		$this->_get_datatables_query_student_curent_courses($postData);

		if ($postData['length'] != -1) {
			$this->db->limit($postData['length'], $postData['start']);
		}
		//$query = $this->db->get('course_assigned_to_users');
		$data['result'] = $this->query->result();
		$data['total'] = $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total;
		return $data;
	}

	private function _get_datatables_query_student_curent_courses($postData)
	{
		//pr($postData['user_id'],"k");
		ini_set("mysql.trace_mode", "0");
		$this->query = $this->db->query('SELECT
		SQL_CALC_FOUND_ROWS c.name as course_name,cs.name as section_name,DATE_FORMAT(caa.created, "%m/%d/%Y") as enrolled_date,COUNT(ca.id) as complete_incomplete,FROM_UNIXTIME(u.last_login) as last_login,cau.is_instructor,gb1.grade as final_term_grade,gb2.grade as mid_term_grade,m.name as last_module_completed,CONCAT_WS(" ", u.first_name, u.middle_name, u.last_name) as user_name,SUM(gb3.grade) as current_grade,COUNT(DISTINCT(mc.module_id)) as total_modules,count(DISTINCT(caa.id)) as completed_lessons,cast(count(DISTINCT(caa.id)) AS unsigned)/cast(COUNT(DISTINCT(mc.module_id)) AS unsigned)*100 as percent
	FROM
		`course_assigned_to_users` AS cau
	JOIN courses AS c
	ON
		cau.course_id = c.id
	JOIN course_sections AS cs
	ON
		cau.section_id = cs.id
	LEFT JOIN gradebook_quiz_assess_grade AS gb1
	ON
		gb1.user_id=cau.user_id AND gb1.course_id=cau.course_id AND gb1.section_id=cau.section_id AND gb1.quiz_assess_id=5
	LEFT JOIN gradebook_quiz_assess_grade AS gb2
	ON
		gb2.user_id=cau.user_id AND gb2.course_id=cau.course_id AND gb2.section_id=cau.section_id AND gb2.quiz_assess_id=4
	LEFT JOIN gradebook_quiz_assess_grade AS gb3
	ON
		gb3.user_id=cau.user_id AND gb3.course_id=cau.course_id AND gb3.section_id=cau.section_id AND gb3.quiz_assess_id IN (1,2)
	JOIN users AS u
	ON
		cau.user_id = u.id
	LEFT JOIN modules as m 
	ON
		m.id = (SELECT module_id FROM `course_activities` as ca WHERE ca.course_id=cau.course_id AND ca.`section_id`=cau.section_id AND ca.user_id=cau.user_id AND lesson_id=0 AND contain_id=0 AND contain_type=0 ORDER BY ca.id DESC LIMIT 1)
	LEFT JOIN course_activities as ca
	ON
		ca.id = (SELECT course_id FROM `course_activities` as ca WHERE ca.course_id=cau.course_id AND ca.`section_id`=cau.section_id AND ca.user_id=cau.user_id AND module_id=0 AND lesson_id=0 AND contain_id=0 AND contain_type=0 ORDER BY ca.id DESC LIMIT 1)
	LEFT JOIN module_contain as mc
	ON
		 mc.course_id=cau.course_id AND mc.quiz_id=0 AND mc.quiz_type=0 AND mc.contain_type=0 AND mc.module_id > 0 AND mc.lesson_id > 0
	LEFT JOIN course_activities as caa
	ON
		caa.course_id=cau.course_id AND caa.section_id=cau.section_id AND caa.user_id=cau.user_id AND caa.module_id > 0 AND caa.lesson_id > 0 AND caa.contain_type=0 AND caa.contain_id=0
	WHERE
		cau.user_id = "' . $postData['user_id'] . '"
	LIMIT 0,10', false);



		//$this->db->where('cau.user_id', $postData['user_id']);
		// $i = 0;
		// // loop searchable columns 
		// foreach ($this->column_search as $item) {
		// 	// if datatable send POST for search
		// 	if ($postData['search']['value']) {
		// 		// first loop
		// 		if ($i === 0) {
		// 			// open bracket 
		// 			$this->db->group_start();
		// 			//pr( $item);	
		// 			$this->db->like($item, $postData['search']['value'], 'after');
		// 		} else {
		// 			$this->db->or_like($item, $postData['search']['value'], 'after');
		// 		}
		// 		// last loop
		// 		if (count($this->column_search) - 1 == $i) {
		// 			// close bracket
		// 			$this->db->group_end();
		// 		}
		// 	}
		// 	$i++;
		// }
		//$this->db->where('container_groups_users.user_id',$postData['user_id']);
	}

	public function dropped_unenrolled_courses($postData)
	{

		$this->_get_datatables_query_dropped_unenrolled_courses($postData);

		if ($postData['length'] != -1) {
			$this->db->limit($postData['length'], $postData['start']);
		}
		//$query = $this->db->get('course_assigned_to_users');
		$data['result'] = $this->query->result();
		$data['total'] = $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total;
		return $data;
	}

	private function _get_datatables_query_dropped_unenrolled_courses($postData)
	{

		ini_set("mysql.trace_mode", "0");
		$this->query = $this->db->query('SELECT
		SQL_CALC_FOUND_ROWS c.name as course_name,cs.name as section_name,DATE_FORMAT(caa.created, "%m/%d/%Y") as enrolled_date,COUNT(ca.id) as complete_incomplete,FROM_UNIXTIME(u.last_login) as last_login,cau.is_instructor,gb1.grade as final_term_grade,gb2.grade as mid_term_grade,m.name as last_module_completed,CONCAT_WS(" ", u.first_name, u.middle_name, u.last_name) as user_name,SUM(gb3.grade) as current_grade,COUNT(DISTINCT(mc.module_id)) as total_modules,count(DISTINCT(caa.id)) as completed_lessons,cast(count(DISTINCT(caa.id)) AS unsigned)/cast(COUNT(DISTINCT(mc.module_id)) AS unsigned)*100 as percent
	FROM
		`course_assigned_to_users` AS cau
	JOIN courses AS c
	ON
		cau.course_id = c.id
	JOIN course_sections AS cs
	ON
		cau.section_id = cs.id
	LEFT JOIN gradebook_quiz_assess_grade AS gb1
	ON
		gb1.user_id=cau.user_id AND gb1.course_id=cau.course_id AND gb1.section_id=cau.section_id AND gb1.quiz_assess_id=5
	LEFT JOIN gradebook_quiz_assess_grade AS gb2
	ON
		gb2.user_id=cau.user_id AND gb2.course_id=cau.course_id AND gb2.section_id=cau.section_id AND gb2.quiz_assess_id=4
	LEFT JOIN gradebook_quiz_assess_grade AS gb3
	ON
		gb3.user_id=cau.user_id AND gb3.course_id=cau.course_id AND gb3.section_id=cau.section_id AND gb3.quiz_assess_id IN (1,2)
	JOIN users AS u
	ON
		cau.user_id = u.id
	LEFT JOIN modules as m 
	ON
		m.id = (SELECT module_id FROM `course_activities` as ca WHERE ca.course_id=cau.course_id AND ca.`section_id`=cau.section_id AND ca.user_id=cau.user_id AND lesson_id=0 AND contain_id=0 AND contain_type=0 ORDER BY ca.id DESC LIMIT 1)
	LEFT JOIN course_activities as ca
	ON
		ca.id = (SELECT course_id FROM `course_activities` as ca WHERE ca.course_id=cau.course_id AND ca.`section_id`=cau.section_id AND ca.user_id=cau.user_id AND module_id=0 AND lesson_id=0 AND contain_id=0 AND contain_type=0 ORDER BY ca.id DESC LIMIT 1)
	LEFT JOIN module_contain as mc
	ON
		 mc.course_id=cau.course_id AND mc.quiz_id=0 AND mc.quiz_type=0 AND mc.contain_type=0 AND mc.module_id > 0 AND mc.lesson_id > 0
	LEFT JOIN course_activities as caa
	ON
		caa.course_id=cau.course_id AND caa.section_id=cau.section_id AND caa.user_id=cau.user_id AND caa.module_id > 0 AND caa.lesson_id > 0 AND caa.contain_type=0 AND caa.contain_id=0
	WHERE
		cau.user_id = ' . $postData['user_id'] . ' AND caa.course_id=0  AND caa.section_id=0
	LIMIT 0,10', false);
	}



	/*
     * Fetch members data from the database
     * @param $_POST filter data based on the posted parameters -- Use in institute user section
     */
	public function getRowsInstituteUsers($postData)
	{

		$this->_get_datatables_query_institute_users($postData);

		if ($postData['length'] != -1) {
			$this->db->limit($postData['length'], $postData['start']);
		}
		$query = $this->db->get();
		$data['result'] = $query->result();
		// pr($this->db->last_query(),"l");
		//exit;
		$data['total'] = $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total;
		return $data;
	}


	/*
     * Perform the SQL queries needed for an server-side processing requested
     * @param $_POST filter data based on the posted parameters
     */
	private function _get_datatables_query_institute_users($postData)
	{
		$this->column_order = array(null, 'user_name', 'users.email', 'course_name', 'course_section_name', 'users.date', 'parent');
		ini_set("mysql.trace_mode", "0");
		$this->db->select('SQL_CALC_FOUND_ROWS users.id, users.email, users.date, users.active, CONCAT_WS(" ", users.first_name, users.middle_name, users.last_name) as user_name, "NA" as parent, roles.id as role_id, GROUP_CONCAT(DISTINCT roles.name SEPARATOR "|") as roles,users.phone,users.institutional_unique_id,users.institutional_unique_id,users.cellphone,users.standing_year,users.dob,', false);

		// $this->db->select('COALESCE(containers.name, \'NA\') as institution');

		if ($postData['order']['0']['column'] == 3) {
			$this->db->select('GROUP_CONCAT(DISTINCT COALESCE(courses.name, \'NA\') SEPARATOR "<br>")  as course_name');
		} else {
			$this->db->select('"..." as course_name');
		}
		//$this->db->select('COALESCE(course_sections.name, \'NA\') as course_section_name');
		if ($postData['order']['0']['column'] == 4) {
			$this->db->select('GROUP_CONCAT(DISTINCT COALESCE(course_sections.name, \'NA\') SEPARATOR "<br>")  as course_section_name');
		} else {
			$this->db->select('"..." as course_section_name');
		}


		$this->db->select('DATE_FORMAT(' . $this->table . '.date, "%m/%d/%Y") as date');
		//$this->db->select('DATE_FORMAT(course_assigned_to_users.created, "%m/%d/%Y") as enrolled_date');

		$this->db->from($this->table);

		$this->db->join('users_roles', $this->table . '.id = users_roles.user_id');
		$this->db->join('containers_users_roles', $this->table . '.id = containers_users_roles.user_id', 'left');

		if (isset($postData['container_selected']) && $postData['container_selected'] > 0) {
			$this->db->join('roles', 'roles.id = containers_users_roles.role_id');
		} else {
			$this->db->join('roles', 'roles.id = users_roles.role_id');
		}

		$this->db->join('containers', 'containers.id = containers_users_roles.container_id', 'left');

		/*$this->db->join('user_parents', $this->table . '.id = user_parents.user_id', 'left');
		$this->db->join('users as parents', 'parents.id = user_parents.parent_id', 'left');*/

		//$this->db->join('container_courses', 'container_courses.container_id = containers.id', 'left');


		/*$this->db->join('course_assigned_to_users', 'course_assigned_to_users.container_id = containers.id AND course_assigned_to_users.user_id = users.id', 'left');
		$this->db->join('courses', 'courses.id = course_assigned_to_users.course_id AND course_assigned_to_users.user_id = users.id', 'left');
		$this->db->join('course_sections', 'course_assigned_to_users.section_id = course_sections.id AND course_assigned_to_users.user_id = users.id', 'left');*/
		$this->db->join('course_assigned_to_users', 'course_assigned_to_users.container_id = containers.id AND course_assigned_to_users.user_id = users.id', 'left');
		$this->db->join('courses', 'courses.id = course_assigned_to_users.course_id AND course_assigned_to_users.user_id = users.id', 'left');
		if ($postData['order']['0']['column'] == 3) {
			// $this->db->join('course_assigned_to_users', 'course_assigned_to_users.container_id = containers.id AND course_assigned_to_users.user_id = users.id', 'left');
			// $this->db->join('courses', 'courses.id = course_assigned_to_users.course_id AND course_assigned_to_users.user_id = users.id', 'left');
		}

		if ($postData['order']['0']['column'] == 4) {
			// $this->db->join('course_assigned_to_users', 'course_assigned_to_users.container_id = containers.id AND course_assigned_to_users.user_id = users.id', 'left');
			// $this->db->join('courses', 'courses.id = course_assigned_to_users.course_id AND course_assigned_to_users.user_id = users.id', 'left');
			$this->db->join('container_courses', 'container_courses.container_id = containers.id AND container_courses.course_id = courses.id', 'inner');
			$this->db->join('course_sections', 'course_sections.id = course_assigned_to_users.section_id AND course_assigned_to_users.user_id = users.id', 'left');
		}

		
		$i = 0;
		// loop searchable columns 
		foreach ($this->column_search as $item) {
			// if datatable send POST for search
			if ($postData['search']['value']) {
				// first loop
				if ($i === 0) {
					// open bracket 
					$this->db->group_start();
					//pr( $item);	
					$this->db->like($item, $postData['search']['value'], 'after');
				} else {
					$this->db->or_like($item, $postData['search']['value'], 'after');
				}
				// last loop
				if (count($this->column_search) - 1 == $i) {
					// close bracket
					$this->db->group_end();
				}
			}
			$i++;
		}


		if (isset($postData['check_users'])) {
			$checked_user = array_filter(explode('|', $postData['check_users']));
			if (count($checked_user) > 0) {
				$this->db->where_in('users.id', $checked_user);
			}
		}

		if (isset($postData['active']) && $postData['active'] < 2) {
			$this->db->where('users.active', $postData['active']);
		}
		if (isset($postData['roles_selected']) && $postData['roles_selected'] > 0) {
			$this->db->where('users_roles.role_id', $postData['roles_selected']);
		}
		// condition for container
		if (isset($postData['container_selected']) && $postData['container_selected'] > 0) {
			$this->db->where('containers_users_roles.container_id', $postData['container_selected']);
		}

		// condition for courses
		if (isset($postData['course_selected']) && $postData['course_selected'] > 0) {
			$this->db->where('course_assigned_to_users.course_id', $postData['course_selected']);
		}
		// condition for sections
		if (isset($postData['section_selected']) && $postData['section_selected'] > 0) {
			$this->db->where('course_assigned_to_users.section_id', $postData['section_selected']);
		}

		// set users 
		if ($this->user_container_id > 0) {
			$this->db->where(['containers_users_roles.container_id' => $this->user_container_id]);
		}

		$this->db->where('users.id >', 1);
		$this->db->where('users.delete_status', 1);
		$this->db->group_by('users.id');

		if (isset($postData['order'])) {
			$this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function get_created_by($source_table, $id)
	{
		$this->db->select($source_table . '.id, CONCAT_WS(" ", users.first_name, users.middle_name, users.last_name) as created_by');
		$this->db->from($source_table);
		$this->db->join('users', $source_table . '.created_by = users.id', 'left');
		$this->db->where([$source_table . '.id' => $id]);
		$created_by = $this->db->get()->row()->created_by;
		return ($created_by) ? $created_by : 'NA';
	}
}

/* End of file Users_modal.php */
/* Location: ./application/models/Users_modal.php */
