<div class="container-fluid">
    <div class="row">
        <div class="col-md-4">
            <div class="info-tile tile-orange">
                <div class="tile-icon"><i class="flaticon-penitentiary"></i></div>
                <div class="tile-heading"><span>Institutions</span></div>
                <div class="tile-body">
                    <span>
                        <?= $institution_count ? $institution_count : '0' ?>
                    </span>
                </div>
                <div class="tile-footer"><span>
                        View All Institutions</span></div>
                <div class="wave"></div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="info-tile tile-slate-blue">
                <div class="tile-icon"><i class="flaticon-avatar"></i></div>
                <div class="tile-heading"><span>Users</span></div>
                <div class="tile-body">
                    <span>
                    <?= $users_count ? $users_count : '0' ?>
                    </span>
                </div>
                <div class="tile-footer"><span>
                        View All Users</span></div>
                <div class="wave"></div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="info-tile tile-ocean-green">
                <div class="tile-icon"><i class="flaticon-book"></i></div>
                <div class="tile-heading"><span>Courses</span></div>
                <div class="tile-body">
                    <span>
                    <?= $courses_count ? $courses_count : '0' ?>
                    </span>
                </div>
                <div class="tile-footer"><span>View All Courses</span></div>
                <div class="wave"></div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="info-tile tile-ocean-green">
                <div class="tile-heading"><span>Quick Access</span></div>
                <div class="tile-content">
                    <ul>
                        <li>
                            <a href="<?php bs('users'); ?>">
                                Add Users
                            </a>
                        </li>
                        <li>
                            <a href="<?php bs('graders'); ?>">
                                Manage Graders
                            </a>
                        </li>
                        <li>
                            <a href="<?php bs('users/menus'); ?>">
                                User Role And Permission
                            </a>
                        </li>
                        <li>
                            <a href="<?php bs('courses'); ?>">
                                New Courses
                            </a>
                        </li>
                        <li>
                            <a href="<?php bs('users/user_roles'); ?>">
                                View Existing Roles
                            </a>
                        </li>
                        <li>
                            <a href="<?php bs('reports/reports'); ?>">
                                Reports
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="info-tile">
                <div class="tile-item">
                    <div class="tile-icon tile-icon-bottom"><i class="flaticon-mind"></i></div>
                    <div class="tile-heading"><span>System Admin</span></div>
                    <div class="tile-body">
                        <span>
                        <?= $system_admin_count ? $system_admin_count : '0' ?>
                        </span>
                    </div>
                    <div class="tile-footer"><span>View All Admin</span></div>
                </div>
                <div class="hr"></div>
                <div class="tile-item">
                    <div class="tile-icon tile-icon-bottom"><i class="flaticon-work-team"></i></div>
                    <div class="tile-heading"><span>Managers</span></div>
                    <div class="tile-body">
                        <span>
                        <?= $manager_count ? $manager_count : '0' ?>
                        </span>
                    </div>
                    <div class="tile-footer"><span>View All Managers</span></div>
                </div>

                <div class="wave"></div>
            </div>
        </div>

        <!-- This is script is for count page visitor  -->


    </div>

    <!--<div data-widget-group="group1">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2>New Users</h2>
                        <div class="panel-ctrls"></div>
                    </div>
                    <div class="panel-body no-padding">
                        <div id="demo">
                            <table cellpadding="0" cellspacing="0" border="0"
                                class="table table-striped table-fixed-header m-n" id="">
                                <thead>
                                    <tr>
                                        <th>User Name</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Email</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php //foreach ($all_users as $user):
                                    ?>
                                    <tr>
                                        <td><?php //echo htmlspecialchars($user->username,ENT_QUOTES,'UTF-8');
                                            ?></td>
                                        <td><?php //echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');
                                            ?></td>
                                        <td><?php //echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');
                                            ?></td>
                                        <td><?php //echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');
                                            ?>
                                        </td>
                                    </tr>
                                    <?php //endforeach;
                                    ?>

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>User Name</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Email</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>-->


</div>