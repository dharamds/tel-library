<div class="static-content-wrapper">
	<div class="static-content">
		<div class="page-content">
			<div class="page-heading">
				<h5>Dashboard</h5>
			</div>
			<ol class="breadcrumb">
				<li class=""><a href="">Home</a></li>
				<li class="active"><span>Dashboard</span></li>
			</ol>
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-4">
						<div class="info-tile tile-orange">
							<div class="tile-icon"><i class="fa fa-users"></i></div>
							<div class="tile-heading"><span>Institutions</span></div>
							<div class="tile-body">
								<span>
									0
								</span>
							</div>
							<div class="tile-footer"><span>
									View All Institutions</span></div>
							<div class="wave"></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="info-tile tile-slate-blue">
							<div class="tile-icon"><i class="fa fa-users"></i></div>
							<div class="tile-heading"><span>Users</span></div>
							<div class="tile-body">
								<span>
									0
								</span>
							</div>
							<div class="tile-footer"><span>
									View All Users</span></div>
							<div class="wave"></div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="info-tile tile-ocean-green">
							<div class="tile-icon"><i class="ti ti-user"></i></div>
							<div class="tile-heading"><span>Courses</span></div>
							<div class="tile-body">
								<span>
									0
								</span>
							</div>
							<div class="tile-footer"><span>View All Courses</span></div>
							<div class="wave"></div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="info-tile tile-ocean-green">
							<div class="tile-heading"><span>Quick Access</span></div>
							<div class="tile-content">
								<ul>
									<li>
										<a href="<?php bs('users'); ?>">
											Add Users
										</a>
									</li>
									<li>
										<a href="<?php bs('graders'); ?>">
											Manage Graders
										</a>
									</li>
									<li>
										<a href="<?php bs('users/menus'); ?>">
											User Role And Permission
										</a>
									</li>
									<li>
										<a href="<?php bs('courses'); ?>">
											New Courses
										</a>
									</li>
									<li>
										<a href="<?php bs('users/user_roles'); ?>">
											View Existing Roles
										</a>
									</li>
									<li>
										<a href="<?php bs('reports/reports'); ?>">
											Reports
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="info-tile tile-ocean-green">
							<div class="tile-item">
								<div class="tile-icon"><i class="ti ti-user"></i></div>
								<div class="tile-heading"><span>System Admin</span></div>
								<div class="tile-body">
									<span>
										0
									</span>
								</div>
								<div class="tile-footer"><span>View All Admin</span></div>
							</div>
							<div class="hr"></div>
							<div class="tile-item">
								<div class="tile-icon"><i class="ti ti-user"></i></div>
								<div class="tile-heading"><span>Managers</span></div>
								<div class="tile-body">
									<span>
										0
									</span>
								</div>
								<div class="tile-footer"><span>View All Managers</span></div>
							</div>

							<div class="wave"></div>
						</div>
					</div>

					<!-- This is script is for count page visitor  -->


				</div>
				<div data-widget-group="group1">
					<div class="row">
						<div class="col-xs-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h2>New Users</h2>
									<div class="panel-ctrls"></div>
									<!-- <a href="<?= bs('Users/print_users') ?>">
									<i class="fa fa-print" style="padding-left: 1%;color: black"></i>
								</a> -->
								</div>
								<div class="panel-body no-padding">
									<div id="demo">
										<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-fixed-header m-n" id="">
											<thead>
												<tr>
													<th>User Name</th>
													<th>First Name</th>
													<th>Last Name</th>
													<th>Email</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach ($all_users as $user) : ?>
												<tr>
													<td><?php echo htmlspecialchars($user->username, ENT_QUOTES, 'UTF-8'); ?></td>
													<td><?php echo htmlspecialchars($user->first_name, ENT_QUOTES, 'UTF-8'); ?></td>
													<td><?php echo htmlspecialchars($user->last_name, ENT_QUOTES, 'UTF-8'); ?></td>
													<td><?php echo htmlspecialchars($user->email, ENT_QUOTES, 'UTF-8'); ?>
													</td>
												</tr>
												<?php endforeach; ?>

											</tbody>
											<tfoot>
												<tr>
													<th>User Name</th>
													<th>First Name</th>
													<th>Last Name</th>
													<th>Email</th>
												</tr>
											</tfoot>
										</table>
									</div>
								</div>
							</div>




						</div> <!-- .container-fluid -->
					</div> <!-- #page-content -->
				</div>
			</div>
		</div>
	</div>