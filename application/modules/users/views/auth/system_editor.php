<div class="static-content-wrapper">
	<div class="static-content">
		<div class="page-content">
			<div class="page-heading">
				<h5>Dashboard</h5>
			</div>
			<ol class="breadcrumb">
				<li class=""><a href="">Home</a></li>
				<li class="active"><span>Dashboard</span></li>
			</ol>
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-4">
						<div class="info-tile tile-orange">
							<div class="tile-icon"><i class="fa fa-users"></i></div>
							<div class="tile-heading"><span>Institutions</span></div>
							<div class="tile-body">
								<span>
									0
								</span>
							</div>
							<div class="tile-footer"><span>
									View All Institutions</span></div>
							<div class="wave"></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="info-tile tile-slate-blue">
							<div class="tile-icon"><i class="fa fa-users"></i></div>
							<div class="tile-heading"><span>Users</span></div>
							<div class="tile-body">
								<span>
									0
								</span>
							</div>
							<div class="tile-footer"><span>
									View All Users</span></div>
							<div class="wave"></div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="info-tile tile-ocean-green">
							<div class="tile-icon"><i class="ti ti-user"></i></div>
							<div class="tile-heading"><span>Courses</span></div>
							<div class="tile-body">
								<span>
									0
								</span>
							</div>
							<div class="tile-footer"><span>View All Courses</span></div>
							<div class="wave"></div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="info-tile tile-ocean-green">
							<div class="tile-heading"><span>Quick Access</span></div>
							<div class="tile-content">
								<ul>
									<li>
										<a href="<?php bs('users'); ?>">
											Add Users
										</a>
									</li>
									<li>
										<a href="<?php bs('graders'); ?>">
											Manage Graders
										</a>
									</li>
									<li>
										<a href="<?php bs('users/menus'); ?>">
											User Role And Permission
										</a>
									</li>
									<li>
										<a href="<?php bs('courses'); ?>">
											New Courses
										</a>
									</li>
									<li>
										<a href="<?php bs('users/user_roles'); ?>">
											View Existing Roles
										</a>
									</li>
									<li>
										<a href="<?php bs('reports/reports'); ?>">
											Reports
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="info-tile tile-ocean-green">
							<div class="tile-item">
								<div class="tile-icon"><i class="ti ti-user"></i></div>
								<div class="tile-heading"><span>System Admin</span></div>
								<div class="tile-body">
									<span>
										0
									</span>
								</div>
								<div class="tile-footer"><span>View All Admin</span></div>
							</div>
							<div class="hr"></div>
							<div class="tile-item">
								<div class="tile-icon"><i class="ti ti-user"></i></div>
								<div class="tile-heading"><span>Managers</span></div>
								<div class="tile-body">
									<span>
										0
									</span>
								</div>
								<div class="tile-footer"><span>View All Managers</span></div>
							</div>

							<div class="wave"></div>
						</div>
					</div>

					<!-- This is script is for count page visitor  -->


				</div>

			</div>
		</div>
	</div>