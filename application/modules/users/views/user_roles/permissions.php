
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <!--                        <div class="alert alert-info clearfix">
                                                    <h3 class="mt0">Draggable Panels</h3>
                                                    <p>Wijets is a jQuery plugin that provides advanced panel options which can be configured all with a single line of code - <code>$.wijets.make();</code>. All other settings are configured directly with data-attributes!</p>
                                                </div>-->
                    </div>
                </div>
                <div class="row" data-widget-group="group-demo">
                    <div class="col-md-12">
                        <div class="panel panel-profile panel-grid">
                            <div class="panel-heading">
                                <div class="panel-ctrls"></div>
                                <h2>Add Permissions</h2>
                            </div>
                            <div class="panel-editbox" data-widget-controls=""></div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6 p-0">
                                        <form id="permi" action="<?= base_url('users/user_roles/permissions') ?>" method="post" role="form">
                                            <div class="form-group col-md-12">
                                                <label class="control-label col-md-12 p-0 text-left">Permission Name</label>
                                                <div class="col-md-8 p-0">
                                                    <input type="text" name="perm" id="perm" class="form-control" placeholder="Permission Name">
                                                </div>
                                                <div class="col-md-4 p-0">
                                                    <button type="submit" class="btn btn-primary btn-md add"><i class="fa fa-plus"></i> Add</button>   
                                                </div>
                                            </div>
                                            <br>
                                            <!-- <span id="err_msg" style="color: red;margin-left:22%"></span> -->
                                        </form>
                                    </div>
                                </div>
                                <div class="row" style="padding-top: 3%">
                                    <div class="col-md-12">
                                        <table id="" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <colgroup>
                                                <col width="10%" />
                                                <col width="70%" />
                                                <col width="10%" />
                                                <col width="10%" />
                                            </colgroup>
                                            <thead>
                                                <tr>
                                                    <th class="text-center">Sr.</th>
                                                    <th class="text-center">Permission Name</th>
                                                    <th class="text-center">Edit</th>
                                                    <th class="text-center">Delete</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $i = 1;
                                                foreach ($perm as $key => $value):
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $i; ?></td>
                                                        <td><?php echo $value->perm_name ?></td>
                                                        <td>
                                                            <a class="btn btn-primary btn-sm update" edit="<?= $value->perm_id ?>" data-toggle="modal" data-target="#myModal"><i class="ti ti-pencil"></i></a>
                                                        </td>
                                                        <td>
                                                            <button class="btn btn-danger btn-sm" id_del="<?= $value->perm_id ?>" id="delete"><i class="fa fa-trash"></i></button>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                                <?php
                                                $i++;
                                            endforeach
                                            ?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- .container-fluid -->
        
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Update Permission</h4>
            </div>
            <form class="form-inline" id="editpermForm" action="<?= base_url('users/user_roles/update_perm') ?>" method="post" role="form">
                <div class="modal-body">
                    <div class="form-group">
                        <label> <font color="#8bc34a">Permission Name </font>&nbsp;</label>
                        <input type="text" name="perm" id="editperm" class="form-control" placeholder="Permission Name" required>
                        <input type="hidden" id="edit" name="edit">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>

    $(document).ready(function () {

        jQuery.validator.addMethod("noSpace", function (value, element, param) {
            return value.match(/^(?=.*\S).+$/);
        }, "No space please and don't leave it empty");

        $('#permi').validate({// initialize the plugin
            rules: {
                perm: {
                    required: true,
                    noSpace: true
                }
            },
            messages: {
                perm: {
                    required: "Permission can not be blank"
                }
            }

        });



        $('#editpermForm').validate({// initialize the plugin
            rules: {
                perm: {
                    required: true,
                    noSpace: true
                }
            },
            messages: {
                perm: {
                    required: "Permission can not be blank"
                }
            }

        });

        //This script will delete record
        $("body").on('click', '#delete', function (event)
        {
            event.preventDefault();
            var result = confirm("Want to delete?");
            if (result) {
                /* Act on the event */
                var del = $(this).attr('id_del');
                //ajax request
                $.ajax({

                    url: '<?php bs('users/user_roles/delete_perm') ?>/' + del,
                    success: function (result)
                    {
                        location.reload();
                    }
                });
            }
        });
        $("body").on('click', '.update', function (event)
        {
            event.preventDefault();
            // body...
            var id = $(this).attr('edit');
            $.ajax({

                url: "<?php bs('users/user_roles/get_perm') ?>/" + id,
                success: function (success)
                {
                    var obj = $.parseJSON(success);
                    $("#editperm").val(obj.perm_name);
                    $("#edit").val(obj.perm_id);
                }

            })
        })

    });

</script>