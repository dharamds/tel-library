<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default  panel-grid" data-widget='{"draggable": "false"}'>
                <div class="panel-heading brd-0"></div>
                <div class="panel-body">
                    <?php
                    $attributes = array('id' => 'editRoleForm');

                    echo form_open(current_url(), $attributes);
                    ?>
                    <div class="list-item icon-left">
                        <div class="col-sm-12 list-item-head">
                            <span class="flaticon-user-1 ficon"></span>
                            <?php echo lang('edit_role_subheading'); ?>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?php echo lang('edit_role_name_label', 'role_name', array('class' => 'control-label')); ?>
                                    <?php echo form_input($role_name); ?>
                                    <span id="err_msg" style="color:red"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?php echo lang('edit_role_desc_label', 'description', array('class' => 'control-label')); ?> <br />
                                    <?php echo form_input($role_description); ?>
                                </div>
                            </div>
                        </div>
                        <div class="hr-line mt-4 mb-4 col-md-12"></div>
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <label class="col-sm-12 p-0 control-label">Permissions</label>
                                <div class="mt-2">
                                    <?php 
                                    ?>
                                    <?php foreach ($privileges as $privilege) : ?>
                                        <label class="checkbox-tel ml-4">
                                            <?php
                                            $pID = $privilege->perm_id;
                                            $checked = null;
                                            $item = null;
                                            foreach ($crtPrivilege as $pri) {
                                                if ($pID == $pri->perm_id) {
                                                    $checked = ' checked="checked"';
                                                    break;
                                                }
                                            }
                                            ?>
                                            <input type="checkbox" name="privlg[]" value="<?php echo $privilege->perm_id; ?>" <?php echo $checked; ?> class="mr-1 t2">
                                            <?php echo htmlspecialchars($privilege->perm_name, ENT_QUOTES, 'UTF-8'); ?>
                                        </label>

                                    <?php endforeach ?>
                                    <?php 
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="hr-line mt-4 mb-4 col-md-12"></div>
                        <div class="row mt-5">
                            <div class="col-md-12">

                                <h4 class="block-heading">Institutions/Sites</h4>
                                <div class="row">
                                    <?php foreach ($containers as $container) : ?>
                                        <div class="col-md-3">
                                            <label>
                                                <?php
                                                $pID = $container->id;
                                                $checked = null;
                                                $item = null;
                                                foreach ($currentContainers as $pri) {
                                                    if ($pID == $pri->container_id) {
                                                        $checked = ' checked="checked"';
                                                        break;
                                                    }
                                                }
                                                ?>
                                                <input type="checkbox" name="containers[]" value="<?php echo $container->id; ?>" <?php echo $checked; ?>>
                                                <?php echo htmlspecialchars($container->name, ENT_QUOTES, 'UTF-8'); ?>
                                            </label>
                                        </div>
                                    <?php endforeach ?>
                                </div>

                            </div>
                        </div>
                        <div class="hr-line mt-4 mb-4 col-md-12"></div>
                        <div class="row mt-5">
                            <div class="col-sm-12">
                                <?php echo form_submit('submit', lang('edit_role_submit_btn'), 'class="btn btn-success"'); ?>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>

    <!-- #page-content -->
    <script>
        $('#editRoleForm').validate({ // initialize the plugin
            rules: {
                role_name: {
                    required: true
                },
                role_description: {
                    required: true
                }
            },
            messages: {
                role_name: {
                    required: "role name can not be blank"
                },
                role_description: {
                    required: "role description can not be blank"
                }

            }

        });


        $(document).ready(function() {
            //This script is to check email validity
            $("body").on('change', '#role_name', function() {
                var role_name = $("#role_name").val();

                if (role_name.length === 0) {
                    $('#err_msg').text('role Name is required');
                    return false;
                }

                $.ajax({
                    url: '<?= base_url("users/User_roles/check_role_name") ?>',
                    method: 'POST',
                    dataType: 'TEXT',
                    data: {
                        role_name: role_name
                    },
                    success: function(result) {
                        var msg = result.split("::");

                        if (msg[0] == "ok") {
                            $("#err_msg").fadeIn();
                            $("#err_msg").text("role name already taken.");
                        } else {
                            console.log('Success');
                            $("#err_msg").fadeIn();
                            $("#err_msg").html("<span class='fa fa-check-circle text-success'> Success</span>");
                            $("#err_msg").delay(3000).fadeOut();
                        }
                    },
                    error: function(result) {
                        // body...
                        console.log(result);
                    }
                })
            });
        });
    </script>