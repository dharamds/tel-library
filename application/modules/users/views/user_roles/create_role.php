            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default panel-grid">
                            <div class="panel-heading brd-0 m-0 pt-0"></div>
                            <div class="panel-body pt-0">
<!--                                 <p>The wizard has jQuery Validation plugin support built-in. All you need to do is turn on the <code>validate: true</code> option on and you instantly have a wizard with form validation!</p> -->
                                <?php echo form_open("users/User_roles/create_role", array('id' => 'wizard', 'class' => 'form-horizontal')); ?>
                                <fieldset title="Step 1">
                                    <legend>Create Role</legend>
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label for="fieldname" class="col-md-3 control-label">Role Name</label>
                                            <div class="col-md-6">
                                                <?php echo form_input($role_name); ?>
                                                <span id="err_msg" style="color:red"></span>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-12">
                                            <label for="fieldemail" class="col-md-3 control-label">Desc</label>
                                            <div class="col-md-6">
                                                <?php echo form_input($description); ?>
                                            </div>
                                        </div>
                                    </div>


                                </fieldset>
                                <fieldset title="Step 2">
                                    <legend>Assign Privileges</legend>
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <div>
                                                <input type="checkbox" id="checkAll" class="mr-1" > Check All
                                                <hr />

                                            </div>
                                            <?php foreach ($perm as $key => $value): ?>
                                                <div class="">
                                                    <label>
                                                        <input type="checkbox" class="checkItem mr-1" name="privilege[]" value="<?php echo $value->perm_id ?>"><?php echo $value->perm_name ?>
                                                    </label>
                                                </div>
                                            <?php endforeach ?>
                                        </div>
                                    </div>
                                </fieldset>
                                <input type="submit" class="finish btn-success btn" value="Submit" />
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<style>
    .help-block{
        color:red;
    }
</style>

<!-- #page-content -->
<script>
    $(document).ready(function () {
        
         jQuery.validator.addMethod("noSpace", function (value, element, param) {
            return value.match(/^(?=.*\S).+$/);
        }, "No space please and don't leave it empty");

        $('#wizard').validate({// initialize the plugin
            rules: {
                role_name: {
                    required: true,
                    noSpace: true
                },
                 description: {
                    required: true,
                },
                'privilege[]':{
                    required:true
                }
                
            },
            messages: {
                role_name: {
                    required: "Role name can not be blank"
                },
                 description: {
                    required: "Role name can not be blank"
                },
                 'privilege[]':{
                    required:"Please select atleast one previlege"
                }
                
            }

        });
       
        
        
        
        
        
        
        
        
        
        

        $('#checkAll').click(function () {
            $(':checkbox.checkItem').prop('checked', this.checked);
        });

//        $('#role_name').keyup(function (e) {
//            if (e.which === 32) {
//                alert('No space are allowed in roll name');
//                var str = $(this).val();
//                str = str.replace(/\s/g, '');
//                $(this).val(str);
//            }
//        }).blur(function () {
//            var str = $(this).val();
//            str = str.replace(/\s/g, '');
//            $(this).val(str);
//        });
        
       

        //This script is to check email validity
        $("body").on('change', '#role_name', function (e)
        {
            var role_name = $("#role_name").val();

//            if (role_name.length === 0)
//            {
//                $('#err_msg').text('Role Name is required');
//                return false;
//            }


            $.ajax({
                url: '<?= base_url("users/User_roles/check_role_name") ?>',
                method: 'POST',
                dataType: 'TEXT',
                data: {role_name: role_name},
                success: function (result)
                {
                    var msg = result.split("::");

                    if (msg[0] == "ok")
                    {
                        $("#err_msg").fadeIn();
                        $("#err_msg").text("Role name already taken.");
                    } else
                    {
                        console.log('Success');
                        $("#err_msg").fadeIn();
                        $("#err_msg").html("<span class='fa fa-check-circle text-success'> Success</span>");
                        $("#err_msg").delay(3000).fadeOut();
                    }
                },
                error: function (result)
                {
                    // body...
                    console.log(result);
                }
            })
        });
    });

</script>