
      <div class="container-fluid">
         <div data-widget-role="role1">
            <div class="row">
               <div class="col-md-12">
                  <div class="panel panel-default panel-grid">
                     <div class="panel-heading">
                        <div class="flex-row">
                           <div class="flex-col">
                              <a class="btn btn-primary" href="<?=base_url('users/user_roles/create_role') ?>">Add New</a> 
                           </div>
                           <div class="flex-col">
                              <div class="panel-ctrls"></div>
                           </div>
                        </div>
                     </div>
                     <div class="panel-body no-padding">
                        <?php 
                           //If user is admin
                           if ($this->ion_auth->get_user_role() <= 2) 
                           {
                           ?>
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                           <colgroup>
                              <col width="5%" />
                              <col width="10%" />
                              <col width="35%" />
                              <col width="55%" />
                           </colgroup>
                           <thead>
                              <tr>
                                 <th>Sr.</th>
                                 <th>Action</th>
                                 <th>Roles</th>
                                 <th>Description</th>
                              </tr>
                           </thead>
                           <tbody>
                              <?php $count = 0; foreach ($roles as $role): $count++;?>
                              <tr>
                              <td>
                              <?php echo htmlspecialchars($count,ENT_QUOTES,'UTF-8');?>
                                 </td>
                                 <td>
                                     <a data-toggle="tooltip" data-placement="top" title="edit" href="<?= base_url('users/User_roles/edit_role')?>/<?= $role->id ?>" class="btn btn-primary btn-sm">
                                    <i class="ti ti-pencil"></i>
                                    </a>
                                    <a data-toggle="tooltip" data-placement="top" title="delete" href="<?= base_url('users/User_roles/delete_role')?>/<?= $role->id ?>" onclick="return confirm('Are you sure you want to delete?');" class="btn btn-danger btn-sm"> 
                                    <i class="ti ti-trash"></i> 
                                    </a> 
                                 </td>
                                 <td>
                                    <?php echo htmlspecialchars($role->name,ENT_QUOTES,'UTF-8');?>
                                 </td>
                                 <td>
                                    <?php echo htmlspecialchars($role->description,ENT_QUOTES,'UTF-8');?>
                                 </td>
                                 
                                 
                              </tr>
                              <?php endforeach;?>
                           </tbody>
                        </table>
                        <?php
                           }
                           
                           if ($this->ion_auth->get_user_role() > 2) {
                           ?>
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                           <thead>
                              <tr>
                                 <th>Role/role</th>
                                 <th>Description</th>
                              </tr>
                           </thead>
                           <tbody>
                              <?php foreach ($roles as $role):?>
                              <tr>
                                 <td>
                                    <?php echo htmlspecialchars($role->name,ENT_QUOTES,'UTF-8');?>
                                 </td>
                                 <td>
                                    <?php echo htmlspecialchars($role->description,ENT_QUOTES,'UTF-8');?>
                                 </td>
                              </tr>
                              <?php endforeach;?>
                           </tbody>
                        </table>
                        <?php
                           }
                           ?>	
                     </div>
                     <div class="panel-footer"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- .container-fluid -->


<script>
$(document).ready(function(){
  $('[data-toggle="editRole"]').tooltip();   
  $('[data-toggle="deleteRole"]').tooltip();   
});
</script>