
<div class="container-fluid">
   <div data-widget-role="role1">
      <div class="row">
         <div class="col-md-12">
            <div class="panel panel-default panel-grid">
               <div class="panel-heading brd-0"></div>
               <div class="panel-body p-0 pl-4 pr-4">
                  <div class="user-menu-list flex-row">
                    <?php echo $menu; ?>  
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
<script type="text/javascript">
$('.delete_confirm ').click(function(e){
   e.preventDefault();
   var url = $(this).attr('href');
   $.confirm({
    title: 'Confirm!',
    content: 'Are you sure want to delete?',
    buttons: {
        confirm: function () {
         location.href = url;
        },
        cancel: function () {
           //return false;
        }
    }
});
   
});
  
</script>