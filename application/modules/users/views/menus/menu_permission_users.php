<div class="col-md-10 col-md-offset-2">
                             <?php foreach ($currentUsers as $key => $value){
                              $checked             = '';
                              $add_perchecked      = '';
                              $edit_perchecked     = '';
                              $delete_perchecked   = '';
                              $view_perchecked     = '';
                              $list_perchecked     = '';
                              $checked = 'checked';
          
                                  if($value->add_per){ $add_perchecked        = "checked"; }
                                  if($value->edit_per){ $edit_perchecked      = "checked"; }
                                  if($value->delete_per){ $delete_perchecked  = "checked"; }
                                  if($value->view_per){ $view_perchecked      = "checked"; }
                                  if($value->list_per){ $list_perchecked      = "checked"; }
                               ?>
                              <div class="checkbox pull-left">
                                 <label>
                                 <input type="checkbox" <?php echo $checked; ?> name="users[]" class="user_status" id="<?php echo $value->id;?>" value="<?php echo $value->id ?>"><?php echo $value->user_name; ?> <br> <?php echo $value->email; ?>
                                 </label>
                              </div>
                              <div class="pull-right">
                                 <label>
                                 <input type="checkbox" class="user_options_<?php echo $value->id;?>" <?php echo $add_perchecked; ?> name="adds[<?php echo $value->id ?>]" value="<?php echo $value->id; ?>"> Add 
                                 </label>
                                 <label>
                                 <input type="checkbox" class="user_options_<?php echo $value->id;?>" <?php echo $edit_perchecked; ?> name="edits[<?php echo $value->id ?>]" value="<?php echo $value->id; ?>"> Edit 
                                 </label>
                                 <label>
                                 <input type="checkbox" class="user_options_<?php echo $value->id;?>" <?php echo $delete_perchecked; ?> name="deletes[<?php echo $value->id ?>]" value="<?php echo $value->id; ?>"> Delete 
                                 </label>
                                 <label>
                                 <input type="checkbox" class="user_options_<?php echo $value->id;?>" <?php echo $view_perchecked; ?> name="views[<?php echo $value->id ?>]" value="<?php echo $value->id; ?>"> View 
                                 </label>
                                 <input type="checkbox" class="user_options_<?php echo $value->id;?>" <?php echo $list_perchecked; ?> name="list[<?php echo $value->id ?>]" value="<?php echo $value->id; ?>"> List 
                                 </label>
                              </div>
                              <hr><div style="clear:both;"></div>
                              <?php } ?>
                           </div>
                           <?php echo $links; ?>
                           <script>
   $(document).ready(function() {
      $('.user_status').change(function(){
         $("input:checkbox.user_options_"+ $(this).attr('id')).prop('checked', $(this).prop("checked"));
      });  

      $(document).on("click", ".pagination li a", function(event){
            event.preventDefault();
                  $.get($(this).attr("href"), function( data ) {
                     $( "#menu_permission_users" ).html( data );
                  });
            });

   });
     
</script>