      <div class="container-fluid">
         <div class="row">
            <div class="col-sm-12">
               <div class="panel panel-info panel-grid" data-widget='{"draggable": "false"}'>
                  <div class="panel-heading brd-0"></div>
                  <div class="panel-body pt-0">
                     <div class="wizard-panel">
                     <?php echo form_open(current_url(), array('id'=>'wizard','class'=>'form-horizontal'));?>
                     <fieldset title="Step 1">
                        <legend class="block-heading mb-4">Save Menu</legend>
                        <div class="col-md-6">
                        <div class="form-group">
                           <label for="fieldname" class="col-md-12 control-label">Parent Menu </label>
                           <div class="col-md-12">
                              <select name="parent_id" id="parentMenu" class="populate form-control">
                                 <option value="0" >-- Select --</option>
                                <?php echo $menus; ?>
                              </select>
                              <span id="err_msg" style="color:red"></span>
                           </div>
                        </div>

                        <div class="form-group">
                           <label for="fieldname" class="col-md-12 control-label">Menu Name</label>
                           <div class="col-md-12">
                              <?php echo form_input($name);?>
                              <span id="err_msg_name" style="color:red"></span>
                           </div>
                        </div>

                        <div class="form-group">
                           <label for="fieldemail" class="col-md-12 control-label">Description</label>
                           <div class="col-md-12">
                              <?php echo form_input($description);?>
                           </div>
                        </div>
                          <div class="form-group">
                           <label for="fieldemail" class="col-md-12 control-label">Icon</label>
                           <div class="col-md-12">
                              <?php echo form_input($icon);?>
                           </div>
                        </div>
                          <div class="form-group">
                           <label for="fieldemail" class="col-md-12 control-label">Slug</label>
                           <div class="col-md-12">
                              <?php echo form_input($slug);?>
                           </div>
                        </div>
                          <div class="form-group">
                           <label for="fieldemail" class="col-md-12 control-label">Order</label>
                           <div class="col-md-12">
                              <?php echo form_input($number);?>
                           </div>
                        </div>
                        </div>
                     <div class="col-md-6">
                         <label for="fieldemail" class="col-md-12 control-label">Assign Roles</label>
                        <div class="form-group">
                           <div class="col-md-12">
                              <?php foreach ($roles as $key => $value): 
                                $checked                    = '';
                                $type                       = '';
                                $role_add_perchecked        = '';
                                $role_edit_perchecked       = '';
                                $role_delete_perchecked     = '';
                                $role_view_perchecked       = '';
                                $role_list_perchecked       = '';

                                 foreach ($currentRoles as $key1 => $crole) {
                                 if( $crole->role_id == $value->id){ 
                                    $checked = 'checked';
                                    if($crole->add_per){       $role_add_perchecked       = "checked"; }
                                    if($crole->edit_per){      $role_edit_perchecked      = "checked"; }
                                    if($crole->delete_per){    $role_delete_perchecked    = "checked"; }
                                    if($crole->view_per){      $role_view_perchecked      = "checked"; }
                                    if($crole->list_per){      $role_list_perchecked      = "checked"; }
                                   }
                                }
        
                  if($value->id == 1){ $checked = 'checked'; $type = 'hidden';  } 
                              ?>
                              <div class="" <?php if($type){ echo 'style="display:none;"';} ?>>

                                <div class="row m-0">
                                  <div class="col-md-12">
                                    <div class="checkbox-heading">
                                      <label class="checkbox-tel">
                                       <input type="checkbox" <?php echo $checked; ?> name="roles[]" value="<?php echo $value->id;?>" class="role_status mr-2 t2" id="<?php echo $value->id;?>">
                                       <?php echo $value->name ?>
                                       </label>
                                    </div>
                                    <div class="checkbox-content">
                                       <label class="checkbox-tel mr-3">
                                       <input type="checkbox" class="role_options_<?php echo $value->id;?>  mr-2 t2" <?php echo $role_add_perchecked; ?> name="role_adds[<?php echo $value->id ?>]" value="<?php echo $value->id; ?>" > Add 
                                       </label>
                                       <label class="checkbox-tel mr-3">
                                       <input type="checkbox" class="role_options_<?php echo $value->id;?> mr-2 t2" <?php echo $role_edit_perchecked; ?> name="role_edits[<?php echo $value->id ?>]" value="<?php echo $value->id; ?>"> Edit 
                                       </label>
                                       <label class="checkbox-tel mr-3">
                                       <input type="checkbox" class="role_options_<?php echo $value->id;?> mr-2 t2" <?php echo $role_delete_perchecked; ?> name="role_deletes[<?php echo $value->id ?>]" value="<?php echo $value->id; ?>"> Delete 
                                       </label>
                                       <label class="checkbox-tel mr-3">
                                       <input type="checkbox" class="role_options_<?php echo $value->id;?> mr-2 t2" <?php echo $role_view_perchecked; ?> name="role_views[<?php echo $value->id ?>]" value="<?php echo $value->id; ?>"> View 
                                       </label>
                                       <label class="checkbox-tel mr-3">
                                       <input type="checkbox" class="role_options_<?php echo $value->id;?> mr-2 t2" <?php echo $role_list_perchecked; ?> name="role_lists[<?php echo $value->id ?>]" value="<?php echo $value->id; ?>"> List 
                                       </label>
                                    </div>
                                  </div>
                                </div>

                                 
                              <!-- <hr><div style="clear:both;"></div> -->
                              </div>
                              <?php endforeach ?>
                           </div>
                        </div>
                        </div>
                     </fieldset>

                     <fieldset title="Step 2">
                        <legend>Assigned Users</legend>
                        <div class="col-md-4">
                        
                        <input class="combobox form-control" id="search_users" name="search_users">
                          
                        </div>
                        <div class="form-group col-md-8" id="menu_permission_users">
                           
                        </div>
                     </fieldset>

                     <input type="submit" class="finish btn-success btn" value="Save" />
                     </form>
                  </div>
                  </div>
               </div>
            </div>
         </div>

<!-- #page-content -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>  

<script>
   $(document).ready(function() {
      $('#search_users').typeahead({
         source: function(query, result)
         {
            $.ajax({
            url:"<?php echo base_url('/users/getUsers'); ?>",
            method:"POST",
            data:{query:query},
            dataType:"json",
            success:function(data)
            {
               result($.map(data, function(item){
               return item;
            }));
            }
            })
         },
         updater:function (item) {
            console.log("<?php echo base_url('/users/menus/set_menu_permission_users/'.$menu_id); ?>/" + item.id);
            //do your stuff.
               $.get( "<?php echo base_url('/users/menus/set_menu_permission_users/'.$menu_id); ?>/" + item.id, function( data ) {
                  $('#search_users').val('');
                  load_menu_permission_users();
               });
         return item;
         }
        });
        
      load_menu_permission_users();

      function load_menu_permission_users(url = "<?php echo base_url('/users/menus/load_menu_permission_users/'.$menu_id); ?>/0"){ 
         $.get(url, function( data ) {
         $( "#menu_permission_users" ).html( data );
         });
      }

    //$("#parentMenu").select2({width: '100%'});

   $('.role_status').change(function(){
      $("input:checkbox.role_options_"+ $(this).attr('id')).prop('checked', $(this).prop("checked"));
   });

   $('.user_status').change(function(){
      $("input:checkbox.user_options_"+ $(this).attr('id')).prop('checked', $(this).prop("checked"));
   });

      //This script is to check email validity
      $("#wizard").submit( function()
      {  
         if ($("#name").val().length === 0) 
            {
            $('#err_msg_name').text('Name is required');
            return false;
            }   
      });   
      
   //////////////////////////////////////search users

      
   
   });
 
     
</script>