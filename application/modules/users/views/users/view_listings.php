            <div class="container-fluid">
                <div class="row">                    
                    <div class="col-md-12">
                        <div class="filter-container flex-row">
                            
                            
                            

                            
                            <div class="flex-col-sm-2">
                                <select id="select_status" class="selected_condition form-control">
                                    <option value="2"> --Select Status--</option>
                                    <option value="1">  Active</option>
                                    <option  value="0"> Inactive</option>
                                </select>
                            </div>
                            <div class="flex-col-sm-1">
                                <a class="btn btn-danger pt-2" id="reload_form" href="#"><i class="flaticon-close-1 f21 fw100"></i></a> 
                            </div>
                            <div class="flex-col-sm-1">
                            <a class="btn btn-warning send_email" href="#"><!-- <i class="flaticon-ring mr-2" aria-hidden="true"></i> -->Notify To</a>
                            </div>
                        </div>
                    </div>        
                </div>
                <div data-widget-role="role1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default panel-grid">
                                <div class="panel-heading">
                                    <div class="flex-row">
                                    <form method="post" action="<?php echo base_url('users/download_csv')?>" id="export_user_form">
                                     <input type="hidden" name="check_users" id="check_users">
                                        <div class="flex-col">
                                            <?php if ($this->ion_auth->is_admin() || $add_permission): ?>
                                                <a class="btn btn-primary" href="<?= base_url('users/create_user') ?>">New</a> 

                                                <button type="button" class="btn btn-primary" id="bulk_export" ><!-- <i class="flaticon-csv mr-2" aria-hidden="true"></i> -->Download CSV of selected Users</button>
                                                <?php endif; ?>
                                               

                                               
                                        </div>
                                        </form>
                                        <div class="flex-col-auto">
                                            <div class="panel-ctrls" id="manage_user_control"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body no-padding p-0">
                                    <table id="memListTable" class="table table-bordered table-striped table-hover" cellspacing="0">                              
                                        <thead>
                                            <tr>
                                                <th>
                                                    <label class="checkbox-tel"><input type="checkbox" class="select_all"></label>
                                                </th>
                                                <th style="min-width: 165px;">  Action</th>
                                                <th style="min-width: 90px">    Status</th>
                                                <th style="min-width: 120px;">  Name</th>
                                                <th style="min-width: 170px;">  Email</th>
                                                <th style="min-width: 150px">   Institution</th>
                                                <th style="min-width: 90px">    Created</th> 
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="panel-footer"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- .container-fluid -->
    
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><i class="fa fa-share-square" aria-hidden="true"></i> Send Email</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="<?php bs() ?>users/email">
                        <div class="form-role">
                            <label>Name</label>
                            <input type="text" class="form-control" value="<?php echo $this->config->item('site_title', 'ion_auth') ?>" name="name" placeholder="Name" required>
                        </div>
                        <div class="form-role">
                            <label>From</label>
                            <input type="email" class="form-control" value="<?php echo $this->config->item('admin_email', 'ion_auth') ?>" name="from_email" placeholder="From" required>
                        </div>
                        <div class="form-role">
                            <label>To</label>
                            <input type="email" id="to" class="form-control" value="" name="to" placeholder="To" required>
                        </div>
                        <div class="form-role">
                            <label>Subject</label>
                            <input type="text" class="form-control" name="subject" value="" placeholder="Subject" required>
                        </div>
                        <div class="form-role">
                            <label>Message</label>
                            <textarea name="msg" class="form-control" rows="10" id="summernote" cols="10" placeholder="Write You Message" required></textarea>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-paper-plane" aria-hidden="true"></i> Send</button>
                </div>
                </form>   
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
           

            $("#bulk_export").click(function () {

                var selected_user_id="";

               $('input[name="user_id[]"]:checked').each(function() {
                   console.log(this.value);

                   selected_user_id=this.value+'|'+selected_user_id;                              

                });

                if(selected_user_id!="")
                {
                    $('#check_users').val(selected_user_id);
                }
                else
                {
                    CommanJS.getDisplayMessgae(400, 'Please select atleast one user.')
                    return false;
                }

                $('#export_user_form').submit();

            });


             $(".delete_selected").click(function () {

                var selected_user_id="";

               $('input[name="user_id[]"]:checked').each(function() {
                   console.log(this.value);

                   selected_user_id=this.value+'|'+selected_user_id;                              

                });

             // alert(selected_user_id);
            if(selected_user_id!="")
            {

                $.confirm({
                title: 'Confirm!',
                content: 'Are you sure want to delete?',
                buttons: {
                    confirm: function (){
                       $.ajax({  
                             url : "<?php echo base_url('users/delete_users')?>",  
                             method:"POST",  
                             data:{selected_user_id:selected_user_id},  
                             success:function(data){ 

                             CommanJS.getDisplayMessgae(400, 'Record has been deleted.');
                             table.search('').draw();
                              return true;     

                                }  
                        }); 

                         },

                         cancel: function () {
                            return true;
                        } 
                       }
                   })
               }
            else
            {
                //alert('please selected atleast one user');
                swal({
                    title: "Alert",
                    text: "Please selected atleast one user",
                    confirmButtonClass: "btn btn-primary btn-md",
                    confirmButtonText: "OK",
                })

            }             

            });  



            $(".select_all").click(function () {
                $(".selct_class").prop('checked', $(this).prop('checked'));
            });
            var table = $('#memListTable').DataTable({
                // Processing indicator
                "processing": true,
                // DataTables server-side processing mode
                "serverSide": true,
                // Initial no order.
                "iDisplayLength": 10,
                "bPaginate": true,
                "order": [],
                // Load data from an Ajax source
                "ajax": {
                    "url": "<?php echo base_url('users/getlistings/'.$role_id); ?>",
                    "type": "POST",
                    "data": function (data) {
                        data.active = $('#select_status option:selected').val();
                        /*data.roles_selected = $('#selected_roles option:selected').val();
                        data.container_selected = $('#selected_container option:selected').val();
                        data.course_selected = $('#selected_course option:selected').val();
                        data.section_selected = $('#selected_section option:selected').val(); */
                        data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                    },
                },
                //Set column definition initialisation properties
                "columnDefs": [
                    {
                        "targets": 0,
                        "data": null,
                        "orderable": false,
                        "render": function (data, type, full, meta) {
                            var data = '';
                            if (type === 'display') {
                                data = '<label class="checkbox-tel"><input type="checkbox" name="user_id[]" class="selct_class" value="' + full['id'] + '"></label>';
                            }
                            return data;
                        }
                    },
                    {
                        "targets": 2,
                        "data": null,
                        "render": function (data, type, full, meta) {
                            if (type === 'display') {
                                if (full['active'] == 1) {
                                    data = '<a href="#"  data-user-id="' + full['id'] + '" class="btn btn-success btn-xs btn-round-25 btn-block"><?php echo lang('index_active_link'); ?></a>';
                                } else {
                                    data = '<a href="#"  data-user-id="' + full['id'] + '"  data-user-id="' + full['id'] + '" class="btn btn-danger btn-xs btn-round-25 btn-block active_btn"><?php echo lang('index_inactive_link'); ?></a>';
                                }
                            }
                            return data;
                        }
                    },
                    {
                        "targets": 1,
                        "orderable": false,
                        "data": null,
                        "render": function (data, type, full, meta) {
                            if (type === 'display') {
                                data = '<a style="margin-left:8px;" data-toggle="tooltip" title="send email" href="#" data-user-id="' + full['id'] + '" class="send_email btn btn-info btn-sm" data-toggle="modal" data-target="#myModal"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>';
<?php if ($this->ion_auth->is_admin() || $permissions->edit_per): ?>
                                    data += '<a data-toggle="tooltip" title="edit user" class="btn btn-primary btn-sm" href="<?= base_url('users/edit_user/') ?>' + full['id'] + '"><i class="ti ti-pencil"></i></a>';
    <?php
endif;
if ($this->ion_auth->is_admin() || $permissions->delete_per):
    ?>
                                    data += '<a data-toggle="tooltip" title="delete user" href="<?= base_url('users/delete_user/') ?>' + full['id'] + '" class="btn btn-danger btn-sm delete_item"><i class="ti ti-trash"></i></a>';
    <?php
endif;
//if ($this->ion_auth->is_admin() || $permissions->view_per):
    ?>
                                   // data += '<a data-toggle="tooltip" title="view user" href="<?= base_url('users/view_user/') ?>' + full['id'] + '" class="btn btn-warning btn-sm"><i class="ti ti-eye"></i></a>';
<?php //endif; ?>
                            }
                            return data;
                        }

                    }],
                "columns": [
                    {"data": "checkbox"},
                   
                    {"data": "id"},
                    {"data": "id"},
                    {"data": "user_name"},
                    {"data": "email"},
                    {"data": "institution"},
                    {"data": "date"},
                ]
            },
                    );
            $('.selected_condition').on('change', function () {
                table.search('').draw();
            });
            //load courses 
           
            $("#reload_form").click(function () 
            { 
              $('#select_status').val(2); 
              table.search('').draw();         
            });

           
            // delete user

            $('[data-toggle="tooltip"]').tooltip();
            $(document).on('click', '.delete_item', function (e) {
                e.preventDefault();
                var scope = $(this);
                $.confirm({
                    title: 'Confirm!',
                    content: 'Are you sure want to delete?',
                    buttons: {
                        confirm: function () {
                            $.get(scope.attr("href"), // url
                                    function (data, textStatus, jqXHR) {  // success callback
                                        var obj = JSON.parse(data);
                                        if (obj.msg === 'deleted') {
                                            // scope.closest("tr").remove();
                                            table.search('').draw();
                                        }
                                    });
                            return true;
                        },
                        cancel: function () {
                            return true;
                        }
                    }
                });
            });
        });
        $(document).on('click', '.send_email', function (event)
        {
            event.preventDefault();
            /* Act on the event */
            var id = $(this).data('user-id');
            $('#myModal').modal('show');
            $.ajax({
                url: '<?php bs() ?>users/email/get_user_email/' + id,
                type: 'POST',
            }).done(function (success)
                    {
                        var obj = $.parseJSON(success);

                        $("#to").val(obj.email);

                    })
        });

        $(document).on('click', '.active_btn', function (e) {
            var scope = $(this);
            e.preventDefault();
            $.ajax({
                type: "GET",
                url: "<?php echo base_url('users/activate/'); ?>" + scope.data('user-id'),
                data: null,
                success: function (response) {
                    console.log(response);
                    var obj = jQuery.parseJSON(response);
                     CommanJS.getDisplayMessgae(200, obj.msg)
                    scope.text('Active');
                    scope.toggleClass('btn-danger');
                    scope.toggleClass('btn-success');
                    scope.toggleClass('active_btn');
                    scope.attr('href', "<?php echo base_url('users/auth/deactivate/'); ?>" + scope.data('user-id'));
                }
            });

            });

        $(document).ready(function () {
            $("#memListTable_paginate .pagination").click(function () {
                $("html, body").animate({
                    scrollTop: 0
                }, "slow");
            });
        });

    </script>