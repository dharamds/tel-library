<!-- --Message Modal-- -->
<div class="modal fade" id="message_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-share-square" aria-hidden="true"></i> Send
                Message</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="<?php bs('users/send_message') ?>" id="send_message" class="noti-msg">
                    <div class="form-role">
                        <label>Subject</label>
                        <input type="text" class="form-control" value="" name="subject"  id="subject"placeholder="Subject" required>
                    </div>
                    <div class="form-role">
                        <label>Message</label>
                        <textarea name="msg" class="form-control editor" rows="10" id="msg" cols="10" placeholder="Write You Message" required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="message_submit_button" ><i class="fa fa-paper-plane" aria-hidden="true"></i>
                    Send</button>
                </div>
                <input type="hidden" name="selected_user_ids" id="selected_user_ids">
            </form>
        </div>
    </div>
</div>
<!-- Message Modal -->

<script type="text/javascript">
    $(document).ready(function() {  
        
       $("#send_message").validate({
            ignore: [],
            rules: {
               subject: {
                    required: true
                },
                msg: {
                  required: function(textarea) {
                    CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                    var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                    return editorcontent.length === 0;
                  }
                }
            },
            messages: {
                subject: "Please enter subject.", 
                msg: "Please enter message."
            },
            errorPlacement: function (error, $elem) {
                if ($elem.is('textarea')) {
                    $elem.insertAfter($elem.next('div'));
                }
                error.insertAfter($elem);
            },
            submitHandler: function (form) {
                $('#message_submit_button').hide();
                $.ajax({
                    url: "<?php echo bs(); ?>users/send_message",
                    method: "POST",
                    data: $('form#send_message').serialize(),
                    //dataType: "json",
                    complete: function (xhr, status) {
                        CommanJS.getDisplayMessgae(200, 'Message sent successfully.');
                       /* $('#message_modal').modal('hide');
                        getDisplayMessgae(200, 'Message sent successfully.');
                        $('#message_submit_button').show();*/
                    },
                    success: function (data) {
                        //console.log(data);
                        console.log('ssttt' +data);
                        $('#message_submit_button').show();
                        $('#message_modal').on('hidden.bs.modal', function() {                           
                            //alert('ss');
                            $('#subject').val('');
                            CKEDITOR.instances.msg.setData("");                            
                        });

                        $('#message_modal').modal('hide');

                    }
                });
                return false;
            }
        });
    });
</script>