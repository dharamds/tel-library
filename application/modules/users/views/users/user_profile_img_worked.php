<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            <ol class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li class="active"><a href="#">Profile</a></li>
            </ol>
            <div class="container-fluid">
                <div data-widget-group="group1">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="panel panel-profile">
                                <div class="panel-body">
                                    <img src="<?php bs() ?>uploads/<?php echo $user_profile->user_img ?>" class="img-circle" width="200" alt="">
                                    <?php
                                    $user = $this->ion_auth->user()->row();
                                    ?>
                                    <div class="name"><?php echo $user->first_name . ' ' . $user->middle_name . ' ' . $user->last_name; ?></div>
                                    <div class="info"><?php echo $user->email; ?></div>
                                    <ul class="list-inline text-center">
                                        <li><a href="#" class="profile-facebook-icon"><i class="ti ti-facebook"></i></a></li>
                                        <li><a href="#" class="profile-twitter-icon"><i class="ti ti-twitter"></i></a></li>
                                        <li><a href="#" class="profile-dribbble-icon"><i class="ti ti-dribbble"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- panel -->
                            <div class="list-group list-group-alternate mb-n nav nav-tabs">
                                <a href="#tab-about" 	role="tab" data-toggle="tab" class="list-group-item active">
                                    <i class="ti ti-user"></i> About <span class="badge badge-primary">80%</span></a>
                                <a href="#change_pic" role="tab" data-toggle="tab" class="list-group-item">
                                    <i class="ti ti-exchange-vertical"></i> Change Profile Avatar</a>
                                <a href="#chang_pass" role="tab" data-toggle="tab" class="list-group-item">
                                    <i class="ti ti-unlock"></i> Change Password</a>
                                <a href="#tab-edit" role="tab" data-toggle="tab" class="list-group-item">
                                    <i class="ti ti-pencil"></i> Edit</a>
                                <?php if (currentGroup() == STUDENT): ?>
                                    <a href="#add_parent" role="tab" data-toggle="tab" class="list-group-item">
                                        <i class="ti ti-pencil"></i> Manage Parent</a>
                                <?php endif; ?>
                            </div>
                        </div>
                        <!-- col-sm-3 -->
                        <div class="col-sm-9">
                            <div class="tab-content">
                                <div class="tab-pane" id="chang_pass">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h2>Change Password</h2>
                                        </div>
                                        <div class="panel-body">
                                            <div class="table">
                                                <div class="row">
                                                    <div class="col-md-5">
                                                        <?php echo form_open("users/auth/change_password", array("id" => "change_pass")); ?>
                                                        <div class="form-group">
                                                            <?php echo lang('change_password_old_password_label', 'old_password'); ?> <br />
                                                            <?php echo form_input($old_password); ?>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="new_password"><?php echo sprintf(lang('change_password_new_password_label'), $min_password_length); ?></label> <br />
                                                            <?php echo form_input($new_password); ?>
                                                        </div>
                                                        <div class="form-group">
                                                            <?php echo lang('change_password_new_password_confirm_label', 'new_password_confirm'); ?> <br />
                                                            <?php echo form_input($new_password_confirm); ?>
                                                        </div>
                                                        <?php echo form_input($user_id); ?>
                                                        <?php echo form_submit('submit', lang('change_password_submit_btn'), 'class="btn btn-toolbar"'); ?>
                                                        <?php echo form_close(); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.table-responsive -->
                                        </div>
                                        <!-- /.panel-body -->
                                    </div>
                                </div>
                                <!-- #tab-projects -->
                                <div class="tab-pane active" id="tab-about">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h2>About</h2>
                                        </div> 
                                        <div class="panel-body">
                                            <div class="about-area">
                                                <h4>Personal Information</h4>
                                                <div class="table-responsive">
                                                    <table class="table about-table">
                                                        <tbody>
                                                            <tr>
                                                                <th>Full Name</th>
                                                                <td><?php echo $user_profile->first_name . " " . $user_profile->last_name ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Country</th>
                                                                <td>
                                                                    <?php
                                                                    //ipinfo grabs the ip of the person requesting

                                                                    $getloc = json_decode(file_get_contents("http://ipinfo.io/"));

                                                                    $country_code = $getloc->country; //to get city

                                                                    echo $country_name = get_country_name($country_code);
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th>Email</th>
                                                                <td><?php echo $user_profile->email ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Mobile</th>
                                                                <td><?php echo $user_profile->phone ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Created Date</th>
                                                                <td><?php echo date('d-m-y', strtotime($user_profile->date)) ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Mobile No</th>
                                                                <td><?php echo $user_profile->phone ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Role</th>
                                                                <td><?php echo $user_role_details->name ?></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="change_pic">
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h2>Change Profile Picture</h2>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php bs() ?>users/update_avatar" role="form">





                                                        <div class=" col-md-2 ist-logo">
                                                            <?php if (!empty($user->user_img)) { ?>
                                                                <img class="img-responsive img-circle" width="200" src="<?php bs() ?>uploads/<?php echo $user->user_img ?>" id="up_course_img">
                                                            <?php } else { ?>
                                                                <img class="img-responsive img-circle" width="200" src="<?php bs() ?>public/assets/img/default_user.png" id="up_course_img">
                                                            <?php } ?>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <div class="ist-visual-info">
                                                                <h4>Select Profile Image</h4>
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <div class="fileinput fileinput-new" style="width: 100%;" data-provides="fileinput">
                                                                            <div>
                                                                                <a style="margin-left:8px;margin-bottom: 8px;" class="btn btn-danger fileinput-exists delete_img" data-dismiss="fileinput" style="line-height: normal;">Remove</a>
                                                                                <span class="btn btn-primary btn-primary-default btn-file">
                                                                                    <span class="fileinput-new">Choose File</span>
                                                                                    <span class="fileinput-exists">Change</span>
                                                                                    <?php
                                                                                    echo form_upload([
                                                                                        'name' => 'courseImg',
                                                                                        'id' => 'courseImg',
                                                                                        'data-type' => 'course_img', 'class' => 'blog_img1 visible1']);
                                                                                    ?>
                                                                                    <?php if (!empty($user->user_img))  { ?>
                                                                                        <input type="hidden" id="chang_image" name="chang_image" value="<?php echo $user->user_img; ?>">
                                                                                    <?php } else { ?>
                                                                                        <input type="hidden" id="chang_image" name="chang_image" value="">
                                                                                    <?php } ?>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="small">(File must be .png, .jpg, .jpeg)</div>
                                                                        <img id="imgLoader" src="<?php echo base_url(); ?>/public/assets/img/spinner.gif" style="display:none;"/>
                                                                        <!----------------->                
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

<!--                                                        <div class="form-group">
                                                            <div class="col-lg-6">
                                                                <?php
                                                                if (empty($user->user_img)) {
                                                                    ?>
                                                                    <img src="<?php bs() ?>public/assets/img/default_user.png" class="img-responsive img-circle" width="200" alt="">
                                                                    <?php
                                                                } else {
                                                                    ?>
                                                                    <img src="<?php bs() ?>uploads/<?php echo $user->user_img ?>" class="img-responsive img-circle" width="200" alt="">
                                                                    <?php
                                                                }
                                                                ?>    
                                                                <br><br>
                                                                <input type="file" name="chang_image">
                                                            </div>
                                                        </div>-->
                                                        
                                                        
                                                        <div class="form-group">
                                                            <div class="col-lg-offset-2 col-lg-10">
                                                                <button type="submit" class="btn btn-success">Change</button>
                                                                <button type="button" class="btn btn-default">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab-edit">
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h2>Edit</h2>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <form method="post" action="<?php bs() ?>users/edit_profile">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">First Name</label>
                                                            <input type="text" class="form-control" name="first_name" value="<?php echo $user_profile->first_name ?>">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">Middle Name</label>
                                                            <input type="text" class="form-control" name="middle_name" value="<?php echo $user_profile->middle_name ?>">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInputPassword1">Last Name</label>
                                                            <input type="text" class="form-control" name="last_name" value="<?php echo $user_profile->last_name ?>">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInputPassword1">Email</label>
                                                            <input type="email" class="form-control" name="email" value="<?php echo $user_profile->email ?>">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInputPassword1">Mobiel No</label>
                                                            <input type="text" class="form-control" name="mobile_no" value="<?php echo $user_profile->phone ?>">
                                                        </div>
                                                        <button type="submit" class="btn btn-primary">Save</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="add_parent">
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h2>Add Parent</h2>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <form method="post" action="<?php bs() ?>users/add_parents" id="add_parents">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">First name</label>
                                                            <input type="text" class="form-control" id="first_name" required name="first_name" value="">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">Middle name</label>
                                                            <input type="text" class="form-control" id="middle_name" name="middle_name" value="">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">Last name</label>
                                                            <input type="text" class="form-control" id="last_name" required name="last_name" value="">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">Parent Email</label>
                                                            <input type="email" class="form-control" name="email" required id="email" value="">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">Parent Phone</label>
                                                            <input type="phone" class="form-control" name="phone" required id="phone" value="">
                                                        </div>
                                                        <button type="submit" class="btn btn-primary">Save</button>
                                                    </form>

                                                </div>
                                                <div id="list_parents_error"></div>

                                            </div>
                                        </div>
                                        <div class="col-md-12"><ul  id="list_parents" class="list-group"></ul></div>

                                    </div>
                                </div>
                            </div>
                            <!-- .tab-content -->
                        </div>
                        <!-- col-sm-8 -->
                    </div>
                </div>
            </div>
            <!-- .container-fluid -->
        </div>
        <!-- #page-content -->
    </div>
</div>
<script type="text/javascript" src="<?= bs('public/assets/plugins/form-jasnyupload/fileinput.min.js') ?>"></script>   
<script>
    $(document).ready(function ()
    {
        load_list();
    });
    $('form#add_parents').submit(function (e) {
        var form = $(this);
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "<?php echo bs('users/add_parents'); ?>",
            data: form.serialize(), // <--- THIS IS THE CHANGE
            dataType: "html",
            success: function (data) {
                //$('#list_parents_error').html(data);
                load_list();
            },
            error: function () {
                alert("Error posting feed.");
            }
        });
    });

    $(document).on("click", ".delete_item", function (e) {
        e.preventDefault();
        var url = $(this).attr('href');
        $form = $(this);

        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure want to remove parent?',
            buttons: {
                confirm: function () {
                    $.ajax({
                        type: "POST",
                        url: url,
                        dataType: "html",
                        success: function (data) {
                            console.log(url);
                            $form.closest("li").remove();
                        },
                        error: function () {
                            alert("Error posting feed.");
                        }
                    });
                },
                cancel: function () {
                    //$.alert('Canceled!');
                }
            }
        });


    });

    function load_list() {
        $.ajax({
            type: "POST",
            url: "<?php echo bs('users/get_parents'); ?>",
            dataType: "json",
            success: function (data) {
                var items = [];
                $.each(data, function (i, item) {
                    items.push(
                            '<li class="list-group-item"><a href="<?php echo bs('/users/remove_parent/'); ?>' + item.id + '" class="pull-right delete_item" title="Remove Parent"><i class="ti ti-close"></i></a><b>' + item.first_name + ' ' + item.last_name + '</b><br><span class="">' + item.email + '</span>' +
                            '</li>');
                });
                $('#list_parents').html('');
                $('#list_parents').append(items.join(''));
            },
            error: function () {
                alert("Error posting feed.");
            }
        });

    }
    
    
    
    
    
    
     // Upload course image
        $(document).on('change', '#courseImg', function () {
            //alert('dddd');
            var ext = $('#courseImg').val().split('.').pop().toLowerCase();
            if ($.inArray(ext, ['jpg', 'png', 'jpeg']) == -1) {
                return false;
            } else {
                $("#uploadedCourseImg").val('');
                var file_data = $(this).prop("files")[0];
                var form_data = new FormData();
                form_data.append("file", file_data)
                form_data.append("elementId", $(this).attr('id'))
                form_data.append("elementType", $(this).data('type'))
                $.ajax({
                    url: "<?php echo bs(); ?>users/Users/upload_docs_image",
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data, // Setting the data attribute of ajax with file_data
                    type: 'post',
                    beforeSend: function () {
                        $("#imgLoader").show();
                    },
                    success: function (data) {
                        console.log(data);
                        $("#imgLoader").hide();
                        var obj = JSON.parse(data);
                        $("#up_course_img").attr('src', obj.filePath + obj.fileName);
                        $("#up_course_img").attr('data-imgname', obj.fileName);
                        $("#chang_image").val(obj.fileName);
                    }
                });
            }
        });


        $(document).on('click', 'a.delete_img', function () {
            var filename = $("#up_course_img").data('imgname');
            alert(filename);
            var scope = $(this);
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure?',
                buttons: {
                    confirm: function () {
                        $.ajax({
                            url: "<?php echo bs(); ?>users/Users/remove_user_img",
                            type: "POST",
                            data: {'file': filename},
                            beforeSend: function () {
                                $("#imgLoader").show();
                            },
                            success: function (data) {
                                console.log(data);
                                $("#imgLoader").hide();
                                if (data) {
                                    $("#up_course_img").attr('src', '<?php echo bs(); ?>/public/assets/img/logo-bg.png');
                                    $("#chang_image").val('');
                                }
                            }
                        });
                        return true;
                    },
                    cancel: function () {
                        return true;
                    }
                }
            });
        });
    
    
    
    
    
    
    
    
    
</script>