<div class="container-fluid">
    <div class="panel panel-default panel-grid" data-widget='{"draggable": "false"}'>
        <div class="panel-heading m-0 p-0 brd-0"></div>

        <div class="panel-body">
            <div class="list-item icon-left mt-4">
                <div class="list-item-head">
                    <span class="ficon flaticon-avatar"></span>
                    Profile Picture
                </div>
                <div class="list-item-content">
                    <div class="row m-0 p-0">
                        <div class="col-md-3 p-0">
                            <?php if ($user_profile->user_img != '') { ?>
                                <div class="user-profile-thumb-new">
                                    <img src="<?php bs($user_profile->user_img) ?>" id="image_write_path" alt="">
                                </div>
                            <?php } else { ?>
                                <div class="user-profile-thumb-new">
                                    <img src="<?php bs() ?>public/assets/img/default_user.png" alt="" id="image_write_path">
                                </div>
                            <?php } ?>
                        </div>
                        <div class="col-md-4 p-0 pt-4">
                            <div class="col-md-12">
                                <?php
                                $user = $this->ion_auth->user()->row();
                                ?>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="hidden" name="old_img" value="">
                                        <div class="fileinput fileinput-new col-md-12 p-0" data-provides="fileinput">
                                            <span class="btn btn-primary btn-primary-default btn-file" id="choose_media_image_file">
                                                <span class="fileinput-new">Choose File</span>
                                                <span class="fileinput-exists">Choose File</span>
                                            </span>
                                            <a class="btn btn-danger fileinput-exists delete_img">Remove</a>
                                        </div>

                                        <div class="small"> (File must be .png, .jpg, .jpeg) </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <form method="post" id="editProfileForm" action="<?php bs() ?>users/edit_profile">
                <input type="hidden" name="img_name" id="img_name" value="">
                <div class="col-md-12 hr-line mt-3 mb-3"></div>
                <div class="col-xs-12 list-item icon-left mt-4">
                    <div class="list-item-head">
                        <span class="ficon flaticon-mind"></span>
                        Basic Information
                    </div>
                    <div class="list-item-content">
                        <div class="row">
                            <div class="col-md-4 p-0 mb-4">
                                <label for="fieldname" class="col-md-12 control-label">First Name<span class="field-required">*</span></label>
                                <div class="col-md-12">
                                    <?php
                                    echo form_input([
                                        'name' => 'first_name',
                                        'id' => 'first_name',
                                        'class' => 'form-control',
                                        'value' => $user_profile->first_name,
                                        'placeholder' => 'Enter First Name'
                                    ]);
                                    ?>
                                    <?php echo form_error('first_name', '<div class="error">', '</div>'); ?>
                                </div>
                            </div>


                            <div class="col-md-4 p-0 mb-4">
                                <label for="fieldname" class="col-md-12 control-label">Middle Name</label>
                                <div class="col-md-12">
                                    <?php
                                    echo form_input([
                                        'name' => 'middle_name',
                                        'id' => 'middle_name',
                                        'class' => 'form-control',
                                        'value' => $user_profile->middle_name,
                                        'placeholder' => 'Enter Middle Name'
                                    ]);
                                    ?>
                                    <?php echo form_error('middle_name', '<div class="error">', '</div>'); ?>
                                </div>
                            </div>

                            <div class="col-md-4 p-0 mb-4">
                                <label for="fieldname" class="col-md-12 control-label">Last Name<span class="field-required">*</span></label>
                                <div class="col-md-12">
                                    <?php
                                    echo form_input([
                                        'name' => 'last_name',
                                        'id' => 'last_name',
                                        'class' => 'form-control',
                                        'value' => $user_profile->last_name,
                                        'placeholder' => 'Enter Last Name'
                                    ]);
                                    ?>
                                    <?php echo form_error('last_name', '<div class="error">', '</div>'); ?> </div>
                            </div>
                            <div class="col-md-4 p-0 mb-4">
                                <label for="fieldname" class="col-md-12 control-label">Unique ID<span class="field-required">*</span></label>
                                <div class="col-md-12">
                                    <?php
                                    echo form_input([
                                        'class' => 'form-control',
                                        'value' => $user_profile->unique_id,
                                        "readonly" => "readonly",
                                        'placeholder' => 'Unique ID'
                                    ]);
                                    ?>
                                </div>
                            </div>

                            <div class=" col-md-4 p-0 mb-4">
                                <label for="fieldname" class="col-md-12 control-label">Primary Email<span class="field-required">*</span></label>
                                <div class="col-md-12">
                                    <?php
                                    echo form_input([
                                        'class' => 'form-control',
                                        'value' => $user_profile->email,
                                        "readonly" => "readonly",
                                        'placeholder' => 'Enter Email'
                                    ]);
                                    ?>
                                    <?php echo form_error('email', '<div class="error">', '</div>'); ?>
                                    <div id="user_mail" style="color: red;font-weight: bold;"></div>
                                </div>
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="fieldname" class="control-label">
                                    Secondary Email
                                </label>
                                <?php
                                echo form_input([
                                    'name' => 'secondary_email',
                                    'id' => 'secondary_email',
                                    'class' => 'form-control',
                                    'value' => $user_profile->secondary_email,
                                    'placeholder' => 'Secondary Email'
                                ]);
                                ?>
                                <?php echo form_error('secondary_email', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="fieldname" class="control-label">
                                    User Role
                                </label>
                                <?php
                                echo form_input([
                                    'id' => 'user_role',
                                    'class' => 'form-control',
                                    "readonly" => "readonly",
                                    'value' => $user_role_details->name,
                                    'placeholder' => 'User Role'
                                ]);
                                ?>
                                <?php echo form_error('secondary_email', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="fieldname" class="control-label">
                                    Associated Institution
                                </label>
                                <?php
                                echo form_input([
                                    'name' => 'institution_name',
                                    'id' => 'institution_name',
                                    'class' => 'form-control',
                                    "readonly" => "readonly",
                                    'value' => $user_profile->institution_name,
                                    'placeholder' => 'Associated Institution'
                                ]);
                                ?>
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="fieldname" class="control-label">
                                    Time Zone
                                </label>

                                <?php echo form_dropdown('time_zone', $timezones, $user->time_zone, ['id' => 'time_zone', 'class' => 'form-control']); ?>
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="fieldname" class="control-label">
                                    Institution ID #
                                </label>
                                <?php
                                echo form_input([
                                    'name' => 'institutional_unique_id',
                                    'id' => 'institutional_unique_id',
                                    'class' => 'form-control',
                                    "readonly" => "readonly",
                                    'value' => $user_profile->institutional_unique_id,
                                    'placeholder' => 'Institutional Unique'
                                ]);
                                ?>
                                <?php echo form_error('secondary_email', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="fieldname" class="control-label">
                                    Phone Number<span class="field-required">*</span>
                                </label>
                                <?php
                                echo form_input([
                                    'name' => 'phone',
                                    'id' => 'phone',
                                    'class' => 'form-control',
                                    'value' => $user_profile->phone,
                                    'placeholder' => 'Phone Number'
                                ]);
                                ?>
                                <?php echo form_error('phone', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="fieldname" class="control-label">
                                    Cell Phone Number
                                </label>
                                <?php
                                echo form_input([
                                    'name' => 'phone',
                                    'id' => 'phone',
                                    'class' => 'form-control',
                                    'value' => $user_profile->cellphone,
                                    'placeholder' => 'Cell Phone Number'
                                ]);
                                ?>
                                <?php echo form_error('phone', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="col-md-4 p-0 form-group">
                                <div class="flex-row flex-col-12">
                                    <div class="flex-col-3">
                                        <label for="fieldname" class="control-label">
                                            Status
                                        </label>
                                    </div>
                                    <div class="flex-col-9">
                                        <label class="radio-tel">
                                            <input type="radio" name="status" class="mr-3">Active
                                        </label>
                                        <label class="radio-tel pl-3">
                                            <input type="radio" name="status" class="mr-3">Inactive
                                        </label>
                                    </div>
                                    <?php echo form_error('phone', '<div class="error">', '</div>'); ?>
                                </div>                            
                            
                                
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-12 hr-line mt-3 mb-3"></div>
                <div class="col-xs-12 list-item icon-left mt-4">
                    <div class="list-item-head">
                        <span class="ficon flaticon-placeholder-1"></span>
                        Address
                    </div>
                    <div class="list-item-content">
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label for="fieldname" class="control-label">
                                    Street Address 1<span class="field-required">*</span>
                                </label>
                                <?php
                                echo form_input([
                                    'name' => 'street_address_1',
                                    'id' => 'street_address_1',
                                    'class' => 'form-control',
                                    'value' => $user_profile->street_address_1,
                                    'placeholder' => 'Street Address 1'
                                ]);
                                ?>
                                <?php echo form_error('street_address_1', '<div class="error">', '</div>'); ?>

                            </div>
                            <div class="col-md-4 form-group">
                                <label for="fieldname" class="control-label">
                                    Street Address 2
                                </label>
                                <?php
                                echo form_input([
                                    'name' => 'street_address_2',
                                    'id' => 'street_address_2',
                                    'class' => 'form-control',
                                    'value' => $user_profile->street_address_2,
                                    'placeholder' => 'Street Address 2'
                                ]);
                                ?>
                                <?php echo form_error('street_address_2', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="fieldname" class="control-label">
                                    Country
                                </label>
                                <?php
                                $country = [
                                    "" => "Select Country",
                                    "India" => "India",
                                    "United States of America" => "United States of America"
                                ]
                                ?>
                                <?php echo form_dropdown('country', $country, $user->country_id, ['id' => 'country', 'class' => 'form-control']); ?>
                                <?php echo form_error('time_zone', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="fieldname" class="control-label">
                                    State<span class="field-required">*</span>
                                </label>
                                <?php
                                echo form_input([
                                    'name' => 'state',
                                    'id' => 'state',
                                    'class' => 'form-control states ',
                                    'value' => $user_profile->state,
                                    'placeholder' => 'State'
                                ]);
                                ?>
                                <?php echo form_error('state', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="fieldname" class="control-label"> City <span class="field-required">*</span> </label>
                                <?php
                                echo form_input([
                                    'name' => 'city',
                                    'id' => 'city',
                                    'class' => 'form-control city ',
                                    'value' => $user_profile->city,
                                    'placeholder' => 'City'
                                ]);
                                ?>
                                <?php echo form_error('city', '<div class="error">', '</div>'); ?>
                            </div>
                           
                            <div class="col-md-4 form-group">
                                <label for="fieldname" class="control-label">
                                    Zip<span class="field-required">*</span>
                                </label>
                                <?php
                                echo form_input([
                                    'name' => 'zip',
                                    'id' => 'zip',
                                    'class' => 'form-control',
                                    'value' => $user_profile->zip,
                                    'placeholder' => 'Zip'
                                ]);
                                ?>
                                <?php echo form_error('zip', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>

                        <!-- <div class="row">
                            <div class="col-md-4 form-group">
                                <label for="fieldname" class="control-label">
                                    Time Zone
                                </label>
                                <?php echo form_dropdown('time_zone', $timezones, $user->time_zone, ['id' => 'time_zone', 'class' => 'form-control']); ?>
                                <?php echo form_error('time_zone', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="col-md-12 form-group">
                                <label class="checkbox-tel"><input type="checkbox" name="password_email_check" value="1" <?php
                                                                                                                            if ($user_profile->password_email_check == 1) {
                                                                                                                                echo "checked";
                                                                                                                            }
                                                                                                                            ?> class="mr-3 t2">Also send Password reset email to this email address</label>
                            </div>
                            <div class="col-md-12 form-group">
                                <label class="checkbox-tel"><input type="checkbox" name="notification_email_check" value="1" <?php
                                                                                                                                if ($user_profile->notification_email_check == 1) {
                                                                                                                                    echo "checked";
                                                                                                                                }
                                                                                                                                ?> class="mr-3 t2">Also send Notifications email to this email address</label>
                            </div>
                        </div> -->
                    </div>
                </div>
                <div class="panel-body">
                    <div class="col-md-12 hr-line mt-3 mb-3"></div>
                    <div class="col-xs-12 list-item icon-left mt-4">
                        <div class="list-item-head">
                            <span class="ficon flaticon-profile"></span>
                            Notes
                        </div>
                        <div class="list-item-content">
                            <div class="row">
                                <div class="col-md-5 form-group">
                                    <label for="fieldname" class="control-label">Notes</label>
                                    <textarea id="notes" name="notes" rows="10" class="editor"><?php echo $lessonsData->notes ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row m-0 text-right">
                        <div class="col-md-12 p-0">
                            <input type="submit" class="finish btn-primary btn" value="Save Changes">
                        </div>
                    </div>
                </div>
        </div>
        </form>
        <!----Change Password---->

        <?php echo form_open("users/change_password", array("id" => "change_pass")); ?>
        <div class="panel-body">
            <div class="col-md-12 hr-line mt-3 mb-3"></div>
            <div class="col-xs-12 list-item icon-left mt-4">
                <div class="list-item-head">
                    <span class="ficon flaticon-profile"></span>
                    Security Question
                </div>
                <div class="list-item-content">
                    <div class="row">
                        <div class="col-md-6 form-group">
                            
                            <?php echo form_dropdown('question', $questions, $user->time_zone, ['id' => 'question', 'class' => 'form-control']); ?>
                        </div>
                        <div class="col-md-6 form-group">
                            <?php echo form_input([
                                    'name' => 'answer',
                                    'id' => 'answer',
                                    'class' => 'form-control',
                                    'value' => "",
                                    'placeholder' => 'Enter answer'
                                ]); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 list-item icon-left mt-4">
                <div class="list-item-head">
                    <span class="ficon flaticon-profile"></span>
                    Change Password
                </div>
                <div class="list-item-content">
                    <div class="row">
                        <div class="col-md-5 form-group">
                            <label for="fieldname" class="control-label">Old Password</label>
                            <?php echo form_input($old_password); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 form-group">
                            <label for="fieldname" class="control-label">New Password (at least 8 characters long)</label>
                            <?php echo form_input($new_password); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 form-group">
                            <label for="fieldname" class="control-label">Confirm New Password </label>
                            <?php echo form_input($new_password_confirm); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 p-0 text-right">
                <?php echo form_input($user_id); ?>
                <?php echo form_submit('submit', lang('change_password_submit_btn'), 'class="btn btn-primary"'); ?>
                <?php echo form_close(); ?>
            </div>
            <div class="col-md-12 hr-line mt-5 mb-3"></div>
        </div>
        </form>


    </div>

</div>
<div class="modal fade modals-tel-theme" id="gallery_images_popup" role="dialog">
    <div class="modal-dialog w80p">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Upload Image</h4>
            </div>
            <div class="modal-body p-0">
                <div id="popup_img_div"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>


<script type="text/javascript" src="<?= bs('public/assets/plugins/form-jasnyupload/fileinput.min.js') ?>"></script>

<script src="http://malsup.github.com/jquery.form.js"></script>

<script type='text/javascript' src="<?php echo base_url('public\assets\js\users\add_user.js'); ?>"></script>

<?php
        $jsVars = [
            'ajax_call_root' => base_url(),
            'states'         => $states,
        ];
        ?>
<script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<script src="https://unpkg.com/jquery-input-mask-phone-number@1.0.0/dist/jquery-input-mask-phone-number.js"></script>
<script type="text/javascript">
    function media_image_callback(image_name, img_full_url) {
        $('#image_write_path').attr('src', img_full_url);
        $('#img_name').val(image_name);
    }

    $("#choose_media_image_file").click(function() {
        CommanJS.choose_media_image_file('media_image_callback');
    });
    $(document).ready(function() {
        var config = {
            height: 120,
            width: 1000,
            toolbar: 'short',
            allowedContent: true,
        };
        // ck editor
        $('.editor').each(function(e) {
            CKEDITOR.replace(this.id, config);
        });
        $('#submitButton').click(function() {

            var ext = $('#uploadImage').val().split('.').pop().toLowerCase();
            if ($.inArray(ext, ['jpg', 'png', 'jpeg']) == -1) {

                $("#outputImage").show();
                $("#outputImage").html("<div class='error'>Please select valid image.</div>");

                return false;
            } else {
                $('#uploadForm').ajaxForm({
                    //   target: '#outputImage',
                    url: "<?php echo bs(); ?>users/Users/upload_docs_image",
                    beforeSubmit: function() {
                        $("#outputImage").hide();
                        if ($("#uploadImage").val() == "") {
                            $("#outputImage").show();
                            $("#outputImage").html("<div class='error'>Choose a file to upload.</div>");
                            return false;
                        }
                        var percentValue = '0%';
                        $('#progressBar').width(percentValue);
                        $('#percent').html(percentValue);
                        $("#progressDivId").css("display", "block");
                    },
                    uploadProgress: function(event, position, total, percentComplete) {

                        var percentValue = percentComplete + '%';
                        $("#progressBar").animate({
                            width: '' + percentValue + ''
                        }, {
                            duration: 5,
                            easing: "linear",
                            step: function(x) {
                                percentText = Math.round(x * 100 / percentComplete);
                                $("#percent").text(percentText + "%");
                                if (percentText == "100") {
                                    $("#progressDivId").css("display", "none");
                                    $('#uploadForm')[0].reset();
                                }
                            }
                        });
                    },
                    error: function(response, status, e) {
                        alert('Oops something went.');
                    },
                    success: function(data, percentComplete) {
                        var obj = JSON.parse(data);
                        if (obj.status == true) {
                            $("#up_course_img").attr('src', obj.filePath + obj.fileName);
                            $("#up_course_img").attr('data-imgname', obj.fileName);
                            var percentValue = '0%';
                            $("#progressBar").animate({
                                'width': percentValue
                            }, {
                                step: function(x) {
                                    $("#percent").text("0%");
                                }
                            });
                        } else {
                            $("#outputImage").show();
                            $("#outputImage").html("<div class='error'>Please select valid image.</div>");
                        }
                    },
                });
            }
        });
    });
</script>
<script>
    $(document).ready(function() {
        $('#phone').usPhoneFormat();
    });
</script>

<script>
    $(document).ready(function() {
        //load_list();

        Users.init(<?php echo json_encode($jsVars); ?>);

        jQuery.validator.addMethod("noSpace", function(value, element, param) {
            return value.match(/^(?=.*\S).+$/);
        }, "No space please and don't leave it empty");

        jQuery.validator.addMethod("emailExt", function(value, element, param) {
            return value.match(/^[a-zA-Z0-9_\.%\+\-]+@[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,}$/);
        }, "Please enter valid email address");

        jQuery.validator.addMethod("phoneUS", function(phone_number, element) {
            phone_number = phone_number.replace(/\s+/g, "");
            return this.optional(element) || phone_number.length > 9 &&
                phone_number.match(/^\(?(\d{3})\)?[-\. ]?(\d{3})[-\. ]?(\d{4})$/);
        }, "Invalid phone number");

    });


    $('#change_pass').validate({ // initialize the plugin
        rules: {
            old: {
                required: true,
                noSpace: true,
            },
            new: {
                required: true,
                noSpace: true,
            },
            new_confirm: {
                required: true,
                noSpace: true,
            }
        },
        messages: {
            old: {
                required: "Please enter current password",
            },
            new: {
                required: "Please enter new password",
            },
            new_confirm: {
                required: "Please confirm new password",
            }
        },
        submitHandler: function(form) {

            // alert('sdsd');

            $.ajax({
                url: $("#change_pass").attr('action'),
                type: "POST",
                data: $("#change_pass").serialize(),
                success: function(data) {
                    var obj = JSON.parse(data);
                    CommanJS.getDisplayMessgae(200, obj.msg);
                }
            });
            return false;
        },
        errorPlacement: function(error, $elem) {
            error.insertAfter($elem);
        },
    });



    $('#editProfileForm').validate({ // initialize the plugin
        rules: {
            first_name: {
                required: true,
                noSpace: true,
            },
            last_name: {
                required: true,
                noSpace: true,
            },
            street_address_1: {
                required: true,
                noSpace: true,
            },
            city: {
                required: true

            },
            phone: {
                required: true,

            },
            state: {
                required: true

            },
            zip: {
                required: true,

            },
            time_zone: {
                required: true,

            }
        },
        messages: {
            first_name: {
                required: "Please enter first name",
            },
            last_name: {
                required: "Please enter last name",
            },
            email: {
                required: "Please enter email",
            },
            mobile_no: {
                required: "Please enter mobile no",
            },
            street_address_1: {
                required: "Please enter street address 1",
            },
            phone: {
                required: "Please enter phone no"
            },
            city: {
                required: "Please enter city",
            },
            state: {
                required: "Please enter state",
            },
            zip: {
                required: "Please enter zip",
            },
            time_zone: {
                required: "Please enter time zone",
            },
        },
        submitHandler: function(form) {
            $.ajax({
                url: $("#editProfileForm").attr('action'),
                type: "POST",
                data: $("#editProfileForm").serialize(),
                success: function(data) {
                    var obj = JSON.parse(data);
                    CommanJS.getDisplayMessgae(200, obj.msg);
                }
            });
            return false;
        },
        errorPlacement: function(error, $elem) {
            error.insertAfter($elem);
        },
    });





    $('form#add_parents').submit(function(e) {
        var form = $(this);
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "<?php echo bs('users/add_parents'); ?>",
            data: form.serialize(), // <--- THIS IS THE CHANGE
            dataType: "html",
            success: function(data) {
                //load_list();
            },
            error: function() {
                alert("Error posting feed.");
            }
        });
    });
    function load_list() {
        $.ajax({
            type: "POST",
            url: "<?php echo bs('users/get_parents'); ?>",
            dataType: "json",
            success: function(data) {
                var items = [];
                $.each(data, function(i, item) {
                    items.push(
                        '<li class="list-group-item"><a href="<?php echo bs(' / users / remove_parent / '); ?>' + item.id + '" class="pull-right delete_item" title="Remove Parent"><i class="ti ti-close"></i></a><b>' + item.first_name + ' ' + item.last_name + '</b><br><span class="">' + item.email + '</span>' +
                        '</li>');
                });
                $('#list_parents').html('');
                $('#list_parents').append(items.join(''));
            },
            error: function() {
                alert("Error posting feed.");
            }
        });
    }


    // Upload course image
    $(document).on('change', '#courseImg', function() {
        //alert('dddd');
        var ext = $('#courseImg').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['jpg', 'png', 'jpeg']) == -1) {
            return false;
        } else {
            $("#uploadedCourseImg").val('');
            var file_data = $(this).prop("files")[0];
            var form_data = new FormData();
            form_data.append("file", file_data)
            form_data.append("elementId", $(this).attr('id'))
            form_data.append("elementType", $(this).data('type'))
            $('#userProfileUpload').ajaxForm({
                url: "<?php echo bs(); ?>users/Users/upload_docs_image",
                cache: false,
                contentType: false,
                processData: false,
                data: form_data, // Setting the data attribute of ajax with file_data
                type: 'post',
                beforeSend: function() {
                    // $("#imgLoader").show();


                    $("#outputImage").hide();
                    if ($("#courseImg").val() == "") {
                        $("#outputImage").show();
                        $("#outputImage").html("<div class='error'>Choose a file to upload.</div>");
                        return false;
                    }

                    $("#progressDivId").css("display", "block");
                    var percentValue = '0%';

                    $('#progressBar').width(percentValue);
                    $('#percent').html(percentValue);



                },

                uploadProgress: function(event, position, total, percentComplete) {

                    console.log(percentComplete);
                    var percentValue = percentComplete + '%';
                    $("#progressBar").animate({
                        width: '' + percentValue + ''
                    }, {
                        duration: 5000,
                        easing: "linear",
                        step: function(x) {
                            percentText = Math.round(x * 100 / percentComplete);
                            $("#percent").text(percentText + "%");
                            if (percentText == "100") {
                                $("#outputImage").show();
                            }
                        }
                    });
                },

                error: function(response, status, e) {
                    alert('Oops something went.');
                },

                success: function(data) {
                    console.log(data);
                    // $("#imgLoader").hide();
                    var obj = JSON.parse(data);
                    $("#up_course_img").attr('src', obj.filePath + obj.fileName);
                    $("#up_course_img").attr('data-imgname', obj.fileName);
                    $("#chang_image").val(obj.fileName);
                }
            });
        }
    });


    $(document).on('click', 'a.delete_img', function() {
        var filename = $("#up_course_img").data('imgname');
        //alert(filename);
        var scope = $(this);
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure?',
            buttons: {
                confirm: function() {
                    $.ajax({
                        url: "<?php echo bs(); ?>users/Users/remove_user_img",
                        type: "POST",
                        data: {
                            'file': filename
                        },
                        beforeSend: function() {
                            $("#imgLoader").show();
                        },
                        success: function(data) {
                            //console.log(data);
                            $("#imgLoader").hide();
                            if (data) {
                                $("#up_course_img").attr('src', '<?php echo bs(); ?>/public/assets/img/logo-bg.png');
                                $("#chang_image").val('');
                            }
                        }
                    });
                    return true;
                },
                cancel: function() {
                    return true;
                }
            }
        });

    });

    $(".browse-tel").on('change', function(e) {
        var getFileName = e.target.files[0].name;
        $(this).children('span.title').text(getFileName);
    })
</script>