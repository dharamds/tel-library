<!-- Notification Modal -->
<div class="modal fade" id="notification_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-share-square" aria-hidden="true"></i> Send
                Notification</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="<?php bs('users/send_notification') ?>" id="send_notification" class="noti-msg">
                <div class="form-role">
                        <label>Subject</label>
                        <input type="text" class="form-control" value="" name="subject"  id="subject" placeholder="Subject" required>
                    </div>
                    <div class="form-role">
                        <label>Notification</label>
                        <textarea name="msg" class="form-control editor" rows="10" id="notification" cols="10" placeholder="Write You Message" required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="email_submit_button" ><i class="fa fa-paper-plane" aria-hidden="true"></i>
                    Send</button>
                </div>
                <input type="hidden" name="selected_emaildata" id="selected_emaildata">
            </form>
        </div>
    </div>
</div>
<!----Notification Modal----->

<script type="text/javascript">
    $(document).ready(function() {  
       
        $("#send_notification").validate({
            
            ignore: [],
            rules: {
                subject: {
                    required: true
                },
                msg: {
                  required: function(textarea) {
                    CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                    var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                    return editorcontent.length === 0;
                  }
                }
            },
            messages: {
                subject: "Please enter subject.", 
                msg: "Please enter message."
            },
            errorPlacement: function (error, $elem) {
                if ($elem.is('textarea')) {
                    $elem.insertAfter($elem.next('div'));
                }
                error.insertAfter($elem);
            },
            submitHandler: function (form) {
                $('#email_submit_button').hide();
                $.ajax({
                    url: "<?php echo bs(); ?>users/send_notification",
                    method: "POST",
                    data: $('form#send_notification').serialize(),
                    complete: function (xhr, status) {
                        CommanJS.getDisplayMessgae(200, 'Email sent successfully.');
                    },
                    success: function (data) {
                        console.log('ttt' +data);
                        $('#email_submit_button').show();
                        $('#notification_modal').on('hidden.bs.modal', function(){                           
                            CKEDITOR.instances.notification.setData("");
                            $('#subject').val('');
                        });

                        $('#notification_modal').modal('hide');                        

                    }
                });
                return false;
            }

        });
    });
</script>
