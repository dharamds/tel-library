      <div class="container-fluid">
         <div data-widget-group="group1">
            <div class="row">
               <div class="col-sm-3">
                  <div class="panel panel-profile panel-grid">
                     <div class="panel-body">
                        <div class="user-profile-img">
                           <img src="<?php bs() ?>uploads/<?php echo $user_profile->user_img ?>" class="img-circle" width="200" alt="">
                        </div>
                        <?php 
                        ?>
                        <div class="name f20"><?php echo $user_profile->first_name; ?> <?php echo $user_profile->last_name; ?></div>
                        <div class="info mt-2"><?php echo $user_profile->email ?></div>
                        <!-- <ul class="list-inline text-center">
                           <li><a href="#" class="profile-facebook-icon"><i class="ti ti-facebook"></i></a></li>
                           <li><a href="#" class="profile-twitter-icon"><i class="ti ti-twitter"></i></a></li>
                           <li><a href="#" class="profile-dribbble-icon"><i class="ti ti-dribbble"></i></a></li>
                        </ul> -->
                     </div>
                  </div>
                 
               </div>
               <!-- col-sm-3 -->
               <div class="col-sm-9">
                  <div class="tab-content">
                  
                     <!-- #tab-projects -->
                     <div class="tab-pane active" id="tab-about">
                        <div class="panel panel-default panel-grid">
                           <div class="panel-heading brd-0 m-0 pt-1"></div>
                           <div class="panel-body">
                              <div class="about-area">
                                 <h4>Personal Information</h4>
                                 <div class="table-responsive">
                                    <table class="table about-table">
                                       <tbody>
                                          <tr>
                                             <th>Full Name</th>
                                             <td><?php echo $user_profile->first_name." ".$user_profile->last_name ?></td>
                                          </tr>
                                          <tr>
                                             <th>Country</th>
                                             <td>
                                                <?php 
                                                   //ipinfo grabs the ip of the person requesting
                                                   
                                                    $getloc = json_decode(file_get_contents("http://ipinfo.io/"));
                                                   
                                                    $country_code = $getloc->country; //to get city
                                                   
                                                    echo $country_name = get_country_name($country_code);
                                                   ?>
                                             </td>
                                          </tr>

                                          <tr>
                                             <th>Email</th>
                                             <td><?php echo $user_profile->email ?></td>
                                          </tr>
                                          <tr>
                                             <th>Mobile</th>
                                             <td><?php echo $user_profile->phone ?></td>
                                          </tr>
                                          <tr>
                                             <th>Created Date</th>
                                             <td><?php echo date('m/d/Y', strtotime($user_profile->date)) ?></td>
                                          </tr>
                                          <tr>
                                             <th>Mobile No</th>
                                             <td><?php echo $user_profile->phone ?></td>
                                          </tr>

                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>                     
                     
                  </div>
                  <!-- .tab-content -->
               </div>
               <!-- col-sm-8 -->
            </div>
         </div>
