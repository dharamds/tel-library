<!-- DataTables CSS library -->
<div class="container-fluid">

    <div data-widget-role="role1">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default panel-grid">
                    <div class="panel-heading"><label for="street_address_1" class="col-md-12 control-label">Parent/Guardian Information</label></div>
                    <div class="panel-body no-padding p-0">
                        <table class="table table-bordered table-striped table-hover" cellspacing="0">
                            <thead>
                                <tr>
                                    <th style="min-width: 140px;">Select All</th>
                                    <th style="min-width: 90px">First Name</th>
                                    <th style="min-width: 200px;">Last Name</th>
                                    <th style="min-width: 250px;">TEL ID Number</th>
                                    <th style="min-width: 250px">Primary Email</th>
                                    <th style="min-width: 250px">Secondary Email</th>
                                    <th style="min-width: 250px">Phone</th>
                                    <th style="min-width: 250px">Cell Phone</th>
                                    <th style="min-width: 250px">Address 1</th>
                                    <th style="min-width: 250px">Address 2</th>
                                    <th style="min-width: 250px">City</th>
                                    <th style="min-width: 250px">State/Province</th>
                                    <th style="min-width: 250px">Zip/Postal</th>
                                    <th style="min-width: 250px">Country</th>
                                    <th style="min-width: 250px">Can view Grads and Notices</th>
                                </tr>
                            </thead>
                            <?php foreach ($parent_data as $data) { ?>
                                <tbody>
                                    <tr>
                                    <th style="min-width: 140px;"><input type="checkbox"></th>
                                    <th style="min-width: 90px"><?php echo $data->first_name ?></th>
                                    <th style="min-width: 200px;"><?php echo $data->last_name ?></th>
                                    <th style="min-width: 250px;"><?php echo $data->unique_id ?></th>
                                    <th style="min-width: 250px"><?php echo $data->email ?></th>
                                    <th style="min-width: 250px"><?php echo $data->secondary_email ?></th>
                                    <th style="min-width: 250px"><?php echo $data->phone ?></th>
                                    <th style="min-width: 250px"><?php echo $data->cellphone ?></th>
                                    <th style="min-width: 250px"><?php echo $data->street_address_1 ?></th>
                                    <th style="min-width: 250px"><?php echo $data->street_address_2 ?></th>
                                    <th style="min-width: 250px"><?php echo $data->city ?></th>
                                    <th style="min-width: 250px"><?php echo $data->state ?></th>
                                    <th style="min-width: 250px"><?php echo $data->zip ?></th>
                                    <th style="min-width: 250px"><?php echo $data->country ?></th>
                                    <th style="min-width: 250px"><?php echo $data->username ?></th>
                                    </tr>
                                </tbody>
                            <?php } ?>
                        </table>
                    </div>
                    <div class="panel-footer"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- .container-fluid -->
</div>
<!-- #page-content -->
</div>