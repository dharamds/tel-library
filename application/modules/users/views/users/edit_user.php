<script src="<?php bs('/public/assets/js/jquery-input-mask-phone-number.js'); ?>"></script>

<div class="container-fluid pr-0">
    <div class="panel panel-default">
        <div class="panel-heading">
            <ul class="nav nav-tabs" role="tablist" id="addLessonTab">
                <li role="presentation" class="active">
                    <a href="#content" class="set_tab" role="tab" data-toggle="tab" data-id="<?php echo $module_id ?>">
                        Content
                    </a>
                </li>
                <li role="presentation">
                    <a href="#user_metadata" id="metadata_id" role="tab" data-id="<?php echo $user_id; ?>" data-toggle="tab"> Metadata </a>
                </li>
                <li role="presentation">
                    <a href="#user_course_information" role="tab" data-id="<?php echo $user_id; ?>" data-toggle="tab"> User Course Information </a>
                </li>

            </ul>
        </div>
        <div class="panel-body">
            <div class="tab-content">

                <!-- Content -->
                <div role="tabpanel" class="tab-pane active" id="content">
                    <?php echo form_open(uri_string(), array('id' => 'user_section_form', 'name' => 'user_section_form', 'class' => 'form-horizontal', 'autocomplete' => 'off')); ?>
                    <?php
                    $hccid_data = [
                        'type' => 'hidden',
                        'id' => 'ccId',
                        'name' => 'ccId',
                        'value' => $user_id
                    ];
                    echo form_input($hccid_data);
                    ?>
                    <div class="form-group col-md-12 p-0 pt-3">

                        <div class="col-md-4 p-0">
                            <label for="fieldname" class="col-md-12 control-label">First Name<span class="field-required">*</span></label>
                            <div class="col-md-12">
                                <?php
                                $first_name['class'] = 'form-control';
                                echo form_input($first_name);
                                ?>
                                <?php echo form_error('first_name', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>


                        <div class="col-md-4 p-0">
                            <label for="fieldname" class="col-md-12 control-label">Middle Name</label>
                            <div class="col-md-12">
                                <?php
                                $first_name['class'] = 'form-control';
                                echo form_input($middle_name);
                                ?>
                            </div>
                        </div>

                        <div class="col-md-4 p-0">
                            <label for="fieldname" class="col-md-12 control-label">Last Name<span class="field-required">*</span></label>
                            <div class="col-md-12">
                                <?php
                                $first_name['class'] = 'form-control';
                                echo form_input($last_name);
                                ?>
                                <?php echo form_error('last_name', '<div class="error">', '</div>'); ?> </div>
                        </div>

                    </div>
                    <div class="form-group col-md-12 p-0">

                        <div class="col-md-4 p-0">
                            <label for="fieldname" class="col-md-12 control-label">Unique ID<span class="field-required">*</span></label>
                            <div class="col-md-12">
                                <?php
                                echo form_input($unique_id);
                                ?>
                            </div>
                        </div>

                        <div class=" col-md-4 p-0">
                            <label for="fieldname" class="col-md-12 control-label">Primary Email<span class="field-required">*</span></label>
                            <div class="col-md-12">
                                <?php
                                echo form_input($email);
                                ?>
                                <?php echo form_error('email', '<div class="error">', '</div>'); ?>
                                <div id="user_mail" style="color: red;font-weight: bold;"></div>
                            </div>
                        </div>
                        <div class="col-md-4 p-0" id="secondary_email_div">
                            <label for="fieldname" class="col-md-12 control-label">Secondary Email</label>
                            <div class="col-md-12">
                                <?php
                                echo form_input($secondary_email);
                                ?>

                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-12 p-0">
                        <div class="col-md-4 p-0">
                            <label for="fieldname" class="col-md-12 control-label">User Role<span class="field-required">*</span></label>
                            <div class="col-md-12">
                                <?php
                                echo form_dropdown('user_role', $roles, $currentroles, ['id' => 'user_role', 'class' => 'form-control']);
                                ?> <?php echo form_error('user_role', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="col-md-4 p-0" id="containers_div" style="display:block;">
                            <label for="fieldname" class="col-md-12 control-label">Associated Institution<span class="field-required">*</span></label>
                            <div class="col-md-12">
                                <?php echo form_dropdown('containers', $containers, $currentcontainers, ['id' => 'containers', 'class' => 'form-control']); ?>
                            </div>
                        </div>

                        <div class="col-md-4 p-0" id="institutional_unique_div" style="display:block;">
                            <label for="fieldname" class="col-md-12 control-label">Institutional ID Number</label>
                            <div class="col-md-12">
                                <?php echo form_input($institutional_unique_id);
                                ?>
                            </div>
                        </div>

                    </div>
                    <div class="form-group col-md-12 p-0">
                        <div class="col-md-4 p-0" id="phone_div">
                            <label for="fieldname" class="col-md-12 control-label">Phone Number</label>
                            <div class="col-md-12">
                                <?php
                                echo form_input($phone);
                                ?>

                            </div>
                        </div>
                        <div class="col-md-4 p-0" id="cellphone_div">
                            <label for="fieldname" class="col-md-12 control-label">Cell Phone Number</label>
                            <div class="col-md-12">
                                <?php
                                echo form_input($cellphone);
                                ?>
                            </div>
                        </div>

                        <div class="col-md-4 p-0" id="timezone_div">
                            <label for="fieldname" class="col-md-12 control-label">Time Zone</label>
                            <div class="col-md-12">
                                <?php
                                echo form_dropdown('time_zone', $timezones, $user->time_zone, ['id' => 'time_zone', 'class' => 'form-control']);
                                ?>
                            </div>
                        </div>
                    </div>


                    <div class="form-group col-md-12 p-0">
                        <div class="col-md-4 p-0" id="standing_year_div">
                            <label for="fieldname" class="col-md-12 control-label">Standing/Year</label>
                            <div class="col-md-12">
                                <?php
                                echo form_dropdown('standing_year', array_combine(range(date('Y'), date('Y') - 20), range(date('Y'), date('Y') - 20)), $user->standing_year, ['class' => 'form-control']);
                                ?>

                            </div>
                        </div>
                        <div class="col-md-4 p-0" id="dob_div">
                            <label for="fieldname" class="col-md-12 control-label">Date of Birth</label>
                            <div class="col-md-12">
                                <?php
                                echo form_input($dob);
                                ?>
                            </div>
                        </div>
                        <div class="col-md-4 p-0" id="dob_div">
                        </div>
                    </div>
                    <div class="col-md-12 hr-line mt-5 mb-5"></div>
                    <!-- Status-->
                    <div class="form-group col-md-12 p-0">
                        <div class="col-md-4 ">
                            <label for="street_address_1" class="control-label mr-5">Status</label>
                            <?php
                            if (isset($user->active)) {
                                if ($user->active == 1) {
                                    $active_status = TRUE;
                                } else {
                                    $active_status = FALSE;
                                }
                                if ($user->active == 0) {
                                    $inactive_status = TRUE;
                                } else {
                                    $inactive_status = FALSE;
                                }
                            } else {
                                $active_status = TRUE;
                                $inactive_status = FALSE;
                            }


                            echo form_radio('active', '1', $active_status);
                            ?> Active
                            <?php echo form_radio('active', '0', $inactive_status); ?> Inactive

                        </div>
                        <div class="col-md-4 ">
                            <?php if ($user_id > 0) : ?>
                                <a class="btn btn-success send_email" href="#">
                                    Send Notification to User</a>
                            <?php endif; ?>
                        </div>
                        <div class="col-md-4 ">
                            <?php if ($user_id > 0) : ?>
                                <a class="btn btn-success" href="#">
                                    Swith To User</a>
                            <?php endif; ?>
                        </div>

                    </div>

                    <?php
                    if ($user_role_name == PARENTS) {
                        echo $this->parser->parse('parent_info', $data, true);
                    } elseif ($user_role_name == STUDENT) {
                        echo $this->parser->parse('student_info', $data, true);
                    }
                    ?>
                    <div class="col-md-12 hr-line mt-5 mb-5"></div>
                    <!-- Address box-->
                    <div class="form-group col-md-12 p-0">
                        <div class="col-md-4 p-0">
                            <label for="street_address_1" class="col-md-12 control-label">Street Address 1 </label>
                            <div class="col-md-12">
<?php
$street_address_1['class'] = 'form-control';
echo form_input($street_address_1);
?>
                                <?php echo form_error('street_address_1', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="col-md-4 p-0">
                            <label for="fieldname" class="col-md-12 control-label">Street Address 2 </label>
                            <div class="col-md-12">
<?php
$street_address_2['class'] = 'form-control';
echo form_input($street_address_2);
?>
                                <?php echo form_error('street_address_2', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="col-md-4 p-0">
                            <label for="fieldname" class="col-md-12 control-label">Country</label>
                            <div class="col-md-12">
<?php
$country = [
    "" => "Select Country",
    "India" => "India",
    "United States of America" => "United States of America"
        ]
?>
                                <?php echo form_dropdown('country', $country, $user->country, ['class' => 'form-control']); ?>
                                <?php echo form_error('country', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-12 p-0">

                        <div class="col-md-4 p-0">
                            <label for="fieldname" class="col-md-12 control-label">State / Province / Region </label>
                            <div class="col-md-12">
<?php
echo form_input($state);
?>
                                <?php echo form_error('state', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="col-md-4 p-0">
                            <label for="city" class="col-md-12 control-label">City </label>
                            <div class="col-md-12">
<?php
echo form_input($city);
?>
                                <?php echo form_error('city', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="col-md-4 p-0">
                            <label for="fieldname" class="col-md-12 control-label">Zip / Postal Code</label>
                            <div class="col-md-12">
<?php
$zip['class'] = 'form-control';
echo form_input($zip);
?>
                                <?php echo form_error('zip', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 hr-line mt-5 mb-5"></div>
                    <!-- Note-->
                    <div class="form-group col-md-12 p-0">
                        <div class="col-md-12 p-0" ">
                            <label for=" street_address_1" class="col-md-12 control-label">Notes </label>
                            <div class="col-md-12">

                                <textarea class="form-control" name="note" rows="4" placeholder="Notes"><?php echo $note['value'] ?></textarea>
<?php echo form_error('note', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>
                    </div>
                    <!--security question-->
                    <div class="col-md-12 hr-line mt-5 mb-5"></div>
                    <div class="form-group col-md-12 p-0">
<?php if ($user_id > 0) : ?>
                            <div class="col-md-12 ">
                                <a class="btn btn-success pull-right" id="sendResetEmail" href="<?php echo bs('/users/auth/forgot_password'); ?>">
                                    Send Password Reset Email</a>
                            </div>
<?php endif; ?>
                        <div class="col-md-12 p-0">
                            <label for="fieldname" class="col-md-12 control-label">Security Question</label>
                            <div class="col-md-5">

<?php echo form_dropdown('security_question', $questions, $user->security_question, ['class' => 'form-control']); ?>
                                <?php echo form_error('security_question', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="col-md-7 ">
<?php
$security_ans['class'] = 'form-control';
echo form_input($security_ans);
?>
                                <?php echo form_error('security_ans', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12 hr-line mt-5 mb-5"></div>
                    <div class="form-group col-md-12 p-0">
                        <div class="col-md-4 p-0">
                            <label for="fieldname" class="col-md-12 control-label">New Password</label>
                            <div class="col-md-12">
<?php echo form_input($password); ?>
                            </div>
                        </div>
                        <div class="col-md-4 p-0">
                            <label for="fieldname" class="col-md-12 control-label">Confirm Password</label>
                            <div class="col-md-12">
<?php echo form_input($password_confirm); ?>
                            </div>
                        </div>
<?php if ($user_id > 0) : ?>
                            <div class="col-md-4 p-0">
                                <label for="fieldname" class="col-md-12 control-label">&nbsp;</label>
                                <div class="col-md-12">
                                    <button style="margin-top:7px;" class="btn btn-success" id="Generate_Password">Reset Password</button>
                                </div>
                                <div class="col-md-6 passwordGenerated " id="generatePasswordSpan"></div>
                            </div>
<?php endif; ?>
                    </div>

<?php //endif; 
?>
                    <div class="col-md-12 hr-line mt-5 mb-5">
                        <div class="error" id="error_response"></div>
                    </div>
                    <div class="col-md-12 text-right">
<?php
$btn_name = "Save";
if (!empty($user_id)) {
    $btn_name = "Save Changes";
}
?>
                        <input type="submit" id="submitButton" class="finish btn-primary btn" value="<?php echo $btn_name; ?>">
                    </div>
<?php echo form_close(); ?>

                </div>
                <!--// Metadata Tab Start  //-->
                <div role="tabpanel" class="tab-pane" id="user_metadata">


                    <div class="pl-4 col-md-12">
                        <div class="col-md-6 pl-0 pt-5">
                            <div class="panel panel-grey">
                                <div class="panel-heading mb-3">
                                    <h2><span>User Categories</span></h2>
                                </div>
                                <div class="panel-body panel-collapse-body" id="users_categories_section">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 pl-0 pt-5">
                            <div class="panel panel-grey">
                                <div class="panel-heading">
                                    <h2><span>User Tags</span></h2>
                                    <div class="button-icon-bg">
                                    </div>
                                </div>
                                <div class="panel-body " id="users_tags_section">

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="hr-line col-md-12"></div>

                    <div class="pl-4 col-md-12">
                        <div class="col-md-6 pl-0 pt-5">
                            <div class="panel panel-grey">
                                <div class="panel-heading mb-3">
                                    <h2><span>System Categories</span></h2>
                                </div>
                                <div class="panel-body panel-collapse-body" id="system_categories_section">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 pl-0 pt-5">
                            <div class="panel panel-grey">
                                <div class="panel-heading">
                                    <h2><span>System Tags</span></h2>
                                    <div class="button-icon-bg">
                                    </div>
                                </div>
                                <div class="panel-body" id="system_tags_section">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div role="tabpanel" class="tab-pane" id="user_course_information">
<?php
if ($currentroles == INSTRUCTOR) {
    echo $this->parser->parse('instructor_information', $data, true);
} elseif ($currentroles == PARENTS) {
    echo $this->parser->parse('parent_viewer_information', $data, true);
} elseif ($currentroles == GRADER) {
    echo $this->parser->parse('grader_set', $data, true);
} elseif ($currentroles == GROUP_LEADER) {
    echo $this->parser->parse('group_leader_view', $data, true);
} elseif ($currentroles == STUDENT) {
    echo $this->parser->parse('student_view', $data, true);
}
?>

                    <?php //echo $this->parser->parse('user_course_information', $data, true); 
                    ?>
                    <?php //echo $this->parser->parse('parent_viewer_information', $data, true); 
                    ?>
                    <?php //echo $this->parser->parse('grader_set', $data, true); 
                    ?>
                    <?php //echo $this->parser->parse('group_leader_view', $data, true); 
                    ?>
                    <?php //echo $this->parser->parse('student_view', $data, true); 
                    ?>
                </div>
            </div>

        </div>
<?php echo $this->parser->parse('send_notification', $data, true); ?>
        <script type='text/javascript' src="<?php echo base_url('public\assets\js\users\add_user.js'); ?>"></script>
        <?php
        $jsVars = [
            'ajax_call_root' => base_url(),
            'states' => $states,
        ];
        ?>
        <script>

            $(document).ready(function () {
                setTimeout(function () {
                    document.getElementById('security_ans').value = "";
                    $("#security_ans").removeClass("valid");
                    document.getElementById('password').value = "";
                    $("#password").removeClass("valid");
                }, 1100);
                set_element_by_role($('#user_role').val());
                $('#user_role').change(function () {
                    set_element_by_role($(this).val());
                });

                $("#user_section_form").validate({
                    rules: {
                        first_name: {
                            required: true,
                            noSpace: true
                        },
                        last_name: {
                            required: true,
                            noSpace: true
                        },
                        unique_id: {
                            required: true,

                        },
                        email: {
                            required: true,
                            emailExt: true

                        },
                        password: {
                            minlength: 8,
                        },

                        password_confirm: {
                            minlength: 8,
                            equalTo: "#password",
                        },
                        user_role: {
                            required: true,
                            min: 1
                        }
                    },
                    messages: {
                        first_name: {
                            required: "Please enter first name"
                        },
                        last_name: {
                            required: "Please enter last name"
                        },
                        unique_id: {
                            required: "Please enter unique id"
                        },
                        email: {
                            required: "Please enter email",
                        },
                        user_role: {
                            required: "Please select role",
                            min: "Please select role",
                        },
                        password: {
                            minlength: "Please enter atleast eight characters",
                        },
                        password_confirm: {
                            minlength: "Please enter atleast eight characters",
                            equalTo: "Password do not match ! Enter same password"
                        },

                    },
                    submitHandler: function (form) {

                        var form = $('#user_section_form');
                        var url = form.attr('action');
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: form.serialize(), // serializes the form's elements.
                            beforeSend: function () {
                                $("#demoskylo").trigger("click");
                            },
                            success: function (data) {
                                data = JSON.parse(data);
                                CommanJS.getDisplayMessgae(200, 'User saved successfully.');
                                $('.nav-tabs a[href="#user_metadata"]').tab('show');
                                $('.nav-tabs a[href="#user_metadata"]').attr('data-id', data.user_id);
                                $('#ccId').val(data.user_id);
                                $(window).scrollTop(0);
                                getMetadata(data.user_id);
                            }
                        });

                        return false;
                    }
                });

                function set_element_by_role(role_id) {
                    // hide all additional fields
                    $('#containers_div').css('display', 'none');
                    $('#institutional_unique_div').css('display', 'none');
                    $('#secondary_email_div').css('display', 'none');
                    $('#phone_div').css('display', 'none');
                    $('#cellphone_div').css('display', 'none');
                    $('#timezone_div').css('display', 'none');
                    $('#standing_year_div').css('display', 'none');
                    $('#dob_div').css('display', 'none');

                    // roles for SITE_MANAGER ETC
                    if ($.inArray(role_id, ['0', '<?php echo SITE_MANAGER; ?>', '<?php echo INSTRUCTOR; ?>', '<?php echo GRADER; ?>', '<?php echo GROUP_LEADER; ?>', '<?php echo STUDENT; ?>', '<?php echo PARENTS; ?>']) > 0) {
                        if ($.inArray(role_id, ['0', '<?php echo GRADER; ?>']) == -1) {
                            $('#containers_div').css('display', 'block').ready(function () {
                                $('#user_section').validate();
                                $('#containers').rules('add', {
                                    required: true,
                                    min: 1,
                                    messages: {
                                        required: "Please select institution.",
                                        min: "Please select institution.",
                                    }
                                });
                            });
                        }
                        $('#phone_div').css('display', 'block');
                        $('#secondary_email_div').css('display', 'block');
                        $('#cellphone_div').css('display', 'block');
                        $('#timezone_div').css('display', 'block');
                    }
                    // roles for SITE_MANAGER ETC
                    // roles for STUDENT
                    if ($.inArray(role_id, ['0', '<?= STUDENT; ?>']) > 0) {
                        $('#institutional_unique_div').css('display', 'block');
                        $('#standing_year_div').css('display', 'block');
                        $('#dob_div').css('display', 'block');
                    }
                    // roles for STUDENT
                }

                $('#phone').usPhoneFormat();
                $('#cellphone').usPhoneFormat();
            });

            $(document).ready(function () {
                $("input").attr("autocomplete", "off");
                var dateToday = new Date();
                var dateFormat = "mm/dd/yy",
                        from = $("#dob").datepicker({
                    changeYear: true,
                    changeMonth: true,
                    numberOfMonths: 1,
                    maxDate: dateToday,
                    yearRange: "-60:+0",
                });

                Users.init(<?php echo json_encode($jsVars); ?>);
            });

            $('.passwordGenerated').hide();
            $('#Generate_Password').click(function (e) {
                e.preventDefault();
                $("#password").rules("add", "required");
                $("#password_confirm").rules("add", "required");
                $("#user_section_form").validate(); //sets up the validator
                $("#user_section_form").submit(); //sets up the validator

            });
            $('#submitButton').click(function (e) {
                $("#password").rules("remove", "required");
                $("#password_confirm").rules("remove", "required");
                $("#user_section_form").validate(); //sets up the validator
            });



            function randomPassword(length) {
                var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
                var pass = "";
                for (var x = 0; x < length; x++) {
                    var i = Math.floor(Math.random() * chars.length);
                    pass += chars.charAt(i);
                }
                return pass;
            }

            $('#metadata_id').click(function () {

                var user_id = $('#ccId').val();

                if (user_id != '') {
                    getMetadata(user_id);
                } else {
                    return false;
                }
            });

            function getMetadata(userId) {

                // Glossary Category  13
                CommanJS.getCatSection(<?= META_USER; ?>, 'users_categories_section', 'users_categories', userId, <?= META_USER; ?>);
                // Glossary tags 
                CommanJS.getTagSection(<?= META_USER; ?>, 'users_tags_section', 'users_tags', userId, <?= META_USER; ?>);
                // System Category  1
                CommanJS.getCatSection(<?= META_SYSTEM; ?>, 'system_categories_section', 'system_categories', userId, <?= META_USER; ?>);
                // System tags  1
                CommanJS.getTagSection(<?= META_SYSTEM; ?>, 'system_tags_section', 'system_tags', userId, <?= META_USER; ?>);
            }

            $(document).on('click', '.send_email', function (event) {

                event.preventDefault();
                /* Act on the event */
                var id = $('#ccId').val();
                var first_name = $('#first_name').val();
                var middle_name = $('#middle_name').val();
                var last_name = $('#last_name').val();
                var email = $('#email').val();
                var name = first_name + ' ' + middle_name + ' ' + last_name;
                var selected_emaildata = name + '~' + email + '~' + id;

                $('#selected_emaildata').val(selected_emaildata);
                if (selected_emaildata == "") {
                    CommanJS.getDisplayMessgae(400, 'Please select atleast one user.')
                    return false;
                }

                $('#notification_modal').modal('show');

            });
            $(document).on('click', '#sendResetEmail', function (event) {

                event.preventDefault();

                var email = $('#email').val();
                var url = '<?php echo bs("/users/auth/forgot_password") ?>';
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        'identity': email,
                        'type': 'ajax'
                    }, // serializes the form's elements.
                    dataType: "json",
                    success: function (data) {

                        if (data.status == 200) {
                            CommanJS.getDisplayMessgae(200, 'Password reset link sent succesfully.');
                        } else {
                            CommanJS.getDisplayMessgae(400, 'Password reset link could not be send , please try again.');
                        }
                    }
                });
            });
        </script>