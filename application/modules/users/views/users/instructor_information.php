<!-- DataTables CSS library -->
<div class="container-fluid">
    <div class="form-group col-md-12 p-0 pt-3">
        <div class="col-md-4 p-0">
            <label for="fieldname" class="col-md-12 control-label">First Name<span class="field-required">*</span></label>
            <div class="col-md-12">
                <?php
                $first_name['class'] = 'form-control';
                echo form_input($first_name);
                ?>
                <?php echo form_error('first_name', '<div class="error">', '</div>'); ?>
            </div>
        </div>


        <div class="col-md-4 p-0">
            <label for="fieldname" class="col-md-12 control-label">Middle Name</label>
            <div class="col-md-12">
                <?php
                $first_name['class'] = 'form-control';
                echo form_input($middle_name);
                ?>
            </div>
        </div>

        <div class="col-md-4 p-0">
            <label for="fieldname" class="col-md-12 control-label">Last Name<span class="field-required">*</span></label>
            <div class="col-md-12">
                <?php
                $first_name['class'] = 'form-control';
                echo form_input($last_name);
                ?>
                <?php echo form_error('last_name', '<div class="error">', '</div>'); ?> </div>
        </div>
    </div>
    <div class="form-group col-md-12 p-0 pt-3">
        <div class="col-md-4 p-0" id="containers_div" style="display:block;">
            <label for="fieldname" class="col-md-12 control-label">Associated Institution<span class="field-required">*</span></label>
            <div class="col-md-12">
                <?php echo form_dropdown('containers', $containers, $currentcontainers, ['id' => 'containers', 'class' => 'form-control']); ?>
            </div>
        </div>
    </div>
    <div data-widget-role="role1">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default panel-grid">
                    <div class="panel-heading"><label for="street_address_1" class="col-md-12 control-label">Current Courses</label></div>
                    <div class="panel-body no-padding p-0">
                        <table id="cc" class="table table-bordered table-striped table-hover" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Course Title</th>
                                    <th>Section</th>
                                    <th>Group Set</th>
                                    <th>Group Name</th>
                                    <th>Group Leader</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default panel-grid">
                    <div class="panel-heading"><label for="street_address_1" class="col-md-12 control-label">Current Course Group</label></div>
                    <div class="panel-body no-padding p-0">
                        <table id="ccg" class="table table-bordered table-striped table-hover" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Course Title</th>
                                    <th>Course Section</th>
                                    <th>End Date</th>
                                    <th>Start Date</th>
                                    <th>No.of Enrolled Students</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default panel-grid">
                    <div class="panel-heading"><label for="street_address_1" class="col-md-12 control-label">Current Institution Group</label></div>
                    <div class="panel-body no-padding p-0">
                        <table id="cig" class="table table-bordered table-striped table-hover" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Group Set</th>
                                    <th>Group Name</th>
                                    <th>Group Leader</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- .container-fluid -->
</div>
<!-- #page-content -->
</div>
<script>
    $(document).ready(function() {
        var table = $('#cc').DataTable({
            // Processing indicator
            "processing": true,
            // DataTables server-side processing mode
            "serverSide": true,
            // Initial no order.
            "iDisplayLength": 10,
            "bPaginate": true,
            "order": [],
            // Load data from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('users/current_courses/'); ?>",
                "type": "POST",
                "data": function(data) {
                    data.active = $('#select_status option:selected').val();
                },
            },
            //Set column definition initialisation properties
            "columnDefs": [],
            "columns": [{
                    "data": "course_name"
                },
                {
                    "data": "section_name"
                },
                {
                    "data": "group_name"
                },
                {
                    "data": "group_name"
                },
                {
                    "data": "group_leader"
                }
            ]
        });
        
        
        var table = $('#ccg').DataTable({
            // Processing indicator
            "processing": true,
            // DataTables server-side processing mode
            "serverSide": true,
            // Initial no order.
            "iDisplayLength": 10,
            "bPaginate": true,
            "order": [],
            // Load data from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('users/current_course_group/'); ?>",
                "type": "POST",
                "data": function(data) {
                    data.active = $('#select_status option:selected').val();
                    data.user_id = <?=$user_id?>
                },
            },
            //Set column definition initialisation properties
            "columnDefs": [],
            "columns": [
                {
                    "data": "course_name"
                },
                {
                    "data": "section_name"
                },
                {
                    "data": "course_expiry"
                },
                {
                    "data": "course_created"
                },
                {
                    "data": "enrolled_students"
                }
            ]
        });

        var table = $('#cig').DataTable({
            // Processing indicator
            "processing": true,
            // DataTables server-side processing mode
            "serverSide": true,
            // Initial no order.
            "iDisplayLength": 10,
            "bPaginate": true,
            "order": [],
            // Load data from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('users/current_institution_group/'); ?>",
                "type": "POST",
                "data": function(data) {
                    data.active = $('#select_status option:selected').val();
                },
            },
            //Set column definition initialisation properties
            "columnDefs": [],
            "columns": [{
                    "data": "group_name"
                },
                {
                    "data": "group_name"
                },
                {
                    "data": "user_name"
                }
            ]
        });
    });
</script>