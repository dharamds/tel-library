<!-- DataTables CSS library -->
<div class="container-fluid">

    <div class="row">
        <div class="col-md-12">
            <div class="filter-container flex-row">
                <div class="flex-col-sm-3">
                    <?php echo form_dropdown('container_id', $institutions, '', 'class="selected_condition form-control" id="selected_container" '); ?>
                </div>
                <div class="flex-col-sm-2">
                    <select id="selected_course" name="course_id" class="selected_condition form-control">
                        <option value="0"> --Course--</option>
                    </select>
                </div>

                <div class="flex-col-sm-2">
                    <select id="selected_section" name="section_id" class="selected_condition form-control">
                        <option value="0"> --Section--</option>
                    </select>
                </div>

                <div class="flex-col-sm-2">
                    <?php echo form_dropdown('role_id', $roles, '', 'class="selected_condition form-control" id="selected_roles"'); ?>
                </div>
                <div class="flex-col-sm-2">
                    <select id="select_status" class="selected_condition form-control">
                        <option value="2"> --Select Status--</option>
                        <option value="1">Active</option>
                        <option value="0">Inactive</option>
                    </select>
                </div>
                <div class="flex-col-sm-1">
                    <a class="btn btn-danger pt-2" href="<?= base_url('users') ?>"><i class="flaticon-close-1 f21 fw100"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div data-widget-role="role1">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default panel-grid">
                    <div class="panel-heading">
                        <div class="flex-row">
                            <form method="post" action="<?php echo base_url('users/download_csv') ?>" id="export_user_form">
                                <input type="hidden" name="check_users" id="check_users">
                                <div class="flex-col">
                                    <?php if ($this->ion_auth->is_admin() || $permissions->add_per) : ?>
                                        <a class="btn btn-primary" href="<?= base_url('users/create_user') ?>">New</a>
                                    <?php endif; ?>
                                    <button type="button" class="btn btn-success" id="bulk_export">
                                        Download CSV of Selected Users
                                    </button>
                                    <button type="button" class="btn btn-success" id="bulk_export_all">
                                        Download CSV of All Users
                                    </button>
                                    <?php if ($this->ion_auth->is_admin() || $permissions->delete_per) : ?>
                                        <button type="button" class="btn btn-danger delete_selected">
                                            Delete Selected
                                        </button>
                                    <?php endif; ?>
                                    <a class="btn btn-warning send_email" href="#">
                                        Notify</a>
                                    <a class="btn btn-primary send_message" href="#">
                                        Message</a>
                                    <?php if ($this->ion_auth->is_admin() || $permissions->add_per) : ?>
                                        <a class="btn btn-info" href="#" id="bulk_upload">
                                            Bulk Upload
                                        </a>
                                    <?php endif; ?>

                                    <!-- <a class="btn btn-info" href="< //?= bs('users/print_with_dompdf') ?>">
                                                <i class="fa fa-print"></i>
                                            </a> -->
                                </div>
                            </form>
                            <div class="flex-col-auto">
                                <div class="panel-ctrls" id="manage_user_control"></div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body no-padding p-0">
                        <table id="memListTable" class="table table-bordered table-striped table-hover" cellspacing="0">
                            <thead>
                                <tr>
                                    <th style="min-width: 50px;"><label class="checkbox-tel"><input type="checkbox" class="select_all"></label></th>
                                    <th style="min-width: 140px;">Action</th>
                                    <th style="min-width: 90px">Status</th>
                                    <th style="min-width: 200px;">Name</th>
                                    <th style="min-width: 250px;">Institutional Email</th>
                                    <th style="min-width: 250px">Institution</th>
                                    <th style="min-width: 250px">Enrolled Courses</th>
                                    <th style="min-width: 200px">Enrolled Section</th>
                                    <th style="min-width: 120px">Date Enrolled</th>
                                    <th style="min-width: 180px">Parent</th>
                                    <th style="min-width: 110px">Roles</th>

                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="panel-footer"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- .container-fluid -->
</div>
<!-- #page-content -->
</div>
<?php echo $this->parser->parse('send_notification', $data, true); ?>
<?php echo $this->parser->parse('send_messages', $data, true); ?>
<script>
    $(document).ready(function() {   
        $("#bulk_upload").click(function() {
            CommanJS.getPopupWithExternalView('Bulk Upload',
                "<?php echo base_url('containers/schoolsUsers/getUploadUserView'); ?>");
        });

        $("#bulk_export").click(function() {

var selected_user_id = "";

$('input[name="user_id[]"]:checked').each(function() {
    selected_user_id = this.value + '|' + selected_user_id;
});

if (selected_user_id != "") {
    $('#check_users').val(selected_user_id);
} else {
    CommanJS.getDisplayMessgae(400, 'Please select atleast one user.')
    return false;
}

$('#export_user_form').submit();

});

$("#bulk_export_all").click(function() {
$('#export_user_form').submit();
});


        $(".delete_selected").click(function() {

            var selected_user_id = "";

            $('input[name="user_id[]"]:checked').each(function() {
                selected_user_id = this.value + '|' + selected_user_id;
            });

            // alert(selected_user_id);
            if (selected_user_id != "") {
                $.confirm({
                    title: 'Confirm!',
                    content: 'Are you sure want to delete?',
                    buttons: {
                        confirm: function() {
                            $.ajax({
                                url: "<?php echo base_url('users/delete_users') ?>",
                                method: "POST",
                                data: {
                                    selected_user_id: selected_user_id
                                },
                                success: function(data) {
                                    CommanJS.getDisplayMessgae(400,
                                        'Record has been deleted.');
                                    table.search('').draw();
                                    return true;
                                }
                            });
                        },
                        cancel: function() {
                            return true;
                        }
                    }
                })
            } else {

                CommanJS.getDisplayMessgae(400, 'Please select atleast one user.')
                return false;

            }

        });

        $(".select_all").click(function() {
            $(".selct_class").prop('checked', $(this).prop('checked'));
        });
        var webroot = "<?php echo base_url(); ?>";
        var table = $('#memListTable').DataTable({
            // Processing indicator
            "processing": true,
            // DataTables server-side processing mode
            "serverSide": true,
            // Initial no order.
            "iDisplayLength": 10,
            "bPaginate": true,
            "order": [],
            "drawCallback": function(settings) {
                $('.load_course_data').each(function(key, item) {
                    $.getJSON(webroot+ "containers/schoolsUsers/get_enroll_courses/" + $(this).attr('id'), function(data) {
                           if(data) $("#"+data.typeid).html(data.value);
                    });
                });
                $('.load_section_data').each(function(key, item) {
                    $.getJSON(webroot+ "containers/schoolsUsers/get_enroll_section/" + $(this).attr('id'), function(data) {
                           if(data) $("#"+data.typeid).html(data.value);
                    });
				});
            },
            // Load data from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('users/getlists/'); ?>",
                "type": "POST",
                "data": function(data) {
                    data.active = $('#select_status option:selected').val();
                    data.roles_selected = $('#selected_roles option:selected').val();
                    data.container_selected = $('#selected_container option:selected').val();
                    data.course_selected = $('#selected_course option:selected').val();
                    data.section_selected = $('#selected_section option:selected').val();
                    data.<?php echo $this->security->get_csrf_token_name(); ?> =
                        "<?php echo $this->security->get_csrf_hash(); ?>";
                },
            },
            //Set column definition initialisation properties
            "columnDefs": [{
                    "targets": 0,
                    "data": null,
                    "orderable": false,
                    "render": function(data, type, full, meta) {
                        var data = '';
                        if (type === 'display') {
                            data =
                                '<label class="checkbox-tel"><input type="checkbox" name="user_id[]" class="selct_class" value="' + full['id'] + '" data-userdata="' + full['user_name'] + '~' + full['email'] + '~' + full['id'] + '"></label>';
                        }
                        return data;
                    }
                },
                {
                    "targets": [6],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                            data = '<span id="' + full['id'] + '_course"  class="load_course_data"> Loading...</span>';
                        }
                        return data;
                    }
                },
                {
                    "targets": [7],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                            data = '<span id="' + full['id'] + '_section"  class="load_section_data"> Loading...</span>';
                        }
                        return data;
                       
                        //return "NA";
                    }
                },
                {
                    "targets": 10,
                    "data": null,
                    "orderable": true,
                    "render": function(data, type, full, meta) {
                        var data = '';
                        if (type === 'display') {
                            $.each(full['roles'].split('|'), function(index, value) {
                                data +=
                                    '<span">' +
                                    value + '</span>';
                            });

                        }
                        return data;
                    }
                },
                {
                    "targets": 2,
                    "data": null,
                    "render": function(data, type, full, meta) {
                        if (type === 'display') {
                            if (full['active'] == 1) {
                                data =
                                    '<a href="<?php echo base_url('users/auth/deactivate/'); ?>' +
                                    full['id'] + '"  data-user-id="' + full['id'] +
                                    '" class="btn btn-success btn-xs btn-round-25 btn-block"><?php echo lang('index_active_link'); ?></a>';
                            } else {
                                data = '<a href="<?php echo base_url('users/activate/'); ?>' + full[
                                        'id'] + '"  data-user-id="' + full['id'] +
                                    '" class="btn btn-danger btn-xs btn-round-25 btn-block active_btn"><?php echo lang('index_inactive_link'); ?></a>';
                            }
                        }
                        return data;
                    }
                },
                {
                    "targets": 1,
                    "orderable": false,
                    "data": null,
                    "render": function(data, type, full, meta) {
                        if (type === 'display') {
                            data =
                                '<a style="margin-left:8px;" data-toggle="tooltip"  data-placement="top"  title="Send Notification" href="#" data-user-id="' + full['id'] + '" class="send_email btn btn-info btn-sm" data-userdata="' + full['user_name'] + '~' + full['email'] + '~' + full['id'] + '" data-toggle="modal" data-target="#message_modal"><i class="fa fa-paper-plane"  aria-hidden="true"></i></a>';
                            <?php if ($this->ion_auth->is_admin() || $permissions->edit_per) : ?>
                                data += '<a data-toggle="tooltip" title="edit user" class="btn btn-primary btn-sm" href="<?= base_url('users/edit_user/') ?>' + full['id'] + '"><i class="ti ti-pencil"></i></a>';
                            <?php
                        endif;
                        if ($this->ion_auth->is_admin() || $permissions->delete_per) :
                            ?>
                                data +=
                                    '<a data-toggle="tooltip" title="delete user" href="<?= base_url('users/delete_user/') ?>' +
                                    full['id'] +
                                    '" class="btn btn-danger btn-sm delete_item"><i class="ti ti-trash"></i></a>';
                            <?php
                        endif;
                        if ($this->ion_auth->is_admin() || $permissions->view_per) :
                            ?>
                                //data +=
                                //    '<a data-toggle="tooltip" title="view user" href="<?= base_url('users/view_user/') ?>' +
                                //    full['id'] +
                                //    '" class="btn btn-warning btn-sm"><i class="ti ti-eye"></i></a>';
                            <?php endif; ?>
                        }
                        return data;
                    }

                }
            ],
            "columns": [{
                    "data": "id"
                },
                {
                    "data": "id"
                },
                {
                    "data": "id"
                },
                {
                    "data": "user_name"
                },
                {
                    "data": "email"
                },
                {
                    "data": "institution", "visible": <?php echo (!$current_countainer_id) ? 'true' : 'false'; ?>
                },
                {
                    "data": "course_name"
                },
                {
                    "data": "course_section_name"
                },
                {
                    "data": "date"
                },
                {
                    "data": "parent"
                },
                {
                    "data": "roles"
                },
            ]
        }, );

        $('.selected_condition').on('change', function() {
            table.search('').draw();
        });

        //load courses
        $('#selected_container').on('change', function() {
            $.ajax({
                type: "POST",
                url: "<?php bs('/users/get_db_cources'); ?>",
                data: {
                    "container_id": $(this).val()
                },
                success: function(response) {
                    var options = '<option>--Course--</option>';
                    $.each(JSON.parse(response), function(key, value) {
                        options = options + "<option value=" + value.id + ">" + value.name + "</option>";
                    });
                    $("#selected_course").html(options);
                }
            });
        });
        <?php if ($current_countainer_id) : ?>
            $('#selected_container').val('<?php echo $current_countainer_id; ?>').trigger('change');
        <?php endif; ?>

        //load sections
        $('#selected_course').on('change', function() {
            $.ajax({
                type: "POST",
                url: "<?php bs('/users/get_db_sections'); ?>",
                data: {
                    "container_id": $('#selected_container option:selected').val(),
                    "course_id": $(this).val()
                },
                success: function(response) {
                    var options = '<option>--Section--</option>';
                    $.each(JSON.parse(response), function(key, value) {
                        options = options + "<option value=" + value.id + ">" + value
                            .name + "</option>";
                    });
                    $("#selected_section").html(options);
                }
            });
        });
        // delete user

        $('[data-toggle="tooltip"]').tooltip();
        $(document).on('click', '.delete_item', function(e) {
            e.preventDefault();
            var scope = $(this);
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure want to delete?',
                buttons: {
                    confirm: function() {
                        $.get(scope.attr("href"), // url
                            function(data, textStatus, jqXHR) { // success callback
                                var obj = JSON.parse(data);
                                if (obj.msg === 'deleted') {
                                    // scope.closest("tr").remove();
                                    table.search('').draw();
                                }
                            });
                        return true;
                    },
                    cancel: function() {
                        return true;
                    }
                }
            });
        });
    });
    $(document).on('click', '.send_email', function(event) {
        event.preventDefault();
        /* Act on the event */
        var id = $(this).data('user-id');
        var selected_emaildata = "";
        
        if(selected_emaildata=='') {
            selected_emaildata=$(this).data('userdata');
        } else {
            selected_emaildata = '';
        }
        if(typeof(id)==='undefined') {
            selected_emaildata = '';
        }
        
        $('input[name="user_id[]"]:checked').each(function(key, val) {
            selected_emaildata = $(this).data('userdata') + '|' + selected_emaildata;

        });
       
        $('#selected_emaildata').val(selected_emaildata);
        if (selected_emaildata == "") {
            CommanJS.getDisplayMessgae(400, 'Please select atleast one user.')
            return false;
        }
        $('#notification_modal').modal('show');
    });

    $(document).on('click', '.send_message', function(event) {
        event.preventDefault();
        var id = $(this).data('user-id');
        var selected_user_ids = "";
        if(selected_user_ids=='') {
            selected_user_ids=$(this).data('userdata');
        } else {
            selected_user_ids = '';
        }
        if(typeof(id)==='undefined') {
            selected_user_ids = '';
        }
            $('input[name="user_id[]"]:checked').each(function() {
                selected_user_ids = this.value + '|' + selected_user_ids;
            });

            if (selected_user_ids != "") {
                $('#selected_user_ids').val(selected_user_ids);
            } else {
                CommanJS.getDisplayMessgae(400, 'Please select atleast one user.')
                return false;
            }
        $('#message_modal').modal('show');
    });



    $(document).on('click', '.active_btn', function(e) {
        var scope = $(this);
        e.preventDefault();
        $.ajax({
            type: "GET",
            url: "<?php echo base_url('users/activate/'); ?>" + scope.attr('data-user-id'),
            data: null,
            success: function(response) {
                var obj = jQuery.parseJSON(response);
                CommanJS.getDisplayMessgae(200, obj.msg)
                scope.text('Active');
                scope.toggleClass('btn-danger');
                scope.toggleClass('btn-success');
                scope.toggleClass('active_btn');
                scope.attr('href', "<?php echo base_url('users/auth/deactivate/'); ?>" + scope.data(
                    'user-id'));
            }
        });

    });

    $(document).ready(function() {
        $("#memListTable_paginate .pagination").click(function() {
            $("html, body").animate({
                scrollTop: 0
            }, "slow");
        });
        var config = { height: 120, allowedContent :true, toolbar: 'short' };
        $('.editor').each(function(e) {
            CKEDITOR.replace(this.id, config);
        });
        
    });
</script>