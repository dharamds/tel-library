<!-- DataTables CSS library -->
<div class="container-fluid">
   
    <div data-widget-role="role1">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default panel-grid">
                    <div class="panel-heading"><label for="student_curent_courses" class="col-md-12 control-label">Current Courses</label></div>
                    <div class="panel-body no-padding p-0">
                        <table id="student_curent_courses" class="table table-bordered table-striped table-hover" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Course Title</th>
                                    <th>Course Section</th>
                                    <th>Current Grade</th>
                                    <th>Final Grade</th>
                                    <th>Last Module Completed</th>
                                    <th>Current Progress Percent</th>
                                    <th>Last Login Date/Time</th>
                                    <th>Course Completed</th>
                                    <th>Mid-Term Exam Grade</th>
                                    <th>Assigned Grader Name</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            
            <div class="col-md-12">
                <div class="panel panel-default panel-grid">
                    <div class="panel-heading"><label for="street_address_1" class="col-md-12 control-label">Completed Courses</label></div>
                    <div class="panel-body no-padding p-0">
                        <table id="student_completed_courses" class="table table-bordered table-striped table-hover" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Course Title</th>
                                    <th>Course Section</th>
                                    <th>Current Grade</th>
                                    <th>Final Grade</th>
                                    <th>Last Module Completed</th>
                                    <th>Current Progress Percent</th>
                                    <th>Last Login Date/Time</th>
                                    <th>Course Completed</th>
                                    <th>Mid-Term Exam Grade</th>
                                    <th>Assigned Grader Name</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            
            <div class="col-md-12">
                <div class="panel panel-default panel-grid">
                    <div class="panel-heading"><label for="street_address_1" class="col-md-12 control-label">Dropped/Unenrolled Courses</label></div>
                    <div class="panel-body no-padding p-0">
                        <table id="dropped_unenrolled_courses" class="table table-bordered table-striped table-hover" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>    
                                    <th>Course Title</th>
                                    <th>Course Section</th>
                                    <th>Current Grade</th>
                                    <th>Final Grade</th>
                                    <th>Last Module Completed</th>
                                    <th>Current Progress Percent</th>
                                    <th>Last Login Date/Time</th>
                                    <th>Course Completed</th>
                                    <th>Mid-Term Exam Grade</th>
                                    <th>Assigned Grader Name</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="panel panel-default panel-grid">
                    <div class="panel-heading"><label for="street_address_1" class="col-md-12 control-label">Current Course Group</label></div>
                    <div class="panel-body no-padding p-0">
                        <table id="ccg" class="table table-bordered table-striped table-hover" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Course Title</th>
                                    <th>Course Section</th>
                                    <th>End Date</th>
                                    <th>Start Date</th>
                                    <th>No.of Enrolled Students</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default panel-grid">
                    <div class="panel-heading"><label for="street_address_1" class="col-md-12 control-label">Current Institution Group</label></div>
                    <div class="panel-body no-padding p-0">
                        <table id="cig" class="table table-bordered table-striped table-hover" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Group Set</th>
                                    <th>Group Name</th>
                                    <th>Group Leader</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- .container-fluid -->
</div>
<!-- #page-content -->
</div>
<script>
    $(document).ready(function() {
        var table = $('#student_curent_courses').DataTable({
            // Processing indicator
            "processing": true,
            // DataTables server-side processing mode
            "serverSide": true,
            // Initial no order.
            "iDisplayLength": 10,
            "bPaginate": true,
            "order": [],
            // Load data from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('users/student_curent_courses/'); ?>",
                "type": "POST",
                "data": function(data) {
                    data.active = $('#select_status option:selected').val();
                    data.user_id = <?=$user_id?>
                },
            },
            //Set column definition initialisation properties
            "columnDefs": [],
            "columns": [{
                    "data": "sr_no",
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                    "autoWidth": true
                },
                {
                    "data": "course_name"
                },
                {
                    "data": "section_name"
                },
                {
                    "data": "current_grade"
                },
                {
                    "data": "final_term_grade"
                },
                {
                    "data": "complete_incomplete"
                },
                {
                    "data": "percent"
                },
                {
                    "data": "last_login"
                },
                {
                    "data": "enrolled_date"
                },
                {
                    "data": "mid_term_grade"
                },
                {
                    "data": "mid_term_grade"
                },
                
            ]
        });

        var table = $('#student_completed_courses').DataTable({
            // Processing indicator
            "processing": true,
            // DataTables server-side processing mode
            "serverSide": true,
            // Initial no order.
            "iDisplayLength": 10,
            "bPaginate": true,
            "order": [],
            // Load data from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('users/student_completed_courses/'); ?>",
                "type": "POST",
                "data": function(data) {
                    data.active = $('#select_status option:selected').val();
                },
            },
            //Set column definition initialisation properties
            "columnDefs": [],
            "columns": [
                {
                    "data": "course_name"
                },
                {
                    "data": "section_name"
                },
                {
                    "data": "group_name"
                },
                {
                    "data": "group_name"
                },
                {
                    "data": "group_name"
                },
                {
                    "data": "course_name"
                },
                {
                    "data": "section_name"
                },
                {
                    "data": "group_name"
                },
                {
                    "data": "group_name"
                },
                {
                    "data": "group_name"
                },
                
            ]
        });
        
        var table = $('#dropped_unenrolled_courses').DataTable({
            // Processing indicator
            "processing": true,
            // DataTables server-side processing mode
            "serverSide": true,
            // Initial no order.
            "iDisplayLength": 10,
            "bPaginate": true,
            "order": [],
            // Load data from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('users/dropped_unenrolled_courses/'); ?>",
                "type": "POST",
                "data": function(data) {
                    data.active = $('#select_status option:selected').val();
                    data.user_id = <?=$user_id?>
                },
            },
            //Set column definition initialisation properties
            "columnDefs": [],
            "columns": [
                {
                    "data": "course_name"
                },
                {
                    "data": "section_name"
                },
                {
                    "data": "current_grade"
                },
                {
                    "data": "final_term_grade"
                },
                {
                    "data": "complete_incomplete"
                },
                {
                    "data": "percent"
                },
                {
                    "data": "last_login"
                },
                {
                    "data": "enrolled_date"
                },
                {
                    "data": "mid_term_grade"
                },
                {
                    "data": "mid_term_grade"
                },
                
            ]
        });

        var table = $('#ccg').DataTable({
            // Processing indicator
            "processing": true,
            // DataTables server-side processing mode
            "serverSide": true,
            // Initial no order.
            "iDisplayLength": 10,
            "bPaginate": true,
            "order": [],
            // Load data from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('users/current_course_group/'); ?>",
                "type": "POST",
                "data": function(data) {
                    data.active = $('#select_status option:selected').val();
                    data.user_id = <?=$user_id?>
                },
            },
            //Set column definition initialisation properties
            "columnDefs": [],
            "columns": [
                {
                    "data": "course_name"
                },
                {
                    "data": "section_name"
                },
                {
                    "data": "course_expiry"
                },
                {
                    "data": "course_created"
                },
                {
                    "data": "enrolled_students"
                }
            ]
        });

        var table = $('#cig').DataTable({
            // Processing indicator
            "processing": true,
            // DataTables server-side processing mode
            "serverSide": true,
            // Initial no order.
            "iDisplayLength": 10,
            "bPaginate": true,
            "order": [],
            // Load data from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('users/current_institution_group/'); ?>",
                "type": "POST",
                "data": function(data) {
                    data.active = $('#select_status option:selected').val();
                },
            },
            //Set column definition initialisation properties
            "columnDefs": [],
            "columns": [{
                    "data": "group_name"
                },
                {
                    "data": "group_name"
                },
                {
                    "data": "user_name"
                }
            ]
        });
    });
</script>