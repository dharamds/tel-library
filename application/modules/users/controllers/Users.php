<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
  Author Salman Iqbal
  Company Parexons
  Date 26/1/2016
 */

class Users extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('form_validation'));
        $this->lang->load('auth');
        $this->load->library('parser');
        $this->load->library('encryption');
        $this->list_permission = '';
        if (!$this->ion_auth->logged_in()) {
            // redirect them to the login page
            redirect('users/auth/login', 'refresh');
        }

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
        $this->load->module('template');
        $this->load->helper(array('html', 'language', 'form', 'country_helper'));
        $this->load->model(array('Users_modal', 'common_model', 'Menu_model'));

        // get controller permissions
        if ($this->current_user_permissions = $this->get_permissions()) {
            $this->add_permission = $this->current_user_permissions->add_per;
            $this->edit_permission = $this->current_user_permissions->edit_per;
            $this->delete_permission = $this->current_user_permissions->delete_per;
            $this->view_permission = $this->current_user_permissions->view_per;
            $this->list_permission = $this->current_user_permissions->list_per;
        }
        // get container_id
        $this->container_id = get_container_id();
    }

    public function index()
    {
        // check permissions
        if (!$this->list_permission && !$this->ion_auth->is_admin()) :
            //$this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect('users/auth', 'refresh');
        endif;
        $data['permissions'] = $this->current_user_permissions;
        //Get All Roles
        $roles = $this->ion_auth->roles()->result();
        $data['roles'][] = '-- Roles --';
        foreach ($roles as $item) {
            $data['roles'][$item->id] = $item->name;
        }
        // Get Institutions
        $data['institutions'][] = '-- Institutions --';
        // set condition for site institutions only
        $container_condition['type'] = 1;

        if ($data['current_countainer_id'] = get_container_id()) :
            $container_condition['id'] = $data['current_countainer_id'];
        endif;

        foreach ($this->common_model->getAllData('containers', 'id, name', '', $container_condition) as $item) {
            $data['institutions'][$item->id] = $item->name;
        }
        //// breadcrumb
        $data['breadcrumb'][] = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][] = ['title' => 'Users', 'link' => '', 'class' => 'active'];

        $data['page'] = 'users/users/view_users';
        $this->template->template_view($data);
    }

    public function listing($role_id = STUDENT)
    {
        // check permissions
        if (!$this->list_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect('users/auth', 'refresh');
        endif;
        $data['permissions'] = $this->current_user_permissions;

        $data['role_id'] = $role_id;
        $data['breadcrumb'][] = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][] = ['title' => ucfirst(get_role_by_id($role_id)), 'link' => '', 'class' => 'active'];

        $data['page'] = 'users/users/view_listings';
        $this->template->template_view($data);
    }

    // list users as per filter
    public function getlistings($role_id)
    {
        // Fetch member's records
        $userData = $this->Users_modal->getListings($role_id, $_POST);
        //gkc on dated 28th May , 2019
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $userData['total'],
            "recordsFiltered" => $userData['total'],
            "data" => $userData['result'],
        );
        // Output to JSON format
        echo json_encode($output);
    }

    // list users as per filter
    public function getUsers()
    { // Fetch member's records
        echo json_encode($this->Users_modal->getUsers());
        exit;
    }

    // list users as per filter
    public function getLists()
    {
        // Fetch member's records
        $userData = $this->Users_modal->getRows($_POST);
        //gkc on dated 28th May , 2019,,,,,,,,,,,,,,,,
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $userData['total'],
            "recordsFiltered" => $userData['total'],
            "data" => $userData['result'],
        );
        // Output to JSON format
        echo json_encode($output);
    }

    // Get cources
    public function get_db_cources()
    {
        echo json_encode($this->Users_modal->get_cources_by_container($this->input->post('container_id')));
    }

    // Get cources
    public function get_db_sections()
    {
        echo json_encode($this->Users_modal->get_sections_by_course($this->input->post('container_id'), $this->input->post('course_id')));
    }
    // Get cources
    public function get_db_groups()
    {
        echo json_encode($this->Users_modal->get_groups_by_section($this->input->post()));
    }
    ////////////////////// Get parents
    public function get_parents()
    {
        echo json_encode($this->Users_modal->get_parents());
    }

    ////////////////////// Get parents
    public function remove_parent($id = null)
    {
        echo json_encode($this->Users_modal->remove_parents($id));
    }

    //////////////////// Add parents
    public function add_parents()
    {
        if ($this->input->post()) {
            $this->form_validation->set_rules('first_name', 'First name', 'trim|required');
            $this->form_validation->set_rules('last_name', 'Last name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|valid_email|required');

            if ($this->form_validation->run() === false) {
                $msg = 'Please Fill The Form Correctly';
                $this->session->set_flashdata('error', $msg);
            } else {
                $username = $this->input->post('email');
                $email = $this->input->post('email');
                $password = DEFAULT_PASSWORD;
                $additional_data = array(
                    'unique_id' => $this->Users_modal->generate_unique_user_id(),
                    'first_name' => $this->input->post('first_name'),
                    'middle_name' => $this->input->post('middle_name'),
                    'last_name' => $this->input->post('last_name'),
                    'date' => date('Y-m-d'),
                    'phone' => '',
                );
                $role[] = PARENT;
                
                if ($parent_id = $this->ion_auth->register($username, $password, $email, $additional_data, $role, $container)) {
                    $msg = $this->ion_auth->messages();
                } else {
                    $userdata = $this->Users_modal->get_details_by_email($email);
                    $parent_id = $userdata->id;
                    $msg = $this->session->set_flashdata('message', $this->ion_auth->errors());
                }
                if ($this->common_model->update_data(['user_id' => $this->ion_auth->user()->row()->id, 'parent_id' => $parent_id], 'user_parents')) {
                    $msg = 'Parent already assigned.';
                    $msg_code = 201;
                } else {
                    $this->common_model->add('user_parents', ['user_id' => $this->ion_auth->user()->row()->id, 'parent_id' => $parent_id]);
                    $msg = 'Parent added successfully.';
                    $msg_code = 200;
                }
            }
        }

        $output = array(
            "msg_code" => $msg_code,
            "msg" => $msg,
        );
        // Output to JSON format
        echo json_encode($output);
    }

    public function multiple_select()
    {
        $arr_role = $this->input->post('roles[]');
        if (empty($arr_role)) :
            $this->form_validation->set_rules('roles[]', 'Select at least one role');
            return false;
        endif;
    }

    // create a new user

    public function create_user()
    {
        // check permissions
        if (!$this->add_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect($_SERVER['HTTP_REFERER'], 'refresh');
        endif;

        if ($this->input->post()) {
            $this->form_validation->set_rules('first_name', 'First name', 'trim|required');
            $this->form_validation->set_rules('last_name', 'Last name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|valid_email|required');
            $this->form_validation->set_rules('user_role', 'role', 'trim|required|greater_than[0]');

            if ($this->form_validation->run() === false) {

                echo json_encode(array("msg" => 'Please Fill The Form Correctly'));
                exit;
            } else {
                
                $first_name = $this->input->post('first_name');
                $last_name = $this->input->post('last_name');
                $middle_name = $this->input->post('middle_name');
                $username = $this->input->post('email');
                $unique_id = $this->input->post('unique_id');
                $email = $this->input->post('email');

                //$password = DEFAULT_PASSWORD;

                $additional_data = array(
                    'unique_id' => $unique_id,
                    'first_name' => $first_name,
                    'middle_name' => $middle_name,
                    'last_name' => $last_name,
                    'date' => date('Y-m-d'),
                    'institutional_unique_id' => $this->input->post('institutional_unique_id'),
                    'secondary_email' => $this->input->post('secondary_email'),
                    'phone' => $this->input->post('phone'),
                    'cellphone' => $this->input->post('cellphone'),
                    'time_zone' => $this->input->post('time_zone'),
                    'standing_year' => $this->input->post('standing_year'),
                    'dob' => ($this->input->post('dob')) ? date('Y-m-d', strtotime($this->input->post('dob'))) : '',
                    'street_address_1' => $this->input->post('street_address_1'),
                    'street_address_2' => $this->input->post('street_address_2'),
                    'country' => $this->input->post('country'),
                    'city' => $this->input->post('city'),
                    'state' => $this->input->post('state'),
                    'zip' => $this->input->post('zip'),
                    'note' => $this->input->post('note'),
                    'security_question' => $this->input->post('security_question'),
                    'security_ans' => $this->input->post('security_ans'),
                    'active' => $this->input->post('active')
                );
                //////////////////////////////
                $role[] = $this->input->post('user_role');
                $container[] = $this->input->post('containers');
            }
            /////////////////////////////    
            $manual_password = ($this->input->post('password')) ? $this->input->post('password') : DEFAULT_PASSWORD;



            if ($user_id = $this->ion_auth->register($username, $manual_password, $email, $additional_data, $role, $container)) {
                // $this->session->set_flashdata('message', $this->ion_auth->messages());
                echo json_encode(array("msg" => $this->ion_auth->messages(), "role_id" => $this->input->post('user_role'), 'user_id' => $user_id));
                exit;
                //redirect('users', 'refresh');
            } else {
                // $this->session->set_flashdata('message', $this->ion_auth->errors());
                echo json_encode(array("msg" => $this->ion_auth->errors()));
                exit;
            }
        }


        $data['identity_column']    = $this->config->item('identity', 'ion_auth');
        $data['unique_id']          = $this->Users_modal->generate_unique_user_id();

        $data['first_name'] = array(
            'name' => 'first_name',
            'id' => 'first_name',
            'type' => 'text',
            'placeholder' => 'First name',
            'value' => $this->form_validation->set_value('first_name', ''),
        );
        $data['middle_name'] = array(
            'name' => 'middle_name',
            'id' => 'middle_name',
            'type' => 'text',
            'class' => 'form-control',
            'placeholder' => 'Middle name',
            'value' => $this->form_validation->set_value('middle_name', ''),
        );
        $data['last_name'] = array(
            'name' => 'last_name',
            'id' => 'last_name',
            'class' => 'form-control',
            'type' => 'text',
            'placeholder' => 'Last name',
            'value' => $this->form_validation->set_value('last_name', ''),
        );

        $data['email'] = array(
            'name' => 'email',
            'id' => 'email',
            'type' => 'email',
            'class' => 'form-control',
            'placeholder' => 'Primary Email',
            'value' => $this->form_validation->set_value('email', ''),
        );
        $data['unique_id'] = array(
            'name' => 'unique_id',
            'id' => 'unique_id',
            'class' => 'form-control',
            'type' => 'text',
            'readonly' => 'readonly',
            'value' => $this->form_validation->set_value('unique_id', $data['unique_id']),
        );
        $data['phone'] = array(
            'name' => 'phone',
            'id' => 'phone',
            'class' => 'form-control',
            'placeholder' => 'Phone',
            'type' => 'text',
            'value' => $this->form_validation->set_value('phone', ''),
        );
        $data['cellphone'] = array(
            'name' => 'cellphone',
            'id' => 'cellphone',
            'class' => 'form-control',
            'type' => 'text',
            'value' => $this->form_validation->set_value('cellphone'),
        );
        $data['institutional_unique_id'] = array(
            'name' => 'institutional_unique_id',
            'id' => 'institutional_unique_id',
            'class' => 'form-control',
            'type' => 'text',
            'value' => $this->form_validation->set_value('institutional_unique_id'),
        );

        $data['secondary_email'] = array(
            'name' => 'secondary_email',
            'id' => 'secondary_email',
            'class' => 'form-control',
            'type' => 'text',
            'value' => $this->form_validation->set_value('secondary_email'),
        );
        $data['active'] = array(
            'name' => 'active',
            'id' => 'active',
            'value' => $this->form_validation->set_value('active'),
        );
        $data['dob'] = array(
            'name' => 'dob',
            'id' => 'dob',
            'class' => 'form-control',
            'type' => 'text',
            'value' => $this->form_validation->set_value('dob'),
        );
        $data['street_address_1'] = array(
            'name' => 'street_address_1',
            'id' => 'street_address_1',
            'class' => 'form-control',
            'type' => 'text',
            'placeholder' => 'Street Address 1',
            'value' => $this->form_validation->set_value('street_address_1'),
        );
        $data['street_address_2'] = array(
            'name' => 'street_address_2',
            'id' => 'street_address_2',
            'class' => 'form-control',
            'type' => 'text',
            'placeholder' => 'Street Address 2',
            'value' => $this->form_validation->set_value('street_address_2'),
        );
        $data['country'] = array(
            'name' => 'country',
            'id' => 'country',
            'class' => 'form-control',
            'placeholder' => 'Country',
            'value' => $this->form_validation->set_value('country'),
        );
        $data['city'] = array(
            'name' => 'city',
            'id' => 'city',
            'class' => 'form-control city',
            'type' => 'text',
            'placeholder' => 'City',
            'autofill' => false,
            'value' => $this->form_validation->set_value('city'),
        );
        $data['state'] = array(
            'name' => 'state',
            'id' => 'state',
            'class' => 'form-control states',
            'type' => 'text',
            'autocomplete' => "nope",
            'placeholder' => 'State',
            'value' => $this->form_validation->set_value('state'),
        );
        $data['zip'] = array(
            'name' => 'zip',
            'id' => 'zip',
            'class' => 'form-control',
            'type' => 'text',
            'placeholder' => 'Zip',
            'value' => $this->form_validation->set_value('zip'),
        );
        $data['note'] = array(
            'name' => 'note',
            'id' => 'note',
            'class' => 'form-control',
            'type' => 'text',

            'value' => $this->form_validation->set_value('note'),
        );
        $data['security_question'] = array(
            'name' => 'security_question',
            'id' => 'security_question',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('security_question'),
        );
        $data['security_ans'] = array(
            'name' => 'security_ans',
            'id' => 'security_ans',
            'class' => 'form-control',
            'type' => 'text',
            'placeholder' => 'Security Answer',
            'value' => ($this->form_validation->set_value('security_ans') ? $this->form_validation->set_value('security_ans') : ''),
        );
        $data['password'] = array(
            'name' => 'password',
            'id' => 'password',
            'type' => 'password',
            'class' => 'form-control',
            'placeholder' => '********',
            'autocomplete' => 'off',
            'value'         => ''
        );
        $data['password_confirm'] = array(
            'name' => 'password_confirm',
            'id' => 'password_confirm',
            'type' => 'password',
            'class' => 'form-control',
            'placeholder' => '********',
            'value'         => ''
        );


        $condition['id !='] = 1;
        if ($this->container_id > 0) {

            $condition['level >'] = currentGroup()->level;
        }
        //$roles = $this->common_model->getAllData('roles', 'id, name', '', $condition, '', '', '', '', 'id', $where_ins);
        $roles = $this->common_model->getAllData('roles', 'id, name', '', $condition);

        $data['roles'][] = '--Select roles--';
        foreach ($roles as $role) {
            $data['roles'][$role->id] = $role->name;
        }
        //// timezones
        $timezones = $this->common_model->getAllData('timezones', '*', '', '');
        $data['timezones'][] = '--Select timezone--';
        foreach ($timezones as $timezone) {
            $data['timezones'][$timezone->id] = $timezone->title . " (" . $timezone->code . ") - " . $timezone->offset;
        }

        $condition = ['status' => 1, 'status_delete' => 1];
        if ($this->container_id > 0) {
            $condition = ['id' => $this->container_id];
        }
        $containers = $this->common_model->getAllData('containers', 'id, name', '', $condition);

        $data['containers'][] = '--Select Container--';

        foreach ($containers as $container) {
            $data['containers'][$container->id] = $container->name;
        }

        $data['currentcontainers'][] = $this->container_id;

        //// breadcrumb
        $data['breadcrumb'][] = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][] = ['title' => 'Users', 'link' => base_url('/users'), 'class' => 'active'];
        $data['breadcrumb'][] = ['title' => 'Add', 'link' => '', 'class' => 'active'];
        $state = $this->common_model->getAllData('states', ['name', 'id'], '', ['status_delete' => 1, 'status' => 1]);
        foreach ($state as $key => $value) {
            $states[] = $value->name;
        }
        $data['states'] = $states;
        /////////////////////////////////////////////////
        $data['questions'][] = '--Select Question--';
        $questions = $this->common_model->getAllData('security_questions', ['id', 'title'], '', ['status' => 1]);
        foreach ($questions as $key => $value) {
            $data['questions'][] = $value->title;
        }
        ///////////////////////////////////////////
        $data['page'] = 'users/users/edit_user';
        $this->template->template_view($data);
    }

    //Delete User
    public function delete_user()
    {
        // check permissions
        if (!$this->delete_permission && !$this->ion_auth->is_admin()) :
            // $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            echo json_encode(['msg' => $this->lang->line('access_denied')]);
            exit;
        endif;

        $id = $this->uri->segment(3);
        if ($this->session->userdata('user_id') == $id) {
            $msg = "You Can't Delete Logged In User";
        }

        $result = $this->ion_auth->delete_user($id);

        if ($result) {
            // $this->session->set_flashdata('message', $this->ion_auth->messages());
            $msg = 'deleted';
        } else {
            // $this->session->set_flashdata('message', $this->ion_auth->errors());
            $msg = 'error';
        }
        echo json_encode(['msg' => $msg]);
        exit;
    }

    public function update_user_metadata()
    {
        $user_id = $this->input->post('user_id');
        $role_id = $this->common_model->getDataById('users_roles', 'role_id', ['user_id' => $user_id])->role_id;
        if (in_array($role_id, [SITE_MANAGER, INSTRUCTOR, GRADE_ADMINISTRATOR, GRADER, STUDENT, PARENTS])) {
            $data = array(
                'institutional_unique_id' => $this->input->post('institutional_unique_id'),
                'secondary_email' => $this->input->post('secondary_email'),
                'phone' => $this->input->post('phone'),
                'cellphone' => $this->input->post('cellphone'),
                'time_zone' => $this->input->post('time_zone'),
                'standing_year' => $this->input->post('standing_year'),
                'dob' => ($this->input->post('dob')) ? date('Y-m-d', strtotime($this->input->post('dob'))) : ''
            );
            $container_id = $this->input->post('containers');

            if ($container_id && $role_id) {
                $this->ion_auth->remove_from_container('', $user_id);
                $this->ion_auth->add_to_container($container_id, $role_id, $user_id);
            }
        } else {
            $data = array(
                'institutional_unique_id' => '',
                'secondary_email' => '',
                'phone' => '',
                'cellphone' => '',
                'time_zone' => '',
                'standing_year' => '',
                'dob' => ''
            );

            $container_id = $this->input->post('containers');

            if ($container_id && $role_id) {
                $this->ion_auth->remove_from_container('', $user_id);
                //$this->ion_auth->add_to_container($container_id, $role_id, $user_id);
            }
        }

        if ($this->ion_auth->update($user_id, $data)) {
            echo json_encode(array("msg" => 'Account Information Successfully Updated.'));
        } else {
            echo json_encode(array("msg" => $this->ion_auth->errors()));
        }
        exit;
    }

    // edit a user
    public function edit_user($id)
    {
        
        $data['title'] = $this->lang->line('edit_user_heading');
        // check permissions
        if (!$this->edit_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect($_SERVER['HTTP_REFERER'], 'refresh');
        endif;

        $user = $this->ion_auth->user($id)->row();
        //validate form input
        $this->form_validation->set_rules('first_name', $this->lang->line('edit_user_validation_fname_label'), 'required');
        $this->form_validation->set_rules('last_name', $this->lang->line('edit_user_validation_lname_label'), 'required');
        $this->form_validation->set_rules('email', $this->lang->line('edit_user_validation_lname_label'), 'required');

        if (isset($_POST) && !empty($_POST)) {
            // update the password if it was posted
            if ($this->input->post('password')) {
                $this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
                $this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
            }

            if ($this->form_validation->run() === true) {


                $roledata[] = $role_id = $this->input->post('user_role');
                $data = array(
                    'first_name' => $this->input->post('first_name'),
                    'middle_name' => $this->input->post('middle_name'),
                    'last_name' => $this->input->post('last_name'),
                    'username' => $this->input->post('email'),
                    'email' => $this->input->post('email'),
                );

                $container_id = $this->input->post('containers');

                if (in_array($role_id, ['0', SITE_MANAGER, INSTRUCTOR, GRADE_ADMINISTRATOR, GRADER, STUDENT, PARENTS])) {
                    $data['institutional_unique_id'] = $this->input->post('institutional_unique_id');
                    $data['secondary_email'] = $this->input->post('secondary_email');
                    $data['phone'] = $this->input->post('phone');
                    $data['cellphone'] = $this->input->post('cellphone');
                    $data['time_zone'] = $this->input->post('time_zone');
                    $data['standing_year'] = $this->input->post('standing_year');
                    $data['dob'] = ($this->input->post('dob')) ? date('Y-m-d', strtotime($this->input->post('dob'))) : '';
                } else {
                    $data['institutional_unique_id']    = '';
                    $data['secondary_email']            = '';
                    $data['phone']                      = '';
                    $data['cellphone']                  = '';
                    $data['time_zone']                  = '';
                    $data['standing_year']              = '';
                    $data['dob']                        = '';
                }

                $data['street_address_1']  = $this->input->post('street_address_1');
                $data['street_address_2']  = $this->input->post('street_address_2');
                $data['country']           = $this->input->post('country');
                $data['city']              = $this->input->post('city');
                $data['state']             = $this->input->post('state');
                $data['zip']               = $this->input->post('zip');
                $data['note']              = $this->input->post('note');
                $data['security_question'] = $this->input->post('security_question');
                $data['security_ans']      = $this->input->post('security_ans');
                $data['active']            = $this->input->post('active');


                if (in_array($role_id, ['0', 6])) {
                    $data['institutional_unique_id'] = '';
                    $container_id = 0;
                }
                $this->ion_auth->remove_from_container('', $id);
                if ($container_id > 0) {
                    $this->ion_auth->add_to_container($container_id, $role_id, $id);
                }
                // update the password if it was posted
                if ($this->input->post('password')) {
                    $data['password'] = trim($this->input->post('password'));
                }

                if (isset($roledata) && !empty($roledata)) {
                    $this->common_model->delete(['user_id' => $id], 'users_roles');
                    $this->ion_auth->add_to_role($roledata, $id);
                }

                // check to see if we are updating the user
                if ($this->ion_auth->update($user->id, $data)) {
                    echo json_encode(array("msg" => $this->ion_auth->messages(), "role_id" => $roledata[0], 'user_id' => $user->id));
                    exit;
                } else {
                    echo json_encode(array("msg" => $this->ion_auth->errors()));
                    exit;
                }
            } else {

                $msg = 'Password do not match';
                $this->session->set_flashdata('error', $msg);
                redirect('users/edit_user/' . $id, 'refresh');
            }
        }
        $roles = $this->ion_auth->roles()->result_array();
        foreach ($roles as $role) {
            $rolelist[$role['id']] = $role['name'];
        }


        $data['roles'] = $rolelist;
        $data['currentroles'] = $this->ion_auth->get_users_roles($id)->row()->id;
        $condition = ['status' => 1, 'status_delete' => 1];
        if ($this->container_id > 0) {
            $condition = ['id' => $this->container_id];
        }
        $containers = $this->common_model->getAllData('containers', 'id, name', '', $condition);

        $data['containers'][] = '--Select Container--';
        foreach ($containers as $container) {
            $data['containers'][$container->id] = $container->name;
        }
        $data['currentcontainers'] = $this->ion_auth->get_users_containers($id)->row()->id;

        // display the edit user form
        $data['csrf'] = $this->_get_csrf_nonce();

        // set the flash data error message if there is one
        $data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

        // pass the user to the view
        $data['user'] = $user;

        $data['first_name'] = array(
            'name' => 'first_name',
            'id' => 'first_name',
            'required' => 'required',
            'type' => 'text',
            'placeholder' => 'First name',
            'value' => $this->form_validation->set_value('first_name', $user->first_name),
        );
        $data['middle_name'] = array(
            'name' => 'middle_name',
            'id' => 'middle_name',
            'type' => 'text',
            'class' => 'form-control',
            'placeholder' => 'Middle name',
            'value' => $this->form_validation->set_value('middle_name', $user->middle_name),
        );
        $data['last_name'] = array(
            'name' => 'last_name',
            'id' => 'last_name',
            'required' => 'required',
            'class' => 'form-control',
            'type' => 'text',
            'value' => $this->form_validation->set_value('last_name', $user->last_name),
        );

        $data['email'] = array(
            'name' => 'email',
            'id' => 'email',
            'type' => 'email',
            'required' => 'required',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('last_name', $user->email),
        );
        $data['unique_id'] = array(
            'name' => 'unique_id',
            'id' => 'unique_id',
            'class' => 'form-control',
            'type' => 'text',
            'required' => 'required',
            'readonly' => 'readonly',
            'value' => $this->form_validation->set_value('unique_id', $user->unique_id),
        );
        $data['phone'] = array(
            'name' => 'phone',
            'id' => 'phone',
            'class' => 'form-control',

            'type' => 'text',
            'value' => $this->form_validation->set_value('phone', $user->phone),
        );
        $data['cellphone'] = array(
            'name' => 'cellphone',
            'id' => 'cellphone',
            'class' => 'form-control',
            'type' => 'text',
            'value' => $this->form_validation->set_value('cellphone', $user->cellphone),
        );
        $data['institutional_unique_id'] = array(
            'name' => 'institutional_unique_id',
            'id' => 'institutional_unique_id',
            'class' => 'form-control',
            'type' => 'text',
            'value' => $this->form_validation->set_value('institutional_unique_id', $user->institutional_unique_id),
        );

        $data['secondary_email'] = array(
            'name' => 'secondary_email',
            'id' => 'secondary_email',
            'class' => 'form-control',
            'type' => 'text',
            'value' => $this->form_validation->set_value('secondary_email', $user->secondary_email),
        );

        $data['dob'] = array(
            'name' => 'dob',
            'id' => 'dob',
            'class' => 'form-control',
            'type' => 'text',
            'autofill' => 'off',
            'value' => $this->form_validation->set_value('dob', ($user->dob == '0000-00-00') ? '' : date('m/d/Y', strtotime($user->dob))),
        );
        $data['active'] = array(
            'name' => 'active',
            'id' => 'active',
            'value' => $this->form_validation->set_value('active'),
        );
        $data['password'] = array(
            'name' => 'password',
            'id' => 'password',
            'type' => 'password',
            'class' => 'form-control',
            'placeholder' => '********',
            'autocomplete' => 'new-password'
        );
        $data['password_confirm'] = array(
            'name' => 'password_confirm',
            'id' => 'password_confirm',
            'type' => 'password',
            'class' => 'form-control',
            'placeholder' => '********',
            'autocomplete' => 'new-password'
        );
        $data['street_address_1'] = array(
            'name' => 'street_address_1',
            'id' => 'street_address_1',
            'class' => 'form-control',
            'type' => 'text',
            'placeholder' => 'Street address 1',
            'value' => $this->form_validation->set_value('street_address_1', $user->street_address_1),
        );
        $data['street_address_2'] = array(
            'name' => 'street_address_2',
            'id' => 'street_address_2',
            'class' => 'form-control',
            'type' => 'text',
            'placeholder' => 'Street address 2',
            'value' => $this->form_validation->set_value('street_address_2', $user->street_address_2),
        );
        $data['country'] = array(
            'name' => 'country',
            'id' => 'country',
            'class' => 'form-control',
            'placeholder' => 'Country',
            'value' => $this->form_validation->set_value('country', $user->country),
        );
        $data['city'] = array(
            'name' => 'city',
            'id' => 'city',
            'class' => 'form-control city',
            'type' => 'text',
            'placeholder' => 'City',
            'value' => $this->form_validation->set_value('city', $user->city),
        );
        $data['state'] = array(
            'name' => 'state',
            'id' => 'state',
            'class' => 'form-control states',
            'type' => 'text',
            'placeholder' => 'State',
            'value' => $this->form_validation->set_value('state', $user->state),
        );
        $data['zip'] = array(
            'name' => 'zip',
            'id' => 'zip',
            'class' => 'form-control',
            'type' => 'text',
            'placeholder' => 'Zip',
            'value' => $this->form_validation->set_value('zip', $user->zip),
        );
        $data['note'] = array(
            'name' => 'note',
            'id' => 'note',
            'class' => 'form-control',
            'type' => 'text',
            'value' => $this->form_validation->set_value('note', $user->note),
        );
        $data['security_question'] = array(
            'name' => 'security_question',
            'id' => 'security_question',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('security_question', $user->security_question),
        );
        $data['security_ans'] = array(
            'name' => 'security_ans',
            'id' => 'security_ans',
            'class' => 'form-control',
            'type' => 'text',
            'placeholder' => 'Security Answer',
            'value' => $this->form_validation->set_value('security_ans', $user->security_ans),
            'autocomplete' => 'new-password'
        );
        $data['breadcrumb'][] = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][] = ['title' => 'Users', 'link' => base_url('/users'), 'class' => 'active'];
        $data['breadcrumb'][] = ['title' => 'Edit user', 'link' => '', 'class' => 'active'];

        $data['user_id'] = $id;
        $CI = &get_instance();
        $role_id = $this->common_model->getDataById('users_roles', 'role_id', ['user_id' => $CI->ion_auth->user()->row()->id])->role_id;
        $data['user_role_name'] = get_role_by_id($role_id);
        if ($data['user_role_name'] ==  PARENTS) {
            $data['parent_data'] = $this->common_model->Djoin('', 'users', 'user_parents', 'user_parents.parent_id = users.id', '', '', 'user_parents.parent_id=' . $id . ' ');
        } elseif ($data['user_role_name'] ==  STUDENT) {
            $data['student_data'] = $this->common_model->Djoin('', 'users', 'user_parents', 'user_parents.user_id = users.id', '', '', 'user_parents.user_id=' . $id . ' ');
        }

        $timezones = $this->common_model->getAllData('timezones', '*', '', '');
        $data['timezones'][] = '--Select timezone--';
        foreach ($timezones as $timezone) {
            $data['timezones'][$timezone->id] = $timezone->title . " (" . $timezone->code . ") - " . $timezone->offset;
        }
        $state = $this->common_model->getAllData('states', ['name', 'id'], '', ['status_delete' => 1, 'status' => 1]);
        foreach ($state as $key => $value) {
            $data['states'][] = $value->name;
        }

        /////////////////////////////////////////////////
        $data['questions'][] = '--Select Question--';
        $questions = $this->common_model->getAllData('security_questions', ['id', 'title'], '', ['status' => 1]);
        foreach ($questions as $key => $value) {
            $data['questions'][] = $value->title;
        }
        ///////////////////////////////////////////         

        $data['page'] = 'users/users/edit_user';
        $this->template->template_view($data);
    }

    // view user
    public function view_user($user_id = null)
    {
        // check permissions
        if (!$this->view_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect($_SERVER['HTTP_REFERER'], 'refresh');
        endif;
        // get user profile data
        $data['user_profile'] = $this->common_model->update_data(['id' => $user_id], 'users');

        $data['page'] = 'users/users/view_user';
        $this->template->template_view($data);
    }

    public function _render_page($view, $data = null, $returnhtml = false)
    { //I think this makes more sense
        $this->viewdata = (empty($data)) ? $this->data : $data;

        $view_html = $this->load->view($view, $this->viewdata, $returnhtml);

        if ($returnhtml) {
            return $view_html;
        } //This will return html on 3rd argument being true
    }

    public function _valid_csrf_nonce()
    {
        $csrfkey = $this->input->post($this->session->flashdata('csrfkey'));
        if ($csrfkey && $csrfkey == $this->session->flashdata('csrfvalue')) {
            return true;
        } else {
            return false;
        }
    }

    public function _get_csrf_nonce()
    {
        $this->load->helper('string');
        $key = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }

    // deactivate the user
    public function deactivate($id = null)
    {
        if (!$this->ion_auth->logged_in()) {
            // redirect them to the home page because they must be an administrator to view this
            return show_error('You must be an administrator to view this page.');
        }

        $id = (int) $id;

        $this->load->library('form_validation');
        $this->form_validation->set_rules('confirm', $this->lang->line('deactivate_validation_confirm_label'), 'required');
        $this->form_validation->set_rules('id', $this->lang->line('deactivate_validation_user_id_label'), 'required|alpha_numeric');

        if ($this->form_validation->run() == false) {
            // insert csrf check
            $this->data['csrf'] = $this->_get_csrf_nonce();
            $this->data['user'] = $this->ion_auth->user($id)->row();

            $this->_render_page('users/deactivate_user', $this->data);
        } else {
            // do we really want to deactivate?
            if ($this->input->post('confirm') == 'yes') {
                // do we have a valid request?
                if ($this->_valid_csrf_nonce() === false || $id != $this->input->post('id')) {
                    show_error($this->lang->line('error_csrf'));
                }

                // do we have the right userlevel?
                if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin()) {
                    $this->ion_auth->deactivate($id);
                }
            }

            // redirect them back to the auth page
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect('Users', 'refresh');
        }
    }

    // activate the user
    public function activate($id, $code = false)
    {
        if ($code !== false) {
            $activation = $this->ion_auth->activate($id, $code);
        } else {
            $activation = $this->ion_auth->activate($id);
        }

        if ($activation) {
            // redirect them to the auth page
            $msg = $this->ion_auth->messages();
        } else {
            // redirect them to the forgot password page
            $msg = $this->ion_auth->errors();
        }
        echo json_encode(['msg' => $msg]);
        exit;
    }

    public function print_users()
    {
        $arr = array(
            'odd' => array(
                'L' => array(
                    'content' => 'Code Master',
                    'font-size' => 20,
                    'font-style' => 'B',
                    'font-family' => 'serif',
                    'color' => 'red',
                ),
                'C' => array(
                    'content' => '',
                    'font-size' => 10,
                    'font-style' => 'B',
                    'font-family' => 'serif',
                    'color' => '#000000',
                ),
                'R' => array(
                    'content' => 'Print Users with Mpdf',
                    'font-size' => 10,
                    'font-style' => 'B',
                    'font-family' => 'serif',
                    'color' => '#000000',
                ),
                'line' => 1,
            ),
            'even' => array(),
        );

        $this->data['all_users'] = $this->Users_modal->all_users();

        $this->load->library('Pdf');
        $this->pdf->SetHeader($arr);
        $this->pdf->SetFooter('||{PAGENO}');
        $this->pdf->load_view('print/print_with_Mpdf', $this->data);
        $this->pdf->Output();
    }

    public function print_with_dompdf($value = '')
    {
        $data['all_users'] = $this->Users_modal->all_users();

        // Load all views as normal
        $this->load->view('print/print_with_Dom-pdf', $data);

        // Get output html
        $html = $this->output->get_output();

        // Load library
        $this->load->library('dompdf_gen');

        // Convert to PDF
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream('welcome.pdf', array('Attachment' => false));
    }

    public function Profile()
    {
        //// timezones
        $timezones = $this->common_model->getAllData('timezones', '*', '', '');
        
        $data['timezones'][] = '--Select timezone--';
        foreach ($timezones as $timezone) {
            $data['timezones'][$timezone->id] = $timezone->title . " (" . $timezone->code . ") - " . $timezone->offset;
        }

        $user_id = array('id' => $this->session->userdata('user_id'));
        $data['user_profile'] = $this->common_model->update_data($user_id, 'users');
        // get user profile data
        $data['institution_name'] = $this->common_model->DJoin('containers.id, containers.name', 'containers','containers_users_roles','containers.id = containers_users_roles.container_id', true, '', ['containers_users_roles.user_id' => $this->session->userdata('user_id')]);
        // display the form
        // set the flash data error message if there is one
        $data['roles'] = '';
        $data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
        $data['user_role_details'] = currentGroup();
        $data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');

        $data['old_password'] = array(
            'name' => 'old',
            'id' => 'old',
            'type' => 'password',
            'class' => 'form-control',
            'required' => 'required',
        );
        $data['new_password'] = array(
            'name' => 'new',
            'id' => 'new',
            'class' => 'form-control',
            'type' => 'password',
            'required' => 'required',
            'minlength' => 8,
            'pattern' => '^.{' . $data['min_password_length'] . '}.*$',
        );
        $data['new_password_confirm'] = array(
            'name' => 'new_confirm',
            'id' => 'new_confirm',
            'class' => 'form-control',
            'type' => 'password',
            'required' => 'required',
            'pattern' => '^.{' . $data['min_password_length'] . '}.*$',
        );
        $data['user_id'] = array(
            'name' => 'user_id',
            'id' => 'user_id',
            'type' => 'hidden',
            'value' => $data['user_profile']->id,
        );
        $data['breadcrumb'][] = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][] = ['title' => 'Profile', 'link' => '', 'class' => 'active'];
        $state = $this->common_model->getAllData('states', ['name', 'id'], '', ['status_delete' => 1, 'status' => 1]);
        foreach ($state as $key => $value) {
            $data['states'][] = $value->name;
        }
          /////////////////////////////////////////////////
          $data['questions'][] = '--Select Question--';
          $questions = $this->common_model->getAllData('security_questions', ['id', 'title'], '', ['status' => 1]);
          foreach ($questions as $key => $value) {
              $data['questions'][] = $value->title;
          }
          ///////////////////////////////////////////
        $data['page'] = 'users/users/user_profile';
        $this->template->template_view($data);
    }

    public function change_password()
    {
        // $resultId = 0;
        // $user_id = array('id' => $this->session->userdata('user_id'));
        if ($this->input->post()) {
            $oldPassword = post('old');
            $newPassword = post('new');
            $confirmPassword = post('new_confirm');
            $identity = $this->session->userdata('identity');

            $change = $this->ion_auth->change_password($identity, $oldPassword, $newPassword);
            if ($change) {
                //$resetPassword = $this->ion_auth->reset_password($identity, $newPassword);
                $msg = 'Password has been updated Successfully';

                $this->session->set_flashdata('msg', $msg);
                //                if ($resetPassword) {
                //                    $result = $this->common_model->update($user_id, $data, 'users');
                //                }
            } else {
                $msg = 'Password could not updated';
                $this->session->set_flashdata('msg', $msg);
            }
            echo json_encode(array("msg" => $msg));
            exit;
        }
    }

    public function edit_profile()
    {
        $resultId = 0;
        $user_id = array('id' => $this->session->userdata('user_id'));
        if ($this->input->post()) {
            $first_name = post('first_name');
            $middle_name = post('middle_name');
            $last_name = post('last_name');
            $email = post('email');
            $mobile_no = post('phone');
            $street_address_1 = post('street_address_1');
            $street_address_2 = post('street_address_2');
            $city = post('city');
            $state = post('state');
            $zip = post('zip');
            $time_zone = post('time_zone');
            $secondary_email = post('secondary_email');
            $password_email_check = post('password_email_check');
            $notification_email_check = post('notification_email_check');
            $img_name = post('img_name');
            $data = array(
                'phone' => $mobile_no,
                'first_name' => $first_name,
                'middle_name' => $middle_name,
                'last_name' => $last_name,
                'street_address_1' => $street_address_1,
                'street_address_2' => $street_address_2,
                'city' => $city,
                'state' => $state,
                'zip' => $zip,
                'time_zone' => $time_zone,
                'secondary_email' => $secondary_email,
                'password_email_check' => $password_email_check,
                'notification_email_check' => $notification_email_check,
                'user_img' => $img_name,
            );
            $user_id = $this->session->userdata('user_id');
            $resultData = $this->common_model->getDataById('users', 'id,email', ['email' => $email]);

            if (empty($resultData)) {
                $result = $this->common_model->update($user_id, $data, 'users');
                if ($result) {
                    $msg = 'Profile Updated Successfully';
                } else {
                    $msg = 'Profile not updated.';
                }
            } else {
                $resultId = $resultData->id;
                if ($resultId == $user_id) {
                    $result = $this->common_model->update($user_id, $data, 'users');
                    if ($result) {
                        $msg = 'Profile Updated Successfully';
                        $this->session->set_flashdata('success', $msg);
                        redirect('users/profile', 'refresh');
                    } else {
                        $msg = 'Profile not updated.';
                    }
                } else {
                    $msg = 'Email already exist.';
                }
            }

            echo json_encode(array("msg" => $msg));
            exit;
        } else {
            $data['user_profile'] = $this->common_model->update_data($user_id, 'users');
            $data['page'] = 'users/users/edit_user_profile';
            $this->template->template_view($data);
        }
    }

    public function update_avatar()
    {
        $image = $this->input->post('chang_image');
        $data = array('user_img' => $image);
        $user_id = $this->session->userdata('user_id');
        $result = $this->common_model->update($user_id, $data, 'users');
        if ($result) {
            $msg = 'User profile image updated Successfully';
            $this->session->set_flashdata('success', $msg);
            redirect('users/Profile', 'refresh');
        } else {
            $msg = 'User profile image could not updated.';
            $this->session->set_flashdata('error', $msg);
            redirect('users/Profile', 'refresh');
        }
    }

    public function resizeImage($filename)
    {
        $source_path = './uploads/user_profile/' . $filename;
        $target_path = './uploads/user_profile/thumbnails/';
        $config_manip = [
            'image_library' => 'gd2',
            'source_image' => $source_path,
            'new_image' => $target_path,
            'maintain_ratio' => true,
            'create_thumb' => true,
            //'thumb_marker' => '_thumb',
            'width' => 150,
            'height' => 150,
        ];
        $this->load->library('image_lib', $config_manip);
        if (!$this->image_lib->resize()) {
            echo $this->image_lib->display_errors();
        }
        $this->image_lib->clear();
    }

    public function upload_docs_image()
    {
        $data = [];
        $file_element_name = 'file';
        if ($_REQUEST['btnSubmit'] == 'Submit Image') {

            $config['upload_path'] = './uploads/user_profile/';
            $config['allowed_types'] = 'gif|jpg|png';
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload($file_element_name)) {
                $status = 'error';
                $data['error'] = $this->upload->display_errors('', '');
                $data['status'] = false;
            } else {
                $fileData = $this->upload->data();

                $this->resizeImage($fileData['file_name']);
                $data['filePath'] = base_url() . 'uploads/user_profile/thumbnails/';
                $data['fileName'] = $fileData['file_name'];
                $data['status'] = true;

                $postData = array('user_img' => $fileData['file_name']);
                $user_id = $this->session->userdata('user_id');
                $result = $this->common_model->update($user_id, $postData, 'users');
            }
        }
        echo json_encode($data);
        exit;
    }

    public function remove_user_img()
    {
        $fileName = $_POST['file'];
        if (unlink(APPPATH . '../uploads/user_profile/' . $fileName)) {
            unlink(APPPATH . '../uploads/user_profile/thumbnails/' . $fileName);
            echo 1;
        } else {
            echo 0;
        }
    }

    public function download_csv()
    {

        //  $user_data = $this->Users_modal->export_users($_POST);
        $file_name = 'user_details_on_' . date('Ymd') . '.csv';
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=$file_name");
        header("Content-Type: application/csv;");
        // get data

        $user_data = $this->Users_modal->export_users($_POST);
        // file creation
        $file = fopen('php://output', 'w');

        $header = array("Id", "Name", "Institutional Email", "Parent", "Enrolled roles", "Status", "Institution", "Institution ID No.", "Phone No.", "Cell Phone No.", "Time Zone", "Standing Year", "Date of Birth");

        fputcsv($file, $header);

        foreach ($user_data['result'] as $key => $value) {
            $status = $value['active'];

            $nstatus = $status == 1 ? 'active' : 'inactive';

            $narray = array($value["id"], $value["user_name"], $value['email'], $value['parent'], $value['roles'], $nstatus); //$value['institution'],
            if ($value['dob'] == '0000-00-00') {
                $dob = '';
            } else {
                $dob = $value['dob'];
            }
            array_push($narray, $value["institution"], $value['institutional_unique_id'], $value['phone'], $value["cellphone"], $value['timezone'], $value['standing_year'], $dob);

            fputcsv($file, $narray);
        }
        fclose($file);
        exit;
    }

    public function delete_users()
    {
        $selected_user_id = $_POST['selected_user_id'];
        $user_data = $this->Users_modal->delete_selected_users($selected_user_id);
    }


    public function send_notification()
    {
        $msg=0;
        $data = explode("|", $_POST['selected_emaildata']);
        foreach ($data as $val) {
            $name_email = explode("~", $val);
            list($user_first_name) = explode(' ', $name_email[0]);
            $email_data['template_code'] = 'NOTIFICATION';

            $email_data['email'] = $name_email[1];
            $email_data['variables']['user_name'] = ucfirst($user_first_name);
            $email_data['variables']['body'] = $_POST['msg'];

            $notification = array(
                "sender_id" => $_SESSION['user_id'],
                "receiver_id" => $name_email[2],
                "status" => 1,
                "type" => 'type',
                "body" => $_POST['msg'],
                "subject" => $_POST['subject'],
                "priority" => 1,
                "is_read" => 0,
                "created" => date('Y-m-d H:i:s')
            );
            
            if ($name_email[0] != '') {
                sendgrid_email($email_data);
                $this->common_model->add('notifications', $notification);
                $msg++;
            }
        }
        echo $msg; exit;
    }

    public function favorites()
    {
        $this->template->error_404();
    }

    public function send_message()
    {
        $data = explode("|", $_POST['selected_user_ids']);
        $msg=0;
        foreach ($data as $val) {
            $messages = array(
                "sender_id" => $_SESSION['user_id'],
                "receiver_id" => $val,
                "subject" => $_POST['subject'],
                "body" => $_POST['msg'],
                "is_read" => 0,
                "created" => date('Y-m-d H:i:s')
            );
            
            $user_details = $this->common_model->getDataById('users','email,CONCAT_WS(" ", users.first_name, users.middle_name, users.last_name) as user_name',["id"=>$val]);
            $email_data['template_code'] = 'NOTIFICATION';
            $email_data['email'] = $user_details->email;
            $email_data['variables']['user_name'] = $user_details->user_name;
            $email_data['variables']['body'] = $_POST['msg'];
            
            if ($val != 0) {
                sendgrid_email($email_data);
                $this->common_model->add('messages', $messages);
                $msg++;
            }
        }
        
        echo $msg; exit;
    }

    // list users as per filter
    public function current_courses()
    {
        // Fetch member's records
        $userData = $this->Users_modal->current_courses($_POST);
        //gkc on dated 28th May , 2019,,,,,,,,,,,,,,,,
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $userData['total'],
            "recordsFiltered" => $userData['total'],
            "data" => $userData['result'],
        );
        // Output to JSON format
        echo json_encode($output);
    }
    // list users as per filter
    public function current_course_group()
    {
        // Fetch member's records
        $userData = $this->Users_modal->current_course_group($_POST);
        //gkc on dated 28th May , 2019,,,,,,,,,,,,,,,,
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $userData['total'],
            "recordsFiltered" => $userData['total'],
            "data" => $userData['result'],
        );
        // Output to JSON format
        echo json_encode($output);
    }
    // list users as per filter
    public function current_institution_group()
    {
        // Fetch member's records
        $userData = $this->Users_modal->current_institution_group($_POST);
        //gkc on dated 28th May , 2019,,,,,,,,,,,,,,,,
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $userData['total'],
            "recordsFiltered" => $userData['total'],
            "data" => $userData['result'],
        );
        // Output to JSON format
        echo json_encode($output);
    }
    // list users as per filter
    public function assigned_courses()
    {
        // Fetch member's records
        $userData = $this->Users_modal->assigned_courses($_POST);
        //gkc on dated 28th May , 2019,,,,,,,,,,,,,,,,
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $userData['total'],
            "recordsFiltered" => $userData['total'],
            "data" => $userData['result'],
        );
        // Output to JSON format
        echo json_encode($output);
    }
    // list users as per filter
    public function assigned_students()
    {
        // Fetch member's records
        $userData = $this->Users_modal->assigned_students($_POST);
        //gkc on dated 28th May , 2019,,,,,,,,,,,,,,,,
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $userData['total'],
            "recordsFiltered" => $userData['total'],
            "data" => $userData['result'],
        );
        // Output to JSON format
        echo json_encode($output);
    }
    // list users as per filter
    public function student_completed_courses()
    {
        // Fetch member's records
        $userData = $this->Users_modal->student_completed_courses($_POST);
        //gkc on dated 28th May , 2019,,,,,,,,,,,,,,,,
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $userData['total'],
            "recordsFiltered" => $userData['total'],
            "data" => $userData['result'],
        );
        // Output to JSON format
        echo json_encode($output);
    }
    // list users as per filter
    public function student_curent_courses()
    {
        // Fetch member's records
        $userData = $this->Users_modal->student_curent_courses($_POST);
        //gkc on dated 28th May , 2019,,,,,,,,,,,,,,,,
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $userData['total'],
            "recordsFiltered" => $userData['total'],
            "data" => $userData['result'],
        );
        // Output to JSON format
        echo json_encode($output);
    }
    // list users as per filter
    public function dropped_unenrolled_courses()
    {
        // Fetch member's records
        $userData = $this->Users_modal->dropped_unenrolled_courses($_POST);
        //gkc on dated 28th May , 2019,,,,,,,,,,,,,,,,
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $userData['total'],
            "recordsFiltered" => $userData['total'],
            "data" => $userData['result'],
        );
        // Output to JSON format
        echo json_encode($output);
    }


    public function get_created_by($source_table, $sourceID){
        $data['typeid'] = $sourceID;
        list($sourceID, $type) = explode("_", $sourceID);
        $this->load->model('Users_modal');
        $data['value'] = $this->Users_modal->get_created_by($source_table, $sourceID);
        echo json_encode($data); 
        exit;
    }

}

/* End of file Users.php */
/* Location: ./application/controllers/Users.php */
