<?php

defined('BASEPATH') or exit('No direct script access allowed');
/*
  Author Salman Iqbal
  date 29/1/2017
  Company Parexons
 */

class User_roles extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $this->load->library(array('form_validation'));
        $this->load->helper(array('html', 'language'));
        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('auth');
        $this->load->model(array('common_model', 'Users_modal', 'Users_roles'));

        $this->load->module('template');

        if (!$this->ion_auth->logged_in()) {
            redirect('auth', 'refresh');
        }

        $sess_data = $this->session->all_userdata();
	
    }

    public function index()
    {
        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'User Roles', 'link' => '', 'class' => 'active'];
        // set the flash data error message if there is one
        $data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
        $this->ion_auth->where(array('id != ' => 1));
        $this->ion_auth->order_by('id', 'asc');
        // list the roles
        $data['roles'] = $this->ion_auth->roles()->result();
        $this->session->set_flashdata('message', $this->ion_auth->messages());
        $data['page'] = 'users/user_roles/view_role';

        $this->template->template_view($data);
    }

    // create a new role
    public function create_role()
    {
        
        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'User Roles', 'link' => base_url('/users/user_roles'), 'class' => 'active'];
        $data['breadcrumb'][]           = ['title' => 'Add', 'link' => '', 'class' => 'active'];
        $data['title'] = $this->lang->line('create_role_title');

        // validate form input
        $this->form_validation->set_rules('role_name', $this->lang->line('create_role_validation_name_label'), 'trim|required');
        $this->form_validation->set_rules('description', $this->lang->line('create_role_validation_name_label'), 'trim|required');

        if ($this->form_validation->run() == true) {
            $pre = $this->input->post('privilege');

            if (empty($pre)) {
                $msg = "You Must Have to Select at least one Previlige";
                $this->session->set_flashdata('error', $msg);
                redirect('users/User_roles/create_role', 'refresh');
            }

            $new_role_id = $this->ion_auth->create_role($this->input->post('role_name'), $this->input->post('description'));



            foreach ($pre as $key => $value) {
                $data = array('perm_id' => $pre[$key], 'role_id' => $new_role_id);

                $result = $this->common_model->add('role_perm', $data);
            }

            if ($result) {
                // check to see if we are creating the role
                // redirect them back to the user roles page
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                redirect("users/User_roles", 'refresh');
            }
        } else {
            // display the create role form
            // set the flash data error message if there is one
            $data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

            $data['role_name'] = array(
                'name' => 'role_name',
                'id' => 'role_name',
                'placeholder' => 'Admin',
                'class' => 'form-control',
                'type' => 'text',
                // 'required' => 'required',
                'value' => $this->form_validation->set_value('role_name'),
            );
            $data['description'] = array(
                'name' => 'description',
                'id' => 'description',
                'class' => 'form-control',
                'placeholder' => 'Administrator',
                //  'required' => 'required',
                'type' => 'text',
                'value' => $this->form_validation->set_value('description'),
            );

            $data['perm'] = $this->common_model->select('permissions');

            $data['page'] = 'users/user_roles/create_role';
            $this->template->template_view($data);
            // $this->_render_page('dashboard', $data);
        }
    }

    // edit a role
    public function edit_role($id)
    {
       
        $data['breadcrumb'][]       = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]       = ['title' => 'User Roles', 'link' => base_url('/users/user_roles'), 'class' => 'active'];
        $data['breadcrumb'][]       = ['title' => 'Edit', 'link' => '', 'class' => 'active'];
        // bail if no role id given
        if (!$id || empty($id)) {
            redirect('users/User_roles', 'refresh');
        }

        $data['title']              = $this->lang->line('edit_role_title');
        $role                       = $this->ion_auth->role($id)->row();
        $data['privileges']         = $this->common_model->select('permissions');
        $data['containers']         = $this->common_model->getAllData('containers', 'id, name', '', 'type = 1');
        $data['crtPrivilege']       = $this->Users_modal->get_user_privileges($id);
        $data['currentContainers']  = $this->Users_roles->get_user_containers($id);

        // validate form input
        $this->form_validation->set_rules('role_name', $this->lang->line('edit_role_validation_name_label'), 'trim|required');
        $this->form_validation->set_rules('role_description', $this->lang->line('edit_role_validation_name_label'), 'trim|required');

        if (isset($_POST) && !empty($_POST)) {

            if ($this->form_validation->run() === true) {

                //Only allow updating roles if user is admin
                if ($this->ion_auth->is_admin()) {
                    //Update the roles user belongs to
                    $privilegeData = $this->input->post('privlg');
                    $containersData = $this->input->post('containers');

                    if (empty($privilegeData)) {
                        $msg = "You Must Have To Set At Least One Privilege";
                        $this->session->set_flashdata('error', $msg);
                        redirect("users/User_roles/edit_role/" . $id, 'refresh');
                    }

                    if (isset($privilegeData) && !empty($privilegeData)) {

                        $query = $this->Users_modal->remove_from_privileges($privilegeData, $id);

                        // print_r($query);die();
                        foreach ($privilegeData as $key => $value) {
                            $data = array('perm_id' => $privilegeData[$key], 'role_id' => $id);

                            $result = $this->common_model->add('role_perm', $data);
                        }
                    }

                    // Add roles to containers
                    if (isset($containersData) && !empty($containersData)) {

                        $query = $this->Users_roles->remove_from_containers($containersData, $id);
                        //pr($containersData); exit;
                        // print_r($query);die();
                        foreach ($containersData as $key => $value) {
                            $data = array('container_id' => $containersData[$key], 'role_id' => $id);

                            $result = $this->common_model->add('containers_roles', $data);
                        }
                    }
                }
                $data = array('name' => $this->input->post('role_name'), 'description' => $this->input->post('role_description'));

                $role_update = $this->common_model->update($id, $data, 'roles');

                if ($role_update) {
                    $this->session->set_flashdata('message', $this->lang->line('edit_role_saved'));
                    redirect("users/User_roles", 'refresh');
                } else {
                    $this->session->set_flashdata('message', $this->ion_auth->errors());
                }
            }
        }

        // set the flash data error message if there is one
        $data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
        //pr($data); exit;
        $data['role'] = $role;
        $readonly = $this->config->item('admin_role', 'ion_auth') === $role->name ? 'readonly' : '';

        $data['role_name'] = array(
            'name' => 'role_name',
            'id' => 'role_name',
            'type' => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('role_name', $role->name),
        );
        $data['role_description'] = array(
            'name' => 'role_description',
            'id' => 'role_description',
            'class' => 'form-control',
            'type' => 'text',
            'value' => $this->form_validation->set_value('role_description', $role->description),
        );


        $data['page'] = 'users/user_roles/edit_role';
        $this->template->template_view($data);
        // $this->_render_page("dashboard", $data);
    }

    public function _render_page($view, $data = null, $returnhtml = false)
    { //I think this makes more sense

        $this->viewdata = (empty($data)) ? $this->data : $data;

        $view_html = $this->load->view($view, $this->viewdata, $returnhtml);

        if ($returnhtml)
            return $view_html; //This will return html on 3rd argument being true
    }

    /*
     * Delete role
     */

    public function delete_role()
    {
        $role_id = $this->uri->segment(4);
        //pass the right arguments and it's done
        $role_delete = $this->ion_auth->delete_role($role_id);
        if ($role_delete) {
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect('users/User_roles', 'refresh');
        } else {
            $msg = "You Can Not Delete Admin!";
            $this->session->set_flashdata('error', $msg);
            redirect('users/User_roles', 'refresh');
        }
    }

    /*
     * Add New Permissions
     */

    public function permissions($value = '')
    {
        
        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Permissions', 'link' => '', 'class' => 'active'];
        if ($_POST) {
            $perm = post('perm');

            $data = array('perm_name' => $perm);

            $result = $this->common_model->add('permissions', $data);

            if ($result) {
                $msg = "Permission Added Successfully";
                $this->session->set_flashdata('success', $msg);
                redirect('users/User_roles/permissions', 'refresh');
            } else {
                $msg = "Error";
                $this->session->set_flashdata('error', $msg);
                redirect('users/User_roles/permissions', 'refresh');
            }
        } else {

            $data['perm'] = $this->common_model->select('permissions');

            $data['page'] = 'users/user_roles/permissions';
            $this->template->template_view($data);
            // $this->_render_page("dashboard", $data);
        }
    }

    /*
     * Delete Permission
     */

    public function delete_perm($id)
    {
        $del_id = array('perm_id' => $id);

        $result = $this->common_model->delete($del_id, "permissions");

        if ($result) {
            $msg = "Permission Delete Successfully";
            $this->session->set_flashdata('success', $msg);
            redirect('users/User_roles/permissions', 'refresh');
        } else {
            $msg = "Error";
            $this->session->set_flashdata('error', $msg);
            redirect('users/User_roles/permissions', 'refresh');
        }
    }

    //Check Duplicate role name
    public function check_role_name()
    {
        $role_name = $this->input->post('role_name');

        $result = $this->Users_roles->check_role('roles', $role_name);

        if ($result) {
            echo "ok::";
        }
    }

    /*
     * Get permission for update
     */

    public function get_perm($id)
    {
        $edit_id = array('perm_id' => $id);

        $result = $this->common_model->update_data($edit_id, 'permissions');

        echo json_encode($result);
    }

    /*
     * Update Permission
     */

    public function update_perm()
    {
       
        $perm = post('perm');
        $id = post('edit');

        $data = array('perm_name' => $perm);
        $edit_id = array('perm_id' => $id);

        $result = $this->Users_roles->update($edit_id, $data, "permissions");

        if ($result) {
            $msg = "Permission Update Successfully";
            $this->session->set_flashdata('success', $msg);
            redirect('users/User_roles/permissions', 'refresh');
        } else {
            $msg = "Error";
            $this->session->set_flashdata('error', $msg);
            redirect('users/User_roles/permissions', 'refresh');
        }
    }
}

/* End of file User_roles.php */
