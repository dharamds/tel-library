<?php defined('BASEPATH') or exit('No direct script access allowed');
/*
Author Salman Iqbal
date 29/1/2017
Company Parexons
 */
class Menus extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->library(array('form_validation'));
		$this->load->helper(array('html', 'language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
		$this->load->model(array('common_model', 'Users_modal', 'Menu_model'));

		$this->load->module('template');

		if (!$this->ion_auth->logged_in()) {
			redirect('auth', 'refresh');
		}

		$sess_data = $this->session->all_userdata();
 	
	}
	public function index()
	{
		// set the flash data error message if there is one
		$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		$this->ion_auth->where(array('id != ' => 1));
		$this->ion_auth->order_by('id','asc');
		// list the roles
		$data['roles'] = $this->ion_auth->roles()->result();
		$data['breadcrumb'][] = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][] = ['title' => 'Permissions', 'link' => '', 'class' => 'active'];
		$items = $this->Menu_model->get_items();
		$data['menu'] = $this->Menu_model->generateTree($items);	
		$data['page'] = 'users/menus/view_menu';
		
		$this->template->template_view($data);
	}

	public function set_menu_permission_users( $menu_id = null, $user_id = null) 
			{
				echo $this->Menu_model->save_menu_permission_users( $user_id, $menu_id); exit;
			}

	public function load_menu_permission_users($id, $page = 0) 
			{
				$this->load->library('pagination');

				$config['base_url'] 		= base_url('/users/menus/load_menu_permission_users/'.$id);
				$config['total_rows'] 		= count($this->common_model->getAllData('menus_permissions', 'id', '', array('menus_permissions.menu_id' => $id, 'menus_permissions.user_id !=' => 0)));
				$config['per_page'] 		= 2;
				$config['full_tag_open'] 	= "<ul class='pagination'>";
				$config['full_tag_close'] 	="</ul>";
				$config['num_tag_open'] 	= '<li>';
				$config['num_tag_close'] 	= '</li>';
				$config['cur_tag_open'] 	= "<li class='disabled'><li class='active'><a href='#'>";
				$config['cur_tag_close'] 	= "<span class='sr-only'></span></a></li>";
				$config['next_tag_open'] 	= "<li>";
				$config['next_tagl_close'] 	= "</li>";
				$config['prev_tag_open'] 	= "<li>";
				$config['prev_tagl_close'] 	= "</li>";
				$config['first_tag_open'] 	= "<li>";
				$config['first_tagl_close'] = "</li>";
				$config['last_tag_open'] 	= "<li>";
				$config['last_tagl_close'] 	= "</li>";
				$config['uri_segment'] 		= 5;
				$this->pagination->initialize($config);
				$data['currentUsers'] = $this->common_model->DJoin('users.id ,CONCAT_WS(" ", users.first_name, users.middle_name, users.last_name) as user_name,users.email, menus_permissions.user_id, menus_permissions.add_per, menus_permissions.edit_per, menus_permissions.delete_per, menus_permissions.view_per, menus_permissions.list_per', 'menus_permissions', 'users', 'menus_permissions.user_id = users.id','', '', array('menus_permissions.menu_id' => $id, 'menus_permissions.user_id !=' => 0), '', '', $config['per_page'], $page);
				$data['links'] = $this->pagination->create_links();
				$data['page'] = 'users/menus/menu_permission_users';
				$this->template->ajax_view($data, true);
			}
	// create a new role
	public function save_menu($id = NULL)
		{
			

		$data['title'] 				= $this->lang->line('create_menu_title');
		if($id){
			$data['menu_id'] 		= $id;
			$data['menuData'] 		= $this->common_model->update_data(array('id' => $id), 'menus');
			$data['currentRoles'] 	= $this->common_model->getAllData('menus_permissions', 'role_id, add_per, edit_per, delete_per, view_per, list_per', '', array('menu_id' => $id, 'menu_id !=' => 0));
		//$data['currentUsers'] 	= $this->common_model->getAllData('menus_permissions', 'user_id, add_per, edit_per, delete_per, view_per, list_per', '', array('menu_id' => $id, 'user_id !=' => 0));
		//$data['currentUsers'] 	= $this->common_model->DJoin('users.first_name, menus_permissions.user_id, menus_permissions.add_per, menus_permissions.edit_per, menus_permissions.delete_per, menus_permissions.view_per, menus_permissions.list_per', 'menus_permissions', 'users', 'menus_permissions.user_id = users.id','', '', array('menus_permissions.menu_id' => $id, 'menus_permissions.user_id !=' => 0));
		}
		
		// validate form input
		$this->form_validation->set_rules('name', $this->lang->line('create_menu_validation_name_label'), 'trim|required');
		$this->form_validation->set_rules('slug', $this->lang->line('create_menu_validation_slug_label'), 'trim|required');

		if ($this->form_validation->run() == true) {
			$users = $this->input->post('users');
			$roles = $this->input->post('roles');
			// Roles Permissions
			$role_edits = $this->input->post('role_edits');
			$role_deletes = $this->input->post('role_deletes');
			$role_adds = $this->input->post('role_adds');
			$role_views = $this->input->post('role_views');
			$role_list = $this->input->post('role_lists');
			//Users permissions
			$edits 		= $this->input->post('edits');
			$deletes 	= $this->input->post('deletes');
			$adds 		= $this->input->post('adds');
			$views 		= $this->input->post('views');
			$list 		= $this->input->post('lists');

			if (empty($roles)) {
				$msg = "You Must Have to Select at least one Role";
				$this->session->set_flashdata('error', $msg);
				//redirect('users/menus/create_menu', 'refresh');
			}
				$menuData['name'] 			= $this->input->post('name');
				$menuData['parent_id'] 		= $this->input->post('parent_id');
				$menuData['description'] 	= $this->input->post('description');
				$menuData['icon'] 			= $this->input->post('icon');
				$menuData['slug'] 			= $this->input->post('slug');
				$menuData['number'] 		= $this->input->post('number');
				if($id){
					$new_menu_id 			= $this->Menu_model->save_menu($menuData, $id);
				}else{
					$new_menu_id 			= $this->Menu_model->save_menu($menuData);
				}
				
			 $this->common_model->delete(array('menu_id' => $new_menu_id), 'menus_permissions');	

			foreach ($roles as $key => $value) {

				$role_add_per 		= ($role_adds[$value]) ? 1 : 0;
				$role_edit_per		= ($role_edits[$value]) ? 1 : 0;
				$role_delete_per 	= ($role_deletes[$value]) ? 1 : 0;
				$role_view_per 		= ($role_views[$value]) ? 1 : 0;
				$role_list_per 		= ($role_list[$value]) ? 1 : 0;
				
				$roledata = array('role_id' => $value, 'menu_id' => $new_menu_id, 'add_per' => $role_add_per, 'edit_per' => $role_edit_per, 'delete_per' => $role_delete_per, 'view_per' => $role_view_per, 'list_per' => $role_list_per);
				$result = $this->common_model->add('menus_permissions', $roledata);
			}
			//pr($users);	
			//pr($adds); exit;

			foreach ($users as $key => $value) {

				$add_per 	= ($adds[$value]) ? 1 : 0;
				$edit_per 	= ($edits[$value]) ? 1 : 0;
				$delete_per = ($deletes[$value]) ? 1 : 0;
				$view_per 	= ($views[$value]) ? 1 : 0;
				$list_per 	= ($list[$value]) ? 1 : 0;

				$userdata = array('user_id' => $users[$key], 'menu_id' => $new_menu_id, 'add_per' => $add_per, 'edit_per' => $edit_per, 'delete_per' => $delete_per, 'view_per' => $view_per, 'list_per' => $list_per);
				$result = $this->common_model->add('menus_permissions', $userdata);
			}

		if ($result) {
				// redirect them back to the user menu page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("users/menus", 'refresh');
		}
		} else {
			// display the create menu form
			// set the flash data error message if there is one
			$data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			$data['name'] = array(
				'name' => 'name',
				'id' => 'name',
				'placeholder' => 'Manu Name',
				'class' => 'form-control',
				'type' => 'text',
				'required' => 'required',
				'value' => $this->form_validation->set_value('name', $data['menuData']->name),
			);
			$data['icon'] = array(
				'name' => 'icon',
				'id' => 'icon',
				'placeholder' => 'Manu Icon Class',
				'class' => 'form-control',
				'type' => 'text',
				'value' => $this->form_validation->set_value('icon', $data['menuData']->icon),
			);
			$data['slug'] = array(
				'name' => 'slug',
				'id' => 'slug',
				'placeholder' => 'Manu slug',
				'class' => 'form-control',
				'type' => 'text',
				'required' => 'required',
				'value' => $this->form_validation->set_value('slug', $data['menuData']->slug),
			);
			$data['number'] = array(
				'name' => 'number',
				'id' => 'number',
				'placeholder' => 'Order',
				'class' => 'form-control',
				'type' => 'text',
				'value' => $this->form_validation->set_value('number', $data['menuData']->number),
			);
			$data['description'] = array(
				'name' => 'description',
				'id' => 'description',
				'class' => 'form-control',
				'placeholder' => 'Menu description',
				'type' => 'text',
				'value' => $this->form_validation->set_value('description', $data['menuData']->description),
			);

			//$data['users'] 		= $this->common_model->getAllData('users',  '', '', '', 'id desc', 5);
			$data['roles'] 		= $this->common_model->getAllData('roles', '', '', '', 'order_by asc');
			$data['permissions']= $this->common_model->getAllData('permissions', '', '', '', 'perm_id asc');

			//// breadcrumb
			$data['breadcrumb'][] = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
			$data['breadcrumb'][] = ['title' => 'Permissions', 'link' => base_url('/users/menus'), 'class' => 'active'];
			$data['breadcrumb'][] = ['title' => 'Add', 'link' => '', 'class' => 'active'];


			$items = $this->Menu_model->get_items();
			$data['menus'] = $this->Menu_model->generateOptionTree($items, 0, $data['menuData']->parent_id);
			$data['page'] = 'users/menus/save_menu';
			$this->template->template_view($data);
		}
	}

	// edit a role
	public function delete_menu($id)
	{
		
		
		// bail if no role id given
		if (!$id || empty($id)) {
			redirect('users/menus', 'refresh');
		}

				$delete_menu = $this->Menu_model->delete_menu($id);
				if ($delete_menu) {
					$this->session->set_flashdata('message', $this->lang->line('delete_menu'));
				} else {
					$this->session->set_flashdata('message', $this->ion_auth->errors());
				}
				redirect("users/menus", 'refresh');
			
		
	}

	public function _render_page($view, $data = null, $returnhtml = false)//I think this makes more sense
	{

		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);

		if ($returnhtml) return $view_html;//This will return html on 3rd argument being true
	}

	/*
	 * Delete role
	 */
	public function delete_role()
	{
		$role_id = $this->uri->segment(4);

		//pass the right arguments and it's done
		$role_delete = $this->ion_auth->delete_role($role_id);

		if ($role_delete) {
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect('users/User_roles', 'refresh');
		} else {
			$msg = "You Can Not Delete Admin!";
			$this->session->set_flashdata('error', $msg);
			redirect('users/User_roles', 'refresh');
		}
	}

	/*
	 * Add New Permissions
	 */
	public function permissions($value = '')
	{
		
		if ($_POST) {
			$perm = post('perm');

			$data = array('perm_name' => $perm);

			$result = $this->common_model->add('permissions', $data);

			if ($result) {
				$msg = "Permission Added Successfully";
				$this->session->set_flashdata('success', $msg);
				redirect('users/User_roles/permissions', 'refresh');
			} else {
				$msg = "Error";
				$this->session->set_flashdata('error', $msg);
				redirect('users/User_roles/permissions', 'refresh');
			}
		} else {

			$data['perm'] = $this->common_model->select('permissions');

			$data['page'] = 'users/user_roles/permissions';
			$this->template->template_view($data);
			// $this->_render_page("dashboard", $data);

		}

	}

	/*
	 * Delete Permission
	 */
	public function delete_perm($id)
	{
		
		$del_id = array('perm_id' => $id);

		$result = $this->common_model->delete($del_id, "permissions");

		if ($result) {
			$msg = "Permission Delete Successfully";
			$this->session->set_flashdata('success', $msg);
			redirect('users/User_roles/permissions', 'refresh');
		} else {
			$msg = "Error";
			$this->session->set_flashdata('error', $msg);
			redirect('users/User_roles/permissions', 'refresh');
		}
	}

	//Check Duplicate role name
	public function check_role_name()
	{
		$role_name = $this->input->post('role_name');

		$result = $this->Users_roles->check_role('roles', $role_name);

		if ($result) {
			echo "ok::";
		}
	}

    /*
	 * Get permission for update
	 */
	public function get_perm($id)
	{
		$edit_id = array('perm_id' => $id);

		$result = $this->common_model->update_data($edit_id, 'permissions');

		echo json_encode($result);
	}

    /*
	 * Update Permission
	 */
	public function update_perm()
	{
		
		$perm = post('perm');
		$id = post('edit');

		$data = array('perm_name' => $perm);
		$edit_id = array('perm_id' => $id);

		$result = $this->Users_roles->update($edit_id, $data, "permissions");

		if ($result) {
			$msg = "Permission Update Successfully";
			$this->session->set_flashdata('success', $msg);
			redirect('users/User_roles/permissions', 'refresh');
		} else {
			$msg = "Error";
			$this->session->set_flashdata('error', $msg);
			redirect('users/User_roles/permissions', 'refresh');
		}
	}
}

/* End of file User_roles.php */
/* Location: ./application/controllers/User_roles.php */

