<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // $this->load->database();
        $this->load->library(array('form_validation', 'email', 'facebook'));
        $this->load->helper(array('html', 'language', 'inflector'));
        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->load->config('ion_auth');

        $this->lang->load('auth');
        $this->load->model(array('Users_modal', 'Users_roles', 'common_model'));

        $this->load->module('template');

        //Include the twitter oauth php libraries
        include_once APPPATH . 'libraries/twitter-oauth-php-codexworld/twitteroauth.php';

        // Include the google api php libraries
        include_once APPPATH . 'libraries/google-api-php-client/Google_Client.php';
        include_once APPPATH . 'libraries/google-api-php-client/contrib/Google_Oauth2Service.php';
    }

    // redirect if needed, otherwise display the user list
    public function index()
    {
        if (!$this->ion_auth->logged_in()) {
            // redirect them to the login page
            redirect('users/auth/login', 'refresh');
        } else {

            //Count all users
            $data['total_users'] = $this->Users_modal->count_users();
            $data['total_institutions'] = count($this->common_model->getAllData('containers', 'id', '', ['status' => 0]));
            $data['total_courses'] = count($this->common_model->getAllData('courses', 'id', '', ['status' => 1]));
            //$currentContainerID = $this->Users_modal->SetCurrentContainer(siteURL());

            // set the flash data error message if there is one
            $data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            //$data['current_group']          = currentGroup();
            $data['breadcrumb'][] = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
            $data['breadcrumb'][] = ['title' => 'Dashboard', 'link' => '', 'class' => 'active'];
            $currentUserRole = $this->Users_roles->currentUserRole();

            switch ($currentUserRole->id) {

                case SUPER_ADMIN:
                    $data['institution_count'] = $this->db->query("SELECT * from containers where status=1 AND status_delete=1")->num_rows();
                    $data['users_count'] = $this->db->query("SELECT * from users where active=1 AND delete_status=1")->num_rows();
                    $data['courses_count'] = $this->db->query("SELECT * from courses where status=1 AND delete_status=1")->num_rows();
                    $data['system_admin_count'] = $this->db->query("SELECT * FROM `users_roles` as ur JOIN users as u ON u.id=ur.user_id WHERE ur.role_id=" . SYSTEM_ADMIN . " AND u.active=1 AND u.delete_status=1")->num_rows();
                    $data['manager_count'] = $this->db->query("SELECT * FROM `users_roles` as ur JOIN users as u ON u.id=ur.user_id WHERE ur.role_id=" . SITE_MANAGER . "  AND u.active=1 AND u.delete_status=1")->num_rows();

                    $data['page'] = 'users/auth/index';
                    break;

                case SYSTEM_ADMIN:
                    $data['institution_count'] = $this->db->query("SELECT * from containers where status=1 AND status_delete=1")->num_rows();
                    $data['users_count'] = $this->db->query("SELECT * from users where active=1 AND delete_status=1")->num_rows();
                    $data['courses_count'] = $this->db->query("SELECT * from courses where status=1 AND delete_status=1")->num_rows();
                    $data['system_admin_count'] = $this->db->query("SELECT * FROM `users_roles` as ur JOIN users as u ON u.id=ur.user_id WHERE ur.role_id=" . SYSTEM_ADMIN . " AND u.active=1 AND u.delete_status=1")->num_rows();
                    $data['manager_count'] = $this->db->query("SELECT * FROM `users_roles` as ur JOIN users as u ON u.id=ur.user_id WHERE ur.role_id=" . SITE_MANAGER . "  AND u.active=1 AND u.delete_status=1")->num_rows();

                    $data['page'] = 'users/auth/index';
                    break;

                case SYSTEM_EDITOR:
                    $data['institution_count'] = $this->db->query("SELECT * from containers where status=1 AND status_delete=1")->num_rows();
                    $data['users_count'] = $this->db->query("SELECT * from users where active=1 AND delete_status=1")->num_rows();
                    $data['courses_count'] = $this->db->query("SELECT * from courses where status=1 AND delete_status=1")->num_rows();
                    $data['system_admin_count'] = $this->db->query("SELECT * FROM `users_roles` as ur JOIN users as u ON u.id=ur.user_id WHERE ur.role_id=" . SYSTEM_ADMIN . " AND u.active=1 AND u.delete_status=1")->num_rows();
                    $data['manager_count'] = $this->db->query("SELECT * FROM `users_roles` as ur JOIN users as u ON u.id=ur.user_id WHERE ur.role_id=" . SITE_MANAGER . "  AND u.active=1 AND u.delete_status=1")->num_rows();

                    $data['page'] = 'users/auth/index';
                    break;

                case TEACHER_GROUP_LEADER:
                    $data['page'] = 'users/auth/' . underscore($currentUserRole->name);
                    break;

                case GRADER:
                    $data['institution_count'] = $this->db->query("SELECT * from containers where status=1 AND status_delete=1")->num_rows();
                    $data['users_count'] = $this->db->query("SELECT * from users where active=1 AND delete_status=1")->num_rows();
                    $data['courses_count'] = $this->db->query("SELECT * from courses where status=1 AND delete_status=1")->num_rows();
                    $data['system_admin_count'] = $this->db->query("SELECT * FROM `users_roles` as ur JOIN users as u ON u.id=ur.user_id WHERE ur.role_id=" . SYSTEM_ADMIN . " AND u.active=1 AND u.delete_status=1")->num_rows();
                    $data['manager_count'] = $this->db->query("SELECT * FROM `users_roles` as ur JOIN users as u ON u.id=ur.user_id WHERE ur.role_id=" . SITE_MANAGER . "  AND u.active=1 AND u.delete_status=1")->num_rows();
                    $data['page'] = 'users/auth/' . underscore($currentUserRole->name);
                    break;

                case SITE_MANAGER:
                    $data['institution_count'] = $this->db->query("SELECT * from containers where status=1 AND status_delete=1")->num_rows();
                    $data['courses_count'] = $this->db->query("SELECT * from courses where status=1 AND delete_status=1")->num_rows();
                    $data['page'] = 'users/auth/' . underscore($currentUserRole->name);
                    break;

                case GRADE_ADMINISTRATOR:
                    $data['institution_count'] = $this->db->query("SELECT * from containers where status=1 AND status_delete=1")->num_rows();
                    $data['users_count'] = $this->db->query("SELECT * from users where active=1 AND delete_status=1")->num_rows();
                    $data['courses_count'] = $this->db->query("SELECT * from courses where status=1 AND delete_status=1")->num_rows();
                    $data['system_admin_count'] = $this->db->query("SELECT * FROM `users_roles` as ur JOIN users as u ON u.id=ur.user_id WHERE ur.role_id=" . SYSTEM_ADMIN . " AND u.active=1 AND u.delete_status=1")->num_rows();
                    $data['manager_count'] = $this->db->query("SELECT * FROM `users_roles` as ur JOIN users as u ON u.id=ur.user_id WHERE ur.role_id=" . SITE_MANAGER . "  AND u.active=1 AND u.delete_status=1")->num_rows();
                    $data['page'] = 'users/auth/' . underscore($currentUserRole->name);
                    break;

                default:
                    $data['page'] = 'users/auth/index';
            }
            //$data['page']               = 'users/auth/index';

            $this->template->template_view($data);
        }
    }

    // log the user in
    public function login()
    {
        $data['title'] = $this->lang->line('login_heading');

        //validate form input
        $this->form_validation->set_rules('identity', str_replace(':', '', $this->lang->line('login_identity_label')), 'required');
        $this->form_validation->set_rules('password', str_replace(':', '', $this->lang->line('login_password_label')), 'required');

        if ($this->form_validation->run() == true) {
            $identity = $this->input->post('identity');
            $password = $this->input->post('password');

            // check to see if the user is logging in
            // check for "remember me"
            $remember = (bool) $this->input->post('remember');

            if ($this->ion_auth->login(trim($this->input->post('identity')), trim($this->input->post('password')), $remember)) {
                $check = $this->common_model->getAllData('settings', 'two_factor_auth');

                // checking if Two  Factor Authentication is Enable
                if ($check[0]->two_factor_auth == 1) {
                    //if the login is successful
                    //redirect them back to the home page
                    $destry = array('identity', 'email', 'user_id', 'old_last_login', 'last_check');

                    $this->session->unset_userdata($destry);

                    $string = mt_rand(100000, 999999);

                    $newdata = array(
                        'new_email' => $identity,
                        'pass' => $password,
                        'remember' => $remember,
                        'verification_code' => $string,
                    );

                    $this->session->set_userdata($newdata);

                    $Message = 'Hi Dear, <br> Your Verification code is: <br> <h3>' . $string . '</h3> <br><br> Thanks';

                    $config = array(
                        'mailtype' => 'html',
                        'charset' => 'utf-8',
                        'wordwrap' => true,
                    );

                    $this->load->library('email', $config);

                    $this->email->from($this->config->item('admin_email', 'ion_auth'), $this->config->item('site_title', 'ion_auth'));
                    $this->email->to($identity);
                    $this->email->subject('Verification');
                    $this->email->message($Message);
                    $send = $this->email->send();

                    if ($send) {
                        $msg = 'Please Check Your Email to Verify your Account';
                        $this->session->set_flashdata('success', $msg);
                        redirect('users/Auth/authentication', 'refresh');
                    } else {
                        $msg = 'Email Can not Send';
                        $this->session->set_flashdata('error', $msg);
                        redirect('users/auth/login', 'refresh');
                    }
                } else {
                    $this->session->set_flashdata('message', $this->ion_auth->messages());
                    redirect('users/auth/', 'refresh');
                }
            } else {
                // if the login was un-successful
                // redirect them back to the login page

                $this->session->set_flashdata('message', $this->ion_auth->errors());
                redirect('users/auth/login', 'refresh'); // use redirects instead of loading views for compatibility with MY_Controller libraries
            }
        } else {
            // the user is not logging in so display the login page
            // set the flash data error message if there is one
            $data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            $data['identity'] = array(
                'name' => 'identity',
                'id' => 'identity',
                'type' => 'text',
                'value' => $this->form_validation->set_value('identity'),
            );
            $data['password'] = array(
                'name' => 'password',
                'id' => 'password',
                'type' => 'password',
            );

            $data['reg_status'] = $this->common_model->select('settings');

            $this->load->config('social_auth_config');

            //Twitter API Configuration
            $consumerKey = $this->config->item('client_id');
            $consumerSecret = $this->config->item('secret_id');
            $oauthCallback = $this->config->item('call_back');

            // Google Project API Credentials
            $clientId = $this->config->item('google_client_id');
            $clientSecret = $this->config->item('google_secret_id');
            $redirectUrl = $this->config->item('google_call_back');

            // Google Client Configuration
            $gClient = new Google_Client();
            $gClient->setClientId($clientId);
            $gClient->setClientSecret($clientSecret);
            $gClient->setRedirectUri($redirectUrl);
            $google_oauthV2 = new Google_Oauth2Service($gClient);

            if (isset($_REQUEST['code'])) {
                $gClient->authenticate();
                $this->session->set_userdata('token', $gClient->getAccessToken());
                redirect($redirectUrl);
            }

            //Twitter Configuration

            // unset token and token secret from session
            $this->session->unset_userdata('token');
            $this->session->unset_userdata('token_secret');

            // Fresh authentication
            $connection = new TwitterOAuth($consumerKey, $consumerSecret);
            $requestToken = $connection->getRequestToken($oauthCallback);

            // Received token info from twitter
            $this->session->set_userdata('token', $requestToken['oauth_token']);
            $this->session->set_userdata('token_secret', $requestToken['oauth_token_secret']);

            // Any value other than 200 is failure, so continue only if http code is 200
            if ($connection->http_code == '200') {
                // redirect user to twitter
                $twitterUrl = $connection->getAuthorizeURL($requestToken['oauth_token']);
                $data['oauthURL'] = $twitterUrl;
            } else {
                $data['oauthURL'] = base_url() . 'social_login';
                $data['error_msg'] = 'Error connecting to twitter! try again later!';
            }

            //google login url
            $data['authUrl'] = $gClient->createAuthUrl();

            // facebook login URL
            $data['fbUrl'] = $this->facebook->login_url();

            //passing linkedin credentials to view
            $data['client_id'] = $this->config->item('linkedin_client_id');
            $data['client_secret'] = $this->config->item('linkedin_client_secret');
            $data['redirect_uri'] = $this->config->item('linkedin_redirect_uri');
            $data['csrf_token'] = $this->config->item('linkedin_csrf_token');
            $data['scopes'] = $this->config->item('linkedin_scopes');

            $data['reg_email'] = $this->config->item('reg_status');

            $this->session->set_flashdata('message', $this->ion_auth->messages());

            // $data['page'] = "users/auth/login";
            //     $this->template->template_view($data);
            $this->load->view('users/auth/login', $data);
        }
    }

    /*
    Two Factor Authentication
     */
    public function Authentication()
    {
        if ($this->input->post()) {
            // validate form input
            $this->form_validation->set_rules('code', 'Code', 'trim|required|numeric');

            if ($this->form_validation->run() === true) {
                $code = $this->input->post('code');

                if ($code == $this->session->userdata('verification_code')) {
                    if ($this->ion_auth->login($this->session->userdata('new_email'), $this->session->userdata('pass'), $this->session->userdata('remember'))) {
                        $destry = array('new_email', 'pass', 'remember', 'verification_code');

                        $this->session->unset_userdata($destry);

                        $msg = 'Your Verification Completed Successfully';
                        $this->session->set_flashdata('success', $msg);
                        redirect('users/auth/', 'refresh');
                    }
                } else {
                    $msg = 'You have Enter wrong Code, Please Try Again';
                    $this->session->set_flashdata('error', $msg);
                    redirect('users/Auth/Authentication', 'refresh');
                }
            } else {
                $data['message'] = validation_errors();
                view('two_factor_auth');
            }
        } else {
            view('users/auth/two_factor_auth');
        }
    }

    // log the user out
    public function logout()
    {
        $data['title'] = 'Logout';

        $this->session->unset_userdata('token');
        $this->session->unset_userdata('userData');
        $this->session->unset_userdata('token_secret');
        $this->session->unset_userdata('status');
        // $this->facebook->destroy_session();

        // log the user out
        // redirect them to the login page
        if ($this->ion_auth->logout()) {
            redirect('users/auth/login');
        }
    }

    // change password
    public function change_password()
    {

        $this->form_validation->set_rules('old', $this->lang->line('change_password_validation_old_password_label'), 'required');
        $this->form_validation->set_rules('new', $this->lang->line('change_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
        $this->form_validation->set_rules('new_confirm', $this->lang->line('change_password_validation_new_password_confirm_label'), 'required');

        if (!$this->ion_auth->logged_in()) {
            redirect('users/auth/login', 'refresh');
        }

        $user = $this->ion_auth->user()->row();

        if ($this->form_validation->run() == false) {
            // display the form
            // set the flash data error message if there is one
            $data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            $data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');

            $data['old_password'] = array(
                'name' => 'old',
                'id' => 'old',
                'type' => 'password',
                'class' => 'form-control',
                'required' => 'required',
            );
            $data['new_password'] = array(
                'name' => 'new',
                'id' => 'new',
                'class' => 'form-control',
                'type' => 'password',
                'required' => 'required',
                'minlength' => 8,
                'pattern' => '^.{' . $data['min_password_length'] . '}.*$',
            );
            $data['new_password_confirm'] = array(
                'name' => 'new_confirm',
                'id' => 'new_confirm',
                'class' => 'form-control',
                'type' => 'password',
                'required' => 'required',
                'pattern' => '^.{' . $data['min_password_length'] . '}.*$',
            );
            $data['user_id'] = array(
                'name' => 'user_id',
                'id' => 'user_id',
                'type' => 'hidden',
                'value' => $user->id,
            );

            // render
            $data['page'] = 'users/users/user_profile';
            redirect('users/Profile', 'refresh');
        } else {
            $identity = $this->session->userdata('identity');
            $change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'));
            //pr("ffff","fdff");
            if ($change) {
                //if the password was successfully changed
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                redirect('users/auth/logout', 'refresh');
            } else {
                $this->session->set_flashdata('message', $this->ion_auth->errors());
                redirect('users/Profile', 'refresh');
            }
        }
    }

    // forgot password
    public function forgot_password()
    {
        // setting validation rules by checking whether identity is username or email
        if ($this->config->item('identity', 'ion_auth') != 'email') {
            $this->form_validation->set_rules('identity', $this->lang->line('forgot_password_identity_label'), 'required');
        } else {
            $this->form_validation->set_rules('identity', $this->lang->line('forgot_password_validation_email_label'), 'required|valid_email');
        }



        if ($this->form_validation->run() == false) {

            $data['type'] = $this->config->item('identity', 'ion_auth');
            // setup the input
            $data['identity'] = array(
                'name' => 'identity',
                'id' => 'identity',
                'class' => 'form-control',
                'required' => 'required',
            );

            if ($this->config->item('identity', 'ion_auth') != 'email') {
                $data['identity_label'] = $this->lang->line('forgot_password_identity_label');
            } else {
                $data['identity_label'] = $this->lang->line('forgot_password_email_identity_label');
            }

            // set any errors and display the form
            $data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            $this->_render_page('users/auth/forgot_password', $data);
        } else {

            $identity_column = $this->config->item('identity', 'ion_auth');
            $identity = $this->ion_auth->where($identity_column, $this->input->post('identity'))->users()->row();

            if (empty($identity)) {
                if ($this->input->post('type') == 'ajax') {
                    echo json_encode(array('status' => 400));
                    exit;
                }
                if ($this->config->item('identity', 'ion_auth') != 'email') {
                    $this->ion_auth->set_error('forgot_password_identity_not_found');
                } else {
                    $this->ion_auth->set_error('forgot_password_email_not_found');
                }
                $this->session->set_flashdata('message', $this->ion_auth->errors());
                redirect('users/auth/forgot_password', 'refresh');
            }
            // run the forgotten password method to email an activation code to the user
            $forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});

            //pr($forgotten); exit;

            if ($forgotten) {

                // if there were no errors
                if ($this->input->post('type') == 'ajax') {
                    echo json_encode(array('status' => 200));
                    exit;
                } else {
                    $this->session->set_flashdata('message', $this->ion_auth->messages());
                    redirect('users/auth/login', 'refresh'); //we should display a confirmation page here instead of the login page
                }
            } else {
                if ($this->input->post('type') == 'ajax') {
                    echo json_encode(array('status' => 400));
                    exit;
                } else {
                    $this->session->set_flashdata('message', $this->ion_auth->errors());
                    redirect('users/auth/forgot_password', 'refresh');
                }
            }
        }
    }

    // reset password - final step for forgotten password
    public function reset_password($code = null)
    {

        if (!$code) {
            show_404();
        }

        $user = $this->ion_auth->forgotten_password_check($code);

        if ($user) {
            // if the code is valid then display the password reset form

            $this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
            $this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');

            if ($this->form_validation->run() == false) {
                // display the form

                // set the flash data error message if there is one
                $data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

                $data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
                $data['new_password'] = array(
                    'name' => 'new',
                    'id' => 'new',
                    'type' => 'password',
                    'class' => 'form-control',
                    'minlength' => '8',
                    'maxlength' => '20',
                    'required' => 'required',
                    'pattern' => '^.{' . $data['min_password_length'] . '}.*$',
                );
                $data['new_password_confirm'] = array(
                    'name' => 'new_confirm',
                    'id' => 'new_confirm',
                    'type' => 'password',
                    'class' => 'form-control',
                    'required' => 'required',
                    'pattern' => '^.{' . $data['min_password_length'] . '}.*$',
                );
                $data['user_id'] = array(
                    'name' => 'user_id',
                    'id' => 'user_id',
                    'type' => 'hidden',
                    'value' => $user->id,
                );
                $data['csrf'] = $this->_get_csrf_nonce();
                $data['code'] = $code;

                // render
                $this->_render_page('users/auth/reset_password', $data);
            } else {
                // do we have a valid request?
                if ($this->_valid_csrf_nonce() === false || $user->id != $this->input->post('user_id')) {
                    // something fishy might be up
                    $this->ion_auth->clear_forgotten_password_code($code);
                    show_error($this->lang->line('error_csrf'));
                } else {
                    // finally change the password
                    $identity = $user->{$this->config->item('identity', 'ion_auth')};
                    $change = $this->ion_auth->reset_password($identity, $this->input->post('new'));
                    if ($change) {
                        // if the password was successfully changed
                        $this->session->set_flashdata('message', $this->ion_auth->messages());
                        redirect('users/auth/login', 'refresh');
                    } else {
                        $this->session->set_flashdata('message', $this->ion_auth->errors());
                        redirect('users/auth/reset_password/' . $code, 'refresh');
                    }
                }
            }
        } else {
            // if the code is invalid then send them back to the forgot password page
            $this->session->set_flashdata('message', $this->ion_auth->errors());
            redirect('users/auth/forgot_password', 'refresh');
        }
    }

    // activate the user
    public function activate($id, $code = false)
    {
        if ($code !== false) {
            $activation = $this->ion_auth->activate($id, $code);
        } elseif ($this->ion_auth->is_admin()) {
            $activation = $this->ion_auth->activate($id);
        }

        if ($activation) {
            // redirect them to the auth page
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect('auth', 'refresh');
        } else {
            // redirect them to the forgot password page
            $this->session->set_flashdata('message', $this->ion_auth->errors());
            redirect('users/auth/forgot_password', 'refresh');
        }
    }

    // deactivate the user
    public function deactivate($id = null)
    {
        /* if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            // redirect them to the home page because they must be an administrator to view this
            // return show_error('You must be an administrator to view this page.');
            $msg = 'You must be an administrator to view this page.';
            $this->session->set_flashdata('error', $msg);
            redirect('users', 'refresh');
        }
*/
        if ($this->session->userdata('user_id') == $id) {
            return show_error("You Can't De-Activated Logged In User");
        }

        $id = (int) $id;

        $this->load->library('form_validation');
        $this->form_validation->set_rules('confirm', $this->lang->line('deactivate_validation_confirm_label'), 'required');
        $this->form_validation->set_rules('id', $this->lang->line('deactivate_validation_user_id_label'), 'required|alpha_numeric');

        if ($this->form_validation->run() == false) {
            // insert csrf check
            $data['csrf'] = $this->_get_csrf_nonce();
            $data['user'] = $this->ion_auth->user($id)->row();

            $data['page'] = 'users/auth/deactivate_user';
            $this->template->template_view($data);
            // $this->_render_page('dashboard', $data);
        } else {
            // do we really want to deactivate?
            if ($this->input->post('confirm') == 'yes') {
                // do we have a valid request?
                if ($id != $this->input->post('id')) {
                    show_error($this->lang->line('error_csrf'));
                }
                $this->ion_auth->deactivate($id);
                // do we have the right userlevel?

            }
            // redirect them back to the auth page
            $this->session->set_flashdata('message', $this->ion_auth->messages());

            redirect('users', 'refresh');
        }
    }

    // create a new user
    public function create_user()
    {
        $data['title'] = $this->lang->line('create_user_heading');

        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            redirect('auth', 'refresh');
        }

        $tables = $this->config->item('tables', 'ion_auth');
        $identity_column = $this->config->item('identity', 'ion_auth');
        $this->data['identity_column'] = $identity_column;

        // validate form input
        $this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'required');
        $this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'required');
        if ($identity_column !== 'email') {
            $this->form_validation->set_rules('identity', $this->lang->line('create_user_validation_identity_label'), 'required|is_unique[' . $tables['users'] . '.' . $identity_column . ']');
            $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email');
        } else {
            $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email|is_unique[' . $tables['users'] . '.email]');
        }
        $this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone_label'), 'trim');
        $this->form_validation->set_rules('company', $this->lang->line('create_user_validation_company_label'), 'trim');
        $this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
        $this->form_validation->set_rules('password_confirm', $this->lang->line('create_user_validation_password_confirm_label'), 'required');

        if ($this->form_validation->run() == true) {
            $email = strtolower($this->input->post('email'));
            $identity = ($identity_column === 'email') ? $email : $this->input->post('identity');
            $password = $this->input->post('password');

            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'company' => $this->input->post('company'),
                'phone' => $this->input->post('phone'),
            );
        }
        if ($this->form_validation->run() == true && $this->ion_auth->register($identity, $password, $email, $additional_data)) {
            // check to see if we are creating the user
            // redirect them back to the admin page
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect('auth', 'refresh');
        } else {
            // display the create user form
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

            $this->data['first_name'] = array(
                'name' => 'first_name',
                'id' => 'first_name',
                'type' => 'text',
                'value' => $this->form_validation->set_value('first_name'),
            );
            $this->data['last_name'] = array(
                'name' => 'last_name',
                'id' => 'last_name',
                'type' => 'text',
                'value' => $this->form_validation->set_value('last_name'),
            );
            $this->data['identity'] = array(
                'name' => 'identity',
                'id' => 'identity',
                'type' => 'text',
                'value' => $this->form_validation->set_value('identity'),
            );
            $this->data['email'] = array(
                'name' => 'email',
                'id' => 'email',
                'type' => 'text',
                'value' => $this->form_validation->set_value('email'),
            );
            $this->data['company'] = array(
                'name' => 'company',
                'id' => 'company',
                'type' => 'text',
                'value' => $this->form_validation->set_value('company'),
            );
            $this->data['phone'] = array(
                'name' => 'phone',
                'id' => 'phone',
                'type' => 'text',
                'value' => $this->form_validation->set_value('phone'),
            );
            $this->data['password'] = array(
                'name' => 'password',
                'id' => 'password',
                'type' => 'password',
                'value' => $this->form_validation->set_value('password'),
            );
            $this->data['password_confirm'] = array(
                'name' => 'password_confirm',
                'id' => 'password_confirm',
                'type' => 'password',
                'value' => $this->form_validation->set_value('password_confirm'),
            );

            $this->_render_page('auth/create_user', $this->data);
        }
    }

    // edit a user
    public function edit_user($id)
    {
        $this->data['title'] = $this->lang->line('edit_user_heading');

        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id))) {
            redirect('auth', 'refresh');
        }

        $user = $this->ion_auth->user($id)->row();
        $roles = $this->ion_auth->roles()->result_array();
        $currentroles = $this->ion_auth->get_users_roles($id)->result();

        // validate form input
        $this->form_validation->set_rules('first_name', $this->lang->line('edit_user_validation_fname_label'), 'required');
        $this->form_validation->set_rules('last_name', $this->lang->line('edit_user_validation_lname_label'), 'required');
        $this->form_validation->set_rules('phone', $this->lang->line('edit_user_validation_phone_label'), 'required');
        $this->form_validation->set_rules('company', $this->lang->line('edit_user_validation_company_label'), 'required');

        if (isset($_POST) && !empty($_POST)) {
            // do we have a valid request?
            if ($this->_valid_csrf_nonce() === false || $id != $this->input->post('id')) {
                show_error($this->lang->line('error_csrf'));
            }

            // update the password if it was posted
            if ($this->input->post('password')) {
                $this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
                $this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
            }

            if ($this->form_validation->run() === true) {
                $data = array(
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'company' => $this->input->post('company'),
                    'phone' => $this->input->post('phone'),
                );

                // update the password if it was posted
                if ($this->input->post('password')) {
                    $data['password'] = $this->input->post('password');
                }

                // Only allow updating roles if user is admin
                if ($this->ion_auth->is_admin()) {
                    //Update the roles user belongs to
                    $roleData = $this->input->post('roles');

                    if (isset($roleData) && !empty($roleData)) {
                        $this->ion_auth->remove_from_role('', $id);

                        foreach ($roleData as $grp) {
                            $this->ion_auth->add_to_role($grp, $id);
                        }
                    }
                }

                // check to see if we are updating the user
                if ($this->ion_auth->update($user->id, $data)) {
                    // redirect them back to the admin page if admin, or to the base url if non admin
                    $this->session->set_flashdata('message', $this->ion_auth->messages());
                    if ($this->ion_auth->is_admin()) {
                        redirect('auth', 'refresh');
                    } else {
                        redirect('/', 'refresh');
                    }
                } else {
                    // redirect them back to the admin page if admin, or to the base url if non admin
                    $this->session->set_flashdata('message', $this->ion_auth->errors());
                    if ($this->ion_auth->is_admin()) {
                        redirect('auth', 'refresh');
                    } else {
                        redirect('/', 'refresh');
                    }
                }
            }
        }

        // display the edit user form
        $this->data['csrf'] = $this->_get_csrf_nonce();

        // set the flash data error message if there is one
        $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

        // pass the user to the view
        $this->data['user'] = $user;
        $this->data['roles'] = $roles;
        $this->data['currentroles'] = $currentroles;

        $this->data['first_name'] = array(
            'name' => 'first_name',
            'id' => 'first_name',
            'type' => 'text',
            'value' => $this->form_validation->set_value('first_name', $user->first_name),
        );
        $this->data['last_name'] = array(
            'name' => 'last_name',
            'id' => 'last_name',
            'type' => 'text',
            'value' => $this->form_validation->set_value('last_name', $user->last_name),
        );
        $this->data['username'] = array(
            'name' => 'username',
            'id' => 'username',
            'type' => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('last_name', $user->username),
        );
        $this->data['last_name'] = array(
            'name' => 'last_name',
            'id' => 'last_name',
            'type' => 'text',
            'value' => $this->form_validation->set_value('last_name', $user->last_name),
        );
        $this->data['company'] = array(
            'name' => 'company',
            'id' => 'company',
            'type' => 'text',
            'value' => $this->form_validation->set_value('company', $user->company),
        );
        $this->data['phone'] = array(
            'name' => 'phone',
            'id' => 'phone',
            'type' => 'text',
            'value' => $this->form_validation->set_value('phone', $user->phone),
        );
        $this->data['password'] = array(
            'name' => 'password',
            'id' => 'password',
            'type' => 'password',
        );
        $this->data['password_confirm'] = array(
            'name' => 'password_confirm',
            'id' => 'password_confirm',
            'type' => 'password',
        );

        $this->_render_page('auth/edit_user', $this->data);
    }

    // create a new role
    public function create_role()
    {
        $this->data['title'] = $this->lang->line('create_role_title');

        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            redirect('auth', 'refresh');
        }

        // validate form input
        $this->form_validation->set_rules('role_name', $this->lang->line('create_role_validation_name_label'), 'required|alpha_dash');

        if ($this->form_validation->run() == true) {
            $new_role_id = $this->ion_auth->create_role($this->input->post('role_name'), $this->input->post('description'));
            if ($new_role_id) {
                // check to see if we are creating the role
                // redirect them back to the admin page
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                redirect('auth', 'refresh');
            }
        } else {
            // display the create role form
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

            $this->data['role_name'] = array(
                'name' => 'role_name',
                'id' => 'role_name',
                'type' => 'text',
                'value' => $this->form_validation->set_value('role_name'),
            );
            $this->data['description'] = array(
                'name' => 'description',
                'id' => 'description',
                'type' => 'text',
                'value' => $this->form_validation->set_value('description'),
            );

            $this->_render_page('auth/create_role', $this->data);
        }
    }

    // edit a role
    public function edit_role($id)
    {
        // bail if no role id given
        if (!$id || empty($id)) {
            redirect('auth', 'refresh');
        }

        $this->data['title'] = $this->lang->line('edit_role_title');

        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            redirect('auth', 'refresh');
        }

        $role = $this->ion_auth->role($id)->row();

        // validate form input
        $this->form_validation->set_rules('role_name', $this->lang->line('edit_role_validation_name_label'), 'required|alpha_dash');

        if (isset($_POST) && !empty($_POST)) {
            if ($this->form_validation->run() === true) {
                $role_update = $this->ion_auth->update_role($id, $_POST['role_name'], $_POST['role_description']);

                if ($role_update) {
                    $this->session->set_flashdata('message', $this->lang->line('edit_role_saved'));
                } else {
                    $this->session->set_flashdata('message', $this->ion_auth->errors());
                }
                redirect('auth', 'refresh');
            }
        }

        // set the flash data error message if there is one
        $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

        // pass the user to the view
        $this->data['role'] = $role;

        $readonly = $this->config->item('admin_role', 'ion_auth') === $role->name ? 'readonly' : '';

        $this->data['role_name'] = array(
            'name' => 'role_name',
            'id' => 'role_name',
            'type' => 'text',
            'value' => $this->form_validation->set_value('role_name', $role->name),
            $readonly => $readonly,
        );
        $this->data['role_description'] = array(
            'name' => 'role_description',
            'id' => 'role_description',
            'type' => 'text',
            'value' => $this->form_validation->set_value('role_description', $role->description),
        );

        $this->_render_page('auth/edit_role', $this->data);
    }

    public function login_usage()
    {
        $this->data['page'] = 'auth/login_usage';
        $this->load->view('dashboard', $this->data);
    }

    public function _get_csrf_nonce()
    {
        $this->load->helper('string');
        $key = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }

    public function _valid_csrf_nonce()
    {
        $csrfkey = $this->input->post($this->session->flashdata('csrfkey'));
        if ($csrfkey && $csrfkey == $this->session->flashdata('csrfvalue')) {
            return true;
        } else {
            return false;
        }
    }

    public function _render_page($view, $data = null, $returnhtml = false) //I think this makes more sense

    {
        $this->viewdata = (empty($data)) ? $this->data : $data;

        $view_html = $this->load->view($view, $this->viewdata, $returnhtml);

        if ($returnhtml) {
            return $view_html;
        } //This will return html on 3rd argument being true
    }
}
