<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Tel Library System</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="shortcut icon" href="<?php bs() ?>public/assets/img/favcon.png"/>

        <link type='text/css' href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600' rel='stylesheet'>
        <link type="text/css" href="<?php bs('public/assets/fonts/flaticon/flaticon.css') ?>" rel="stylesheet">
        <link type="text/css" href="<?php bs('public/assets/fonts/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet">        <!-- Font Awesome -->
        <link type="text/css" href="<?php bs('public/assets/fonts/themify-icons/themify-icons.css') ?>" rel="stylesheet">              <!-- Themify Icons -->
        <link type="text/css" href="<?php bs('public/assets/css/bootstrap-new-grid.css') ?>" rel="stylesheet">                                     <!-- Core CSS with all styles --->
        <link type="text/css" href="<?php bs('public/assets/css/styles.css') ?>" rel="stylesheet">                                     <!-- Core CSS with all styles -->

        <link type="text/css" href="<?php bs('public/assets/css/custom.css') ?>" rel="stylesheet">

        <link type="text/css" href="<?php bs('public/assets/plugins/codeprettifier/prettify.css') ?>" rel="stylesheet">                <!-- Code Prettifier -->
        <link type="text/css" href="<?php bs('public/assets/plugins/iCheck/skins/minimal/blue.css') ?>" rel="stylesheet">              <!-- iCheck -->
        <link type="text/css" href="<?= base_url('public/assets/css/animate.css') ?>" rel="stylesheet">              
        <!-- Animate css -->

        <link type="text/css" href="<?php bs('public/assets/plugins/switchery/switchery.css') ?>" rel="stylesheet">   							<!-- Switchery -->

        <link type="text/css" href="<?php bs('public/assets/css/mystyle.css') ?>" rel="stylesheet">   

<!-- 	<link type="text/css" href="<?php //bs('public/assets/plugins/progress-skylo/skylo.css')        ?>" rel="stylesheet">  -->	
        <!-- Skylo -->

        <!-- Custom Checkboxes / iCheck -->
        <link type="text/css" href="<?php bs('public/assets/plugins/iCheck/skins/flat/_all.css') ?>" rel="stylesheet">
        <link type="text/css" href="<?php bs('public/assets/plugins/iCheck/skins/square/_all.css') ?>" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

        <link type="text/css" href="<?php bs('public/assets/css/jquery-ui.min-1.12.1.css') ?>" rel="stylesheet">



        <link type="text/css" href="<?php bs('public/assets/plugins/jvectormap/jquery-jvectormap-2.0.2.css') ?>" rel="stylesheet"> 			<!-- jVectorMap -->

        <link type="text/css" href="<?php bs('public/assets/css/build.css') ?>" rel="stylesheet"> 			<!-- jVectorMap -->

        <link type="text/css" href="<?php bs('public/assets/css/fancymetags.css') ?>" rel="stylesheet"> 

        <script type="text/javascript" src="<?= base_url('public/assets/js/jquery-1.10.2.min.js') ?>"></script> 
   
        <script type="text/javascript" src="<?php echo base_url('public/assets/js/jquery.validate.min.js') ?>"></script> 
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.min.js"></script>


        <script type="text/javascript" src="<?php bs('public/assets/js/comman.js') ?>"></script>
        <?php
        $jsVarsComman = [
            'ajax_call_root' => base_url(),
            'notification' => [
                'success' => $this->session->flashdata('success'),
                'error' => $this->session->flashdata('error')
            ]
        ];
        ?>
        <script>
            jQuery(document).ready(function () {
                CommanJS.init(<?php echo json_encode($jsVarsComman); ?>);
            });

        </script> 

        <script>
            $(document).ready(function () {
                $.ajax({
                    url: "<?php echo bs(); ?>Notifications/getUserNotification",
                    context: document.body,
                    dataType: 'json',
                    success: function (data) {
                        $('#notificationArr').val(data.notificationArr);
                        $('#notificationCount').html(data.count);
                        $('.notificationPanel').removeClass('scroll-pane');
                        $('.notificationPanel').css('width', '288px');
                        $('.notificationPanel').removeClass('has-scrollbar');
                        $('.notificationPanel').css('height', '');
                        $('.notificationPanel').empty().html(data.html);
                        $('.alertNotification').css('border', 'solid 1px');

                    }});


                $.ajax({
                    url: "<?php echo bs(); ?>Messages/getUserMessages",
                    context: document.body,
                    dataType: 'json',
                    success: function (data) {
                        
                      //  console.log(data);
                        
                        
                        
                        $('#messageArr').val(data.messageArr);
                        $('.messageCount').html(data.count);
                        $('.MessagePanel').removeClass('scroll-pane');
                        $('.MessagePanel').css('width', '288px');
                        $('.MessagePanel').removeClass('has-scrollbar');
                        $('.MessagePanel').css('height', '');
                        $('.MessagePanel').empty().html(data.html);
                        $('.alertMessage').css('border', 'solid 1px');

                    }});


            });
        </script> 

        <script>

            $(document).on('click', '#notificationIcon', function () {
                var notificationIds = $('#notificationArr').val();
                $.ajax({
                    url: '<?php echo bs(); ?>Notifications/readNotifications',
                    type: 'post',
                    data: 'ids=' + notificationIds,
                    dataType: 'json',
                    success: function (data) {
                         $('#notificationCount').html(0);
                    }});

            });
            
             $(document).on('click', '#messageIcon', function () {
//                var notificationIds = $('#messageArr').val();
//                $.ajax({
//                    url: '<?php echo bs(); ?>Messages/readMessages',
//                    type: 'post',
//                    data: 'ids=' + notificationIds,
//                    dataType: 'json',
//                    success: function (data) {
//                        console.log(data);
//                    }});

            });

        </script>

    <a href="javascript:;" id="demoskylo"></a>

    <!-- Load jQuery -->

    <style>
        .success-noty
        {
            background-color: #8bc34a;
            color: white;
        }
        .error-noty
        {
            background-color: #dd191d;
            color:white;
        }
        .error
        {
            color: red;
        }
    </style>
    <link type="text/css" href="<?= bs('public/assets/plugins/form-select/select2.css') ?>" rel="stylesheet">
</head>

<body class="animated-content">

    <header id="topnav" class="navbar navbar-default navbar-fixed-top" role="banner">

        <div class="logo-area">

            <span id="trigger-sidebar" class="toolbar-trigger toolbar-icon-bg">
                <a data-toggle="tooltips" data-placement="right" title="Toggle Sidebar">
                    <span  class="icon-bg">
                        <i class="ti ti-menu"></i>
                    </span>
                </a>
            </span>
            <a class="navbar" href="<?php bs(); ?>">
                <img src="<?php bs() ?>/public/assets/img/tel-logo.png" class="img-responsive" />
            </a>
        </div><!-- logo-area -->

        <ul class="nav navbar-nav toolbar pull-right">

            <!-- <li class="toolbar-icon-bg visible-xs-block" id="trigger-toolbar-search">
                    <a href="#"><span class="icon-bg"><i class="ti ti-search"></i></span></a>
            </li> -->

            <!--<li class="dropdown toolbar-icon-bg hidden-xs">
                <a href="#" class="hasnotifications dropdown-toggle" data-toggle='dropdown'><span  id="messageIcon"  class="icon-bg"><i class="ti ti-email"></i></span><span 
                        class="badge badge-deeporange messageCount">0</span></a>
                <div class="dropdown-menu notifications arrow">
                    <div class="topnav-dropdown-header">
                        <span>Messages</span>
                    </div>
                    <div class="scroll-pane MessagePanel">
                        <ul class="media-list scroll-content">

                            <li class="media notification-message">
                                <div class="alert alert-light-warning m-0 pt-5 pb-5 text-center alertMessage">No messages</div>
                            </li>
                        </ul>
                    </div>
                    <div class="topnav-dropdown-footer">
                        <a href="<?//php echo bs() ?>messages/messages/received_messages">See all messages</a>
                    </div>
                </div>
            </li> -->

            <li class="dropdown toolbar-icon-bg">
                <input type="hidden" id="notificationArr" value="">
                <input type="hidden" id="messageArr" value="">
                <a href="#" class="hasnotifications dropdown-toggle" data-toggle='dropdown'><span id="notificationIcon" class="icon-bg"><i class="ti ti-bell"></i></span><span id="notificationCount" class="badge badge-deeporange">0</span></a>
                <div class="dropdown-menu notifications arrow">
                    <div class="topnav-dropdown-header">
                        <span>Notifications</span>
                    </div>
                    <div class="scroll-pane notificationPanel">
                        <ul class="media-list scroll-content">

                            <li class="media notification-message " >
                                <div class="alert alert-light-warning m-0 pt-5 pb-5 text-center alertNotification">No notifications</div>
                            </li>

                        </ul>
                    </div>
                    <div class="topnav-dropdown-footer">
                        <a href="<?php echo bs() ?>notifications">See all notifications</a>
                    </div>
                </div>
            </li>

            <?php $user = $this->ion_auth->user()->row(); ?>

            <li class="dropdown toolbar-icon-bg">
                <a href="#" class="dropdown-toggle username" data-toggle="dropdown">
                    <?php
                    if (empty($user->user_img)) {
                        ?>
                        <img src="<?php bs() ?>public/assets/img/default_user.png" class="img-responsive img-circle" width="200" alt="">
                        <?php
                    } else {
                        ?>
                        <img src="<?php bs($user->user_img) ?>" class="img-responsive img-circle" width="200" alt="">
                        <?php
                    }
                    ?> 
                </a>
                <ul class="dropdown-menu userinfo arrow">
                    <li>
                        <a href="<?php bs('users/profile') ?>">
                            <i class="ti ti-user"></i><span><?php echo currentUser()->first_name; ?> <?php echo currentUser()->middle_name; ?> <?php echo currentUser()->last_name; ?></span>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="<?= base_url('users/auth/logout') ?>">
                            <i class="ti ti-shift-right"></i><span>Sign Out</span>
                        </a>
                    </li>
                </ul>
            </li>

        </ul>

    </header>



