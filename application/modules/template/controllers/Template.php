<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template extends MY_Controller 
{

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		
	}

	public function template_view($data = NULL)
	{
        $this->load->model(array('users/users_modal', 'users/menu_model', ));       
		$data['sidebar_menus'] = $this->menu_model->getCurrentMenu();
		$this->load->view('dashboard',$data);
	}

	public function iframe_view($data = NULL)
	{
        $this->load->model(array('users/users_modal', 'users/menu_model', ));       
		$this->load->view('iframe',$data);
	}
	public function ajax_view($data = NULL)
	{	
        $this->load->model(array('users/users_modal', 'users/menu_model', ));       
		$this->load->view('ajax',$data);
	}

	public function error_404($data = NULL)
	{	
        $this->load->view('error_404');
	}

}

/* End of file Template.php */
/* Location: ./application/modules/template/controllers/Template.php */

/* End of file Template.php */
/* Location: ./application/modules/template/controllers/Template.php */