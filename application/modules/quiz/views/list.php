<div class="container-fluid">
    <div data-widget-group="group1">
        <div class="row">
            <div class="col-md-12">
                <div class="filter-container flex-row"  id="toggleFilterSection">
                    <div class="flex-col-12 flex-col-sm-11 flex-row m-0">
                        <div class="flex-col-3 pl-0" id="selected_system_cat"></div>
                        <div class="flex-col-3" id="selected_system_tag"></div>
                        <div class="flex-col-3" id="selected_assessment_cat"></div>
                        <div class="flex-col-3" id="selected_assessment_tag"></div>
                        <div id="toggleListSection" class="flex-row flex-col-12 m-0 pt-4 pl-0" style="display: none">
                            <!-- <div class="flex-col-3 pb-4 pl-0" id="selected_course_tag"></div>
                            <div class="flex-col-3 pb-4" id="selected_module_tag"></div>
                            <div class="flex-col-3 pb-4" id="selected_lesson_tag"></div> -->
                            <div class="flex-col-3 pb-4  pl-0" id="selected_outcomes_cat"></div>
                      <!--       <div class="flex-col-3 pb-4" id="selected_quiz_type"></div>
                            <div class="flex-col-3 pb-4" id="selected_quiz_level"></div> -->
                            <div class="flex-col-3 pb-4 " id="selected_standard_cat"></div>
                           <!--  <div class="flex-col-3 pb-4" id="selected_competencies_cat"></div>
                            <div class="flex-col-3 pb-4" id="selected_skills_cat"></div> -->
                            <div class="flex-col-sm-1">
                                <button type="button"  class="btn btn-danger btn-block selected_reset_filter">
                                    <span class="fa fa-times"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="flex-col-12 flex-col-sm-1">
                        <button type="button" class="btn btn-default btn-block btn-arrow" data-panel-toggle="toggleListSection">
                          <span class="flaticon-back"></span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default  panel-grid">
                    <div class="panel-heading">
                        <a class="btn btn-primary" href="<?= base_url('quiz/add') ?>">
                            <i class="flaticon-add mr-2 f11"></i> Add new
                        </a>
                        <a class="btn btn-success" href="javascript:void(0)" id="bulk_export">
                            <i class="flaticon-download-1 mr-2 f11"></i> Download selected
                        </a>
                        <a class="btn btn-success" href="javascript:void(0)" id="bulk_export_all">
                            <i class="flaticon-download-1 mr-2 f11"></i> Download all
                        </a>
                        <a class="btn btn-danger" href="javascript:void(0)" id="remove_multiple">
                            <i class="flaticon-trash mr-2 f11"></i> Remove
                        </a>
                        <div class="panel-ctrls"></div>
                    </div>
                    <div class="panel-body pl-0 pr-0">
                        <div class="row m-0">
                            <div class="col-md-12">
                                <table id="selected_quiz_table" class="table"  style="width:100%">
                                    <thead>
                                        <tr>
                                            <th style="min-width: 40px;">
                                                <label class="checkbox-tel">
                                                    <input type="checkbox" name="selected_select_all" id="selected_select_all" class="mr-2 selected_select_all">
                                                </label>
                                            </th>
                                            <th style="min-width: 100px;">Actions</th>
                                            <th style="min-width: 80px;">Status</th>
                                            <th style="min-width: 150px;">Title</th>
                                            <th style="min-width: 150px;">Long Title</th>
                                            <th style="min-width: 150px;">Type</th>
                                            <th style="min-width: 200px;">Assessment Categories</th>
                                            <th style="min-width: 150px;">Assessment Tags</th>
                                            <th style="min-width: 150px;">System Categories</th>
                                            <th style="min-width: 150px;">System Tags</th><!-- 
                                            <th style="min-width: 150px;">Course Tags</th>
                                            <th style="min-width: 150px;">Module Tags</th>
                                            <th style="min-width: 150px;">Lesson Tags</th> -->
                                            <th style="min-width: 150px;">Learning Outcomes</th>
                                            <!-- <th style="min-width: 150px;">Quiz Type</th>
                                            <th style="min-width: 150px;">Quiz Level</th> -->
                                            <th style="min-width: 200px;">TEL Mastery Standards</th><!-- 
                                            <th style="min-width: 200px;">Learning Competencies</th>
                                            <th style="min-width: 150px;">Learning Skills</th> -->
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php bs('public/assets/js/quiz/list.js') ?>"></script>

<?php
$jsVars = [
    'ajax_call_root' => base_url()
];    
?>
<script type="text/javascript">   
   $(document).ready(function() {
      QuizList.init(<?php echo json_encode($jsVars); ?>);
   });  
</script>
