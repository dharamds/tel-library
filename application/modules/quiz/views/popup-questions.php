<div class="row m-0">
	<div class="col-md-12 pb-3 pt-3">
		<div class="panel-form-control flex-row" id="popup_toggle_filters">
			<div class="flex-col-12 flex-col-sm-11 flex-row">
				<div class="flex-col-3 pb-4" id="pop_system_cat"></div>
				<div class="flex-col-3 pb-4" id="pop_system_tag"></div>
				<div class="flex-col-3 pb-4" id="pop_assessment_cat"></div>
				<div class="flex-col-3 pb-4" id="pop_assessment_tag"></div>
				<div id="togglePopupFilterSection" class="flex-row m-0  flex-col-12 pl-0" style="display: none">
					<!-- <div class="flex-col-3 pb-4" id="pop_course_tag"></div>
					<div class="flex-col-3 pb-4" id="pop_module_tag"></div>
					<div class="flex-col-3 pb-4" id="pop_lesson_tag"></div> -->
					<div class="flex-col-3 pb-4" id="pop_outcomes_cat"></div>
					<div class="flex-col-3 pb-4" id="pop_standard_cat"></div>
					<div class="flex-col-3 pb-4" id="pop_bloom_level_cat"></div>
					<!-- <div class="flex-col-3 pb-4" id="pop_competencies_cat"></div>
					<div class="flex-col-3 pb-4" id="pop_skills_cat"></div> -->
					<div class="flex-col-sm-1">
						<button type="button"  class="btn btn-danger btn-block popup_reset_filter">
							<span class="fa fa-times"></span>
						</button>
					</div>
				</div>
			</div>
			<div class="flex-col-12 flex-col-sm-1">
				<button type="button" class="btn btn-default btn-block btn-arrow" data-panel-toggle="togglePopupFilterSection">
					<span class="flaticon-back"></span>
				</button>
			</div>
		</div>
	</div>
	<div class="col-xs-12 p-0">
		<div class="hr-line"></div>
	</div>
	<div class="col-xs-12 p-0 row m-0 pt-3 pb-3">
		<div class="col-md-6 pt-3">
			<a href="javascript:void(0)" class="btn btn-success btn-xs add-questions-to-section">
				<i class="flaticon-add"></i>
				Add To Section
			</a>
		</div>
		<div class="col-md-6" id="sectionOptions">
			<div class="panel-ctrls"></div>
		</div>
	</div>
	<div class="row m-0">
		<div class="col-xs-12 p-0 pb-5">
			<table id="all-active-question-popup-list" class="table">
				<thead>
					<tr>
						<th style="min-width: 50px;">
							<lable class="checkbox-tel">
								 <input type="checkbox" name="popup_select_all" id="popup_select_all" class="mr-2 popup_select_all"> 
							</lable>
						</th>
						<th style="min-width: 200px;">Title</th>
						<th style="min-width: 150px;">Long Title</th>
						<th style="min-width: 200px;">Assessment Categories</th>
						<th style="min-width: 140px;">Assessment Tags</th>
						<th style="min-width: 180px;">System Categories</th>
						<th style="min-width: 120px;">System Tags</th>
						<!-- <th style="min-width: 120px;">Course Tags</th>
						<th style="min-width: 120px;">Module Tags</th>
						<th style="min-width: 120px;">Lesson Tags</th> -->
						<th style="min-width: 180px;">Learning Outcomes</th>
						<th style="min-width: 180px;">TEL Mastery Standards</th>
						<th style="min-width: 180px;">Blooms Level</th>
						<!-- <th style="min-width: 180px;">Learning Competencies</th>
						<th style="min-width: 140px;">Learning Skills</th> -->
						<!-- <th>Used in These Quizzes</th> -->
					</tr>
				</thead>
			</table>
		</div>	
	</div>	


</div>

<script type="text/javascript" src="<?php bs('public/assets/js/quiz/popup-questions.js') ?>"></script>
<?php
//echo $recordID;
$jsVars = [
    'ajax_call_root' => base_url(),
    'quizID' => $quizID,
    'sectionID' => $sectionID,
];    
?>
<script type="text/javascript">   
   $(document).ready(function() {
      PopupQuestions.init(<?php echo json_encode($jsVars); ?>);
      	$("#all-question-popup-list").DataTable({
			"processing": true,
			"iDisplayLength": 10,
			"bPaginate": true,
			"order": [],
			"scrollX": true,
            "autoWidth": false,
		});
   });  
	$("[data-panel-toggle]").each(function(){
	    var $this =  $(this),
	        toggleContainer = $this.attr("data-panel-toggle");
	    $this.click(function(){
	        $("#"+toggleContainer).slideToggle()
	    })
	});     
</script>