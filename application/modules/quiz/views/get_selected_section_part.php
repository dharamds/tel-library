<div class="col-xs-12 pt-3 themePanel icon-left p-0 m-0 mb-3">
	<h4 class="f17">
		<i class="flaticon-discuss-issue mr-2"></i>  <?=$sectionDetails->title?> Question
	</h4>
</div>
<div class="col-xs-12 p-0">
	<div class="col-md-12 pb-4">
		<div class="panel-form-control flex-row" id="selected_toggle_filters">
			<div class="flex-col-12 flex-col-sm-11 flex-row">
				<div class="flex-col-3 pb-4" id="selected_system_cat"></div>
				<div class="flex-col-3 pb-4" id="selected_system_tag"></div>
				<div class="flex-col-3 pb-4" id="selected_assessment_cat"></div>
				<div class="flex-col-3 pb-4" id="selected_assessment_tag"></div>
				<div id="toggleQuestionListSection" class="flex-row m-0 flex-col-12 pl-0" style="display: none">
					<!-- <div class="flex-col-3 pb-4" id="selected_course_tag"></div>
					<div class="flex-col-3 pb-4" id="selected_module_tag"></div>
					<div class="flex-col-3 pb-4" id="selected_lesson_tag"></div> -->
					<div class="flex-col-3 pb-4" id="selected_outcomes_cat"></div>
					<div class="flex-col-3 pb-4" id="selected_standard_cat"></div>
					<div class="flex-col-3 pb-4" id="selected_bloom_level_cat"></div>
					<!-- <div class="flex-col-3 pb-4" id="selected_competencies_cat"></div>
					<div class="flex-col-3 pb-4" id="selected_skills_cat"></div> -->
					<div class="flex-col-sm-1">
						<button type="button"  class="btn btn-danger btn-block selected_reset_filter">
							<span class="fa fa-times"></span>
						</button>
					</div>
				</div>
			</div>
			<div class="flex-col-12 flex-col-sm-1">
				<button type="button" class="btn btn-default btn-block btn-arrow" data-panel-toggle="toggleQuestionListSection">
					<span class="flaticon-back"></span>
				</button>
			</div>
		</div>
	</div>
	<div class="col-xs-12 p-0">
		<div class="hr-line"></div>
	</div>
	<div class="col-xs-12 p-0 row m-0 pt-3 pb-3">
		<div class="col-md-6 pt-3">
			<!-- $('#selected_section').val() -->
			<a href="javascript:void(0)" class="btn btn-primary btn-xs" onclick="AddQuestionSectionPart.get_open_questions_popup()">
				<i class="flaticon-add"></i>
				Add Selected Question To Section
			</a>

			<a href="javascript:void(0)" class="btn btn-danger btn-xs remove-questions-from-section" >
				<i class="ti ti-trash"></i>
				Remove
			</a>
		</div>
		<div class="col-md-6" id="addQuestionSectionOPtions">
			<div class="panel-ctrls"></div>
		</div>
	</div>
	<div class="col-xs-12 p-0">
		<div class="hr-line"></div>
	</div>
	<div class="col-xs-12 p-0 pb-4">
		<table id="selected_quiz_table" class="table">
			<thead>
				<tr>
					<th style="min-width: 50px;">
						<label class="checkbox-tel">
							<input type="checkbox" name="selected_select_all" id="selected_select_all" class="mr-2 selected_select_all">
						</label>
					</th>
					<th style="min-width: 100px;">Title</th>
					<th style="min-width: 150px;">Long Title</th>
					<th style="min-width: 200px;">Assessment Categories</th>
					<th style="min-width: 140px;">Assessment Tags</th>
					<th style="min-width: 180px;">System Categories</th>
					<th style="min-width: 140px;">System Tags</th>
					<!-- <th style="min-width: 140px;">Course Tags</th>
					<th style="min-width: 140px;">Module Tags</th>
					<th style="min-width: 100px;">Lesson Tags</th> -->
					<th style="min-width: 180px;">Learning Outcomes</th>
					<th style="min-width: 180px;">TEL Mastery Standards</th>
					<th style="min-width: 180px;">Blooms level</th>
					<!-- <th style="min-width: 200px;">Learning Competencies</th>
					<th style="min-width: 180px;">Learning Skills</th> -->
				</tr>
			</thead>
		</table>
	</div>
</div>
<script type="text/javascript" src="<?php bs('public/assets/js/quiz/get_selected_section_part.js') ?>"></script>
<?php
//echo $recordID;
$jsVars = [
    'ajax_call_root' => base_url(),
    'quizID' => $quizID,
    'sectionID' => $sectionID
];    
?>
<script type="text/javascript">   
   $(document).ready(function() {
      AddQuestionSectionPart.init(<?php echo json_encode($jsVars); ?>);
   });  
	$("[data-panel-toggle]").each(function(){
	    var $this =  $(this),
	        toggleContainer = $this.attr("data-panel-toggle");
	    $this.click(function(){
	        $("#"+toggleContainer).slideToggle()
	    })
	});  
</script>