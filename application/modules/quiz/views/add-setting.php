<link type="text/css" href="<?= bs('public/assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css')?>" rel="stylesheet"> 
<?php echo form_open('quiz/add',['id'=>'createquiz','name'=>'createquiz']); ?>
<div class="row mt-4 mb-3">
    <div class="col-sm-12">
        
        <div class="form-group flex-row m-0 pb-4">
            <label for="question_type" class="control-label pt-4">Numbers of Attempts</label>
            <div class="p-0 m-0 ml-4 w80 flex-auto pt-3">
                <?php echo form_input([ 
                              'name' => 'number_attempt',
                              'id' => 'number_attempt',
                              'class' => 'form-control touchspin4',
                              'value' => (isset($details->number_attempt)?$details->number_attempt:0)
                ]);?>
            </div>
            <span class="flex-auto pt-4 pl-3 f14">
                Number of quiz attempts allowed: 0 = unlimited, 1=1, 2=2, etc.
            </span>
        </div>
        <div class="form-group m-0 flex-row pt-4 pb-3">
            <label for="question_type" class="control-label">
                Record in Gradebook <span class="field-required">*</span>
            </label>
            <div class="flex-col-auto flex-row">
                <label class="radio-tel ml-5">
                    <input type="radio" name="gradebook_record" value="1" <?php echo (isset($details->gradebook_record) && $details->gradebook_record == 1 ? 'checked': '')?> class="m-0 mr-1 t2">
                    Highest attempt
                </label>
                <label class="radio-tel ml-4">
                    <input type="radio"  name="gradebook_record" <?php echo (isset($details->gradebook_record) && $details->gradebook_record == 2 ? 'checked': '')?> value="2" class="m-0 mr-1 t2">
                    Last Attempt
                </label>
                <label class="radio-tel ml-4">
                    <input type="radio" name="gradebook_record" <?php echo (isset($details->gradebook_record) && $details->gradebook_record == 3 ? 'checked': '')?> value="3" class="m-0 mr-1 t2">
                    Average
                </label>
                <span class="rg-error radio-tel ml-4 mt--5"></span>
            </div>
        </div>
        

        <div class="col-xs-12 p-0">
            <div class="hr-line mt-3 mb-3"></div>
        </div>
        <div class="col-xs-12 p-0 pl-1 pt-4">
            <div class="themePanel icon-left pl-5">
                <h4>
                    <i class="ficon flaticon-settings"></i>
                    Student Question Review Settings
                </h4>
                <div class="col-xs-12 p-0 pt-4 pb-3">
                    <label class="checkbox-tel control-label">
                        <input type="checkbox" name="view_result_btn"  value="1" <?php echo (isset($details->view_result_btn) && $details->view_result_btn == 1 ? 'checked': '')?> class="m-0 mr-1 t2">
                        Show "View result" buttons
                    </label> 
                </div>                                               
            </div>
        </div>
        <div class="col-xs-12 p-0">
            <div class="hr-line mt-3 mb-3"></div>
        </div>     

        <div class="col-xs-12 fib-response-panel">
            <h4>
                <i class="flaticon-checklist"></i> 
                Create quiz sections
            </h4>
        </div>
        <div class="col-xs-12 p-0 fib-left" id="count_section_start">
        
        </div>
        <div class="col-sm-12 p-0 mt-5 mb-5">
            <div class="hr-line"></div>
        </div>
        <div class="row mb-3">
            <div class="col-sm-12 text-right">
                <button class="btn btn-danger click_to_back">Back</button>
                <button type="submit" class="btn btn-primary scroll-top"><?php echo ($this->session->userdata('action') == 'add')?'Save & Next':'Save Changes' ?></button>
            </div>
        </div>
    </div>
</div>
</form>
<script type="text/javascript" src="<?=bs('public/assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js') ?>"></script>

<script type="text/javascript" src="<?=bs('public/assets/js/quiz/add-setting.js') ?>"></script>
<?php

$jsVars = [
    'ajax_call_root' => base_url(),
    'recordID' => $recordID
];    
?>
<script type="text/javascript">   
   $(document).ready(function() {
      AddQuizSetting.init(<?php echo json_encode($jsVars); ?>);
      $(".scroll-top").on("click", function(){
        $('body,html').animate({
            scrollTop: 0
        }, 500);
      });   


   });  
</script>    