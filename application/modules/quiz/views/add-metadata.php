<link type="text/css" href="<?= bs('public/assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css')?>" rel="stylesheet">
<div class="row mt-3">  
    <div class="col-sm-5">
        <div class="panel panel-grey mb-0">
            <div class="panel-heading">
                <h2>
                    <label class="checkbox-inline icheck">
                        Assessment Categories
                    </label>
                </h2>
            </div>          
            <div class="col-md-12 tag-lib-container" id="assessment_categories"></div>           
        </div>
    </div>
    <div class="col-sm-5">
        <div class="panel panel-grey mb-0">
            <div class="panel-heading">
                <h2>
                    <label class="checkbox-inline icheck">Assessment Tags</label>
                </h2>
            </div>
            <div class="panel-body" id="assessment_tag"></div>
        </div>
    </div>
</div>
<div class="col-xs-12 p-0">
    <div class="hr-line mb-5"></div>
</div>
<div class="row">  
    <div class="col-sm-5">
        <div class="panel panel-grey mb-0">
            <div class="panel-heading">
                <h2>
                    <label class="checkbox-inline icheck">
                        System Categories
                    </label>
                </h2>
            </div>          
            <div class="col-md-12 tag-lib-container" id="system_categories"></div>           
        </div>
    </div>
    <div class="col-sm-5">
        <div class="panel panel-grey mb-0">
            <div class="panel-heading">
                <h2>
                    <label class="checkbox-inline icheck">System Tags</label>
                </h2>
            </div>
            <div class="panel-body" id="system_tag"></div>
        </div>
    </div>
</div>
<div class="col-xs-12 p-0">
    <div class="hr-line mb-5"></div>
</div>
<!-- <div class="row">  
    <div class="col-sm-4">
        <div class="panel panel-grey mb-0">
            <div class="panel-heading">
                <h2>
                    <label class="checkbox-inline icheck">Course Tags</label>
                </h2>
            </div>
            <div class="panel-body" id="course_tag"></div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="panel panel-grey mb-0">
            <div class="panel-heading">
                <h2>
                    <label class="checkbox-inline icheck">Module Tags</label>
                </h2>
            </div>
            <div class="panel-body" id="module_tag"></div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="panel panel-grey">
            <div class="panel-heading">
                <h2>
                    <label class="checkbox-inline icheck">Lesson Tags</label>
                </h2>
            </div>
            <div class="panel-body" id="lesson_tag"></div>
        </div>
    </div>
</div>
<div class="col-xs-12 p-0">
    <div class="hr-line mb-5"></div>
</div> -->
<div class="row">  
    <div class="col-sm-5">
        <div class="panel panel-grey mb-0">
            <div class="panel-heading">
                <h2>
                    <label class="checkbox-inline icheck">
                        Learning Outcomes
                    </label>
                </h2>
            </div>          
            <div class="col-md-12 tag-lib-container" id="learning_outcomes"></div>           
        </div>
    </div>
    <div class="col-sm-5">
        <div class="panel panel-grey mb-0">
            <div class="panel-heading">
                <h2>
                    <label class="checkbox-inline icheck">TEL Mastery Standards</label>
                </h2>
            </div>
            <div class="col-md-12 tag-lib-container" id="learning_standards"></div>
        </div>
    </div>
</div>
<div class="col-xs-12 p-0">
    <div class="hr-line mb-5"></div>
</div>
<!-- <div class="row">  
    <div class="col-sm-5">
        <div class="panel panel-grey mb-0">
            <div class="panel-heading">
                <h2>
                    <label class="checkbox-inline icheck">
                        Competencies
                    </label>
                </h2>
            </div>          
            <div class="col-md-12 tag-lib-container" id="competencies"></div>           
        </div>
    </div>
    <div class="col-sm-5">
        <div class="panel panel-grey mb-0">
            <div class="panel-heading">
                <h2>
                    <label class="checkbox-inline icheck">Skills</label>
                </h2>
            </div>
            <div class="col-md-12 tag-lib-container" id="skills"></div>
        </div>
    </div>
</div> -->
<!-- <div class="col-xs-12 p-0">
    <div class="hr-line mb-5"></div>
</div>
<div class="row">  
    <div class="col-sm-5">
        <div class="panel panel-grey mb-0">
            <div class="panel-heading">
                <h2>
                    <label class="checkbox-inline icheck">
                        Quiz Type
                    </label>
                </h2>
            </div>          
            <div class="col-md-12 tag-lib-container" id="quiz_type"></div>           
        </div>
    </div>
    <div class="col-sm-5">
        <div class="panel panel-grey mb-0">
            <div class="panel-heading">
                <h2>
                    <label class="checkbox-inline icheck">Quiz Type Restrictions</label>
                </h2>
            </div>
            <div class="col-md-12 tag-lib-container" id="quiz_type_restrictions"></div>
        </div>
    </div>
</div>
<div class="col-xs-12 p-0">
    <div class="hr-line"></div>
</div> -->
<?php
echo form_open('quiz/add',['id'=>'createquiz','name'=>'createquiz']); ?>

<div class="col-xs-12 p-0 pt-4">
        <div class="form-group flex-row m-0 pb-4">
            <div class="flex-col-auto flex-row p-0 m-0">
                <label for="question_type" class="control-label pt-4 flex-col-auto">Skill Mastery Points</label>
                <div class="p-0 m-0 ml-4 w80 flex-auto pt-3">
                    <input type="text" name="skill_mastery_point" value="<?php echo (isset($details->skill_mastery_point)?$details->skill_mastery_point:0) ?>" class="touchspin4 form-control">
                </div>
            </div>
            <div class="flex-col-auto flex-row ml-5">
                <label for="question_type" class="control-label pt-4 flex-col-auto">Quiz Levels</label>
                <div class="p-0 m-0 ml-4 w80 flex-auto pt-3">
                     <?php echo form_input([ 
                      'name' => 'level',
                      'id' => 'level',
                      'class' => 'form-control meta_quiz_level',
                      'value' => (isset($details->level)?$details->level:1)
                  ]);?>
                </div>
            </div>
        </div>       
</div>



<div class="col-xs-12 p-0">
    <div class="hr-line mt-5 mb-5"></div>
</div>
<div class="col-xs-12 p-0 text-right">
    <button type="button" class="btn btn-danger scroll-top click_to_back">Back</button>
    <button class="btn btn-primary scroll-top click_to_next">Next</button>
</div>
</form>
<script type="text/javascript" src="<?= bs('public/assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js') ?>"></script>
<script type="text/javascript" src="<?php bs('public/assets/js/quiz/add-metadata.js') ?>"></script>
<?php
//echo $recordID;
$jsVars = [
    'ajax_call_root' => base_url(),
     'recordID' => $recordID,
];    
?>
<script type="text/javascript">   
   $(document).ready(function() {
      AddQuizMetadata.init(<?php echo json_encode($jsVars); ?>);
   });  
</script>

<script>
    /*$("input.touchspin4").TouchSpin({
      verticalbuttons: true,
      step: 1,
      min : 0,
      max : 100
    });*/
    $(".scroll-top").on("click", function(){
      $('body,html').animate({
          scrollTop: 0
      }, 500);
    });        
</script>