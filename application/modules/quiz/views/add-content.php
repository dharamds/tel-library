<link type="text/css" href="<?= bs('public/assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css')?>" rel="stylesheet"> 
<?php echo form_open('quiz/add',['id'=>'createquiz','name'=>'createquiz']); ?>
<div class="row mt-4 mb-3">
    <div class="col-sm-3">
        <div class="form-group">
            <label for="question_type" class="control-label"> Quiz Type <span class="field-required">*</span> </label>
              <select class="form-control" name="type" id="type">
                <option value="">Select type</option>
                  <?php 
                    foreach ($types as $key => $value) {
                      $selected = ($value->id == $details->type)?"selected":"";
                      echo '<option value='.$value->id.' '.$selected.'>'.$value->name.'</option>';
                    } 
                  ?>
              </select>
        </div>
    </div>
    <div class="col-sm-10">
        <div class="form-group">
            <label for="question_type" class="control-label"> Quiz Title <span class="field-required">*</span> </label>
            <?php echo form_input([ 
                              'name' => 'title',
                              'id' => 'title',
                              'class' => 'form-control',
                              'value' => (isset($details->title)?$details->title:'')
            ]);?>
        </div>
    </div>
    <div class="col-sm-2">
        <div class="form-group">
            <label for="question_type" class="control-label"> Total Points <span class="field-required">*</span> </label>
            <?php echo form_input([ 
                              'name' => 'total_points',
                              'id' => 'total_points',
                              'class' => 'form-control touchspin4',
                              'value' => (isset($details->total_points)?$details->total_points:0)
            ]);?>
        </div>
    </div>
</div>
<div class="row mt-4 mb-3">
    <div class="col-sm-10">
        <div class="form-group">
            <label for="question_type" class="control-label"> Quiz Long Title </label>
            <?php echo form_input([ 
                              'name' => 'long_title',
                              'id' => 'long_title',
                              'class' => 'form-control',
                              'value' => (isset($details->long_title)?$details->long_title:'')
            ]);?>
        </div>
    </div>
</div>
<div class="row mt-4 mb-3">
    <div class="col-sm-12">
        <div class="form-group">
            <label for="question_type" class="control-label"> Quiz Instructions <span class="field-required">*</span> </label>
            <div class="col-sm-12 p-0">
                <?php echo form_textarea([ 
                              'name' => 'quizInstructions',
                              'id' => 'quizInstructions',
                              'class' => 'form-control',
                              'value' => (isset($details->instruction)?$details->instruction:'')
                ]);?>
            </div>
        </div>
    </div>
</div>
<div class="row mt-5 mb-3">
    <div class="col-sm-12">
        <div class="form-group">
            <label for="question_type" class="control-label"> Quiz Feedback </label>
            <div class="col-sm-12 p-0">
                <?php echo form_textarea([ 
                              'name' => 'quizFeedback',
                              'id' => 'quizFeedback',
                              'class' => 'form-control',
                              'value' => (isset($details->feedback)?$details->feedback:'')
                ]);?>
            </div>
        </div>
    </div>
</div>
<div class="col-sm-12 p-0 mt-5 mb-5">
    <div class="hr-line"></div>
</div>
<div class="row mb-3">
    <div class="col-sm-12 text-right">
        <button class="btn btn-danger click_to_finish">Cancel</button>
        <button type="submit" class="btn btn-primary scroll-top"><?php echo ($this->session->userdata('action') == 'add')?'Save & Next':'Save Changes' ?></button>
    </div>
</div>
</form>
<script type="text/javascript" src="<?=bs('public/assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js') ?>"></script>
<script type="text/javascript" src="<?php bs('public/assets/js/quiz/add-content.js') ?>"></script>
<?php

$jsVars = [
    'ajax_call_root' => base_url(),
    'recordID' => (isset($recordID)?$recordID:0),
];    
?>
<script type="text/javascript">   
   $(document).ready(function() {
      AddQuizContent.init(<?php echo json_encode($jsVars); ?>);
   }); 
  $(".scroll-top").on("click", function(){
    $('body,html').animate({
        scrollTop: 0
    }, 500);
  });     
</script>