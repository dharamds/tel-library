<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
  Author Salman Iqbal
  Company Parexons
  Date 26/1/2017
 */

class Quizzes_modal extends CI_Model
{

    function __construct()
    {
        // Set table name
        $this->table = 'quizzes';
        // Set orderable column fields
        $this->column_order = array(null, null, 'quizzes.status', 'quizzes.title', 'quizzes.long_title', 'quiz_type', 'assesment_category', 'assessment_tags', 'system_category', 'system_tags',/*'courses_tags','modules_tags','lessons_tags',*/ 'outcomes_category',/*'meta_title','level',*/ 'standards_category',/*'competencies_category','skills_category'*/);
        // Set searchable column fields
        $this->column_search = array('quizzes.title');
        // Set default order
        $this->order = array('quizzes.id' => 'desc');
    }
    /*
    *getCheckExist this method is used to check data is already exist or not
    * @param string
    * @return string 
    */
    public function getCount($condition = [], $table)
    {
        $this->db->where($condition);
        if ($query = $this->db->get($table)) {
            return $query->num_rows();
        }
    }




    /*
    * Fetch members data from the database
    * @param $_POST filter data based on the posted parameters
    */
    public function getRows($postData)
    {

        $this->_get_datatables_query($postData);
        if ($postData['length'] != -1) {
            $this->db->limit($postData['length'], $postData['start']);
        }
        $query = $this->db->get();
        $data['result'] = $query->result();
        // pr($this->db->last_query());
        $data['total'] = $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total;
        return $data;
    }

    /*
     * Perform the SQL queries needed for an server-side processing requested
     * @param $_POST filter data based on the posted parameters
     */
    private function _get_datatables_query($postData)
    {
        // pr($postData['order']['0']['column'],"k");
        $this->db->select("SQL_CALC_FOUND_ROWS quizzes.id,quizzes.title,quizzes.long_title,quizzes.status,quizzes.level", false);
        if ((isset($postData['assessment_category']) && count($postData['assessment_category']) > 0) || $postData['order']['0']['column'] == 6) {
            $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT categories.name ORDER BY categories.name ASC SEPARATOR "<br>"), \'NA\')  as assesment_category');
        } else {
            $this->db->select('"..." as assesment_category');
        }

        if ((isset($postData['assessment_tag']) && count($postData['assessment_tag']) > 0) || $postData['order']['0']['column'] == 7) {
            $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT assessment_tags.name ORDER BY assessment_tags.name ASC  SEPARATOR "<br>"), \'NA\')  as assessment_tags');
        } else {
            $this->db->select('"..." as assessment_tags');
        }

        if ((isset($postData['system_category']) && count($postData['system_category']) > 0) || $postData['order']['0']['column'] == 8) {
            $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT system_category.name ORDER BY system_category.name ASC  SEPARATOR "<br>"), \'NA\')  as system_category');
        } else {
            $this->db->select('"..." as system_category');
        }

        if ((isset($postData['system_tag']) && count($postData['system_tag']) > 0) || $postData['order']['0']['column'] == 9) {
            $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT system_tags.name ORDER BY system_tags.name ASC  SEPARATOR "<br>"), \'NA\')  as system_tags');
        } else {
            $this->db->select('"..." as system_tags');
        }

        if ((isset($postData['outcomes_category']) && count($postData['outcomes_category']) > 0) || $postData['order']['0']['column'] == 10) {
            $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT outcomes_category.name ORDER BY outcomes_category.name ASC  SEPARATOR "<br>"), \'NA\')  as outcomes_category');
        } else {
            $this->db->select('"..." as outcomes_category');
        }

        if ((isset($postData['tel_mastery_standards']) && count($postData['tel_mastery_standards']) > 0) || $postData['order']['0']['column'] == 11) {
            $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT standards_category.name ORDER BY standards_category.name ASC  SEPARATOR "<br>"), \'NA\')   as standards_category');
        } else {
            $this->db->select('"..." as standards_category');
        }







        /*$this->db->select(' COALESCE(GROUP_CONCAT(DISTINCT skills_category.name ORDER BY skills_category.name ASC  SEPARATOR "<br>") , \'NA\')  as skills_category');*/
        /*$this->db->select('COALESCE(GROUP_CONCAT(DISTINCT competencies_category.name ORDER BY competencies_category.name ASC  SEPARATOR "<br>"), \'NA\')  as competencies_category');*/
        /*Tags*/


        /* $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT courses_tags.name ORDER BY courses_tags.name ASC  SEPARATOR "<br>"), \'NA\')  as courses_tags');
        $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT modules_tags.name ORDER BY modules_tags.name ASC  SEPARATOR "<br>"), \'NA\')  as modules_tags');
        $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT lessons_tags.name ORDER BY lessons_tags.name ASC  SEPARATOR "<br>"), \'NA\')  as lessons_tags');

        $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT quiz_type_category.name ORDER BY quiz_type_category.name ASC  SEPARATOR "<br>"), \'NA\')  as quiz_type_category');*/

        /*End Tags*/

        $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT quiz_type.name ORDER BY quiz_type.name ASC  SEPARATOR "<br>"), \'NA\')  as quiz_type');

        $this->db->from('quizzes');


        /*$this->db->join('category_assigned as assigned_quiz_type_category', 'assigned_quiz_type_category.reference_id = quizzes.id AND assigned_quiz_type_category.reference_type = '.QUIZ_TYPE.' AND  assigned_quiz_type_category.reference_sub_type ='.META_QUIZ, 'left');
        $this->db->join('categories as quiz_type_category', 'quiz_type_category.id = assigned_quiz_type_category.category_id AND quiz_type_category.delete_status = 1 AND quiz_type_category.status = 1', 'left');*/

        $this->db->join('quiz_assessment_type as quiz_type', 'quiz_type.id = quizzes.type', 'left');

        if ((isset($postData['assessment_category']) && count($postData['assessment_category']) > 0) || $postData['order']['0']['column'] == 6) {
            $this->db->join('category_assigned', 'category_assigned.reference_id = quizzes.id AND category_assigned.reference_type = ' . META_ASSESSMENT . ' AND  category_assigned.reference_sub_type =' . META_QUIZ, 'left');
            $this->db->join('categories', 'categories.id = category_assigned.category_id AND categories.delete_status = 1 AND categories.status = 1', 'left');
        }

        if ((isset($postData['assessment_tag']) && count($postData['assessment_tag']) > 0) || $postData['order']['0']['column'] == 7) {
            $this->db->join('tag_assigned as assigned_assessment_tag', 'assigned_assessment_tag.reference_id = quizzes.id AND assigned_assessment_tag.reference_type = ' . META_ASSESSMENT . ' AND  assigned_assessment_tag.reference_sub_type =' . META_QUIZ, 'left');
            $this->db->join('tags as assessment_tags', 'assessment_tags.id = assigned_assessment_tag.tag_id AND assessment_tags.delete_status = 1 AND assessment_tags.status = 1', 'left');
        }

        if ((isset($postData['system_category']) && count($postData['system_category']) > 0) || $postData['order']['0']['column'] == 8) {
            $this->db->join('category_assigned as assigned_system_category', 'assigned_system_category.reference_id = quizzes.id AND assigned_system_category.reference_type = ' . META_SYSTEM . ' AND  assigned_system_category.reference_sub_type =' . META_QUIZ, 'left');
            $this->db->join('categories as system_category', 'system_category.id = assigned_system_category.category_id AND system_category.delete_status = 1 AND system_category.status = 1', 'left');
        }

        if ((isset($postData['system_tag']) && count($postData['system_tag']) > 0) || $postData['order']['0']['column'] == 9) {
            $this->db->join('tag_assigned as assigned_system_tags', 'assigned_system_tags.reference_id = quizzes.id AND assigned_system_tags.reference_type = ' . META_SYSTEM . ' AND  assigned_system_tags.reference_sub_type =' . META_QUIZ, 'left');
            $this->db->join('tags as system_tags', 'system_tags.id = assigned_system_tags.tag_id AND system_tags.delete_status = 1 AND system_tags.status = 1', 'left');
        }

        if ((isset($postData['outcomes_category']) && count($postData['outcomes_category']) > 0) || $postData['order']['0']['column'] == 10) {
            $this->db->join('category_assigned as assigned_outcomes_category', 'assigned_outcomes_category.reference_id = quizzes.id AND assigned_outcomes_category.reference_type = ' . META_LEARNING_OUTCOMES . ' AND  assigned_outcomes_category.reference_sub_type =' . META_QUIZ, 'left');
            $this->db->join('categories as outcomes_category', 'outcomes_category.id = assigned_outcomes_category.category_id AND outcomes_category.delete_status = 1 AND outcomes_category.status = 1', 'left');
        }

        if ((isset($postData['tel_mastery_standards']) && count($postData['tel_mastery_standards']) > 0) || $postData['order']['0']['column'] == 11) {
            $this->db->join('category_assigned as assigned_standards_category', 'assigned_standards_category.reference_id = quizzes.id AND assigned_standards_category.reference_type = ' . META_LEARNING_STANDARDS . ' AND  assigned_standards_category.reference_sub_type =' . META_QUIZ, 'left');
            $this->db->join('categories as standards_category', 'standards_category.id = assigned_standards_category.category_id AND standards_category.delete_status = 1 AND standards_category.status = 1', 'left');
        }




        /* $this->db->join('category_assigned as assigned_skills_category', 'assigned_skills_category.reference_id = quizzes.id AND assigned_skills_category.reference_type = '.META_SKILLS.' AND  assigned_skills_category.reference_sub_type ='.META_QUIZ, 'left');
        $this->db->join('categories as skills_category', 'skills_category.id = assigned_skills_category.category_id AND skills_category.delete_status = 1 AND skills_category.status = 1', 'left');*/

        /*$this->db->join('category_assigned as assigned_competencies_category', 'assigned_competencies_category.reference_id = quizzes.id AND assigned_competencies_category.reference_type = '.META_COMPETENCIES.' AND  assigned_competencies_category.reference_sub_type ='.META_QUIZ, 'left');
        $this->db->join('categories as competencies_category', 'competencies_category.id = assigned_competencies_category.category_id AND competencies_category.delete_status = 1 AND competencies_category.status = 1', 'left');*/
        /*Tags*/




        /* $this->db->join('tag_assigned as assigned_courses_tags', 'assigned_courses_tags.reference_id = quizzes.id AND assigned_courses_tags.reference_type = '.META_COURSE.' AND  assigned_courses_tags.reference_sub_type ='.META_QUIZ, 'left');
        $this->db->join('tags as courses_tags', 'courses_tags.id = assigned_courses_tags.tag_id AND courses_tags.delete_status = 1 AND courses_tags.status = 1', 'left');

        $this->db->join('tag_assigned as assigned_modules_tags', 'assigned_modules_tags.reference_id = quizzes.id AND assigned_modules_tags.reference_type = '.META_MODULE.' AND  assigned_modules_tags.reference_sub_type ='.META_QUIZ, 'left');
        $this->db->join('tags as modules_tags', 'modules_tags.id = assigned_modules_tags.tag_id AND modules_tags.delete_status = 1 AND modules_tags.status = 1', 'left');*/

        /* $this->db->join('tag_assigned as assigned_lessons_tags', 'assigned_lessons_tags.reference_id = quizzes.id AND assigned_lessons_tags.reference_type = '.META_LESSON.' AND  assigned_lessons_tags.reference_sub_type ='.META_QUIZ, 'left');
        
        $this->db->join('tags as lessons_tags', 'lessons_tags.id = assigned_lessons_tags.tag_id AND lessons_tags.delete_status = 1 AND lessons_tags.status = 1', 'left');*/



        /*End tags*/
        // pr($postData);

        /*if (isset($postData['system_category']) && $postData['system_category'] != '') 
        {
            $postData['system_category'] = [69,6,4];
            $this->db->where_in('assigned_system_category.category_id',$postData['system_category']);
        }*/

        if (isset($postData['system_category']) && $postData['system_category'] != '') {
            /*$this->db->where(['assigned_system_category.category_id' => $postData['system_category']]);*/
            $this->db->where_in('assigned_system_category.category_id', $postData['system_category']);
        }
        if (isset($postData['system_tag']) && $postData['system_tag'] != '') {
            $this->db->where_in('assigned_system_tags.tag_id', $postData['system_tag']);
            //$this->db->where(['assigned_system_tags.tag_id' => $postData['system_tag']]);
        }
        if (isset($postData['assessment_category']) && $postData['assessment_category'] != '') {
            $this->db->where_in('category_assigned.category_id', $postData['assessment_category']);
        }
        if (isset($postData['assessment_tag']) && $postData['assessment_tag'] != '') {
            $this->db->where_in('assigned_assessment_tag.tag_id', $postData['assessment_tag']);
        }
        /* if (isset($postData['course_tag']) && $postData['course_tag'] != '') 
        {
            $this->db->where(['assigned_courses_tags.tag_id' => $postData['course_tag']]);
        }
        if (isset($postData['module_tag']) && $postData['module_tag'] != '') 
        {
            $this->db->where(['assigned_modules_tags.tag_id' => $postData['module_tag']]);
        }
        if (isset($postData['lesson_tag']) && $postData['lesson_tag'] != '') 
        {
            $this->db->where(['assigned_lessons_tags.tag_id' => $postData['lesson_tag']]);
        }*/
        if (isset($postData['outcomes_category']) && $postData['outcomes_category'] != '') {
            $this->db->where_in('assigned_outcomes_category.category_id', $postData['outcomes_category']);
        }
        if (isset($postData['tel_mastery_standards']) && $postData['tel_mastery_standards'] != '') {
            $this->db->where_in('assigned_standards_category.category_id', $postData['tel_mastery_standards']);
        }
        /*if (isset($postData['compentencies_category']) && $postData['compentencies_category'] != '') 
        {
            $this->db->where(['assigned_competencies_category.category_id' => $postData['compentencies_category']]);
        }
        if (isset($postData['skills_category']) && $postData['skills_category'] != '') 
        {
            $this->db->where(['assigned_skills_category.category_id' => $postData['skills_category']]);
        }*/
        //pr($postData);
        /*if (isset($postData['quiz_type']) && $postData['quiz_type'] != '') 
        {
            $this->db->where(['assigned_quiz_type_category.category_id' => $postData['quiz_type']]);
        }

        if (isset($postData['quiz_level']) && $postData['quiz_level'] != '') 
        {
            $this->db->where(['quizzes.level' => $postData['quiz_level']]);
        }*/

        $this->db->where(['quizzes.delete_status' => 1]);

        $i = 0;
        // loop searchable columns 
        foreach ($this->column_search as $item) {

            // if datatable send POST for search
            if ($postData['search']['value']) {
                // first loop
                if ($i === 0) {
                    // open bracket 
                    $this->db->group_start();
                    //pr( $item);	
                    $this->db->like($item, $postData['search']['value'], 'both');
                } else {
                    $this->db->or_like($item, $postData['search']['value'], 'both');
                }
                // last loop
                if (count($this->column_search) - 1 == $i) {
                    // close bracket
                    $this->db->group_end();
                }
            }
            $i++;
        }


        $this->db->group_by('quizzes.id');
        //pr($postData,"l");
        if (isset($postData['order'])) {
            $this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    /*
     * Fetch containers data from the database
     * @param $_POST filter data based on the posted parameters
     */
    public function getQuizRowsForExport($postData)
    {
        $this->_get_datatables_query_export($postData);
        $query = $this->db->get();
        $data['result'] = $query->result();
        $data['total'] = $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total;
        return $data;
    }


    /*
     * Perform the SQL queries needed for an server-side processing requested
     * @param $_POST filter data based on the posted parameters
     */
    private function _get_datatables_query_export($postData)
    {

        $this->db->select("SQL_CALC_FOUND_ROWS quizzes.id,quizzes.title,quizzes.long_title,quizzes.status,quizzes.level", false);

        $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT categories.name ORDER BY categories.name ASC SEPARATOR ","), \'NA\')  as assesment_category');
        $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT system_category.name ORDER BY system_category.name ASC  SEPARATOR ","), \'NA\')  as system_category');

        $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT outcomes_category.name ORDER BY outcomes_category.name ASC  SEPARATOR ","), \'NA\')  as outcomes_category');

        $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT standards_category.name ORDER BY standards_category.name ASC  SEPARATOR ","), \'NA\')   as standards_category');


        /*$this->db->select(' COALESCE(GROUP_CONCAT(DISTINCT skills_category.name ORDER BY skills_category.name ASC  SEPARATOR ",") , \'NA\')  as skills_category');
        $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT competencies_category.name ORDER BY competencies_category.name ASC  SEPARATOR ","), \'NA\')  as competencies_category');*/

        $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT assessment_tags.name ORDER BY assessment_tags.name ASC  SEPARATOR ","), \'NA\')  as assessment_tags');
        $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT system_tags.name ORDER BY system_tags.name ASC  SEPARATOR ","), \'NA\')  as system_tags');
        /*$this->db->select('COALESCE(GROUP_CONCAT(DISTINCT courses_tags.name ORDER BY courses_tags.name ASC  SEPARATOR ","), \'NA\')  as courses_tags');
        $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT modules_tags.name ORDER BY modules_tags.name ASC  SEPARATOR ","), \'NA\')  as modules_tags');
        $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT lessons_tags.name ORDER BY lessons_tags.name ASC  SEPARATOR ","), \'NA\')  as lessons_tags');*/

        /*$this->db->select('COALESCE(GROUP_CONCAT(DISTINCT quiz_type_category.name ORDER BY quiz_type_category.name ASC  SEPARATOR ","), \'NA\')  as quiz_type_category');*/

        $this->db->from('quizzes');

        /*$this->db->join('category_assigned as assigned_quiz_type_category', 'assigned_quiz_type_category.reference_id = quizzes.id AND assigned_quiz_type_category.reference_type = '.QUIZ_TYPE.' AND  assigned_quiz_type_category.reference_sub_type ='.META_QUIZ, 'left');
        $this->db->join('categories as quiz_type_category', 'quiz_type_category.id = assigned_quiz_type_category.category_id AND quiz_type_category.delete_status = 1 AND quiz_type_category.status = 1', 'left');*/

        $this->db->join('category_assigned', 'category_assigned.reference_id = quizzes.id AND category_assigned.reference_type = ' . META_ASSESSMENT . ' AND  category_assigned.reference_sub_type =' . META_QUIZ, 'left');
        $this->db->join('categories', 'categories.id = category_assigned.category_id AND categories.delete_status = 1 AND categories.status = 1', 'left');

        $this->db->join('category_assigned as assigned_system_category', 'assigned_system_category.reference_id = quizzes.id AND assigned_system_category.reference_type = ' . META_SYSTEM . ' AND  assigned_system_category.reference_sub_type =' . META_QUIZ, 'left');
        $this->db->join('categories as system_category', 'system_category.id = assigned_system_category.category_id AND system_category.delete_status = 1 AND system_category.status = 1', 'left');

        $this->db->join('category_assigned as assigned_outcomes_category', 'assigned_outcomes_category.reference_id = quizzes.id AND assigned_outcomes_category.reference_type = ' . META_LEARNING_OUTCOMES . ' AND  assigned_outcomes_category.reference_sub_type =' . META_QUIZ, 'left');
        $this->db->join('categories as outcomes_category', 'outcomes_category.id = assigned_outcomes_category.category_id AND outcomes_category.delete_status = 1 AND outcomes_category.status = 1', 'left');

        $this->db->join('category_assigned as assigned_standards_category', 'assigned_standards_category.reference_id = quizzes.id AND assigned_standards_category.reference_type = ' . META_LEARNING_STANDARDS . ' AND  assigned_standards_category.reference_sub_type =' . META_QUIZ, 'left');

        $this->db->join('categories as standards_category', 'standards_category.id = assigned_standards_category.category_id AND standards_category.delete_status = 1 AND standards_category.status = 1', 'left');

        /* $this->db->join('category_assigned as assigned_skills_category', 'assigned_skills_category.reference_id = quizzes.id AND assigned_skills_category.reference_type = '.META_SKILLS.' AND  assigned_skills_category.reference_sub_type ='.META_QUIZ, 'left');
        $this->db->join('categories as skills_category', 'skills_category.id = assigned_skills_category.category_id AND skills_category.delete_status = 1 AND skills_category.status = 1', 'left');

        $this->db->join('category_assigned as assigned_competencies_category', 'assigned_competencies_category.reference_id = quizzes.id AND assigned_competencies_category.reference_type = '.META_COMPETENCIES.' AND  assigned_competencies_category.reference_sub_type ='.META_QUIZ, 'left');
        $this->db->join('categories as competencies_category', 'competencies_category.id = assigned_competencies_category.category_id AND competencies_category.delete_status = 1 AND competencies_category.status = 1', 'left');
       */
        $this->db->join('tag_assigned as assigned_assessment_tag', 'assigned_assessment_tag.reference_id = quizzes.id AND assigned_assessment_tag.reference_type = ' . META_ASSESSMENT . ' AND  assigned_assessment_tag.reference_sub_type =' . META_QUIZ, 'left');
        $this->db->join('tags as assessment_tags', 'assessment_tags.id = assigned_assessment_tag.tag_id AND assessment_tags.delete_status = 1 AND assessment_tags.status = 1', 'left');

        $this->db->join('tag_assigned as assigned_system_tags', 'assigned_system_tags.reference_id = quizzes.id AND assigned_system_tags.reference_type = ' . META_SYSTEM . ' AND  assigned_system_tags.reference_sub_type =' . META_QUIZ, 'left');
        $this->db->join('tags as system_tags', 'system_tags.id = assigned_system_tags.tag_id AND system_tags.delete_status = 1 AND system_tags.status = 1', 'left');

        /*$this->db->join('tag_assigned as assigned_courses_tags', 'assigned_courses_tags.reference_id = quizzes.id AND assigned_courses_tags.reference_type = '.META_COURSE.' AND  assigned_courses_tags.reference_sub_type ='.META_QUIZ, 'left');
        $this->db->join('tags as courses_tags', 'courses_tags.id = assigned_courses_tags.tag_id AND courses_tags.delete_status = 1 AND courses_tags.status = 1', 'left');

        $this->db->join('tag_assigned as assigned_modules_tags', 'assigned_modules_tags.reference_id = quizzes.id AND assigned_modules_tags.reference_type = '.META_MODULE.' AND  assigned_modules_tags.reference_sub_type ='.META_QUIZ, 'left');
        $this->db->join('tags as modules_tags', 'modules_tags.id = assigned_modules_tags.tag_id AND modules_tags.delete_status = 1 AND modules_tags.status = 1', 'left');

        $this->db->join('tag_assigned as assigned_lessons_tags', 'assigned_lessons_tags.reference_id = quizzes.id AND assigned_lessons_tags.reference_type = '.META_LESSON.' AND  assigned_lessons_tags.reference_sub_type ='.META_QUIZ, 'left');
        $this->db->join('tags as lessons_tags', 'lessons_tags.id = assigned_lessons_tags.tag_id AND lessons_tags.delete_status = 1 AND lessons_tags.status = 1', 'left');*/



        if (isset($postData['selected_site_ids']) && $postData['selected_site_ids'] != '') {
            $selectedIDS = explode(',', $postData['selected_site_ids']);
            $this->db->where_in('quizzes.id', $selectedIDS);
        }

        $this->db->where(['quizzes.delete_status' => 1]);
        $this->db->group_by('quizzes.id');
    }
}
