<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Questions_modal extends CI_Model
{

    function __construct()
    {
        // Set table name
        $this->table = 'questions';
        // Set orderable column fields
        $this->column_order = array(null, 'questions.title', 'questions.long_title', 'assesment_category', 'assessment_tags', 'system_category', 'system_tags',/*'courses_tags','modules_tags','lessons_tags',*/ 'outcomes_category', 'standards_category', 'blooms_level_category'/*,'competencies_category','skills_category'*/);
        // Set searchable column fields
        $this->column_search = array('questions.title');
        // Set default order
        $this->order = array('questions.id' => 'desc');
    }

    /*
     * Fetch members data from the database
     * @param $_POST filter data based on the posted parameters
     */
    public function getRows($postData)
    {

        //$postData['sectionID']
        $existQueIDS = $this->__getExistQuestionsInSection($postData['sectionID']);
        $this->_get_datatables_query($postData, $existQueIDS);
        if ($postData['length'] != -1) {
            $this->db->limit($postData['length'], $postData['start']);
        }
        $query = $this->db->get();
        $data['result'] = $query->result();
        // /pr($this->db->last_query());
        $data['total'] = $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total;
        return $data;
    }

    private function __getExistQuestionsInSection($sectionID)
    {
        if ($sectionID) {
            $result = $this->db->select('question_id')->from('quiz_section_questions')->where(['section_id' => $sectionID])->get()->result_array();
            foreach ($result as $key => $value) {
                $returnArray[] = $value['question_id'];
            }
            return $returnArray;
        }
    }



    /*
     * Perform the SQL queries needed for an server-side processing requested
     * @param $_POST filter data based on the posted parameters
     */
    private function _get_datatables_query($postData, $existIDS = [])
    {

        //pr($postData); exit;

        $this->db->select("SQL_CALC_FOUND_ROWS questions.id,questions.title,questions.long_title", false);



        if ((isset($postData['assessment_category']) && count($postData['assessment_category']) > 0) || $postData['order']['0']['column'] == 3) {
            $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT categories.name ORDER BY categories.name ASC SEPARATOR "<br>"), \'NA\')  as assesment_category');
        } else {
            $this->db->select('"..." as assesment_category');
        }

        if ((isset($postData['assessment_tag']) && count($postData['assessment_tag']) > 0) || $postData['order']['0']['column'] == 4) {
            $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT assessment_tags.name ORDER BY assessment_tags.name ASC  SEPARATOR "<br>") , \'NA\') as assessment_tags');
        } else {
            $this->db->select('"..." as assessment_tags');
        }

        if ((isset($postData['system_category']) && count($postData['system_category']) > 0) || $postData['order']['0']['column'] == 5) {
            $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT system_category.name ORDER BY system_category.name ASC  SEPARATOR "<br>"), \'NA\')  as system_category');
        } else {
            $this->db->select('"..." as system_category');
        }

        if ((isset($postData['system_tag']) && count($postData['system_tag']) > 0) || $postData['order']['0']['column'] == 6) {
            $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT system_tags.name ORDER BY system_tags.name ASC  SEPARATOR "<br>"), \'NA\')  as system_tags');
        } else {
            $this->db->select('"..." as system_tags');
        }

        if ((isset($postData['outcomes_category']) && count($postData['outcomes_category']) > 0) || $postData['order']['0']['column'] == 7) {
            $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT outcomes_category.name ORDER BY outcomes_category.name ASC  SEPARATOR "<br>"), \'NA\')  as outcomes_category');
        } else {
            $this->db->select('"..." as outcomes_category');
        }

        if ((isset($postData['tel_mastery_standards']) && count($postData['tel_mastery_standards']) > 0) || $postData['order']['0']['column'] == 8) {
            $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT standards_category.name ORDER BY standards_category.name ASC  SEPARATOR "<br>"), \'NA\')  as standards_category');
        } else {
            $this->db->select('"..." as standards_category');
        }

        if ((isset($postData['blooms_level_category']) && count($postData['blooms_level_category']) > 0) || $postData['order']['0']['column'] == 9) {
            $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT blooms_level_category.name ORDER BY blooms_level_category.name ASC  SEPARATOR "<br>"), \'NA\')  as blooms_level_category');
        } else {
            $this->db->select('"..." as blooms_level_category');
        }

        /* $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT skills_category.name ORDER BY skills_category.name ASC  SEPARATOR "<br>"), \'NA\')  as skills_category');
        $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT competencies_category.name ORDER BY competencies_category.name ASC  SEPARATOR "<br>"), \'NA\')  as competencies_category');*/
        /*Tags*/
        //$this->db->select('COALESCE(GROUP_CONCAT(DISTINCT assessment_tags.name ORDER BY assessment_tags.name ASC  SEPARATOR "<br>") , \'NA\') as assessment_tags');
        //$this->db->select('COALESCE(GROUP_CONCAT(DISTINCT system_tags.name ORDER BY system_tags.name ASC  SEPARATOR "<br>"), \'NA\')  as system_tags');
        /* $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT courses_tags.name ORDER BY courses_tags.name ASC  SEPARATOR "<br>"), \'NA\')  as courses_tags');
        $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT modules_tags.name ORDER BY modules_tags.name ASC  SEPARATOR "<br>"), \'NA\')  as modules_tags');
        $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT lessons_tags.name ORDER BY lessons_tags.name ASC  SEPARATOR "<br>"), \'NA\')  as lessons_tags');*/

        /*$this->db->select('COALESCE(GROUP_CONCAT(DISTINCT lessons_tags.name ORDER BY lessons_tags.name ASC  SEPARATOR "<br>"), \'NA\')  as lessons_tags');*/
        /*End Tags*/
        $this->db->from('questions');

        if ((isset($postData['assessment_category']) && count($postData['assessment_category']) > 0) || $postData['order']['0']['column'] == 3) {
            $this->db->join('category_assigned', 'category_assigned.reference_id = questions.id AND category_assigned.reference_type = ' . META_ASSESSMENT . ' AND  category_assigned.reference_sub_type =' . META_QUESTION, 'left');
            $this->db->join('categories', 'categories.id = category_assigned.category_id AND categories.delete_status = 1 AND categories.status = 1', 'left');
        }

        if ((isset($postData['assessment_tag']) && count($postData['assessment_tag']) > 0) || $postData['order']['0']['column'] == 4) {
            $this->db->join('tag_assigned as assigned_assessment_tag', 'assigned_assessment_tag.reference_id = questions.id AND assigned_assessment_tag.reference_type = ' . META_ASSESSMENT . ' AND  assigned_assessment_tag.reference_sub_type =' . META_QUESTION, 'left');
            $this->db->join('tags as assessment_tags', 'assessment_tags.id = assigned_assessment_tag.tag_id AND assessment_tags.delete_status = 1 AND assessment_tags.status = 1', 'left');
        }

        if ((isset($postData['system_category']) && count($postData['system_category']) > 0) || $postData['order']['0']['column'] == 5) {
            $this->db->join('category_assigned as assigned_system_category', 'assigned_system_category.reference_id = questions.id AND assigned_system_category.reference_type = ' . META_SYSTEM . ' AND  assigned_system_category.reference_sub_type =' . META_QUESTION, 'left');
            $this->db->join('categories as system_category', 'system_category.id = assigned_system_category.category_id AND system_category.delete_status = 1 AND system_category.status = 1', 'left');
        }

        if ((isset($postData['system_tag']) && count($postData['system_tag']) > 0) || $postData['order']['0']['column'] == 6) {
            $this->db->join('tag_assigned as assigned_system_tags', 'assigned_system_tags.reference_id = questions.id AND assigned_system_tags.reference_type = ' . META_SYSTEM . ' AND  assigned_system_tags.reference_sub_type =' . META_QUESTION, 'left');
            $this->db->join('tags as system_tags', 'system_tags.id = assigned_system_tags.tag_id AND system_tags.delete_status = 1 AND system_tags.status = 1', 'left');
        }

        if ((isset($postData['outcomes_category']) && count($postData['outcomes_category']) > 0) || $postData['order']['0']['column'] == 7) {
            $this->db->join('category_assigned as assigned_outcomes_category', 'assigned_outcomes_category.reference_id = questions.id AND assigned_outcomes_category.reference_type = ' . META_LEARNING_OUTCOMES . ' AND  assigned_outcomes_category.reference_sub_type =' . META_QUESTION, 'left');
            $this->db->join('categories as outcomes_category', 'outcomes_category.id = assigned_outcomes_category.category_id AND outcomes_category.delete_status = 1 AND outcomes_category.status = 1', 'left');
        }

        if ((isset($postData['tel_mastery_standards']) && count($postData['tel_mastery_standards']) > 0) || $postData['order']['0']['column'] == 8) {
            $this->db->join('category_assigned as assigned_standards_category', 'assigned_standards_category.reference_id = questions.id AND assigned_standards_category.reference_type = ' . META_LEARNING_STANDARDS . ' AND  assigned_standards_category.reference_sub_type =' . META_QUESTION, 'left');
            $this->db->join('categories as standards_category', 'standards_category.id = assigned_standards_category.category_id AND standards_category.delete_status = 1 AND standards_category.status = 1', 'left');
        }

        if ((isset($postData['blooms_level_category']) && count($postData['blooms_level_category']) > 0) || $postData['order']['0']['column'] == 9) {
            $this->db->join('category_assigned as bloom_level_category', 'bloom_level_category.reference_id = questions.id AND bloom_level_category.reference_type = ' . BLOOMS_LEVEL . ' AND  bloom_level_category.reference_sub_type =' . META_QUESTION, 'left');
            $this->db->join('categories as blooms_level_category', 'blooms_level_category.id = bloom_level_category.category_id AND blooms_level_category.delete_status = 1 AND blooms_level_category.status = 1', 'left');
        }









        /*$this->db->join('category_assigned as assigned_skills_category', 'assigned_skills_category.reference_id = questions.id AND assigned_skills_category.reference_type = '.META_SKILLS.' AND  assigned_skills_category.reference_sub_type ='.META_QUESTION, 'left');
        $this->db->join('categories as skills_category', 'skills_category.id = assigned_skills_category.category_id AND skills_category.delete_status = 1 AND skills_category.status = 1', 'left');

        $this->db->join('category_assigned as assigned_competencies_category', 'assigned_competencies_category.reference_id = questions.id AND assigned_competencies_category.reference_type = '.META_COMPETENCIES.' AND  assigned_competencies_category.reference_sub_type ='.META_QUESTION, 'left');
        $this->db->join('categories as competencies_category', 'competencies_category.id = assigned_competencies_category.category_id AND competencies_category.delete_status = 1 AND competencies_category.status = 1', 'left');*/
        /*Tags*/


        //$this->db->join('tag_assigned as assigned_assessment_tag', 'assigned_assessment_tag.reference_id = questions.id AND assigned_assessment_tag.reference_type = '.META_ASSESSMENT.' AND  assigned_assessment_tag.reference_sub_type ='.META_QUESTION, 'left');
        // $this->db->join('tags as assessment_tags', 'assessment_tags.id = assigned_assessment_tag.tag_id AND assessment_tags.delete_status = 1 AND assessment_tags.status = 1', 'left');

        // $this->db->join('tag_assigned as assigned_system_tags', 'assigned_system_tags.reference_id = questions.id AND assigned_system_tags.reference_type = '.META_SYSTEM.' AND  assigned_system_tags.reference_sub_type ='.META_QUESTION, 'left');
        //  $this->db->join('tags as system_tags', 'system_tags.id = assigned_system_tags.tag_id AND system_tags.delete_status = 1 AND system_tags.status = 1', 'left');



        /*$this->db->join('tag_assigned as assigned_courses_tags', 'assigned_courses_tags.reference_id = questions.id AND assigned_courses_tags.reference_type = '.META_COURSE.' AND  assigned_courses_tags.reference_sub_type ='.META_QUESTION, 'left');
        $this->db->join('tags as courses_tags', 'courses_tags.id = assigned_courses_tags.tag_id AND courses_tags.delete_status = 1 AND courses_tags.status = 1', 'left');

        $this->db->join('tag_assigned as assigned_modules_tags', 'assigned_modules_tags.reference_id = questions.id AND assigned_modules_tags.reference_type = '.META_MODULE.' AND  assigned_modules_tags.reference_sub_type ='.META_QUESTION, 'left');
        $this->db->join('tags as modules_tags', 'modules_tags.id = assigned_modules_tags.tag_id AND modules_tags.delete_status = 1 AND modules_tags.status = 1', 'left');

        $this->db->join('tag_assigned as assigned_lessons_tags', 'assigned_lessons_tags.reference_id = questions.id AND assigned_lessons_tags.reference_type = '.META_LESSON.' AND  assigned_lessons_tags.reference_sub_type ='.META_QUESTION, 'left');
        $this->db->join('tags as lessons_tags', 'lessons_tags.id = assigned_lessons_tags.tag_id AND lessons_tags.delete_status = 1 AND lessons_tags.status = 1', 'left');*/

        //$this->db->join('quiz_section_questions', 'quiz_section_questions.section_id ='.$postData['sectionID'], 'left');
        //pr($existIDS);
        if ($existIDS) {
            $this->db->where_not_in('questions.id', $existIDS);
        }

        /*End tags*/
        if (isset($postData['system_category']) && $postData['system_category'] != '') {
            $this->db->where_in('assigned_system_category.category_id', $postData['system_category']);
        }
        if (isset($postData['system_tag']) && $postData['system_tag']  != '') {
            $this->db->where_in('assigned_system_tags.tag_id', $postData['system_tag']);
        }
        if (isset($postData['assessment_category']) && $postData['assessment_category']  != '') {
            $this->db->where_in('category_assigned.category_id', $postData['assessment_category']);
        }

        if (isset($postData['blooms_level_category']) && $postData['blooms_level_category']  != '') {
            $this->db->where_in('bloom_level_category.category_id', $postData['blooms_level_category']);
        }

        if (isset($postData['assessment_tag']) && $postData['assessment_tag']  != '') {
            $this->db->where_in('assigned_assessment_tag.tag_id', $postData['assessment_tag']);
        }
        /*if (isset($postData['course_tag']) && $postData['course_tag'] > 0) 
        {
            $this->db->where(['assigned_courses_tags.tag_id' => $postData['course_tag']]);
        }
        if (isset($postData['module_tag']) && $postData['module_tag'] > 0) 
        {
            $this->db->where(['assigned_modules_tags.tag_id' => $postData['module_tag']]);
        }
        if (isset($postData['lesson_tag']) && $postData['lesson_tag'] > 0) 
        {
            $this->db->where(['assigned_lessons_tags.tag_id' => $postData['lesson_tag']]);
        }*/
        if (isset($postData['outcomes_category']) && $postData['outcomes_category']  != '') {
            $this->db->where_in('assigned_outcomes_category.category_id', $postData['outcomes_category']);
        }
        if (isset($postData['tel_mastery_standards']) && $postData['tel_mastery_standards']  != '') {
            $this->db->where_in('assigned_standards_category.category_id', $postData['tel_mastery_standards']);
        }
        /* if (isset($postData['compentencies_category']) && $postData['compentencies_category'] > 0) 
        {
            $this->db->where(['assigned_competencies_category.category_id' => $postData['compentencies_category']]);
        }
        if (isset($postData['skills_category']) && $postData['skills_category'] > 0) 
        {
            $this->db->where(['assigned_skills_category.category_id' => $postData['skills_category']]);
        }*/


        $this->db->where(['questions.status' => 1, 'questions.delete_status' => 1]);



        $i = 0;
        // loop searchable columns 
        foreach ($this->column_search as $item) {

            // if datatable send POST for search
            if ($postData['search']['value']) {
                // first loop
                if ($i === 0) {
                    // open bracket 
                    $this->db->group_start();
                    //pr( $item);	
                    $this->db->like($item, $postData['search']['value'], 'both');
                } else {
                    $this->db->or_like($item, $postData['search']['value'], 'both');
                }
                // last loop
                if (count($this->column_search) - 1 == $i) {
                    // close bracket
                    $this->db->group_end();
                }
            }
            $i++;
        }




        $this->db->group_by('questions.id');

        if (isset($postData['order'])) {
            $this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }


    /*
     * Fetch members data from the database
     * @param $_POST filter data based on the posted parameters
     */
    public function getSelectedRows($postData)
    {

        //$postData['sectionID']
        $existQueIDS = $this->__getExistQuestionsInSection($postData['sectionID']);
        // pr($existQueIDS);exit;
        $this->_get_selected_datatables_query($postData, $existQueIDS);
        if ($postData['length'] != -1) {
            $this->db->limit($postData['length'], $postData['start']);
        }
        $query = $this->db->get();
        $data['result'] = ($existQueIDS) ? $query->result() : 0;
        // /pr($this->db->last_query());
        $data['total'] = ($existQueIDS) ? $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total : 0;
        return $data;
    }

    /*
     * Perform the SQL queries needed for an server-side processing requested
     * @param $_POST filter data based on the posted parameters
     */
    private function _get_selected_datatables_query($postData, $existIDS = [])
    {


        $this->db->select("SQL_CALC_FOUND_ROWS questions.id,questions.title,questions.long_title", false);
        if ((isset($postData['assessment_category']) && count($postData['assessment_category']) > 0) || $postData['order']['0']['column'] == 3) {
            $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT categories.name ORDER BY categories.name ASC SEPARATOR "<br>"), \'NA\')  as assesment_category');
        } else {
            $this->db->select('"..." as assesment_category');
        }

        if ((isset($postData['assessment_tag']) && count($postData['assessment_tag']) > 0) || $postData['order']['0']['column'] == 4) {
            $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT assessment_tags.name ORDER BY assessment_tags.name ASC  SEPARATOR "<br>") , \'NA\') as assessment_tags');
        } else {
            $this->db->select('"..." as assessment_tags');
        }

        if ((isset($postData['system_category']) && count($postData['system_category']) > 0) || $postData['order']['0']['column'] == 5) {
            $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT system_category.name ORDER BY system_category.name ASC  SEPARATOR "<br>"), \'NA\')  as system_category');
        } else {
            $this->db->select('"..." as system_category');
        }

        if ((isset($postData['system_tag']) && count($postData['system_tag']) > 0) || $postData['order']['0']['column'] == 6) {
            $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT system_tags.name ORDER BY system_tags.name ASC  SEPARATOR "<br>"), \'NA\')  as system_tags');
        } else {
            $this->db->select('"..." as system_tags');
        }

        if ((isset($postData['outcomes_category']) && count($postData['outcomes_category']) > 0) || $postData['order']['0']['column'] == 7) {
            $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT outcomes_category.name ORDER BY outcomes_category.name ASC  SEPARATOR "<br>"), \'NA\')  as outcomes_category');
        } else {
            $this->db->select('"..." as outcomes_category');
        }

        if ((isset($postData['tel_mastery_standards']) && count($postData['tel_mastery_standards']) > 0) || $postData['order']['0']['column'] == 8) {
            $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT standards_category.name ORDER BY standards_category.name ASC  SEPARATOR "<br>"), \'NA\')  as standards_category');
        } else {
            $this->db->select('"..." as standards_category');
        }

        if ((isset($postData['blooms_level_category']) && count($postData['blooms_level_category']) > 0) || $postData['order']['0']['column'] == 9) {
            $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT blooms_level_category.name ORDER BY blooms_level_category.name ASC  SEPARATOR "<br>"), \'NA\')  as blooms_level_category');
        } else {
            $this->db->select('"..." as blooms_level_category');
        }







        /* $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT skills_category.name ORDER BY skills_category.name ASC  SEPARATOR "<br>"), \'NA\')  as skills_category');
        $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT competencies_category.name ORDER BY competencies_category.name ASC  SEPARATOR "<br>"), \'NA\')  as competencies_category');*/
        /*Tags*/

        /*Tags*/

        //System Tags   

        /*End Tags*/

        // $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT assessment_tags.name ORDER BY assessment_tags.name ASC  SEPARATOR "<br>"), \'NA\')  as assessment_tags');
        //  $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT system_tags.name ORDER BY system_tags.name ASC  SEPARATOR "<br>"), \'NA\')  as system_tags');
        /*   $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT courses_tags.name ORDER BY courses_tags.name ASC  SEPARATOR "<br>"), \'NA\')  as courses_tags');
        $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT modules_tags.name ORDER BY modules_tags.name ASC  SEPARATOR "<br>"), \'NA\')  as modules_tags');
        $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT lessons_tags.name ORDER BY lessons_tags.name ASC  SEPARATOR "<br>"), \'NA\')  as lessons_tags');*/
        /*End Tags*/
        $this->db->from('questions');
        if ((isset($postData['assessment_category']) && count($postData['assessment_category']) > 0) || $postData['order']['0']['column'] == 3) {
            $this->db->join('category_assigned', 'category_assigned.reference_id = questions.id AND category_assigned.reference_type = ' . META_ASSESSMENT . ' AND  category_assigned.reference_sub_type =' . META_QUESTION, 'left');
            $this->db->join('categories', 'categories.id = category_assigned.category_id AND categories.delete_status = 1 AND categories.status = 1', 'left');
        }

        if ((isset($postData['assessment_tag']) && count($postData['assessment_tag']) > 0) || $postData['order']['0']['column'] == 4) {
            $this->db->join('tag_assigned as assigned_assessment_tag', 'assigned_assessment_tag.reference_id = questions.id AND assigned_assessment_tag.reference_type = ' . META_ASSESSMENT . ' AND  assigned_assessment_tag.reference_sub_type =' . META_QUESTION, 'left');
            $this->db->join('tags as assessment_tags', 'assessment_tags.id = assigned_assessment_tag.tag_id AND assessment_tags.delete_status = 1 AND assessment_tags.status = 1', 'left');
        }

        if ((isset($postData['system_category']) && count($postData['system_category']) > 0) || $postData['order']['0']['column'] == 5) {
            $this->db->join('category_assigned as assigned_system_category', 'assigned_system_category.reference_id = questions.id AND assigned_system_category.reference_type = ' . META_SYSTEM . ' AND  assigned_system_category.reference_sub_type =' . META_QUESTION, 'left');
            $this->db->join('categories as system_category', 'system_category.id = assigned_system_category.category_id AND system_category.delete_status = 1 AND system_category.status = 1', 'left');
        }

        if ((isset($postData['system_tag']) && count($postData['system_tag']) > 0) || $postData['order']['0']['column'] == 6) {
            $this->db->join('tag_assigned as assigned_system_tags', 'assigned_system_tags.reference_id = questions.id AND assigned_system_tags.reference_type = ' . META_SYSTEM . ' AND  assigned_system_tags.reference_sub_type =' . META_QUESTION, 'left');
            $this->db->join('tags as system_tags', 'system_tags.id = assigned_system_tags.tag_id AND system_tags.delete_status = 1 AND system_tags.status = 1', 'left');
        }

        if ((isset($postData['outcomes_category']) && count($postData['outcomes_category']) > 0) || $postData['order']['0']['column'] == 7) {
            $this->db->join('category_assigned as assigned_outcomes_category', 'assigned_outcomes_category.reference_id = questions.id AND assigned_outcomes_category.reference_type = ' . META_LEARNING_OUTCOMES . ' AND  assigned_outcomes_category.reference_sub_type =' . META_QUESTION, 'left');
            $this->db->join('categories as outcomes_category', 'outcomes_category.id = assigned_outcomes_category.category_id AND outcomes_category.delete_status = 1 AND outcomes_category.status = 1', 'left');
        }

        if ((isset($postData['tel_mastery_standards']) && count($postData['tel_mastery_standards']) > 0) || $postData['order']['0']['column'] == 8) {
            $this->db->join('category_assigned as assigned_standards_category', 'assigned_standards_category.reference_id = questions.id AND assigned_standards_category.reference_type = ' . META_LEARNING_STANDARDS . ' AND  assigned_standards_category.reference_sub_type =' . META_QUESTION, 'left');
            $this->db->join('categories as standards_category', 'standards_category.id = assigned_standards_category.category_id AND standards_category.delete_status = 1 AND standards_category.status = 1', 'left');
        }

        if ((isset($postData['blooms_level_category']) && count($postData['blooms_level_category']) > 0) || $postData['order']['0']['column'] == 9) {
            $this->db->join('category_assigned as bloom_level_category', 'bloom_level_category.reference_id = questions.id AND bloom_level_category.reference_type = ' . BLOOMS_LEVEL . ' AND  bloom_level_category.reference_sub_type =' . META_QUESTION, 'left');
            $this->db->join('categories as blooms_level_category', 'blooms_level_category.id = bloom_level_category.category_id AND blooms_level_category.delete_status = 1 AND blooms_level_category.status = 1', 'left');
        }
        /*$this->db->join('category_assigned as assigned_skills_category', 'assigned_skills_category.reference_id = questions.id AND assigned_skills_category.reference_type = '.META_SKILLS.' AND  assigned_skills_category.reference_sub_type ='.META_QUESTION, 'left');
        $this->db->join('categories as skills_category', 'skills_category.id = assigned_skills_category.category_id  AND skills_category.delete_status = 1 AND skills_category.status = 1', 'left');

        $this->db->join('category_assigned as assigned_competencies_category', 'assigned_competencies_category.reference_id = questions.id AND assigned_competencies_category.reference_type = '.META_COMPETENCIES.' AND  assigned_competencies_category.reference_sub_type ='.META_QUESTION, 'left');
        $this->db->join('categories as competencies_category', 'competencies_category.id = assigned_competencies_category.category_id AND competencies_category.delete_status = 1 AND competencies_category.status = 1', 'left');*/
        /*Tags*/

        /*Tags*/




        // $this->db->join('tag_assigned as assigned_assessment_tag', 'assigned_assessment_tag.reference_id = questions.id AND assigned_assessment_tag.reference_type = '.META_ASSESSMENT.' AND  assigned_assessment_tag.reference_sub_type ='.META_QUESTION, 'left');
        // $this->db->join('tags as assessment_tags', 'assessment_tags.id = assigned_assessment_tag.tag_id AND assessment_tags.delete_status = 1 AND assessment_tags.status = 1', 'left');

        //  $this->db->join('tag_assigned as assigned_system_tags', 'assigned_system_tags.reference_id = questions.id AND assigned_system_tags.reference_type = '.META_SYSTEM.' AND  assigned_system_tags.reference_sub_type ='.META_QUESTION, 'left');
        // $this->db->join('tags as system_tags', 'system_tags.id = assigned_system_tags.tag_id AND system_tags.delete_status = 1 AND system_tags.status = 1', 'left');

        /*$this->db->join('tag_assigned as assigned_courses_tags', 'assigned_courses_tags.reference_id = questions.id AND assigned_courses_tags.reference_type = '.META_COURSE.' AND  assigned_courses_tags.reference_sub_type ='.META_QUESTION, 'left');
        $this->db->join('tags as courses_tags', 'courses_tags.id = assigned_courses_tags.tag_id AND courses_tags.delete_status = 1 AND courses_tags.status = 1', 'left');

        $this->db->join('tag_assigned as assigned_modules_tags', 'assigned_modules_tags.reference_id = questions.id AND assigned_modules_tags.reference_type = '.META_MODULE.' AND  assigned_modules_tags.reference_sub_type ='.META_QUESTION, 'left');
        $this->db->join('tags as modules_tags', 'modules_tags.id = assigned_modules_tags.tag_id AND modules_tags.delete_status = 1 AND modules_tags.status = 1', 'left');

        $this->db->join('tag_assigned as assigned_lessons_tags', 'assigned_lessons_tags.reference_id = questions.id AND assigned_lessons_tags.reference_type = '.META_LESSON.' AND  assigned_lessons_tags.reference_sub_type ='.META_QUESTION, 'left');
        $this->db->join('tags as lessons_tags', 'lessons_tags.id = assigned_lessons_tags.tag_id AND lessons_tags.delete_status = 1 AND lessons_tags.status = 1', 'left');*/

        //$this->db->join('quiz_section_questions', 'quiz_section_questions.section_id ='.$postData['sectionID'], 'left');
        //pr($existIDS);
        $this->db->where_in('questions.id', $existIDS);

        /*End tags*/
        if (isset($postData['system_category']) && $postData['system_category'] != '') {
            $this->db->where_in('assigned_system_category.category_id', $postData['system_category']);
        }
        if (isset($postData['system_tag']) && $postData['system_tag'] != '') {
            $this->db->where_in('assigned_system_tags.tag_id', $postData['system_tag']);
        }
        if (isset($postData['assessment_category']) && $postData['assessment_category'] != '') {
            $this->db->where_in('category_assigned.category_id', $postData['assessment_category']);
        }
        if (isset($postData['assessment_tag']) && $postData['assessment_tag'] != '') {
            $this->db->where_in('assigned_assessment_tag.tag_id', $postData['assessment_tag']);
        }
        if (isset($postData['blooms_level_category']) && $postData['blooms_level_category']  != '') {
            $this->db->where_in('bloom_level_category.category_id', $postData['blooms_level_category']);
        }

        /*  if (isset($postData['course_tag']) && $postData['course_tag'] != '') 
        {
            $this->db->where(['assigned_courses_tags.tag_id' => $postData['course_tag']]);
        }
        if (isset($postData['module_tag']) && $postData['module_tag'] != '') 
        {
            $this->db->where(['assigned_modules_tags.tag_id' => $postData['module_tag']]);
        }
        if (isset($postData['lesson_tag']) && $postData['lesson_tag'] != '') 
        {
            $this->db->where(['assigned_lessons_tags.tag_id' => $postData['lesson_tag']]);
        }*/
        if (isset($postData['outcomes_category']) && $postData['outcomes_category'] != '') {
            $this->db->where_in('assigned_outcomes_category.category_id', $postData['outcomes_category']);
        }
        if (isset($postData['tel_mastery_standards']) && $postData['tel_mastery_standards'] != '') {
            $this->db->where_in('assigned_standards_category.category_id', $postData['tel_mastery_standards']);
        }
        /*if (isset($postData['compentencies_category']) && $postData['compentencies_category'] != '') 
        {
            $this->db->where(['assigned_competencies_category.category_id' => $postData['compentencies_category']]);
        }
        if (isset($postData['skills_category']) && $postData['skills_category'] != '') 
        {
            $this->db->where(['assigned_skills_category.category_id' => $postData['skills_category']]);
        }*/


        $this->db->where(['questions.status' => 1, 'questions.delete_status' => 1]);



        $i = 0;
        // loop searchable columns 
        foreach ($this->column_search as $item) {

            // if datatable send POST for search
            if ($postData['search']['value']) {
                // first loop
                if ($i === 0) {
                    // open bracket 
                    $this->db->group_start();
                    //pr( $item);	
                    $this->db->like($item, $postData['search']['value'], 'both');
                } else {
                    $this->db->or_like($item, $postData['search']['value'], 'both');
                }
                // last loop
                if (count($this->column_search) - 1 == $i) {
                    // close bracket
                    $this->db->group_end();
                }
            }
            $i++;
        }



        $this->db->group_by('questions.id');

        if (isset($postData['order'])) {
            $this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
}
