<?php
/**
*
*/
class Common_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}
	
	function InsertData($table,$Data)
	{
		$Insert = $this->db->insert($table,$Data);
		if ($Insert):
			return true;
		endif;
	}
	function getAllData($table,$specific='',$row='',$Where='',$order='',$limit='',$groupBy='',$like = '')
	{	

		// If Condition
		if (!empty($Where)):
			$this->db->where($Where);
		endif;
		// If Specific Columns are require
		if (!empty($specific)):
			$this->db->select($specific);
		else:
			$this->db->select('*');
		endif;

		if (!empty($groupBy)):
			$this->db->group_by($groupBy);
		endif;
		// if Order
		if (!empty($order)):
			$this->db->order_by($order);
		endif;
		// if limit
		if (!empty($limit)):
			$this->db->limit($limit);
		endif;

		//if like
		if(!empty($like)):
			$this->db->like($like);
		endif;	
		// get Data
		
		//if select row
		if(!empty($row)):
			$GetData = $this->db->get($table);
			return $GetData->row();
		else:
			$GetData = $this->db->get($table);
			return $GetData->result();
		endif;	
	}
	function UpdateDB($table,$Where,$Data)
	{
		$this->db->where($Where);
		$Update = $this->db->update($table,$Data);
		if ($Update):
			return true;
		else:
			return false;
		endif;
	}
	
    function DeleteDB($table,$where)
    {
    	$this->db->where($where);
    	$done = $this->db->delete($table);
    	if ($done) {
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    }

   
    /**
     * getCountRecord method
     * @description this method is use to get count record
	 * @param string, number
     * @return number
     */	
    public function getCountRecord($table,$condition) {
    	$this->db->where($condition);
		if($query = $this->db->get($table))
		{	
			return $query->num_rows();
		}
    }


    /**
     * getRecordByID method
     * @description this method is use to get  record
	 * @param string, number
     * @return array
     */	
    public function getRecordByID($table,$condition,$columns = '') {

    	if($columns)
    	{
    		$this->db->select($columns);
    	}	
    	$this->db->where($condition);
		if($query = $this->db->get($table))
		{	
			return $query->row();
		}
    }

    /**
     * getDatawithIncluse method
     * @description this method is use to get date with multiple conditions
	 * @param string, int
     * @return array
     */	
	public function getDatawithIncluse($tableName,$columns= '', $incluseArray='',$incluseColumn='', $where= '' ) {
		
		// If Specific Columns are require
		if (!empty($columns)):
			$this->db->select($columns);
		else:
			$this->db->select('*');
		endif;
		$this->db->from($tableName);

		// If Condition
		if (!empty($where)):
			$this->db->where($where);
		endif;
		if (!empty($incluseArray)):
			$this->db->where_in($incluseColumn,$incluseArray);
		endif;
		
		$query = $this->db->get();
		/*$query->result_array();
		pr($this->db->last_query());*/
		return $query->result_array();
    }

     /**
     * InsertBatchData method
     * @description this method is use to save mutiple record in signle query
	 * @param array
     * @return boolean
     */	

    public function InsertBatchData($table,$Data)
	{
		$Insert = $this->db->insert_batch($table,$Data);		
		if ($Insert):
			return true;
		endif;
	}

	
	
	public function multiUpdateRecords($table,$selectedRecordID = [], $column)
    {	
    	if ($this->db->update_batch($table,$selectedRecordID, $column)) {
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    }

    /**
     * getDatawithIncluse method
     * @description this method is use to get date with multiple conditions
	 * @param string, int
     * @return array
     */	
	public function getCountRecordUsingincluse($tableName,$columns= '', $incluseArray='',$incluseColumn='', $where= '' ) {
		
		// If Specific Columns are require
		if (!empty($columns)):
			$this->db->select($columns);
		else:
			$this->db->select('*');
		endif;
		$this->db->from($tableName);

		// If Condition
		if (!empty($where)):
			$this->db->where($where);
		endif;
		if (!empty($incluseArray)):
			$this->db->where_in($incluseColumn,$incluseArray);
		endif;
		
		$query = $this->db->get();
		/*$query->result_array();
		pr($this->db->last_query());*/
		return $query->num_rows();
    }


    public function MultiDeleteDB($table, $where = '', $selectedID = [],$column)
    {	

    	$this->db->where_in($column,$selectedID);
    	$this->db->where($where);
    	$done = $this->db->delete($table);
    	if ($done) {
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    }

    
     
}
?>
