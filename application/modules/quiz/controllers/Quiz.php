<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Quiz extends MY_Controller
{

    public function __construct()
    {
        //ini_set('display_errors', 1);
        parent::__construct();
        //Do your magic here

        $this->load->module('template');
        $this->load->model('common_model');
        $this->load->helper(['html', 'form']);
        $this->load->library('form_validation');

        if (!$this->ion_auth->logged_in()) :
            redirect('users/auth', 'refresh');
        endif;
    }

    /**
     * setup method
     * @description this function use to create container
     * @return void
     */
    public function index()
    {
        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Quiz', 'link' => '', 'class' => 'active'];

        /*$quizType = $this->common_model->getAllData('meta_levels', ['title','id','type'], '', ['delete_status' => 1, 'status' => 1]);

        $meta_quiz_data = [];
        foreach ($quizType as $key => $value) {
            $meta_quiz_data[$value->type][] = [
                'id' => $value->id,
                'title' => $value->title
            ];
        }
        $data['quizType'] = $meta_quiz_data[QUIZ_TYPE];
        $data['quizLevel'] = $meta_quiz_data[QUIZ_LEVEL];*/
      
        $data['page'] = "quiz/list";
        $this->template->template_view($data);
    }



    /**
     * add method
     * @description this method is use to insert site creatation data in databse
     * @param string, numbers
     * @return void
     */
    public function add($id = null)
    {
        //echo "sss";exit;   
        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Quiz', 'link' => base_url('quiz'), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Add', 'link' => '', 'class' => 'active'];  
        $data['page'] = "quiz/add";
        $this->session->set_userdata('action', 'add');
        $this->template->template_view($data);
    }


    /**
     * edit method
     * @description this method is use to insert site creatation data in databse
     * @param string, numbers
     * @return void
     */
    public function edit($id = null)
    {   
        if(is_numeric(base64_decode($id))){
            $condition = [
                'id' =>base64_decode($id),
                'delete_status' => 1
            ];
            if ($this->common_model->getCountRecord('quizzes', $condition) == 0) {
                $this->session->set_flashdata('error', "Something went wrong");
                redirect('quiz', 'refresh');
            }
        }else{
            $this->session->set_flashdata('error', "Something went wrong");
            redirect('quiz', 'refresh');
        }
        $this->session->set_userdata('action', 'edit');
        
        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Quiz', 'link' => base_url('quiz'), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Edit', 'link' => '', 'class' => 'active']; 

        $data['recordID'] = base64_decode($id);
        $data['page'] = "quiz/edit";
        $this->template->template_view($data);
    }

    /**
     * load_view method
     * @description this function use to load view in tab
     * @return html
     */
    public function load_view($viewType = '', $recordID = '')
    {   
       

        switch ($viewType) {
            case 'content':
                
                $data['types'] = $this->common_model->getAllData('quiz_assessment_type', ['name','id'], '', ['status' => 1,'type' => 1]);

                $template_path = "quiz/add-content";
                if($recordID > 0)
                {   
                    $columns = 'quizzes.title,quizzes.total_points,quizzes.type,quizzes.long_title,quizzes.instruction,quizzes.feedback,quizzes.id';
                    $condition = [
                        'quizzes.id' => $recordID
                    ];
                    $data['details'] = $this->common_model->getRecordByID('quizzes', $condition, $columns);

                }else{
                    $data['details'] = [];
                }    
               // pr($data);
            break;
            case 'setting':
                $template_path = "quiz/add-setting";

                /*$data['quizType'] = $this->common_model->getAllData('meta_levels', ['title','id','type'], '', ['delete_status' => 1, 'status' => 1,'type'=>3]);*/

                if($recordID > 0)
                {   
                    $columns = 'quizzes.number_attempt,quizzes.gradebook_record,quizzes.view_result_btn';
                    $condition = [
                        'quizzes.id' => $recordID,
                        'delete_status' => 1, 
                        'status' => 1
                    ];
                    $data['details'] = $this->common_model->getRecordByID('quizzes', $condition, $columns);
                    /*$data['sections'] = $this->common_model->getAllData('quiz_sections', ['title', 'description','id','weight'], '', ['delete_status' => 1, 'status' => 1,'quiz_id'=>$recordID]);
                    $data['sections_count'] = (count($data['sections']) == 0)?1:count($data['sections']);*/
                }else{
                    $data['details'] = [];
                  //  $data['sections_count'] = 1;
                } 
            break;
            case 'metadata':

                $template_path = "quiz/add-metadata";
                if($recordID > 0)
                {   
                    $columns = 'quizzes.skill_mastery_point,quizzes.level';
                    $condition = [
                        'quizzes.id' => $recordID,
                        'delete_status' => 1, 
                        'status' => 1
                    ];
                    $data['details'] = $this->common_model->getRecordByID('quizzes', $condition, $columns);
                }else{
                    $data['details'] = [];
                }
            break;
            case 'questions':
                $template_path = "quiz/add-questions";
                 if($recordID > 0)
                {   
                    $data['sections'] = $this->common_model->getAllData('quiz_sections', ['title','id'], '', ['delete_status' => 1, 'status' => 1,'quiz_id'=>$recordID]);
                }else{
                    $data['sections'] = [];
                  //  $data['sections_count'] = 1;
                } 
            break;
        }
        $data['recordID'] = $recordID;
        echo $this->load->view($template_path, $data, true);
        exit;
    }



    public  function getSectionPart()
    {
        $template_path = "quiz/get-section-part";
        if($this->input->post())
        {   
            $data['recordID'] = post('recordID');
            $data['sections'] = $this->common_model->getAllData('quiz_sections', ['title', 'description','id','weight'], '', ['delete_status' => 1, 'status' => 1,'quiz_id'=>post('recordID')]);
            $data['sections_count'] = (count($data['sections']) == 0)?1:count($data['sections']);
        }
        echo $this->load->view($template_path, $data, true);
        exit;
    }

    /**
     * getCheckExist method
     * @description this method is use to check name is already exist or not
     * @param string
     * @return json array
     */
    public function getCheckExist()
    {
        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        if ($this->form_validation->run() == FALSE) 
        {
            $this->setup();
        } else {
            $this->load->model('Quizzes_modal');
            $conditions = [
                'title' => $this->input->post('title'),
                'delete_status' => 1
            ];
            if(post('recordID') != 0 && post('recordID') != '')
            {
                $conditions['id !='] = post('recordID');
            }

            if ($this->Quizzes_modal->getCount($conditions,'quizzes') == 0) 
            {
                echo 'true';
                exit;
            } else {
                echo 'false';
                exit;
            }
        }
        exit;
    }


    public function saveContents()
    {
        if($this->input->post())
        {   
            $saveData = [
                'type' => post('type'),
                'created_by' => $this->session->userdata('user_id'),
                'title' => post('title'),
                'long_title' => post('long_title'),
                'instruction' => post('quizInstructions'),
                'feedback' => post('quizFeedback'),
                'total_points' => post('total_points'),
                'created' => date("Y-m-d H:i:s"),
            ];
            if(post('recordID') > 0){
                if ($this->common_model->UpdateDB('quizzes', ['id' => post('recordID')], $saveData)) {
                    echo json_encode(['code' => 200, 'message' => "Content has been successfully saved",'id'=>post('recordID')]);
                    exit;
                }
                    echo json_encode(['code' => 400, 'message' => "Something went wrong, please try again!"]);
                    exit;

            }else{
                if($this->common_model->InsertData('quizzes', $saveData)){
                    echo json_encode(['code' => 200, 'message' => "Content has been successfully saved",'id'=>$this->db->insert_id()]);
                    exit;   
                }
                    echo json_encode(['code' => 400, 'message' => "Something went wrong, please try again!"]);
                    exit; 
            }
              
        }
        exit;    
    }

    public function saveSetting()
    {
        if($this->input->post())
        {
            $quizData = [
                'number_attempt' => post('number_attempt'),
                'gradebook_record' => post('gradebook_record'),
                'view_result_btn' => (post('view_result_btn'))?post('view_result_btn'):0              
            ];
            $totalWeight = 0;
            foreach (post('section') as $key => $value) {
                $totalWeight += $value['weight'];  
            }
            if($totalWeight != 100){
                echo json_encode(['code' => 400, 'message' => "Section weight must be equal to 100%"]);
                exit;
            }
            $sectionDataArray = [];
            foreach (post('section') as $key => $value) {
                $sectionData = [
                    'title' => $value['title'],
                    'description' => $value['description'],
                    'weight' => $value['weight']
                ];
                if($sectionData){
                    $this->common_model->UpdateDB('quiz_sections', ['quiz_sections.quiz_id' => post('recordID'),'quiz_sections.id'=>base64_decode($value['section_id'])], $sectionData);
                }
                if($value['section_id'] == 'new_section'){
                    $sectionDataArray[] = [
                        'quiz_id' => post('recordID'),
                        'title' => $value['title'],
                        'description' => $value['description'],
                        'weight' => $value['weight']
                    ];
                }
            }
            if ($this->common_model->UpdateDB('quizzes', ['id' => post('recordID')], $quizData)) {
                /* echo json_encode(['code' => 200, 'message' => "Setting has been saved successfully"]);
                        exit;*/
                clean_cache('api', 'tel_courseDetailsBySlugs_');
                clean_cache('api', 'tel_getLessonByLessonSlug_');
                clean_cache('api', 'tel_moduleDetailsBySlugs_');        
                if($sectionDataArray){
                    if($this->db->insert_batch('quiz_sections', $sectionDataArray )){
                        echo json_encode(['code' => 200, 'message' => "Setting has been saved successfully"]);
                        exit;  
                    } 
                }else{
                    echo json_encode(['code' => 200, 'message' => "Setting has been saved successfully"]);
                    exit;
                }
            }
            echo json_encode(['code' => 400, 'message' => "Something went wrong, please try again!"]);
            exit; 
        }
        exit;
    }

    public function save_skill_mastery_points()
    {
        if($this->input->post())
        {
            $quizData = [
                'skill_mastery_point' => post('skill_mastery_point'),
                'level' => (post('level'))?post('level'):0
            ];

            if ($this->common_model->UpdateDB('quizzes', ['id' => post('recordID')], $quizData)) {
                echo json_encode(['code' => 200, 'message' => "Metadata has been saved successfully"]);
                exit;
            }
            echo json_encode(['code' => 400, 'message' => "Something went wrong, please try again!"]);
            exit; 
        }
        exit;  
    }


    public function get_active_questions()
    {
        $data = [];
        if($this->input->post())
        {   
            //pr($this->input->post());
            $data['quizID'] = $this->input->post('quizID'); 
            $data['sectionID'] = $this->input->post('sectionID'); 
        }    
        $template_path = "quiz/popup-questions";
        echo $this->load->view($template_path, $data, true);
        exit;
    }

    public function get_selected_section_part(){
        $data = [];
        if($this->input->post())
        {   
            $data['quizID'] = $this->input->post('quizID'); 
            $data['sectionID'] = $this->input->post('sectionID');
            $data['sectionDetails'] = $this->common_model->getRecordByID('quiz_sections',['quiz_sections.id'=>$this->input->post('sectionID')],'quiz_sections.title'); 
        }    
        $template_path = "quiz/get_selected_section_part";
        echo $this->load->view($template_path, $data, true);
        exit;
    }

    /**
     * getLists method 
     * @description this function called via ajax request, use to display list of questions
     * @return void
     */
    public function getLists() 
    {
        $this->load->model('Quizzes_modal');
        $data = $this->Quizzes_modal->getRows($_POST);
        //pr($data);
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $data['total'],
            "recordsFiltered" => $data['total'],
            "data" => $data['result']
        );
        echo json_encode($output);
    }

    

     /**
     * get_make_action_request method
     * @description this method is use to make action as per request(like status change)
     * @param int
     * @return json array
     */
    public function get_make_action_request()
    {
        if ($this->input->post()) {
           // pr($this->input->post());exit;
            $updateData = [];
            switch ($this->input->post('actionType')) {
                case 'status':
                    $updateData = [
                        'status' => post('status')
                    ];
                    $message = "You has been changed status successfully";
                    break;

                case 'delete':
                    $updateData = [
                        'delete_status' => post('status')
                    ];
                    $message = "You has been removed record successfully";
                    break;
            }
            if ($this->common_model->UpdateDB('quizzes', ['id' => post('id')], $updateData)) {
                echo json_encode(['code' => 200, 'message' => $message]);
                exit;
            }
            echo json_encode(['code' => 400, 'message' => "Something went wrong, please try again!"]);
            exit;
        }
        exit;
    }

    public function get_remove_quiz()
    {
        if ($this->input->post()) {
            $postArray = [];
            foreach ($this->input->post('selected_record') as $key => $value) {
                $postArray[] = [
                    'id'            => $value,
                    'delete_status' => 0
                ];
            }
            if ($this->common_model->multiUpdateRecords('quizzes', $postArray, 'id')) {
                clean_cache('api', 'tel_courseDetailsBySlugs_');
                clean_cache('api', 'tel_getLessonByLessonSlug_');
                clean_cache('api', 'tel_moduleDetailsBySlugs_'); 
                echo json_encode(['code' => 200, 'message' => "You have been removed record successfully"]);
                exit;
            }
            echo json_encode(['code' => 400, 'message' => "Something went wrong, please try again!"]);
            exit;
        }
        exit;
    }


    public function get_export_quiz()
    {
        if($this->input->post())
        {   
            $file_name = 'quiz_list'.strtotime("now"). '.csv';
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$file_name");
            header("Content-Type: application/csv;");
            // get data
            $_POST['export_csv'] = true;
            $_POST['selected_site_ids'] = $this->input->post('selected_record');

            $this->load->model('Quizzes_modal');
            $data = $this->Quizzes_modal->getQuizRowsForExport($_POST);
          //  pr($data);exit;
            // file creation
            $file = fopen('php://output', 'w');

            $header = array("Sr. No", "Title","Long title", "Assessment Category", "System Category", "Learning Outcome",/*"Quiz Type",*/"Quiz Level", "TEL Mastery Standards",/*"Skill Category","Competencies Category",*/"Assessment Tag","System Tag",/*"Course Tag","Module Tag","Lesson Tag",*/ "Status");
            fputcsv($file, $header);
            $i = 1;
            foreach ($data['result'] as $key => $value) {
                $nstatus = ($value->status == 1) ? 'Active' : 'Inactive';
                $narray = [
                            $i, 
                            $value->title, 
                            $value->long_title,
                            $value->assesment_category, 
                            $value->system_category, 
                            $value->outcomes_category,
                            /*$value->quiz_type_category,*/
                            $value->level, 
                            $value->standards_category,
                            /*$value->skills_category,
                            $value->competencies_category,*/
                            $value->assessment_tags,
                            $value->system_tags,
                           /* $value->courses_tags,
                            $value->modules_tags,
                            $value->lessons_tags,*/
                            $nstatus
                        ];
                fputcsv($file, $narray);
                $i++;
            }
            fclose($file);
            exit;
        }   
       exit; 
    }


    public function get_check_quiz_section_count(){
        if($this->input->post())
        {
            $condition = [
                'quiz_id' =>post('quizID'),
                'delete_status' => 1
            ];
            $count = $this->common_model->getCountRecord('quiz_sections', $condition);
            echo json_encode(['count'=>$count]);
            exit;
        }
        exit;
    }


    public function removeSection()
    {
        if($this->input->post())
        {   
            $updateData = [
                'delete_status' => 0
            ];
            if(post('record_ID') > 0){
                if ($this->common_model->UpdateDB('quiz_sections', ['id' => post('record_ID')], $updateData)) {
                    clean_cache('api', 'tel_courseDetailsBySlugs_');
                    clean_cache('api', 'tel_getLessonByLessonSlug_');
                    clean_cache('api', 'tel_moduleDetailsBySlugs_'); 
                    $this->__getMakeDivideWeight(post('record_ID'),post('quizID'));
                    echo json_encode(['code' => 200, 'message' => "Section has been successfully removed",'id'=>post('record_ID')]);
                    exit;
                }
                    echo json_encode(['code' => 400, 'message' => "Something went wrong, please try again!"]);
                    exit;

            }
             echo json_encode(['code' => 400, 'message' => "Something went wrong, please try again!"]);
                    exit; 
              
        }
        exit;    
    }


    private function __getMakeDivideWeight($recordID, $quizID)
    {
        $columns = 'quiz_sections.weight';
        $condition = [
            'quiz_sections.id' => $recordID
        ];
        $remove_quiz_section_weight = $this->common_model->getRecordByID('quiz_sections', $condition, $columns);
        $quiz_sections_record = $this->common_model->getAllData('quiz_sections', ['weight','id'], '', ['delete_status' => 1, 'status' => 1,'quiz_id'=>$quizID , 'id !='=>$recordID]);
        $division_number = floor($remove_quiz_section_weight->weight/count($quiz_sections_record));
        $i = 1;
        $remove_weight = $remove_quiz_section_weight->weight;
        foreach ($quiz_sections_record as $key => $value) {
            if($i == count($quiz_sections_record) ){
                $update_weight = $value->weight + $remove_weight;
                if($this->common_model->UpdateDB('quiz_sections', ['id' =>$value->id], ['weight'=>$update_weight])){
                    $remove_weight  = $remove_weight - $division_number;
                }
            }else{
                $update_weight = $value->weight + $division_number;
                if($this->common_model->UpdateDB('quiz_sections', ['id' =>$value->id], ['weight'=>$update_weight])){
                    $remove_weight  = $remove_weight - $division_number;
                }
            }

            $i++;
        }
    }

}
