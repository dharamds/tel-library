<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Questions extends MY_Controller {

    public function __construct() {
        //ini_set('display_errors', 2);
        parent::__construct();
        $this->load->module('template');
        $this->load->helper(array('html', 'form'));
        $this->load->library('form_validation');

        if (!$this->ion_auth->logged_in()):
            redirect('users/auth', 'refresh');
        endif;
    }
    

    /**
     * getSelectedLists method 
     * @description this function called via ajax request, use to display list of selected questions
     * @return void
     */
    public function getSelectedLists() 
    {
        $this->load->model('Questions_modal');
      //  pr($_POST);exit;
        $data = $this->Questions_modal->getSelectedRows($_POST);
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $data['total'],
            "recordsFiltered" => $data['total'],
            "data" => $data['result']
        );
        echo json_encode($output);
    }
   
     /**
     * getLists method 
     * @description this function called via ajax request, use to display list of questions
     * @return void
     */
    public function getLists() 
    {
        $this->load->model('Questions_modal');
        $data = $this->Questions_modal->getRows($_POST);
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $data['total'],
            "recordsFiltered" => $data['total'],
            "data" => $data['result']
        );
        echo json_encode($output);
    }


     /**
     * selectedAssigned method 
     * @description this function called via ajax request, use to add selected record to sections
     * @param int
     * @return json array
     */
    public function selectedAssigned()
    {
        if($this->input->post())
        {
            $selectedQue = $this->input->post('selected_record');
            $quizID = $this->input->post('quizID');
            $sectionID = $this->input->post('sectionID');
            foreach ($selectedQue as $key => $value) {
                $this->load->model('common_model');
                $check_exist = $this->common_model->getRecordByID('quiz_section_questions',['quiz_id' => $quizID,'question_id' => $value],'quiz_section_questions.id'); 
                //pr($check_exist,"j");
                if(empty($check_exist)) {
                    $saveData[] = [
                        'quiz_id' => $quizID,
                        'section_id' => $sectionID,
                        'question_id' => $value,
                    ];
                }
                //pr($saveData,"kj");
                
            }
            if(!empty($saveData)) {
                if($this->db->insert_batch('quiz_section_questions', array_filter($saveData)))
                {
                    clean_cache('api', 'tel_courseDetailsBySlugs_');
                    clean_cache('api', 'tel_getLessonByLessonSlug_');
                    clean_cache('api', 'tel_moduleDetailsBySlugs_'); 
                    echo json_encode(['code'=>200,'message'=>"Questions has been added successfully"]);
                    exit;
                }
                echo json_encode(['code' => 400, 'message' => "Something went wrong, please try again!"]);
                exit;
            } else {
                echo json_encode(['code' => 400, 'message' => "Question already exist in this quiz!"]);
                exit;
            }
            
                
        }
        exit;
    }

    public function selectedRemove(){
        if($this->input->post())
        {
            $selectedQue = $this->input->post('selected_record');
            $where = [
                    'section_id' => $this->input->post('sectionID'),
                    'quiz_id'    => $this->input->post('quizID')
            ];
            
            $this->load->model('common_model');
            if($this->common_model->MultiDeleteDB('quiz_section_questions',$where,$selectedQue,'question_id'))
            {
                clean_cache('api', 'tel_courseDetailsBySlugs_');
                clean_cache('api', 'tel_getLessonByLessonSlug_');
                clean_cache('api', 'tel_moduleDetailsBySlugs_'); 
                echo json_encode(['code'=>200,'message'=>"Questions has been removed successfully"]);
                exit;
            }
                echo json_encode(['code' => 400, 'message' => "Something went wrong, please try again!"]);
                exit;
        }
        exit;
    }

    



}
