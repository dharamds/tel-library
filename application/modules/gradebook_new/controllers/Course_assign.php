<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Course_assign extends MY_Controller
{

    public function __construct()
    {

        parent::__construct();
        //Do your magic here

        $this->load->module('template');
        $this->load->model('common_model');
        $this->load->model('Course_assign_model');
        $this->load->library('form_validation');
        $this->load->helper(array('html', 'language', 'form', 'country_helper'));
        if (!$this->ion_auth->logged_in()):
            redirect('users/auth', 'refresh');
        endif;

        if (!$this->ion_auth->is_admin()):
            return show_error("You Must Be An Administrator To View This Page");
        endif;
    }

    /**
     * [Load poll list]
     * @return [void]
     */
    public function index()
    {
        if (!$this->ion_auth->is_admin()) {
            return show_error('You Must Be an Administrator To view This Page');
        }
        
        //$data['Gradebook'] = $this->Gradebook_model->Course_assign_list();
        $data['page'] = "Gradebook/Course_assign/view";
        $this->template->template_view($data);

    }

    function getLists(){       
        // Fetch records
        $Data = $this->Course_assign_model->getRows($_POST);
        $i = $_POST['start'];
        $data = [];
        foreach($Data as $list){                 
            $i++;
            $data[] = array(
                    "id"                => $list->id, 
                    "sr_no"             => $i, 
                    "containers_name"   => $list->containers_name, 
                    "created"           => date("m-d-Y", strtotime($list->created_at)),
                    "status"            => $list->status,
                );
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Gradebook_model->count_gradebook(),
            "recordsFiltered" => $this->Gradebook_model->countFiltered($_POST),
            "data" => $data,
        );
        // Output to JSON format
        echo json_encode($output);
    }

    //create gradebook page
    public function save()
    {
        $this->benchmark->mark('code_start');
        if (!$this->ion_auth->is_admin()) {
            return show_error('You Must Be an Administrator To view This Page');
        }
        /*********Container**************/
        $data['container_list'] = $this->Course_assign_model->get_container_list(); //get container list 
        $container[""] = "Select";
        foreach ($data['container_list'] as $value) {
            $container[$value['id']] = $value['name'];
        }
        $data['container_list'] = $container;
        /**********Course*************/
        $data['course_list'] = $this->Course_assign_model->get_course_list(); //get course list 
        foreach ($data['course_list'] as $value) {
            $course[$value['id']] = $value['name'];
        }
        $data['course_list'] = $course;
        
        $data['page'] = 'Gradebook/Course_assign/create';
        $this->template->template_view($data);
    }
    /**
     * [Add New Poll]
     */
    public function add()
    {
        $this->form_validation->set_rules('container_id', 'Institution', 'trim|required');
        $this->form_validation->set_rules('course_id[]', 'Course', 'trim|required');

        if ($this->form_validation->run() == false) {
            $msg = 'Grader\'s name should not be blank.';
            $this->session->set_flashdata('error', $msg);

        } else {
            if(post(id)=='') {
                $delete_where=array(
                    "container_id" => $id
                );
                $this->common_model->DeleteDB('container_courses', $delete_where);
            }
            $cid=post(course_id);
            foreach($cid as $data) {
                $data = array(
                    'assigned_by' => $this->session->userdata('user_id'),
                    'container_id' => post('container_id'),
                    'course_id' => $data,
                    'status' => 1,
                    'created' => date("Y-m-d H:i:s"),
                );
                $this->common_model->InsertData('container_courses', $data);
            }
            
            
           set_flashdata('success', 'Added Successfully');
        }
        redirect('Gradebook/Gradebook/index', 'refresh');
    }

    //Gradebook edit page
    public function edit()
    {

        $this->session->set_flashdata('message', $this->ion_auth->messages());
        $data['data'] = $this->Gradebook_model->edit($this->uri->segment(4));
        $data['id'] = $this->uri->segment(4);
        $data['Gradebook_options'] = $this->Gradebook_model->get_category_list($this->uri->segment(4));
        $cat[""] = "Select Category";
        foreach ($data['Gradebook_options'] as $value) {
            $cat[$value['id']] = $value['name'];
        }
        $data['Gradebook_options'] = $cat;
        $data['Gradebook_data'] = $this->Gradebook_model->edit($this->uri->segment(4));
        $data['page'] = 'Gradebook/Gradebook/edit';
        $this->template->template_view($data);
    }

    // update Gradebook functionality
    public function update()
    {
        $this->form_validation->set_rules('category_id', 'Gradebook\'s Name', 'trim|required');
        

        if ($this->form_validation->run() === false) {
            $msg = 'Gradebook\'s name should not be blank.';
            $this->session->set_flashdata('error', $msg);
            redirect('Gradebook/Gradebook/' . $this->input->post('id'));
        } else {

            $data = array(
                'created_by' => $this->session->userdata('user_id'),
                'category_id' => post('category_id'),
                'template' => post('template'),
                'description' => post('description'),
                'status' => 1,
                'delete_status' => 1,
                'updated_at' => date("Y-m-d H:i:s"),
            );
            //pr(); exit;
            if ($this->Gradebook_model->update($this->input->post('id'), $data)) {
                $msg = "Updated successfully";
                $this->session->set_flashdata('success', $msg);
                redirect('Gradebook/Gradebook/', 'refresh');
            } else {
                $this->session->set_flashdata('error', 'Something went wrong');
                redirect('Gradebook/Gradebook/', 'refresh');
            }
        }
    }

    //delete Gradebook
    public function delete()
    {
        $this->session->set_flashdata('success', $this->ion_auth->messages());
        if ($this->input->post('status') == '') {
            $status = 0;
        } else {
            $status = 1;
        }
        $additional_data = array(
            'delete_status' => $status,
        );
        $this->Gradebook_model->update($this->uri->segment(4), $additional_data);
        echo json_encode(array("msg"=>"deleted")); exit;
    }

    //update Gradebook status
    public function update_status()
    {
        
        if ($this->uri->segment(5) == 'activate') {
            $data = array(
                "status" => 1,
            );
        } else {
            $data = array(
                "status" => 0,
            );
        }

        $this->session->set_flashdata('success', $this->ion_auth->messages());
        $this->Gradebook_model->update_status($this->uri->segment(4), $data);
        $msg = "Updated successfully";
        $this->session->set_flashdata('success', $msg);
        redirect('Gradebook/Gradebook/', 'refresh');
    }

}

/* End of file Posts.php */
/* Location: ./application/modules/blog/controllers/Posts.php */
