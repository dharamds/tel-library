<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Gradebook extends MY_Controller {

    public function __construct() {

        parent::__construct();
        //Do your magic here

        $this->load->module('template');
        $this->load->model('common_model');
        $this->load->model('Gradebook_model');
        $this->load->library('form_validation');
        $this->load->helper(array('html', 'language', 'form', 'country_helper'));
        if (!$this->ion_auth->logged_in()) :
            redirect('users/auth', 'refresh');
        endif;

        /*if (!$this->ion_auth->is_admin()) :
            return show_error("You Must Be An Administrator To View This Page");
        endif;*/

        // get controller permissions       
        if ($this->current_user_permissions = $this->get_permissions()) {
            $this->add_permission = $this->current_user_permissions->add_per;
            $this->edit_permission = $this->current_user_permissions->edit_per;
            $this->delete_permission = $this->current_user_permissions->delete_per;
            $this->view_permission = $this->current_user_permissions->view_per;
            $this->list_permission = $this->current_user_permissions->list_per;
        }
        $this->container_id = get_container_id();

    }

    /**
     * [Load poll list]
     * @return [void]
     */
    public function index() {

        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Gradebook', 'link' => '', 'class' => 'active'];


        if (!$this->ion_auth->is_admin()) {
            return show_error('You Must Be an Administrator To view This Page');
        }

        //$data['Gradebook'] = $this->Gradebook_model->gradebook_list();
        $data['page'] = "Gradebook/Gradebook/view";
        $this->template->template_view($data);
    }

    function getLists() {
        // Fetch records
        $Data = $this->Gradebook_model->getRows($_POST);
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $Data['total'],
            "recordsFiltered" => $Data['total'],
            "data" => $Data['result'],
        );
        // Output to JSON format
        echo json_encode($output);
    }

    //create gradebook page
    public function save() {
        $this->benchmark->mark('code_start');
        if (!$this->ion_auth->is_admin()) {
            return show_error('You Must Be an Administrator To view This Page');
        }
        $data['category_list'] = $this->Gradebook_model->get_category_list(); //get category list for gradung with type=3
        $cat[""] = "Select Category";
        foreach ($data['category_list'] as $value) {
            $cat[$value['id']] = $value['name'];
        }
        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Gradebook', 'link' => base_url('gradebook/gradebook'), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Add', 'link' => '', 'class' => 'active'];
        $data['category_list'] = $cat;
        
        $data['page'] = 'Gradebook/Gradebook/create';
        $this->template->template_view($data);
    }

    /**
     * [Add New Poll]
     */
    public function add() {
        $this->form_validation->set_rules('name', 'Grader\'s Name', 'trim|required');
        $this->form_validation->set_rules('category_id', 'Grader\'s Category', 'trim|required');
        $this->form_validation->set_rules('template', 'Grader\'s Template', 'trim|required');
        $this->form_validation->set_rules('description', 'Grader\'s Description', 'trim|required');

        
        if ($this->form_validation->run() == false) {
            $msg = 'Grader\'s name/category/template/description should not be blank.';
            $this->session->set_flashdata('error', $msg);
        } else {
            $data = array(
                'created_by' => $this->session->userdata('user_id'),
                'category_id' => post('category_id'),
                'name' => post('name'),
                'template' => post('template'),
                'description' => post('description'),
                'status' => 1,
                'delete_status' => 1,
                'created_at' => date("Y-m-d H:i:s"),
            );
            $this->common_model->InsertData('gradebooks', $data); //insert data
            set_flashdata('success', 'Added Successfully');
        }
        redirect('Gradebook/Gradebook/index', 'refresh');
    }

    //Gradebook edit page
    public function edit() {

        $this->session->set_flashdata('message', $this->ion_auth->messages());
        $data['data'] = $this->Gradebook_model->edit($this->uri->segment(4));
        $data['id'] = $this->uri->segment(4);
        $data['Gradebook_options'] = $this->Gradebook_model->get_category_list($this->uri->segment(4));
        $cat[""] = "Select Category";
        foreach ($data['Gradebook_options'] as $value) {
            $cat[$value['id']] = $value['name'];
        }
        $data['Gradebook_options'] = $cat;
        $data['Gradebook_data'] = $this->Gradebook_model->edit($this->uri->segment(4));
        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Gradebook', 'link' => base_url('gradebook/gradebook'), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Edit', 'link' => '', 'class' => 'active'];
        $data['page'] = 'Gradebook/Gradebook/edit';
        $this->template->template_view($data);
    }

    // update Gradebook functionality
    public function update() {
        $this->form_validation->set_rules('category_id', 'Grader\'s Category', 'trim|required');
        $this->form_validation->set_rules('template', 'Grader\'s Template', 'trim|required');
        $this->form_validation->set_rules('description', 'Grader\'s Description', 'trim|required');

        //pr("h"); exit;
        if ($this->form_validation->run() == false) {
            $msg = 'Grader\'s category/template/description should not be blank.';
            $this->session->set_flashdata('error', $msg);
            redirect('Gradebook/Gradebook/edit/' . $this->input->post('id'));
        } else {

            $data = array(
                'created_by' => $this->session->userdata('user_id'),
                'category_id' => post('category_id'),
                'name' => post('name'),
                'template' => post('template'),
                'description' => post('description'),
                'status' => 1,
                'delete_status' => 1,
                'updated_at' => date("Y-m-d H:i:s"),
            );
            
            if ($this->Gradebook_model->update($this->input->post('id'), $data)) {
                $msg = "Updated successfully";
                $this->session->set_flashdata('success', $msg);
                redirect('Gradebook/Gradebook/', 'refresh');
            } else {
                $this->session->set_flashdata('error', 'Something went wrong');
                redirect('Gradebook/Gradebook/', 'refresh');
            }
        }
    }

    //delete Gradebook
    public function delete() {
        $this->session->set_flashdata('success', $this->ion_auth->messages());
        if ($this->input->post('status') == '') {
            $status = 0;
        } else {
            $status = 1;
        }
        $additional_data = array(
            'delete_status' => $status,
        );
        $this->Gradebook_model->update($this->uri->segment(4), $additional_data);
        echo json_encode(array("msg" => "deleted"));
        exit;
    }

    //update Gradebook status
       public function update_status($id, $action) {
        $status = ($action == 'activate') ? 1 : 0; 
        $data = array('status' => $status);
        $this->Gradebook_model->update_status($id, $data);
        $msg = "Updated";
        echo json_encode(['msg' => $msg]);
        exit;
    }

    //update Gradebook status
    public function sendgrid() {
        $this->load->library('email');

        $this->email->initialize(array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.sendgrid.net',
            'smtp_user' => 'akash2051',
            'smtp_pass' => 'akash@123',
            'smtp_port' => 587,
            'crlf' => "\r\n",
            'newline' => "\r\n"
        ));

        $this->email->from('your@example.com', 'Akash');
        $this->email->to('akash@datalogysoftware.com');
        $this->email->cc('akashhedaoo2051@gmail.com');
        $this->email->bcc('test.d701@gmail.com');
        $this->email->subject('Email Test');
        $this->email->message('Testing the email class.');
        $this->email->send();

        echo $this->email->print_debugger();
    }


    # ================================================= GRADEBOOK =============================================== #

    public function gradings() {

        $data['page'] = "gradebook/gradings";
        $container_id = $this->container_id; // Container ID
        

        $data['breadcrumb'][]   = [ 'title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]   = [ 'title' => 'Grading', 'link' => base_url('gradebook/gradebook'), 'class' => ''];
        $data['page']           = 'gradebook/gradings';

        $this->load->model('containers/Container_model');
        $data['courses']        = $this->Container_model->getAllActiveCourseBySiteID($container_id);
        $data['containers']        = $this->common_model->getAllData('containers', ['id', 'name' ], '', ['status' => 1, 'status_delete' => 1], 'id ASC');
        //pr($data['containers']);
        //$data['container_id']   =$this->container_id;
        
        # When submit course and section
        if($this->input->post() ) {

            # Get the student basic information
            $section_id = $this->input->post('sectionId');
            $course_id  = $this->input->post('courseId');           
            
            # Course select box
            $data['selectedCourseId'] = $course_id;

            # Section select box
            $sections = $this->Gradebook_model->getSectionsListCourse($course_id, $container_id); 
            $section_opts = '<option value="">-- Select --</option>';
            foreach ($sections as $key => $section) {
                $sectionSelected = "";
                if($section["id"]== $section_id)
                    $sectionSelected = "selected";
                $section_opts .= '<option value='.$section["id"].' '.$sectionSelected.' >'.$section["section_name"].'</option>';
            }
            $data['section_options'] = $section_opts; 

            # Fetching gradbook table header data
            $data['gradebookTableHeaderData'] = $this->common_model->getAllData('courses_gradebook', ['id', 'section_title', 'section_type','weight' ], '', ['course_id' => $course_id], 'section_type ASC');
            # CYK quizzes = 1;                    
            $cyk_gradebook = $this->db->query('SELECT DISTINCT `quizzes`.`id` , `quizzes`.`title`, `quizzes`.`type` FROM `module_contain` JOIN `lessons` ON `module_contain`.`lesson_id` = `lessons`.`id` JOIN `quizzes` ON `quizzes`.`id` = `lessons`.`quiz_id` WHERE `module_contain`.`course_id` = '.$course_id.' AND `quizzes`.`type` = 1 ORDER BY `quizzes`.`id` ASC');
            $data['gradebooks'][1] = $cyk_gradebook->result_array();
            
            # Module 
            $gradebook_data = $this->db->query('SELECT * FROM ( (SELECT `quizzes`.`id`, `quizzes`.`type` FROM `quizzes` JOIN `lesson_contain` ON `quizzes`.`id` = `lesson_contain`.`quiz_id` WHERE `lesson_contain`.`course_id` = '.$course_id. ' AND  `quizzes`.`type` != 1) UNION (SELECT `quizzes`.`id`, `quizzes`.`type` FROM `quizzes` JOIN `module_contain` ON `quizzes`.`id` = `module_contain`.`quiz_id` WHERE `module_contain`.`course_id` = '.$course_id.'  AND  `quizzes`.`type` != 1) UNION ( SELECT `quizzes`.`id`, `quizzes`.`type` FROM `quizzes` JOIN `course_contain` ON `quizzes`.`id` = `course_contain`.`quiz_id` WHERE `course_contain`.`course_id` = '.$course_id.'  AND  `quizzes`.`type` != 1) ) AS q ORDER BY `id` ');
            $gradebook_quiz_data = $gradebook_data->result_array();

            foreach ($gradebook_quiz_data as $gb_qz_key => $gb_qz_data) {
                if($gb_qz_data['type']==2)        // Module quizzes
                    $data['gradebooks'][2][] = $gb_qz_data;
                else if($gb_qz_data['type']==4)        // Mid term
                    $data['gradebooks'][4][] = $gb_qz_data;
                else if($gb_qz_data['type']==5)        // Final term
                    $data['gradebooks'][5][] = $gb_qz_data;
            }
            
            # Assessment data
            $gradebook_ass_data = $this->db->query('SELECT * FROM ( ( SELECT `assessments`.`id`, `assessments`.`title` FROM `assessments` JOIN `lesson_contain` ON `assessments`.`id` = `lesson_contain`.`quiz_id` WHERE `lesson_contain`.`course_id` = '.$course_id.' AND `lesson_contain`.`contain_type` = 1 ) UNION ( SELECT `assessments`.`id`, `assessments`.`title` FROM `assessments` JOIN `module_contain` ON `assessments`.`id` = `module_contain`.`quiz_id` WHERE `module_contain`.`course_id` = '.$course_id.' AND `module_contain`.`contain_type` = 1) UNION ( SELECT `assessments`.`id`, `assessments`.`title` FROM `assessments` JOIN `course_contain` ON `assessments`.`id` = `course_contain`.`quiz_id` WHERE `course_contain`.`course_id` = '.$course_id.' AND `course_contain`.`contain_type` = 1) ) AS ass ORDER BY `id` ');

            $gradebook_assmnts_data = $gradebook_ass_data->result_array();
            $data['gradebooks'][3] = $gradebook_assmnts_data;

            # Get student records 
            $this->db->select('course_assigned_to_users.user_id');
            $this->db->from('course_assigned_to_users');
            $this->db->where(['course_assigned_to_users.section_id' => $section_id,'course_assigned_to_users.course_id' => $course_id ]);
            $user_ids = [];
            $query = $this->db->get();
            foreach ($query->result_array() as $key => $value) {
                $user_ids[] = $value['user_id'];
            }
            $studentList = $this->Gradebook_model->getStudentRecords($user_ids);

            $quiz_type = [ 1, 2, 3, 4, 5 ];
            # Get the student recored with grades
            $studentSectionAvgs = [];
            foreach ($studentList as $skey => $student) {
                foreach ($quiz_type as $key => $qtype) {
                
                    $studentList[$skey][$qtype] = $data['gradebooks'][$qtype];    // MOdule

                    foreach ($studentList[$skey][$qtype] as $gbkey => $gradebookData) {

                        $studentGrades = $this->Gradebook_model->getStudentGrade($gradebookData['id'], $student['id'], $course_id, $section_id, $qtype);
                        $studentList[$skey][$qtype][$gbkey]['grade'] = $studentGrades->grade;

                        $sid = $student['id'];
                        if(count($studentGrades) == 1)
                            $studentSectionAvgs[$sid][$qtype]['current'][] = $studentGrades->grade;

                        $studentSectionAvgs[$sid][$qtype]['final'][] = $studentGrades->grade;


                    } 
                }               
            }            
            $data['studentList'] = $studentList;          


            # FINAL GRADE & CURRENT GRADE 
            foreach ($studentSectionAvgs as $ssAkey => $ssAvalue) {

                # CYK
                $cyk_section_weight = $data['gradebookTableHeaderData'][0];
                if(count($ssAvalue[1]['final']) > 0) {
                    $cyk_final_grade = ( ( array_sum($ssAvalue[1]['final']) / count($ssAvalue[1]['final']) ) * $cyk_section_weight->weight )/100;
                }else 
                    $cyk_final_grade = 0;            
                $studentSectionAvgs[$ssAkey]['final_grade'][1] = $cyk_final_grade;

                if(count($ssAvalue[1]['current']) > 0) {
                    $cyk_current_grade = ( ( array_sum($ssAvalue[1]['current']) / count($ssAvalue[1]['current']) ) * $cyk_section_weight->weight )/100;
                }else 
                    $cyk_current_grade = 0;            
                $studentSectionAvgs[$ssAkey]['current_grade'][1] = $cyk_current_grade;


                # Module
                $module_section_weight = $data['gradebookTableHeaderData'][1];
                if(count($ssAvalue[2]['final']) > 0) {
                    $module_final_grade = ( ( array_sum($ssAvalue[2]['final']) / count($ssAvalue[2]['final']) ) * $module_section_weight->weight ) / 100;
                }else 
                    $module_final_grade = 0;            
                $studentSectionAvgs[$ssAkey]['final_grade'][2] = $module_final_grade;

                if(count($ssAvalue[2]['current']) > 0) {
                    $module_current_grade = ( ( array_sum($ssAvalue[2]['current']) / count($ssAvalue[2]['current']) ) * $module_section_weight->weight ) / 100;
                }else 
                    $module_current_grade = 0;            
                $studentSectionAvgs[$ssAkey]['current_grade'][2] = $module_current_grade;


                # Evidence
                $evidence_section_weight = $data['gradebookTableHeaderData'][2];
                if(count($ssAvalue[3]['final']) > 0) {
                    $evidence_final_grade = ( ( array_sum($ssAvalue[3]['final']) / count($ssAvalue[3]['final']) ) * $evidence_section_weight->weight ) / 100;
                }else 
                    $evidence_final_grade = 0;            
                $studentSectionAvgs[$ssAkey]['final_grade'][3] = $evidence_final_grade;

                $evidence_section_weight = $data['gradebookTableHeaderData'][2];
                if(count($ssAvalue[3]['current']) > 0) {
                    $evidence_current_grade = ( ( array_sum($ssAvalue[3]['current']) / count($ssAvalue[3]['current']) ) * $evidence_section_weight->weight ) / 100;
                }else 
                    $evidence_current_grade = 0;            
                $studentSectionAvgs[$ssAkey]['current_grade'][3] = $evidence_current_grade;


                # Mid Term
                $midterm_section_weight = $data['gradebookTableHeaderData'][3];
                if(count($ssAvalue[4]['final']) > 0) {
                    $midterm_final_grade = ( ( array_sum($ssAvalue[4]['final']) / count($ssAvalue[4]['final']) ) * $midterm_section_weight->weight ) / 100;
                }else 
                    $midterm_final_grade = 0;            
                $studentSectionAvgs[$ssAkey]['final_grade'][4] = $midterm_final_grade;

                $midterm_section_weight = $data['gradebookTableHeaderData'][3];
                if(count($ssAvalue[4]['current']) > 0) {
                    $midterm_current_grade = ( ( array_sum($ssAvalue[4]['current']) / count($ssAvalue[4]['current']) ) * $midterm_section_weight->weight ) / 100;
                }else 
                    $midterm_current_grade = 0;            
                $studentSectionAvgs[$ssAkey]['current_grade'][4] = $midterm_current_grade;


                # Final
                $final_section_weight = $data['gradebookTableHeaderData'][4];
                if(count($ssAvalue[5]['final']) > 0) {
                    $final_grade = ( ( array_sum($ssAvalue[5]['final']) / count($ssAvalue[5]['final']) ) * $final_section_weight->weight ) / 100;
                }else 
                    $final_grade = 0;            
                $studentSectionAvgs[$ssAkey]['final_grade'][5] = $final_grade;


                $final_section_weight = $data['gradebookTableHeaderData'][4];
                if(count($ssAvalue[5]['current']) > 0) {
                    $current_grade = ( ( array_sum($ssAvalue[5]['current']) / count($ssAvalue[5]['current']) ) * $final_section_weight->weight ) / 100;
                }else 
                    $current_grade = 0;            
                $studentSectionAvgs[$ssAkey]['current_grade'][5] = $final_current_grade;

            }
            //pr($studentSectionAvgs);  exit;
            $data['studentSectionAvgs'] = $studentSectionAvgs;
        } // post end
        $this->template->template_view($data);

    }

    # Displaying select box of course sections 
    public function getSectionList() {
        if($this->input->post() ) {
            $container_id = $this->input->post('container_id');
            $course_id = $this->input->post('course_id');
            if(empty($container_id)) {
                $container_id = $this->container_id;
            }
            $sections = $this->Gradebook_model->getSectionsListCourse($course_id, $container_id); 
            //pr($sections); exit;

            $section_opts = '<option value="">-- Select --</option>';
            foreach ($sections as $key => $section) {
                $section_opts .= '<option value='.$section["id"].'>'.$section["section_name"].'</option>';
            }
            echo $section_opts;
        }
    }

    # Displaying select box of course sections 
    public function getCourseList() {
        if($this->input->post() ) {
            $container_id = $this->input->post('container_id');
            $courses = $this->Gradebook_model->getCourseList($container_id); 
            //pr($courses); exit;

            $course_opts = '<option value="">-- Select --</option>';
            foreach ($courses as $key => $course) {
                $course_opts .= '<option value="'.$course['id'].'">'.$course['name'].'</option>';
            }
            echo $course_opts;
        }
    }

    # Function 

    public function getStudentsList() {
        $studentList = [];
        if($this->input->post('sectionId') ) {
            $this->db->select('course_assigned_to_users.user_id');
            $this->db->from('course_assigned_to_users');
            $this->db->where(['course_assigned_to_users.section_id' => $this->input->post('sectionId'),'course_assigned_to_users.course_id' => $this->input->post('courseId'), ]);
            $user_id = [];
            $query = $this->db->get();
            foreach ($query->result_array() as $key => $value) {
                $user_id[] = $value['user_id'];
            }

            $studentList = $this->Gradebook_model->getStudentRecords($user_id);
        }


        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => count($studentList),
            "recordsFiltered" => count($studentList),
            "data" => $studentList,
        );
        // Output to JSON format
        echo json_encode($output);
        exit;
    }


    /* API Function of gradebook*/

    

}

/* End of file Posts.php */
/* Location: ./application/modules/blog/controllers/Posts.php */
