<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            <ol class="breadcrumb">
                <li class=""><a href="">Home</a></li>
                <li class=""><a href="">Gradebooks</a></li>
                <li class="active"><a href="">Course assign</a></li>
            </ol>
            
            <div class="container-fluid">
                <div class="panel panel-info" data-widget='{"draggable": "false"}'>
                    <div class="panel-heading">
                        <h2><i class="fa fa-user"></i>Course assign</h2>
                        <div class="panel-ctrls" data-actions-container="" data-action-collapse='{"target": ".panel-body"}'></div>
                    </div>
                    <div class="panel-body">
                        <?= form_open('Gradebook/Course_assign/add', array('id' => 'user_form_validation', 'class' => 'form-horizontal')); ?>
                        <div class="form-group col-md-12">
                            
                            <div class="col-md-6">
                                <label for="fieldname" class="col-md-12 control-label">Institution</label>
                                <?php 
                                $extra = array(
                                    'id'          => 'container_id',
                                    'value'       => set_value('container_id'),
                                    'class'       => 'form-control'
                                );
                                echo form_dropdown('container_id', $container_list, 0, $extra); ?>
                                <?php echo form_error('container_id', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="col-md-6">
                                <label for="fieldname" class="col-md-12 control-label">Courses</label>
                                <?php 
                                $extra = array(
                                    'id'          => 'course_id',
                                    'multiple'    => 'multiple',
                                    'value'       => set_value('course_id'),
                                    'class'       => 'form-control'
                                );
                                echo form_dropdown('course_id[]', $course_list, 0, $extra); ?>
                                <?php echo form_error('template', '<div class="error">', '</div>'); ?>
                            </div>
                            
                        </div>
                        
                        
                        
                        <div class="form-group">
                            
                            <div class="col-md-6">
                                <input type="submit" class="finish btn-success btn" value="Add">
                            </div>
                            </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .container-fluid -->
</div>
<!-- #page-content -->
</div>
<script type='text/javascript' src="<?php echo base_url(); ?>public\assets\js\Gradebook\Course_assign_view.js"></script>
<?php
$jsVars = [
    'ajax_call_root' => base_url()
];    
?>
<script>
    jQuery(document).ready(function() { 
        Course_assign_view.init(<?php echo json_encode($jsVars); ?>);
    });
</script>
