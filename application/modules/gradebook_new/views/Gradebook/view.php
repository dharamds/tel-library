
            <div class="container-fluid">

                <br>
                <div data-widget-group="group1">
                    <div class="row">
                        <div class="col-md-12">
                            <a class="btn btn-primary" href="<?php echo base_url('gradebook/gradebook/save') ?>">Add New Gradebook</a> 
                        </div>  
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h2>Gradebook's Records</h2>
                                    <div class="panel-ctrls"></div>
                                    <a href="<?php echo bs('users/print_with_dompdf') ?>">
                                        <i class="fa fa-print" style="padding-left: 1%;color: black"></i>
                                    </a>	
                                </div>
                                <div class="panel-body no-padding">


                                    <table id="memListTable" class="table table-bordered table-striped table-hover" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>

                                                <th>Sr.</th>
                                                <th>Grader's Category</th>
                                                <th>Grader's Template</th>
                                              
                                                    <th>Status</th>
                                                    <th>Action</th>
                                             
                                            </tr>
                                        </thead>
                                        
                                    </table>
                                </div>
                                <div class="panel-footer"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- .container-fluid -->
        
<script type="text/javascript">
    $(document).ready(function () {
        var webroot = '<?php echo base_url(); ?>';
        var table = table = table = $('#memListTable').DataTable({
            // Processing indicator
            "processing": true,
            // DataTables server-side processing mode
            "serverSide": true,
            // Initial no order.
            "iDisplayLength": 10,
            "bPaginate": true,
            "order": [],
            // Load data from an Ajax source
            "ajax": {
                "url": webroot + "Gradebook/Gradebook/getLists",
                "type": "POST",
                "data": function (data) {
                    //alert("h");
                },
            },
            //Set column definition initialisation properties
            "columnDefs": [{
                    "targets": 3,
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                            if (full['status_button'] == '1') {
                                data = '<a href="<?php bs('Gradebook/Gradebook/update_status/') ?>' + full['id'] + '/deactivate" data-toggle="tooltip" data-placement="top" title="Click to Change Status" class="btn btn-midnightblue-alt btn-sm change_status">Active</a>';
                            } else {
                                data = '<a href="<?php bs('Gradebook/Gradebook/update_status/') ?>' + full['id'] + '/activate" data-toggle="tooltip" data-placement="top" title="Click to Change Status" class="btn btn-midnightblue-alt btn-sm change_status">Inactive</a>';
                            }
                        }
                        return data;

                    }

                },
                {
                    "targets": 4,
                    "data": null,
                    "width": "20%",
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                            data = "<a class='btn btn-midnightblue-alt btn-sm' href='" + webroot + "Gradebook/Gradebook/edit/" + full['id'] + "'><i class='ti ti-pencil'></i></a>";
                            data += '<a href="' + webroot + 'Gradebook/Gradebook/delete/' + full['id'] + ' " class="btn btn-midnightblue-alt btn-sm delete_item" ><i class="ti ti-close"></i></a>';
                            data += '<a href="#" class="btn btn-midnightblue-alt btn-sm" ><i class="ti ti-eye"></i></a>';
                        }
                        return data;

                    }

                }
            ],
            "columns": [
                {
                    "data": "sr_no",
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },

                    "autoWidth": true
                },
                {
                    "data": "cat_name",
                    "autoWidth": true
                },
                {
                    "data": "template",
                    "autoWidth": true
                },
                {
                    "data": "created",
                    "autoWidth": true
                }
            ]
        });


        $(document).on('click', '.change_status', function (e) {
            e.preventDefault();
             var scope = $(this);
           // $.get($(this).attr("href"), // url
            $.get(scope.attr("href"), // url
                    function (data, textStatus, jqXHR) { // success callback
                        var obj = JSON.parse(data);
                        if (obj.msg === 'Updated') {
                            table.ajax.reload();  //just reload table
                        }
                    });
        });



        $(document).on('click', '.delete_item', function (e) {
            e.preventDefault();
            var scope = $(this);
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure?',
                buttons: {
                    confirm: function () {
                        $.get(scope.attr("href"), // url
                                function (data, textStatus, jqXHR) { // success callback
                                    var obj = JSON.parse(data);
                                    if (obj.msg === 'deleted') {
                                        table.ajax.reload();  //just reload table
                                    }
                                });
                        return true;
                    },
                    cancel: function () {
                        return true;
                    }
                }
            });
        });




    });
</script>