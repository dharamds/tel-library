<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
  Author Akash Hedaoo
  Date 16/04/2019
 */

class Gradebook_model extends CI_Model {

    function __construct() {
       
        // Set table name
        $this->table = 'gradebooks';
        // Set orderable column fields
        $this->column_order = array(null, 'cat_name', 'template', 'status');
        // Set searchable column fields
        $this->column_search = array('template', 'categories.name');
        // Set default order
        $this->order = array('gradebooks.created_at' => 'desc');
       
    }

    /*
     * Fetch data from the database
     * @param $_POST filter data based on the posted parameters
     */

    public function gradebook_list() {
        $this->db->where('delete_status', 1);
        $query = $this->db->get('gradebooks');
        return $query->result();
    }

    /*
     * Fetch category data from the database
     * @param $_POST filter data based on the posted parameters
     */

    public function get_category_list() {
        $array = array('type' => 1, 'status' => 1);
        $this->db->select('id,name');
        $this->db->where($array);
        $query = $this->db->get('categories');
        return $query->result_array();
    }

    /*
     * Fetch data from the database
     * @param $id filter data based on the particular id from url
     */

    public function edit($id = null) {
        $this->db->where('id', $id);
        $query = $this->db->get('gradebooks');
        return $query->row();
    }

    /*
     * Update data into the database
     * @param $id filter data based on the particular id from url
     */

    public function update($id = null, $data) {
        $this->db->where('id', $id);
        $update = $this->db->update('gradebooks', $data);
        if ($update):
            return true;
        endif;
    }

    /*
     * update delete_status for active inactive skills from the database
     * @param $id filter data based on the particular id from url
     */

    public function update_status($id = null, $data) {
        $this->db->where('id', $id);
        $delete = $this->db->update('gradebooks', $data);
        if ($delete):
            return true;
        endif;
    }

    /*
     * delete option for active inactive skills from the database
     * @param $id filter data based on the particular id from url
     */

    public function delete_option($id = null) {
        $this->db->where('id', $id);
        $delete = $this->db->delete('polls_options');
        //pr($this->db->last_query()); exit;
        if ($delete):
            return true;
        endif;
    }

    /*
     * Fetch data from the database
     * @param $_POST filter data based on the posted parameters
     */

    public function getRows($postData) {
        $this->_get_datatables_query($postData);
        if ($postData['length'] != -1) {
            $this->db->limit($postData['length'], $postData['start']);
        }
        $query = $this->db->get();

        $data['result'] = $query->result();
        $data['total'] = $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total;
        return $data;
    }

    /*
     * Perform the SQL queries needed for an server-side processing requested
     * @param $_POST filter data based on the posted parameters
     */

    private function _get_datatables_query($postData) {
        $this->db->select('SQL_CALC_FOUND_ROWS ' . 'gradebooks.id,gradebooks.template,gradebooks.status as status_button,categories.name as cat_name', false);
        $this->db->select('DATE_FORMAT(' . $this->table . '.created_at, "%m/%d/%Y") as created');
        //$this->db->select("IF(".$this->table.".status = 1, 'Publish', 'Unpublish') as status");
        //$this->db->select("IF(".$this->table.".type = 1, 'System', 'Default') as type");
        //$this->db->select("CONCAT_WS(' ', users.first_name, users.middle_name, users.last_name) as user_name");
        $this->db->from($this->table);
        $this->db->join('categories', 'gradebooks.category_id = categories.id');
        //$this->db->join('users', $this->table.'.created_by = users.id');
        $this->db->where($this->table . '.delete_status = 1');


        // $fields = 'gradebooks.*,categories.name as cat_name';
        // $tbl = 'gradebooks';
        // $jointbl1 = 'categories';
        // $Joinone = 'gradebooks.category_id = categories.id';
        // $array = array('gradebooks.delete_status' => 1);
        // $this->common_model->DJoin($fields, $tbl, $jointbl1, $Joinone,"","", $array);
        $i = 0;
        // loop searchable columns 
        foreach ($this->column_search as $item) {
            // if datatable send POST for search
            if ($postData['search']['value']) {
                // first loop
                if ($i === 0) {
                    // open bracket
                    $this->db->like($item, $postData['search']['value']);
                } else {
                    $this->db->or_like($item, $postData['search']['value']);
                }
            }
            $i++;
        }
        if (isset($postData['order'])) {
            $this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
    }

    //Count gradebook
    public function count_gradebook() {
        $this->db->select('*');
        $this->db->from('gradebooks');
        return $this->db->count_all_results();
    }

    /*
     * Count records based on the filter params
     * @param $_POST filter data based on the posted parameters
     */

    public function countFiltered($postData) {
        $this->_get_datatables_query($postData);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function getSectionsListCourse($courseID, $containerID)
    {
        $this->db->select('course_sections.name as section_name,course_sections.start_date,course_sections.end_date,course_sections.id,courses.name,courses.id as course_id');
        $this->db->from('course_sections');
        $this->db->join('container_courses', 'course_sections.course_id = container_courses.course_id', 'inner');
        $this->db->join('courses', 'container_courses.course_id = courses.id', 'inner');
        $this->db->where(['course_sections.institute_id' => $containerID, 'course_sections.course_id' => $courseID]);
        $this->db->where(['container_courses.container_id' => $containerID, 'container_courses.course_id' => $courseID]);
        //$this->db->group_by('courses.id');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCourseList($containerID)
    {
        $this->db->select('courses.id,courses.name');
        $this->db->from('container_courses');
        $this->db->join('courses', 'courses.id = container_courses.course_id', 'inner');
        $this->db->where(['container_courses.container_id' => $containerID]);
        //$this->db->group_by('courses.id');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getStudentRecords($userIds) {
    
        $this->db->select("id, CONCAT_WS(' ', users.first_name, users.last_name) as student, unique_id");
        $this->db->from('users');
        $this->db->where_in('id', $userIds);
        $query = $this->db->get();
        return $query->result_array();
    }

    # Get student grade
    public function getStudentGrade($quizId, $studId, $courseId, $sectionId, $type) {
        $this->db->select('quiz_assess_id, quiz_assess_type, grade, user_id');
        $this->db->from('gradebook_quiz_assess_grade');
        $this->db->where(['quiz_assess_id' => $quizId, 'user_id' => $studId, 'course_id' => $courseId, 'section_id' => $sectionId, 'quiz_assess_type' => $type ]);
        $this->db->order_by('quiz_assess_id ASC');
        $query = $this->db->get();
        return $query->row();
    }


    public function studentGradebook($course_id=12, $section_id=21, $student_id=82) {
       

        # Fetching gradbook table header data
        $data['gradebookTableHeaderData'] = $this->Common_model->getAllData('courses_gradebook', ['id', 'section_title', 'section_type','weight' ], '', ['course_id' => $course_id], 'section_type ASC');
       
        # CYK quizzes = 1;                    
        $cyk_gradebook = $this->db->query('SELECT DISTINCT `quizzes`.`id` , `quizzes`.`title`, `quizzes`.`type` FROM `module_contain` JOIN `lessons` ON `module_contain`.`lesson_id` = `lessons`.`id` JOIN `quizzes` ON `quizzes`.`id` = `lessons`.`quiz_id` WHERE `module_contain`.`course_id` = '.$course_id.' AND `quizzes`.`type` = 1 ORDER BY `quizzes`.`id` ASC');
        $data['gradebooks'][1] = $cyk_gradebook->result_array();
        
        # Module 
        $gradebook_data = $this->db->query('SELECT * FROM ( (SELECT `quizzes`.`id`, `quizzes`.`type`, `quizzes`.`title` FROM `quizzes` JOIN `lesson_contain` ON `quizzes`.`id` = `lesson_contain`.`quiz_id` WHERE `lesson_contain`.`course_id` = '.$course_id. ' AND  `quizzes`.`type` != 1) UNION (SELECT `quizzes`.`id`, `quizzes`.`type`, `quizzes`.`title` FROM `quizzes` JOIN `module_contain` ON `quizzes`.`id` = `module_contain`.`quiz_id` WHERE `module_contain`.`course_id` = '.$course_id.'  AND  `quizzes`.`type` != 1) UNION ( SELECT `quizzes`.`id`, `quizzes`.`type`, `quizzes`.`title` FROM `quizzes` JOIN `course_contain` ON `quizzes`.`id` = `course_contain`.`quiz_id` WHERE `course_contain`.`course_id` = '.$course_id.'  AND  `quizzes`.`type` != 1) ) AS q ORDER BY `id` ');
        $gradebook_quiz_data = $gradebook_data->result_array();

        foreach ($gradebook_quiz_data as $gb_qz_key => $gb_qz_data) {
            if($gb_qz_data['type']==2)        // Module quizzes
                $data['gradebooks'][2][] = $gb_qz_data;
            else if($gb_qz_data['type']==4)        // Mid term
                $data['gradebooks'][4][] = $gb_qz_data;
            else if($gb_qz_data['type']==5)        // Final term
                $data['gradebooks'][5][] = $gb_qz_data;
        }
        
        # Assessment data
        $gradebook_ass_data = $this->db->query('SELECT * FROM ( ( SELECT `assessments`.`id`, `assessments`.`title` FROM `assessments` JOIN `lesson_contain` ON `assessments`.`id` = `lesson_contain`.`quiz_id` WHERE `lesson_contain`.`course_id` = '.$course_id.' AND `lesson_contain`.`contain_type` = 1 ) UNION ( SELECT `assessments`.`id`, `assessments`.`title` FROM `assessments` JOIN `module_contain` ON `assessments`.`id` = `module_contain`.`quiz_id` WHERE `module_contain`.`course_id` = '.$course_id.' AND `module_contain`.`contain_type` = 1) UNION ( SELECT `assessments`.`id`, `assessments`.`title` FROM `assessments` JOIN `course_contain` ON `assessments`.`id` = `course_contain`.`quiz_id` WHERE `course_contain`.`course_id` = '.$course_id.' AND `course_contain`.`contain_type` = 1) ) AS ass ORDER BY `id` ');

        $gradebook_assmnts_data = $gradebook_ass_data->result_array();
        $data['gradebooks'][3] = $gradebook_assmnts_data;


        # Get student records 
        $this->db->select('course_assigned_to_users.user_id');
        $this->db->from('course_assigned_to_users');
        $this->db->where(['course_assigned_to_users.section_id' => $section_id,'course_assigned_to_users.course_id' => $course_id, 'course_assigned_to_users.user_id' => $student_id ]);
        $user_ids = [];
        $query = $this->db->get();
        foreach ($query->result_array() as $key => $value) {
            $user_ids[] = $value['user_id'];
        }
        $studentList = $this->Gradebook_model->getStudentRecords($user_ids);

        $quiz_type = [ 1, 2, 3, 4, 5 ];
        # Get the student recored with grades
        $studentSectionAvgs = [];
        foreach ($studentList as $skey => $student) {
            foreach ($quiz_type as $key => $qtype) {
            
                $studentList[$skey][$qtype] = $data['gradebooks'][$qtype];    // MOdule

                foreach ($studentList[$skey][$qtype] as $gbkey => $gradebookData) {

                    $studentGrades = $this->Gradebook_model->getStudentGrade($gradebookData['id'], $student['id'], $course_id, $section_id, $qtype);
                    $studentList[$skey][$qtype][$gbkey]['grade'] = $studentGrades->grade;

                    $sid = $student['id'];
                    if(count($studentGrades) == 1)
                        $studentSectionAvgs[0][$qtype]['current'][] = $studentGrades->grade;

                    $studentSectionAvgs[0][$qtype]['final'][] = $studentGrades->grade;


                } 
            }               
        }            
        $data['studentList'] = $studentList;
        //pr($studentList); exit;

        # FINAL GRADE & CURRENT GRADE 
        foreach ($studentSectionAvgs as $ssAkey => $ssAvalue) {

            # CYK
            $cyk_section_weight = $data['gradebookTableHeaderData'][0];
            if(count($ssAvalue[1]['final']) > 0) {
                $cyk_final_grade = ( ( array_sum($ssAvalue[1]['final']) / count($ssAvalue[1]['final']) ) * $cyk_section_weight->weight )/100;
            }else 
                $cyk_final_grade = 0;            
            $studentSectionAvgs[$ssAkey]['final_grade'][1] = $cyk_final_grade;

            if(count($ssAvalue[1]['current']) > 0) {
                $cyk_current_grade = ( ( array_sum($ssAvalue[1]['current']) / count($ssAvalue[1]['current']) ) * $cyk_section_weight->weight )/100;
            }else 
                $cyk_current_grade = 0;            
            $studentSectionAvgs[$ssAkey]['current_grade'][1] = $cyk_current_grade;


            # Module
            $module_section_weight = $data['gradebookTableHeaderData'][1];
            if(count($ssAvalue[2]['final']) > 0) {
                $module_final_grade = ( ( array_sum($ssAvalue[2]['final']) / count($ssAvalue[2]['final']) ) * $module_section_weight->weight ) / 100;
            }else 
                $module_final_grade = 0;            
            $studentSectionAvgs[$ssAkey]['final_grade'][2] = $module_final_grade;

            if(count($ssAvalue[2]['current']) > 0) {
                $module_current_grade = ( ( array_sum($ssAvalue[2]['current']) / count($ssAvalue[2]['current']) ) * $module_section_weight->weight ) / 100;
            }else 
                $module_current_grade = 0;            
            $studentSectionAvgs[$ssAkey]['current_grade'][2] = $module_current_grade;


            # Evidence
            $evidence_section_weight = $data['gradebookTableHeaderData'][2];
            if(count($ssAvalue[3]['final']) > 0) {
                $evidence_final_grade = ( ( array_sum($ssAvalue[3]['final']) / count($ssAvalue[3]['final']) ) * $evidence_section_weight->weight ) / 100;
            }else 
                $evidence_final_grade = 0;            
            $studentSectionAvgs[$ssAkey]['final_grade'][3] = $evidence_final_grade;

            $evidence_section_weight = $data['gradebookTableHeaderData'][2];
            if(count($ssAvalue[3]['current']) > 0) {
                $evidence_current_grade = ( ( array_sum($ssAvalue[3]['current']) / count($ssAvalue[3]['current']) ) * $evidence_section_weight->weight ) / 100;
            }else 
                $evidence_current_grade = 0;            
            $studentSectionAvgs[$ssAkey]['current_grade'][3] = $evidence_current_grade;


            # Mid Term
            $midterm_section_weight = $data['gradebookTableHeaderData'][3];
            if(count($ssAvalue[4]['final']) > 0) {
                $midterm_final_grade = ( ( array_sum($ssAvalue[4]['final']) / count($ssAvalue[4]['final']) ) * $midterm_section_weight->weight ) / 100;
            }else 
                $midterm_final_grade = 0;            
            $studentSectionAvgs[$ssAkey]['final_grade'][4] = $midterm_final_grade;

            $midterm_section_weight = $data['gradebookTableHeaderData'][3];
            if(count($ssAvalue[4]['current']) > 0) {
                $midterm_current_grade = ( ( array_sum($ssAvalue[4]['current']) / count($ssAvalue[4]['current']) ) * $midterm_section_weight->weight ) / 100;
            }else 
                $midterm_current_grade = 0;            
            $studentSectionAvgs[$ssAkey]['current_grade'][4] = $midterm_current_grade;


            # Final
            $final_section_weight = $data['gradebookTableHeaderData'][4];
            if(count($ssAvalue[5]['final']) > 0) {
                $final_grade = ( ( array_sum($ssAvalue[5]['final']) / count($ssAvalue[5]['final']) ) * $final_section_weight->weight ) / 100;
            }else 
                $final_grade = 0;            
            $studentSectionAvgs[$ssAkey]['final_grade'][5] = $final_grade;


            $final_section_weight = $data['gradebookTableHeaderData'][4];
            if(count($ssAvalue[5]['current']) > 0) {
                $current_grade = ( ( array_sum($ssAvalue[5]['current']) / count($ssAvalue[5]['current']) ) * $final_section_weight->weight ) / 100;
            }else 
                $current_grade = 0;            
            $studentSectionAvgs[$ssAkey]['current_grade'][5] = $final_current_grade;

        }
        //pr($studentSectionAvgs);  exit;
        $data['studentSectionAvgs'] = $studentSectionAvgs;
        //pr($data); exit;
        return $data;
    }



}

/* End of file Users_modal.php */
