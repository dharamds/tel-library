<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Questions_modal extends CI_Model
{

    function __construct()
    {
        ini_set('max_execution_time', 1200);
        // Set table name
        $this->table = 'questions';
        // Set orderable column fields
        $this->column_order = array(null, null, 'questions.status', 'questions.title', 'questions.long_title', 'assesment_category', 'assessment_tags', 'system_category', 'system_tags', 'outcomes_category', 'standards_category', 'skill_mastery_point', 'question_quizzes', 'created_by', 'created');
        // Set searchable column fields
        $this->column_search = array('questions.title', 'questions.long_title');
        // Set default order
        $this->order = array('questions.id' => 'desc');
    }

    /*
     * Fetch members data from the database
     * @param $_POST filter data based on the posted parameters
     */
    public function getRows($postData)
    {
        //pr($postData);
        $this->_get_datatables_query($postData);

        if ($postData['length'] != -1) {
            $this->db->limit($postData['length'], $postData['start']);
        }
        $query = $this->db->get();
        $data['result'] = $query->result_array();
        //echo $this->db->last_query(); exit;
        $data['total'] = $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total;
        //pr($data);
        return $data;
    }

    //update status
    public function update_status($id = null, $data)
    {
        $this->db->where('id', $id);
        $delete = $this->db->update($this->table, $data);
        if ($delete) :
            return true;
        endif;
    }

    //update status
    public function get_question_used($question_id = null)
    {
        $sql = "SELECT COALESCE(GROUP_CONCAT(DISTINCT quizzes.title ORDER BY quizzes.title ASC SEPARATOR '<br>'), 'NA')  as name from quiz_section_questions 
        JOIN quizzes ON quizzes.id = quiz_section_questions.quiz_id
        WHERE quiz_section_questions.question_id = " . $question_id . ' group by quiz_section_questions.question_id';

        //return $sql;
        $query =  $this->db->query($sql);
        return ($query->row()->name) ? $query->row()->name : 'NA';
    }



    /*
     * Perform the SQL queries needed for an server-side processing requested
     * @param $_POST filter data based on the posted parameters
     */
    private function _get_datatables_query($postData, $existIDS = [])
    {

        $this->db->select("SQL_CALC_FOUND_ROWS questions.id,questions.title,questions.long_title,questions.skill_mastery_point,questions.status", false);
        $this->db->select('DATE_FORMAT(' . $this->table . '.created, "%m/%d/%Y") as created');
        if ($postData['order']['0']['column'] == 13) 
        {
            $this->db->select('CONCAT_WS(" ", users.first_name, users.middle_name, users.last_name) as created_by');
        }else{
            $this->db->select('"..." as created_by');
        }
        # Quizzes
        if ((isset($postData['assessment_category']) && count($postData['assessment_category']) > 0) || $postData['order']['0']['column'] == 5) {
            $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT categories.name ORDER BY categories.name ASC SEPARATOR "<br>"), \'NA\')  as assesment_category');
        } else {
            $this->db->select('"..." as assesment_category');
        }

        if ((isset($postData['system_cat']) && count($postData['system_cat']) > 0) || $postData['order']['0']['column'] == 7) {
            $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT system_category.name ORDER BY system_category.name ASC  SEPARATOR "<br>"), \'NA\')  as system_category');
        } else {
            $this->db->select('"..." as system_category');
        }

        if ((isset($postData['learning_outcomes']) && count($postData['learning_outcomes']) > 0) || $postData['order']['0']['column'] == 9) {
            $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT outcomes_category.name ORDER BY outcomes_category.name ASC  SEPARATOR "<br>"), \'NA\')  as outcomes_category');
        } else {
            $this->db->select('"..." as outcomes_category');
        }

        if ((isset($postData['learning_standards']) && count($postData['learning_standards']) > 0) || $postData['order']['0']['column'] == 10) {
            $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT standards_category.name ORDER BY standards_category.name ASC  SEPARATOR "<br>"), \'NA\')  as standards_category');
        } else {
            $this->db->select('"..." as standards_category');
        }


        // echo $postData['order']['0']['column']; exit;
        /*Tags*/
        if ((isset($postData['assessment_tag']) && count($postData['assessment_tag']) > 0) || $postData['order']['0']['column'] == 6) {
            $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT assessment_tags.name ORDER BY assessment_tags.name ASC  SEPARATOR "<br>") , \'NA\') as assessment_tags');
        } else {
            $this->db->select('"..." as assessment_tags');
        }
        //System Tags   
        if ((isset($postData['system_tag']) && count($postData['system_tag']) > 0) || $postData['order']['0']['column'] == 8) {
            $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT system_tags.name ORDER BY system_tags.name ASC  SEPARATOR "<br>"), \'NA\')  as system_tags');
        } else {
            $this->db->select('"..." as system_tags');
        }
        /*End Tags*/
        $this->db->from('questions');
        if ($postData['order']['0']['column'] == 13) 
        {
        $this->db->join('users', $this->table . '.created_by = users.id');
        }  

        # Quizzes
        // $this->db->join('quiz_section_questions', 'quiz_section_questions.question_id = questions.id', 'left');
        // $this->db->join('quizzes', 'quizzes.id = quiz_section_questions.quiz_id', 'left');
        if ((isset($postData['assessment_category']) && count($postData['assessment_category']) > 0) || $postData['order']['0']['column'] == 5) {
            $this->db->join('category_assigned', 'category_assigned.reference_id = questions.id AND category_assigned.reference_type = ' . META_ASSESSMENT . ' AND  category_assigned.reference_sub_type =' . META_QUESTION, 'left');
            $this->db->join('categories', 'categories.id = category_assigned.category_id AND categories.delete_status = 1 AND categories.status = 1', 'left');
        }

        if ((isset($postData['system_cat']) && count($postData['system_cat']) > 0) || $postData['order']['0']['column'] == 7) {
            $this->db->join('category_assigned as assigned_system_category', 'assigned_system_category.reference_id = questions.id AND assigned_system_category.reference_type = ' . META_SYSTEM . ' AND  assigned_system_category.reference_sub_type =' . META_QUESTION, 'left');
            $this->db->join('categories as system_category', 'system_category.id = assigned_system_category.category_id AND system_category.delete_status = 1 AND system_category.status = 1', 'left');
        }

        if ((isset($postData['learning_outcomes']) && count($postData['learning_outcomes']) > 0) || $postData['order']['0']['column'] == 9) {
            $this->db->join('category_assigned as assigned_outcomes_category', 'assigned_outcomes_category.reference_id = questions.id AND assigned_outcomes_category.reference_type = ' . META_LEARNING_OUTCOMES . ' AND  assigned_outcomes_category.reference_sub_type =' . META_QUESTION, 'left');
            $this->db->join('categories as outcomes_category', 'outcomes_category.id = assigned_outcomes_category.category_id AND outcomes_category.delete_status = 1 AND outcomes_category.status = 1', 'left');
        }
        if ((isset($postData['learning_standards']) && count($postData['learning_standards']) > 0) || $postData['order']['0']['column'] == 10) {
            $this->db->join('category_assigned as assigned_standards_category', 'assigned_standards_category.reference_id = questions.id AND assigned_standards_category.reference_type = ' . META_LEARNING_STANDARDS . ' AND  assigned_standards_category.reference_sub_type =' . META_QUESTION, 'left');
            $this->db->join('categories as standards_category', 'standards_category.id = assigned_standards_category.category_id AND standards_category.delete_status = 1 AND standards_category.status = 1', 'left');
        }
        /*Tags*/
        if ((isset($postData['assessment_tag']) && count($postData['assessment_tag']) > 0) || $postData['order']['0']['column'] == 6) {
            $this->db->join('tag_assigned as assigned_assessment_tag', 'assigned_assessment_tag.reference_id = questions.id AND assigned_assessment_tag.reference_type = ' . META_ASSESSMENT . ' AND  assigned_assessment_tag.reference_sub_type =' . META_QUESTION, 'left');
            $this->db->join('tags as assessment_tags', 'assessment_tags.id = assigned_assessment_tag.tag_id AND assessment_tags.delete_status = 1 AND assessment_tags.status = 1', 'left');
        }
        if ((isset($postData['system_tag']) && count($postData['system_tag']) > 0) || $postData['order']['0']['column'] == 8) {
            $this->db->join('tag_assigned as assigned_system_tags', 'assigned_system_tags.reference_id = questions.id AND assigned_system_tags.reference_type = ' . META_SYSTEM . ' AND  assigned_system_tags.reference_sub_type =' . META_QUESTION, 'left');
            $this->db->join('tags as system_tags', 'system_tags.id = assigned_system_tags.tag_id AND system_tags.delete_status = 1 AND system_tags.status = 1', 'left');
        }
        if ($existIDS) {
            $this->db->where_not_in('questions.id', $existIDS);
        }

        if (isset($postData['tel_mastery_points']) && count($postData['tel_mastery_points']) > 0) {
            $this->db->where_in('questions.skill_mastery_point', $postData['tel_mastery_points']);
        }

        /*End tags*/
        if (isset($postData['system_category']) && count($postData['system_category']) > 0) {
            $this->db->where_in('assigned_system_category.category_id', $postData['system_category']);
        }
        if (isset($postData['system_tag']) && count($postData['system_tag']) > 0) {
            $this->db->where_in('assigned_system_tags.tag_id', $postData['system_tag']);
        }
        if (isset($postData['assessment_category']) && count($postData['assessment_category']) > 0) {
            $this->db->where_in('category_assigned.category_id', $postData['assessment_category']);
        }
        if (isset($postData['assessment_tag']) && count($postData['assessment_tag']) > 0) {
            $this->db->where_in('assigned_assessment_tag.tag_id', $postData['assessment_tag']);
        }

        if (isset($postData['learning_outcomes']) && count($postData['learning_outcomes']) > 0) {
            $this->db->where_in('assigned_outcomes_category.category_id', $postData['learning_outcomes']);
        }
        if (isset($postData['learning_standards']) && count($postData['learning_standards'] > 0)) {
            $this->db->where_in('assigned_standards_category.category_id', $postData['learning_standards']);
        }

        $this->db->where(['questions.delete_status' => 1]);

        $i = 0;
        // loop searchable columns 
        foreach ($this->column_search as $item) {
            // if datatable send POST for search
            if ($postData['search']['value']) {
                // first loop
                if ($i === 0) {
                    // open bracket 
                    $this->db->group_start();
                    //pr( $item);	
                    $this->db->like($item, $postData['search']['value'], 'both');
                } else {
                    $this->db->or_like($item, $postData['search']['value'], 'both');
                }
                // last loop
                if (count($this->column_search) - 1 == $i) {
                    // close bracket
                    $this->db->group_end();
                }
            }
            $i++;
        }
        $this->db->group_by('questions.id');
        if (isset($postData['order'])) {
            $this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
}
