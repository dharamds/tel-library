<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Questions extends MY_Controller {

    public function __construct() {
        //ini_set('display_errors', 2);
        parent::__construct();
        $this->load->module('template');
        $this->load->model(['common_model', 'Questions_modal']);
        $this->load->helper(array('html', 'form'));
        $this->load->library('form_validation');

        if (!$this->ion_auth->logged_in()):
            redirect('users/auth', 'refresh');
        endif;
         // get controller permissions		
         if ($this->current_user_permissions = $this->get_permissions()) {
            $this->add_permission = $this->current_user_permissions->add_per;
            $this->edit_permission = $this->current_user_permissions->edit_per;
            $this->delete_permission = $this->current_user_permissions->delete_per;
            $this->view_permission = $this->current_user_permissions->view_per;
            $this->list_permission = $this->current_user_permissions->list_per;
        }
        $this->container_id = get_container_id();
    }
    
    /**
     * index method
     * @description this function use to display list of lesson
     * @return void
     */
    public function index() {
        if (!$this->list_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect('users/auth', 'refresh');
        endif;
        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];       
        $data['breadcrumb'][]           = ['title' => 'Questions', 'link' => '', 'class' => 'active'];
        $data['permissions'] = $this->current_user_permissions;

        $data['page'] = "questions/list";
        $this->template->template_view($data);
    }

     /**
     * getLists method 
     * @description this function called via ajax request, use to display list of lesson
     * @return void
     */
    public function getLists() {
        ini_set('max_execution_time', 1200); //600 seconds = 10 minutes        
        $questionsData = $this->Questions_modal->getRows($_POST);
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $questionsData['total'],
            "recordsFiltered" => $questionsData['total'],
            "data" => $questionsData['result'],
        );
        echo json_encode($output);
    }

    /**
     * add method
     * @description this function use to add question
     * @return void
     */
    public function add($id=NULL) {

         //Permission to access
         if ($id && !$this->edit_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect($_SERVER['HTTP_REFERER'], 'refresh');
        elseif (!$this->add_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect($_SERVER['HTTP_REFERER'], 'refresh');
        endif;
        
        $data['breadcrumb'][] = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][] = ['title' => 'Questions', 'link' => base_url('/questions'), 'class' => 'active'];
        $data['breadcrumb'][] = ['title' => ($id) ? 'Edit' :'Add', 'link' => '', 'class' => 'active'];

        if ($id != null) {
            $data['questionsData'] = $this->common_model->getDataById('questions', '*', ['id' => $id]);
        }
        $questionTypes = $this->common_model->getDataForDropdown('question_types', 'id, name', 'id');
        $data['questionTypes'] = $questionTypes;
        $data['qid'] = $id;
        $data['page'] = "questions/add";
        $this->template->template_view($data);
    }

    /**
     * update_status method
     * @description this function use to update status of question 
     * @return json
     */
    public function update_status($id, $action) {
        $status = ($action == 'activate') ? 1 : 0; 
        $data = array('status' => $status);
        $this->Questions_modal->update_status($id, $data);
        $msg = "Updated";
        echo json_encode(['msg' => $msg]);
        exit;
    }

    public function delete($id) {
            // check permissions
            if (!$this->delete_permission && !$this->ion_auth->is_admin()) :
                $this->session->set_flashdata('error', $this->lang->line('access_denied'));
                redirect($_SERVER['HTTP_REFERER'], 'refresh');
            endif;

        $data = array('delete_status' => 0);
        $this->Questions_modal->update_status($id, $data);
        $msg = "Deleted";
        echo json_encode(['msg' => $msg]);
        exit;
    }


    /**
     * load_view method
     * @description this function use to load view in tab
     * @return html
     */
    public function load_view($qtype = '', $queID = '') {

        $queID = (int)$queID;
        switch ($qtype) {
            case '1': 
                if($queID) {     
                    $columns = "title, long_title, question_prompt,post_submission_msg, response_type_text, response_type_file, random_option";
                    $data['queData'] = $this->common_model->getDataById('questions', $columns, ['id' => $queID]);
                    //echo "ssss".$this->db->last_query();
                    $data['responseData'] = $this->common_model->getAllData('question_responses', '*', '', ['question_id' => $queID, 'delete_status' => 1]);
                }else {
                    $data = [];
                }
                $template_path = "questions/multiple_choice_question";
                break;
            case '2':
                if($queID) {     
                    $columns = "title, long_title, question_prompt,post_submission_msg, response_type_text, response_type_file, random_option, response_credit";
                    $data['queData'] = $this->common_model->getDataById('questions', $columns, ['id' => $queID]);

                    $data['responseData'] = $this->common_model->getAllData('question_responses', '*', '', ['question_id' => $queID, 'delete_status' => 1]);
                }else {
                    $data = [];
                }
                $template_path = "questions/multiple_select_question";
                break;
            case '3': 
                if($queID) {     
                    $columns = "title, long_title, question_prompt,post_submission_msg, response_type_text, response_type_file, random_option";
                    $data['queData'] = $this->common_model->getDataById('questions', $columns, ['id' => $queID]);

                    $data['responseData'] = $this->common_model->getAllData('question_responses', '*', '', ['question_id' => $queID, 'delete_status' => 1]);
                }else {
                    $data = [];
                }
                $template_path = "questions/fib_single_question"; // fib= Fill in the blank
                break;
            case '4':
                if($queID) {     
                    $columns = "title, long_title, question_prompt,post_submission_msg, response_type_text, response_type_file, random_option";
                    $data['queData'] = $this->common_model->getDataById('questions', $columns, ['id' => $queID]);

                    $respData = $this->common_model->DJoin('question_responses.id AS resID, question_responses.answer_status, question_multiple_responses.id AS multiResID, question_multiple_responses.response, question_multiple_responses.feedback', 'question_responses', 'question_multiple_responses', 'question_responses.id=question_multiple_responses.question_responses_id', false, '', ['question_responses.question_id' => $queID, 'question_responses.delete_status' => 1,'question_multiple_responses.delete_status' => 1 ], 'resID ASC');
                    //'question_responses.question_id = '.$queID
                    $multiResData = [];  $temp_id = '';
                    foreach ($respData as $key => $val) {
                        
                        if($temp_id == $val->resID) {
                            $multiResData[$temp_id][] = $val;                             
                        }else{
                           $multiResData[$val->resID][] = $val;                             
                        }
                        $temp_id = $val->resID;
                    }
                    //pr($multiResData);  exit;
                    $data['multiResponseData'] = $multiResData;

                }else {
                    $data = [];
                }
                $template_path = "questions/fib_multiple_question"; // fib= Fill in the blank
                break;
            case '5': 
                if($queID) {     
                    $columns = "title, long_title, question_prompt,post_submission_msg, response_type_text, response_type_file, random_option";
                    $data['queData'] = $this->common_model->getDataById('questions', $columns, ['id' => $queID]);

                    $data['responseData'] = $this->common_model->getAllData('question_responses', '*', '', ['question_id' => $queID, 'delete_status' => 1, 'answer_status' => 1]);

                    $data['detractorData'] = $this->common_model->getAllData('question_responses', '*', '', ['question_id' => $queID, 'delete_status' => 1, 'answer_status' => 0]);
                }else {
                    $data = [];
                }
                $template_path = "questions/drag_drop_question";
                break;
            case '6':
                if($queID) {
                    $columns = "title, long_title, question_prompt, post_submission_msg, response_type_text, response_type_file";
                    $data['queData'] = $this->common_model->getDataById('questions', $columns, ['id' => $queID]);
                    //pr($data['queData']);
                }else {
                    $data = [];
                }
                $template_path = "questions/essay_upload_question";
                break;
        }
        $data['qid'] = $queID;
        echo $this->load->view($template_path, $data, true);
        exit;
    }

    /**
     * addQuestionContent method
     * @description this function use to save questions data in database
     * @return html
     */
    public function addQuestionContent() {        
        //pr($_POST); exit;
        $question_type = $this->input->post('question_type');
        switch ($question_type) 
        {            
            case '1':            
                # multiple choice question
                $que_data = [
                    'created_by'            => $this->session->userdata('user_id'),
                    'type'                  => $this->input->post('question_type'),
                    'title'                 => $this->input->post('title'),
                    'long_title'            => $this->input->post('long_title'),
                    'question_prompt'       => $this->input->post('question_prompt'),
                    'random_option'         => ($this->input->post('option_rand')) ? 1 : 0
                ];
                break;
            case '2':
               # multiple selection question
                $que_data = [
                    'created_by'            => $this->session->userdata('user_id'),
                    'type'                  => $this->input->post('question_type'),
                    'title'                 => $this->input->post('title'),
                    'long_title'            => $this->input->post('long_title'),
                    'question_prompt'       => $this->input->post('question_prompt'),
                    'random_option'         => ($this->input->post('option_rand')) ? 1 : 0,
                    'response_credit'       => $this->input->post('response_credit')
                ];   
                break;
            case '3':
                # fill in the blanck single
                $que_data = [
                    'created_by'            => $this->session->userdata('user_id'),
                    'type'                  => $this->input->post('question_type'),
                    'title'                 => $this->input->post('title'),
                    'long_title'            => $this->input->post('long_title'),
                    'question_prompt'       => $this->input->post('question_prompt'),
                    //'random_option'         => ($this->input->post('option_rand')) ? 1 : 0
                ]; 
                break;
            case '4':
                # fill in the blanck multiple
                $que_data = [
                    'created_by'            => $this->session->userdata('user_id'),
                    'type'                  => $this->input->post('question_type'),
                    'title'                 => $this->input->post('title'),
                    'long_title'            => $this->input->post('long_title'),
                    'question_prompt'       => $this->input->post('question_prompt'),
                    //'random_option'         => ($this->input->post('option_rand')) ? 1 : 0
                ]; 
                break;
            case '5':
                # drag and drop
                $que_data = [
                    'created_by'            => $this->session->userdata('user_id'),
                    'type'                  => $this->input->post('question_type'),
                    'title'                 => $this->input->post('title'),
                    'long_title'            => $this->input->post('long_title'),
                    'question_prompt'       => $this->input->post('question_prompt'),
                    //'random_option'         => ($this->input->post('option_rand')) ? 1 : 0
                ]; 
                break;
            case '6':
                # essay fileupload
                //$post_submission_msg = ($this->input->post('post_submission_msg')!='') ? $this->input->post('post_submission_msg') : '';   
                $que_data = [
                    'created_by'            => $this->session->userdata('user_id'),
                    'type'                  => $this->input->post('question_type'),
                    'title'                 => $this->input->post('title'),
                    'long_title'            => $this->input->post('long_title'),
                    'question_prompt'       => $this->input->post('question_prompt'),
                    'post_submission_msg'   => $this->input->post('post_submission_msg'),
                    'response_type_text'    => ($this->input->post('response_type_text') == 1) ? 1 : NULL,
                    'response_type_file'    => ($this->input->post('response_type_file') == 1) ? 1 : NULL,
                    
                ];
                break;           
        }// end switch ($question_type)    
        //pr($que_data);exit;

        if($question_type==4) {
            # FIB Mulitple
            //pr($_POST);
            if($this->input->post('que_cont_id')=="") {
                pr($_POST);exit;
                $this->form_validation->set_rules('question_type', 'Question type', 'required');
                $this->form_validation->set_rules('title', 'Title', 'trim|required');
                $this->form_validation->set_rules('question_prompt', 'question prompt', 'trim|required');
                //$this->form_validation->set_rules('response[]', 'Response', 'trim|required');
                if ($this->form_validation->run() == FALSE) {
                    $msg['error'] = validation_errors();
                    $msg['code'] = 400;
                    //echo validation_errors();
                } else {
                    $this->common_model->InsertData('questions', $que_data);
                    $lastInsertId = $this->db->insert_id();
                    $resp_data = $this->input->post('res_data');
                    $answer_status = $this->input->post('answer_status');
                    $que_res_data = []; $i=0; $cnt=1;
                    foreach ($resp_data as $rskey => $respValue) {

                        $que_res_data[$i]['question_id'] = $lastInsertId;
                        $que_res_data[$i]['response']      = 'FIB '.$cnt;
                        $que_res_data[$i]['feedback']      =  NULL;
                        $que_res_data[$i]['weight']        =  NULL;

                        if(in_array($cnt, $answer_status)) {
                            $que_res_data[$i]['answer_status'] = 1;
                        }else 
                            $que_res_data[$i]['answer_status'] = 0;

                        # Insert data into question_responses tabale
                        $this->common_model->InsertData('question_responses', $que_res_data[$i]);
                        $lastQueRespInsertId = $this->db->insert_id();

                        # Inserting data into multiple FIB responses into the table question_multiple_responses
                        $multi_res_data = []; $k=0; $c=1;
                        foreach ($respValue['response'] as $mreskey => $resMultiValue) {
                            $multi_res_data[$k]['question_responses_id'] = $lastQueRespInsertId;
                            $multi_res_data[$k]['question_id'] = $lastInsertId;
                            $multi_res_data[$k]['response'] = $resMultiValue;
                            $multi_res_data[$k]['feedback'] = isset($respValue['feedback'][$mreskey]) ? $respValue['feedback'][$mreskey] : NULL;                         
                            $k++; $c++;
                        }
                        $this->common_model->InsertBatchData('question_multiple_responses', $multi_res_data);
                        $i++; $cnt++;
                    }
                    //echo $lastInsertId;
                    $msg['success'] = "Question has been added successfully.";
                    $msg['code'] = 200;
                    $msg['id'] = $lastInsertId;
                }
                echo json_encode($msg);
            } else {
                //pr($_POST); exit;
                $this->form_validation->set_rules('question_type', 'Question type', 'required');
                $this->form_validation->set_rules('title', 'Title', 'trim|required');
                $this->form_validation->set_rules('question_prompt', 'question prompt', 'trim|required');
                //$this->form_validation->set_rules('response[]', 'Response', 'trim|required');
                
                if ($this->form_validation->run() == FALSE) {
                    $msg['error'] = validation_errors();
                    $msg['code'] = 400;
                    //echo validation_errors();
                } else {

                    $question_id = $this->input->post('que_cont_id');
                    $this->common_model->UpdateDB('questions', ['id' => $question_id], $que_data);

                    $resp_data = $this->input->post('res_data');
                    $answer_status = $this->input->post('answer_status');
                    $que_resp_id_data = $this->input->post('resp_id_data');
                    $que_res_data = []; $i=0; $cnt=1;
                    foreach ($resp_data as $rskey => $respValue) {

                        $que_res_data[$i]['question_id']   = $question_id;
                        $que_res_data[$i]['response']      = 'FIB '.$cnt;
                        $que_res_data[$i]['feedback']      =  NULL;
                        $que_res_data[$i]['weight']        =  NULL;

                        if(in_array($cnt, $answer_status)) {
                            $que_res_data[$i]['answer_status'] = 1;
                        }else 
                            $que_res_data[$i]['answer_status'] = 0;

                        //$que_response_id = isset($respValue['id'][0]) ? $respValue['id'][0] : NULL;
                        $que_response_id = isset($que_resp_id_data[$i]['id']) ? $que_resp_id_data[$i]['id'] : NULL;
                        //echo $que_response_id; echo "<br>"; pr($que_res_data[$i]);
                        if($que_response_id == NULL) {
                            $this->common_model->InsertData('question_responses', $que_res_data[$i]);
                            $lastQueRespInsertId = $this->db->insert_id();

                            $multi_res_data = []; $k=0; $c=1;
                            foreach ($respValue['response'] as $mreskey => $resMultiValue) {
                                $multi_res_data[$k]['question_responses_id'] = $lastQueRespInsertId;
                                $multi_res_data[$k]['question_id'] = $question_id;
                                $multi_res_data[$k]['response'] = $resMultiValue;
                                $multi_res_data[$k]['feedback'] = isset($respValue['feedback'][$mreskey]) ? $respValue['feedback'][$mreskey] : NULL;                         
                                $k++; $c++;
                            }
                            $this->common_model->InsertBatchData('question_multiple_responses', $multi_res_data);
                        }else {
                            $this->common_model->UpdateDB('question_responses', ['id'=> $que_response_id], $que_res_data[$i]);
                            
                            $multi_res_data = []; $k=0; $c=1;
                            foreach ($respValue['response'] as $mreskey => $resMultiValue) {
                                $multi_res_data[$k]['question_responses_id'] = $que_response_id;
                                $multi_res_data[$k]['question_id'] = $question_id;
                                $multi_res_data[$k]['response'] = $resMultiValue;
                                $multi_res_data[$k]['feedback'] = isset($respValue['feedback'][$mreskey]) ? $respValue['feedback'][$mreskey] : NULL;    

                                $que_multi_response_id  =  isset($respValue['mresid'][$k]) ? $respValue['mresid'][$k] : NULL; 
                                if($que_multi_response_id==NULL) {
                                    $this->common_model->InsertData('question_multiple_responses', $multi_res_data[$k]);
                                }else {
                                    $this->common_model->UpdateDB('question_multiple_responses', ['id'=> $que_multi_response_id], $multi_res_data[$k]);         
                                }
                                $k++; $c++;
                            }
                        }
                        $i++; $cnt++;                    
                    }
                    //exit;
                    $msg['success'] = "Question has been updated successfully.";
                    $msg['code'] = 200;
                    $msg['id'] = $question_id;
                }                
                echo json_encode($msg);            
            }
        } else if($question_type == 5) {         
            # Drag and Drop
            //pr($_POST); exit;
            if($this->input->post('que_cont_id')=="") {
                //pr($_POST); exit;
                $this->form_validation->set_rules('question_type', 'Question type', 'required');
                $this->form_validation->set_rules('title', 'Title', 'trim|required');
                $this->form_validation->set_rules('question_prompt', 'question prompt', 'trim|required');
                //$this->form_validation->set_rules('response[]', 'Response', 'trim|required');
                if ($this->form_validation->run() == FALSE) {
                    $msg['error'] = validation_errors();
                    $msg['code'] = 400;
                    //echo validation_errors();
                } else {

                    $this->common_model->InsertData('questions', $que_data);
                    $lastInsertId = $this->db->insert_id();
                             
                    $responses = $this->input->post('response'); // Responses
                    $feedbacks = $this->input->post('feedback'); // Feedback
                    $answer_status = $this->input->post('answer_status'); // Answer      
                    $weights = $this->input->post('weight'); // Answer                
                    $res_data = [];
                    $i=0; $cnt=1; 
                    foreach ($responses as $reskey => $respValue) {
                        $res_data[$i]['question_id'] = $lastInsertId;
                        $res_data[$i]['response'] = $respValue;
                        $res_data[$i]['feedback'] = $feedbacks[$reskey];
                        $res_data[$i]['weight'] = isset($weights[$reskey]) ? $weights[$reskey] : NULL;
                        $res_data[$i]['answer_status'] = 1;
                        $i++; $cnt++;
                    }

                    # Detractor
                    $detract_responses = $this->input->post('detract_resp');
                    $detract_feedbacks = $this->input->post('detract_feedback'); // Feedback
                    //$i=0;
                    foreach ($detract_responses as $detrkey => $respDetrValue) {
                        $res_data[$i]['question_id'] = $lastInsertId;
                        $res_data[$i]['response'] = $respDetrValue;
                        $res_data[$i]['feedback'] = $detract_feedbacks[$detrkey];
                        $res_data[$i]['weight'] = NULL;
                        $res_data[$i]['answer_status'] = 0;
                        $i++; 
                    }
                    $this->common_model->InsertBatchData('question_responses', $res_data);
                    //echo $lastInsertId; 
                    $msg['success'] = "Question has been added successfully.";
                    $msg['code'] = 200;
                    $msg['id'] = $lastInsertId;
                }
                echo json_encode($msg);
            }else {
                //pr($_POST);exit;
                $this->form_validation->set_rules('question_type', 'Question type', 'required');
                $this->form_validation->set_rules('title', 'Title', 'trim|required');
                $this->form_validation->set_rules('question_prompt', 'question prompt', 'trim|required');
                //$this->form_validation->set_rules('response[]', 'Response', 'trim|required');
                if ($this->form_validation->run() == FALSE) {
                    $msg['error'] = validation_errors();
                    $msg['code'] = 400;
                    //echo validation_errors();
                } else {
                    $question_id = $this->input->post('que_cont_id');
                    $this->common_model->UpdateDB('questions', ['id' => $question_id], $que_data);

                    $responses = $this->input->post('response'); // Responses
                    $feedbacks = $this->input->post('feedback'); // Feedback
                    $answer_status = $this->input->post('answer_status'); // Answer      
                    $weights = $this->input->post('weight'); // Answer    
                    $question_resp = $this->input->post('que_resp');        
                    $res_data = [];
                    
                    # Response
                    foreach ($responses as $reskey => $respValue) {
                        $res_data['question_id'] = $question_id;
                        $res_data['response'] = $respValue;
                        $res_data['feedback'] = $feedbacks[$reskey];
                        $res_data['weight'] = isset($weights[$reskey]) ? $weights[$reskey] : NULL;
                        $res_data['answer_status'] = 1;
                        
                        $que_response_id = isset($question_resp[$reskey]) ? $question_resp[$reskey] : NULL; 
                        if($que_response_id==NULL) {
                            $this->common_model->InsertData('question_responses', $res_data);
                        }else {
                            $this->common_model->UpdateDB('question_responses', ['id'=> $que_response_id], $res_data);         
                        }
                    }

                    # Detractor
                    $detract_responses = $this->input->post('detract_resp'); // Detractor
                    $detract_feedbacks = $this->input->post('detract_feedback'); // Feedback
                    $question_detr = $this->input->post('ques_detr');  
                    //$i=0; 
                    //$detr_data = [];
                    foreach ($detract_responses as $detrkey => $respDetrValue) {
                        $detr_data['question_id'] = $question_id;
                        $detr_data['response'] = $respDetrValue;
                        $detr_data['feedback'] = $detract_feedbacks[$detrkey];
                        $detr_data['weight'] = NULL;
                        $detr_data['answer_status'] = 0;

                        $que_detractor_id = isset($question_detr[$detrkey]) ? $question_detr[$detrkey] : NULL; 
                        if($que_detractor_id==NULL) {
                            $this->common_model->InsertData('question_responses', $detr_data);
                        }else {
                            $this->common_model->UpdateDB('question_responses', ['id'=> $que_detractor_id], $detr_data);         
                        }   
                        //echo $que_detractor_id; pr($detr_data);                
                    } //exit;
                    //echo $question_id;
                    $msg['success'] = "Question has been updated successfully.";
                    $msg['code'] = 200;
                    $msg['id'] = $question_id;    
                }
                echo json_encode($msg);
            }
        } else if($question_type == 6) {      
            # Essay and file upload
            if($this->input->post('que_cont_id')=="") {
                $this->form_validation->set_rules('question_type', 'Question type', 'required');
                $this->form_validation->set_rules('title', 'Title', 'trim|required');
                $this->form_validation->set_rules('question_prompt', 'question prompt', 'trim|required');
                if ($this->form_validation->run() == FALSE) {
                    $msg['error'] = validation_errors();
                    $msg['code'] = 400;
                    //echo validation_errors();
                } else {
                    $this->common_model->InsertData('questions', $que_data);
                    $lastInsertId = $this->db->insert_id();
                    //echo $lastInsertId;
                    $msg['success'] = "Question has been added successfully.";
                    $msg['code'] = 200;
                    $msg['id'] = $lastInsertId;                    
                }
                echo json_encode($msg);
            }else {
                $this->form_validation->set_rules('question_type', 'Question type', 'required');
                $this->form_validation->set_rules('title', 'Title', 'trim|required');
                $this->form_validation->set_rules('question_prompt', 'question prompt', 'trim|required');
                if ($this->form_validation->run() == FALSE) {
                    $msg['error'] = validation_errors();
                    $msg['code'] = 400;
                    //echo validation_errors();
                } else {
                    //pr($que_data);
                    $question_id = $this->input->post('que_cont_id');
                    $this->common_model->UpdateDB('questions', ['id' => $question_id], $que_data);
                    //echo $this->db->last_query(); exit;
                    $msg['success'] = "Question has been added successfully.";
                    $msg['code'] = 200;
                    $msg['id'] = $question_id;     
                }
                echo json_encode($msg);
            }
        } else {
            //echo "Here";
            # for question type MCQ, MSQ, FIB Single
            if($this->input->post('que_cont_id')=="") {
                //pr($_POST);exit;

                $this->form_validation->set_rules('question_type', 'Question type', 'required');
                $this->form_validation->set_rules('title', 'Title', 'trim|required');
                $this->form_validation->set_rules('question_prompt', 'question prompt', 'trim|required');
                $this->form_validation->set_rules('response[]', 'Response', 'trim|required');
                if ($this->form_validation->run() == FALSE) {
                    $msg['error'] = validation_errors();
                    $msg['code'] = 400;
                    //echo validation_errors();
                } else {

                    $this->common_model->InsertData('questions', $que_data);
                    $lastInsertId = $this->db->insert_id();
                    //$lastInsertId =55; # For testing
         
                    $responses = $this->input->post('response'); // Responses
                    $feedbacks = $this->input->post('feedback'); // Feedback
                    $answer_status = $this->input->post('answer_status'); // Answer      
                    $weights = $this->input->post('weight'); // Answer                
                    $res_data = [];
                    $i=0; $cnt=1; 
                    foreach ($responses as $reskey => $respValue) {
                        $res_data[$i]['question_id'] = $lastInsertId;
                        $res_data[$i]['response'] = $respValue;
                        $res_data[$i]['feedback'] = $feedbacks[$reskey];
                        $res_data[$i]['weight'] = isset($weights[$reskey]) ? $weights[$reskey] : NULL;

                        if(in_array($cnt, $answer_status)) {
                            $res_data[$i]['answer_status'] = 1;
                        }else 
                            $res_data[$i]['answer_status'] = 0;
                        $i++; $cnt++;
                    }
                    //pr($res_data); exit();
                    $this->common_model->InsertBatchData('question_responses', $res_data);

                    $que_resp_ids = $this->common_model->getAllData('question_responses', 'id', '', ['question_id' => $lastInsertId , 'delete_status'=> 1], 'id');
                    $msg['respids'] = $this->getKeyValueArray($que_resp_ids, 'id');                    
                    $msg['success'] = "Question has been added successfully.";
                    $msg['code'] = 200;
                    $msg['id'] = $lastInsertId;   
                }
                echo json_encode($msg);
            }else {
                //echo "test";  pr($_POST);exit;
                $this->form_validation->set_rules('question_type', 'Question type', 'required');
                $this->form_validation->set_rules('title', 'Title', 'trim|required');
                $this->form_validation->set_rules('question_prompt', 'question prompt', 'trim|required');
                $this->form_validation->set_rules('response[]', 'Response', 'trim|required');
                if ($this->form_validation->run() == FALSE) {
                    $msg['error'] = validation_errors();
                    $msg['code'] = 400;
                    //echo validation_errors();
                } else {
                    $question_id = $this->input->post('que_cont_id');
                    $this->common_model->UpdateDB('questions', ['id' => $question_id], $que_data);

                    $responses = $this->input->post('response'); // Responses
                    $feedbacks = $this->input->post('feedback'); // Feedback
                    $answer_status = $this->input->post('answer_status'); // Answer      
                    $weights = $this->input->post('weight'); // Answer       
                    $que_res = $this->input->post('res_data'); 

                    $que_res_ids_arr = [];
                    foreach ($que_res as $qrskey => $qrsvalue) {
                        array_push($que_res_ids_arr, $qrsvalue['id']);
                    }
                    sort($que_res_ids_arr);
                    //pr($que_res_ids_arr); exit;
                    $cnt=1; 
                    foreach ($responses as $reskey => $respValue) {
                        
                        $res_data = [];
                        $res_data['question_id'] = $question_id;
                        $res_data['response'] = $respValue;
                        $res_data['feedback'] = $feedbacks[$reskey];
                        $res_data['weight'] = isset($weights[$reskey]) ? $weights[$reskey] : NULL;

                        if(in_array($cnt, $answer_status)) {
                            $res_data['answer_status'] = 1;
                        }else 
                            $res_data['answer_status'] = 0;

                        $que_response_id = isset($que_res_ids_arr[$reskey]) ? $que_res_ids_arr[$reskey] : NULL;

                        if($que_response_id == NULL) {
                            $this->common_model->InsertData('question_responses', $res_data);
                        }else {
                            $this->common_model->UpdateDB('question_responses', ['id'=> $que_response_id], $res_data);
                        }
                        $cnt++;
                    } 
                    //echo $question_id;    
                    $que_resp_ids = $this->common_model->getAllData('question_responses', 'id', '', ['question_id' => $question_id, 'delete_status'=> 1 ], 'id');
                    $msg['respids'] = $this->getKeyValueArray($que_resp_ids, 'id'); 

                    $msg['success'] = "Question has been updated successfully.";
                    $msg['code'] = 200;
                    $msg['id'] = $question_id;                   
                }   
                echo json_encode($msg);     
            } 
        }        
        exit;
    }


    public function setQuestionMasteryPoint() {
        //pr($_POST); exit;

        if((int)$this->input->post('qid') ) {
            $point = $this->input->post('skill_mastery_point');
            $data = ['skill_mastery_point' => $point];
            $id = $this->input->post('qid');
            $this->common_model->UpdateDB('questions', ['id' => $id], $data);
            $msg['success'] = "Question mastery point has been set.";
            $msg['code'] = 200;
            $msg['id'] = $id;
        }else {
            $msg['error'] = "Unable to set mastery point";
            $msg['code'] = 400;
            $msg['id'] = $id;
        }
        echo json_encode($msg); 
    }

    public function remove_question_response() {
        //pr($_POST); exit;
        if((int)$this->input->post('id') ) {
            $question_id =  $this->input->post('qid');
            $data = ['delete_status' => 0];
            $id = $this->input->post('id');
            $this->common_model->UpdateDB('question_responses', ['id' => $id], $data);
            $que_resp_ids = $this->common_model->getAllData('question_responses', 'id', '', ['question_id' => $question_id, 'delete_status' => 1 ], 'id');
            $msg['respids'] = $this->getKeyValueArray($que_resp_ids, 'id');                    
            $msg['success'] = "Question has been removed successfully.";
            $msg['code'] = 200;
            $msg['id'] = 1;  
        }else {
            $msg['error'] = "Unable to remove question.";
            $msg['code'] = 400;
        }
        echo json_encode($msg);     
    }

    public function remove_question_response_drag() {
        
        if((int)$this->input->post('id') ) {
            $question_id    =  $this->input->post('qid');
            $data           = ['delete_status' => 0];
            $id             = $this->input->post('id');
            $this->common_model->UpdateDB('question_responses', ['id' => $id], $data);
            $que_resp_ids   = $this->common_model->getAllData('question_responses', 'id', '', ['question_id' => $question_id, 'delete_status' => 1, 'answer_status' => 1 ], 'id');
            $msg['respids'] = $this->getKeyValueArray($que_resp_ids, 'id');                    
            $msg['success'] = "Question has been removed successfully.";
            $msg['code']    = 200;
            $msg['id']      = 1;  
        }else {
            $msg['error']   = "Unable to remove question.";
            $msg['code']    = 400;
        }
        echo json_encode($msg);     
    }


    public function remove_question_detractor_drag() {
        //pr($_POST); exit;
        if((int)$this->input->post('id') ) {
            $question_id =  $this->input->post('qid');
            $data = ['delete_status' => 0];
            $id = $this->input->post('id');
            $this->common_model->UpdateDB('question_responses', ['id' => $id], $data);
            $que_detr_ids = $this->common_model->getAllData('question_responses', 'id', '', ['question_id' => $question_id, 'delete_status' => 1, 'answer_status' => 0 ], 'id');
            $msg['detrids'] = $this->getKeyValueArray($que_detr_ids, 'id');                    
            $msg['success'] = "Question has been removed successfully.";
            $msg['code'] = 200;
            $msg['id'] = 1;  
        }else {
            $msg['error'] = "Unable to remove question.";
            $msg['code'] = 400;
        }
        echo json_encode($msg);     
    }

    public function remove_question_outer_response() {
        //pr($_POST); exit;

        if((int)$this->input->post('id') ) {
            $question_id =  $this->input->post('qid');
            $question_responses_id =  $this->input->post('id');
            $data = ['delete_status' => 0];

            $this->common_model->UpdateDB('question_multiple_responses', ['question_id' => $question_id, 'question_responses_id' => $question_responses_id ], $data);
            $this->common_model->UpdateDB('question_responses', ['id' => $question_responses_id], $data);

            $que_resp_ids = $this->common_model->getAllData('question_responses', 'id', '', ['question_id' => $question_id, 'delete_status' => 1 ], 'id');

            $msg['respids'] = $this->getKeyValueArray($que_resp_ids, 'id');                                       
            $msg['success'] = "Question has been removed successfully.";
            $msg['code'] = 200;
            $msg['id'] = 1;  
        }else {
            $msg['error'] = "Unable to remove question.";
            $msg['code'] = 400;
        }
        echo json_encode($msg);     
    }

    public function remove_question_inner_response () {
        //pr($_POST); exit;
        if((int)$this->input->post('id') ) {
            $question_id =  $this->input->post('qid');
            $question_responses_id =  $this->input->post('question_responses_id');
            $data = ['delete_status' => 0];
            $id = $this->input->post('id');
            $this->common_model->UpdateDB('question_multiple_responses', ['id' => $id], $data);
            $que_multi_resp_ids = $this->common_model->getAllData('question_multiple_responses', 'id', '', ['question_id' => $question_id, 'question_responses_id' => $question_responses_id, 'delete_status' => 1 ], 'id');
            $msg['multirespids'] = $this->getKeyValueArray($que_multi_resp_ids, 'id');                    
            $msg['success'] = "Question has been removed successfully.";
            $msg['code'] = 200;
            $msg['id'] = 1;  
        }else {
            $msg['error'] = "Unable to remove question.";
            $msg['code'] = 400;
        }
        echo json_encode($msg);     
    }

    function get_question_details() {

        if((int)$this->input->post('qid') ) {
            $question_id = $this->input->post('qid');
            $question_type = $this->input->post('qtype');

            if($question_type==2) {
                $columns = "title, long_title, question_prompt";
                $queDetails = $this->common_model->getDataById('questions', $columns, ['id' => $question_id]);
                
                $data['title'] = $queDetails->question_prompt;

                $rescolumns = "response, answer_status";
                $respDetails = $this->common_model->getAllData('question_responses', $rescolumns, '', ['question_id' => $question_id, 'delete_status' => 1]);
                foreach ($respDetails as $key => $value) {
                    $data['responses'][$key] = $value->response;
                    $data['ansstatus'][$key] = $value->answer_status;
                }
                $data['success'] = "Fetching question details";
                $data['code'] = 200;
            }else if($question_type==1) {
                $columns = "title, long_title,question_prompt";
                $queDetails = $this->common_model->getDataById('questions', $columns, ['id' => $question_id]);
                
                $data['title'] = $queDetails->question_prompt;

                $rescolumns = "response, answer_status";
                $respDetails = $this->common_model->getAllData('question_responses', $rescolumns, '', ['question_id' => $question_id, 'delete_status' => 1]);
                foreach ($respDetails as $key => $value) {
                    $data['responses'][$key] = $value->response;
                    $data['ansstatus'][$key] = $value->answer_status;
                }
                $data['success'] = "Fetching question details";
                $data['code'] = 200;
            }else if($question_type==3) {
                $columns = "title, long_title, question_prompt";
                $queDetails = $this->common_model->getDataById('questions', $columns, ['id' => $question_id]);
                $singleFib = '<div class="d-inline-block ml-3 mr-3"><input type="text" class="form-control inline" name=""></div>'; 
                $question_sentence = str_replace("{1}", $singleFib, $queDetails->question_prompt);
                
                $data['title'] = $question_sentence;

                $rescolumns = "response, answer_status";
                $respDetails = $this->common_model->getAllData('question_responses', $rescolumns, '', ['question_id' => $question_id, 'delete_status' => 1]);
                foreach ($respDetails as $key => $value) {                   
                    $data['responses'][$key] = $value->response;
                    $data['ansstatus'][$key] = $value->answer_status;
                }
                $data['success'] = "Fetching question details";
                $data['code'] = 200;
            }else if($question_type==5) {
                $columns = "title, long_title, question_prompt";
                $queDetails = $this->common_model->getDataById('questions', $columns, ['id' => $question_id]);
                $singleFib = '<div class="d-inline-block ml-3 mr-3 w120"><input type="text" class="form-control inline form-input-drag" name=""></div>'; 

                $question_sentence = str_replace("{1}", $singleFib, $queDetails->question_prompt);                
                $question_sentence = str_replace("{2}", $singleFib, $question_sentence);
                $question_sentence = str_replace("{3}", $singleFib, $question_sentence);
                $question_sentence = str_replace("{4}", $singleFib, $question_sentence);
                
                $data['title'] = $question_sentence;

                # Responses
                $rescolumns = "response, answer_status";
                $respDetails = $this->common_model->getAllData('question_responses', $rescolumns, '', ['question_id' => $question_id, 'delete_status' => 1, 'answer_status' => 1]);
                foreach ($respDetails as $key => $value) {                   
                    $data['responses'][$key] = $value->response;
                    $data['ansstatus'][$key] = $value->answer_status;
                }

                # Detractor
                $detrcolumns = "response, answer_status";
                $respDetails = $this->common_model->getAllData('question_responses', $rescolumns, '', ['question_id' => $question_id, 'delete_status' => 1, 'answer_status' => 0]);
                foreach ($respDetails as $key => $value) {                   
                    $data['detractors'][$key] = $value->response;
                    $data['ansstatus'][$key] = $value->answer_status;
                }

                $data['success'] = "Fetching question details";
                $data['code'] = 200;
            }

        }else {
            $data['error'] = "Unable to remove question.";
            $data['code'] = 400;
        }
        echo json_encode($data);     
    }
    
    function get_meta_tags($typeid)
        {
            $data['typeid'] = $typeid;
            list($question_id, $type, $subtype) = explode("_", $typeid);
            $data['value'] = $this->common_model->getTags($question_id, $type, $subtype);
            echo json_encode($data); exit;
        }
    function get_meta_categories($typeid)
        {
            $data['typeid'] = $typeid;
            list($question_id, $type, $subtype, $type2) = explode("_", $typeid);
            $data['value'] = $this->common_model->getCategories($question_id, $type, $subtype);
            echo json_encode($data); exit;
        }
    function get_question_used($question_id)
        {
            $data['typeid'] = $question_id;
            list($question_id, $type) = explode("_", $question_id);
            $data['value'] = $this->Questions_modal->get_question_used($question_id);
            echo json_encode($data); exit;
        }    
    
        function getKeyValueArray($arr, $field) {
        $data = [];
        foreach ($arr as $key => $value) {
            # code...
            array_push($data, $value->$field);
        }
        return $data;
    }

}
