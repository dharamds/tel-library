<!-- FIB Multiple -->

<!-- Title -->
<div class="row mt-4 mb-3">
  <div class="col-sm-4">
    <div class="form-group">
      <label for="title" class="control-label"> Title <span class="field-required">*</span> </label>
      <?php
        $title = isset($queData->title) ? $queData->title : '';
        echo form_input([
          'id'    => 'title',
          'name'  => 'title',
          'value' => $title,
          'class' => 'form-control'
        ]);
      ?>
    </div>
  </div>
  <!-- Long Title  -->
  <div class="col-sm-8">
    <div class="form-group">
      <label for="long_title" class="control-label"> Long Title </label>
      <?php
        $long_title = isset($queData->long_title) ? $queData->long_title : '';
        echo form_input([
          'id'    => 'long_title',
          'name'  => 'long_title',
          'value' => $long_title,
          'class' => 'form-control'
        ]);
      ?>
    </div>
  </div>
</div>

<!-- Question or prompt -->
<div class="row mt-4 mb-3">
  <div class="col-sm-8">
    <div class="form-group">
      <label for="question_prompt" class="control-label">Question or prompt <span class="field-required">*</span></label>
      <?php
        $question_prompt = isset($queData->question_prompt) ? $queData->question_prompt : '';
      ?>
      <?php
        echo form_textarea([
          'name'  => 'question_prompt',
          'id'    => 'question_prompt',
          'value' => $question_prompt,
          'class' => 'form-control ckeditor',
          'rows'  => '5'
        ]);
      ?>
    </div>
  </div>
</div>
<!-- Question type oriented view -->


<?php if(isset($multiResponseData)) { //pr($multiResponseData); exit; ?>
  <div id="extraOuterFibMResponseDiv">
<?php
  $cnt=1; $c=0;
  foreach ($multiResponseData as $key => $multiResponses) {  ?>

  
    <?php if($cnt > 2) { ?>
      <div class="out-res" id="div_outer_<?=$cnt?>">
    <?php } ?>

    <div class="col-xs-12 fib-response-panel up-out-class">
      
        <div class="hiddenOuterinputs">        
          <?php 
            $hid_resp_opt = [  'type'  => 'hidden', 
                               'name'    => 'resp_id_data['.$c.'][id]',
                               'id'  => 'que_res'.$c,
                               'value' => $key
                            ];
            echo form_input($hid_resp_opt); 
          ?> 
        </div>

      <h4 class="lblFibOuter"> <i class="flaticon-checklist"></i> FIB Response <?=$cnt;?> *</h4>
    </div>
    <?php
      if(count($multiResponses) > 0 ) { ?>

        <div class="col-xs-12 p-0 fib-left">
          <div id="extraInnerFibMResponseDiv_<?=$cnt?>">
        <?php  
        $inncnt=1; $innc=0;
        //pr($multiResponses);
        foreach($multiResponses as $rkey => $response) { ?>
          
          

          <?php if($inncnt > 1) { ?>
            <div class="inn-res-<?=$cnt?>" id="div_<?=$cnt?>_inn_res_<?=$inncnt?>"> 
          <?php } ?>
          
            <div class="panel panel-response">
              <div class="panel-body">
                <div class="form-group col-xs-12 p-0 pt-3 up-inn-class-<?=$cnt;?>">
                  <div class="hiddeninputs">
                    <?php 
                      $hid_multi_resp_opt = [  'type'  => 'hidden', 
                                                'name' => 'res_data['.$cnt.'][mresid]['.$innc.']',
                                                'id'   => 'que_multi_res_'.$cnt.'_'.$innc,
                                                'value' => $response->multiResID
                                            ];
                      echo form_input($hid_multi_resp_opt); 
                    ?>
                  </div>
                  <label class="control-label lblInnRes-<?=$cnt;?>">Response <?=$inncnt;?> *</label>
                  <?php if($inncnt==1) { ?>
                    <div class="pull-right mt--10">
                      <div class="flex-row align-self-end text-right">
                        <label class="checkbox-tel flex-col-auto p-0 ml-auto pt-2">  
                          <?php 
                            $opt_ans = ['name'   => 'answer_status[]',
                                         'id'   => 'fib_'.$cnt.'_answer_status_'.$inncnt,
                                         'value'  => $cnt,
                                         'checked' => ($response->answer_status==1)?'checked' : '',
                                         'class'  => 'ans-opt'
                                        ];
                                      echo form_checkbox($opt_ans); 
                          ?> Correct Response
                        </label>
                      </div>
                    </div>
                  <?php } ?>
                  <div class="col-xs-12 p-0 mt-3">
                    <?php
                      $txtRarea = [ 'name'  => 'res_data['.$cnt.'][response][]',
                                    'id'    => 'fib_'.$cnt.'_response_'.$inncnt,
                                    'value' => $response->response,
                                    'class' => 'data-sample-short ckeditor txtInnRes-'.$cnt,
                                  ];
                      echo form_textarea($txtRarea);
                    ?>
                  </div>
                </div>
                <div class="form-group col-xs-12 p-0 mt-3">
                  <label class="control-label">Feedback</label>
                  <div class="col-xs-12 p-0">
                    <div class="flex-row align-items-end">
                      <div class="flex-col-7">
                        <?php
                          $txtFarea = [ 'name'  => 'res_data['.$cnt.'][feedback][]',
                                        'id'    => 'fib_'.$cnt.'_feedback_'.$inncnt,
                                        'value' => $response->feedback,
                                        'class' => 'data-sample-short ckeditor txtInnFb-'.$cnt,
                                      ];
                          echo form_textarea($txtFarea);
                        ?>
                      </div>
                      <?php if($inncnt > 1) { ?>
                        <div class="flex-col text-right">
                          <a href="javascript:void(0)" class="btn btn-danger btn-md remove-inner-response" data-inndivid="<?=$cnt?>" data-innresid="<?=$inncnt?>" data-multi-resp-id="<?php echo $response->multiResID; ?>" data-que-resp-id="<?php echo $key; ?>"><i class="flaticon-waste-bin "></i>Remove</a>
                        </div>
                      <?php } // end if($inncnt > 1) ?>   
                    </div>
                  </div>
                </div>
              </div>
            </div>            
          <?php if($inncnt > 1) { ?> </div> <?php } ?>                 
        <?php
          $inncnt++; $innc++;
        } // end foreach($multiResponses as $rkey => $response) ?>      
      </div>
    <?php } //end if(count($multiResponses) > 0 ) ?>
    <!-- Add inner response button -->
    <div class="col-xs-12 p-0 pb-4" >
      <div id="errorLimitDiv_<?=$cnt?>" style="display: none;"></div>
      <div class="col-xs-12 col-md-6 p-0">
        <a href="javascript:void(0)" class="btn btn-warning add-inner-fib-response" data-id="<?=$cnt?>">Add inner Response</a>
      </div>
      <?php if($cnt > 2) { ?>
        <div class="col-xs-12 col-md-6 p-0 text-right">
          <a href="javascript:void(0)" class="btn btn-danger btn-md remove-outer-response" data-fibid="<?=$cnt?>"  data-que-resp-id="<?=$key?>"> <i class="flaticon-waste-bin"></i> Remove FIB </a>
        </div>
      <?php } ?>     
    </div> 
    </div>

    <!-- Seperator -->
    <div class="col-xs-12 mt-3 mb-3 p-0 pb-2"> <div class="hr-line mb-4"></div> </div>
   
    <?php if($cnt > 2) { ?></div> <?php } ?>

  <?php
  $cnt++; $c++;
  }// end foreach ?>
</div>
<?php 
} else { ?>  
<!--  FIB 1 Response -->
  <div class="col-xs-12 fib-response-panel up-out-class">
    <div class="hiddenOuterinputs"></div>
    <h4 class="lblFibOuter"> <i class="flaticon-checklist"></i> FIB Response 1 *</h4>
  </div>
    <div class="col-xs-12 p-0 fib-left">
      <div class="panel panel-response">
        <div class="panel-body">
          <div class="form-group col-xs-12 p-0 pt-3 up-inn-class-1">
            <div class="hiddeninputs"></div>
            <label class="control-label lblInnRes-1">Response 1 *</label>
              <div class="pull-right mt--10">
                <div class="flex-row align-self-end text-right">
                  <label class="checkbox-tel flex-col-auto p-0 ml-auto pt-2">  
                    <?php 
                      $opt_ans = [
                                         'name'   => 'answer_status[]',
                                         'id'   => 'fib_1_answer_status_1',
                                         'value'  => 1,
                                         'class'  => 'ans-opt'
                                        ];
                                      echo form_checkbox($opt_ans); 
                    ?>
                    Correct Response
                  </label>
                </div>
              </div>
            <div class="col-xs-12 p-0 mt-3">
              <?php
                $txtRarea = [ 'name'  => 'res_data[1][response][]',
                              'id'    => 'fib_1_response_1',
                              'value' => '',
                              'class' => 'data-sample-short ckeditor txtInnRes-1',
                            ];
                echo form_textarea($txtRarea);
              ?>
            </div>
          </div>
          <div class="form-group col-xs-12 p-0 mt-3">
            <label class="control-label">Feedback</label>
            <div class="col-xs-12 p-0">
              <div class="flex-row align-items-end">
                <div class="flex-col-7">
                  <?php
                    $txtFarea = [ 'name'  => 'res_data[1][feedback][]',
                                  'id'    => 'fib_1_feedback_1',
                                  'value' => '',
                                  'class' => 'data-sample-short ckeditor txtInnFb-1',
                                ];
                    echo form_textarea($txtFarea);
                  ?>
                </div>
                <div class="flex-col text-right"></div>
              </div>
            </div>
          </div>
        </div>
      </div>  
      <div id="extraInnerFibMResponseDiv_1"></div>

      <div class="col-xs-12 p-0 pb-4">
        <div id="errorLimitDiv_1" style="display: none;"></div>
        <div class="col-xs-12 col-md-6 p-0">
          <a href="javascript:void(0)" class="btn btn-warning add-inner-fib-response" data-id="1">Add inner Response</a>
        </div>
      </div>
    </div>

    <!-- Seperator -->
    <div class="col-xs-12 mt-3 mb-3 p-0 pb-2"> <div class="hr-line mb-4"></div> </div>

    <!--  FIB 2 Response -->
    <div class="col-xs-12 fib-response-panel">
      <h4 class="lblFibOuter"><i class="flaticon-checklist"></i>FIB Response 2 *</h4>
    </div>
    <div class="col-xs-12 p-0 fib-left">
      <div class="panel panel-response">
        <div class="panel-body">
          <div class="form-group col-xs-12 p-0 pt-3 up-inn-class-2">
            <label class="control-label lblInnRes-2">Response 1 *</label>
            <div class="pull-right mt--10">
              <div class="flex-row align-self-end text-right">
                <label class="checkbox-tel flex-col-auto p-0 ml-auto pt-2">  
                  <?php 
                    $opt_ans = [ 'name' => 'answer_status[]',
                                 'id'   => 'fib_2_answer_status_1',
                                 'value'=> 2,
                                 'class'=> 'ans-opt'
                                ];
                                echo form_checkbox($opt_ans); 
                  ?> Correct Response
                </label>
              </div>
            </div>
            <div class="col-xs-12 p-0 mt-3">
              <?php
                $txtRarea = [ 'name'  => 'res_data[2][response][]',
                              'id'    => 'fib_2_response_1',
                              'value' => '',
                              'class' => 'data-sample-short ckeditor txtInnRes-2',
                  ];
                echo form_textarea($txtRarea);
              ?>
            </div>
          </div>
          <div class="form-group col-xs-12 p-0 mt-3">
            <label class="control-label">Feedback</label>
            <div class="col-xs-12 p-0">
              <div class="flex-row align-items-end">
                <div class="flex-col-7">
                  <?php
                    $txtFarea = [ 'name'  => 'res_data[2][feedback][]',
                                   'id'   => 'fib_2_feedback_1',
                                   'value'=> '',
                                   'class'=> 'data-sample-short ckeditor txtInnFb-2',
                                ];
                    echo form_textarea($txtFarea);
                  ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      
      <div id="extraInnerFibMResponseDiv_2"></div>

      <div class="col-xs-12 p-0 pb-4">
        <div id="errorLimitDiv_2" style="display: none;"></div>
        <div class="col-xs-12 col-md-6 p-0">
          <a href="javascript:void(0)" class="btn btn-warning add-inner-fib-response" data-id="2">Add inner Response</a>
        </div>
      </div>
      <div class="col-xs-12 mt-3 mb-3 p-0 pb-2"> <div class="hr-line mb-4"></div> </div>
    </div>

    <div id="extraFibMResponseDiv_2"></div>

    <div id="extraOuterFibMResponseDiv"></div>
  <?php } ?>

<div class="col-xs-12 p-0 pb-4">
  <div class="col-xs-12 col-md-6 p-0">
    <a href="javascript:void(0)" class="btn btn-primary add-outer-another-fib-response">Add Another FIB Response</a>
  </div>
</div>

<div class="col-xs-12 p-0 mb-3">
  <div class="col-xs-12 col-md-6 p-0">
    <a href="javascript:void(0)" class="btn btn-warning btnPreview" data-toggle="modal" data-target="#quePreview" data-previewid="<?php echo $qid; ?>" style="display:none;">Preview Question</a>
  </div>
  <div class="col-xs-12 col-md-6 text-right p-0">
    <button type="submit" id="nextContent" class="btn btn-primary"><?php echo isset($qid) ? 'Save changes' : 'Save and next'?></button>
  </div>
</div>

<script type="text/javascript">
  
  $(document).ready(function() {            
    var config = { height: 80, allowedContent :true, toolbar : 'short'};    
      $('.ckeditor').each(function(e){        
          CKEDITOR.replace(this.id, config);
      });

    //Preview
    var getCountVal = $("[data-previewid]").attr('data-previewid');
    getCountVal>0?$("[data-previewid]").show():$("[data-previewid]").hide();

    // Validating module content form
    $("#questionContent").validate({
      ignore: [],
      rules: {        
        title: {
          required: true
        },
        question_prompt: {
          required: function(textarea) {
            CKEDITOR.instances[textarea.id].updateElement(); // update textarea
            var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
            return editorcontent.length === 0;
          }
        },
        'answer_status[]': {
          required: true,
          minlength: 1
        },
       'res_data[1][response][]': {
          required: function(textarea) {
            //CKEDITOR.instances[textarea.id].updateElement(); // update textarea
            $('textarea.ckeditor').each(function() {
              var $textarea = $(this).attr('id');
              CKEDITOR.instances[$textarea].updateElement();
            });
            var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
            return editorcontent.length === 0;            
          }
        },
        'res_data[2][response][]': {
          required: function(textarea) {
            var qtype = $("#question_type").val();            
            //CKEDITOR.instances[textarea.id].updateElement(); // update textarea
            $('textarea.ckeditor').each(function() {
              var $textarea = $(this).attr('id');
              CKEDITOR.instances[$textarea].updateElement();
            });
            var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
            return editorcontent.length === 0;
          } 
        },
        'res_data[3][response][]': {
          required: function(textarea) {            
            //CKEDITOR.instances[textarea.id].updateElement(); // update textarea
            $('textarea.ckeditor').each(function() {
              var $textarea = $(this).attr('id');
              CKEDITOR.instances[$textarea].updateElement();
            });
            var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
            return editorcontent.length === 0;            
          }
        },
        'res_data[4][response][]': {
          required: function(textarea) {            
            //CKEDITOR.instances[textarea.id].updateElement(); // update textarea
            $('textarea.ckeditor').each(function() {
              var $textarea = $(this).attr('id');
              CKEDITOR.instances[$textarea].updateElement();
            });
            var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
            return editorcontent.length === 0;            
          }
        },
        'res_data[5][response][]': {
          required: function(textarea) {           
            //CKEDITOR.instances[textarea.id].updateElement(); // update textarea
            $('textarea.ckeditor').each(function() {
              var $textarea = $(this).attr('id');
              CKEDITOR.instances[$textarea].updateElement();
            });
            var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
            return editorcontent.length === 0;           
          }
        },
        'res_data[6][response][]': {
          required: function(textarea) {
            //CKEDITOR.instances[textarea.id].updateElement(); // update textarea
            $('textarea.ckeditor').each(function() {
              var $textarea = $(this).attr('id');
              CKEDITOR.instances[$textarea].updateElement();
            });
            var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
            return editorcontent.length === 0;            
          }
        },        
      },
      messages: {
        title: { required: "Please enter question title" },
        question_prompt:{ required: "Please enter question" },
        'answer_status[]': { required: "Select at-least one correct answer" },
        'res_data[1][response][]': { required: "Please enter response" },
        'res_data[2][response][]': { required: "Please enter response" },
        'res_data[3][response][]': { required: "Please enter response" },
        'res_data[4][response][]': { required: "Please enter response" },
        'res_data[5][response][]': { required: "Please enter response" },
        'res_data[6][response][]': { required: "Please enter response" },
      },
      submitHandler: function(form) { 
          console.log('form');
          submitQuestionContentForm();
          return false;

      },
      errorPlacement: function(error, $elem) {
        if ($elem.is('textarea')) {
          $elem.insertAfter($elem.next('div'));
        }
        error.insertAfter($elem);
      }      
    });
  });  // end ready

  // Add exter outer response for question
  $(document).on('click', '.add-outer-another-fib-response', function () {

    var div_id = $("#extraOuterFibMResponseDiv .out-res").length+2;
    div_id++;
    alert(div_id);

    var inn_res_cnt = $("#extraInnerFibMResponseDiv_"+div_id+" .inn-res-"+div_id).length;
    inn_res_cnt++;      

    $("#extraOuterFibMResponseDiv").append('<div class="out-res" id="div_outer_'+div_id+'"><div class="inn-res-'+div_id+'" id="div_'+div_id+'_inn_res_'+inn_res_cnt+'"><div class="col-xs-12 fib-response-panel"><h4 class="lblFibOuter"> <i class="flaticon-checklist"></i> FIB Response '+div_id+' *</h4></div><div class="col-xs-12 p-0 fib-left"><div class="panel panel-response"><div class="panel-body"><div class="form-group col-xs-12 p-0 pt-3"><label class="control-label lblInnRes-'+div_id+'">Response '+inn_res_cnt+' *</label><div class="pull-right mt--10"><div class="flex-row align-self-end text-right"><label class="checkbox-tel flex-col-auto p-0 ml-auto pt-2"><input type="checkbox" name="answer_status[]" id ="fib_'+div_id+'_answer_status_'+inn_res_cnt+'" class="ans-opt" value="'+div_id+'"> Correct Response</label></div></div><div class="col-xs-12 p-0 mt-3"><textarea id="fib_'+div_id+'_response_'+inn_res_cnt+'" name="res_data['+div_id+'][response][]" class="data-sample-short ckeditor txtInnRes-'+div_id+'"></textarea></div></div><div class="form-group col-xs-12 p-0 mt-3"><label class="control-label">Feedback</label><div class="col-xs-12 p-0"><div class="flex-row align-items-end"><div class="flex-col-7"><textarea id="fib_'+div_id+'_feedback_'+inn_res_cnt+'" name="res_data['+div_id+'][feedback][]" class="data-sample-short ckeditor txtInnFb-'+div_id+'"></textarea></div></div></div></div></div></div><div id="extraInnerFibMResponseDiv_'+div_id+'"></div><div class="col-xs-12 p-0 pb-4"><div id="errorLimitDiv_'+div_id+'" style="display: none;"></div><div class="col-xs-12 col-md-6 p-0"><a href="javascript:void(0)" class="btn btn-warning add-inner-fib-response" data-id="'+div_id+'">Add inner Response</a></div><div class="col-xs-12 col-md-6 p-0"><div class="flex-col text-right"><a href="javascript:void(0)" class="btn btn-danger btn-md remove-outer-response" data-fibid="'+div_id+'"> <i class="flaticon-waste-bin"></i> Remove FIB </a></div></div></div></div></div></div>').ready(function() {   
      var mcqConfig = { height: 80, toolbar : 'short'}; 
        CKEDITOR.replace('fib_'+div_id+'_response_'+inn_res_cnt, mcqConfig);  
        CKEDITOR.replace('fib_'+div_id+'_feedback_'+inn_res_cnt, mcqConfig);
      });
  });

  // Remove extra outer inner responses for the question
  $(document).on('click', '.remove-outer-response', function() {
    var outdivid = $(this).data('fibid');
    var que_resp_id = $(this).data('que-resp-id');
    var que_id = $("#que_cont_id").val();

    //alert(outdivid + " == " + que_resp_id); return false;

    $.confirm({
      title: 'Confirm!',
      content: 'Are you sure?',
      buttons: {
        confirm: function() {

          $("#div_outer_"+outdivid).remove();
          var h_outdiv = outdivid - 1;
          $('#que_res'+h_outdiv).val('');
          $('#que_res'+h_outdiv).remove();

          // Re-arrange outer extra div of FIB response
          var i_outres = 3;
          $('.out-res').each(function () {
            $(this).attr('id', 'div_outer_'+i_outres);   
            $(this).children().find('#extraInnerFibMResponseDiv_'+(i_outres + 1)).attr('id', "extraInnerFibMResponseDiv_"+i_outres)
            $(this).children().find('.lblInnRes-'+(i_outres + 1)).attr('class', "lblInnRes-"+i_outres);
            $(this).children().find('.checkbox-tel input').val(i_outres);
            $(this).children().find('.checkbox-tel input').attr('id', "fib_"+i_outres+"_answer_status_1");
            $(this).find('.txtInnRes-'+(i_outres + 1)).attr('id', 'fib_'+i_outres+'_response_1');
            //$(this).find('.txtInnRes-'+(i_outres + 1)).attr('name', 'res_data['+i_outres+'][response][]');

            $(this).find('.txtInnFb-'+(i_outres + 1)).attr('id', 'fib_'+i_outres+'_feedback_1');
           // $(this).find('.txtInnRes-'+(i_outres + 1)).attr('name', 'res_data['+i_outres+'][feedback][]');
            
            /*setTimeout( function () {
              CKEDITOR.instances['fib_'+(i_outres + 1)+'_response_1'].updateElement();    
              CKEDITOR.instances['fib_'+(i_outres + 1)+'_feedback_1'].updateElement();
            }, 100 );*/


            $('.txtInnRes-'+(i_outres + 1)).each( function (){
              setTimeout( function () {
                CKEDITOR.instances['fib_'+i_outres+'_response_1'].updateElement();
              }, 100 );
            });

            $('.txtInnFb-'+(i_outres + 1)).each( function (){
              setTimeout( function () {                  
                CKEDITOR.instances['fib_'+i_outres+'_feedback_1'].updateElement();
              }, 100 );
            });

            $(this).find('a[data-id="'+(i_outres + 1)+'"]').attr('data-id', i_outres);
            $(this).find('.inn-res-'+(i_outres + 1)).attr('class', 'inn-res-'+i_outres);
            //alert('here '+i_outres); return false;


            var i_innres = 2;
            $('.inn-res-'+i_outres).each( function () {
                $(this).attr('id', 'div_'+i_outres+'_inn_res_'+i_innres); //div_4_inn_res_1
                
                $(this).find('.txtInnRes-'+(i_outres + 1)).attr('id', 'fib_'+i_outres+'_response_'+i_innres);          
                $(this).find('.txtInnFb-'+(i_outres + 1)).attr('id', 'fib_'+i_outres+'_feedback_'+i_innres);
              
                $('.txtInnRes-'+(i_outres + 1)).each( function (){
                  setTimeout( function () {
                    CKEDITOR.instances['fib_'+i_outres+'_response_'+i_innres].updateElement();
                  }, 100 );
                });

                $('.txtInnFb-'+(i_outres + 1)).each( function (){
                  setTimeout( function () {                  
                    CKEDITOR.instances['fib_'+i_outres+'_feedback_'+i_innres].updateElement();
                  }, 100 );
                });
                i_innres++; 
            });   

            $(this).find('.txtInnRes-'+(i_outres + 1)).attr('class', 'txtInnRes-'+i_outres);
            $(this).find('.txtInnFb-'+(i_outres + 1)).attr('class', 'txtInnFb-'+i_outres);
            i_outres++;
          });

          // Re-arrange lable of outer div FIB response
          var i_outFiblbl = 1;
          $('.lblFibOuter').each(function () {
            $(this).text('FIB Response '+i_outFiblbl);
            $(this).parent().siblings('div').find('a.remove-outer-response').attr('data-fibid', i_outFiblbl);
            i_outFiblbl++;
          });
          //return true;

          if(que_id && que_resp_id) {
            //return false;
            $.ajax({
                url:  "<?php echo bs();?>questions/remove_question_outer_response",      
                type:   "POST",  
                dataType: 'json',           
                data:   {'id': que_resp_id, 'qid' : que_id}, 
                beforeSend: function() {
                  //$("#imgLoader").show();
                },
                success: function(data) {
                  console.log(data);
                  if(data.code==200) {
                    $('.up-out-class .hiddenOuterinputs').html(''); 
                    var i=0;
                    $.each(data.respids, function (key, val) {  

                      $('.up-out-class .hiddenOuterinputs').append('<input type="hidden"  id="que_res'+i+'" name="resp_id_data['+i+'][id]"  value="'+val+'">');
                      alert(key +" === " + val);
                      i++;
                    });
                    CommanJS.getDisplayMessgae(data.code, data.success);
                  }else {
                    CommanJS.getDisplayMessgae(data.code, data.error);
                  }            
                }
              }); 

            return true;
          }else {
            CommanJS.getDisplayMessgae(200, 'Response removed');
            return true;
          }
        },
        cancel: function() {
          alert('2');
          return true;
        }
      }
    });  
  });



  // Add extra inner responses for question
  $(document).on('click', '.add-inner-fib-response', function() {
    var div_id = $(this).data('id'); 
    var inn_res_cnt = $("#extraInnerFibMResponseDiv_"+div_id+" .inn-res-"+div_id).length + 1;
    inn_res_cnt++;
    alert(inn_res_cnt);
    if(inn_res_cnt < 5) {
      $("#extraInnerFibMResponseDiv_"+div_id).append('<div class="inn-res-'+div_id+'" id="div_'+div_id+'_inn_res_'+inn_res_cnt+'"><div class="panel panel-response"><div class="panel-body"><div class="form-group col-xs-12 p-0 pt-3"><label class="control-label lblInnRes-'+div_id+'">Response '+inn_res_cnt+' *</label><div class="pull-right mt--10"><div class="flex-row align-self-end text-right"></div></div><div class="col-xs-12 p-0 mt-3"><textarea id="fib_'+div_id+'_response_'+inn_res_cnt+'" name="res_data['+div_id+'][response][]" class="data-sample-short ckeditor txtInnRes-'+div_id+'"></textarea></div></div><div class="form-group col-xs-12 p-0 mt-3"><label class="control-label">Feedback</label><div class="col-xs-12 p-0"><div class="flex-row align-items-end"><div class="flex-col-7"><textarea id="fib_'+div_id+'_feedback_'+inn_res_cnt+'" name="res_data['+div_id+'][feedback][]" class="data-sample-short ckeditor txtInnFb-'+div_id+'"></textarea></div><div class="flex-col text-right"><a href="javascript:void(0)" class="btn btn-danger btn-md remove-inner-response" data-inndivid="'+div_id+'" data-innresid="'+inn_res_cnt+'"><i class="flaticon-waste-bin "></i>Remove</a></div></div></div></div></div></div></div>')
        .ready(function(){    
          var mcqConfig = { height: 80, toolbar : 'short'};            
          CKEDITOR.replace('fib_'+div_id+'_response_'+inn_res_cnt, mcqConfig);  
          CKEDITOR.replace('fib_'+div_id+'_feedback_'+inn_res_cnt, mcqConfig);

          CKEDITOR.instances['fib_'+div_id+'_response_'+inn_res_cnt].updateElement();    
          CKEDITOR.instances['fib_'+div_id+'_feedback_'+inn_res_cnt].updateElement();
        });  
    }else { 
      $("#errorLimitDiv_"+div_id).show();
      $("#errorLimitDiv_"+div_id).html("<div class='error'>Only four responses is allowed.</div>");
      return false;
    }
  });    

  // Remove innner responses
  $(document).on('click', '.remove-inner-response', function() {
    var inndivid = $(this).data('inndivid');
    var innresid = $(this).data('innresid');
    var que_multi_resp_id = $(this).data('multi-resp-id');  // Question multiple response id
    var que_id = $("#que_cont_id").val(); // Question id
    var que_resp_id = $(this).data('que-resp-id');

    alert(inndivid + " = " + innresid + " = " + que_resp_id+ " = " + que_multi_resp_id); 
    //return false;

    $.confirm({
      title: 'Confirm!',
      content: 'Are you sure?',
      buttons: {
        confirm: function() { 

          $("#div_"+inndivid+"_inn_res_"+innresid).remove();
          // Re-arrange inner div
          var i_res = 2;
          $('.inn-res-'+inndivid).each(function () {
            $(this).attr('id', 'div_'+inndivid+'_inn_res_'+i_res);
            setTimeout( function () {
              CKEDITOR.instances['fib_'+inndivid+'_response_'+i_res].updateElement(); 
              CKEDITOR.instances['fib_'+inndivid+'_feedback_'+i_res].updateElement();
            }, 100 );
            i_res++;
          });

          //Re-arranage innner labels
          var lb_i=1;
          $('.lblInnRes-'+inndivid).each(function () {
            $(this).text('Response '+lb_i);
            $(this).parent().siblings('div').find('a.remove-inner-response').attr('data-innresid', lb_i);
            lb_i++;
          });

          // Re-arranage inner response
          var resp_i=1;                     
          $('.txtInnRes-'+inndivid).each(function(){
            $(this).attr('id', 'fib_'+inndivid+'_response_'+resp_i); 
            CKEDITOR.instances['fib_'+inndivid+'_response_'+resp_i].updateElement(); 
            resp_i++; 
          });

          var fb_i=1;                     
          $('.txtInnFb-'+inndivid).each(function(){
            $(this).attr('id', 'fib_'+inndivid+'_feedback_'+fb_i);
            CKEDITOR.instances[ 'fib_'+inndivid+'_feedback_'+fb_i].updateElement(); 
            fb_i++; 
          });

          if(que_multi_resp_id && que_resp_id) {

            $.ajax({
                url:  "<?php echo bs();?>questions/remove_question_inner_response",      
                type:   "POST",  
                dataType: 'json',           
                data:   {'id': que_multi_resp_id, 'question_responses_id' : que_resp_id, 'qid' : que_id}, 
                beforeSend: function() {
                  //$("#imgLoader").show();
                },
                success: function(data) {
                  console.log(data.multirespids);
                  if(data.code==200) {
                    $('.up-inn-class-'+inndivid+' .hiddeninputs').html(''); 
                    var i=0;
                    $.each(data.multirespids, function (key, val) {  
                      
                      $('.up-inn-class-'+inndivid+' .hiddeninputs').append('<input type="hidden" name="res_data['+inndivid+'][mresid]['+key+']"  value="'+val+'">');

                      alert(key +"  ==  "+val);
                      i++;
                    });
                    CommanJS.getDisplayMessgae(data.code, data.success);
                  }else {
                    CommanJS.getDisplayMessgae(data.code, data.error);
                  }            
                }
              }); 

            return true;
          }else {
            CommanJS.getDisplayMessgae(200, 'Response removed');
            return true;
          }
          
        },
        cancel: function() {
          alert('2');
          return true;
        }
      }
    });
  });

</script>