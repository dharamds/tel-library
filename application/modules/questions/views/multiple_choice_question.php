<!-- MCQ -->

<!-- Title -->
    <div class="row mt-4 mb-3">
      <div class="col-sm-4">
        <div class="form-group">
          <label for="title" class="control-label"> Title <span class="field-required">*</span> </label>
          <?php
            $title = isset($queData->title) ? $queData->title : '';
            echo form_input([
              'id'    => 'title',
              'name'  => 'title',
              'value' => $title,
              'class' => 'form-control'
            ]);
          ?>
        </div>
      </div>
      <!-- Long Title  -->
      <div class="col-sm-8">
        <div class="form-group">
          <label for="long_title" class="control-label"> Long Title </label>
          <?php
            $long_title = isset($queData->long_title) ? $queData->long_title : '';
            echo form_input([
              'id'    => 'long_title',
              'name'  => 'long_title',
              'value' => $long_title,
              'class' => 'form-control'
            ]);
          ?>
        </div>
      </div>
    </div>

    <!-- Question or prompt -->
    <div class="row mt-4 mb-3">
      <div class="col-sm-8">
        <div class="form-group">
          <label for="question_prompt" class="control-label">Question or prompt <span class="field-required">*</span></label>
          <?php
            $question_prompt = isset($queData->question_prompt) ? $queData->question_prompt : '';
          ?>
          <?php
            echo form_textarea([
              'name'  => 'question_prompt',
              'id'    => 'question_prompt',
              'value' => $question_prompt,
              'class' => 'form-control ckeditor',
              'rows'  => '5'
            ]);
          ?>
        </div>
      </div>
    </div>
<?php 
if(!$responseData) { ?>   
    <!-- Question type oriented view -->
    <div class="col-xs-12 p-0">
      <div class="panel panel-response">
        <div class="panel-body">
          <div class="form-group col-xs-12 p-0 pt-3 up-class">
            <div class="hiddeninputs"></div>
              <label class="control-label lblRes">Response 1 *</label>
              <div class="pull-right mt--10">
                <div class="flex-row align-self-end text-right">
                  <label class="radio-tel flex-col-auto p-0 ml-auto pt-2">  
                    <?php 
                      $opt_ans = [ 'name'   => 'answer_status[]',
                                   'id'     => 'answer_status_1',
                                   'value'  => 1,
                                   'class'  => 'ans-opt'
                                  ];
                      echo form_radio($opt_ans); 
                    ?> Correct Response
                  </label>
                  <div class="form-group flex-row flex-col-5 p-0 m-0 ml-5">
                    <label class="control-label flex-col-5 p-0 pt-2">Weight %</label>
                    <div class="flex-col-5 p-0 m-0 ml-4">
                      <?php 
                        $txt_weight = [ 'name'  => 'weight[]',
                                        'id'    => 'weight_1',
                                        'value' => 0,
                                        'class' => 'touchspin4 selWeight'
                                  ];
                          echo form_input($txt_weight); 
                      ?>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 p-0 mt-3 ">
                <?php
                  $txtRarea = [ 'name'    => 'response[]',
                                 'id'     => 'response_1',
                                 'value'  => '',
                                 'class'  => 'data-sample-short ckeditor txtres',
                                ];
                  echo form_textarea($txtRarea);
                ?>
              </div>
            </div>
            <div class="form-group col-xs-12 p-0 mt-3 ">
              <label class="control-label">Feedback</label>
              <div class="col-xs-12 p-0">
                <div class="flex-row align-items-end">
                  <div class="flex-col-6">
                    <?php
                      $txtFarea = [ 'name'    => 'feedback[]',
                                     'id'     => 'feedback_1',
                                     'value'  => '',
                                     'class'  => 'data-sample-short ckeditor txtfb',
                                  ];
                      echo form_textarea($txtFarea);
                    ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    <div class="col-xs-12 p-0">
      <div class="panel panel-response">
        <div class="panel-body">
          <div class="form-group col-xs-12 p-0 pt-3">
            <label class="control-label lblRes">Response 2 *</label>
            <div class="pull-right mt--10">
              <div class="flex-row align-self-end text-right">
                <label class="radio-tel flex-col-auto p-0 ml-auto pt-2">
                <?php 
                  $opt_ans = [ 'name'  => 'answer_status[]',
                                'id' => 'answer_status_2',
                                'value' => 2,
                                'class' => 'ans-opt'
                              ];
                                echo form_radio($opt_ans); 
                ?> Correct Response
                </label>
                <div class="form-group flex-row flex-col-5 p-0 m-0 ml-5">
                  <label class="control-label flex-col-5 p-0 pt-2">Weight %</label>
                  <div class="flex-col-5 p-0 m-0 ml-4">                
                    <?php 
                      $txt_weight = [ 'name'    => 'weight[]',
                                        'id'    => 'weight_2',
                                        'value' => 0,
                                        'class' => 'touchspin4 selWeight'
                                    ];
                      echo form_input($txt_weight); 
                    ?>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xs-12 p-0 mt-3 ">
              <?php
                $txtFarea2 = [ 'name' => 'response[]',
                     'id' => 'response_2',
                     'value' => '',
                     'class' => 'data-sample-short ckeditor txtres',
                    ];
                echo form_textarea($txtFarea2);
              ?>
            </div>
          </div>

          <div class="form-group col-xs-12 p-0 mt-3 ">
            <label class="control-label">Feedback</label>
            <div class="col-xs-12 p-0">
              <div class="flex-row align-items-end">
                <div class="flex-col-6">
                  <?php
                    $txtRarea2 = [ 'name'   => 'feedback[]',
                                    'id'    => 'feedback_2',
                                    'value' => '',
                                    'class' => 'data-sample-short ckeditor txtfb',
                                  ];
                    echo form_textarea($txtRarea2);
                  ?>
                </div>
              </div>
            </div>
          </div>        
        </div>
      </div>
    </div>
    <!-- For extra responses -->
    <div id="extraMcqResponseDiv"></div>

<?php } else { 
  echo '<div id="extraMcqResponseDiv">';
  $cnt=1; $c=0;
  foreach ($responseData as $reskey => $resvalue) { ?>
    <?php if($cnt>2) 
      echo '<div class="res" id="res_'.$cnt.'">';
    ?>  
    
    <div class="col-xs-12 p-0">
      <div class="panel panel-response">
        <div class="panel-body">
          <div class="form-group col-xs-12 p-0 pt-3 up-class">
            <div class="hiddeninputs">
              <?php 
                $hid_resp_opt = [  'type'  => 'hidden', 
                                  'name' => 'res_data['.$c.'][id]',
                                  'id'   => 'que_res'.$c,
                                  'value' => $resvalue->id
                              ];
                echo form_input($hid_resp_opt); ?> 
            </div>
                <label class="control-label lblRes">Response <?php echo $cnt; ?> *</label>
                <div class="pull-right mt--10">
              <div class="flex-row align-self-end text-right">
                    <label class="radio-tel flex-col-auto p-0 ml-auto pt-2">
                <?php
                  $ans_status =$resvalue->answer_status;$chk = '';
                  if($ans_status==1)  $chk = 'checked';
                ?>
                <?php 
                  $opt_ans = [
                                   'name'  => 'answer_status[]',
                                   'id' => 'answer_status_'.$cnt,
                                   'value' => $cnt,
                                   'class' => 'ans-opt',
                                    'checked' => $chk                                
                                  ];
                                echo form_radio($opt_ans); 
                ?> Correct Response
                </label>
                <div class="form-group flex-row flex-col-5 p-0 m-0 ml-5">
                  <label class="control-label flex-col-5 p-0 pt-2">Weight %</label>
                  <div class="flex-col-5 p-0 m-0 ml-4">
                  <?php 
                    $txt_weight = [ 'name'  => 'weight[]',
                                              'id'  => 'weight_'.$cnt,
                                              'value' => $resvalue->weight,
                                              'class' => 'touchspin4 selWeight'
                                          ];
                                    echo form_input($txt_weight); 
                  ?>
                  </div>
                </div>
              </div>
                </div>
                <div class="col-xs-12 p-0 mt-3 ">
                <?php
              $txtFarea2 = [ 'name' => 'response[]',
                     'id' => 'response_'.$cnt,
                     'value' => $resvalue->response,
                     'class' => 'data-sample-short ckeditor txtres',
                    ];
              echo form_textarea($txtFarea2);
            ?>
                </div>
          </div>
          <div class="form-group col-xs-12 p-0 mt-3 ">
                <label class="control-label">Feedback</label>
                <div class="col-xs-12 p-0">
                  <div class="flex-row align-items-end">
                    <div class="flex-col-6">
                <?php
                  $txtRarea2 = [ 'name'   => 'feedback[]',
                           'id'   => 'feedback_'.$cnt,
                           'value'  => $resvalue->feedback,
                           'class'  => 'data-sample-short ckeditor txtfb',
                        ];
                  echo form_textarea($txtRarea2);
                ?>
                    </div>
                    <!-- test -->
                    <?php if($cnt>2) {?>
                      <div class="flex-col text-right" id="rem_<?php echo $cnt;?>">
                        <a href="javascript:void(0)" class="btn btn-danger btn-md remove-response" data-resid="<?php echo $cnt;?>" data-queresid="<?php echo $resvalue->id; ?>"><i class="flaticon-waste-bin"></i> Remove</a>
                      </div>
                    <?php } ?>
                  </div>
                </div>
          </div>
        </div>
      </div>
    </div>  
  <?php
  if($cnt>2) 
    echo '</div>';    
  $cnt++; $c++;
  } // end foreach
  echo '</div>';
} //end if ?>

<div class="col-xs-12 p-0">
  <div class="col-xs-12 col-md-7 p-0 pb-3">
    <a href="javascript:void(0)" class="btn btn-primary add-another-response">Add Another Response</a>
  </div>
</div>  
<div class="col-xs-12 mt-2 mb-3 p-0">  <div class="e mb-4"></div></div>

<!-- Randomize Responses -->
<div class="col-xs-12 p-0">
  <label class="checkbox-tel flex-col-auto p-0">
    <?php
      $rchk = '';
      if($responseData) {
        $random_status = ($queData->random_option) ? 1 : 0; 
        if($random_status==1)  $rchk = 'checked';
      }
      $opt_random = [ 'name'    => 'option_rand',
                      'id'      => 'option_rand',
                      'value'   => 1,
                      'checked' => $rchk
                    ];
        echo form_checkbox($opt_random); 
    ?>Randomize Responses
  </label>
</div>

<div class="col-xs-12 mt-3 mb-3 p-0 pb-2"> <div class="hr-line mb-4"></div> </div>

<div class="col-xs-12 p-0 mb-3">
  <div class="col-xs-12 col-md-6 p-0">
    <a href="javascript:void(0)" class="btn btn-warning btnPreview" data-toggle="modal" data-target="#quePreview" data-previewid="<?php echo $qid; ?>" style="display:none;">Preview Question</a>
  </div>
  <div class="col-xs-12 col-md-6 text-right p-0">
    <button type="submit" id="nextContent" class="btn btn-primary"><?php echo isset($qid) ? 'Save changes' : 'Save and next'?></button>
  </div>
</div>

<!-- -------------------------------------- Preview Modal ----------------------------------- -->
<div class="modal fade modals-tel-theme" id="quePreview" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog w60p">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h2 class="modal-title">
          Question Preview
        </h2>
      </div>
      <div class="modal-body" id="que_details">
        
      </div>
      <div class="col-xs-12 mt-3 mb-3">
        <div class="hr-line"></div>
      </div>
      <div class="modal-footer text-right">
        <button type="button" class="btn btn-danger btn-md" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- -------------------------------------- Preview Modal ends ----------------------------------- -->
<script type="text/javascript">

  $(document).ready(function() {
    //ck editor
    var config = { height: 80, allowedContent :true, toolbar : 'short'};    
    $('.ckeditor').each(function(e) {
      CKEDITOR.replace( this.id, config);
    });

    //Preview
    var getCountVal = $("[data-previewid]").attr('data-previewid');
    getCountVal>0?$("[data-previewid]").show():$("[data-previewid]").hide();
    // Check uncheck of radio button
    $('.ans-opt').on('ifChecked', function (event) {
      $('.ans-opt').iCheck('uncheck');
      $(this).prop('checked', true);
    }); 
    
    //Touchspin for weight
    $("input.touchspin4").TouchSpin({
      verticalbuttons: true,
      step: 5,
      min : 0,
      max : 100
    });

    // Validating module content form
    $("#questionContent").validate({
      ignore: [],
      rules: {        
        title: {
          required: true
        },
        question_prompt: {
          required: function(textarea) {
            CKEDITOR.instances[textarea.id].updateElement(); // update textarea
            var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
            return editorcontent.length === 0;
          }
        },
        'answer_status[]': {
          required: true,
          minlength: 1
        },
        'weight[]': {
          required: function() {
            var qtype = $("#question_type").val();
            if (qtype == 1 || qtype == 3) {
              return true
            }
          },
          minlength: 1
        },
        'response[]': {
          required: function(textarea) {
            var qtype = $("#question_type").val();
            if (qtype == 5 || qtype == 1 || qtype == 2 || qtype == 3) {
              //CKEDITOR.instances[textarea.id].updateElement(); // update textarea
              $('textarea.ckeditor').each(function() {
                var $textarea = $(this).attr('id');
                CKEDITOR.instances[$textarea].updateElement();
              });
              var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
              return editorcontent.length === 0;
            } else return 1;
          }
        },
      },
      messages: {
        title: { required: "Please enter question title" },
        question_prompt:{ required: "Please enter question" },
        'answer_status[]': { required: "Select at-least one correct answer" },
        'weight[]': { required: "Enter at-least one weight" },
        'response[]': { required: "Please enter response" },
      },
      submitHandler: function(form) { 
          console.log('form');
          submitQuestionContentForm();
          return false;

      },
      errorPlacement: function(error, $elem) {
        if ($elem.is('textarea')) {
          $elem.insertAfter($elem.next('div'));
        }
        error.insertAfter($elem);
      }      
    });

  }); // end document ready


  // Add extra responses for question
  $(document).on('click', '.add-another-response', function() {
    var res_cnt = $("#extraMcqResponseDiv .res").length + 2;
    res_cnt++;
    //alert(res_cnt);
    $("#extraMcqResponseDiv").append('<div class="res" id="res_'+res_cnt+'"><div class="col-xs-12 p-0"><div class="panel panel-response"><div class="panel-body"><div class="form-group col-xs-12 p-0 pt-3"><label class="control-label lblRes">Response '+res_cnt+' *</label><div class="pull-right mt--10"><div class="flex-row align-self-end text-right"><label class="radio-tel flex-col-auto p-0 ml-auto pt-2"><input type="radio" id="answer_status_'+res_cnt+'" value="'+res_cnt+'" name="answer_status[]" class="ans-opt"> Correct Response</label><div class="form-group flex-row flex-col-5 p-0 m-0 ml-5"><label class="control-label flex-col-5 p-0 pt-2">Weight %</label><div class="flex-col-5 p-0 m-0 ml-4"><input name="weight[]" id="weight_'+res_cnt+'" class="touchspin4 selWeight" value="0"></div></div></div></div><div class="col-xs-12 p-0 mt-3 "><textarea name="response[]"  id="response_'+res_cnt+'" class="data-sample-short ckeditor txtres"></textarea></div></div><div class="form-group col-xs-12 p-0 mt-3"><label class="control-label">Feedback</label><div class="col-xs-12 p-0"><div class="flex-row align-items-end"><div class="flex-col-6 "><textarea id="feedback_'+res_cnt+'" name="feedback[]" class="data-sample-short ckeditor txtfb"></textarea></div><div class="flex-col text-right" id="rem_'+res_cnt+'"><a href="javascript:void(0)" class="btn btn-danger btn-md remove-response" data-resid="'+res_cnt+'"><i class="flaticon-waste-bin"></i> Remove</a></div></div></div></div></div></div></div></div>')
      .ready(function(){
        $("input.touchspin4").TouchSpin({
          verticalbuttons: true,
          step: 5,
          min : 0,
          max : 100
        });

        // Check uncheck of radio button
       /* $('.ans-opt').on('ifChecked', function (event) {
          $('.ans-opt').iCheck('uncheck');
          $(this).prop('checked', true);
        });*/
        
        // load ckeditor
        var mcqConfig = { height: 80, toolbar : 'short'};           
        CKEDITOR.replace('response_'+res_cnt, mcqConfig);         
        CKEDITOR.replace('feedback_'+res_cnt, mcqConfig);
        CKEDITOR.instances['response_'+res_cnt].updateElement();    
        CKEDITOR.instances['feedback_'+res_cnt].updateElement();
    });  
  }); // ends Add extra responses for question

  // Remove response 
  $(document).on('click', '.remove-response', function() {
    var resdiv = $(this).data('resid');
    var que_res_id = $(this).attr('data-queresid');
    var que_id = $("#que_cont_id").val();
    //alert(que_res_id); return false;
      $.confirm({
      title: 'Confirm!',
      content: 'Are you sure?',
      buttons: {
        confirm: function() {             
          var mcqConfig = {height: 80, toolbar : 'short'};

          var h_resdiv = resdiv - 1;
          $('#que_res'+h_resdiv).val('');
          $('#que_res'+h_resdiv).remove();
          $("#res_"+resdiv).remove();
        
          var resp_i=1;                     
          $('.txtres').each(function(){
            $(this).attr('id', 'response_'+resp_i); 
            CKEDITOR.instances['response_'+resp_i].updateElement(); 
            resp_i++; 
          });

          var ansopt_i=1;                     
          $('.ans-opt').each(function(){
            $(this).attr('id', 'id="answer_status_'+ansopt_i);
            $(this).val(ansopt_i);
            ansopt_i++; 
          });

          var wght_i=1;
          $('.selWeight').each(function(){
            $(this).attr('id', 'id="weight_'+wght_i);
            wght_i++; 
          });

          var fb_i=1;                     
          $('.txtfb').each(function(){
            $(this).attr('id', 'feedback_'+fb_i);
            //CKEDITOR.replace('feedback_'+fb_i, mcqConfig);
            CKEDITOR.instances['feedback_'+fb_i].updateElement(); 
            fb_i++; 
          });

          var lb_i=1;
          $('.lblRes').each(function () {
            $(this).text('Response '+lb_i);
            $(this).parent().siblings('div').find('a.remove-response').attr('data-resid', lb_i);
            lb_i++;
          });

          var i_res = 3;
          $('.res').each(function () {
            //alert(i_res);
            $(this).attr('id', 'res_'+i_res);
            setTimeout( function () {
              CKEDITOR.instances['response_'+i_res].updateElement(); 
              CKEDITOR.instances['feedback_'+i_res].updateElement();
            }, 100 );
            i_res++;
          });

          if(que_res_id) {
            
            $.ajax({
                url:  "<?php echo bs();?>questions/remove_question_response",      
                type:   "POST",  
                dataType: 'json',           
                data:   {'id': que_res_id, 'qid' : que_id}, 
                beforeSend: function() {
                  //$("#imgLoader").show();
                },
                success: function(data) {
                  
                  if(data.code==200) {
                    $('.up-class .hiddeninputs').html(''); 
                    var i=0;
                    $.each(data.respids, function (key, val) {  
                      $('.up-class .hiddeninputs').append('<input type="hidden" name="res_data['+i+'][id]" id="que_res'+i+'" value="'+val+'">');
                      i++;
                    }); 
                    CommanJS.getDisplayMessgae(data.code, data.success);
                  }else {
                    CommanJS.getDisplayMessgae(data.code, data.error);
                  }            
                }
              }); 
              return true;
            }else {

              CommanJS.getDisplayMessgae(200, 'Response removed');return true;
            }
          },
          cancel: function() {
            return true;
          }
        }
      });
  });


  // Ajax request to fetch data for preview
  $(document).on('click' , '.btnPreview', function() {
    var que_id = $("#que_cont_id").val();
    var qtype = $('#question_type').val();
   // alert(que_id + " = " + qtype); return false;
    $.ajax({
        url:  "<?php echo bs();?>questions/get_question_details",      
        type:   "POST",  
        dataType: 'json',           
        data: {'qid' : que_id, 'qtype' : qtype},
        success: function(data) {
          console.log(data);     
          var que_opts = '<div class="question-content p-4 pl-5">';

          $.each(data.responses, function (key, val) { 
            var chk=""; 
            if(data.ansstatus[key]==1) chk = 'checked'; else chk = '';

            que_opts = que_opts +'<div><label class="checkbox-tel pb-2"><input type="radio" name="ans" class="mr-2" '+chk+'>'+val+'</label></div>';            
          }); 
          que_opts = que_opts +'</div';
          var que_title = '<div class="question-panel p-5"><h4><i class="flaticon-question-mark mr-3"></i>'+data.title+'</h4>'+que_opts+'</div>'; 

          $('#que_details').html(que_title); 
        }
      }); 
  });

  // Changes weight value as per correction answer sets
  $(document).on('click', '.ans-opt', function () {
    if ( $(this).is(':checked') ) {
      var ans_opt_id = $(this).attr('id');
      var res_ans_opt = ans_opt_id.split('_');
        var c = res_ans_opt[2];
        var i=1;
        $('.selWeight').each( function () {
          var wt_val  = $("#weight_"+i).val();
          if(wt_val==100)
            $("#weight_"+i).val(0);
          i++;
        });
        $("#weight_"+c).val('100');
      }
  });
</script>