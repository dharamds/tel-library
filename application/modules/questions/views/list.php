<div class="container-fluid">
    <div data-widget-group="group1">
        <div class="row">
            <div class="col-md-12">
                <div class="filter-container flex-row" id="toggleFilterSection">
                    <div class="flex-col-12 flex-col-sm-12 flex-row m-0">
                        <div class="flex-col-sm-11 flex-row">
                            <div class="flex-col pl-0" id="selected_system_cat"> </div>
                            <div class="flex-col" id="selected_system_tag"></div>
                            <div class="flex-col" id="selected_assessment_cat"></div>
                            <div class="flex-col" id="selected_assessment_tag"></div>
                            <div class="flex-row m-0 p-0 pt-4 flex-col-12" style="display: none" id="toggleFilterSectionPanel">
                                <div class="flex-col-auto pb-4 pl-0" id="selected_tel_matery_stds"></div>
                                <div class="flex-col-auto pb-4" id="selected_outcomes_cat"></div>
                                <div class="flex-col-auto pb-4" id="selected_tel_matery_points"></div>
                                <div class="flex-col-sm-1">
                                    <a class="btn btn-danger pt-2" id="reset_form" href="javascript:void(0)"><i class="flaticon-close-1 f21 fw100"></i></a>
                                </div>
                            </div>
                        </div>

                        <div class="flex-col-12 flex-col-sm-1">
                            <button type="button" class="btn btn-default btn-block btn-arrow" data-panel-toggle="toggleFilterSectionPanel">
                                <span class="flaticon-back"></span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default panel-grid">
                    <div class="panel-heading">
                        <a class="btn btn-primary" href="<?= base_url('questions/add') ?>">New</a>
                        <div class="panel-ctrls"></div>
                    </div>
                    <div class="panel-body pl-0 pr-0">
                        <div class="row m-0">
                            <div class="col-md-12">
                                <table id="questionListTable" class="table table-bordered table-striped table-hover" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th style="min-width: 40px;">
                                                <label class="checkbox-tel">
                                                    <input type="checkbox" name="popup_select_all" id="popup_select_all" class="mr-2 popup_select_all">
                                                </label>
                                            </th>
                                            <th style="min-width: 100px;">Actions</th>
                                            <th style="min-width: 100px;">Status</th>
                                            <th style="min-width: 150px;">Title</th>
                                            <th style="min-width: 200px;">Long Title</th>
                                            <th style="min-width: 180px;">Assessment Categories</th>
                                            <th style="min-width: 150px;">Assessment Tags</th>
                                            <th style="min-width: 150px;">System Categories</th>
                                            <th style="min-width: 110px;">System Tags</th>
                                            <th style="min-width: 200px;">Learning Outcomes</th>
                                            <th style="min-width: 200px;">TEL Mastery Standards</th>
                                            <th style="min-width: 200px;">Mastery Points</th>
                                            <th style="min-width: 200px;">Used in the quizzes</th>
                                            <th style="min-width: 100px;">Created By</th>
                                            <th style="min-width: 100px;">Created</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#reset_form').click(function() {
            $(".meta_data_filter").prop("checked", false);
            $(".selectFilterMode span").text('');
            table.search('').draw();
        });

        $(document).on('change', '.metadata_selector', function() {
            table.search('').draw();
        });

        CommanJS.get_metadata_options("System category", 1, 1, "selected_system_cat");
        CommanJS.get_metadata_options("System tag", 2, 1, "selected_system_tag");
        CommanJS.get_metadata_options("Assessment category", 1, 9, "selected_assessment_cat");
        CommanJS.get_metadata_options("Assessment tag", 2, 9, "selected_assessment_tag");
        CommanJS.get_metadata_options("Learning outcomes", 1, 14, "selected_outcomes_cat");
        CommanJS.get_metadata_options("TEL Mastery standards", 1, 15, "selected_tel_matery_stds");
        CommanJS.get_metadata_options("Mastery points", 1, 25, "selected_tel_matery_points");

        //CommanJS.get_metadata_options("Blooms level", 1, 18, "selected_blooms_level");

        var table = $('#questionListTable').DataTable({
            // Processing indicator
            "processing": true,
            // DataTables server-side processing mode
            "serverSide": true,
            // Initial no order.
            "iDisplayLength": 10,
            "bPaginate": true,
            "scrollX": true,
            "autoWidth": false,
            "order": [],
            "drawCallback": function(settings) {
                $('.load_meta_data').each(function(key, item) {
                    $.getJSON("<?php echo bs('questions/get_meta_tags/'); ?>" + $(this).attr('id'), function(data) {
                        if (data) $("#" + data.typeid).html(data.value);
                    });
                });

                $('.load_meta_category').each(function(key, item) {
                    $.getJSON("<?php echo bs('questions/get_meta_categories/'); ?>" + $(this).attr('id'), function(data) {
                           if(data) $("#"+data.typeid).html(data.value);
                    });
				});
                
                $('.load_quiz_data').each(function(key, item) {
                    $.getJSON("<?php echo bs('questions/get_question_used/'); ?>" + $(this).attr('id'), function(data) {
                        if (data) $("#" + data.typeid).html(data.value);
                    });
                });
                $('.load_module_users').each(function(key, item) {
                    $.getJSON("<?php echo bs('users/get_created_by/questions/'); ?>" + $(this).attr('id'), function(data) {
                        if (data) $("#" + data.typeid).html(data.value);
                    });
                });
            },
            // Load data from an Ajax source
            "ajax": {
                "url": "<?php echo bs('questions/getLists'); ?>",
                "type": "POST",
                "data": function(data) {
                    data.sectionID = 0;
                    data.system_category = CommanJS.getMetaCallBack('system_category');
                    data.system_tag = CommanJS.getMetaCallBack('system_tag');
                    data.assessment_category = CommanJS.getMetaCallBack('assessment_category');
                    data.assessment_tag = CommanJS.getMetaCallBack('assessment_tag');
                    data.learning_outcomes = CommanJS.getMetaCallBack('learning_outcomes');
                    data.learning_standards = CommanJS.getMetaCallBack('tel_mastery_standards');
                    data.tel_mastery_points = CommanJS.getMetaCallBack('tel_mastery_points');
                    //data.blooms_level = CommanJS.getMetaCallBack('blooms_level'); 
                    $(window).scrollTop(0);

                },
            },
            //Set column definition initialisation properties
            "columnDefs": [{
                    "targets": [0],
                    "data": "sr_no",
                    "orderable": false,
                    "data": null,
                    render: function(data, type, row, meta) {
                        if (type === 'display') {
                            data = '<label class="checkbox-tel"><input type="checkbox" class="question_select" name="question_select" value="' + row['id'] + '" class="mr-2"></label>';
                        }
                        return data;
                    },
                    "autoWidth": true
                },
                {
                    "targets": 1,
                    "data": null,
                    "orderable": false,
                    "render": function(data, type, full, meta) {
                        if (type === 'display') {
                            data = '';
                            <?php if ($this->ion_auth->is_admin() || $permissions->edit_per) : ?>
                            data = '<a class="btn btn-primary btn-sm" href="<?php echo base_url('questions/add/') ?>' + full['id'] + '" > <i class="ti ti-pencil"></i> </a>';
                            <?php
                            endif;
                            //if ($this->ion_auth->is_admin() || $permissions->view_per) :
                            ?>
                            /*data += '<a class="btn btn-midnightblue-alt btn-sm" href="< ?php echo base_url('courses/detail/') ?>' + full['id'] + '"> <i class="ti ti-eye"></i> </a>';*/
                            //   data += '<a class="btn btn-info btn-sm" href="#"> <i class="ti ti-eye"></i> </a>';
                            <?php
                            //  endif;
                            if ($this->ion_auth->is_admin() || $permissions->delete_per) :
                                ?>
                            data += '<a class="btn btn-danger btn-sm delete_item" href="<?php echo base_url('questions/delete/') ?>' + full['id'] + '"> <i class="ti ti-trash"></i> </a>';
                            <?php endif; ?>
                        }
                        return data;
                    }
                },
                {
                    "targets": [2],
                    "data": null,
                    "render": function(data, type, full, meta) {
                        if (type === 'display') {
                            if (full['status'] == '1') {
                                data = '<a href="<?php bs('questions/update_status/') ?>' + full['id'] + '/deactivate" data-toggle="tooltip" data-placement="top" title="Click to Change Status" class="btn btn-success btn-xs btn-round-25 btn-block change_status">Active</a>';
                            } else {
                                data = '<a href="<?php bs('questions/update_status/') ?>' + full['id'] + '/activate" data-toggle="tooltip" data-placement="top" title="Click to Change Status" class="btn btn-danger btn-xs btn-round-25 btn-block active_btn change_status">Inactive</a>';
                            }
                        }
                        return data;
                    }
                },
                {
                    "targets": [5],
                    "data": null,
                    "render": function(data, type, full, meta) {
                        if (type === 'display') {
                            data = '<span id="' + full['id'] + '_<?php echo META_QUESTION; ?>_<?php echo META_ASSESSMENT; ?>_assessment_cats" data-type="assessment_cats" class="load_meta_category"> Loading...</span>';
                        }
                        return data;
                    }
                },
                {
                    "targets": [6],
                    "data": null,
                    "render": function(data, type, full, meta) {
                        if (type === 'display') {
                            data = '<span id="' + full['id'] + '_<?php echo META_QUESTION; ?>_<?php echo META_ASSESSMENT; ?>_assessment_tags" data-type="assessment_tags" class="load_meta_data"> Loading...</span>';
                        }
                        return data;
                    }
                },
                {
                    "targets": [7],
                    "data": null,
                    "render": function(data, type, full, meta) {
                        if (type === 'display') {
                            data = '<span id="' + full['id'] + '_<?php echo META_QUESTION; ?>_<?php echo META_SYSTEM; ?>_system_cat" data-type="system_cat" class="load_meta_category"> Loading...</span>';
                        }
                        return data;
                    }
                },
                {
                    "targets": [8],
                    "data": null,
                    "render": function(data, type, full, meta) {
                        if (type === 'display') {
                            data = '<span id="' + full['id'] + '_<?php echo META_QUESTION; ?>_<?php echo META_SYSTEM; ?>_system_tags" data-type="system_tags" class="load_meta_data"> Loading...</span>';
                        }
                        return data;
                    }
                },
                {
                    "targets": [9],
                    "data": null,
                    "render": function(data, type, full, meta) {
                        if (type === 'display') {
                            data = '<span id="' + full['id'] + '_<?php echo META_QUESTION; ?>_<?php echo META_LEARNING_OUTCOMES; ?>_learning_outcomes" data-type="learning_outcomes" class="load_meta_category"> Loading...</span>';
                        }
                        return data;
                    }
                },
                {
                    "targets": [10],
                    "data": null,
                    "render": function(data, type, full, meta) {
                        if (type === 'display') {
                            data = '<span id="' + full['id'] + '_<?php echo META_QUESTION; ?>_<?php echo META_LEARNING_STANDARDS; ?>_learning_standards" data-type="learning_standards" class="load_meta_category"> Loading...</span>';
                        }
                        return data;
                    }
                },
                {
                    "targets": [13],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                            data = '<span id="' + full['id'] + '_users"  class="load_module_users"> Loading...</span>';
                        }
                        return data;
                    }
                },
                {
                    "targets": [12],
                    "data": null,
                    "orderable": false,
                    "render": function(data, type, full, meta) {
                        if (type === 'display') {
                            data = '<span id="' + full['id'] + '_quiz"  class="load_quiz_data"> Loading...</span>';
                        }
                        return data;
                    }
                }
            ],
            "columns": [{
                    "data": "id",
                    "autoWidth": true
                },
                {
                    "data": null,
                    "autoWidth": true
                },
                {
                    "data": null,
                    "autoWidth": true
                },
                {
                    "data": "title",
                    "autoWidth": true
                },
                {
                    "data": "long_title",
                    "autoWidth": true
                },
                {
                    "data": "assesment_category",
                    "autoWidth": true
                },
                {
                    "data": "assessment_tags",
                    "autoWidth": true
                },
                {
                    "data": "system_category",
                    "autoWidth": true
                },
                {
                    "data": "system_tags",
                    "autoWidth": true
                },
                {
                    "data": "outcomes_category",
                    "autoWidth": true
                },
                {
                    "data": "standards_category",
                    "autoWidth": true
                },
                {
                    "data": "skill_mastery_point",
                    "autoWidth": true
                },
                {
                    "data": "id",
                    "autoWidth": true
                },
                {
                    "data": "created_by",
                    "autoWidth": true
                },
                {
                    "data": "created",
                    "autoWidth": true
                }

            ]
        });

        $("[data-panel-toggle]").each(function() {
            var $this = $(this),
                toggleContainer = $this.attr("data-panel-toggle");
            $this.click(function() {
                $("#" + toggleContainer).slideToggle()
            })
        });

        $('.filter-container').on('change', '.meta_data_filter', function() {
            table.search('').draw();
        });

        // To change the status
        $(document).on('click', '.change_status', function(e) {
            e.preventDefault();
            var scope = $(this);
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure?',
                buttons: {
                    confirm: function() {
                        $.get(scope.attr("href"), // url
                            function(data, textStatus, jqXHR) { // success callback
                                var obj = JSON.parse(data);
                                if (obj.msg === 'Updated') {
                                    table.ajax.reload(); //just reload table
                                }
                            });
                        return true;
                    },
                    cancel: function() {
                        return true;
                    }
                }
            });
        });

        // To delete record
        $(document).on('click', '.delete_item', function(e) {
            e.preventDefault();
            var scope = $(this);
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure?',
                buttons: {
                    confirm: function() {
                        $.get(scope.attr("href"), // url
                            function(data, textStatus, jqXHR) { // success callback
                                var obj = JSON.parse(data);
                                if (obj.msg === 'Deleted') {
                                    table.ajax.reload(); //just reload table
                                }
                            });
                        return true;
                    },
                    cancel: function() {
                        return true;
                    }
                }
            });
        });
    });
</script>