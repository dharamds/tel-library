<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            <ol class="breadcrumb">
                <li class=""><a href="<?php bs(); ?>">Home</a></li>
                <li class=""><a href="<?php bs(); ?>questions/">Questions</a></li>
                <li class="active">  <a href="">  <?php echo (!empty($questionsData->id) ? 'Edit Question' : 'Add Question'); ?> </a></li>
            </ol>
            <div class="container-fluid">
                <div class="panel panel-info" data-widget='{"draggable": "false"}'>
                    <div class="panel-heading">
                        <h2><i class="fa fa-user"></i>
                           <?php echo (!empty($questionsData->id) ? 'Edit Question' : 'Add Question'); ?>
                        </h2>
                        <div class="panel-ctrls" data-actions-container="" data-action-collapse='{"target": ".panel-body"}'></div>
                    </div>
                    <div class="panel-body">
                        <!-- Form starts -->  
                        <?php isset($questionsData->id) ? $id = $questionsData->id : $id = '' ?>
                        <?php
                        echo form_open('questions/add/' . $id, [
                            'name' => 'frmAddQuestion',
                            'id' => 'frmAddQuestion',
                            'class' => 'form-horizontal',
                            'enctype' => 'multipart/form-data']);
                        ?> 
                        <div class="form-group">
                            <label for="name" class="col-md-3 control-label">Question type</label>
                            <div class="col-md-6">
                                <?php isset($questionsData->question_type) ? $question_type = $questionsData->question_type : $question_type = '' ?>
                                <?php
                                $options = ['' => 'Select Question Type',
                                    1 => 'true/false', 2 => 'Single choice',
                                    3 => 'Multiple choice', 4 => 'Descriptive',
                                    5 => 'Paring', 6 => 'Fill in the blank',
                                    7 => 'Mathematical'
                                ];
                                $extra = ['class' => 'form-control', 'id' => 'question_type',
                                    'onChange' => 'getAnsOptions(this.value)'];
                                echo form_dropdown('question_type', $options, $question_type, $extra);
                                ?>
                                <?php echo form_error('question_type', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="question" class="col-md-3 control-label">Question</label>
                            <div class="col-md-9">
                                <?php isset($questionsData->name) ? $question = $questionsData->name : $question = '' ?>
                                <?php
                                echo form_textarea([
                                    'name' => 'question',
                                    'id' => 'question',
                                    'value' => $question,
                                    'class' => 'form-control editor',
                                    'rows' => '3']);
                                ?>
                            </div>
                        </div>

                        <!-- true/false options -->
                        <?php
                        $tf_div = 'none';
                        if ($id && $question_type == 1)
                            $tf_div = "block";
                        ?>
                        <div id="optsTrueFalse" style="display: <?php echo $tf_div; ?>;">
                            <?php if ($id && $question_type == 1) { ?>
                                <?php
                                if (isset($answerData)) {
                                    $i = 1;
                                    foreach ($answerData as $key => $answer) {
                                        $tf_chk = ($answer->answer == 1) ? 'checked' : '';
                                        echo form_input([
                                            'name' => 'tf_id_' . $i, 'id' => 'tf_id_' . $i,
                                            'value' => $answer->id,
                                            'type' => 'hidden']);
                                        ?>
                                        <div class="form-group">
                                            <label for="tf_opts_<?php echo $i; ?>" class="col-md-3 control-label">Options <?php echo $i; ?></label>  
                                            <div class="col-md-5">                     
                                                <?php
                                                echo form_input([
                                                    'name' => 'tf_opts_' . $i, 'id' => 'tf_opts_' . $i,
                                                    'value' => $answer->option,
                                                    'class' => 'form-control', 'placeholder' => 'Options']);
                                                ?>                           
                                            </div>
                                            <div class="col-md-4">                     
                                                <?php
                                                $data = ['name' => 'tf_check', 'id' => 'tf_check_' . $i,
                                                    'value' => $i, 'checked' => $tf_chk];
                                                echo form_radio($data);
                                                ?>
                                            </div>
                                        </div>
                                        <?php
                                        $i++;
                                    } // end foreach
                                } // end if(isset($answerData))
                                ?>
                            <?php } else { ?>
                                <div class="form-group">
                                    <label for="code" class="col-md-3 control-label">Options 1</label>  
                                    <div class="col-md-5">                     
                                        <?php
                                        echo form_input([
                                            'name' => 'tf_opts_1', 'id' => 'tf_opts_1',
                                            'value' => 'True', 'class' => 'form-control',
                                            'placeholder' => 'Options']);
                                        ?>                           
                                    </div>
                                    <div class="col-md-4">                     
                                        <?php
                                        $data = ['name' => 'tf_check', 'id' => 'tf_check_1',
                                            'value' => 1];
                                        echo form_radio($data);
                                        ?>
                                    </div>
                                </div>   

                                <div class="form-group">
                                    <label for="code" class="col-md-3 control-label">Options 2</label>  
                                    <div class="col-md-5">                     
                                        <?php
                                        echo form_input([
                                            'name' => 'tf_opts_2', 'id' => 'tf_opts_2',
                                            'value' => 'False', 'class' => 'form-control',
                                            'placeholder' => 'Options']);
                                        ?>                           
                                    </div>
                                    <div class="col-md-4">                     
                                        <?php
                                        $data = ['name' => 'tf_check', 'id' => 'tf_check_2',
                                            'value' => 2];
                                        echo form_radio($data);
                                        ?>
                                    </div>
                                </div> 
                            <?php } ?>

                        </div>

                        <!-- Multiple choice options -->
                        <?php
                        $multi_div = 'none';
                        if ($id && $question_type == 3)
                            $multi_div = "block";
                        ?>
                        <div id="optsMultiple" style="display: <?php echo $multi_div; ?>;">
                            <?php if ($id && $question_type == 3) { ?>
                                <?php
                                if (isset($answerData)) {
                                    $cnt_ans_mopt = ['type' => 'hidden', 'name' => 'cnt_mopts', 'id' => 'cnt_mopts', 'value' => count($answerData)];
                                    echo form_input($cnt_ans_mopt);
                                    $i = 0;
                                    foreach ($answerData as $key => $ans_value) {
                                        $i++;
                                        ?>
                                        <div id="divMOpts_<?php echo $i; ?>">
                                            <?php
                                            $que_ans_mopt = ['type' => 'hidden', 'name' => 'que_ans_id_' . $i, 'id' => 'que_ans_id_' . $i, 'value' => $ans_value->id];
                                            echo form_input($que_ans_mopt);
                                            ?>
                                            <div class="form-group"> 
                                                <label for="code" class="col-md-3 control-label">Options <?php echo $i; ?></label>   
                                                <div class="col-md-6">                               
                                                    <?php
                                                    echo form_input([
                                                        'name' => 'mopts_' . $i,
                                                        'id' => 'mopts_' . $i,
                                                        'value' => $ans_value->option,
                                                        'class' => 'form-control multipleOpts',
                                                        'placeholder' => 'Options']);
                                                    ?>                           
                                                </div>
                                                <div class="col-md-3">                            
                                                    <?php
                                                    $checkMOpts = ($ans_value->answer == 1) ? 'checked' : '';
                                                    $data = [
                                                        'name' => 'chk_mopts[]',
                                                        'id' => 'chk_mopts_' . $i,
                                                        'value' => $i,
                                                        'checked' => $checkMOpts
                                                    ];
                                                    echo form_checkbox($data);
                                                    ?> Correct
                                                </div>       
                                            </div>
                                        </div>
                                        <?php
                                    }  //foreach
                                    echo '<div id="multipleOptsDiv"></div>';
                                    ?>
                                    <div class="form-group">     
                                        <div class="col-md-9"></div>               
                                        <div class="col-md-3">
                                            <a onClick="addMultipleOpts();" title="Add options"><i class="ti ti-plus"></i></a>&nbsp;&nbsp;
                                            <a onClick="removeMultipleOpts(<?php echo $ans_value->id; ?>);"  title="Remove options"><i class="ti ti-minus"></i></a> 
                                        </div>
                                    </div>
                                    <?php
                                } // if
                            } else {
                                ?>
                                <div class="form-group"> 
                                    <?php
                                    $cnt_ans_mopt = ['type' => 'hidden', 'name' => 'cnt_mopts', 'id' => 'cnt_mopts', 'value' => '2'];
                                    echo form_input($cnt_ans_mopt);
                                    ?>
                                    <label for="code" class="col-md-3 control-label">Options 1</label>   
                                    <div class="col-md-6">                               
                                        <?php
                                        echo form_input([
                                            'name' => 'mopts_1',
                                            'id' => 'mopts_1',
                                            'class' => 'form-control multipleOpts',
                                            'placeholder' => 'Options']);
                                        ?>                           
                                    </div>
                                    <div class="col-md-3">                            
                                        <?php
                                        $data = [
                                            'name' => 'chk_mopts[]',
                                            'id' => 'chk_mopts_1',
                                            'value' => 1
                                        ];
                                        echo form_checkbox($data);
                                        ?> Correct
                                    </div>       
                                </div>   

                                <div class="form-group"> 
                                    <label for="code" class="col-md-3 control-label">Options 2</label>   
                                    <div class="col-md-6">                               
                                        <?php
                                        echo form_input([
                                            'name' => 'mopts_2',
                                            'id' => 'mopts_2',
                                            'class' => 'form-control multipleOpts',
                                            'placeholder' => 'Options']);
                                        ?>                           
                                    </div>
                                    <div class="col-md-3">                            
                                        <?php
                                        $data = [
                                            'name' => 'chk_mopts[]',
                                            'id' => 'chk_mopts_2',
                                            'value' => 2
                                        ];
                                        echo form_checkbox($data);
                                        ?> Correct
                                    </div>       
                                </div>
                                <div id="multipleOptsDiv"></div>

                                <div class="form-group">     
                                    <div class="col-md-9"></div>               
                                    <div class="col-md-3">
                                        <a onClick="addMultipleOpts();" title="Add options"><i class="ti ti-plus"></i></a>&nbsp;&nbsp;
                                        <a onClick="removeMultipleOpts(0);"  title="Remove options"><i class="ti ti-minus"></i></a> 
                                    </div>
                                </div>
                            <?php } ?>  
                        </div>
                        <!-- Multiple choice options ends-->

                        <!-- Single choice options -->
                        <?php
                        $single_div = 'none';
                        if ($id && $question_type == 2)
                            $single_div = "block";
                        ?>
                        <div id="optsSingle" style="display: <?php echo $single_div; ?>;">

                            <?php if ($id && $question_type == 2) { ?>

                                <?php
                                if (isset($answerData)) {

                                    $cnt_ans_sopt = ['type' => 'hidden', 'name' => 'cnt_sopts', 'id' => 'cnt_sopts', 'value' => count($answerData)];
                                    echo form_input($cnt_ans_sopt);

                                    $i = 0;
                                    foreach ($answerData as $key => $ans_value) {
                                        $i++;
                                        ?>
                                        <div id="divSOpts_<?php echo $i; ?>">
                                            <?php
                                            $que_ans_sopt = ['type' => 'hidden', 'name' => 'que_ans_id_' . $i, 'id' => 'que_ans_id_' . $i, 'value' => $ans_value->id];
                                            echo form_input($que_ans_sopt);
                                            ?>
                                            <div class="form-group"> 
                                                <label for="code" class="col-md-3 control-label">Options <?php echo $i; ?></label>  
                                                <div class="col-md-6">                     
                                                    <?php
                                                    echo form_input([
                                                        'name' => 'opts_' . $i,
                                                        'id' => 'opts_' . $i,
                                                        'value' => $ans_value->option,
                                                        'class' => 'form-control singleOpts',
                                                        'placeholder' => 'Options']);
                                                    ?>                           
                                                </div>
                                                <div class="col-md-3">
                                                    <?php $checkSOpts = ($ans_value->answer == 1) ? 'checked' : ''; ?>                   
                                                    <?php
                                                    $data = [
                                                        'name' => 'opts_status',
                                                        'id' => 'opts_status_' . $i,
                                                        'value' => $i,
                                                        'checked' => $checkSOpts
                                                    ];
                                                    echo form_radio($data);
                                                    ?> Correct
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }// end foreach
                                    echo '<div id="singleOptsDiv"></div>';
                                    ?>
                                    <div class="form-group">     
                                        <div class="col-md-9"></div>               
                                        <div class="col-md-3">
                                            <a onClick="addSingleOpts();" title="Add options"><i class="ti ti-plus"></i></a>&nbsp;&nbsp;
                                            <a onClick="removeSingleOpts(<?php echo $ans_value->id; ?>);"  title="Remove options"><i class="ti ti-minus"></i></a> 
                                        </div>
                                    </div>
                                    <?php
                                }// end if(isset($answerData)) 
                                ?>
                            <?php } else { ?>
                                <div class="form-group">
                                    <?php
                                    $cnt_ans_sopt = ['type' => 'hidden', 'name' => 'cnt_sopts', 'id' => 'cnt_sopts', 'value' => '2'];
                                    echo form_input($cnt_ans_sopt);
                                    ?> 
                                    <label for="code" class="col-md-3 control-label">Options 1</label>  
                                    <div class="col-md-6">                     
                                        <?php
                                        echo form_input([
                                            'name' => 'opts_1',
                                            'id' => 'opts_1',
                                            'class' => 'form-control singleOpts',
                                            'placeholder' => 'Options']);
                                        ?>                           
                                    </div>
                                    <div class="col-md-3">                     
                                        <?php
                                        $data = [
                                            'name' => 'opts_status',
                                            'id' => 'opts_status_1',
                                            'value' => 1
                                        ];
                                        echo form_radio($data);
                                        ?> Correct
                                    </div>
                                </div>

                                <div class="form-group"> 
                                    <label for="code" class="col-md-3 control-label">Options 2</label>  
                                    <div class="col-md-6">                     
                                        <?php
                                        echo form_input([
                                            'name' => 'opts_2',
                                            'id' => 'opts_2',
                                            'class' => 'form-control singleOpts',
                                            'placeholder' => 'Options']);
                                        ?>                           
                                    </div>
                                    <div class="col-md-3">                     
                                        <?php
                                        $data = [
                                            'name' => 'opts_status',
                                            'id' => 'opts_status_2',
                                            'value' => 2
                                        ];
                                        echo form_radio($data);
                                        ?> Correct
                                    </div>
                                </div>

                                <div id="singleOptsDiv"></div>

                                <div class="form-group">     
                                    <div class="col-md-9"></div>               
                                    <div class="col-md-3">
                                        <a onClick="addSingleOpts();" title="Add options"><i class="ti ti-plus"></i></a>&nbsp;&nbsp;
                                        <a onClick="removeSingleOpts(0);"  title="Remove options"><i class="ti ti-minus"></i></a> 
                                    </div>
                                </div>
                            <?php } ?>
                        </div>                     

                        <!-- Single choice options ends -->
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <input type="submit" class="finish btn-success btn" value="Submit">
                            </div>
                        </div> 
                        <?php echo form_close(); ?>      
                        <!-- Form ends -->        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- .container-fluid -->
</div>

<!--<script type="text/javascript" src="<?php bs('public/assets/js/questions/add.js') ?>"></script>-->
//<?php
//$jsVars = [
//    'ajax_call_root' => base_url()
//];
//?>

<script type="text/javascript">

    $(document).ready(function () {
        
        
          $("#frmAddQuestion").validate({
            ignore: [],
            rules: {
                question_type: {
                    required: true
                },
                question: {
                    required: function (textarea) {
                        CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                        return editorcontent.length === 0;
                    }
                },
                opts_1: {
                    required: function () {
                        var a = $("#question_type").val();
                        if (a == 2)
                            return true;
                    }
                },
                opts_2: {
                    required: function () {
                        var a = $("#question_type").val();
                        if (a == 2)
                            return true;
                    }
                },
                opts_status: {
                    required: function () {
                        var a = $("#question_type").val();
                        if (a == 2)
                            return true;
                    }
                },
                mopts_1: {
                    //required : true					
                    required: function () {
                        var a = $("#question_type").val();
                        if (a == 3)
                            return true;
                    }
                },
                mopts_2: {
                    required: function () {
                        var a = $("#question_type").val();
                        if (a == 3)
                            return true;
                    }
                },
//                'chk_mopts[]': {
//                    minlength: 1,
//                    required: function () {
//                        var a = $("#question_type").val();
//                        if (a == 3)
//                            return true;
//                    }
//                }
            },
            messages: {
                question_type: {
                    required: "Please select question type"
                },
                question: {
                    required: "Please enter QUESTION"
                },
                opts_1: {
                    required: "Please enter option"
                },
                opts_2: {
                    required: "Please enter option"
                },
                opts_status: {
                    required: "Please select at-least one correct option"
                },
                mopts_1: {
                    required: "Please enter option value"
                },
                mopts_2: {
                    required: "Please enter option value"
                },
//                'chk_mopts[]': {
//                    required: "Please select at-least one correct option"
//                },
                
                
            },
            errorPlacement: function (error, $elem) {
                if ($elem.is('textarea')) {
                    $elem.insertAfter($elem.next('div'));
                }
                error.insertAfter($elem);
            }
           
            
        });
        
        
        
        
        
        
        
        
        // Jquery validations
      //  AddQuestion.init(<?php echo json_encode($jsVars); ?>);
        // CK editor
        var config = {
            toolbarGroups: [{
                    "name": "basicstyles",
                    "groups": ["basicstyles"]
                },
                {
                    "name": "links",
                    "groups": ["links"]
                },
                {
                    "name": "paragraph",
                    "groups": ["list", "blocks"]
                },
                {
                    "name": "document",
                    "groups": ["mode"]
                },
                {
                    "name": "insert",
                    "groups": ["insert"]
                },
                {
                    "name": "styles",
                    "groups": ["styles"]
                },
                {
                    "name": "about",
                    "groups": ["about"]
                }
            ],
            removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar'
        };
        // to assign ckeditor
        CKEDITOR.replace('question', config);
    });


    /* Single choice option add remove*/
    function addSingleOpts() {
        var optcnt = ($('.singleOpts').length);
        optcnt++;
        $("#cnt_sopts").val(optcnt);

        $('#singleOptsDiv').append('<div id="divSOpts_' + optcnt + '"><div class="form-group"><label for="code" class="col-md-3 control-label">Options ' + optcnt + '</label><div class="col-md-6"><input type="text" name="opts_' + optcnt + '" value="" id="opts_' + optcnt + '" class="form-control singleOpts" placeholder="Options"></div><div class="col-md-3"><input type="radio" name="opts_status" value="' + optcnt + '" id="opts_status_' + optcnt + '"> Correct</div></div></div>');
    }
    function removeSingleOpts(val) {

        var optcnt = ($('.singleOpts').length);
        if (val == 0) {
            //alert(optcnt + val);
            if (optcnt != 2) {
                $('#divSOpts_' + optcnt).remove();
                optcnt--;
                $("#cnt_sopts").val(optcnt);
            }
        } else if (val > 0) {
            alert(val + "  =  " + optcnt);
            $.ajax({
                url: "<?php echo bs(); ?>questions/remove_options",
                type: "POST",
                data: 'id=' + val,
                success: function (data) {
                    if (data) {
                        $('#divSOpts_' + optcnt).remove();
                        optcnt--;
                        $("#cnt_sopts").val(optcnt);
                    }
                }
            });
        }
    }

    /* Multiple choice option add remove */
    function addMultipleOpts() {
        var optcnt = ($('.multipleOpts').length);
        //alert(optcnt);
        optcnt++;
        $("#cnt_mopts").val(optcnt);
        $('#multipleOptsDiv').append('<div id="divMOpts_' + optcnt + '"><div class="form-group"><label for="code" class="col-md-3 control-label">Options ' + optcnt + '</label><div class="col-md-6"><input type="text" name="mopts_' + optcnt + '" value="" id="mopts_' + optcnt + '" class="form-control multipleOpts" placeholder="Options"></div><div class="col-md-3"><input type="checkbox" name="chk_mopts[]" value="' + optcnt + '" id="chk_mopts_' + optcnt + '"> Correct</div></div></div>');
    }

    function removeMultipleOpts(val) {

        var optcnt = ($('.multipleOpts').length);
        if (val == 0) {
            alert(optcnt + val);
            if (optcnt != 2) {
                $('#divMOpts_' + optcnt).remove();
                optcnt--;
                $("#cnt_mopts").val(optcnt);
            }
        } else if (val > 0) {
            alert(val);
            $.ajax({
                url: "<?php echo bs(); ?>questions/remove_options",
                type: "POST",
                data: 'id=' + val,
                success: function (data) {
                    if (data) {
                        $('#divMOpts_' + optcnt).remove();
                        optcnt--;
                        $("#cnt_mopts").val(optcnt);
                    }
                }
            });
        }
    }

    function getAnsOptions(val) {
        if (val == 1) {
            $("#optsTrueFalse").show();
            $("#optsSingle").hide();
            $("#optsMultiple").hide();
        } else if (val == 2) {
            $("#optsTrueFalse").hide();
            $("#optsSingle").show();
            $("#optsMultiple").hide();
        } else if (val == 3) {
            $("#optsTrueFalse").hide();
            $("#optsMultiple").show();
            $("#optsSingle").hide();
        } else {
            $("#optsTrueFalse").hide();
            $("#optsMultiple").hide();
            $("#optsSingle").hide();
        }
    }
</script>