<link rel="stylesheet" href="<?php bs('public/assets/plugins/multiselect/sumoselect.min.css') ?>" />
<link type="text/css" href="<?= bs('public/assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css')?>" rel="stylesheet"> 
<div class="container-fluid pr-0">
   <div class="panel panel-default">
      <div class="panel-heading">
         <ul class="nav nav-tabs" id="coursePanelTab">
            <li role="presentation" class="active">
               <a data-toggle="tab" href="#content" role="tab" id="content_id">Content</a>
            </li>
            <li role="presentation">
               <a data-toggle="tab" href="#metadata" role="tab" id="metadata_id">Metadata</a>
            </li>
         </ul>
      </div>
      <div class="panel-body" style="padding: 16px 20px;">
         <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="content">
            <?php
              echo form_open('questions/add/' . $qid, [
                'name'    => 'questionContent',
                'id'      => 'questionContent',
                'enctype' => 'multipart/form-data'
              ]);
            ?>
            <?php
              $que_cont_id_hid = [
                'type'  => 'hidden',
                'id'    => 'que_cont_id',
                'name'  => 'que_cont_id',
                'value' => $qid
              ];
              echo form_input($que_cont_id_hid);
            ?>
            <!-- Question type -->
            <div class="row mt-4 mb-3">
               <div class="col-sm-6">
                  <div class="form-group">
                     <label for="question_type" class="control-label"> Select question type <span class="field-required">*</span></label>
                     <?php
                        $extra         = ['id' => 'question_type', 'class' => 'form-control'];
                        $selectedType  = isset($questionsData->type) ? $questionsData->type : '';
                        echo form_dropdown('question_type', $questionTypes, $selectedType, $extra);
                     ?>
                  </div>
               </div>
            </div>

            <!--// Start Content Tab  -->
            <div class="loader"> </div>

            <!-- Question type oriented view -->
            <div id="question_type_views"> </div>
            <?php echo form_close(); ?>
         </div>


         <!-- Metadata -->
        <div role="tabpanel" class="tab-pane" id="metadata">
          <div class="pl-4 col-md-12">
            <div class="col-md-6 pl-0 pt-5">
              <div class="panel panel-grey">
                <div class="panel-heading mb-3">
                  <h2><span>Assessment Categories</span></h2>
                </div>
                <div class="panel-body panel-collapse-body" id="assessment_categories_section">
                </div>
              </div>
            </div>
            <div class="col-md-6 pl-0 pt-5">
              <div class="panel panel-grey">
                <div class="panel-heading">
                  <h2><span>Assessment Tags</span></h2>
                  <div class="panel-ctrls button-icon-bg">
                  </div>
                </div>
                <div class="panel-body" id="assessment_tags_section"> </div>
              </div>
            </div>
          </div>

          <div class="hr-line col-md-12"></div>

          <div class="pl-4 col-md-12">
            <div class="col-md-6 pl-0 pt-5">
              <div class="panel panel-grey">
                <div class="panel-heading mb-3">
                  <h2><span>System Categories</span></h2>
                </div>                
                <div class="panel-body panel-collapse-body" id="system_categories_section"> </div>
              </div>
            </div>
            <div class="col-md-6 pl-0 pt-5">
              <div class="panel panel-grey">
                <div class="panel-heading">
                  <h2><span>System Tags</span></h2>
                  <div class="panel-ctrls button-icon-bg">
                  </div>
                </div>
                <div class="panel-body" id="system_tags_section"> </div>
              </div>
            </div>
          </div>

          <div class="hr-line col-md-12"></div>
         
          <div class="pl-4 col-md-12">
            <div class="col-md-6 pl-0 pt-5">
              <div class="panel panel-grey">
                <div class="panel-heading mb-3">
                  <h2><span>Learning Outcomes</span></h2>
                </div>
                <div class="panel-body panel-collapse-body" id="learningoutcome_categories_section">
                </div>
              </div>
            </div>
            <div class="col-md-6 pl-0 pt-5">
              <div class="panel panel-grey">
                <div class="panel-heading mb-3">
                  <h2><span>TEL Mastery Standards</span></h2>
                </div>
                <div class="panel-body panel-collapse-body" id="learninstandards_categories_section"> </div>
              </div>
            </div>
          </div>

        <div class="hr-line col-md-12"></div>

        <div class="col-xs-12 p-0 pt-4">
          <div class="form-group flex-row m-0 pb-4">
            <div class="flex-col-auto flex-row p-0 m-0">
              <label for="skill_mastery_point" class="control-label pt-4 flex-col-auto">Mastery Points</label>
                <div class="p-0 m-0 ml-4 w80 flex-auto pt-3">
                    <input type="text" name="skill_mastery_point" id="skill_mastery_point" value="<?php echo isset($questionsData->skill_mastery_point) ? $questionsData->skill_mastery_point : 1;?>" class="touchspin4 form-control mastery-point">
                </div>
            </div>             
          </div>       
        </div>
        <div class="col-xs-12 text-right p-0 mt-4">
          <button type="button" id="nextMetadata" class="btn btn-primary">Back to the list</button>
        </div>
        </div>
        <!-- Metadata ends -->
      </div>
   </div>
</div>

<script src="http://malsup.github.com/jquery.form.js"></script>      
<script type="text/javascript" src="<?= bs('public/assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js') ?>"></script>      

<script type="text/javascript">

   $(document).ready(function() { 
      var selected_qtype = $('#question_type').val();
      //Touchspin for weight
      $("#skill_mastery_point").TouchSpin({
         verticalbuttons: true,
         step: 1,
         min : 1,
         max : 5
      });

      $('#metadata_id').click(function() {
        var que_cont_id = $('#que_cont_id').val();
        //alert(que_cont_id);
        if (que_cont_id != '') {
          getMetadata(que_cont_id);
        } else {
          return false;
        }
      });

      /*$('#content_id').click(function() {
        var que_cont_id = $('#que_cont_id').val();
        //alert(que_cont_id);
        if (que_cont_id != '') {
          window.location.href = "<?php echo bs(); ?>questions/add/"+que_cont_id;
        } else {
          return false;
        }
      });*/

      var selected_qtype = $('#question_type').val();
      if (selected_qtype) {
          var que_cont_id = $('#que_cont_id').val();
          getViewTemplate("question_type_views", selected_qtype, que_cont_id );
      }      

   }); //end $(document).ready

   // On change question type
   $(document).on('change', '#question_type', function () {
      var selected_qtype = $('#question_type').val();
      getViewTemplate("question_type_views", selected_qtype);

   });

    $("#nextMetadata").click(function() {
      window.location.href = "<?php echo bs(); ?>questions/";
    });


   // Submit form
   function submitQuestionContentForm() {
      //alert('test'); return false;
      $.ajax({
          type: "POST",
          data: $("#questionContent").serialize(),
          dataType: 'json',
          url: "<?php echo bs(); ?>questions/addQuestionContent",
          success: function(data) {
            console.log(data);
            if (data.code == 200) {
              $('.nav-tabs a[href="#metadata"]').tab('show');
              $('.nav-tabs a[href="#content"]').attr('data-id', data.id);
              $('.nav-tabs a[href="#metadata"]').attr('data-id', data.id);
              $('#que_cont_id').val(data.id);
              $('#que_meta_id').val(data.id);

              if(data.respids) {
                var i=0; var resp_i = 1;
                $('.up-class .hiddeninputs').html(''); 
                  $.each(data.respids, function (key, val) {
                     // alert(key + val);                      
                      $('.remove-response').each( function (k, v) {
                        var rem_res = $(this).attr('data-resid');
                        if(resp_i==rem_res)
                          $(this).attr('data-queresid', val);
                      });       
                      
                    $('.up-class .hiddeninputs').append('<input type="hidden" name="res_data['+i+'][id]" id="que_res'+i+'" value="'+val+'">');
                    i++; resp_i++;
                });
              }
              getMetadata(data.id);
              CommanJS.getDisplayMessgae(data.code, data.success);
            }else if(data.code == 400) {
              CommanJS.getDisplayMessgae(data.code, data.error);
            }
          }
        });
   }

   // Load template
   function getViewTemplate(selector_view, quetionType, queId) {

      $(".loader").html('<div class="col-sm-12"><div class="page-loading-box"><span class="page-loader-quart"></span> Loading...</div></div>');
      $("#" + selector_view).load("<?php echo base_url() ?>questions/load_view/" + quetionType + "/" + queId, function() {
           // $(".loader").hide();
      });
      setTimeout(function(){ $(".loader").hide(); }, 2000);
   }

   // to get all system and default categories and save into table
   function getMetadata(queID) {    
  
      // System Category  1
      CommanJS.getCatSection(1, 'system_categories_section', 'system_categories', queID, 11);
      CommanJS.getTagSection(1, 'system_tags_section', 'system_tags', queID, 11);

      // Assessment category and tags
      CommanJS.getCatSection(9, 'assessment_categories_section', 'assessment_categories', queID, 11);
      CommanJS.getTagSection(9, 'assessment_tags_section', 'assessment_tags', queID, 11);
      
      // Learning Outcomes  1
      CommanJS.getCatSection(14, 'learningoutcome_categories_section', 'learningoutcome_categories', queID, 11);
      // Learning standards  15
      CommanJS.getCatSection(15, 'learninstandards_categories_section', 'learninstandards_categories', queID, 11);
      
      // Blooms taxanomy level
      CommanJS.getCatSection(18, 'blooms_taxanomy_categories_section', 'blooms_taxanomy_categories', queID, 11);
   
   }

   // save mastery points
   $(document).on('change', '.mastery-point', function () {    
    var point = $("#skill_mastery_point").val();
    var qid = $('#que_cont_id').val();

    $.ajax({
          type: "POST",
          data: {'skill_mastery_point' : point, 'qid' : qid },      
          dataType: 'json',
          url: "<?php echo bs(); ?>questions/setQuestionMasteryPoint",
          success: function(data) {
            //console.log(data);
            if(data.code==200) 
              CommanJS.getDisplayMessgae(data.code, data.success);
            else
              CommanJS.getDisplayMessgae(data.code, data.error);
          },
      });
  });

</script>