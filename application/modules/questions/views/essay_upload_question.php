<!-- Question type essay/fileupload -->   

<!-- Title -->
<div class="row mt-4 mb-3">
  <div class="col-sm-4">
    <div class="form-group">
      <label for="title" class="control-label"> Title <span class="field-required">*</span> </label>
      <?php
        $title = isset($queData->title) ? $queData->title : '';
        echo form_input([
          'id'    => 'title',
          'name'  => 'title',
          'value' => $title,
          'class' => 'form-control'
        ]);
      ?>
    </div>
  </div>
  <!-- Long Title  -->
  <div class="col-sm-8">
    <div class="form-group">
      <label for="long_title" class="control-label"> Long Title </label>
      <?php
        $long_title = isset($queData->long_title) ? $queData->long_title : '';
        echo form_input([
          'id'    => 'long_title',
          'name'  => 'long_title',
          'value' => $long_title,
          'class' => 'form-control'
        ]);
      ?>
    </div>
  </div>
</div>

<!-- Question or prompt -->
<div class="row mt-4 mb-3">
  <div class="col-sm-8">
    <div class="form-group">
      <label for="question_prompt" class="control-label">Question or prompt <span class="field-required">*</span></label>
      <?php
        $question_prompt = isset($queData->question_prompt) ? $queData->question_prompt : '';
      ?>
      <?php
        echo form_textarea([
          'name'  => 'question_prompt',
          'id'    => 'question_prompt',
          'value' => $question_prompt,
          'class' => 'form-control ckeditor',
          'rows'  => '5'
        ]);
      ?>
    </div>
  </div>
</div>
<!-- Question type oriented view -->        
<!-- Post submission message -->
<div class="row mt-4 mb-3 m-0">
  <div class="col-sm-8 p-0">
    <div class="form-group">
      <label for="title" class="control-label">Post submission message</label>
      <?php 
        $post_submission_msg = isset($queData->post_submission_msg) ? $queData->post_submission_msg : ''; 
      ?>
      <?php 
        echo form_textarea([ 'name'  => 'post_submission_msg',
                             'id'    => 'post_submission_msg',
                             'value' => $post_submission_msg,
                             'class' => 'form-control ckeditor',
                             'rows'  => '5'
                          ]);
      ?>
    </div>
  </div>
</div>

<div class="col-md-6 p-0 ">
  <div class="form-group col-xs-12 mb-3">
    <div>
      <label class="m-0 checkbox-tel">
       <?php if(isset($queData->response_type_text) && $queData->response_type_text!="") $res_exp=TRUE; else $res_exp=FALSE; ?>
        <?php echo form_checkbox('response_type_text', '1', $res_exp, 'id="response_type_text" class="mr-2"');?> Student response text box
      </label>
    </div>
    <div>
      <label class="checkbox-tel m-0">
        <?php if( isset($queData->response_type_file) && $queData->response_type_file!="") $resf_exp=TRUE; else $resf_exp=FALSE; ?>
        <?php echo form_checkbox('response_type_file', '1', $resf_exp, 'id="response_type_file" class="mr-2"');?> Student will upload file
      </label>
    </div>
  </div>
</div>


<div class="col-xs-12 mt-3 mb-3 p-0 pb-2"> <div class="hr-line mb-4"></div> </div>

<div class="col-xs-12 p-0 mb-3">
  <div class="col-xs-12 col-md-6 p-0">
    <a href="javascript:void(0)" class="btn btn-warning btnPreview" data-toggle="modal" data-target="#quePreview" data-previewid="<?php echo $qid; ?>" style="display: none;">Preview Question</a>
  </div>
  <div class="col-xs-12 col-md-6 text-right p-0">
    <button type="submit" id="nextContent" class="btn btn-primary"><?php echo isset($qid) ? 'Save changes' : 'Save and next'?></button>
  </div>
</div>


<!-- -------------------------------------- Preview Modal ---------------------------------------- -->
<div class="modal fade modals-tel-theme" id="quePreview" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog w60p">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h2 class="modal-title">
          Question Preview
        </h2>
      </div>
      <div class="modal-body">
        <div class="question-panel p-5">
          <div class="pb-3">
            <div class="alert alert-primary">
              Submit your 200 word essay on what you did last summer.
            </div>
          </div>

          <h5 class="mt-0">
            <i class="flaticon-question-mark mr-3"></i>
            You may either upload a file (MS Word, PDF, RTF or Pages), link to a Google Document, or type your response in the box below.
          </h5>

          <div class="row col-sm-12 istVisualContainer brd-0 m-0 p-0">
            <div class="col-sm-3">
              <div class="ist-logo auto-wh">
                <img src="http://192.168.1.74:7777/tel_library/public/assets/img/logo-bg.png" id="ist_logo_id" class="img-responsive">
              </div>
            </div>
            <div class="col-sm-8">
                <div class="ist-visual-info pt-2">
                    <h4 class="f15 mb-3 p-0">Select a file to upload</h4>
                    <div class="row">
                        <div class="col-sm-6 pl-0">
                             <div class="fileinput fileinput-new col-md-12 p-0" data-provides="fileinput">
                                <span class="btn btn-danger btn-file col-md-12">
                                    <span class="fileinput-new">Choose File</span>
                                    <span class="fileinput-exists">Change</span>
                                    <input id="dis_img" name="logo" type="file" class="blog_img1 visible1">
                                    <br>
                                    <div id="uploaded_image"></div>
                                </span>
                                <div class="filename-with-btn col-md-12">
                                    <span class="fileinput-filename"></span>
                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="small">
                                (MS Word, PDF, RTF or Pages)
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

          <div class="form-group col-xs-12 p-0 mt-5">
            <label class="control-label col-md-12 p-0"> Added Documents (<span id="docCnt">1</span>) </label>
            <div class="col-md-6 p-0">
              <div class="list-group list-file-row mb-3">
                  <span class="list-group-item">
                    <h4 class="list-group-item-heading">Document 1</h4>
                    <a class="btn btn-danger delete_doc" data-docname="ExampleFile.xlsx"> 
                      <i class="flaticon-trash"></i> 
                    </a>
                  </span>
              </div>
            </div>
          </div>

          <div class="col-xs-12 mt-3 mb-3 p-0 pb-2">
            <div class="hr-line mb-4"></div>
          </div>

          <div class="form-group col-xs-12 p-0 pt-3">
            <label class="control-label">
              Type your response below
            </label>
            <div class="pull-right">
              Word Count : 15
            </div>
            <div class="col-xs-12 p-0 mt-3">
              <textarea id="response_editor_1" class="ckeditor">Sample text of essay..</textarea>
            </div>
          </div>

        </div>
      </div>
      <div class="col-xs-12 mt-3 mb-3">
        <div class="hr-line"></div>
      </div>
      <div class="modal-footer text-right">
        <button type="button" class="btn btn-danger btn-md" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- -------------------------------------- Preview Modal ends ---------------------------------------- -->

<script type="text/javascript">
   $(document).ready(function() {
    //ck editor
    var config = { height: 80, allowedContent :true, toolbar : 'short'};    
    $('.ckeditor').each(function(e) {
      CKEDITOR.replace( this.id, config);
    });

    //Preview
    var getCountVal = $("[data-previewid]").attr('data-previewid');
    getCountVal>0?$("[data-previewid]").show():$("[data-previewid]").hide();

    // Validating module content form
    $("#questionContent").validate({
      ignore: [],
      rules: {        
        title: {
          required: true
        },
        question_prompt: {
          required: function(textarea) {
            CKEDITOR.instances[textarea.id].updateElement(); // update textarea
            var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
            return editorcontent.length === 0;
          }
        },
      },
      messages: {
        title: { required: "Please enter question title" },
        question_prompt:{ required: "Please enter question" }
      },
      submitHandler: function(form) { 
          console.log('form');
          //alert('here submit');
          CKEDITOR.instances['post_submission_msg'].updateElement();
          submitQuestionContentForm();
          return false;

      },
      errorPlacement: function(error, $elem) {
        if ($elem.is('textarea')) {
          $elem.insertAfter($elem.next('div'));
        }
        error.insertAfter($elem);
      }      
    });

  }); // end ready


</script>                