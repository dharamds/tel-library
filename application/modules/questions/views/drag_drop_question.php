<!-- Drag & Drop -->

<!-- Title -->
<div class="row mt-4 mb-3">
  <div class="col-sm-4">
    <div class="form-group">
      <label for="title" class="control-label"> Title <span class="field-required">*</span> </label>
      <?php
        $title = isset($queData->title) ? $queData->title : '';
        echo form_input([
          'id'    => 'title',
          'name'  => 'title',
          'value' => $title,
          'class' => 'form-control'
        ]);
      ?>
    </div>
  </div>
  <!-- Long Title  -->
  <div class="col-sm-8">
    <div class="form-group">
      <label for="long_title" class="control-label"> Long Title </label>
      <?php
        $long_title = isset($queData->long_title) ? $queData->long_title : '';
        echo form_input([
          'id'    => 'long_title',
          'name'  => 'long_title',
          'value' => $long_title,
          'class' => 'form-control'
        ]);
      ?>
    </div>
  </div>
</div>

<!-- Question or prompt -->
<div class="row mt-4 mb-3">
  <div class="col-sm-8">
    <div class="form-group">
      <label for="question_prompt" class="control-label">Question or prompt <span class="field-required">*</span></label>
      <?php
        $question_prompt = isset($queData->question_prompt) ? $queData->question_prompt : '';
      ?>
      <?php
        echo form_textarea([
          'name'  => 'question_prompt',
          'id'    => 'question_prompt',
          'value' => $question_prompt,
          'class' => 'form-control ckeditor',
          'rows'  => '5'
        ]);
      ?>
    </div>
  </div>
</div>
<!-- Question type oriented view RESPONSE -->

<?php if(!$responseData) { ?>   
  <div class="col-xs-12 p-0">
    <div class="panel panel-response">
      <div class="panel-body">
        <div class="form-group col-xs-12 p-0 pt-3 res-up-class">
          <div class="hiddenResInputs"></div>
          <label class="control-label lblRes">Response 1 *</label>
            <div class="col-xs-12 p-0 mt-3">
            <?php
              $txtRarea = [ 'name'    => 'response[]',
                             'id'     => 'response_1',
                             'value'  => '',
                             'class'  => 'data-sample-short ckeditor txtRes',
                          ];
              echo form_textarea($txtRarea);
            ?>
          </div>
        </div>
        <div class="form-group col-xs-12 p-0 mt-3">
          <label class="control-label">Feedback</label>
          <div class="col-xs-12 p-0">
            <div class="flex-row align-items-end">
              <div class="flex-col-7">
                <?php
                  $txtFarea = [ 'name'    => 'feedback[]',
                                 'id'     => 'feedback_1',
                                 'value'  => '',
                                 'class'  => 'data-sample-short ckeditor txtFb',
                                ];
                  echo form_textarea($txtFarea);
                ?>
              </div>                  
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="extraResponseDiv"></div>
<?php } else { ?>
  <!-- UPDATE -->
  <div id="extraResponseDiv">
  <?php $cnt=1; $c=0; ?>
  <?php foreach ($responseData as $key => $respvalue) { ?>   
  
    <?php if($cnt>1) { ?>
      <div class="res" id="res_<?=$cnt;?>">
    <?php } ?>  
      <div class="col-xs-12 p-0">
        <div class="panel panel-response">
          <div class="panel-body">
            <div class="form-group col-xs-12 p-0 pt-3 res-up-class">
            <div class="hiddenResInputs">
              <?php 
                $hid_resp_opt = [  'type'  => 'hidden', 
                                  'name'    => 'que_resp[]',
                                  'id'  => 'que_resp'.$c,
                                  'value' => $respvalue->id,
                              ];
                echo form_input($hid_resp_opt); 
              ?>
            </div>
              <label class="control-label lblRes">Response <?=$cnt;?> *</label>
                <div class="col-xs-12 p-0 mt-3">
                <?php
                  $txtRarea = [ 'name'    => 'response[]',
                                 'id'     => 'response_'.$cnt,
                                 'value'  => $respvalue->response,
                                 'class'  => 'data-sample-short ckeditor txtRes',
                              ];
                  echo form_textarea($txtRarea);
                ?>
              </div>
            </div>
            <div class="form-group col-xs-12 p-0 mt-3">
              <label class="control-label">Feedback</label>
              <div class="col-xs-12 p-0">
                <div class="flex-row align-items-end">
                  <div class="flex-col-7">
                    <?php
                      $txtFarea = [ 'name'    => 'feedback[]',
                                     'id'     => 'feedback_'.$cnt,
                                     'value'  => $respvalue->feedback,
                                     'class'  => 'data-sample-short ckeditor txtFb',
                                    ];
                      echo form_textarea($txtFarea);
                    ?>
                  </div> 
                  <?php if($cnt>1) { ?> 
                    <div class="flex-col text-right"><a class="btn btn-danger btn-md remove-response" data-resid="<?=$cnt;?>" data-qresid="<?=$respvalue->id;?>"><i class="flaticon-waste-bin"></i>Remove</a></div>
                  <?php } ?>               
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php if($cnt>1) { ?>
      </div>
    <?php } ?>
    <?php $cnt++; $c++; ?>
  <?php } //end foreach ($variable as $key => $value) ?>
</div>
<?php } // end if else if(!$responseData) ?>

<div class="col-xs-12 col-md-6 p-0 mb-3">
  <a class="btn btn-primary add-another-response">Add Another Response</a>
</div>
<div class="col-xs-12 mt-2 mb-3 p-0">
  <div class="hr-line mb-4"></div>
</div>


<!-- Question type oriented view Detractor -->

<?php if(!$detractorData) { ?>
  <div class="col-xs-12 p-0">
    <div class="panel panel-response panel-danger-light">
      <div class="panel-body">
        <div class="form-group col-xs-12 p-0 pt-3 detr-up-class">
          <div class="hiddenDetrInputs"></div>
          <label class="control-label lblDetr">Detractor 1 *</label>
          <div class="col-xs-12 p-0 mt-3">
            <?php
              $txtFarea = [ 'name'  => 'detract_resp[]',
                            'id'    => 'detract_resp_1',
                            'value' => '',
                            'class' => 'data-sample-short ckeditor txtDetrRes',
                          ];
              echo form_textarea($txtFarea);
            ?>
          </div>
        </div>
        <div class="form-group col-xs-12 p-0 mt-3">
          <label class="control-label">Feedback</label>
          <div class="col-xs-12 p-0">
            <div class="flex-row align-items-end">
              <div class="flex-col-7">
                <?php
                  $txtFarea = [ 'name'  => 'detract_feedback[]',
                                'id'    => 'detract_feedback_1',
                                'value' => '',
                                'class' => 'data-sample-short ckeditor txtDetrFb',
                              ];
                  echo form_textarea($txtFarea);
                ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="extraDetractorDiv"></div>
<?php } else { ?>
  <!-- UPDATE Detractor -->
  <div id="extraDetractorDiv">
    <?php $cnt=1; $c=0; ?>
    <?php foreach ($detractorData as $key => $detrvalue) { ?>

      <?php if($cnt>1) { ?>
        <div class="detr" id="detr_<?=$cnt;?>">
      <?php } ?>  

        <div class="col-xs-12 p-0">
          <div class="panel panel-response panel-danger-light">
            <div class="panel-body">
              <div class="form-group col-xs-12 p-0 pt-3 detr-up-class">
                <div class="hiddenDetrInputs">
                  <?php 
                    $hid_detr_opt = [  'type'   => 'hidden', 
                                      'name'    => 'ques_detr[]',
                                      'id'      => 'ques_detr'.$cnt,
                                      'value'   => $detrvalue->id,
                                  ];
                    echo form_input($hid_detr_opt); 
                  ?>
                </div>
                <label class="control-label lblDetr">Detractor <?=$cnt;?> *</label>
                <div class="col-xs-12 p-0 mt-3">
                  <?php
                    $txtFarea = [ 'name'    => 'detract_resp[]',
                                  'id'      => 'detract_resp_'.$cnt,
                                  'value'   => $detrvalue->response,
                                  'class'   => 'data-sample-short ckeditor txtDetrRes',
                                ];
                    echo form_textarea($txtFarea);
                  ?>
                </div>
              </div>
              <div class="form-group col-xs-12 p-0 mt-3">
                <label class="control-label">Feedback</label>
                <div class="col-xs-12 p-0">
                  <div class="flex-row align-items-end">
                    <div class="flex-col-7">
                      <?php
                        $txtFarea = [ 'name'    => 'detract_feedback[]',
                                      'id'      => 'detract_feedback_'.$cnt,
                                      'value'   => $detrvalue->feedback,
                                      'class'   => 'data-sample-short ckeditor txtDetrFb',
                                    ];
                        echo form_textarea($txtFarea);
                      ?>
                    </div>
                    <?php if($cnt>1) { ?>
                      <div class="flex-col text-right"><a class="btn btn-danger btn-md remove-detractor" data-detrid="<?=$cnt;?>" data-quesdetrid="<?=$detrvalue->id;?>"><i class="flaticon-waste-bin"></i>Remove</a></div>
                    <?php } ?>  
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
       <?php if($cnt>1) { ?>
        </div>
      <?php } ?>  
    <?php $cnt++; $c++; ?>
    <?php } ?>
  </div>
<?php } // end if else if(!$detractorData)?>

<div class="col-xs-12 col-md-6 p-0 mb-3">
  <a class="btn btn-danger add-another-detractor">Add Another Detractor</a>
</div>

<div class="col-xs-12 mt-3 mb-3 p-0 pb-2"> <div class="hr-line mb-4"></div> </div>

<div class="col-xs-12 p-0 mb-3">
  <div class="col-xs-12 col-md-6 p-0">
    <a href="javascript:void(0)" class="btn btn-warning btnPreview" data-toggle="modal" data-target="#quePreview" data-previewid="<?php echo $qid; ?>" style="display: none;">Preview Question</a>
  </div>
  <div class="col-xs-12 col-md-6 text-right p-0">
    <button type="submit" id="nextContent" class="btn btn-primary"><?php echo isset($qid) ? 'Save changes' : 'Save and next'?></button>
  </div>
</div>


<!------------------------------------------ Preview Modal ---------------------------------------->

<div class="modal fade modals-tel-theme" id="quePreview" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog w70p">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h2 class="modal-title">
          Question Preview
        </h2>
      </div>
      <div class="modal-body minheight" id="que_details">
        <div class="question-panel p-5">
          <div class="pb-3">
            <div class="alert alert-primary">
              Complete the sentence below with the appropriate from of verb.
            </div>
          </div>
          <div class="multiple mb-5">
            <i class="flaticon-question-mark mr-3"></i>
            <?php 
              $singleFib = '<div class="d-inline-block ml-3 mr-3 w120"><input type="text" class="form-control inline form-input-drag" name=""></div>';
              $question_sentence = strip_tags($queData->question_prompt); 
              $question_sentence = str_replace("{1}", $singleFib, $question_sentence);
              $question_sentence = str_replace("{2}", $singleFib, $question_sentence);
              $question_sentence = str_replace("{3}", $singleFib, $question_sentence);
              $question_sentence = str_replace("{4}", $singleFib, $question_sentence);
              echo $question_sentence;
            ?>
          </div>
          <div class="col-xs-12 p-0">
            <div class="col-xs-12 p-0 mt-5">
              <small class="alert alert-light-warning">
                <i class="ti ti-info-alt"></i>
                Drag the correct word into the paragraph
              </small>
            </div>
            <div class="col-xs-12 p-0 mt-4">              
              <?php foreach ($responseData as $key => $response) { ?>
                <button type="button" class="btn btn-primary-light"><?php echo $response->response;?>
                </button>
              <?php } ?>

              <?php foreach ($detractorData as $key => $detractor) { ?>
                <button type="button" class="btn btn-primary-light"><?php echo $detractor->response;?>
                </button>
              <?php } ?>              
            </div>
          </div>
        </div>
      </div>
      <div class="col-xs-12 mt-3 mb-3">
        <div class="hr-line"></div>
      </div>
      <div class="modal-footer text-right">
        <button type="button" class="btn btn-danger btn-md" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- ------------------------------------- Preview Modal ends ----------------------------------- -->


<script type="text/javascript">
  $(document).ready(function() {
    //ck editor
    var config = { height: 80, allowedContent :true, toolbar : 'short'};    
    $('.ckeditor').each(function(e) {
      CKEDITOR.replace( this.id, config);
    });

    //Preview
    var getCountVal = $("[data-previewid]").attr('data-previewid');
    getCountVal>0?$("[data-previewid]").show():$("[data-previewid]").hide();

    // Validating module content form
    $("#questionContent").validate({
      ignore: [],
      rules: {        
        title: {
          required: true
        },
        question_prompt: {
          required: function(textarea) {
            CKEDITOR.instances[textarea.id].updateElement(); // update textarea
            var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
            return editorcontent.length === 0;
          }
        },
        'answer_status[]': {
          required: true,
          minlength: 1
        },
        'weight[]': {
          required: function() {
            var qtype = $("#question_type").val();
            if (qtype == 1 || qtype == 3) {
              return true
            }
          },
          minlength: 1
        },
        'response[]': {
          required: function(textarea) {            
            $('textarea.ckeditor').each(function() {
              var $textarea = $(this).attr('id');
              CKEDITOR.instances[$textarea].updateElement();
            });
            var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
            return editorcontent.length === 0;            
          }
        },
        'detract_resp[]': {
          required: function(textarea) {           
            //CKEDITOR.instances[textarea.id].updateElement(); // update textarea
            $('textarea.ckeditor').each(function() {
              var $textarea = $(this).attr('id');
              CKEDITOR.instances[$textarea].updateElement();
            });
            var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
            return editorcontent.length === 0;            
          }
        },
      },
      messages: {
        title: { required: "Please enter question title" },
        question_prompt:{ required: "Please enter question" },        
        'response[]': { required: "Please enter response" },
        'detract_resp[]': { required: "Please enter detractor" },
      },
      submitHandler: function(form) { 
          console.log('form');
          //alert('here submit');
          submitQuestionContentForm();
          return false;

      },
      errorPlacement: function(error, $elem) {
        if ($elem.is('textarea')) {
          $elem.insertAfter($elem.next('div'));
        }
        error.insertAfter($elem);
      }      
    });

  }); // end ready

  // ================================== RESPONSE ================================== //
  // Add responses
  $(document).on('click', '.add-another-response', function () {

    var res_cnt = $("#extraResponseDiv .res").length + 2;
    //alert(res_cnt);
    $("#extraResponseDiv").append('<div class="res" id="res_'+res_cnt+'"><div class="col-xs-12 p-0"><div class="panel panel-response"><div class="panel-body"><div class="form-group col-xs-12 p-0 pt-3"><label class="control-label lblRes">Response '+res_cnt+' *</label><div class="col-xs-12 p-0 mt-3"><textarea id="response_'+res_cnt+'" name="response[]" class="data-sample-short ckeditor txtRes"></textarea></div></div><div class="form-group col-xs-12 p-0 mt-3"><label class="control-label">Feedback</label><div class="col-xs-12 p-0"><div class="flex-row align-items-end"><div class="flex-col-7"><textarea id="feedback_'+res_cnt+'" name="feedback[]" class="data-sample-short ckeditor txtFb"></textarea></div><div class="flex-col text-right"><a class="btn btn-danger btn-md remove-response" data-resid="'+res_cnt+'"><i class="flaticon-waste-bin"></i>Remove</a></div></div></div></div></div></div></div><div>').
        ready(function(){     
          var mcqConfig = { height: 80, toolbar : 'short'};            
          CKEDITOR.replace('response_'+res_cnt, mcqConfig);         
          CKEDITOR.replace('feedback_'+res_cnt, mcqConfig);
          CKEDITOR.instances['response_'+res_cnt].updateElement();    
          CKEDITOR.instances['feedback_'+res_cnt].updateElement();
        }); 
    });


  // Ajax request to fetch data for preview
  $(document).on('click' , '.btnPreview', function() {
    var que_id = $("#que_cont_id").val();
    var qtype = $('#question_type').val();
   // alert(que_id + " = " + qtype); return false;
    $.ajax({
        url:  "<?php echo bs();?>questions/get_question_details",      
        type:   "POST",  
        dataType: 'json',           
        data: {'qid' : que_id, 'qtype' : qtype},
        success: function(data) {
          console.log(data);     
          var que_opts = '';

          $.each(data.responses, function (key, val) { 
            que_opts = que_opts +'<button type="button" class="btn btn-primary-light">'+val+'</button>';            
          }); 

          $.each(data.detractors, function (key, val) { 
            que_opts = que_opts +'<button type="button" class="btn btn-primary-light">'+val+'</button>';            
          }); 
          //que_opts = que_opts +'</div';
          var que_title = '<div class="question-panel p-5"><div class="pb-3"><div class="alert alert-primary">Complete the sentence below with the appropriate from of verb.</div></div><div class="multiple mb-5"><i class="flaticon-question-mark mr-3"></i> '+data.title+'  </div><div class="col-xs-12 p-0"><div class="col-xs-12 p-0 mt-5"><small class="alert alert-light-warning"><i class="ti ti-info-alt"></i>Drag the correct word into the paragraph</small></div><div class="col-xs-12 p-0 mt-4">'+que_opts+' </div></div> </div>'; 

          $('#que_details').html(que_title); 
        }
      }); 
  });

  // Remove responses
  $(document).on('click', '.remove-response', function () {
    var resdiv = $(this).data('resid');
    var que_res_id = $(this).data('qresid');
    var que_id = $("#que_cont_id").val();
    //alert(resdiv + " = " + que_res_id);
    //return false;

    $.confirm({
      title: 'Confirm!',
      content: 'Are you sure?',
      buttons: {
        confirm: function() {             
          var mcqConfig = {height: 80, toolbar : 'short'};

          var h_resdiv = resdiv - 1;
          $('#que_res'+h_resdiv).val('');
          $('#que_res'+h_resdiv).remove();
          $("#res_"+resdiv).remove();

          var i_res = 2;
          $('.res').each(function () {
            $(this).attr('id', 'res_'+i_res);
            setTimeout( function () {
              CKEDITOR.instances['response_'+i_res].updateElement(); 
              CKEDITOR.instances['feedback_'+i_res].updateElement();
            }, 100 );
            i_res++;
          });
          
          //Re-arrange response label
          var lb_i=1;
          $('.lblRes').each(function () {
            $(this).text('Response '+lb_i);
            $(this).parent().siblings('div').find('a.remove-response').attr('data-resid', lb_i);
            lb_i++;
          });         
          
          //Re-arrange response
          var resp_i=1;                     
          $('.txtRes').each(function(){
            $(this).attr('id', 'response_'+resp_i); 
            CKEDITOR.instances['response_'+resp_i].updateElement(); 
            resp_i++; 
          });

          //Re-arrange response feedback
          var fb_i=1;                     
          $('.txtFb').each(function(){
            $(this).attr('id', 'feedback_'+fb_i);            
            CKEDITOR.instances['feedback_'+fb_i].updateElement(); 
            fb_i++; 
          });

          if(que_res_id) {
            
            $.ajax({
                url:  "<?php echo bs();?>questions/remove_question_response_drag",      
                type:   "POST",  
                dataType: 'json',           
                data:   {'id': que_res_id, 'qid' : que_id}, 
                beforeSend: function() {
                  //$("#imgLoader").show();
                },
                success: function(data) {
                  
                  if(data.code==200) {
                    $('.res-up-class .hiddenResInputs').html(''); 
                    var i=0;
                    $.each(data.respids, function (key, val) {  
                      $('.res-up-class .hiddenResInputs').append('<input type="hidden" name="que_resp['+i+']" id="que_res'+i+'" value="'+val+'">');
                      i++;
                    }); 
                    CommanJS.getDisplayMessgae(data.code, data.success);
                  }else {
                    CommanJS.getDisplayMessgae(data.code, data.error);
                  }            
                }
              });
              return true;
            }else {
              CommanJS.getDisplayMessgae(200, 'Response removed');return true;
            }
          },
          cancel: function() {
            return true;
          }
        }
      });
  });

  // ================================== DETRACTOR ================================== //

  // Add detractor
  $(document).on('click', '.add-another-detractor', function () {

    var detr_cnt = $("#extraDetractorDiv .detr").length + 2;
    //alert(res_cnt);
    $("#extraDetractorDiv").append('<div class="detr" id="detr_'+detr_cnt+'"><div class="col-xs-12 p-0"><div class="panel panel-response panel-danger-light"><div class="panel-body"><div class="form-group col-xs-12 p-0 pt-3"><label class="control-label lblDetr">Detractor '+detr_cnt+' *</label><div class="col-xs-12 p-0 mt-3"><textarea name="detract_resp[]" id="detract_resp_'+detr_cnt+'" class="data-sample-short ckeditor txtDetrRes" ></textarea></div></div><div class="form-group col-xs-12 p-0 mt-3"><label class="control-label">Feedback</label><div class="col-xs-12 p-0"><div class="flex-row align-items-end"><div class="flex-col-7"><textarea name="detract_feedback[]" cols="40" rows="10" id="detract_feedback_'+detr_cnt+'" class="data-sample-short ckeditor txtDetrFb"></textarea></div><div class="flex-col text-right"><a class="btn btn-danger btn-md remove-detractor" data-detrid="'+detr_cnt+'"><i class="flaticon-waste-bin"></i>Remove</a></div></div></div></div></div></div></div><div>').
        ready(function(){     
          var mcqConfig = { height: 80, toolbar : 'short'};            
          CKEDITOR.replace('detract_resp_'+detr_cnt, mcqConfig);          
          CKEDITOR.replace('detract_feedback_'+detr_cnt, mcqConfig);          
          CKEDITOR.instances['detract_resp_'+detr_cnt].updateElement();    
          CKEDITOR.instances['detract_feedback_'+detr_cnt].updateElement(); 
        });   
    });

  // Remove detractor
  $(document).on('click', '.remove-detractor', function () {
    var detrdiv = $(this).data('detrid');
    var que_detr_id = $(this).data('quesdetrid');
    var que_id = $("#que_cont_id").val();
    //alert(detrdiv + " = " + que_detr_id);
    //return false;

    $.confirm({
      title: 'Confirm!',
      content: 'Are you sure?',
      buttons: {
        confirm: function() {             
          var mcqConfig = {height: 80, toolbar : 'short'};

          var h_detrdiv = detrdiv - 1;
          $('#que_detr'+h_detrdiv).val('');
          $('#que_detr'+h_detrdiv).remove();
          $("#detr_"+detrdiv).remove();

          var i_detr = 2;
          $('.detr').each(function () {
            $(this).attr('id', 'detr_'+i_detr);
            setTimeout( function () {
              CKEDITOR.instances['detract_resp_'+i_detr].updateElement(); 
              CKEDITOR.instances['detract_feedback_'+i_detr].updateElement();
            }, 100 );
            i_detr++;
          });
          
          //Re-arrange detractor label
          var lb_i=1;
          $('.lblDetr').each(function () {
            $(this).text('Detractor '+lb_i);
            $(this).parent().siblings('div').find('a.remove-detractor').attr('data-detrid', lb_i);
            lb_i++;
          });         
          
          //Re-arrange detractor response
          var detr_i=1;                     
          $('.txtDetrRes').each(function(){
            $(this).attr('id', 'detract_resp_'+detr_i); 
            CKEDITOR.instances['detract_resp_'+detr_i].updateElement(); 
            detr_i++; 
          });

          //Re-arrange detractor feedback
          var fb_i=1;                     
          $('.txtDetrFb').each(function(){
            $(this).attr('id', 'detract_feedback_'+fb_i);            
            CKEDITOR.instances['detract_feedback_'+fb_i].updateElement(); 
            fb_i++; 
          });

          if(que_detr_id) {
            
            $.ajax({
                url:  "<?php echo bs();?>questions/remove_question_detractor_drag",      
                type:   "POST",  
                dataType: 'json',           
                data:   {'id': que_detr_id, 'qid' : que_id}, 
                beforeSend: function() {
                  //$("#imgLoader").show();
                },
                success: function(data) {
                  
                  if(data.code==200) {
                    $('.detr-up-class .hiddenDetrInputs').html(''); 
                    var i=0;
                    $.each(data.detrids, function (key, val) {  
                      $('.detr-up-class .hiddenDetrInputs').append('<input type="hidden" name="ques_detr['+i+']" id="ques_detr'+i+'" value="'+val+'">');
                      i++;
                    }); 
                    CommanJS.getDisplayMessgae(data.code, data.success);
                  }else {
                    CommanJS.getDisplayMessgae(data.code, data.error);
                  }            
                }
              });
              return true;
            }else {
              CommanJS.getDisplayMessgae(200, 'Detractor removed');return true;
            }
          },
          cancel: function() {
            return true;
          }
        }
      });
  });

</script>

