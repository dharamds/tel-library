<div class="static-content-wrapper">
   <div class="static-content">
      <div class="page-content">
         <ol class="breadcrumb">
            <li class=""><a href="<?php bs(); ?>">Home</a></li>
            <li class=""><a href="<?php bs(); ?>questions/">Questions</a></li>
            <li class="active"><a href="">Detail</a></li>
         </ol>
         <div class="container-fluid">
            <div class="tab-pane active" id="tab-about">
               <div class="panel panel-default">
                  <div class="panel-heading">
                     <h2>Details</h2>
                  </div>
                  <div class="panel-body">
                     <div class="about-area">
                        <div class="table-responsive">
                           <table class="table about-table">
                              <tbody>
                                 <tr>
                                    <th>Type</th>
                                    <td width="5%"></td>
                                    <td><?php $options = [ '' => 'Select Question Type',
                                             1 => 'true/false', 2 => 'Single choice', 
                                             3 => 'Multiple choice', 4 => 'Descriptive', 
                                             5 => 'Paring', 6 => 'Fill in the blank', 
                                             7 => 'Mathematical'
                                          ]; 
                                          echo $options[$questionsData->question_type];?>
                                    </td>
                                 </tr>
                                 <tr>
                                    <th>Question</th>
                                    <td width="5%"></td>
                                    <td><?php echo $questionsData->name;?></td>
                                 </tr>                  
                                 <tr>
                                    <th>Options</th>
                                    <td width="5%"></td>
                                    <td> 
                                       <table>
                                       <?php  //pr($answerData);
                                       foreach ($answerData as $key => $answer) {
                                          ?>
                                          <tr>
                                             <td><?php echo $answer->option; ?></td>
                                             <td width="2%">&nbsp;</td>
                                             <td style="color:green;"><?php echo ($answer->answer==1) ? 'Correct' : ''; ?></td>
                                          </tr>
                                          <?php                                         
                                       }
                                       ?></table>
                                    </td>
                                 </tr>
                                 <tr>
                                    <th>Created by</th>
                                    <td width="5%"></td>
                                    <td><?php 
                                       $authorData = $this->ion_auth->user($questionsData->created_by)->row(); 
                                       echo ucfirst($authorData->first_name); echo " ";  
                                       echo ucfirst($authorData->last_name);
                                       ?></td>
                                 </tr>                                 
                                 <tr>
                                    <th>Created on</th>
                                    <td width="5%"></td>
                                    <td><?php echo date('m-d-Y',strtotime($questionsData->created)); ?></td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- .container-fluid -->
</div>
<!-- #page-content -->
