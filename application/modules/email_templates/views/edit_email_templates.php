

<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            <div class="page-heading">
              <h5>Add Email Template</h5>
            </div> 
            <ol class="breadcrumb">
                <li class=""><a href="<?php bs(); ?>">Home</a></li>
                <li class=""><a href="<?php bs(); ?>email_templates/email_templates_list">Email Templates</a></li>
                <li class="active"><a href="">Edit Email Template</a></li>
            </ol>
            <div class="container-fluid">
                <div class="panel panel-info panel-grid">
                    <div class="panel-heading brd-0 pt-0"></div>
                    <div class="panel-body">
                          <?php isset($emailTemplates->id) ? $emailTemplatesId = $emailTemplates->id  : $emailTemplatesId = '' ?>
                        <?php echo form_open('email_templates/edit_email_templates/'.$emailTemplatesId, array('id' => 'edit_email_templates_form_validation')); ?>
                        
                        
                        
                           <?php isset($emailTemplates->from_email) ? $fromEmail = $emailTemplates->from_email : $fromEmail = '' ?>
                        <div class="form-group col-xs-12 p-0">
                            <label for="from_email" class="col-md-12 control-label">Sender Email</label>
                            <div class="col-md-6">                        
                                <?php
                                echo form_input([
                                    'name' => 'from_email',
                                    'id' => 'from_email',
                                    'class' => 'form-control',
                                    'value' => $fromEmail,
                                    'placeholder' => 'Enter From Name Detail']);
                                ?>
                                <?php echo form_error('from_email', '<div class="error">', '</div>'); ?>
                            </div>
                        </div> 


                         <?php isset($emailTemplates->from_name) ? $fromName = $emailTemplates->from_name : $fromName = '' ?>
                        <div class="form-group col-xs-12 p-0">
                            <label for="from_name" class="col-md-12 control-label">Sender Name</label>
                            <div class="col-md-6">                        
                                <?php
                                echo form_input([
                                    'name' => 'from_name',
                                    'id' => 'from_name',
                                    'class' => 'form-control',
                                    'value' => $fromName,
                                    'placeholder' => 'Enter From Name Detail']);
                                ?>
                                <?php echo form_error('from_name', '<div class="error">', '</div>'); ?>
                            </div>
                        </div> 
                        
                        
                        
                        
                        <?php isset($emailTemplates->temp_code) ? $tempCode = $emailTemplates->temp_code : $tempCode = '' ?>
                        <div class="form-group col-xs-12 p-0">
                            <label for="temp_code" class="col-md-12 control-label">Temporary Code</label>
                            <div class="col-md-6">                        
                                <?php
                                echo form_input([
                                    'name' => 'temp_code',
                                    'id' => 'temp_code',
                                    'class' => 'form-control',
                                    'value' => $tempCode,
                                    'placeholder' => 'Enter Temporary Code']);
                                ?>
                                <?php echo form_error('temp_code', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>                  
                        <?php isset($emailTemplates->subject) ? $subject = $emailTemplates->subject : $subject = '' ?>
                        <div class="form-group col-xs-12 p-0">
                            <label for="subject" class="control-label">Subject</label>
                            <div class="col-md-6">                        
                                <?php
                                echo form_input([
                                    'name' => 'subject',
                                    'id' => 'subject',
                                    'class' => 'form-control',
                                    'value' => $subject,
                                    'placeholder' => 'Enter Subject']);
                                ?>
                                <?php echo form_error('subject', '<div class="error">', '</div>'); ?>
                            </div>
                        </div> 

                        <?php isset($emailTemplates->message) ? $message = $emailTemplates->message : $message = '' ?>
                        <div class="form-group col-xs-12 p-0">
                            <label for="message" class="col-md-12 control-label">Message</label>
                            <div class="col-md-9">                        
                                <?php
                                echo form_textarea([
                                    'name' => 'message',
                                    'id' => 'message',
                                    'class' => 'form-control editor',
                                    'value' => $message,
                                    'rows' => '5']);
                                ?>
                                <?php echo form_error('message', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>
                        
                       

                        <div class="form-group">
                            <div class="col-md-12 text-right pb-4 pt-4">
                                <input type="submit" class="finish btn-primary btn" value="Update">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .container-fluid -->
</div>
<!-- #page-content -->

<script>

    $(document).ready(function() {
        var config = {
         toolbarGroups: [{
            "name": "basicstyles",
            "groups": ["basicstyles"]
         },
         {
            "name": "links",
            "groups": ["links"]
         },
         {
            "name": "paragraph",
            "groups": ["list", "blocks"]
         },
         {
            "name": "document",
            "groups": ["mode"]
         },
         {
            "name": "insert",
            "groups": ["insert"]
         },
         {
            "name": "styles",
            "groups": ["styles"]
         },
         {
            "name": "about",
            "groups": ["about"]
         }
         ],
         removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar'
      };
      // to assign ckeditor
      CKEDITOR.replace('message',config);
    });
</script>


<script>
    $(document).ready(function () {
        jQuery.validator.addMethod("emailExt", function (value, element, param) {
            return value.match(/^[a-zA-Z0-9_\.%\+\-]+@[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,}$/);
        }, "Please enter valid email address");

        $("#edit_email_templates_form_validation").validate({
            
            ignore:[],

            rules: {
                temp_code: {
                    required: true,
                },
                subject: {
                    required: true
                },
                message: {
                    required: true
                },
                from_email: {
                    required: true,
                    emailExt: true

                },
                from_name: {
                    required: true
                },
            },
            messages: {
                temp_code: {
                    required: 'Please enter template code',
                },
                subject: {
                    required: 'Please enter subject'
                },
                message: {
                    required: 'Please enter message'
                },
                from_email: {
                    required: 'Please enter email',

                },
                from_name: {
                    required: 'Please enter name'
                },
            },
             errorPlacement: function(error, $elem) {
            if ($elem.is('textarea')) {
                $elem.insertAfter($elem.next('div'));
            }
            error.insertAfter($elem);
        },
        });
    });
</script>



