
            <div class="container-fluid">
                <div class="panel panel-info panel-grid" data-widget='{"draggable": "false"}'>
                    <div class="panel-heading brd-0 pt-0"></div>
                    <div class="panel-body">
                        <?php echo form_open('email_templates/email_templates/add_email_templates', array('id' => 'add_email_templates_form_validation')); ?>

                        <div class="form-group col-xs-12 p-0">
                            <label for="from_email" class="col-md-12 control-label">Sender Email</label>
                            <div class="col-md-6">                        
                                <?php
                                echo form_input([
                                    'name' => 'from_email',
                                    'id' => 'from_email',
                                    'class' => 'form-control',
                                    'placeholder' => 'Enter From Name Detail']);
                                ?>
                                <?php echo form_error('from_email', '<div class="error">', '</div>'); ?>
                            </div>
                        </div> 

                        <div class="form-group col-xs-12 p-0">
                            <label for="from_name" class="col-md-12 control-label">Sender Name</label>
                            <div class="col-md-6">                        
                                <?php
                                echo form_input([
                                    'name' => 'from_name',
                                    'id' => 'from_name',
                                    'class' => 'form-control',
                                    'placeholder' => 'Enter From Name Detail']);
                                ?>
                                <?php echo form_error('from_name', '<div class="error">', '</div>'); ?>
                            </div>
                        </div> 

                        <div class="form-group col-xs-12 p-0">
                            <label for="temp_code" class="col-md-12 control-label">Template Code</label>
                            <div class="col-md-6">                        
                                <?php
                                echo form_input([
                                    'name' => 'temp_code',
                                    'id' => 'temp_code',
                                    'class' => 'form-control',
                                    'placeholder' => 'Enter Temporary Code']);
                                ?>
                                <?php echo form_error('temp_code', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>                  

                        <div class="form-group col-xs-12 p-0">
                            <label for="subject" class="col-md-12 control-label">Subject</label>
                            <div class="col-md-6">                        
                                <?php
                                echo form_input([
                                    'name' => 'subject',
                                    'id' => 'subject',
                                    'class' => 'form-control',
                                    'placeholder' => 'Enter Subject']);
                                ?>
                                <?php echo form_error('subject', '<div class="error">', '</div>'); ?>
                            </div>
                        </div> 


                        <div class="form-group col-xs-12 p-0">
                            <label for="message" class="col-md-12 control-label">Message</label>
                            <div class="col-md-9">                        
                                <?php
                                echo form_textarea([
                                    'name' => 'message',
                                    'id' => 'message',
                                    'class' => 'form-control editor',
                                    'rows' => '5']);
                                ?>
                                <?php echo form_error('message', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 text-right pb-4">
                                <input type="submit" class="finish btn-primary btn" value="Save">
                            </div>
                        </div>
                    </div>
                </div>
            </div>





<script>

    $(document).ready(function() {
        var config = {
         toolbarGroups: [{
            "name": "basicstyles",
            "groups": ["basicstyles"]
         },
         {
            "name": "links",
            "groups": ["links"]
         },
         {
            "name": "paragraph",
            "groups": ["list", "blocks"]
         },
         {
            "name": "document",
            "groups": ["mode"]
         },
         {
            "name": "insert",
            "groups": ["insert"]
         },
         {
            "name": "styles",
            "groups": ["styles"]
         },
         {
            "name": "about",
            "groups": ["about"]
         }
         ],
         removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar'
      };
      // to assign ckeditor
      CKEDITOR.replace('message',config);
    });
</script>


<script>
    $(document).ready(function () {
        
        jQuery.validator.addMethod("emailExt", function (value, element, param) {
            return value.match(/^[a-zA-Z0-9_\.%\+\-]+@[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,}$/);
        }, "Please enter valid email address");

        $("#add_email_templates_form_validation").validate({
            
            ignore:[],

            rules: {
                temp_code: {
                    required: true,
                  
                },
                subject: {
                    required: true
                },
                message: {
                    required: function (textarea) {
                        CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                        return editorcontent.length === 0;
                    }
                },
                from_email: {
                    required: true,
                    //  email: true,
                    emailExt: true

                },
                from_name: {
                    required: true
                },
            },
            messages: {
                temp_code: {
                    required: 'Please enter template code',
                    
                },
                subject: {
                    required: 'Please enter subject'
                },
                message: {
                    required: 'Please enter message'
                },
                from_email: {
                    required: 'Please enter email',

                },
                from_name: {
                    required: 'Please enter name'
                },
            },
             errorPlacement: function(error, $elem) {
            if ($elem.is('textarea')) {
                $elem.insertAfter($elem.next('div'));
            }
            error.insertAfter($elem);
        },
        });
    });
</script>

<?php
$jsVars = [
    'ajax_call_root' => base_url(),
];
?>



