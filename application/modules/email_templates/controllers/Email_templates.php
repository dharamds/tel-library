<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Email_templates extends MY_Controller
{

    public function __construct()
    {

        parent::__construct();
        //Do your magic here

        $this->load->module('template');
        $this->load->model('common_model');
        $this->load->helper(['html', 'form']);
        $this->load->library('form_validation');

        if (!$this->ion_auth->logged_in()) :
            redirect('users/auth', 'refresh');
        endif;
    }

    /**
     * setup method
     * @description this function use to create container
     * @return void
     */
    public function email_templates_list()
    {
        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Email Templates', 'link' => '', 'class' => ''];
        $data['emailTeplates'] = $this->common_model->getAllData('email_templates', '*', '');
        $data['page'] = "email_templates/email_templates_list";
        $this->template->template_view($data);
    }

    /**
     * add_new method
     * @description this method is use to insert site creatation data in databse
     * @param string, numbers
     * @return void
     */
    public function add_email_templates()
    {
        if ($this->input->post()) {
            $validation_rules = [
                [
                    'field' => 'temp_code',
                    'label' => 'temp_code',
                    'rules' => 'required',
                    'errors' => [
                        'required' => 'Please enter Temporary Code',
                    ],
                ],
                [
                    'field' => 'subject',
                    'label' => 'subject',
                    'rules' => 'required',
                    'errors' => [
                        'required' => 'Please enter Subject',
                    ],
                ],
                [
                    'field' => 'message',
                    'label' => 'message',
                    'rules' => 'required',
                    'errors' => [
                        'required' => 'Please select message',
                    ],
                ],
                [
                    'field' => 'from_email',
                    'label' => 'from_email',
                    'rules' => 'required',
                    'errors' => [
                        'required' => 'Please select From Email Detail',
                    ],
                ],
                [
                    'field' => 'from_name',
                    'label' => 'from_name',
                    'rules' => 'required',
                    'errors' => [
                        'required' => 'Please select From Name',
                    ],
                ],
            ];
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run() == FALSE) {
                $data['page'] = "email_templates/add_email_templates";
                $this->template->template_view($data);
            } else {
                $tempCode = strtoupper(post('temp_code'));
                $fromName = ucwords(strtolower(post('temp_code')));

                $data = array(
                    'temp_code' => $tempCode,
                    'subject' => post('subject'),
                    'message' => post('message'),
                    'from_email' => post('from_email'),
                    'from_name' => $fromName
                );

                $this->common_model->InsertData('email_templates', $data);
                set_flashdata('success', 'Email Template has been added successfully');
                redirect('email_templates/email_templates_list/', 'refresh');
            }
        } else {
            $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
            $data['breadcrumb'][]           = ['title' => 'Email Templates', 'link' => base_url('email_templates/email_templates_list'), 'class' => ''];
            $data['breadcrumb'][]           = ['title' => 'Add', 'link' => '', 'class' => 'active'];
            $data['page'] = "email_templates/add_email_templates";
            $this->template->template_view($data);
        }
    }

    public function edit_email_templates($id = NULL)
    {
        if ($this->input->post()) {

            $validation_rules = [
                [
                    'field' => 'temp_code',
                    'label' => 'temp_code',
                    'rules' => 'required',
                    'errors' => [
                        'required' => 'Please enter Temporary Code',
                    ],
                ],
                [
                    'field' => 'subject',
                    'label' => 'subject',
                    'rules' => 'required',
                    'errors' => [
                        'required' => 'Please enter Subject',
                    ],
                ],
                [
                    'field' => 'message',
                    'label' => 'message',
                    'rules' => 'required',
                    'errors' => [
                        'required' => 'Please select message',
                    ],
                ],
                [
                    'field' => 'from_email',
                    'label' => 'from_email',
                    'rules' => 'required',
                    'errors' => [
                        'required' => 'Please select From Email Detail',
                    ],
                ],
                [
                    'field' => 'from_name',
                    'label' => 'from_name',
                    'rules' => 'required',
                    'errors' => [
                        'required' => 'Please select From Name',
                    ],
                ],
            ];
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run() == FALSE) {
                $data['page'] = "email_templates/edit_email_templates";
                $this->template->template_view($data);
                set_flashdata('error', 'Please fill all fields !');
                redirect('email_templates/edit_email_templates/' . $id);
            } else {

                $tempCode = strtoupper(post('temp_code'));
                $fromName = ucwords(strtolower(post('from_name')));

                $data = array(
                    'temp_code' => $tempCode,
                    'subject' => post('subject'),
                    'message' => post('message'),
                    'from_email' => post('from_email'),
                    'from_name' => $fromName
                );



                $this->common_model->UpdateDB('email_templates', ['id' => $id], $data);
                set_flashdata('success', 'Email Template has been updated Successfully');
                redirect('email_templates/email_templates_list/', 'refresh');
            }
        } else {
            $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
            $data['breadcrumb'][]           = ['title' => 'Email Templates', 'link' => base_url('email_templates/email_templates_list'), 'class' => ''];
            $data['breadcrumb'][]           = ['title' => 'Edit', 'link' => '', 'class' => 'active'];
            $data['emailTemplates'] = $this->common_model->getDataById('email_templates', '*', ['id' => $id]);
            $data['page'] = "email_templates/edit_email_templates";
            $this->template->template_view($data);
        }
    }

    function check_default($post_string)
    {
        if ($this->input->post('country') === 'selectcountry') {
            $this->form_validation->set_message('country_check', 'Please choose your country.');
            return FALSE;
        } else {
            return TRUE;
        }
    }
}
