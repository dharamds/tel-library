            <div class="container-fluid">
                <div class="panel panel-info panel-grid">
                    <div class="panel-heading brd-0 pt-0"></div>
                    <div class="panel-body">
                        <?php isset($detail->id) ? $detailId = $detail->id  : $detailId = '' ?>
                        <?php echo form_open('notifications_templates/save/' . $details->id, array('id' => 'form_validation')); ?>



                        <div class="form-group col-xs-12 p-0">
                            <label for="temp_code" class="col-md-12 control-label">Container</label>
                            <div class="col-md-6">
                                <?php isset($details->container_id) ? $currentcontainers = $details->container_id  : $currentcontainers = '' ?>
                                <?php echo form_dropdown('container_id', $containers, $currentcontainers, ['class' => 'form-control']); ?>
                                <?php echo form_error('container_id', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group col-xs-12 p-0">
                            <label for="temp_code" class="col-md-12 control-label">Template Code</label>
                            <div class="col-md-6">
                                <?php
                                echo form_input([
                                    'name' => 'temp_code',
                                    'id' => 'temp_code',
                                    'class' => 'form-control',
                                    'value' => $details->temp_code,
                                    'placeholder' => 'Enter Template Code'
                                ]);
                                ?>
                                <?php echo form_error('temp_code', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group col-xs-12 p-0">
                            <label for="temp_code" class="col-md-12 control-label">Type</label>
                            <div class="col-md-6">
                                <?php 
                                $current_type = ["" => "Select Type", "1" => "Course", "2" => "Assignment", "3" => "Activity", "4" => "Grader"]; 
                                isset($details->type) ? $current_type = $details->type  : $current_type = ''
                                ?>
                                <?php echo form_dropdown('type', ["" => "Select Type", "1" => "Course", "2" => "Assignment", "3" => "Activity", "4" => "Grader"], $current_type, ['class' => 'form-control']); ?>
                                <?php echo form_error('type', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group col-xs-12 p-0">
                            <label for="from_email" class="col-md-12 control-label">From Email</label>
                            <div class="col-md-6">
                                <?php
                                echo form_input([
                                    'name' => 'from_email',
                                    'id' => 'from_email',
                                    'class' => 'form-control',
                                    'value' => $details->from_email,
                                    'placeholder' => 'Enter From Name Detail'
                                ]);
                                ?>
                                <?php echo form_error('from_email', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>


                        <div class="form-group col-xs-12 p-0">
                            <label for="from_name" class="col-md-12 control-label">From Name</label>
                            <div class="col-md-6">
                                <?php
                                echo form_input([
                                    'name' => 'from_name',
                                    'id' => 'from_name',
                                    'class' => 'form-control',
                                    'value' => $details->from_name,
                                    'placeholder' => 'Enter From Name Detail'
                                ]);
                                ?>
                                <?php echo form_error('from_name', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group col-xs-12 p-0">
                            <label for="subject" class="col-md-12 control-label">Subject</label>
                            <div class="col-md-6">
                                <?php
                                echo form_input([
                                    'name' => 'subject',
                                    'id' => 'subject',
                                    'class' => 'form-control',
                                    'value' => $details->subject,
                                    'placeholder' => 'Enter Subject'
                                ]);
                                ?>
                                <?php echo form_error('subject', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>

                        <div class="form-group col-xs-12 p-0">
                            <label for="message" class="col-md-12 control-label">Message</label>
                            <div class="col-md-9">
                                <?php
                                echo form_textarea([
                                    'name' => 'message',
                                    'id' => 'message',
                                    'class' => 'form-control editor',
                                    'value' => $details->message,
                                    'rows' => '5'
                                ]);
                                ?>
                                <?php echo form_error('message', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 text-left pb-4 pt-4">
                                <a class="finish btn-primary btn" href="<?php bs('/notifications'); ?>">Back</a>
                            </div>
                            <div class="col-md-6 text-right pb-4 pt-4">
                                <input type="submit" class="finish btn-primary btn" value="Save">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #page-content -->

            <script>
                $(document).ready(function() {
                    var config = {
                        height: 320,
                        toolbar: 'short',
                    };
                    // to assign ckeditor
                    CKEDITOR.replace('message', config);

                    $("#form_validation").validate({
                        ignore: [],
                        rules: {
                            temp_code: {
                                required: true,
                            },
                            subject: {
                                required: true
                            },
                            message: {
                                required: true
                            },
                            from_email: {
                                required: true,
                                //  email: true,
                                emailExt: true

                            },
                            from_name: {
                                required: true
                            },
                        },
                        messages: {
                            temp_code: {
                                required: 'Please enter template code',
                            },
                            subject: {
                                required: 'Please enter subject'
                            },
                            message: {
                                required: 'Please enter message'
                            },
                            from_email: {
                                required: 'Please enter email',

                            },
                            from_name: {
                                required: 'Please enter name'
                            },
                        },
                        errorPlacement: function(error, $elem) {
                            if ($elem.is('textarea')) {
                                $elem.insertAfter($elem.next('div'));
                            }
                            error.insertAfter($elem);
                        },
                        submitHandler: function(form) { // for demo
                            return true; // for demo
                        }
                    });
                });
            </script>