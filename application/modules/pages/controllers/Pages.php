<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Pages extends MY_Controller
{

    public function __construct()
    {

        //ini_set('display_errors', 1);
        $this->load->library('session');
        parent::__construct();
        $this->load->module('template');
        $this->load->model(['Common_model', 'Pages_model']);
        $this->load->helper(array('html', 'form'));
        $this->load->library('form_validation');

        if (!$this->ion_auth->logged_in()) :
            redirect('users/auth', 'refresh');
        endif;
          // get controller permissions
          if ($this->current_user_permissions = $this->get_permissions()) {
            $this->add_permission = $this->current_user_permissions->add_per;
            $this->edit_permission = $this->current_user_permissions->edit_per;
            $this->delete_permission = $this->current_user_permissions->delete_per;
            $this->view_permission = $this->current_user_permissions->view_per;
            $this->list_permission = $this->current_user_permissions->list_per;
        }
    }

    /**
     * index method
     * @description this function use to display list of polls
     * @return void
     */
    public function index()
    {   
        ///////////////////////////////
        //$testdata = $this->test();
        //system_send_email(['template_code'=> 'Poll Submission','email' =>'shrikant@datalogysoftware.com','testdata' => $testdata]);
        /////////////////////////////////
        $data['page'] = "pages/list";
        if (!$this->list_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect('users/auth', 'refresh');
        endif;
        $data['permissions'] = $this->current_user_permissions;   

        $this->template->template_view($data);
    }

    /**
     * getLists method 
     * @description this function called via ajax request, use to display list of course
     * @return void
     */
    public function getLists()
    {
        $pagesData = $this->Pages_model->getRows($_POST);
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $pagesData['total'],
            "recordsFiltered" => $pagesData['total'],
            "data" => $pagesData['result'],
        );
        echo json_encode($output);
    }

    /**
     * add/edit method 
     * @description this method is use to add/edit new poll
     * @return void
     */
    public function add($id = NULL)
    {   
        if (!$this->add_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect($_SERVER['HTTP_REFERER'], 'refresh');
        endif;

        if ($this->input->post()) {
            $this->form_validation->set_rules('name', 'Name', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $data['page'] = "Pages/add";
                set_flashdata('error', 'Unable to add pages, try again');
                $this->template->template_view($data);
            } else {
                $requestData = $this->input->post();
                $slug = slugify(post('name'));
                $_SESSION['slug'] = 0;
                $this->Pages_model->checkSlug($slug);
                unset( $_SESSION['slug']);
                $slug = $_SESSION['slugValue'];
                $requestData['slug'] = $slug;
                if($requestData['status'] == ""){
                    $requestData['status'] = '1';
                }
                $this->Common_model->InsertData('pages', $requestData);
                $this->session->set_flashdata('success', 'Page add successfully.');
                redirect('pages','refresh');
            }
        } else {
            $data['parent'] = $this->Pages_model->getParentList();
            $data['page'] = "Pages/add";
            $this->template->template_view($data);
        }
    }

    public function edit($id = NULL)
    {
        if (!$this->edit_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect($_SERVER['HTTP_REFERER'], 'refresh');
        endif;

        if ($this->input->post()) {
            $requestData = $this->input->post();
            $this->Common_model->UpdateDB('pages','id = '.$id,$requestData);
            $this->session->set_flashdata('success', 'Page edited successfully.');
            redirect('pages','refresh');
        }
        
        $data['requestData'] = $this->Common_model->getDataById('pages','*',['id' => $id]);
        $data['parent'] = $this->Pages_model->getParentList();
        $data['page'] = "Pages/edit";
        $this->template->template_view($data);
    }

    /**
     * delete method
     * @description this function use to delete tempray record 
     * @return void
     */
    function delete()
    {
        if (!$this->delete_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect($_SERVER['HTTP_REFERER'], 'refresh');
        endif;

       
        $id = $this->uri->segment(3);
        $data = ['delete_status' => 0];
        if ($id) {
            $this->Common_model->UpdateDB('pages', ['id' => $id], $data);
            $this->session->set_flashdata('success', $this->ion_auth->messages());
            $msg = "deleted";
            echo json_encode(array("msg" => $msg));
            exit;
        } else {
            set_flashdata('error', 'Unable to delete poll, try again');
            redirect('pages/', 'refresh');
        }
    }

    public function detail()
    {   
        if (!$this->view_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect($_SERVER['HTTP_REFERER'], 'refresh');
        endif;


        $id = $this->uri->segment(3);
        if (isset($id)) {
            $data['pagesData'] = $this->Common_model->getDataById('pages', '*', ['id' => $id]);

            $data['page'] = "pages/detail";
            $this->template->template_view($data);
        }
    }

    //update status function 
    public function update_status($id, $action)
    {
        $status = ($action == 'activate') ? 1 : 0;
        $data = array('status' => $status);
        $this->Pages_model->update_status($id, $data);
        $msg = "Updated";
        echo json_encode(['msg' => $msg]);
        exit;
    }


    public function test(){

        $header = '<html xmlns="http://www.w3.org/1999/xhtml">
        <head>
          <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
          <!--[if !mso]><!-->
          <meta http-equiv="X-UA-Compatible" content="IE=edge" />
          <!--<![endif]-->
          <meta name="viewport" content="width=device-width, initial-scale=1.0" />
          <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
          <title></title>
        </head>
                    <body style="Margin:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;min-width:100%;background-color:#f3f2f0; font-family: Roboto, sans-serif;">
          <center class="wrapper" style="width:100%;table-layout:fixed;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;background-color:#f3f2f0;">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:#f3f2f0;" bgcolor="#f3f2f0;">
              <tr>
                <td width="100%"><div class="webkit" style="max-width:750px;Margin:0 auto;"> 
                  <table class="outer" align="center" cellpadding="0" cellspacing="0" border="0" style="border-spacing:0;Margin:0 auto;width:100%;max-width:750px;">  
                    <tr>
                      <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#FFFFFF"  style=" border-left:1px solid #e8e7e5; border-right:1px solid #e8e7e5">
                          <tr>
                            <td bgcolor="#10436d" width="600" height="90" valign="top" align="center" style=" background-size: cover; padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;text-align:center;font-size:0" 
                            class="two-column">   
                            <div>
                              <div class="column" style="width:100%;display:inline-block;vertical-align:top;">
                                <table width="100%" style="border-spacing:0">
                                  <tr>
                                    <td class="inner" style="padding-bottom:10px; padding-right:10px;padding-left:30px;">
                                    <table class="contents1" style="border-spacing:0; width:100%">
                                      <tr>
                                        <td align="center" valign="middle" style="padding-right:30px"><p style="font-size:30px; text-decoration:none; color:#ffffff; font-family: Roboto, sans-serif; text-align:left; margin: 10px 0 0;">
                                          <a href="" target="_blank"><img src="" style="height: 60px; width: auto;"></a></p>
                                        </td>
                                        <td  style="border-spacing:0; width:400px; padding: 30px 0 0; display: block;">
                                         <p style="font-size:18px; text-decoration:none; color:#ffffff; font-family: Roboto, sans-serif; text-align:left; line-height:26px">Your Next BIG IDEA Becoms reality <br />
                                         </td>
                                       </tr>
                                     </table>
                                     </td>
                                   </tr>
                                 </table>
                               </div>                     
                            </div>
                          </td>
                        </tr>
                      </table>
        
                      <table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#FFFFFF">
                        <tr>
                          <td class="" style=""> 
                            <div class="" style="">
                              <table class="contents" style="border-spacing:0; width:100%; padding:0px;" bgcolor="#FFFFFF">
                                <thead style=" color: #484848; font-size: 15px;">
                                  <tr>
                                    <th align="left" style="padding: 15px 5px 15px 10px; text-align: left;font-weight:normal">';

        $footer = '</th>
        </tr>
        </thead>
    </table>
  </div>
</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#b51f24" style="background-size: cover;">
<tr>
<td><table width="100%" cellpadding="0" cellspacing="0" border="0"  >
  <tr>
    <td height="5" align="center" class="one-column">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" class="one-column" style="padding-top:0;padding-bottom:0;padding-right:10px;padding-left:10px;"><font style="font-size:13px; text-decoration:none; color:#ffffff; font-family: Roboto, sans-serif; text-align:center">Call us:'.$emailData['cu_phone'].',  Email us:<a href="mailto:'.$emailData['cu_email'].'" style="color:#ffffff; text-decoration:underline">'.$emailData['cu_email'].'</a> </font></td>
  </tr>
  <tr>
    <td align="center" class="one-column" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" class="one-column" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;"><table width="150" border="0" cellspacing="0" cellpadding="0">
      <tr>';

    if(!empty($emailData['cu_facebook'])){
       $footerData .= '<td width="33" align="center"><a href="'.$emailData['cu_facebook'].'" target="_blank"><img src="'.Router::url('/', true).'img/email/facebook.png" alt="facebook" width="30" height="30" border="0"/></a></td>';
    }

    if(!empty($emailData['cu_twitter'])){
       $footerData .= '<td width="34" align="center"><a href="'.$emailData['cu_twitter'].'" target="_blank"><img src="'.Router::url('/', true).'img/email/twitter.png" alt="twitter" width="30" height="30" border="0"/></a></td>';
    }

    if(!empty($emailData['cu_linkedin'])){
        $footerData .= '<td width="33" align="center"><a href="'.$emailData['cu_linkedin'].'" target="_blank"><img src="'.Router::url('/', true).'img/email/linkedin.png" alt="linkedin" width="30" height="30" border="0"/></a></td>';
    }
    if(!empty($emailData['cu_instagram'])){
        $footerData .= '<td width="33" align="center"><a href="'.$emailData['cu_instagram'].'" target="_blank"><img src="'.Router::url('/', true).'img/email/instagram.png" alt="instagram" width="30" height="30" border="0"/></a></td>';
    }
     $footerData .= '</tr>
    </table></td>
  </tr>
  <tr>
    <td height="3" align="center" class="one-column" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;"></td>
  </tr>
  <tr>
    <td align="center" class="one-column" style="padding-top:0;padding-bottom:0;padding-right:10px;padding-left:10px;"><font style="font-size:13px; text-decoration:none; color:#ffffff; font-family: Roboto, sans-serif; text-align:center"> <a href="'.Router::url('/', true).'" target="_blank" style="color:#ffffff; text-decoration:underline">'.$emailData['cu_copyright'].'</a></font></td>
  </tr>
 
  <tr>
    <td height="8" class="contents1" style="width:100%; border-bottom-left-radius:10px; border-bottom-right-radius:10px"></td>
  </tr>
</table></td>
</tr>

</table>
</td>
</tr>
</table>

</div>
</td>
</tr>
</table>
</center>
</body>
</html>';

    return $header.$footer;

    }
}
