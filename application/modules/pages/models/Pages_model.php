<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
  Author Akash Hedaoo
  Date 16/04/2019
 */

class Pages_model extends CI_Model {

    function __construct() {
        $this->load->library('session');
        $this->table = 'pages';
        $this->column_order = array(null, 'pages.name', 'pages.parent', 'pages.created','pages.updated','pages.status');
        // Set searchable column fields
        $this->column_search = array('pages.name', 'pages.parent', 'pages.created','pages.updated','pages.status');
        // Set default order
        $this->order = array('pages.created' => 'desc');
    }

    public function getParentList(){
        $this->db->select('id, name');
        $this->db->from('pages');
        $this->db->where(['parent' => 0]);
        $result = $this->db->get()->result_array();
        return $result;
    }

    public function checkSlug($slug){
        $this->db->select('slug');
        $this->db->from('pages');
        $this->db->where(['slug' => $slug]);            
        $result = $this->db->get()->result_array();
        if(!empty($result)){
            $value = $_SESSION['slug'] + 1;
            $_SESSION['slug'] = $value;
            $slug = $result[0]['slug'];
            $data = explode('-', $slug);
            if(count($data) > 1) {
                $sufix = $data[1] + 1;
                $slug = $data[0].'-'.$sufix;
            } else {
                $slug = $slug.'-'.$value;
            }
            
            $this->checkSlug($slug);
        } else {
            $_SESSION['slugValue'] =  $slug;
        }
    }



      /*
     * Fetch members data from the database
     * @param $_POST filter data based on the posted parameters
     */

    public function getRows($postData) {

        $this->_get_datatables_query($postData);
        if ($postData['length'] != -1) {
            $this->db->limit($postData['length'], $postData['start']);
        }
        $query = $this->db->get();
        $data['result'] = $query->result();
        $data['total'] = $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total;
        return $data;
    }

    /*
     * Perform the SQL queries needed for an server-side processing requested
     * @param $_POST filter data based on the posted parameters
     */

    private function _get_datatables_query($postData) {
        $current_container_id = get_contener_id();
        $this->db->select('SQL_CALC_FOUND_ROWS ' . $this->table . '.id, ' . $this->table . '.name, ' . $this->table . '.parent, ' . $this->table . '.updated, ' . $this->table . '.created, ' . $this->table . '.status', false);
        $this->db->from($this->table);
        $this->db->where($this->table . '.delete_status = 1');

        $i = 0;
        // loop searchable columns 
        foreach ($this->column_search as $item) {
            // if datatable send POST for search
            if ($postData['search']['value']) {
                // first loop
                if ($i === 0) {
                    // open bracket
                    $this->db->group_start();
                    $this->db->like($item, $postData['search']['value']);
                } else {
                    $this->db->or_like($item, $postData['search']['value']);
                }

                // last loop
                if (count($this->column_search) - 1 == $i) {
                    // close bracket
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($postData['status']) && $postData['status'] < 2) {
            $this->db->where($this->table . '.status', $postData['status']);
        }
        $this->db->group_by($this->table . '.id');

        if (isset($postData['order'])) {
            $this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

      //update status function 
      public function update_status($id = null, $data) {
        $this->db->where('id', $id);
        $delete = $this->db->update('pages', $data);
        if ($delete):
            return true;
        endif;
    }

}

/* End of file Users_modal.php */
