
            <div class="container-fluid">
                <div data-widget-group="group1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default panel-grid">
                                <div class="panel-heading">
                                    <a class="btn btn-primary" href="<?= base_url('pages/add') ?>">Add New</a>
                                    <div class="panel-ctrls"></div>
                                </div>
                                <div class="panel-body pl-0 pr-0">
                                    <div class="row m-0">
                                        <div class="col-md-12">
                                            <table id="pollsListTable" class="table table-bordered table-striped table-hover" cellspacing="0" width="100%">
                                                <colgroup>
                                                    <col width="5%">
                                                    <col width="15%">
                                                    <col width="30%">
                                                    <col width="15%">
                                                    <col width="10%">
                                                    <col width="10%">
                                                    <col width="15%">
                                                </colgroup>
                                                <thead>
                                                    <tr>
                                                        <th>Sr.</th>
                                                        <th>Name</th>
                                                        <th>Parent</th>
                                                        <th>updated</th>
                                                        <th>Created</th>
                                                        <th>Status</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<script type="text/javascript">
    $(document).ready(function () {

        var table = table = $('#pollsListTable').DataTable({
            // Processing indicator
            "processing": true,
            // DataTables server-side processing mode
            "serverSide": true,
            // Initial no order.
            "iDisplayLength": 10,
            "bPaginate": true,
            "order": [],
            // Load data from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('pages/getLists/'); ?>",
                "type": "POST",
                "data": function (data) {
                    // data.active = $('#select_status option:selected').val();
                    data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                },
            },

            //Set column definition initialisation properties
            "columnDefs": [{
                    "targets": 6,
                    "width": "4%",
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                            data = '';
<?php if ($this->ion_auth->is_admin() || $permissions->edit_per) : ?>
                                data = '<a class="btn btn-primary btn-sm" href="<?= base_url('pages/edit/') ?>' + full['id'] + '"><i class="ti ti-pencil"></i></a>';
<?php endif;
if ($this->ion_auth->is_admin() || $permissions->view_per) :
    ?>
                                data += '<a class="btn btn-info btn-sm " href="<?= base_url('pages/detail/') ?>' + full['id'] + '"><i class="ti ti-eye"></i></a>';
<?php endif;
if ($this->ion_auth->is_admin() || $permissions->delete_per) :
    ?>
                                data += '<a href="<?= base_url('pages/delete/') ?>' + full['id'] + '" class="btn btn-danger btn-sm delete_item"><i class="ti ti-trash"></i></a>';
<?php endif; ?>
                        }
                        return data;
                    }
                },
                {
                    "targets": 5,
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                            if (full['status'] == '1') {
                                data = '<a href="<?php bs('pages/Pages/update_status/') ?>' + full['id'] + '/deactivate" data-toggle="tooltip" data-placement="top" title="Click to Change Status" class="btn btn-success btn-status btn-sm change_status">Active</a>';
                            } else {
                                data = '<a href="<?php bs('pages/Pages/update_status/') ?>' + full['id'] + '/activate" data-toggle="tooltip" data-placement="top" title="Click to Change Status" class="btn btn-danger btn-status btn-sm change_status">Inactive</a>';
                            }
                        }
                        return data;

                    }

                },
            ],
            "columns": [{
                    "data": "sr_no",
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                    "autoWidth": true
                },
                {
                    "data": "name",
                    "autoWidth": true
                },
                {
                    "data": "parent",
                    "autoWidth": true
                },
                {
                    "data": "updated",
                    "autoWidth": true
                },
                {
                    "data": "created",
                    "autoWidth": true
                }
            ]
        });

        $(document).on('click', '.change_status', function (e) {
            e.preventDefault();
            $.get($(this).attr("href"), // url
                    function (data, textStatus, jqXHR) { // success callback
                        var obj = JSON.parse(data);
                        if (obj.msg === 'Updated') {
                            table.ajax.reload();  //just reload table
                        }
                    });
        });

        // To delete record
        $(document).on('click', '.delete_item', function (e) {
            e.preventDefault();
            var scope = $(this);
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure?',
                buttons: {
                    confirm: function () {
                        $.get(scope.attr("href"), // url
                                function (data, textStatus, jqXHR) { // success callback
                                    var obj = JSON.parse(data);
                                    if (obj.msg === 'deleted') {
                                        table.ajax.reload(); //just reload table
                                    }
                                });
                        return true;
                    },
                    cancel: function () {
                        return true;
                    }
                }
            });
        });
    });
</script>