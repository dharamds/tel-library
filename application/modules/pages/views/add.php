<div class="container-fluid">
    <div class="panel panel-default">
        <?php
               echo form_open('Pages/add/', [
                  'name' => 'frmAddPages',
                  'id' => 'frmAddPages',
                  'class' => 'form-horizontal'
               ]);
               ?>
        <!--//  Panel Body Start  //-->
        <div class="panel-body">
            <div class="tab-content">
                <!--//  Tab Content Start  //-->
                <div role="tabpanel" class="tab-pane active" id="poll_content">
                    <div class="row mt-3 mb-3">
                        <div class="col-sm-4">
                            <div class="form-group m-0">
                                <label for="coursetitle" class="control-label">
                                    Name *
                                </label>
                                <?php
                                 echo form_input([
                                    'class' => 'form-control',
                                    'name' => 'name',
                                    'id' => 'name'
                                 ]);
                                 ?>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group m-0">
                                <label for="longtitle" class="control-label">
                                    Parent
                                </label>
                                 <select class="form-control" name="parent">
                                    <option value="" >Please select</option>
                                    <?php foreach ($parent as $value) {?>
                                    <option value="<?php echo $value['id'] ?>" ><?php echo $value['name'] ?></option>
                                       <?php } ?>
                              </select>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group m-0">
                                <label for="longtitle" class="control-label">
                                    Status
                                </label>
                                 <select class="form-control" name="status">
                                    <option value="" >Please select</option>
                                    <option value="1" >Active</option>
                                    <option value="0" >Inactive</option>
                              </select>
                            </div>
                        </div>
                    </div>

                    <div class="row m-0 mb-3">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label for="coursedescription" class="control-label">
                                    Meta Keywords
                                </label>
                                <div>
                                    <?php echo form_textarea([
                                       'name' => 'meta_keywords',
                                       'id' => 'meta_keywords',
                                       'class' => 'form-control',
                                       'rows' => '5'
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row m-0 mb-3">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label for="coursedescription" class="control-label">
                                    Meta Description
                                </label>
                                <div>
                                    <?php echo form_textarea([
                                       'name' => 'meta_description',
                                       'id' => 'meta_description',
                                       'class' => 'form-control',
                                       'rows' => '5'
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row m-0 mb-3">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label for="coursedescription" class="control-label">
                                    Body
                                </label>
                                <div>
                                    <?php echo form_textarea([
                                       'name' => 'body',
                                       'id' => 'body',
                                       'class' => 'form-control editor',
                                       'rows' => '5'
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-md-offset-1">
                        <input type="submit" class="finish btn-success btn" value="Submit">
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!--//  Panel Body End  //-->
    </div>
</div>
<script>
   var config = { height: 120, toolbar: 'short'};
   $(document).ready(function() {
      $('.editor').each(function(e) {
         CKEDITOR.replace(this.id, config);
      });

      $("#frmAddPages").validate({
         ignore: [],
         rules: {
            name: {
               required: true
            }
         },
         messages: {
            name: "Please enter name"
         }
      });

   });
</script>