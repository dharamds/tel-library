<div class="static-content-wrapper">
   <div class="static-content">
      <div class="page-content">
         <ol class="breadcrumb">
            <li class=""><a href="<?php bs(); ?>">Home</a></li>
            <li class=""><a href="<?php bs(); ?>pages/">Pages</a></li>
            <li class="active"><a href="">Detail</a></li>
         </ol>
         <div class="container-fluid">
            <div class="tab-pane active" id="tab-about">
               <div class="panel panel-default">
                  <div class="panel-heading">
                     <h2>Details</h2>
                  </div>
                  <div class="panel-body">
                     <div class="about-area">
                        <div class="table-responsive">
                           <table class="table about-table">
                              <tbody>  
                                 <tr>
                                    <th>Name</th>
                                    <td width="5%"></td>
                                    <td><?php echo $pagesData->name;?></td>
                                 </tr>  
                                 <tr>
                                    <th>Parent</th>
                                    <td width="5%"></td>
                                    <td><?php echo $pagesData->parent;?></td>
                                 </tr>                                   
                                 <tr>
                                    <th>Body</th>
                                    <td width="5%"></td>
                                    <td><?php echo $pagesData->body;?></td>
                                 </tr>   
                                 <tr>
                                    <th>Mata Keywords</th>
                                    <td width="5%"></td>
                                    <td><?php echo $pagesData->meta_keywords;?></td>
                                 </tr>   
                                 <tr>
                                    <th>Meta Description</th>
                                    <td width="5%"></td>
                                    <td><?php echo $pagesData->meta_description;?></td>
                                 </tr> 
                                 <tr>
                                    <th>Status</th>
                                    <td width="5%"></td>
                                    <td><?php echo $pagesData->status;?></td>
                                 </tr>          
                                 <tr>
                                    <th>Created</th>
                                    <td width="5%"></td>
                                    <td><?php 
                                          echo $pagesData->created;
                                       ?></td>
                                 </tr>                                 
                                 <tr>
                                    <th>Updated</th>
                                    <td width="5%"></td>
                                    <td><?php echo date('m-d-Y',strtotime($pollsData->updated)); ?></td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- .container-fluid -->
</div>
<!-- #page-content -->
