<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
Author Salman Iqbal
Company Parexons
Date 26/1/2017
*/

class Lessons_modal extends CI_Model
{

    function __construct()
    {
        // Set table name
        $this->table = 'lessons';
        // Set orderable column fields
        $this->column_order = array(null, null, $this->table.'.status', $this->table.'.name', $this->table.'.long_title','categories.name','tags.name','system_category.name','system_tags.name', $this->table.'.template_type', $this->table.'.created', 'created_by');
        // Set searchable column fields
        $this->column_search = array($this->table.'.name', $this->table.'.long_title');
        // Set default order
        $this->order = array($this->table.'.id' => 'desc');
    }

    /*
     * Fetch members data from the database
     * @param $_POST filter data based on the posted parameters
     */
    public function getRows($postData = NULL)
    {
        $this->_get_datatables_query($postData);
        if ($postData['length'] != -1) {
            $this->db->limit($postData['length'], $postData['start']);
        }

        $query = $this->db->get();
        //pr(vd(),"j");
        $data['result'] = $query->result();
        $data['total'] = $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total;
        return $data;
    }

    /*
     * Count records based on the filter params
     * @param $_POST filter data based on the posted parameters
     */
    public function countFiltered($postData)
    {
        $this->_get_datatables_query($postData);
        $query = $this->db->get();
        return $query->num_rows();
    }


    //Count lessons
    public function count_records()
    {
        $this->db->select('*');
        $this->db->from('lessons');
        return $this->db->count_all_results();
    }


    /*
     * Perform the SQL queries needed for an server-side processing requested
     * @param $_POST filter data based on the posted parameters
     */
    private function _get_datatables_query($postData)
    {
        //pr($postData);
        $this->db->select('SQL_CALC_FOUND_ROWS ' . $this->table . '.id, ' . $this->table . '.name, ' . $this->table . '.long_title, ' . $this->table . '.template_type,  ' . $this->table . '.created_by, ' . $this->table . '.status, ' . $this->table . '.created,' . $this->table . '.status', false);
        
        if ($postData['order']['0']['column'] == 11) 
        {
            $this->db->select('CONCAT_WS(" ", users.first_name, users.middle_name, users.last_name) as created_by');
        }else{
            $this->db->select('"..." as created_by');
        }

        $this->db->select('DATE_FORMAT('.$this->table.'.created, "%m/%d/%Y") as created');
        
        // courses metadata
        if ((isset($postData['lesson_cat']) && count($postData['lesson_cat']) > 0) || $postData['order']['0']['column'] == 5) 
        {
            $this->db->select('GROUP_CONCAT( DISTINCT categories.name SEPARATOR \'<br> \')  as categories');
        }else{
            $this->db->select('"..." as categories');
        }  
        
        if ((isset($postData['lesson_tag']) && count($postData['lesson_tag']) > 0) || $postData['order']['0']['column'] == 6) 
        {
        $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT tags.name ORDER BY tags.name ASC  SEPARATOR "<br>") , \'NA\') as tags');
        }else{
            $this->db->select('"..." as tags');
        }   

        // system metadata
        if ((isset($postData['system_cat']) && count($postData['system_cat']) > 0) || $postData['order']['0']['column'] == 7) 
        {
            $this->db->select('GROUP_CONCAT( DISTINCT system_category.name SEPARATOR \'<br> \')  as system_categories');
        }else{
            $this->db->select('"..." as system_categories');
        }  
        

        if ((isset($postData['system_tag']) && count($postData['system_tag']) > 0) || $postData['order']['0']['column'] == 8) 
        {
        $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT system_tags.name ORDER BY system_tags.name ASC  SEPARATOR "<br>") , \'NA\') as system_tags');
        }else{
            $this->db->select('"..." as system_tags');
        }  

        $this->db->from($this->table);
        if ($postData['order']['0']['column'] == 11) 
        {
        $this->db->join('users', $this->table . '.created_by = users.id');
        }    

        
        if ((isset($postData['lesson_cat']) && count($postData['lesson_cat']) > 0) || $postData['order']['0']['column'] == 5) {
            $this->db->join('category_assigned', 'category_assigned.reference_id = '.$this->table.'.id AND category_assigned.reference_type = '.META_LESSON, 'left');
            $this->db->join('categories', 'categories.id = category_assigned.category_id AND categories.delete_status = 1 AND categories.status = 1', 'left');
        }
        
        
        if ((isset($postData['lesson_tag']) && count($postData['lesson_tag']) > 0) || $postData['order']['0']['column'] == 6) {
            $this->db->join('tag_assigned', 'tag_assigned.reference_id = '.$this->table.'.id AND tag_assigned.reference_type = '.META_LESSON, 'left');
            $this->db->join('tags', 'tags.id = tag_assigned.tag_id  AND tags.delete_status = 1 AND tags.status = 1', 'left');
        }
        
        if ((isset($postData['system_cat']) && count($postData['system_cat']) > 0) || $postData['order']['0']['column'] == 7) {
            $this->db->join('category_assigned as assigned_system_category', 'assigned_system_category.reference_id =  '.$this->table.'.id AND assigned_system_category.reference_type = '.META_SYSTEM.' AND  assigned_system_category.reference_sub_type ='.META_LESSON, 'left');
            $this->db->join('categories as system_category', 'system_category.id = assigned_system_category.category_id AND system_category.delete_status = 1 AND system_category.status = 1', 'left');
        }
        
        if ((isset($postData['system_tag']) && count($postData['system_tag']) > 0) || $postData['order']['0']['column'] == 8) {
            $this->db->join('tag_assigned as assigned_system_tags', 'assigned_system_tags.reference_id =  '.$this->table.'.id AND assigned_system_tags.reference_type = '.META_SYSTEM.' AND  assigned_system_tags.reference_sub_type ='.META_LESSON, 'left');
            $this->db->join('tags as system_tags', 'system_tags.id = assigned_system_tags.tag_id AND system_tags.delete_status = 1 AND system_tags.status = 1', 'left');
        }

        $this->db->where($this->table . '.delete_status = 1');
      
        if (isset($postData['status']) && $postData['status'] < 2) {
            $this->db->where('lessons.status', $postData['status']);
        }
        if($postData['system_cat']) {
            $this->db->where_in('assigned_system_category.category_id', $postData['system_cat']);
        }
        if($postData['system_tag']) {
            $this->db->where_in('assigned_system_tags.tag_id', $postData['system_tag']);
        }
        if($postData['lesson_cat']) {
            $this->db->where_in('category_assigned.category_id', $postData['lesson_cat']);
        }
        if($postData['lesson_tag']) {
            $this->db->where_in('tag_assigned.tag_id', $postData['lesson_tag']);
        }

        $i = 0;
        // loop searchable columns 
        foreach($this->column_search as $item){
		    // if datatable send POST for search
            if($postData['search']['value']){
                // first loop
                if($i === 0){
				    // open bracket 
					$this->db->group_start();
                    $this->db->like($item, $postData['search']['value'], 'both');
                }else{
                    $this->db->or_like($item, $postData['search']['value'], 'both');
                }
                    // last loop
                if(count($this->column_search) - 1 == $i){
                    // close bracket
                    $this->db->group_end();
                }
            }
            $i++;
        }
        $this->db->group_by($this->table.'.id');
        if (isset($postData['order'])) {
            $this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    /*
     * Serach for Reflect Search data from database 
     * @param $_POST filter data based on the posted parameters
     */
    public function reflect_search($system_cat, $system_tag, $poll_category, $poll_tag)
    {
        $this->db->select('polls.id,polls.title');
        $this->db->from('polls');
        
        if (count($system_cat) > 0) {
            $this->db->join('category_assigned', 'polls.id = category_assigned.reference_id AND category_assigned.reference_type = '.META_SYSTEM.' AND category_assigned.reference_sub_type = '.META_POLL);
        }
        if (count($system_tag) > 0) {
            $this->db->join('tag_assigned', 'polls.id = tag_assigned.reference_id AND tag_assigned.reference_type ='.META_SYSTEM.' AND tag_assigned.reference_sub_type = '.META_POLL);
        }  
        if (count($poll_category) > 0) {
            $this->db->join('category_assigned as polls_category_assigned', 'polls.id = polls_category_assigned.reference_id AND polls_category_assigned.reference_type = '.META_POLL.' AND polls_category_assigned.reference_sub_type = '.META_POLL);
        }
        if (count($poll_tag) > 0) {
            $this->db->join('tag_assigned as polls_tag_assigned', 'polls.id = polls_tag_assigned.reference_id AND polls_tag_assigned.reference_type ='.META_POLL.' AND polls_tag_assigned.reference_sub_type = '.META_POLL);
        }  


        if (count($system_cat) > 0) {
            $this->db->where_in("category_assigned.category_id ", $system_cat);
        }
        if (count($system_tag) > 0) {
            $this->db->where_in("tag_assigned.tag_id", $system_tag);
        }
        if (count($poll_category) > 0) {
            $this->db->where_in("polls_category_assigned.category_id ", $poll_category);
        }
        if (count($poll_tag) > 0) {
            $this->db->where_in("polls_tag_assigned.tag_id", $poll_tag);
        }
        $this->db->where(["polls.status" => 1,"polls.delete_status"=>1]);
        //$this->db->where_in("polls.delete_status", 1);
        
        $query = $this->db->get();
        // echo $this->db->last_query();
        // exit;
        $data = $query->result_array();
        return $data;
    }

    /*
     * Serach for Reflect Search data from database 
     * @param $_POST filter data based on the posted parameters
     */
    public function reflect_search_quiz($system_cat, $system_tag, $assessment_category, $assessment_tag)
    {
      
        $this->db->select('quizzes.id,quizzes.title');
        $this->db->from('quizzes');
        if (count($system_cat) > 0) {
            $this->db->join('category_assigned', 'quizzes.id = category_assigned.reference_id AND category_assigned.reference_type = '.META_SYSTEM.' AND category_assigned.reference_sub_type = '.META_QUIZ);
        }
        if (count($system_tag) > 0) {
            $this->db->join('tag_assigned', 'quizzes.id = tag_assigned.reference_id AND tag_assigned.reference_type ='.META_SYSTEM.' AND tag_assigned.reference_sub_type = '.META_QUIZ);
        }  
        
        
        if (count($assessment_category) > 0) {
            $this->db->join('category_assigned as assessment_category_assigned', 'quizzes.id = assessment_category_assigned.reference_id AND assessment_category_assigned.reference_type = '.META_ASSESSMENT.' AND assessment_category_assigned.reference_sub_type = '.META_QUIZ);
        }
        if (count($assessment_tag) > 0) {
            $this->db->join('tag_assigned as assessment_tag_assigned', 'quizzes.id = assessment_tag_assigned.reference_id AND assessment_tag_assigned.reference_type ='.META_ASSESSMENT.' AND assessment_tag_assigned.reference_sub_type = '.META_QUIZ);
        }  

        if (count($system_cat) > 0) {
            $this->db->where_in("category_assigned.category_id ", $system_cat);
        }
        if (count($system_tag) > 0) {
            $this->db->where_in("tag_assigned.tag_id", $system_tag);
        }

        if (count($assessment_category) > 0) {
            $this->db->where_in("assessment_category_assigned.category_id ", $assessment_category);
        }
        if (count($assessment_tag) > 0) {
            $this->db->where_in("assessment_tag_assigned.tag_id", $assessment_tag);
        }

        $this->db->where(["quizzes.status" => 1,"quizzes.delete_status" => 1,"quizzes.type"=>1]);
        //$this->db->where_in("quizzes.delete_status", 1);
        return $this->db->get()->result_array();
    }

    public function update_status($id = null, $data)
    {
        $this->db->where('id', $id);
        $delete = $this->db->update('lessons', $data);
        if ($delete) :
            return true;
        endif;
    }
    
    /*
    *getCheckExist this method is used to check data is already exist or not
    * @param string
    * @return string 
    */
    public function getCount($condition=[],$table)
    {
        $this->db->where($condition);
        if($query = $this->db->get($table))
        {   
            return $query->num_rows();
        }
    }

    //update status
    public function get_lesson_used($lessonID = null)
    {
        $sql = "SELECT COALESCE(GROUP_CONCAT(DISTINCT courses.name ORDER BY courses.name ASC SEPARATOR '<br>'), 'NA')  as course_name from courses 
        JOIN module_contain ON courses.id = module_contain.course_id
        WHERE module_contain.lesson_id = " . $lessonID . ' group by module_contain.lesson_id';

        //return $sql;
        $query =  $this->db->query($sql);
        return ($query->row()->course_name) ? $query->row()->course_name : 'NA';
    }

    public function get_module_used($lessonID){
        $sql = "SELECT COALESCE(GROUP_CONCAT(DISTINCT modules.name ORDER BY modules.name ASC SEPARATOR '<br>'), 'NA')  as module_name from modules 
        JOIN module_contain ON modules.id = module_contain.module_id
        WHERE module_contain.lesson_id = " . $lessonID . ' group by module_contain.lesson_id';
        //return $sql;
        $query =  $this->db->query($sql);
        return ($query->row()->module_name) ? $query->row()->module_name : 'NA';
    }
}
