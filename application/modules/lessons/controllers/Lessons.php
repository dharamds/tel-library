<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Lessons extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->module('template');
        $this->load->model(['common_model', 'Lessons_modal']);
        $this->load->helper(['html', 'form', 'language']);
        $this->load->library('form_validation');

        if (!$this->ion_auth->logged_in()) :
            redirect('users/auth', 'refresh');
        endif;
        // get controller permissions		
        if ($this->current_user_permissions = $this->get_permissions()) {
            $this->add_permission = $this->current_user_permissions->add_per;
            $this->edit_permission = $this->current_user_permissions->edit_per;
            $this->delete_permission = $this->current_user_permissions->delete_per;
            $this->view_permission = $this->current_user_permissions->view_per;
            $this->list_permission = $this->current_user_permissions->list_per;
        }
    }

    /**
     * index method
     * @description this function use to display list of lesson
     * @return void
     */
    public function index()
    {
        $data['page'] = "lessons/list";
        // check permissions
        if (!$this->list_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect('users/auth', 'refresh');
        endif;
        $data['permissions'] = $this->current_user_permissions;
        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Lessons', 'link' => '', 'class' => 'active'];
        $this->template->template_view($data);
    }

    /**
     * getLists method 
     * @description this function called via ajax request, use to display list of lesson
     * @return void
     */
    public function setAllSelected()
    {
        $lessonIds = array_filter(explode("|", $this->input->post('selectedLesson')));
        $data = [];
        $status = $this->input->post('status');
        foreach ($lessonIds as $key => $val) {
            $data['status'] = $status;
            $this->Lessons_modal->update_status($val, $data);
        }
        $msg = 'Updated';
        echo json_encode(['msg' => $msg]);
        exit;
    }

    public function update_status($id, $action)
    {

        # clear cache when lesson created or updated
        $lesson_slug = $this->common_model->getAllData('lessons', 'name', true,['id' => $id]);
        

        $status = ($action == 'activate') ? 1 : 0;
        $data = array('status' => $status);
        $this->Lessons_modal->update_status($id, $data);
        $msg = "Updated";
        clean_cache('api', 'tel_getLessonByLessonSlug_');
        clean_cache_by_key('api',md5(slugify($lesson_slug->name)));
        echo json_encode(['msg' => $msg]);
        exit;
    }

    public function getLists()
    {
        $lessonsData = $this->Lessons_modal->getRows($_POST);
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $lessonsData['total'],
            "recordsFiltered" => $lessonsData['total'],
            "data" => $lessonsData['result'],
        );
        // Output to JSON format
        echo json_encode($output);
    }

    /**
     * add_lesson method
     * @description this function use to create a new lesson
     * @return void
     */
    public function add($id = null)
    {

        $data['lesson_template'] = $this->common_model->getAllData('lesson_template_types', 'id,name', '', array('status' => 1));
        // check method permission
        if ($id > 0) :
            if ($id && !$this->edit_permission && !$this->ion_auth->is_admin()) :
                $this->session->set_flashdata('error', $this->lang->line('access_denied'));
                redirect($_SERVER['HTTP_REFERER'], 'refresh');
            endif;
        else :
            if (!$this->add_permission && !$this->ion_auth->is_admin()) :
                $this->session->set_flashdata('error', $this->lang->line('access_denied'));
                redirect($_SERVER['HTTP_REFERER'], 'refresh');
            endif;
        endif;

        $data['permissions'] = $this->current_user_permissions;
        if ($this->input->post()) {           
            $this->form_validation->set_rules('name', 'Name', 'trim|required');         
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error', validation_errors());
                $data['page'] = "lessons/add";
                $this->template->template_view($data);
            } else {                
                
                $data = array(
                    'created_by' => $this->session->userdata('user_id'),
                    'name' => post('name'),
                    'template_type' => post('template_type'),
                    'slug' => slugify(post('name')),
                    'long_title' => post('long_title'),
                    'inquire_section_title' => post('inquire_section_title'),
                    'inquire_section' => post('inquire_section'),
                    'watch_title' => post('watch_title'),
                    'watch_video_link' => post('watch_video_link'),
                    'watch_video_transcript' => post('watch_video_transcript'),
                    'watch_video_file' => post('watch_video_file'),
                    'read_section_title' => post('read_section_title'),
                    'read_section' => post('read_section'),
                    'poll_id' => post('poll_id'),
                    'quiz_id' => post('quiz_id'),
                    'expand_title' => post('expand_title'),
                    'expand_description' => post('expand_description'),
                    'lesson_toolbox' => post('lesson_toolbox'),
                    'license_citation' => post('license_citation'),
                    'practice_title' => post('practice_title'),
                    'practice_description' => post('practice_description'),
                    'summary' => post('summary'),
                    'outcomes' => post('outcomes'),
                    'glossary' => post('glossary'),
                    'status' => 1,
                    'delete_status' => 1,
                );
                  
                # clear cache when lesson created or updated
                clean_cache_by_key('api',md5(slugify(post('name'))));

                if (post('id')) {
                  
                    if($this->common_model->UpdateDB('lessons', array("id" => post('id')), $data)) {
                        clean_cache('api', 'tel_getLessonByLessonSlug_');
                        echo post('id');
                    }
                    
                    
                } else {

                    $this->common_model->InsertData('lessons', $data);
                    echo $this->db->insert_id();
                }
                exit;
            }
        } else {
            $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
            $data['breadcrumb'][]           = ['title' => 'Lessons', 'link' => base_url('lessons'), 'class' => ''];

            if ($id != null) {
                $data['lessonsData'] = $this->common_model->getDataById('lessons', '*', ['id' => $id]);
                
                $data['PollData'] = $this->common_model->getDataById('polls', 'id,title', ['id' => $data['lessonsData']->poll_id]);
                $data['QuizData'] = $this->common_model->getDataById('quizzes', 'id,title', ['id' => $data['lessonsData']->quiz_id]);
                
                $data['id'] = $id;
                $data['breadcrumb'][]           = ['title' => 'Edit', 'link' => '', 'class' => 'active'];
            } else {
                $data['breadcrumb'][]           = ['title' => 'Add', 'link' => '', 'class' => 'active'];
            }

            $data['page'] = "lessons/add";
            $this->template->template_view($data);
        }
    }

    /**
     * view_lesson method
     * @description this function use to view complete lesson details
     * @return void
     */


    public function detail()
    {
        if (!$this->view_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect($_SERVER['HTTP_REFERER'], 'refresh');
        endif;

        $data['permissions'] = $this->current_user_permissions;
        $lesson_id = $this->uri->segment(3);
        if (isset($lesson_id)) {
            $data['lessons_data'] = $this->common_model->getDataById('lessons', '*', ['id' => $lesson_id]);
            $data['documents_data'] = $this->common_model->getAllData('documents', '*', '', ['reference_id' => $lesson_id, 'reference_type' => 3]);
            $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
            $data['breadcrumb'][]           = ['title' => 'Lessons', 'link' => base_url('lessons'), 'class' => ''];
            $data['breadcrumb'][]           = ['title' => 'Detail', 'link' => '', 'class' => 'active'];
            $data['page'] = "lessons/detail";
            $this->template->template_view($data);
        }
    }

    function delete()
    {
        if (!$this->delete_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            echo json_encode(array("msg" => $this->lang->line('access_denied')));
            exit;
        endif;

        $data['permissions'] = $this->current_user_permissions;
        $id = $this->uri->segment(3);
        $data = ['delete_status' => 0];
        if ($id) {
            $this->common_model->UpdateDB('lessons', ['id' => $id], $data);
            $this->common_model->UpdateDB('documents', ['reference_id' => $id, 'reference_type' => 3,], $data);
            clean_cache('api', 'tel_getLessonByLessonSlug_');
            $this->session->set_flashdata('success', $this->ion_auth->messages());
            $msg = "deleted";
            echo json_encode(array("msg" => $msg));
            exit;
        } else {
            set_flashdata('error', 'Unable to delete Question, try again');
            redirect('questions/', 'refresh');
        }
    }

    public function remove_image()
    {
        $lesson_id = $this->input->post('lesson_id');

        $postData = array('lesson_img' => '');
        $this->common_model->UpdateDB('lessons', array("id" => $lesson_id), $postData);
        // $this->Common_model->DeleteDB('lessons',array('id'=>$this->input->post('lesson_id')));

        $pathinfo = pathinfo($_POST['file']);
        //$fileName = $pathinfo['filename'].'.'.$pathinfo['extension'];
        $fileName = $pathinfo['basename'];

        $img_status = unlink(APPPATH . '../uploads/lesson_images/' . $fileName);
        $thumb_status = unlink(APPPATH . '../uploads/lesson_images/thumbnails/' . $fileName);

        if ($img_status == TRUE && $thumb_status == TRUE)
            echo bs() . 'uploads/lesson_images/thumbnails/default.png';
        else
            echo 0;
    }

    public function remove_doc()
    {
        $fileName = $_POST['file'];
        $cnt = $_POST['cnt'];
        $doc_status = unlink(APPPATH . '../uploads/lesson_docs/' . $fileName);
        if ($doc_status == TRUE)
            echo $cnt;
        else
            echo 0;
    }

    /**
     * Manage uploadImage
     *
     * @return Response
     */
    public function resizeImage($filename)
    {
        $source_path = FCPATH.'/uploads/media_images/' . $filename;
        $target_path = FCPATH.'/uploads/media_images/thumbnails/';
        $config_manip = [
            'image_library' => 'gd2',
            'source_image' => $source_path,
            'new_image' => $target_path,
            'maintain_ratio' => TRUE,
            'create_thumb' => TRUE,
            'width' => 150,
            'height' => 150
        ];

        $this->load->library('image_lib', $config_manip);
        if (!$this->image_lib->resize()) {
            echo $this->image_lib->display_errors();
        }
        $this->image_lib->clear();
    }

    public function upload_docs()
    {
        $data = [];
        $postData = [];
        $config['upload_path'] = FCPATH.'/uploads/media_images/';
        $config['allowed_types'] = ALLOWED_FILES_EXT;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file')) {
            $data['error'] = $this->upload->display_errors('', '');
        } else {
            $fileData = $this->upload->data();

            $ext = pathinfo(base_url('uploads/media_images/').$fileData['file_name'] , PATHINFO_EXTENSION);
            
            if($ext == 'png' || $ext == 'jpg' || $ext == 'jpeg') {
                $this->resizeImage($fileData['file_name']);
            }
            
            $data['filePath'] = 'uploads/media_images/';
            $data['fullfilePath'] = base_url() . 'uploads/media_images/';
            $data['fileName'] = $fileData['file_name'];
            $media_data = [
                "title" => post('title'),
                "file_name" => $data['fileName']
            ];
            $id = $this->common_model->InsertDataWithLastID('media_sources', $media_data);
            
            if (post('media_category')) {
                
                foreach (post('media_category') as $val) {
                    $this->common_model->InsertData('category_assigned', array("reference_id" => $id, "reference_type" => META_MEDIA, "reference_sub_type" => META_MEDIA, "category_id" => $val));
                }
            }
            
            if (post('media_tag')) {
                foreach (post('media_tag') as $val2) {
                    $this->common_model->InsertData('tag_assigned', array("reference_id" => $id, "reference_type" => META_MEDIA, "reference_sub_type" => META_MEDIA, "tag_id" => $val2));
                }
                
            }
        }
        echo json_encode($data);
        exit;
    }

    public function reflect_search()
    {
        $system_cat = array_filter(post('system_cat'));
        $system_tag = array_filter(post('system_tag'));
        $assessment_category = array_filter(post('assessment_category'));
        $assessment_tag = array_filter(post('assessment_tag'));
        $system_category_poll = array_filter(post('system_category_poll'));
        $system_tag_poll = array_filter(post('system_tag_poll'));
        $poll_category = array_filter(post('poll_category'));
        $poll_tag = array_filter(post('poll_tag'));
        //pr($poll_category,"fd");
        if(post('type') == 'quiz'){ 
            $data = $this->Lessons_modal->reflect_search_quiz($system_cat, $system_tag, $assessment_category, $assessment_tag);
        }else{
            $data = $this->Lessons_modal->reflect_search($system_category_poll, $system_tag_poll, $poll_category, $poll_tag);
        }
        foreach ($data as $val) {
            $items[$val['id']] = $val['title'];
        }
        echo json_encode($items);
    }

    public function metadata_save($id = null)
    {
        if ($id && !$this->edit_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect($_SERVER['HTTP_REFERER'], 'refresh');
        endif;
        // check method permission
        if (!$this->add_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect($_SERVER['HTTP_REFERER'], 'refresh');
        endif;

        $data['permissions'] = $this->current_user_permissions;
        
        if ($this->input->post()) {
            //pr($_POST,"j");
            $this->form_validation->set_rules('lesson_availability', 'Name', 'trim|required');
            $this->form_validation->set_rules('show_glossary', 'long_title', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error', 'Please fill all mandatory fields properly.');
                $data['page'] = "lessons/add";
                $this->template->template_view($data);
            } else {

                $data = array(
                    'created_by' => $this->session->userdata('user_id'),
                    'lesson_availability' => post('lesson_availability'),
                    'show_glossary' => post('show_glossary'),
                    'lesson_expiry_check' => post('lesson_expiry_check'),
                    'lesson_img' => post('img_name'),
                    'lesson_expiry' => $this->common_model->getDateMdYToYmd(post('lesson_expiry')),
                    'template_type' => post('template_type'),
                );
                $id = post('id');
                $this->common_model->UpdateDB('lessons', array("id" => $id), $data);
                echo $id;
            }
        }
    }

    /**************HELPER******************/
    function course_enroll_trigger($trigger_id)
    {

        $notifications_auto = $this->common_model->getAllData('notifications_auto', '*', '1', array("status" => 1, "trigger_type" => $trigger_id));
        //check for permission
        $current_role_details = currentGroup();
        $check_users_roles = $this->db->query("SELECT ur.user_id,ur.role_id from users_roles as ur JOIN notifications_auto_roles as ar ON ur.role_id=ar.role_id where ur.role_id='" . $current_role_details->id . "' and ur.user_id='" . $this->session->userdata('user_id') . "' and ar.notification_id='" . $trigger_id . "'")->result();
        if (empty($check_users_roles)) {
            return false;
        }
        //fetch role id for required trigger
        $notifications_auto_send_roles = $this->common_model->getAllData('notifications_auto_send_roles', 'role_id', '', array("notification_id" => $notifications_auto->id));
        foreach ($notifications_auto_send_roles as $nasr) {
            $roles_arr[] = $nasr->role_id;
        }

        //fetch user ids acc to role id
        $this->db->select('user_id');
        $this->db->from('users_roles');
        $this->db->where_in('role_id', $roles_arr);
        $this->db->group_by('user_id');
        $user_data = $this->db->get()->result();
        foreach ($user_data as $ud) {
            $user_id_arr[] = $ud->user_id;
        }

        //fetch email using user ids
        $this->db->select('email');
        $this->db->from('users');
        $this->db->join('course_assigned_to_users', 'course_assigned_to_users.user_id=users.id');
        $this->db->where_in('users.id', $user_id_arr);
        if ($notifications_auto->institution_id != '-1') {
            $this->db->where('course_assigned_to_users.container_id', $notifications_auto->institution_id);
        }
        $this->db->group_by('users.email');
        $user_emails = $this->db->get()->result();

        //set trigger msg acc to trigger id
        if ($trigger_id == 1) {
            $msg = 'You enrolled for a course.';
        } else if ($trigger_id == 2) {
            $msg = 'Your Course is completed.';
        } else if ($trigger_id == 3) {
            $msg = 'Passed';
        } else if ($trigger_id == 4) {
            $msg = 'Failed';
        } else if ($trigger_id == 6) {
            $msg = 'You recieved a badge';
        } else if ($trigger_id == 7) {
            $msg = 'You recieved a certificate';
        }

        //looping array to send email 
        foreach ($user_emails as $emails) {
            $email_data['template_code'] = 'GROUP_NOTIFICATION';
            $email_data['variables']['user_name'] = 'Akash';
            $email_data['variables']['body'] = $msg;
            $email_data['email'] = $emails->email;

            //send email method
            //sendgrid_email($email_data);
        }

        return true;
    }

    function course_expiry_trigger($trigger_id)
    {

        $notifications_auto = $this->common_model->getAllData('notifications_auto', '*', '1', array("status" => 1, "trigger_type" => $trigger_id));
        $current_date = date('Y-m-d');
        $q = $this->db->query("SELECT us.email, c.course_expiry,CONCAT (us.first_name,us.middle_name,us.last_name) as name FROM containers AS cont JOIN course_assigned_to_users AS cau ON cau.container_id = cont.id JOIN courses AS c ON c.id = cau.course_id JOIN users AS us ON us.id = cau.user_id LEFT JOIN notifications_auto AS na ON na.id = '" . $notifications_auto->id . "' JOIN notifications_auto_send_roles AS nasr ON nasr.notification_id = na.id JOIN users_roles AS ur ON ur.role_id = nasr.role_id WHERE c.status = 1 AND us.active = 1 GROUP BY us.email HAVING c.course_expiry = DATE_SUB('" . $current_date . "', INTERVAL " . $notifications_auto->days . " DAY) ")->result();

        //looping array to send email 
        foreach ($q as $emails) {
            $email_data['template_code'] = 'GROUP_NOTIFICATION';
            $email_data['variables']['user_name'] = 'Akash';
            $email_data['variables']['body'] = "Your course is going to expire in " . $notifications_auto->days . " days ";
            $email_data['email'] = $emails->email;

            //send email method
            //sendgrid_email($email_data);
        }

        return true;
    }
    /**************HELPER******************/
    /**
     * getCheckExist method
     * @description this method is use to check name is already exist or not
     * @param string
     * @return json array
     */
    public function getCheckExist()
    {
        //pr($_POST,"k");
        $this->form_validation->set_rules('name', 'Title', 'trim|required');
        if ($this->form_validation->run() == FALSE) 
        {
            $this->setup();
        } else {
            $this->load->model('Lessons_modal');
            $conditions = [
                'name' => $this->input->post('name'),
                'delete_status' => 1
            ];
            if(post('recordID') != 0 && post('recordID') != '')
            {
                $conditions['id !='] = post('recordID');
            }

            if ($this->Lessons_modal->getCount($conditions,'lessons') == 0) 
            {
                echo 'true';
                exit;
            } else {
                echo 'false';
                exit;
            }
        }
        exit;
    }

    public function get_course_used($lessonID)
    {
        $data['typeid'] = $lessonID;
        list($lessonID, $type) = explode("_", $lessonID);
        $this->load->model('Lessons_modal');
        $data['value'] = $this->Lessons_modal->get_lesson_used($lessonID);
        echo json_encode($data); 
        exit;
    }  

    public function get_module_used($lessonID){
        $data['typeid'] = $lessonID;
        list($lessonID, $type) = explode("_", $lessonID);
        $this->load->model('Lessons_modal');
        $data['value'] = $this->Lessons_modal->get_module_used($lessonID);
        echo json_encode($data); 
        exit;
    }
}
