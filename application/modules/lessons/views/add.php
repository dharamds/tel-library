<link rel="stylesheet" href="<?php bs('public/assets/plugins/multiselect/sumoselect.min.css') ?>" />
<div class="container-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <ul class="nav nav-tabs" role="tablist" id="addLessonTab">
                <li role="presentation" class="active">
                    <a href="#lesson_content" role="tab" data-toggle="tab">
                        Content
                    </a>
                </li>
                <li role="presentation">
                    <a href="#lesson_metadata" id="metadata_id" role="tab" data-toggle="tab">
                        Metadata
                    </a>
                </li>
            </ul>
        </div>

        <div class="panel-body">
            <div class="tab-content">
                <!--// Content Tab Start  //-->
                <div role="tabpanel" class="tab-pane active" id="lesson_content">
                    <?php
                    echo form_open('lessons/add/' . $id, [
                        'name' => 'frmAddLesson',
                        'id' => 'frmAddLesson',
                        'enctype' => 'multipart/form-data',
                    ]);
                    ?>

                    <input type="hidden" name="id" id="id" value=<?php echo $id ?>>
                    <input type="hidden" name="url_id" id="url_id" value=<?php echo $id ?>>
                    <div class="row">
                        <div class="col-md-6 p-0">
                            <div class="form-group col-xs-12">
                                <label class="control-label block">
                                    Lesson Templates
                                </label>
                                <div class="row col-xs-12 m-0 p-0 mt-3">
                                    <div class="col-md-12 p-0">
                                        
                                        <select name="template_type" id="template_type" class="form-control">
                                            <option value="">Select Lesson Template</option>
                                            <?php
                                            foreach ($lesson_template as $val) {
                                                //                         
                                                ?>
                                                <option <?php
                                                        if (!empty($lessonsData->template_type)) {
                                                            if ($lessonsData->template_type == $val->name) {
                                                                echo 'selected';
                                                            }
                                                        } else {
                                                            if ($val->name == 'A') {
                                                                echo 'selected';
                                                            }
                                                        }
                                                        ?> value="<?php echo $val->name; ?>">Template <?php echo $val->name; ?></option>
                                                }
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </div>

                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="row mt-4 mb-3">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="title" class="control-label">
                                    Title <span class="field-required">*</span>
                                </label>
                                <input type="text" name="name" value="<?php echo $lessonsData->name ?>" id="name" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label for="long_title" class="control-label">
                                    Long Title
                                </label>
                                <input type="text" name="long_title" value="<?php echo $lessonsData->long_title ?>" id="long_title" class="form-control">
                            </div>
                        </div>
                    </div>

                    <!---Lession Module-->
                    <div class="hr-line mt-4 mb-4 col-md-12 module_section"></div>
                    <div class="row module_section">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label for="title" class="control-label">
                                    Lesson Summary <span class="field-required">*</span>
                                </label>
                                <textarea class="editor" id="lession_summary" name="summary" rows="10"><?php echo $lessonsData->summary ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row module_section">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label for="title" class="control-label">
                                    Lesson Outcomes <span class="field-required">*</span>
                                </label>
                                <textarea class="editor" id="lession_outcomes" name="outcomes" rows="10"><?php echo $lessonsData->outcomes ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row module_section">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label for="title" class="control-label">
                                    Lesson Glossary
                                </label>
                                <textarea class="editor" id="lession_glossary" name="glossary" rows="10"><?php echo $lessonsData->glossary ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="hr-line mt-4 mb-4 col-md-12"></div>
                    <div id="temp_m">
                        <div class="row mt-4 mb-3" id="inquire_section_div">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="title" class="control-label">
                                        Inquire Section <span class="field-required">*</span>
                                    </label>
                                    <input type="text" name="inquire_section_title" value="<?php echo $lessonsData->inquire_section_title ?>" id="inquire_section_title" class="form-control" placeholder="Inquire Title">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <textarea class="editor" id="inquire_section" name="inquire_section" rows="10"><?php echo $lessonsData->inquire_section ?></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="hr-line mt-4 mb-4 col-md-12"></div>

                        <div class="row mt-4 mb-3">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="title" class="control-label">
                                        Watch Section <span class="field-required">*</span>
                                    </label>
                                    <input type="text" name="watch_title" value="<?php echo $lessonsData->watch_title; ?>" id="watch_title" class="form-control" placeholder="Watch Title">
                                </div>
                            </div>
                            <div class="col-sm-6">
                            <label for="title" class="control-label">
                                        Video Link <span class="field-required">*</span>
                                    </label>
                                <div class="form-group">
                                    <input type="url" name="watch_video_link" placeholder="https://example.com" pattern="https://.*" size="30" value="<?php echo $lessonsData->watch_video_link ?>" id="watch_video_link" class="form-control" placeholder="Video link">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row mt-4 mb-3">
                            
                            
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="col-sm-12 p-0">
                                    <div class="form-group">
                                        <input type="text" name="watch_video_transcript" value="<?php echo $lessonsData->watch_video_transcript ?>" id="watch_video_transcript" class="form-control" placeholder="Video transcript link">
                                    </div>
                                </div>
                                <div class="col-sm-12 p-0">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <span class="btn btn-primary btn-primary-default btn-file" id="watch_video_file">
                                            <span class="fileinput-new">Choose File</span>
                                            <span class="fileinput-exists">Choose File</span>
                                            <input type="text" id="temp_a_img_file" name="watch_video_file">
                                        </span>
                                        <?php 
                                        $show = !empty($lessonsData->watch_video_file) ? "display:inline-block" : "display:none";
                                        ?>
                                        <a class="btn btn-danger fileinput-exists" style="<?php echo $show ?>" id="image_remove">Remove</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 p-0">
                                <div class="col-sm-6 mb-3">
                                    <div class="fileinput fileinput-new" id="watch_file_img_show" data-provides="fileinput">
                                        <?php
                                        if (isset($lessonsData->watch_video_file) && (!empty($lessonsData->watch_video_file))) {
                                            ?>
                                            <img src="<?php echo base_url($lessonsData->watch_video_file); ?>" id="show_temp_a_img" style="width:200px;height:150px" class="img-rounded">
                                        <?php } else { ?>
                                            <img src="<?php echo base_url('/public/assets/img/logo-bg.png'); ?>" id="show_temp_a_img" style="width:200px;height:150px" class="img-rounded">
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!------------------->
                        <div class="row mt-4 mb-3">
                            
                            
                        </div>

                        <div class="hr-line mt-4 mb-4 col-md-12"></div>

                        <div class="row mt-4 mb-3">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="title" class="control-label">
                                        Read Section <span class="field-required">*</span>
                                    </label>
                                    <input type="text" name="read_section_title" value="<?php echo $lessonsData->read_section_title ?>" id="read_section_title" class="form-control" placeholder="Read Title">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <textarea id="read_section" name="read_section" class="editor" rows="10"><?php echo $lessonsData->read_section ?></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="hr-line mt-4 mb-4 col-md-12"></div>

                        <div class="row mt-4 mb-3">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="title" class="control-label">
                                        Reflect Section <span class="field-required">*</span>
                                    </label>
                                    <div class="col-md-12 p-0 mt-4" id="reflect_each">
                                        <div class="flex-row m-0">
                                            <div class="flex-col-12 flex-col-md-3 pl-0" id="reflect_system_category">
                                            </div>
                                            <div class="flex-col-12 flex-col-md-2 pl-0" id="reflect_system_tag">
                                            </div>
                                            <div class="flex-col-12 flex-col-md-3 pl-0" id="reflect_poll_category">
                                            </div>
                                            <div class="flex-col-12 flex-col-md-2 pl-0" id="reflect_poll_tag">
                                            </div>
                                            <div class="flex-col-12 flex-col-md-auto pl-0">
                                                <a href="JavaScript:void(0);" class="btn btn-primary btn-block" id="reflect_search">Search</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 p-0 pt-4">
                                <label class="col-md-12">
                                    Select a Poll to Embed on this page
                                </label>
                                <div class="col-md-7 col-xs-12">
                                    <select class="form-control" id="poll_id" name="poll_id">
                                        <option value=""> Select Poll</option>
                                        <?php if($PollData->id!=0) { ?>
                                        <option selected value="<?=$PollData->id?>"><?=$PollData->title?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="hr-line mt-4 mb-4 col-md-12"></div>

                        <div class="row mt-4 mb-3">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="title" class="control-label">
                                        Expand Section <span class="field-required">*</span>
                                    </label>
                                    <input type="text" name="expand_title" value="<?php echo $lessonsData->expand_title ?>" id="expand_title" class="form-control" placeholder="Expand Title">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <textarea id="expand_description" name="expand_description" rows="10" class="editor"><?php echo $lessonsData->expand_description ?></textarea>
                                </div>
                            </div>
                        </div>
                        <!---Practice Section-->
                        <div class="hr-line mt-4 mb-4 col-md-12 practice_section"></div>

                        <div class="row mt-4 mb-3 practice_section">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="title" class="control-label">
                                        Practice Section
                                    </label>
                                    <input type="text" name="practice_title" value="<?php echo $lessonsData->practice_title ?>" id="practice_title" class="form-control practice_section_val" placeholder="Practice Title">
                                </div>
                            </div>
                        </div>
                        <div class="row practice_section">
                            <div class="col-sm-8">
                                <div class="form-group">

                                    <textarea class="editor " id="practice_description" name="practice_description" rows="10"><?php echo $lessonsData->practice_description ?></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-4 mb-3">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="title" class="control-label">
                                        Check your Knowledge Section <span class="field-required">*</span>
                                    </label>
                                    <div class="col-md-12 p-0 mt-4" id="cyk_each">
                                        <div class="flex-row m-0">
                                            <div class="flex-col-12 flex-col-md-auto pl-0" id="cks_sys_cat">

                                            </div>
                                            <div class="flex-col-12 flex-col-md-auto pl-0" id="cks_sys_tag">

                                            </div>
                                            <div class="flex-col-12 flex-col-md-2 pl-0" id="cks_ass_cat">

                                            </div>
                                            <div class="flex-col-12 flex-col-md-auto pl-0" id="cks_ass_tag">

                                            </div>

                                            <div class="flex-col-12 flex-col-md-auto pl-0">
                                                <a href="JavaScript:void(0);" class="btn btn-primary btn-block" id="cyk_search">Search</a>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 p-0 pt-4">
                                <label class="col-md-12">
                                    Select a Quiz to Embed on this page
                                </label>
                                <div class="col-md-7 col-xs-12">
                                    <select class="form-control" id="quiz_id" name="quiz_id">
                                        <option value="">Select Quiz</option>
                                        <?php if($QuizData->id!=0) { ?>
                                        <option selected value="<?=$QuizData->id?>"><?=$QuizData->title?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="hr-line mt-4 mb-4 col-md-12"></div>
                    </div>
                    <div class="row mt-4 mb-3">
                        <div class="col-sm-12">
                            <h4 class="f17 fw500 text-dark">Lesson Resources Section <span class="field-required">*</span></h4>
                        </div>
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label for="title" class="control-label">
                                    Lesson Toolbox <span class="field-required">*</span>
                                </label>
                                <textarea id="lesson_toolbox" name="lesson_toolbox" rows="10" class="editor"><?php echo $lessonsData->lesson_toolbox; ?></textarea>
                            </div>
                        </div>
                        <div class="col-sm-8 mt-3 license_citation_div">
                            <div class="form-group">
                                <label for="title" class="control-label">
                                    License and Citations <span class="field-required">*</span>
                                </label>
                                <textarea id="license_citation" name="license_citation" rows="10" class="editor"><?php echo $lessonsData->license_citation; ?></textarea>
                            </div>
                        </div>
                        <div class="hr-line col-md-12 mt-5"></div>


                    </div>

                    <div class="hr-line mt-4 mb-4 col-md-12"></div>

                    <div class="col-xs-12 text-right p-0">
                        <button class="btn btn-primary" data-event-bind="#addLessonTab">Save & Next</button>
                    </div>



                    </form>
                </div>
                <!--//  Content Tab End  //-->
                <!--//  Metadata Tab Start  //-->

                <div role="tabpanel" class="tab-pane" id="lesson_metadata">
                    <div class="istVisualContainer">
                        <div class="row">
                            <div class="row col-sm-10">
                                <div class="form-group col-xs-12 mt-4">
                                    <label class="control-label block"> Lesson Image </label>
                                </div>
                                <div class="col-sm-3 mr-5">
                                    <div class="ist-logo">
                                        <?php
                                        if (isset($lessonsData->lesson_img) && (!empty($lessonsData->lesson_img))) {
                                            ?>
                                            <img src="<?php echo base_url($lessonsData->lesson_img); ?>" id="image_write_path" class="img-rounded">
                                        <?php } else { ?>
                                            <img src="<?php echo base_url(); ?>/public/assets/img/logo-bg.png" id="image_write_path" class="img-rounded">
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="ist-visual-info">
                                        <h4>Select Lesson Image</h4>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input type="hidden" name="old_img" value="">
                                                <div class="fileinput fileinput-new col-md-12 p-0" data-provides="fileinput">
                                                    <span class="btn btn-primary btn-primary-default btn-file" id="choose_media_image_file1">
                                                        <span class="fileinput-new">Choose File</span>
                                                        <span class="fileinput-exists">Choose File</span>
                                                    </span>
                                                    <a class="btn btn-danger fileinput-exists delete_img_main_page">Remove</a>
                                                </div>

                                                <div class="small"> (File must be .png, .jpg, .jpeg) </div>
                                            </div>
                                        </div>
                                        <div style="color:red;" id="upload_error"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <?php
                    echo form_open('lessons/add/' . $id, [
                        'name' => 'form_lesson_metadata',
                        'id' => 'form_lesson_metadata',
                        'enctype' => 'multipart/form-data',
                    ]);
                    ?>
                    <input type="hidden" name="img_name" id="img_name" value="<?php echo $lessonsData->lesson_img; ?>">
                    <div class="row">
                        <div class="col-md-6 p-0">
                            <div class="form-group col-xs-12">
                                <label class="control-label block">
                                    Lesson Availability
                                </label>
                                <div class="row col-xs-12 m-0 p-0 mt-3">
                                    <div class="col-md-6 p-0">
                                        <label class="radio-tel m-0">
                                            <input type="radio" value="1" name="lesson_availability" id="lesson_availability" <?php
                                                                                                                                if ($lessonsData->lesson_availability == 1) {
                                                                                                                                    echo "checked";
                                                                                                                                }
                                                                                                                                ?> class="mr-3 t2">
                                            Available
                                        </label>
                                    </div>
                                    <div class="col-md-6 p-0">
                                        <label class="radio-tel m-0">
                                            <input type="radio" value="0" name="lesson_availability" id="lesson_availability" <?php
                                                                                                                                if ($lessonsData->lesson_availability == 0) {
                                                                                                                                    echo "checked";
                                                                                                                                }
                                                                                                                                ?> class="mr-3 t2">
                                            Hidden
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 p-0 ">
                            <div class="form-group col-xs-12 mb-3">
                                <div>
                                    <label class="checkbox-tel m-0">
                                        <input type="checkbox" value="1" name="lesson_expiry_check" id="lesson_expiry_check" <?php
                                                                                                                                if ($lessonsData->lesson_expiry_check == 1) {
                                                                                                                                    echo "checked";
                                                                                                                                }
                                                                                                                                ?> class="mr-3 t2">Set Lesson Expiry
                                    </label>
                                </div>

                                <div class="form-group colorPicker pl-3 pt-4 m-0" id="lesson_expiry_div">
                                    <label class="col-md-5">Lesson Expiry Date</label>
                                    <div class="input-group col-md-5">
                                        <?php if (date("m/d/Y", strtotime($lessonsData->lesson_expiry)) == '01/01/1970' || $lessonsData->lesson_expiry == '0000-00-00') { ?>
                                            <input type="text" id="lesson_expiry" name="lesson_expiry" class="form-control" value="">
                                            <span class="input-group-addon">
                                                <span class="flaticon-calendar-1"></span>
                                            </span>
                                        <?php } else { ?>
                                            <input type="text" id="lesson_expiry" name="lesson_expiry" class="form-control" value="<?php echo date("m/d/Y", strtotime($lessonsData->lesson_expiry)) ?>">
                                            <span class="input-group-addon">
                                                <span class="flaticon-calendar-1"></span>
                                            </span>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="hr-line mt-4 mb-4"></div>

                    <div class="col-md-12 p-0">
                        <div class="control-label block mb-4 f16">
                            Glossary Display on this Page
                        </div>
                        <div class="pl-4 col-md-12">
                            <div class="form-group">
                                <label class="control-label block">
                                    Inline Pop-ups on this Page
                                </label>
                                <div class="col-md-3 p-0">
                                    <label class="radio-tel m-0">
                                        <input type="radio" value="1" name="show_glossary" id="show_glossary" <?php
                                                                                                                if ($lessonsData->show_glossary == 1) {
                                                                                                                    echo "checked";
                                                                                                                }
                                                                                                                ?> class="mr-3 t2">Available
                                    </label>
                                </div>
                                <div class="col-md-3 p-0">
                                    <label class="radio-tel m-0">
                                        <input type="radio" name="show_glossary" id="show_glossary" value="0" <?php
                                                                                                                if ($lessonsData->show_glossary == 0) {
                                                                                                                    echo "checked";
                                                                                                                }
                                                                                                                ?> class="mr-3 t2">
                                        Hidden
                                    </label>
                                </div>
                                <div class="col-md-3 p-0">
                                    <label class="radio-tel m-0">
                                        <input type="radio" name="show_glossary" id="show_glossary" value="2" <?php
                                                                                                                if ($lessonsData->show_glossary == 2) {
                                                                                                                    echo "checked";
                                                                                                                }
                                                                                                                ?> class="mr-3 t2">
                                        Only for first instance
                                    </label>
                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="hr-line col-md-12 mt-4 mb-5"></div>

                    <div class="col-xs-12 text-right">
                        <button type="submit" id="saveMetadata" class="btn btn-primary"><?php echo isset($id) ? 'Save changes' : 'Save and add' ?></button>
                    </div>
                    <div class="hr-line col-md-12 mt-5">&nbsp;</div>
                    <div class="col-md-12 p-0">

                        <div class="pl-4 col-md-12">

                            <div class="col-md-6 pl-0 pt-5">
                                <div class="panel panel-grey">
                                    <div class="panel-heading mb-3">
                                        <h2><span> System categories for Glossary </span></h2>
                                    </div>
                                    <div class="panel-body panel-collapse-body">
                                        <div class="tag-lib-input brd-0 mb-0 pb-0 checkbox-list-search open" id="system_categories_glossary_metadata">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 pl-0 pt-5">
                                <div class="panel panel-grey">
                                    <div class="panel-heading">
                                        <h2><span> System Tags for Glossary</span></h2>
                                        <div class="panel-ctrls button-icon-bg">
                                        </div>
                                    </div>
                                    <div class="panel-body" id="syatem_tags_glossary_metadata">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="pl-4 col-md-12">

                            <div class="col-md-6 pl-0 pt-5">
                                <div class="panel panel-grey">
                                    <div class="panel-heading mb-3">
                                        <h2><span> Glossary categories for Glossary </span></h2>
                                    </div>
                                    <div class="panel-body panel-collapse-body">
                                        <div class="tag-lib-input brd-0 mb-0 pb-0 checkbox-list-search open" id="glossary_categories_metadata">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 pl-0 pt-5">
                                <div class="panel panel-grey">
                                    <div class="panel-heading">
                                        <h2><span> Glossary Tags for Glossary</span></h2>
                                        <div class="panel-ctrls button-icon-bg">
                                        </div>
                                    </div>
                                    <div class="panel-body" id="glossary_tags_metadata">

                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="hr-line col-md-12 mt-5"></div>

                        <div class="pl-4 col-md-12">
                            <div class="col-md-6 pl-0 pt-5">
                                <div class="panel panel-grey">
                                    <div class="panel-heading mb-3">
                                        <h2><span>Lesson Categories</span></h2>
                                    </div>
                                    <div class="panel-body panel-collapse-body">
                                        <div class="tag-lib-input brd-0 mb-0 pb-0 checkbox-list-search open" id="lesson_categories_metadata">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 pl-0 pt-5">
                                <div class="panel panel-grey">
                                    <div class="panel-heading">
                                        <h2><span>Lesson Tags</span></h2>
                                        <div class="panel-ctrls button-icon-bg">
                                        </div>
                                    </div>
                                    <div class="panel-body" id="lesson_tags_metadata">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="hr-line col-md-12"></div>

                        <div class="pl-4 col-md-12">
                            <div class="col-md-6 pl-0 pt-5">
                                <div class="panel panel-grey">
                                    <div class="panel-heading mb-3">
                                        <h2><span>System Categories</span></h2>
                                    </div>
                                    <div class="panel-body panel-collapse-body">
                                        <div class="tag-lib-input brd-0 mb-0 pb-0 checkbox-list-search open" id="system_categories_metadata">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 pl-0 pt-5">
                                <div class="panel panel-grey">
                                    <div class="panel-heading">
                                        <h2><span>System Tags</span></h2>
                                        <div class="panel-ctrls button-icon-bg">
                                        </div>
                                    </div>
                                    <div class="panel-body" id="lesson_system_tags_metadata">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="hr-line col-md-12"></div>

                        <div class="pl-4 col-md-12">
                            <div class="col-md-6 pl-0 pt-5">
                                <div class="panel panel-grey">
                                    <div class="panel-heading mb-3">
                                        <h2><span>Learning Outcomes</span></h2>
                                    </div>
                                    <div class="panel-body panel-collapse-body">
                                        <div class="tag-lib-input brd-0 mb-0 pb-0 checkbox-list-search open" id="learning_outcomes_metadata">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 pl-0 pt-5">
                                <div class="panel panel-grey">
                                    <div class="panel-heading mb-3">
                                        <h2><span>TEL Mastery Standards</span></h2>
                                    </div>
                                    <div class="panel-body panel-collapse-body">
                                        <div class="tag-lib-input brd-0 mb-0 pb-0 checkbox-list-search open" id="learning_standards_metadata">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        
                        </form>
                    </div>
                </div>
                <!--//  Metadata Tab End  //-->
            </div>
        </div>
    </div>
</div>

</div>
<div class="modal fade modals-tel-theme" id="gallery_images_popup" role="dialog">
    <div class="modal-dialog w80p">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Upload Image</h4>
            </div>
            <div class="modal-body p-0">
                <div id="popup_img_div"></div>
            </div>
        </div>
    </div>
</div>


<!-- #page-content -->

<script type="text/javascript">
    // Show media library

    function template_a_media_image_callback(image_name, img_full_url) {
        $('#watch_file_img_show').html(CommanJS.getImagebox(img_full_url, 'thumbnail', 'f45 text-center mt-3 pt-5'));
        $('#temp_a_img_file').val(image_name);
        $('#image_remove').css('display','inline-block');
        $('#image_remove').show();
    }

    $("#watch_video_file").click(function() {
        CommanJS.choose_media_image_file('template_a_media_image_callback');
    });

    $(document).on('click', '#image_remove', function() {

        var module_id = '<?php echo $glossary_id; ?>';
        var filename = $("#image_file_name").val();
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure?',
            buttons: {
                confirm: function() {
                    $("#temp_a_img_file").val('');
                    $("#watch_file_img_show").html('');
                    $("#watch_file_img_show").append('<img src="<?php echo bs("public/assets/img/logo-bg.png"); ?>" id="image_write_path" class="img-rounded">');
                    $('#image_remove').hide();
                },
                cancel: function() {
                    return true;
                }
            }
        });
    });

    function media_image_callback_metadata(image_name, img_full_url) {

        $('.ist-logo').html(CommanJS.getImagebox(img_full_url, 'thumbnail', 'f45 text-center mt-3 pt-5'));
        $('#img_name').val(image_name);
    }

    $("#choose_media_image_file1").click(function() {
        CommanJS.choose_media_image_file('media_image_callback_metadata');
    });

    function showValues() {
        var str = $("form").serialize();
        return str;
    }
    $(document).ready(function() {
        if($('#id').val() != '' || $('#id').val() > 0){
            $('.delete_img_main_page').show();
        }
        $('.delete_img_main_page').off('click').on('click', function() {

                var module_id = '<?php echo $module_id; ?>';
                var filename = $("#img_name").val();
                //var scope = $(this);

                $.confirm({
                    title: 'Confirm!',
                    content: 'Are you sure?',
                    buttons: {
                        confirm: function() {
                            $("#img_name").val('');
                            $(".ist-logo").html('');
                            $(".ist-logo").append('<img src="<?php echo bs("public/assets/img/logo-bg.png"); ?>" id="image_write_path" class="img-rounded">');
                        },
                        cancel: function() {
                            return true;
                        }
                    }
                });
            });
        $('.ist-logo').html(CommanJS.getImagebox($('#image_write_path').attr('src'), 'thumbnail', 'f45 text-center mt-3 pt-5'));
        $('#watch_file_img_show').html(CommanJS.getImagebox($('#show_temp_a_img').attr('src'), 'thumbnail', 'f45 text-center mt-3 pt-5'));
        $('[data-toggle="tooltip"]').tooltip();
        $('#document_error').hide();

        if ($('#lesson_expiry').val() == '') {
            $('#lesson_expiry_div').hide();
            $('#lesson_expiry_check').iCheck('uncheck');
        }
        $("input[type='checkbox'], input[type='radio']").on("click", showValues);

        $('#lesson_expiry_check').on('change', function() {
            if ($('#lesson_expiry_check').is(":checked")) {
                $('#lesson_expiry_div').show();
                $('#frmAddLesson').validate();

                $('#lesson_expiry').rules('add', {
                    required: true,
                    messages: {
                        required: "Please select date.",
                    }
                });

            } else {
                $('#lesson_expiry').rules('remove');
                $('#lesson_expiry').val('');
                $('#lesson_expiry_div').hide();
            }
        })



        $("#lesson_expiry").datepicker({
            changeYear: true,
            changeMonth: true,
            numberOfMonths: 1,
            minDate: 0
        });

        var config = {
            height: 120,
            toolbar: 'short',
            allowedContent: true,
        };
        // ck editor
        $('.editor').each(function(e) {
            CKEDITOR.replace(this.id, config);
        });

        $('#metadata_id').click(function() {
            var lesson_id = $('#id').val();
            if (lesson_id != '') {
                set_metadata(lesson_id);
            } else {
                return false;
            }
        });
        $('#removeResp').hide();
        var rescnt = ($('.res').length);
        if (rescnt > 2) {
            $('#removeResp').show();
        }

        $("#frmAddLesson").validate({
            ignore: [],
            rules: {
                name: {
                    required: true,
                    remote:{
                        url: "<?php echo bs(); ?>lessons/getCheckExist",
                        type: 'POST',
                        data: {'recordID':$('#id').val()}
                    }
                },
                inquire_section_title: {
                    required: function(textarea) {
                        if ($('#template_type').val() == 'A' || $('#template_type').val() == 'P') {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                watch_title: {
                    required: function(textarea) {
                        if ($('#template_type').val() == 'A' || $('#template_type').val() == 'P') {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                read_section_title: {
                    required: function(textarea) {
                        if ($('#template_type').val() == 'A' || $('#template_type').val() == 'P') {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                expand_title: {
                    required: function(textarea) {
                        if ($('#template_type').val() == 'A' || $('#template_type').val() == 'P') {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                summary: {
                    required: function(textarea) {

                        if ($('#template_type').val() == 'M') {
                            CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                            var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                            return editorcontent.length === 0;
                        }
                    }
                },
                outcomes: {
                    required: function(textarea) {
                        if ($('#template_type').val() == 'M') {
                            CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                            var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                            return editorcontent.length === 0;
                        }
                    }
                },
                inquire_section: {
                    required: function(textarea) {
                        if ($('#template_type').val() == 'A' || $('#template_type').val() == 'P') {
                            CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                            var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                            return editorcontent.length === 0;
                        }
                    }
                },
                read_section: {
                    required: function(textarea) {
                        if ($('#template_type').val() == 'A' || $('#template_type').val() == 'P') {
                            CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                            var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                            return editorcontent.length === 0;
                        }
                    }
                },
                expand_description: {
                    required: function(textarea) {
                        if ($('#template_type').val() == 'A' || $('#template_type').val() == 'P') {
                            CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                            var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                            return editorcontent.length === 0;
                        }
                    }
                },
                lesson_toolbox: {
                    required: function(textarea) {
                        CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                        return editorcontent.length === 0;
                    }
                },
                license_citation: {
                    required: function(textarea) {
                        if ($('#template_type').val() == 'A' || $('#template_type').val() == 'P') {
                            CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                            var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                            return editorcontent.length === 0;
                        }
                    }
                },
            },
            messages: {
                name: {
                    required:"Please enter name",
                    remote: "Course name is already exist"
                },
                inquire_section_title: "Please enter inquire section title",
                watch_title: "Please enter watch title",
                read_section_title: "Please enter read section title",
                expand_title: "Please enter expand title",
                summary: "Please enter lesson summary",
                outcomes: "Please enter lesson outcomes",
                inquire_section: "Please enter inquire section",
                read_section: "Please enter read section",
                expand_description: "Please enter expand description",
                lesson_toolbox: "Please enter lesson toolbox",
                license_citation: "Please enter license citation",
            },
            errorPlacement: function(error, $elem) {
                if ($elem.is('textarea')) {
                    $elem.insertAfter($elem.next('div'));
                }
                error.insertAfter($elem);
            },
            submitHandler: function(form) {
                for (instance in CKEDITOR.instances) {
                    CKEDITOR.instances[instance].updateElement();
                }

                $.ajax({
                    url: $('form#frmAddLesson').attr('action'),
                    method: "POST",
                    data: $('form#frmAddLesson').serialize(),
                    dataType: "json",
                    complete: function(xhr, status) {},
                    success: function(data) {
                        if ($('#url_id').val() != '') {
                            CommanJS.getDisplayMessgae(200, 'Lesson updated successfully.');
                        } else {
                            CommanJS.getDisplayMessgae(200, 'Lesson Created Successfully.');
                        }
                        $('#id').val(data);
                        $('#metadata_id').trigger('click');
                        $(window).scrollTop(0);
                    }
                });
                return false;
            }

        });

        //get data from category/tag assigned table
        //data-type => metadata category / tag
        //data-sub_type => metadata type [Poll,Assesment/System,etc.]
        $('#reflect_search').click(function() {
            $.ajax({
                url: "<?php bs('lessons/reflect_search'); ?>",
                method: "POST",
                data: {
                    type: 'poll',
                    system_category_poll: CommanJS.getMetaCallBack('system_categories'),
                    system_tag_poll: CommanJS.getMetaCallBack('system_tags'),
                    poll_category: CommanJS.getMetaCallBack('poll_categories'),
                    poll_tag: CommanJS.getMetaCallBack('poll_tags')
                },
                dataType: "json",
                success: function(data) {
                    if (data != '') {
                        $('#poll_id').find('option').remove();
                        $('#poll_id').append('<option value=""> Select Poll </option>');
                        $.each(data, function(key, value) {
                            $('#poll_id').append('<option value=' + key + '>' + value + '</option>');
                        });
                    }
                }
            });
        });

        $('#cyk_search').click(function() {

            $.ajax({
                url: "<?php bs('lessons/reflect_search'); ?>",
                method: "POST",
                data: {
                    type: 'quiz',
                    system_cat: CommanJS.getMetaCallBack('system_category'),
                    system_tag: CommanJS.getMetaCallBack('system_tag'),
                    assessment_category: CommanJS.getMetaCallBack('assessment_category'),
                    assessment_tag: CommanJS.getMetaCallBack('assessment_tag'),
                },
                dataType: "json",
                success: function(data) {
                    if (data != '') {
                        $('#quiz_id').find('option').remove();
                        $('#quiz_id').append('<option value=""> Select Quiz </option>');
                        $.each(data, function(key, value) {
                            $('#quiz_id').append('<option value=' + key + '>' + value + '</option>');
                        });
                    }
                }
            });

        });

        $("#form_lesson_metadata").validate({
            ignore: [],
            rules: {
                lesson_availability: {
                    required: true
                },
                show_glossary: {
                    required: true
                }
            },
            messages: {
                lesson_availability: "Please enter availability",
                show_glossary: "Please select expiry"
            },
            errorPlacement: function(error, $elem) {
                if ($elem.is('textarea')) {
                    $elem.insertAfter($elem.next('div'));
                }
                error.insertAfter($elem);
            },
            submitHandler: function(form) {
                $.ajax({
                    url: "<?php bs('lessons/metadata_save'); ?>",
                    method: "POST",
                    data: showValues(),
                    dataType: "json",
                    success: function(data) {
                        CommanJS.getDisplayMessgae(200, 'Lesson updated successfully.');
                    }
                });
                return false;
            }

        });

        // call Reflect Section filters
        CommanJS.get_metadata_options("System categories", 1, <?php echo META_SYSTEM; ?>, "reflect_system_category");
        CommanJS.get_metadata_options("System tags", 2, <?php echo META_SYSTEM; ?>, "reflect_system_tag");
        CommanJS.get_metadata_options("Poll categories", 1, <?php echo META_POLL; ?>, "reflect_poll_category");
        CommanJS.get_metadata_options("Poll tags", 2, <?php echo META_POLL; ?>, "reflect_poll_tag");

        // call Check your Knowledge Section filters
        CommanJS.get_metadata_options("System category", 1, <?php echo META_SYSTEM; ?>, "cks_sys_cat");
        CommanJS.get_metadata_options("System tag", 2, <?php echo META_SYSTEM; ?>, "cks_sys_tag");
        CommanJS.get_metadata_options("Assessment category", 1, <?php echo META_ASSESSMENT; ?>, "cks_ass_cat");
        CommanJS.get_metadata_options("Assessment tag", 2, <?php echo META_ASSESSMENT; ?>, "cks_ass_tag");


        function set_metadata(id) {
            // Call Metadata Sections Filters

            //For glossary
            CommanJS.getCatSection(<?php echo META_SYSTEM; ?>, "system_categories_glossary_metadata", "system_categories_glossary", id, <?php echo META_SYSTEM_GLOSSARY; ?>);
            CommanJS.getTagSection(<?php echo META_SYSTEM; ?>, "syatem_tags_glossary_metadata", "syatem_tags_glossary", id, <?php echo META_SYSTEM_GLOSSARY; ?>);
            
            CommanJS.getCatSection(<?php echo META_GLOSSARY; ?>, "glossary_categories_metadata", "glossary_categories", id, <?php echo META_SYSTEM_GLOSSARY; ?>);
            CommanJS.getTagSection(<?php echo META_GLOSSARY; ?>, "glossary_tags_metadata", "glossary_tags", id, <?php echo META_SYSTEM_GLOSSARY; ?>);
         
            //For lesson
            CommanJS.getCatSection(<?php echo META_LESSON; ?>, "lesson_categories_metadata", "lesson_categories", id, <?php echo META_LESSON; ?>);
            CommanJS.getTagSection(<?php echo META_LESSON; ?>, "lesson_tags_metadata", "lesson_tags", id, <?php echo META_LESSON; ?>);
            CommanJS.getCatSection(<?php echo META_SYSTEM; ?>, "system_categories_metadata", "system_categories", id, <?php echo META_LESSON; ?>);
            CommanJS.getTagSection(<?php echo META_SYSTEM; ?>, "lesson_system_tags_metadata", "lesson_system_tags", id, <?php echo META_LESSON; ?>);
            CommanJS.getCatSection(<?php echo META_LEARNING_OUTCOMES; ?>, "learning_outcomes_metadata", "learning_outcomes", id, <?php echo META_LESSON; ?>);
            CommanJS.getCatSection(<?php echo META_LEARNING_STANDARDS; ?>, "learning_standards_metadata", "learning_standards", id, <?php echo META_LESSON; ?>);
        }

    });

    // on template change
    $(document).ready(function() {
        var template = $('#template_type').val();
        manageFields(template);
    });
    $('#template_type').change(function() {
        var template = $(this).val();
        manageFields(template);
    });

    function manageFields(template) {

        if (template == 'A') {
            $('.practice_section').hide();
            $('.module_section').hide();
            $('#temp_m').show();
            $('.license_citation_div').show();

        } else if (template == 'M') {
            $('.practice_section').hide();
            $('.module_section').show();
            $('#temp_m').hide();
            $('.license_citation_div').hide();


        } else if (template == 'P') {

            $('.practice_section').show();
            $('.module_section').hide();
            $('#temp_m').show();
            $('.license_citation_div').show();


        }
    }
</script>