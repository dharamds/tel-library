<style>
    .selectAllform {
        display: inline;
        margin: 0;
        padding: 0;
    }
</style>


<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="filter-container flex-row">
                <div class="flex-col-sm-3 selected_condition" id="system_cat">

                </div>
                <div class="flex-col-sm-2 selected_condition" id="system_tag">

                </div>
                <div class="flex-col-sm-3 selected_condition" id="lesson_cat">

                </div>
                <div class="flex-col-sm-2 selected_condition" id="lesson_tag">

                </div>
                <div class="flex-col-12 flex-col-md-auto ml-auto">
                    <a class="btn btn-danger pt-2" id="reset_form" href="javascript:void(0)"><i class="flaticon-close-1 f21 fw100"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default panel-grid">
                    <div class="panel-heading">
                        <?php if ($this->ion_auth->is_admin() || $permissions->add_per): ?>
                            <a class="btn btn-primary" href="<?= base_url('lessons/add') ?>">New</a>
                        <?php endif; ?>

                        <?php if ($this->ion_auth->is_admin() || $permissions->delete_per): ?>
                            <a href="<?php echo base_url('lessons/setAllSelected') ?>" class="btn btn-primary" id="bulk_lesson">
                                Set all selected as available
                            </a>
                        <?php endif; ?>
                        <?php if ($this->ion_auth->is_admin() || $permissions->delete_per): ?>
                            <a href="<?php echo base_url('lessons/setAllSelected') ?>" class="btn btn-primary" id="bulk_lesson2">
                                Set all selected as unavailable
                            </a>
                        <?php endif; ?>

                        <div class="panel-ctrls"></div>
                    </div>
                    <div class="panel-body">
                        <div class="row m-0">
                            <div class="col-md-12 p-0">
                                <table id="lessonListTable" class="table table-bordered table-striped table-hover" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>
                                                <input type="checkbox" class="checkAll">
                                            </th>
                                            <th style="min-width: 100px;">Action</th>
                                            <th style="min-width: 180px;">Lesson Available</th>
                                            <th style="min-width: 120px;">Lesson Title</th>
                                            <th style="min-width: 180px;">Long Title</th>
                                            <th style="min-width: 180px;">Lesson Categories</th>
                                            <th style="min-width: 120px;">Lesson Tags</th>
                                            <th style="min-width: 180px;">System Categories</th>
                                            <th style="min-width: 120px;">System Tags</th>
                                            
                                            <th style="min-width: 180px;">Lesson Template Used</th>
                                            <th style="min-width: 120px;">Date Created</th>
                                            <th style="min-width: 120px;">Created By</th>
                                            <th style="min-width: 180px;">Used in these Courses</th>
                                            <th style="min-width: 180px;">Used in these Modules</th>
                                        </tr>
                                    </thead>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#reset_form').click(function() {
            $(".meta_data_filter").prop("checked",false);
            $(".selectFilterMode span").text('');            
            table.search('').draw();
        });

        $('.checkAll').click(function () {
            $(':checkbox.checkLesson').prop('checked', this.checked);
        });
        CommanJS.get_metadata_options("System category", 1, <?=META_SYSTEM;?>, "system_cat");
        CommanJS.get_metadata_options("System tag", 2, <?=META_SYSTEM;?>, "system_tag");
        CommanJS.get_metadata_options("Lesson category", 1, <?=META_LESSON;?>, "lesson_cat");
        CommanJS.get_metadata_options("Lesson tag", 2, <?=META_LESSON;?>, "lesson_tag");
   
        $("#bulk_lesson").click(function (e) {
            e.preventDefault();
            var selected_user_id = "";
            $('input[name="item[]"]:checked').each(function () {
                //  console.log(this.value);
                selected_user_id = this.value + '|' + selected_user_id;
            });
            if (selected_user_id != "") {
                $('#check').val(selected_user_id);
            } else {
                CommanJS.getDisplayMessgae(400, 'Please select atleast one lesson.')
                return false;
            }
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('lessons/setAllSelected'); ?>",
                data: {
                    selectedLesson: selected_user_id,
                    status: '1'
                },
                dataType: 'json',
                success: function (data) {
                    //console.log(data);
                    if (data.msg === 'Updated') {
                        $('.checkAll').prop('checked', false);
                        table.ajax.reload(); //just reload table
                    }
                }
            });
        });
        $("#bulk_lesson2").click(function (e) {
            e.preventDefault();
            var selected_user_id = "";
            $('input[name="item[]"]:checked').each(function () {
                // console.log(this.value);
                selected_user_id = this.value + '|' + selected_user_id;
            });
            if (selected_user_id != "") {
                $('#check2').val(selected_user_id);
            } else {
                CommanJS.getDisplayMessgae(400, 'Please select atleast one lesson.')
                return false;
            }

            $.ajax({
                type: "POST",
                url: "<?php echo base_url('lessons/setAllSelected'); ?>",
                data: {
                    selectedLesson: selected_user_id,
                    status: '0'
                },
                dataType: 'json',
                success: function (data) {
                    if (data.msg === 'Updated') {
                        $('.checkAll').prop('checked', false);
                        table.ajax.reload(); //just reload table
                    }
                }
            });
        });
        
        //alert('sss');
        var table = $('#lessonListTable').DataTable({
            // Processing indicator
            "processing": true,
            // DataTables server-side processing mode
            "serverSide": true,
            // Initial no order.
            "iDisplayLength": 10,
            "bPaginate": true,
            "order": [],
            "scrollX": true,
            "autoWidth": false,
            "drawCallback": function(settings) {
                $('.load_meta_data').each(function(key, item) {
                    $.getJSON("<?php echo bs('questions/get_meta_tags/'); ?>" + $(this).attr('id'), function(data) {
                           if(data) $("#"+data.typeid).html(data.value);
                    });
                });
                $('.load_meta_category').each(function(key, item) {
                    $.getJSON("<?php echo bs('questions/get_meta_categories/'); ?>" + $(this).attr('id'), function(data) {
                           if(data) $("#"+data.typeid).html(data.value);
                    });
				});

                $('.load_course_data').each(function(key, item) {
                    $.getJSON("<?php echo bs('lessons/get_course_used/'); ?>" + $(this).attr('id'), function(data) {
                        if (data) $("#" + data.typeid).html(data.value);
                    });
                });

                $('.load_module_data').each(function(key, item) {
                    $.getJSON("<?php echo bs('lessons/get_module_used/'); ?>" + $(this).attr('id'), function(data) {
                        if (data) $("#" + data.typeid).html(data.value);
                    });
                });
                $('.load_module_users').each(function(key, item) {
                    $.getJSON("<?php echo bs('users/get_created_by/lessons/'); ?>" + $(this).attr('id'), function(data) {
                        if (data) $("#" + data.typeid).html(data.value);
                    });
                });
            },
            // Load data from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('lessons/getLists/'); ?>",
                "type": "POST",
                "data": function (data) {
                    data.system_cat = CommanJS.getMetaCallBack('system_category'); 
                    data.system_tag = CommanJS.getMetaCallBack('system_tag'); 
                    data.lesson_cat = CommanJS.getMetaCallBack('lesson_category'); 
                    data.lesson_tag = CommanJS.getMetaCallBack('lesson_tag'); 
                    data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                },
            },
            //Set column definition initialisation properties
            "columnDefs": [{
                    "targets": [0],
                    "orderable": false,
                    "data": null,
                    "render": function (data, type, full, meta) {
                        var data = '';
                        if (type == 'display') {
                            data = '<input type="checkbox" class="checkLesson" name="item[]" value="' + full['id'] + '">';
                        }
                        return data;
                    }
                },
                {
                    "targets": [1],
                    "data": null,
                    "orderable": false,
                    "render": function (data, type, full, meta) {
                        var data = '';
<?php if ($this->ion_auth->is_admin() || $permissions->edit_per): ?>
    data = '<a class="btn btn-primary btn-sm" href="<?= base_url('lessons/add/') ?>' + full['id'] + '"><i class="ti ti-pencil"></i></a>&nbsp;';
    <?php
endif;
if ($this->ion_auth->is_admin() || $permissions->delete_per):
    ?>
    data += '<a href="<?= base_url('lessons/delete/') ?>' + full['id'] + '" class="btn btn-danger btn-sm delete_item"><i class="ti ti-trash"></i></a>&nbsp;';
                            //                         <?php
endif;
if ($this->ion_auth->is_admin() || $permissions->view_per):
    ?>
       data += '<a class="btn btn-midnightblue-alt btn-sm" href="#' + full['id'] + '"><i class="ti ti-eye"></i></a>';
<?php endif; ?>
                        return data;
                    }
                },
                {
                    "targets": [2],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                            if (full['status'] == '1') {
                                data = '<a href="<?php bs('lessons/Lessons/update_status/') ?>' + full['id'] + '/deactivate" data-toggle="tooltip" data-placement="top" title="Click to Change Status" class="text-success f18 change_status"><i class="flaticon-checked"></i></a>';
                            } else {
                                data = '<a href="<?php bs('lessons/Lessons/update_status/') ?>' + full['id'] + '/activate" data-toggle="tooltip" data-placement="top" title="Click to Change Status" class="text-danger f18 change_status"><i class="flaticon-close"></i></a>';
                            }
                        }
                        return data;
                    }
                },
                {
                    "targets": [5],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                           data = '<span id="' + full['id'] +'_<?php echo META_LESSON; ?>_<?php echo META_LESSON; ?>_lesson_cat" data-type="lesson_cats" class="load_meta_category"> Loading...</span>';
                        }
                        return data;
                    }
                },{
                    "targets": [6],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                           data = '<span id="' + full['id'] +'_<?php echo META_LESSON; ?>_<?php echo META_LESSON; ?>_lesson_tag" data-type="lesson_tags" class="load_meta_data"> Loading...</span>';
                        }
                        return data;
                    }
                },
                {
                    "targets": [7],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                           data = '<span id="' + full['id'] +'_<?php echo META_LESSON ; ?>_<?php echo META_SYSTEM; ?>_system_cat" data-type="system_cats" class="load_meta_category"> Loading...</span>';
                        }
                        return data;
                    }
                },
                {
                    "targets": [8],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                           data = '<span id="' + full['id'] +'_<?php echo META_LESSON ; ?>_<?php echo META_SYSTEM; ?>_system_tag" data-type="system_tags" class="load_meta_data"> Loading...</span>';
                        }
                        return data;
                    }
                },
                {
                    "targets": [11],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                            data = '<span id="' + full['id'] + '_users"  class="load_module_users"> Loading...</span>';
                        }
                        return data;
                    }
                },
                {
                    "targets": [12],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                            data = '<span id="' + full['id'] + '_course"  class="load_course_data"> Loading...</span>';
                        }
                        return data;
                    }
                },
                {
                    "targets": [13],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                            data = '<span id="' + full['id'] + '_module"  class="load_module_data"> Loading...</span>';
                        }
                        return data;
                       
                        //return "NA";
                    }
                }
            ],
            "columns": [{
                    "data": "id"
                },
                {
                    "data": null,

                },{
                    "data": null,
                },
                {
                   "data": "name",
                },
                {
                    "data": "long_title",
                },
                {
                    "data": "categories",
                },
                {
                    "data": "tags",
                },
                {
                    "data": "system_categories",
                },
                {
                    "data": "system_tags",
                },               
                {
                    "data": "template_type",
                },
                {
                    "data": "created",
                },
                {
                    "data": "created_by",
                },                
                {
                    "data": "id",
                },

                {
                    "data": "id",
                },
            ]
        });
        $('.filter-container').on('change','.meta_data_filter', function(){
            table.search('').draw();
        });

        $(document).on('click', '.change_status', function (e) {
            e.preventDefault();
            var scope = $(this);
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure?',
                buttons: {
                    confirm: function () {
                        $.get(scope.attr("href"), // url
                    function (data, textStatus, jqXHR) { // success callback
                        var obj = JSON.parse(data);
                        if (obj.msg === 'Updated') {
                            table.ajax.reload(); //just reload table
                        }
                    });
                        return true;
                    },
                    cancel: function () {
                        return true;
                    }
                }
            });
         });
        // To delete record
        $(document).on('click', '.delete_item', function (e) {
            e.preventDefault();
            var scope = $(this);
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure?',
                buttons: {
                    confirm: function () {
                        $.get(scope.attr("href"), // url
                                function (data, textStatus, jqXHR) { // success callback
                                    var obj = JSON.parse(data);
                                    if (obj.msg === 'deleted') {
                                        table.ajax.reload(); //just reload table
                                    }
                                });
                        return true;
                    },
                    cancel: function () {
                        return true;
                    }
                }
            });
        });
    });
    $(document).ready(function () {
        $("#lessonListTable_paginate .pagination").click(function () {
            $("html, body").animate({
                scrollTop: 0
            }, "slow");
        });
    });
</script>