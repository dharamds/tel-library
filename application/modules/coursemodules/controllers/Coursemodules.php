<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Coursemodules extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        ini_set("display_errors", 0);
        //echo "sss"; exit;
        $this->load->module('template');
        $this->load->model(['common_model', 'Modules_modal']);
        $this->load->helper(array('html', 'form'));
        $this->load->library('form_validation');

        if (!$this->ion_auth->logged_in()) :
            redirect('users/auth', 'refresh');
        endif;
        // get controller permissions
        if ($this->current_user_permissions = $this->get_permissions()) {
            $this->add_permission = $this->current_user_permissions->add_per;
            $this->edit_permission = $this->current_user_permissions->edit_per;
            $this->delete_permission = $this->current_user_permissions->delete_per;
            $this->view_permission = $this->current_user_permissions->view_per;
            $this->list_permission = $this->current_user_permissions->list_per;
        }
    }

    /**
     * index method
     * @description this function use to display list of module
     * @return void
     */
    public function index()
    {

        // check permissions
        if (!$this->list_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect('users/auth', 'refresh');
        endif;
        $data['permissions'] = $this->current_user_permissions;

        $data['breadcrumb'][] = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][] = ['title' => 'Modules', 'link' => '', 'class' => 'active'];
        $data['page'] = "coursemodules/list";
        $this->template->template_view($data);
    }

    /**
     * getLists method 
     * @description this function called via ajax request, use to display list of lesson
     * @return void
     */
    public function getLists()
    {
        $modulesData = $this->Modules_modal->getRows($_POST);
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $modulesData['total'],
            "recordsFiltered" => $modulesData['total'],
            "data" => $modulesData['result'],
        );
        // Output to JSON format
        echo json_encode($output);
    }

    /**
     * add method
     * @description this function use to create a new module
     * @return void
     */
    public function add($id = NULL)
    {
        //ini_set('display_errors', '2');
        // check method permission
        if ($id > 0) :
            if ($id && !$this->edit_permission && !$this->ion_auth->is_admin()) :
                $this->session->set_flashdata('error', $this->lang->line('access_denied'));
                redirect($_SERVER['HTTP_REFERER'], 'refresh');
            endif;
        else :
            if (!$this->add_permission && !$this->ion_auth->is_admin()) :
                $this->session->set_flashdata('error', $this->lang->line('access_denied'));
                redirect($_SERVER['HTTP_REFERER'], 'refresh');
            endif;
        endif;
        $data['breadcrumb'][] = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][] = ['title' => 'Modules', 'link' => base_url('coursemodules'), 'class' => ''];

        if ($id != null) {
            $data['modulesData'] = $this->common_model->getDataById('modules', '*', ['id' => $id]);
            $data['documentData'] = $this->common_model->getAllData('documents', '*', '', ['reference_id' => $id, 'reference_type' => 2, 'delete_status' => 1]);
            $data['breadcrumb'][] = ['title' => 'Edit', 'link' => '', 'class' => 'active'];
            //pr($data['modulesData']);exit;
        } else {
            $data['breadcrumb'][] = ['title' => 'Add', 'link' => '', 'class' => 'active'];
        }
        $data['page'] = "coursemodules/add";
        $this->template->template_view($data);
    }

    /**
     * view_coursemodule method
     * @description this function use to view complete course module details
     * @return void
     */
    public function detail()
    {
        // check permissions
        if (!$this->view_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect('users/auth', 'refresh');
        endif;
        $module_id = $this->uri->segment(3);
        if (isset($module_id)) {
            $data['modulesData'] = $this->common_model->getDataById('modules', '*', ['id' => $module_id]);

            $data['documentsData'] = $this->common_model->getAllData('documents', '*', '', ['reference_id' => $module_id, 'reference_type' => 2]);
            $data['breadcrumb'][] = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
            $data['breadcrumb'][] = ['title' => 'Modules', 'link' => base_url('courses'), 'class' => ''];
            $data['breadcrumb'][] = ['title' => 'View', 'link' => '', 'class' => 'active'];
            $data['page'] = "coursemodules/detail";
            $this->template->template_view($data);
        }
    }

    // Delete module
    function delete()
    {
        // check permissions
        if (!$this->delete_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect('users/auth', 'refresh');
        endif;
        $data['permissions'] = $this->current_user_permissions;

        $id = $this->uri->segment(3);
        $data = ['delete_status' => 0];
        if ($id) {

            # clear cache when course module deleted
            $module_slug = $this->common_model->getAllData('modules',  'name', true,['id' => $id]);
            clean_cache_by_key('api',md5(slugify($module_slug->name)));

            $this->common_model->UpdateDB('modules', ['id' => $id], $data);
            $this->common_model->UpdateDB('documents', ['reference_id' => $id, 'reference_type' => 2,], $data);
            $this->session->set_flashdata('success', $this->ion_auth->messages());
            $msg = "deleted";
            echo json_encode(array("msg" => $msg));
            exit;
        } else {
            set_flashdata('error', 'Unable to delete Question, try again');
            redirect('questions/', 'refresh');
        }
    }

    public function update_status($id, $action)
    {
        # clear cache when course module created or updated
        $module_slug = $this->common_model->getAllData('modules', 'name', true,['id' => $id]);
        clean_cache_by_key('api',md5(slugify($module_slug->title)));

        $status = ($action == 'activate') ? 1 : 0;
        $data = array('status' => $status);
        $this->Modules_modal->update_status($id, $data);
        clean_cache('api', 'tel_moduleDetailsBySlugs_');
        $msg = "Updated";
        echo json_encode(['msg' => $msg]);
        exit;
    }

    /**
     * Manage upload Image resize
     *
     * @return Response
     */
    public function resizeImage($filename)
    {
        $source_path = './uploads/modules_images/' . $filename;
        $target_path = './uploads/modules_images/thumbnails/';
        $config_manip = [
            'image_library' => 'gd2',
            'source_image' => $source_path,
            'new_image' => $target_path,
            'maintain_ratio' => TRUE,
            'create_thumb' => TRUE,
            //'thumb_marker' => '_thumb',
            'width' => 150,
            'height' => 150
        ];

        $this->load->library('image_lib', $config_manip);
        if (!$this->image_lib->resize()) {
            echo $this->image_lib->display_errors();
        }
        $this->image_lib->clear();
    }

    # Ajax calls
    # Add module content

    public function addModulesContent()
    {
        if ((int) $this->input->post('mod_meta_id')) {
            
            if ($this->input->post('module_expiry') != "") {
                $module_expiry = $this->common_model->getDateMdYToYmd($this->input->post('module_expiry'));
            } else
                $module_expiry = NULL;

            $mid = $this->input->post('mod_meta_id');
            $data = [
                'module_availability' => $this->input->post('module_availability'),
                'show_glossary' => $this->input->post('show_glossary'),
                'exp_status' => $this->input->post('exp_status'),
                'module_expiry' => $module_expiry,
                'module_img' => post('img_name'),
            ];
            $this->common_model->UpdateDB('modules', ['id' => $mid], $data);

            $cache_key = 'tel_get_module_detail_by_slug_';
            clean_cache('api', $cache_key);

            $msg['success'] = "Module has been updated successfully.";
            $msg['code'] = 200;
            $msg['id'] = $mid;
            
        } else {

            $this->form_validation->set_rules('title', 'Title', 'trim|required');
            $this->form_validation->set_rules('getting_started', 'Getting started', 'trim|required');
            $this->form_validation->set_rules('introduction', 'Introduction', 'trim|required');
            $this->form_validation->set_rules('outcomes', 'Outcomes', 'trim|required');
            
           
            # clear cache when course module created or updated
            clean_cache_by_key('api',md5(slugify($this->input->post('title'))));

            if ($this->form_validation->run() == FALSE) {
                $msg['error'] = validation_errors();
                $msg['code'] = 400;
                //echo validation_errors();
            } else {
                $data = [
                    'created_by' => $this->session->userdata('user_id'),
                    'name' => $this->input->post('title'),
                    'slug' => slugify($this->input->post('title')),
                    'long_title' => $this->input->post('long_title'),
                    'getting_started' => $this->input->post('getting_started'),
                    'introduction' => $this->input->post('introduction'),
                    'outcomes' => $this->input->post('outcomes')
                ];

                if ($this->input->post('mod_cont_id') > 0) {
                    $this->common_model->UpdateDB('modules', ['id' => $this->input->post('mod_cont_id')], $data);
                    
                   // clean_cache('api', 'tel_moduleDetailsBySlugs_');
                    $cache_key = 'tel_get_module_detail_by_slug_';
                    clean_cache('api', $cache_key);

                    $lastID = $this->input->post('mod_cont_id');
                    $msg['success'] = "Module has been updated successfully.";
                } else {
                    $data['slug'] = slugify($this->input->post('title'));
                    $this->common_model->InsertData('modules', $data);
                    $lastID = $this->db->insert_id();
                    $msg['success'] = "Module has been inserted successfully.";
                }

                if ($this->input->post('documents')) {
                    if ($this->input->post('mod_cont_id') > 0) {
                    $this->common_model->DeleteDB('documents', array("reference_id" => $lastID, "reference_type" => 2));
                    }
                    foreach ($this->input->post('documents') as $docData) {
                        $exp = explode('|', $docData);
                        $docDataarr['reference_id'] = $lastID;
                        $docDataarr['reference_type'] = 2; // 2=Modules                
                        $docDataarr['title'] = $exp[1];
                        $docDataarr['filename'] = $exp[0];
                        $this->common_model->InsertData('documents', $docDataarr);
                    }
                }
                $msg['code'] = 200;
                $msg['id'] = $lastID;
            }
        }
        echo json_encode($msg);
        exit;       

    }

    # Remove module docuemnt from server

    public function remove_module_doc()
    {

        if ((int) $this->input->post('fileID')) {
            $data = ['delete_status' => 0];
            $id = $this->input->post('fileID');
            $this->common_model->UpdateDB('documents', ['id' => $id, 'reference_type' => 2], $data);
        }
        return true;
    }

    # Remove module image from server

    public function remove_module_img()
    {

        $module_id = $this->input->post('module_id');
        $postData = array('module_img ' => '');
        $this->common_model->UpdateDB('modules', array("id" => $module_id), $postData);

        $fileName = $_POST['file'];
        if (unlink(APPPATH . '../uploads/modules_images/' . $fileName)) {
            unlink(APPPATH . '../uploads/modules_images/thumbnails/' . $fileName);
            echo 1;
        } else
            echo 0;
    }

    # Upload images and documents for modules

    public function upload_docs_image()
    {
        $data = [];
        $file_element_name = 'file';
        if ($_REQUEST['elementType'] == 'modules_doc') {
            $config['upload_path'] = './uploads/modules_docs/';
            $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|ppt|pptx|odt';

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload($file_element_name)) {
                $status = 'error';
                $data['error'] = $this->upload->display_errors('', '');
            } else {
                $fileData = $this->upload->data();
                $data['fileName'] = $fileData['file_name'];
                $data['element'] = $_REQUEST['elementId'];
                $data['fileTitle'] = $_REQUEST['fileTitle'];
            }
        } else {
            $config['upload_path'] = './uploads/modules_images/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload($file_element_name)) {
                $status = 'error';
                $data['error'] = $this->upload->display_errors('', '');
            } else {
                $fileData = $this->upload->data();
                $this->resizeImage($fileData['file_name']);
                $data['filePath'] = base_url() . 'uploads/modules_images/thumbnails/';
                $data['fileName'] = $fileData['file_name'];
                $img_data = ['module_img' => $fileData['file_name']];
                $mid = $this->input->post('mod_up_img_meta_id');
                $this->common_model->UpdateDB('modules', ['id' => $mid], $img_data);
            }
        }
        echo json_encode($data);
        exit;
    }

    /**
     * getLists method 
     * @description this function called via ajax request, use to display list of lesson
     * @return void
     */
    public function setAllSelected()
    {
        $lessonIds = array_filter(explode("|", $this->input->post('selectedLesson')));
        $data = [];
        $status = $this->input->post('status');
        foreach ($lessonIds as $key => $val) {
            $data['status'] = $status;
            $this->Modules_modal->update_status($val, $data);
        }
        $msg = 'Updated';
        echo json_encode(['msg' => $msg]);
        exit;
    }

    /**
     * getCheckExist method
     * @description this method is use to check name is already exist or not
     * @param string
     * @return json array
     */
    public function getCheckExist()
    {
        //pr($_POST,"k");
        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        if ($this->form_validation->run() == FALSE) 
        {
            $this->setup();
        } else {
            $this->load->model('Modules_modal');
            $conditions = [
                'name' => $this->input->post('title'),
                'delete_status' => 1
            ];
            if(post('recordID') != 0 && post('recordID') != '')
            {
                $conditions['id !='] = post('recordID');
            }

            if ($this->Modules_modal->getCount($conditions,'modules') == 0) 
            {
                echo 'true';
                exit;
            } else {
                echo 'false';
                exit;
            }
        }
        exit;
    }

    public function get_course_used($lessonID)
    {
        $data['typeid'] = $lessonID;
        list($lessonID, $type) = explode("_", $lessonID);
        $this->load->model('Modules_modal');
        $data['value'] = $this->Modules_modal->get_module_used($lessonID);
        echo json_encode($data); 
        exit;
    }  

}
