<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
Author Salman Iqbal
Company Parexons
Date 26/1/2017
*/

class Modules_modal extends CI_Model 
{
        
    function __construct() {
        // Set table name
        $this->table = 'modules';
        // Set orderable column fields
        $this->column_order = array(null, null, $this->table.'.name', $this->table.'.long_title', 'categories', 'tags','system_categories', 'system_tags', null, $this->table.'.created', 'created_by');
        // Set searchable column fields
        $this->column_search = array($this->table.'.name', $this->table.'.long_title');
        // Set default order
        $this->order = array( $this->table.'.id' => 'desc');      
           
    }


    /*
     * Fetch members data from the database
     * @param $_POST filter data based on the posted parameters
     */
    public function getRows($postData) {      
        //pr($postData);
        $this->_get_datatables_query($postData);          

        if($postData['length'] != -1){
            $this->db->limit($postData['length'], $postData['start']);
        }
        $query = $this->db->get();
        $data['result'] = $query->result_array();
        //pr($this->db->last_query()); exit;
        $data['total'] = $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total;
        //pr($data);
        return $data;
    }
    
    //update status
     public function update_status($id = null, $data)
    {
        $this->db->where('id', $id);
        $delete = $this->db->update($this->table, $data);
        if ($delete):
            return true;
        endif;
    }

   /*
     * Perform the SQL queries needed for an server-side processing requested
     * @param $_POST filter data based on the posted parameters
     */
    private function _get_datatables_query($postData){
        $this->db->select('SQL_CALC_FOUND_ROWS '.$this->table.'.id, '.$this->table.'.name, '.$this->table.'.long_title, '.$this->table.'.status', false); 
        $this->db->select('DATE_FORMAT('.$this->table.'.created, "%m/%d/%Y") as created');
        if ($postData['order']['0']['column'] == 10) 
        {
            $this->db->select('CONCAT_WS(" ", users.first_name, users.middle_name, users.last_name) as created_by');
        }else{
            $this->db->select('"..." as created_by');
        }
        // modules metadata
        if ((isset($postData['module_cat']) && count($postData['module_cat']) > 0) || $postData['order']['0']['column'] == 5) {
            $this->db->select('GROUP_CONCAT( DISTINCT categories.name SEPARATOR \'<br> \')  as categories');
        }else{
            $this->db->select('"..." as categories');
        }  
        if ((isset($postData['module_tag']) && count($postData['module_tag']) > 0) || $postData['order']['0']['column'] == 6) {
            $this->db->select('GROUP_CONCAT( DISTINCT tags.name SEPARATOR \'<br> \')  as tags');
        }else{
            $this->db->select('"..." as tags');
        }  
        // system metadata
        if ((isset($postData['system_cat']) && count($postData['system_cat']) > 0) || $postData['order']['0']['column'] == 7) {
            $this->db->select('GROUP_CONCAT( DISTINCT system_category.name SEPARATOR \'<br> \')  as system_categories');
        }else{
            $this->db->select('"..." as system_categories');
        }  

        if ((isset($postData['system_tag']) && count($postData['system_tag']) > 0) || $postData['order']['0']['column'] == 8) {
            $this->db->select('GROUP_CONCAT( DISTINCT system_tags.name SEPARATOR \'<br> \')  as system_tags');
        }else{
            $this->db->select('"..." as system_tags');
        }  
        

        $this->db->from($this->table);
        if ($postData['order']['0']['column'] == 10) 
        {
        $this->db->join('users', $this->table . '.created_by = users.id');
        }  
        if ((isset($postData['module_cat']) && count($postData['module_cat']) > 0) || $postData['order']['0']['column'] == 5) {
            $this->db->join('category_assigned', 'category_assigned.reference_id = '.$this->table.'.id AND category_assigned.reference_type = '.META_MODULE, 'left');
            $this->db->join('categories', 'categories.id = category_assigned.category_id AND categories.delete_status = 1 AND categories.status = 1', 'left');
        }
        
        if ((isset($postData['module_tag']) && count($postData['module_tag']) > 0) || $postData['order']['0']['column'] == 6) {
            $this->db->join('tag_assigned', 'tag_assigned.reference_id = '.$this->table.'.id AND tag_assigned.reference_type = '.META_MODULE, 'left');
            $this->db->join('tags', 'tags.id = tag_assigned.tag_id  AND tags.delete_status = 1 AND tags.status = 1', 'left');
        }
        
        if ((isset($postData['system_cat']) && count($postData['system_cat']) > 0) || $postData['order']['0']['column'] == 7) {
            $this->db->join('category_assigned as assigned_system_category', 'assigned_system_category.reference_id = modules.id AND assigned_system_category.reference_type = '.META_SYSTEM.' AND  assigned_system_category.reference_sub_type ='.META_MODULE, 'left');
            $this->db->join('categories as system_category', 'system_category.id = assigned_system_category.category_id AND system_category.delete_status = 1 AND system_category.status = 1', 'left');
        }
        
        if ((isset($postData['system_tag']) && count($postData['system_tag']) > 0) || $postData['order']['0']['column'] == 8) {
            $this->db->join('tag_assigned as assigned_system_tags', 'assigned_system_tags.reference_id = modules.id AND assigned_system_tags.reference_type = '.META_SYSTEM.' AND  assigned_system_tags.reference_sub_type ='.META_MODULE, 'left');
            $this->db->join('tags as system_tags', 'system_tags.id = assigned_system_tags.tag_id AND system_tags.delete_status = 1 AND system_tags.status = 1', 'left');
        }
        
        $this->db->where($this->table.'.delete_status = 1');
       
        $i = 0;
        // loop searchable columns 
        foreach($this->column_search as $item){
            // if datatable send POST for search
            if($postData['search']['value']){
                // first loop
                if($i === 0){
				    // open bracket 
					$this->db->group_start();
					//pr( $item);	
                    $this->db->like($item, $postData['search']['value'], 'both');
                }else{
                    $this->db->or_like($item, $postData['search']['value'], 'both');
                }
                    // last loop
                if(count($this->column_search) - 1 == $i){
                    // close bracket
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if(isset($postData['status']) && $postData['status'] < 2){
            $this->db->where($this->table.'.status', $postData['status']);
        }   
        //pr($postData['system_cat'],"j");     
        if($postData['system_cat']) {
            $this->db->where_in('assigned_system_category.category_id', $postData['system_cat']);
        }
        if($postData['system_tag']) {
            $this->db->where_in('assigned_system_tags.tag_id', $postData['system_tag']);
        }
        if($postData['module_cat']) {
            $this->db->where_in('category_assigned.category_id', $postData['module_cat']);
        }
        if($postData['module_tag']) {
            $this->db->where_in('tag_assigned.tag_id', $postData['module_tag']);
        }
        $this->db->group_by($this->table.'.id');

        if(isset($postData['order'])){
            $this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        }else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    
    /*
    *getCheckExist this method is used to check data is already exist or not
    * @param string
    * @return string 
    */
    public function getCount($condition=[],$table)
    {
        $this->db->where($condition);
        if($query = $this->db->get($table))
        {   
            return $query->num_rows();
        }
    }

    public function get_module_used($moduleID = null)
    {
        $sql = "SELECT COALESCE(GROUP_CONCAT(DISTINCT courses.name ORDER BY courses.name ASC SEPARATOR '<br>'), 'NA')  as course_name from courses 
        JOIN course_contain ON courses.id = course_contain.course_id
        WHERE course_contain.module_id = " . $moduleID . ' group by course_contain.module_id';

        //return $sql;
        $query =  $this->db->query($sql);
        return ($query->row()->course_name) ? $query->row()->course_name : 'NA';
    }

}