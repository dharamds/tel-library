         <div class="container-fluid">
            <div class="tab-pane active" id="tab-about">
               <div class="panel panel-default">
                  <div class="panel-heading">
                     <h2>Details</h2>
                  </div>
                  <div class="panel-body">
                     <div class="about-area">
                        <div class="table-responsive">
                           <table class="table about-table">
                              <tbody>
                                 <tr>
                                    <th>Title</th>
                                    <td width="5%"></td>
                                    <td><?php echo $modulesData->name;?></td>
                                 </tr>
                                 <tr>
                                    <th>Image</th>
                                    <td width="5%"></td>
                                    <td> <img src="<?php bs('uploads/modules_images/')?><?php echo $modulesData->image;?>" alt="" width="120" /> </td>
                                 </tr>
                                 <tr>
                                    <th>Introduction</th>
                                    <td width="5%"></td>
                                    <td><?php echo $modulesData->introduction;?></td>
                                 </tr>
                                 <tr>
                                    <th>Instruction</th>
                                    <td width="5%"></td>
                                    <td><?php echo $modulesData->instruction;?></td>
                                 </tr>
                                 <tr>
                                    <th>Learning outcome</th>
                                    <td width="5%"></td>
                                    <td><?php echo $modulesData->learning_outcome;?></td>
                                 </tr>
                                 <tr>
                                    <th>Learning activities</th>
                                    <td width="5%"></td>
                                    <td><?php echo $modulesData->learning_activities;?></td>
                                 </tr>
                                 <tr>
                                    <th>Documents</th>
                                    <td width="5%"></td>
                                    <td> 
                                       <?php  //pr($documents_data);
                                       $doc_type = ['1' => 'Syllabus', '2' => 'Assignments'];
                                       foreach ($documentsData as $key => $doc) {
                                          ?>
                                          <span><?php echo $doc_type[$doc->document_type]; ?>: &nbsp;</span>
                                          <a href="<?php bs('uploads/modules_docs/')?><?php echo $doc->file_path;?>" target="_new" title='Download'><span><?php echo $doc->name;?></span>
                                          </a><br>
                                          <?php                                         
                                       }
                                       ?>
                                    </td>
                                 </tr>
                                 <tr>
                                    <th>Created by</th>
                                    <td width="5%"></td>
                                    <td><?php 
                                       $authorData = $this->ion_auth->user($modulesData->created_by)->row(); 
                                       echo ucfirst($authorData->first_name); echo " ";  
                                       echo ucfirst($authorData->last_name);
                                       ?></td>
                                 </tr>
                                 <tr>
                                    <th>Created on</th>
                                    <td width="5%"></td>
                                    <td><?php echo date('m-d-Y',strtotime($modulesData->created)); ?></td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
