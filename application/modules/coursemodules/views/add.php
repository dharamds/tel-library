<link rel="stylesheet" href="<?php bs('public/assets/plugins/multiselect/sumoselect.min.css') ?>" />
<?php $module_id = isset($modulesData->id) ? $modulesData->id : NULL; ?>

<div class="container-fluid pr-0">
    <div class="panel panel-default">
        <div class="panel-heading">
            <ul class="nav nav-tabs" role="tablist" id="addLessonTab">
                <li role="presentation" class="active">
                    <a href="#content" class="set_tab" role="tab" data-toggle="tab" data-id="<?php echo $module_id ?>">
                        Content
                    </a>
                </li>
                <li role="presentation">
                    <a href="#metadata" class="set_tab" id="metadata_id" role="tab" data-toggle="tab" data-id="<?php echo $module_id ?>">
                        Metadata
                    </a>
                </li>
            </ul>
        </div>
        <div class="panel-body">
            <div class="tab-content">

                <!-- Content -->
                <div role="tabpanel" class="tab-pane active" id="content">
                    <?php
                    echo form_open('modules/add/', [
                        'name' => 'modulesContent',
                        'id' => 'modulesContent',
                        'enctype' => 'multipart/form-data'
                    ]);
                    ?>

                    <?php
                    $hid_cont_id_data = [
                        'type' => 'hidden',
                        'id' => 'mod_cont_id',
                        'name' => 'mod_cont_id',
                        'value' => $module_id
                    ];
                    echo form_input($hid_cont_id_data);
                    ?>
                    <div class="row mt-4 mb-3">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="title" class="control-label"> Title <span class="field-required">*</span> </label>
                                <?php
                                $name = isset($modulesData->name) ? $modulesData->name : '';
                                echo form_input([
                                    'id' => 'title',
                                    'name' => 'title',
                                    'value' => $name,
                                    'class' => 'form-control'
                                ]);
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label for="long_title" class="control-label"> Long Title </label>
                                <?php
                                $long_title = isset($modulesData->long_title) ? $modulesData->long_title : '';
                                echo form_input([
                                    'id' => 'long_title',
                                    'name' => 'long_title',
                                    'value' => $long_title,
                                    'class' => 'form-control'
                                ]);
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4 mb-3">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label for="title" class="control-label">Getting Started <span class="field-required">*</span> </label>
                                <?php $getting_started = isset($modulesData->getting_started) ? $modulesData->getting_started : ''; ?>
                                <?php
                                echo form_textarea([
                                    'name' => 'getting_started',
                                    'id' => 'getting_started',
                                    'value' => $getting_started,
                                    'class' => 'form-control editor',
                                    'rows' => '5'
                                ]);
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-4 mb-3">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label for="title" class="control-label"> Introduction <span class="field-required">*</span></label>
                                <?php $introduction = isset($modulesData->introduction) ? $modulesData->introduction : ''; ?>
                                <?php
                                echo form_textarea([
                                    'name' => 'introduction',
                                    'id' => 'introduction',
                                    'value' => $introduction,
                                    'class' => 'form-control editor',
                                    'rows' => '5'
                                ]);
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-4 mb-3">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label for="title" class="control-label"> Outcomes <span class="field-required">*</span> </label>
                                <?php $outcomes = isset($modulesData->outcomes) ? $modulesData->outcomes : ''; ?>
                                <?php
                                echo form_textarea([
                                    'name' => 'outcomes',
                                    'id' => 'outcomes',
                                    'value' => $outcomes,
                                    'class' => 'form-control editor',
                                    'rows' => '5'
                                ]);
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="hr-line mt-4 mb-4 col-md-12 p-0"></div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group col-xs-12 p-0 mb-2">
                                <label class="control-label"> Documents </label>
                            </div>
                            <div class="form-group col-xs-12 p-0">
                                <div class="col-md-3 p-0">
                                    <?php
                                    echo form_input([
                                        'id' => 'docTitle',
                                        'name' => 'docTitle',
                                        'class' => 'form-control',
                                        'placeholder' => 'Enter document title'
                                    ]);
                                    ?>
                                    <div id='docValidFormat'></div>
                                </div>
                                <div class="col-md-2">
                                    <div class="fileinput fileinput-new col-xs-12 p-0" data-provides="fileinput">

                                        <span class="btn btn-primary btn-primary-default btn-file">
                                            <span class="fileinput-new">Choose File</span>
                                            <span class="fileinput-exists">Change</span>
                                            <!--                                                    <input id="uploadImage" name="file" type="file" class="blog_img1 visible1" accept=".mp3,.wav,.ogg">-->
                                            <input id="choose_document" name="file" class="blog_img1 visible1">
                                        </span>

                                        <?php $show1 = !empty($glossaryData->media_file) ? "display:inline-block" : "display:none"; ?>
                                        <a class="btn btn-danger fileinput-exists" style="<?php echo $show1 ?>" id="mediaAudio">Remove</a>
                                        <div class="error" id="document_error">Please enter title</div>
                                    </div>

                                </div>

                            </div>

                            <div class="form-group col-xs-12 p-0 mt-3">
                                <?php if (isset($module_id)) { ?>
                                <label class="control-label">Added Documents</label>
                                <div class="col-12 mr-5 p-0">
                                    <div class="list-group list-file-row document_show">
                                        <?php foreach ($documentData as $key => $document) : ?>
                                        <?php
                                                $ext = pathinfo($document->filename, PATHINFO_EXTENSION);
                                                //pr("Extension:".$ext);
                                                $data1 = '';
                                                switch ($ext) {
                                                    case 'jpg':
                                                    case 'jpeg':
                                                    case 'png':
                                                    case 'gif':
                                                        $data1 .= "<div class='col-md-3 ml-4 p-0 mb-3' style='width: 190px;' data-toggle='tooltip' data-placement='top' title='" . $document->title . "'><input type='hidden' class='documents_name' name='documents[]' value=" . $document->filename . "|" . $document->title . "> <img src='" . base_url($document->filename) . "' style='object-fit: cover;height: 120px;min-width: 170px;width: 100%;' class='img-rounded img-responsive'> <a class='btn btn-delete delete_doc'  data-docname='" . $document->filename . "' data-did='" . $document->id . "' ><i class='flaticon-rubbish-bin-delete-button'></i></a></div>";
                                                        break;
                                                    case "pdf":
                                                        $data1 .= "<div class='col-md-3 ml-4 p-0 mb-3 doc-panel-pdf' style='width: 190px;' data-toggle='tooltip' data-placement='top' title='" . $document->title . "'><input type='hidden' class='documents_name' name='documents[]' value=" . $document->filename . "|" . $document->title . "><div class='flaticon-pdf f45 text-center pt-4 mt-3' ></div> <a class='btn btn-delete delete_doc'  data-docname='" . $document->filename . "' data-did='" . $document->id . "'><i class='flaticon-rubbish-bin-delete-button'></i></a></div>";
                                                        break;
                                                    case "csv":
                                                        $data1 .= "<div class='col-md-3 ml-4 p-0 mb-3 doc-panel-csv' style='width: 190px;' data-toggle='tooltip' data-placement='top' title='" . $document->title . "'><input type='hidden' class='documents_name' name='documents[]' value=" . $document->filename . "|" . $document->title . "><div class='flaticon-csv f45 text-center pt-4 mt-3' ></div> <a class='btn btn-delete delete_doc' data-docname='" . $document->filename . "' data-did='" . $document->id . "'><i class='flaticon-rubbish-bin-delete-button'></i></a></div>";
                                                        break;
                                                    case 'mb':
                                                    case 'mp3':
                                                        $data1 .= "<div class='col-md-3 ml-4 p-0 mb-3 doc-panel-mp3' style='width: 190px;' data-toggle='tooltip' data-placement='top' title='" . $document->title . "'><input type='hidden' class='documents_name' name='documents[]' value=" . $document->filename . "|" . $document->title . "><div class='flaticon-mp3 f45 text-center pt-4 mt-3' ></div> <a class='btn btn-delete delete_doc' data-docname='" . $document->filename . "' data-did='" . $document->id . "'><i class='flaticon-rubbish-bin-delete-button'></i></a></div>";
                                                        break;
                                                    case 'doc':
                                                    case 'docx':
                                                        $data1 .= "<div class='col-md-3 ml-4 p-0 mb-3 doc-panel-doc' style='width: 190px;' data-toggle='tooltip' data-placement='top' title='" . $document->title . "'><input type='hidden' class='documents_name' name='documents[]' value=" . $document->filename . "|" . $document->title . "><div class='flaticon-doc f45 text-center pt-4 mt-3' ></div> <a class='btn btn-delete delete_doc' data-docname='" . $document->filename . "' data-did='" . $document->id . "'><i class='flaticon-rubbish-bin-delete-button'></i></a></div>";
                                                        break;
                                                    case 'xls':
                                                    case 'xlsx':
                                                        $data1 .= "<div class='col-md-3 ml-4 p-0 mb-3 doc-panel-xls' style='width: 190px;' data-toggle='tooltip' data-placement='top' title='" . $document->title . "'><input type='hidden' class='documents_name' name='documents[]' value=" . $document->filename . "|" . $document->title . "><div class='flaticon-xls f45 text-center pt-4 mt-3' ></div> <a class='btn btn-delete delete_doc' data-docname='" . $document->filename . "' data-did='" . $document->id . "'><i class='flaticon-rubbish-bin-delete-button'></i></a></div>";
                                                        break;
                                                    case 'mp4':
                                                        $data1 .= "<div class='col-md-3 ml-4 p-0 mb-3 doc-panel-xls' style='width: 190px;' data-toggle='tooltip' data-placement='top' title='" . $document->title . "'><input type='hidden' class='documents_name' name='documents[]' value=" . $document->filename . "|" . $document->title . "><div class='f45 text-center pt-4 mt-3 media-thumb-img' ><img src='" . base_url('/public/assets/img/mp4.png') . "'></img></div> <a class='btn btn-delete delete_doc' data-docname='" . $document->filename . "' data-did='" . $document->id . "'><i class='flaticon-rubbish-bin-delete-button'></i></a></div>";
                                                        break;
                                                    default:
                                                        $data1 .= '<div class="col-md-3 ml-4 p-0 mb-3" style="width: 190px;" data-toggle="tooltip" data-placement="top" title="' . $document->title . '"> <input type="hidden" class="documents_name" name="documents[]" value=' . $document->filename . '|' . $document->title . '><img src="' . base_url($document->filename) . '" style="object-fit: cover;height: 120px;min-width: 170px;width: 100%;" class="img-rounded img-responsive"> <a class="btn btn-delete delete_doc" data-docname="' . $document->filename . '" data-did="' . $document->id . '"  ><i class="flaticon-rubbish-bin-delete-button"></i></a></div>';
                                                }
                                                ?>
                                        <?php echo $data1 ?>
                                        <label class="control-label"><?php  ?></label>


                                        <?php endforeach ?>
                                        <div id="dFiles"></div>
                                    </div>

                                    <img id="docLoader" src="<?php echo base_url(); ?>/public/assets/img/spinner.gif" style="display:none;" />

                                </div>
                                <?php } else { ?>

                                <label class="control-label"> Added Documents </label>
                                <div class="list-group list-file-row mb-3 document_show">
                                    <div id="dFiles"></div>
                                    <img id="docLoader" src="<?php echo base_url(); ?>/public/assets/img/spinner.gif" style="display:none;" />
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-xs-12 text-right p-0 mt-4">
                            <button type="submit" id="nextContent" class="btn btn-primary"><?php echo isset($module_id) ? 'Save changes' : 'Save and next' ?></button>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
                <!-- Metadata -->
                <div role="tabpanel" class="tab-pane" id="metadata">
                    <?php
                    echo form_open('', [
                        'name' => 'module_img_up',
                        'id' => 'module_img_up',
                        'enctype' => 'multipart/form-data'
                    ]);
                    ?>
                    <?php
                    $hide_meta_id_data = [
                        'type' => 'hidden',
                        'id' => 'mod_up_img_meta_id',
                        'name' => 'mod_up_img_meta_id',
                        'value' => isset($module_id) ? $module_id : ''
                    ];
                    echo form_input($hide_meta_id_data);
                    ?>
                    <div class="istVisualContainer">
                        <div class="row">
                            <div class="row col-sm-10">
                                <div class="form-group col-xs-12 mt-4">
                                    <label class="control-label block"> Module Image </label>
                                </div>

                                <div class="col-sm-3 mr-5">
                                    <div class="ist-logo">
                                        <?php
                                        if (isset($modulesData->module_img) && (!empty($modulesData->module_img))) {
                                            ?>
                                        <img src="<?php echo base_url($modulesData->module_img); ?>" id="image_write_path" class="img-rounded">
                                        <?php } else { ?>
                                        <img src="<?php echo base_url(); ?>/public/assets/img/logo-bg.png" id="image_write_path" class="img-rounded">
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="ist-visual-info">
                                        <h4>Select Module Image</h4>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input type="hidden" name="old_img" value="">
                                                <div class="fileinput fileinput-new col-md-12 p-0" data-provides="fileinput">
                                                    <span class="btn btn-primary btn-primary-default btn-file" id="choose_media_image_file1">
                                                        <span class="fileinput-new">Choose File</span>
                                                        <span class="fileinput-exists">Choose File</span>
                                                    </span>
                                                    <a class="btn btn-danger fileinput-exists delete_img_main_page">Remove</a>
                                                </div>

                                                <div class="small"> (File must be .png, .jpg, .jpeg) </div>
                                            </div>
                                        </div>
                                        <div style="color:red;" id="upload_error"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                    <?php
                    echo form_open('coursemodules/add/', [
                        'name' => 'modulesMetadata',
                        'id' => 'modulesMetadata',
                        'enctype' => 'multipart/form-data'
                    ]);
                    ?>
                    <?php
                    $hid_meta_id_data = [
                        'type' => 'hidden',
                        'id' => 'mod_meta_id',
                        'name' => 'mod_meta_id',
                        'value' => isset($module_id) ? $module_id : ''
                    ];
                    echo form_input($hid_meta_id_data);
                    ?>
                    <input type="hidden" name="img_name" id="img_name" value="<?php echo $modulesData->module_img ?>">
                    <div class="row">
                        <div class="col-md-6 p-0">
                            <div class="form-group col-xs-12">
                                <label class="control-label block"> Module Availability </label>
                                <div class="row col-xs-12 m-0 p-0 mt-3">
                                    <div class="col-md-6 p-0">
                                        <label class="radio-tel m-0">
                                            <?php
                                            if (isset($module_id) && $modulesData->module_availability == 1) {
                                                $avail_status = 'checked';
                                            } elseif (isset($module_id) && $modulesData->module_availability != 1) {
                                                $avail_status = '';
                                            } else {
                                                $avail_status = 'checked';
                                            }
                                            $data_avai = [
                                                'name' => 'module_availability',
                                                'id' => 'module_avai',
                                                'value' => 1,
                                                'checked' => $avail_status,
                                                'class' => 'mr-3 t2'
                                            ];
                                            echo form_radio($data_avai);
                                            ?> Available
                                        </label>
                                    </div>
                                    <div class="col-md-6 p-0">
                                        <label class="radio-tel m-0">
                                            <?php
                                            if (isset($module_id) && $modulesData->module_availability == 0) {
                                                $hide_status = 'checked';
                                            } else {
                                                $hide_status = '';
                                            }
                                            ?>
                                            <?php
                                            $data_avai = [
                                                'name' => 'module_availability',
                                                'id' => 'module_hid',
                                                'value' => 0,
                                                'checked' => $hide_status,
                                                'class' => 'mr-3 t2'
                                            ];
                                            echo form_radio($data_avai);
                                            ?> Hidden
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 p-0 ">
                            <div class="form-group col-xs-12 mb-3">
                                <div>
                                    <label class="checkbox-tel m-0">
                                        <input type="checkbox" value="1" name="exp_status" id="module_expiry_check" <?php
                                                                                                                    if ($modulesData->exp_status == 1) {
                                                                                                                        echo "checked";
                                                                                                                    }
                                                                                                                    ?> class="mr-3 t2">Set Module Expiry
                                    </label>
                                </div>

                                <div class="form-group colorPicker pl-3 pt-4 m-0" id="module_expiry_div">
                                    <label class="col-md-5">Module Expiry Date</label>
                                    <div class="input-group col-md-5">
                                        <?php if (date("m/d/Y", strtotime($modulesData->module_expiry)) == '01/01/1970' || $modulesData->module_expiry == '0000-00-00') { ?>
                                        <input type="text" id="module_expiry" name="module_expiry" class="form-control" value="">
                                        <span class="input-group-addon">
                                            <span class="flaticon-calendar-1"></span>
                                        </span>
                                        <?php } else { ?>
                                        <input type="text" id="module_expiry" name="module_expiry" class="form-control" value="<?php echo date("m/d/Y", strtotime($modulesData->module_expiry)) ?>">
                                        <span class="input-group-addon">
                                            <span class="flaticon-calendar-1"></span>
                                        </span>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="hr-line mt-4 mb-4"></div>

                    <div class="col-md-12 p-0">
                        <div class="control-label block mb-4 f16">
                            Glossary Display on this Page
                        </div>
                        <div class="pl-4 col-md-12">
                            <div class="form-group">
                                <label class="control-label block"> Inline Pop-ups on this Page </label>
                                <div class="col-md-3 p-0">
                                    <label class="radio-tel m-0">
                                        <!-- <input type="radio" value="" checked>Available -->
                                        <?php
                                        if (isset($module_id) && $modulesData->show_glossary == 1) {
                                            $gloss_avail_status = 'checked';
                                        } elseif (isset($module_id) && $modulesData->show_glossary != 1) {
                                            $gloss_avail_status = '';
                                        } else {
                                            $gloss_avail_status = 'checked';
                                        }

                                        $data_avai = [
                                            'name' => 'show_glossary',
                                            'id' => 'available',
                                            'value' => 1,
                                            'checked' => $gloss_avail_status,
                                            'class' => 'mr-3 t2'
                                        ];
                                        echo form_radio($data_avai);
                                        ?> Available
                                    </label>
                                </div>
                                <div class="col-md-3 p-0">
                                    <label class="radio-tel m-0">
                                        <?php
                                        if (isset($module_id) && $modulesData->show_glossary == 0) {
                                            $gloss_hide_status = 'checked';
                                        } else {
                                            $gloss_hide_status = '';
                                        }
                                        $data_hidden = [
                                            'name' => 'show_glossary',
                                            'id' => 'hidden',
                                            'value' => 0,
                                            'checked' => $gloss_hide_status,
                                            'class' => 'mr-3 t2'
                                        ];
                                        echo form_radio($data_hidden);
                                        ?> Hidden
                                    </label>
                                </div>
                                <div class="col-md-3 p-0">
                                    <label class="radio-tel m-0">
                                        <?php
                                        if (isset($module_id) && $modulesData->show_glossary == 2) {
                                            $gloss_once_status = 'checked';
                                        } else {
                                            $gloss_once_status = '';
                                        }
                                        $data_instance = [
                                            'name' => 'show_glossary',
                                            'id' => 'instance',
                                            'value' => 2,
                                            'checked' => $gloss_once_status,
                                            'class' => 'mr-3 t2'
                                        ];
                                        echo form_radio($data_instance);
                                        ?> Only for first instance
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="hr-line col-md-12 mt-4 mb-5"></div>

                        <div class="col-xs-12 text-right">
                            <button type="submit" id="saveMetadata" class="btn btn-primary"><?php echo isset($module_id) ? 'Save changes' : 'Save and add' ?></button>
                        </div>
                        <div class="hr-line col-md-12 mt-5"></div>
                        <div class="pl-4 col-md-12">
                            <div class="col-md-6 pl-0 pt-5">
                                <div class="panel panel-grey">
                                    <div class="panel-heading mb-3">
                                        <h2><span>Module Categories</span></h2>
                                    </div>
                                    <div class="panel-body panel-collapse-body" id="module_categories_section">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 pl-0 pt-5">
                                <div class="panel panel-grey">
                                    <div class="panel-heading">
                                        <h2><span>Module Tags</span></h2>
                                        <div class="panel-ctrls button-icon-bg">
                                        </div>
                                    </div>
                                    <div class="panel-body" id="module_tags_sections">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="hr-line col-md-12"></div>

                        <div class="pl-4 col-md-12">
                            <div class="col-md-6 pl-0 pt-5">
                                <div class="panel panel-grey">
                                    <div class="panel-heading mb-3">
                                        <h2><span>System Categories</span></h2>
                                    </div>
                                    <div class="panel-body panel-collapse-body" id="system_categories_section">

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 pl-0 pt-5">
                                <div class="panel panel-grey">
                                    <div class="panel-heading">
                                        <h2><span>System Tags</span></h2>
                                        <div class="panel-ctrls button-icon-bg">
                                        </div>
                                    </div>
                                    <div class="panel-body" id="system_tags_sections">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade modals-tel-theme" id="gallery_images_popup" role="dialog">
        <div class="modal-dialog w80p">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Upload Image</h4>
                </div>
                <div class="modal-body p-0">
                    <div id="popup_img_div"></div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="<?= bs('public/assets/plugins/form-jasnyupload/fileinput.min.js') ?>"></script>

    <!-- #page-content -->

    <script type="text/javascript">
        function media_image_callback(image_name, img_full_url) {

            $('#document_error').hide();

            var extension = img_full_url.substr((img_full_url.lastIndexOf('.') + 1));
            switch (extension) {
                case 'jpg':
                case 'jpeg':
                case 'png':
                case 'gif':
                    $('.document_show').append('<div class="col-md-3 ml-4 p-0 mb-3" style="width: 190px;" data-toggle="tooltip" data-placement="top" title="' + $('#docTitle').val() + '"><input type="hidden" class="documents_name" name="documents[]" value="' + image_name + '|' + $('#docTitle').val() + '"> <img src=' + img_full_url + ' style="object-fit: cover;height: 120px;min-width: 170px;width: 100%;" class="img-rounded img-responsive"> <a class="btn btn-delete delete_data_img"><i class="flaticon-rubbish-bin-delete-button"></i></a></div>').ready(function() {
                        $('[data-toggle="tooltip"]').tooltip();
                        $('.delete_data_img').click(function() {
                            $(this).parent().remove();
                            $(document).find('.tooltip').hide()
                        });
                    });
                    break; // the alert ended with pdf instead of gif.
                case 'pdf':
                    $('.document_show').append("<div class='col-md-3 ml-4 p-0 mb-3 doc-panel-pdf' style='width: 190px;' data-toggle='tooltip' data-placement='top' title='" + $('#docTitle').val() + "'><input type='hidden' class='documents_name' name='documents[]' value='" + image_name + "|" + $('#docTitle').val() + "'><div class='flaticon-pdf f45 text-center pt-4 mt-3' ></div> <a class='btn btn-delete delete_data_img'><i class='flaticon-rubbish-bin-delete-button'></i></a></div>").ready(function() {
                        $('.delete_data_img').click(function() {
                            $('[data-toggle="tooltip"]').tooltip();
                            $(this).parent().remove();
                            $(document).find('.tooltip').hide()
                        });
                    });
                    break;
                case 'csv':
                    $('.document_show').append("<div class='col-md-3 ml-4 p-0 mb-3 doc-panel-csv' style='width: 190px;' data-toggle='tooltip' data-placement='top' title='" + $('#docTitle').val() + "'><input type='hidden' class='documents_name' name='documents[]' value='" + image_name + "|" + $('#docTitle').val() + "'><div class='flaticon-csv f45 text-center pt-4 mt-3' ></div> <a class='btn btn-delete delete_data_img'><i class='flaticon-rubbish-bin-delete-button'></i></a></div>").ready(function() {
                        $('.delete_data_img').click(function() {
                            $('[data-toggle="tooltip"]').tooltip();
                            $(this).parent().remove();
                            $(document).find('.tooltip').hide()
                        });
                    });
                    break;
                case 'mb':
                case 'mp3':
                    $('.document_show').append("<div class='col-md-3 ml-4 p-0 mb-3 doc-panel-mp3' style='width: 190px;' data-toggle='tooltip' data-placement='top' title='" + $('#docTitle').val() + "'><input type='hidden' class='documents_name' name='documents[]' value='" + image_name + "|" + $('#docTitle').val() + "'><div class='flaticon-mp3 f45 text-center pt-4 mt-3' ></div> <a class='btn btn-delete delete_data_img'><i class='flaticon-rubbish-bin-delete-button'></i></a></div>").ready(function() {
                        $('.delete_data_img').click(function() {
                            $('[data-toggle="tooltip"]').tooltip();
                            $(this).parent().remove();
                            $(document).find('.tooltip').hide()
                        });
                    });
                    break;
                case 'doc':
                case 'docx':
                    $('.document_show').append("<div class='col-md-3 ml-4 p-0 mb-3 doc-panel-doc' style='width: 190px;' data-toggle='tooltip' data-placement='top' title='" + $('#docTitle').val() + "'><input type='hidden' class='documents_name' name='documents[]' value='" + image_name + "|" + $('#docTitle').val() + "'><div class='flaticon-doc f45 text-center pt-4 mt-3' ></div> <a class='btn btn-delete delete_data_img'><i class='flaticon-rubbish-bin-delete-button'></i></a></div>").ready(function() {
                        $('.delete_data_img').click(function() {
                            $('[data-toggle="tooltip"]').tooltip();
                            $(this).parent().remove();
                            $(document).find('.tooltip').hide()
                        });
                    });
                    break;
                case 'xls':
                case 'xlsx':
                    $('.document_show').append("<div class='col-md-3 ml-4 p-0 mb-3 doc-panel-xls' style='width: 190px;' data-toggle='tooltip' data-placement='top' title='" + $('#docTitle').val() + "'><input type='hidden' class='documents_name' name='documents[]' value='" + image_name + "|" + $('#docTitle').val() + "'><div class='flaticon-xls f45 text-center pt-4 mt-3' ></div> <a class='btn btn-delete delete_data_img'><i class='flaticon-rubbish-bin-delete-button'></i></a></div>").ready(function() {
                        $('.delete_data_img').click(function() {
                            $('[data-toggle="tooltip"]').tooltip();
                            $(this).parent().remove();
                            $(document).find('.tooltip').hide()
                        });
                    });
                    break;
                case 'mp4':
                    $('.document_show').append("<div class='col-md-3 ml-4 p-0 mb-3 doc-panel-xls' style='width: 190px;' data-toggle='tooltip' data-placement='top' title='" + $('#docTitle').val() + "'><input type='hidden' class='documents_name' name='documents[]' value='" + image_name + "|" + $('#docTitle').val() + "'><div class='f45 text-center pt-4 mt-3 media-thumb-img'><img src='<?php bs('/public/assets/img/mp4.png') ?>'></img></div> <a class='btn btn-delete delete_data_img'><i class='flaticon-rubbish-bin-delete-button'></i></a></div>").ready(function() {
                        $('.delete_data_img').click(function() {
                            $('[data-toggle="tooltip"]').tooltip();
                            $(this).parent().remove();
                            $(document).find('.tooltip').hide()
                        });
                    });
                    break;
                default:
            }
            $('#docTitle').val('');
        }

        $("#choose_document").click(function() {
            if ($('#docTitle').val() == '') {
                $('#document_error').show();
            } else {
                CommanJS.choose_media_image_file('media_image_callback');
            }
        });

        function media_image_callback_metadata(image_name, img_full_url) {

            $('.ist-logo').html(CommanJS.getImagebox(img_full_url, 'thumbnail', 'f45 text-center mt-3 pt-5'));
            $('#img_name').val(image_name);
            $('#image_remove').show();
            $('.delete_img_main_page').css('display', 'inline-block');
            $('.delete_img_main_page').show();
            //alert(img_full_url)
        }

        $("#choose_media_image_file1").click(function() {
            CommanJS.choose_media_image_file('media_image_callback_metadata');
        });

        $(document).ready(function() {

            $('.delete_img_main_page').off('click').on('click', function() {

                var module_id = '<?php echo $module_id; ?>';
                var filename = $("#img_name").val();
                //var scope = $(this);

                $.confirm({
                    title: 'Confirm!',
                    content: 'Are you sure?',
                    buttons: {
                        confirm: function() {
                            //alert("true");
                            $("#img_name").val('');
                            $(".ist-logo").html('');
                            $(".ist-logo").append('<img src="<?php echo bs("public/assets/img/logo-bg.png"); ?>" id="image_write_path" class="img-rounded">');

                        },
                        cancel: function() {
                            return true;
                        }
                    }
                });
            });

            $('.ist-logo').html(CommanJS.getImagebox($('#image_write_path').attr('src'), 'thumbnail', 'f45 text-center mt-3 pt-5'));
            $('[data-toggle="tooltip"]').tooltip();
            $('#document_error').hide();
            $('.delete_data_img').click(function() {
                $(this).parent().remove()
                CommanJS.getDisplayMessgae(200, 'Deleted successfully.');
            });
            $('#metadata_id').click(function() {
                var mod_cont_id = $('#mod_cont_id').val();
                if (mod_cont_id != '') {
                    getMetadata(mod_cont_id);
                } else {
                    return false;
                }
            });

            // Module expirry datepicker
            $("#module_expiry").datepicker({
                changeYear: true,
                changeMonth: true,
                numberOfMonths: 1,
                minDate: 0
            });

            if ($('#module_expiry').val() == '') {
                $('#module_expiry_div').hide();
                $('#module_expiry_check').iCheck('uncheck');
            }

            $('#module_expiry_check').on('change', function() {
                if ($('#module_expiry_check').is(":checked")) {
                    $('#module_expiry_div').show();
                    $('#modulesMetadata').validate();

                    $('#module_expiry').rules('add', {
                        required: true,
                        messages: {
                            required: "Please select date.",
                        }
                    });

                } else {
                    $('#modulesMetadata').validate();

                    $('#module_expiry').rules('remove');
                    $('#module_expiry').val('');
                    $('#module_expiry_div').hide();
                }
            })


            // Uploaded document count  
            var up_doc_cnt = $('span.list-group-item').length;
            $("#docCnt").text(up_doc_cnt);

            // Ck editor
            var config = {
                height: 120,
                toolbar: 'short',
                allowedContent: true
            };

            $('.editor').each(function(e) {
                CKEDITOR.replace(this.id, config);
            });
            // Validating module content form
            $("#modulesContent").validate({
                ignore: [],
                rules: {
                    title: {
                        required: true,
                        remote: {
                            url: "<?php echo bs(); ?>coursemodules/getCheckExist",
                            type: 'POST',
                            data: {
                                'recordID': $('#mod_cont_id').val()
                            }
                        }
                    },
                    getting_started: {
                        required: function(textarea) {
                            CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                            var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                            return editorcontent.length === 0;
                        },
                        //noSpace: true
                    },
                    introduction: {
                        required: function(textarea) {
                            CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                            var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                            return editorcontent.length === 0;
                        },
                        //noSpace: true
                    },
                    outcomes: {
                        required: function(textarea) {
                            CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                            var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                            return editorcontent.length === 0;
                        },
                        //noSpace: true
                    },
                },
                messages: {
                    title: {
                        required: "Please enter module title",
                        remote: "Module name is already exist"
                    },
                    getting_started: {
                        required: "Please enter module getting started "
                    },
                    introduction: {
                        required: "Please enter module introduction"
                    },
                    outcomes: {
                        required: "Please enter module outcomes"
                    },
                },
                submitHandler: function(form) {
                    $.ajax({
                        url: "<?php echo bs(); ?>coursemodules/addModulesContent",
                        type: "POST",
                        dataType: 'json',
                        data: $("#modulesContent").serialize(),
                        success: function(data) {
                            //alert(data);
                            console.log(data);
                            if (data.code == 200) {
                                $('.nav-tabs a[href="#metadata"]').tab('show');
                                $('.nav-tabs a[href="#content"]').attr('data-id', data.id);
                                $('.nav-tabs a[href="#metadata"]').attr('data-id', data.id);
                                $('#mod_cont_id').val(data.id);
                                $('#mod_meta_id').val(data.id);
                                $('#mod_up_img_meta_id').val(data.id);
                                getMetadata(data.id);
                                CommanJS.getDisplayMessgae(200, data.success);
                            } else {
                                CommanJS.getDisplayMessgae(400, data.error);
                            }
                        }
                    });
                },
                errorPlacement: function(error, $elem) {
                    if ($elem.is('textarea')) {
                        $elem.insertAfter($elem.next('div'));
                    }
                    error.insertAfter($elem);
                },
            });


            $.validator.addMethod("noSpace", function(value, element, param) {
                return value.match(/^(?=.*\S).+$/);
            }, "No space please and don't leave it empty");

            // Validating metadata form tab
            $("#modulesMetadata").validate({
                ignore: [],
                rules: {
                    module_expiry: {
                        required: function() {
                            if ($('#exp_status').is(':checked'))
                                return true;
                            else
                                return false;
                        },
                    }
                },
                messages: {
                    module_expiry: {
                        required: "Please set date for expiry of course"
                    },
                },
                submitHandler: function(form) {
                    $.ajax({
                        url: "<?php echo bs('coursemodules/addModulesContent'); ?>",
                        type: "POST",
                        dataType: 'json',
                        data: $("#modulesMetadata").serialize(),
                        success: function(data) {
                            //console.log(data);
                            if (data.code == 200) {
                                $('.nav-tabs a[href="#content"]').attr('data-id', data.id);
                                $('.nav-tabs a[href="#metadata"]').attr('data-id', data.id);
                                $('#hid_meta_id_data').val(data.id);
                                $('#hid_cont_id_data').val(data.id);
                                CommanJS.getDisplayMessgae(200, 'Module saved successfully.');
                                $(window).scrollTop(0);

                            } else {
                                CommanJS.getDisplayMessgae(400, data.error);
                            }
                        }
                    });
                }
            });
        });

        // to get all system and default categories and save into table
        function getMetadata(moduleID) {
            // Course Category  4
            CommanJS.getCatSection(<?= META_MODULE; ?>, 'module_categories_section', 'module_categories', moduleID, <?= META_MODULE; ?>);
            // Course tags  4
            CommanJS.getTagSection(<?= META_MODULE; ?>, 'module_tags_sections', 'module_tags', moduleID, <?= META_MODULE; ?>);

            // System Category  1
            CommanJS.getCatSection(<?= META_SYSTEM; ?>, 'system_categories_section', 'system_categories', moduleID, <?= META_MODULE; ?>);
            // System tags  4
            CommanJS.getTagSection(<?= META_SYSTEM; ?>, 'system_tags_sections', 'system_tags', moduleID, <?= META_MODULE; ?>);

        }

        // Delete module document
        $(document).on('click', '.delete_doc', function() {

            var filename = $(this).data('docname');
            var filenameID = $(this).data('did');

            var scope = $(this);
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure?',
                buttons: {
                    confirm: function() {
                        $.ajax({
                            url: "<?php echo bs(); ?>coursemodules/remove_module_doc",
                            type: "POST",
                            data: {
                                'file': filename,
                                'fileID': filenameID
                            },
                            beforeSend: function() {
                                $("#docLoader").show();
                            },
                            success: function(data) {
                                scope.parent().remove()
                                CommanJS.getDisplayMessgae(200, 'Deleted successfully.');
                                $('#docLoader').hide();
                            }
                        });
                        return true;
                    },
                    cancel: function() {
                        return true;
                    }
                }
            });
        });
    </script>