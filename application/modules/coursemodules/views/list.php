            <div class="container-fluid">
            <div class="row">
        <div class="col-md-12">
            <div class="filter-container flex-row">
                <div class="flex-col-sm-3 selected_condition" id="system_cat">

                </div>
                <div class="flex-col-sm-2 selected_condition" id="system_tag">

                </div>
                <div class="flex-col-sm-3 selected_condition" id="module_cat">

                </div>
                <div class="flex-col-sm-2 selected_condition" id="module_tag">

                </div>
                <div class="flex-col-12 flex-col-md-auto ml-auto">
                    <a class="btn btn-danger pt-2" id="reset_form" href="javascript:void(0)"><i class="flaticon-close-1 f21 fw100"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div data-widget-group="group1">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default panel-grid">
                    <div class="panel-heading">
                    <?php if ($this->ion_auth->is_admin() || $permissions->add_per): ?>
                        <a class="btn btn-primary" href="<?= base_url('coursemodules/add') ?>">Add New</a>
                    <?php endif; ?>    
                    <?php if ($this->ion_auth->is_admin() || $permissions->delete_per): ?>
                <a href="<?php echo base_url('Lessons/setAllSelected') ?>" class="btn btn-primary" id="bulk_lesson">
                    Set all selected as available
                </a>
            <?php endif;?>
            <?php if ($this->ion_auth->is_admin() || $permissions->delete_per): ?>
                <a href="<?php echo base_url('Lessons/setAllSelected') ?>" class="btn btn-primary" id="bulk_lesson2">
                    Set all selected as unavailable
                </a>
            <?php endif;?>

                        <div class="panel-ctrls"></div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="modulesListTable" class="table table-bordered table-striped table-hover" cellspacing="0">
                        <thead>
                            <tr>
                                <th>
                                    <label class="checkbox-tel">
                                        <input type="checkbox" class="checkAll">
                                    </label>
                                </th>
                                <th style="min-width: 120px;">Action</th>
                                <th style="min-width: 180px;">Module Available</th>
                                <th style="min-width: 120px;">Module Title</th>
                                <th style="min-width: 180px;">Long Title</th>
                                <th style="min-width: 180px;">Module Categories</th>
                                <th style="min-width: 120px;">Module Tags</th>
                                <th style="min-width: 180px;">System Categories</th>
                                <th style="min-width: 120px;">System Tags</th>
                                
                                <th style="min-width: 120px;">Date</th>
                                  <th style="min-width: 120px;">Created By</th>
                                <th style="min-width: 180px;">Used in Courses</th>
                                
                            </tr>
                        </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $("#bulk_lesson").click(function(e) {
            e.preventDefault();
            var selected_user_id = "";
            $('input[name="item[]"]:checked').each(function() {
                //  console.log(this.value);
                selected_user_id = this.value + '|' + selected_user_id;
            });
            if (selected_user_id != "") {
                $('#check').val(selected_user_id);
            } else {
                CommanJS.getDisplayMessgae(400, 'Please select atleast one module.')
                return false;
            }
            $.ajax({
                type: "POST",
                url: "coursemodules/setAllSelected",
                data: {
                    selectedLesson: selected_user_id,
                    status: '1'
                },
                dataType: 'json',
                success: function(data) {
                    //console.log(data);
                    if (data.msg === 'Updated') {
                        $('.checkAll').prop('checked', false);
                        table.ajax.reload(); //just reload table
                    }
                }
            });
        });
        $("#bulk_lesson2").click(function(e) {
            e.preventDefault();
            var selected_user_id = "";
            $('input[name="item[]"]:checked').each(function() {
                selected_user_id = this.value + '|' + selected_user_id;
            });
            if (selected_user_id != "") {
                $('#check2').val(selected_user_id);
            } else {
                CommanJS.getDisplayMessgae(400, 'Please select atleast one module.')
                return false;
            }

            $.ajax({
                type: "POST",
                url: "coursemodules/setAllSelected",
                data: {
                    selectedLesson: selected_user_id,
                    status: '0'
                },
                dataType: 'json',
                success: function(data) {
                    // console.log(data);
                    if (data.msg === 'Updated') {
                        $('.checkAll').prop('checked', false);
                        table.ajax.reload(); //just reload table
                    }
                }
            });
        });
        $('#reset_form').click(function() {
            $(".meta_data_filter").prop("checked",false);
            $(".selectFilterMode span").text('');            
            table.search('').draw();
        });
    
    $('.selected_condition').on('change', function() {
            table.search('').draw();
    });

        $('.checkAll').click(function() {
        $(':checkbox.checkLesson').prop('checked', this.checked);
    });
        CommanJS.get_metadata_options("System category", 1, <?=META_SYSTEM;?>, "system_cat");
        CommanJS.get_metadata_options("System tag", 2, <?=META_SYSTEM;?>, "system_tag");
        CommanJS.get_metadata_options("Module category", 1, <?=META_MODULE;?>, "module_cat");
        CommanJS.get_metadata_options("Module tag", 2, <?=META_MODULE;?>, "module_tag");
       
        var table = table = $('#modulesListTable').DataTable({
            // Processing indicator
            "processing": true,
            // DataTables server-side processing mode
            "serverSide": true,
            // Initial no order.
            "iDisplayLength": 10,
            "bPaginate": true,
            "order": [],
            "scrollX": true,
            "autoWidth": false,
            "drawCallback": function(settings) {
                $('.load_meta_data').each(function(key, item) {
                    $.getJSON("<?php echo bs('questions/get_meta_tags/'); ?>" + $(this).attr('id'), function(data) {
                           if(data) $("#"+data.typeid).html(data.value);
                    });
				});
				$('.load_meta_category').each(function(key, item) {
                    $.getJSON("<?php echo bs('questions/get_meta_categories/'); ?>" + $(this).attr('id'), function(data) {
                           if(data) $("#"+data.typeid).html(data.value);
                    });
				});

                $('.load_course_data').each(function(key, item) {
                    $.getJSON("<?php echo bs('coursemodules/get_course_used/'); ?>" + $(this).attr('id'), function(data) {
                        if (data) $("#" + data.typeid).html(data.value);
                    });
                });
                $('.load_module_users').each(function(key, item) {
                    $.getJSON("<?php echo bs('users/get_created_by/modules/'); ?>" + $(this).attr('id'), function(data) {
                        if (data) $("#" + data.typeid).html(data.value);
                    });
                });
            },
            // Load data from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('coursemodules/getLists/'); ?>",
                "type": "POST",
                "data": function(data) {
                    data.system_cat = CommanJS.getMetaCallBack('system_category'); 
                    data.system_tag = CommanJS.getMetaCallBack('system_tag'); 
                    data.module_cat = CommanJS.getMetaCallBack('module_category'); 
                    data.module_tag = CommanJS.getMetaCallBack('module_tag'); 
                    data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                },
            },
            //Set column definition initialisation properties
            "columnDefs": [{
                    "targets": [0],
                    "orderable": false,
                    "data": null,
                    "render": function(data, type, full, meta) {
                        var data = '';
                        if (type == 'display') {
                            data = '<label class="checkbox-tel"><input type="checkbox" class="checkLesson" name="item[]" value="' + full['id'] + '"></label>';
                        }
                        return data;
                    }
                },
                {
                    "targets": [2],
                    "data": null,
                    "render": function(data, type, full, meta) {
                        if (type === 'display') {
                            if (full['status'] == '1') {
                                data = '<a href="<?php bs('coursemodules/Coursemodules/update_status/')?>' + full['id'] + '/deactivate" data-toggle="tooltip" data-placement="top" title="Click to Change Status" class="text-success f18 change_status"><i class="flaticon-checked"></i></a>';
                            } else {
                                data = '<a href="<?php bs('coursemodules/Coursemodules/update_status/')?>' + full['id'] + '/activate" data-toggle="tooltip" data-placement="top" title="Click to Change Status" class="text-danger f18 change_status"><i class="flaticon-close"></i></a>';
                            }
                        }
                        return data;
                    }
                   
                },
                {
                    "targets": [1],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                            data = '';
                        <?php if ($this->ion_auth->is_admin() || $permissions->edit_per) : ?>
                                data = '<a class="btn btn-primary btn-sm" href="<?php echo base_url('coursemodules/add/') ?>' + full['id'] + '" > <i class="ti ti-pencil"></i> </a>';
                            <?php
                        endif;
                       
                        if ($this->ion_auth->is_admin() || $permissions->delete_per) :
                            ?>
                                data += '<a class="btn btn-danger btn-sm delete_item" href="<?php echo base_url('coursemodules/delete/') ?>' + full['id'] + '"> <i class="ti ti-trash"></i> </a>';
                        <?php endif; ?>
                        }
                        return data;
                    }
                },
                {
                    "targets": [5],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                         data = '<span id="' + full['id'] +'_<?php echo META_MODULE; ?>_<?php echo META_MODULE; ?>_cat" data-type="modules_category" class="load_meta_category"> Loading...</span>';
						  //data = 1;
						}
                        return data;
                    }
                },
				{
                    "targets": [6],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                           data = '<span id="' + full['id'] +'_<?php echo META_MODULE; ?>_<?php echo META_MODULE; ?>_tag" data-type="modules_tags" class="load_meta_data"> Loading...</span>';
                        }
                        return data;
                    }
                },
                {
                    "targets": [7],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                           data = '<span id="' + full['id'] +'_<?php echo META_MODULE ; ?>_<?php echo META_SYSTEM; ?>_syscat" data-type="system_category" class="load_meta_category"> Loading...</span>';
						 //data = 2;
						}
                        return data;
                    }
                },
                {
                    "targets": [8],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                           data = '<span id="' + full['id'] +'_<?php echo META_MODULE ; ?>_<?php echo META_SYSTEM; ?>_systag" data-type="system_tags" class="load_meta_data"> Loading...</span>';
                        }
                        return data;
                    }
                },
                {
                    "targets": [10],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                            data = '<span id="' + full['id'] + '_users"  class="load_module_users"> Loading...</span>';
                        }
                        return data;
                    }
                },
                {
                    "targets": [11],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                            data = '<span id="' + full['id'] + '_course"  class="load_course_data"> Loading...</span>';
                        }
                        return data;
                        //return "NA";
                    }
                }
            ],
            "columns": [               
                {
                    "data": null,
                },
                {
                    "data": null,
                },
                {
                    "data": null,
                },
                {
                    "data": "name",
                },
                {
                    "data": "long_title",
                },
                {
                    "data": "categories",
                },
                {
                    "data": "tags",
                },
                {
                    "data": "system_categories",
                },
                {
                    "data": "system_tags",
                },
                {
                    "data": "created",
                },
                {
                    "data": "created_by",
                },
                {
                    "data": "id",
                }                
            ]
        });

        $(document).on('click', '.change_status', function (e) {
            e.preventDefault();
            $.get($(this).attr("href"), // url
                function (data, textStatus, jqXHR) { // success callback
                    var obj = JSON.parse(data);
                    if (obj.msg === 'Updated') {
                        table.ajax.reload();  //just reload table
                    }
                });
        });

        // To delete record
        $(document).on('click', '.delete_item', function (e) {
            e.preventDefault();
            var scope = $(this);
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure?',
                buttons: {
                    confirm: function () {
                        $.get(scope.attr("href"), // url
                            function (data, textStatus, jqXHR) { // success callback
                                var obj = JSON.parse(data);
                                if (obj.msg === 'deleted') {
                                    table.ajax.reload(); //just reload table
                                }
                            });
                        return true;
                    },
                    cancel: function () {
                        return true;
                    }
                }
            });
        });
    });
      
    
        $(document).ready(function() {
        $("#modulesListTable_paginate .pagination").click(function() {
            $("html, body").animate({
                scrollTop: 0
            }, "slow");
        });
    });
</script>