<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Quiz_model extends CI_Model {

    public function __construct() {
        parent::__construct();

        //load database library
        $this->load->database();
    }



    function getQuestionBysection($id = "") {
        $this->db->select('quiz_section_questions.id,quiz_section_questions.question_id,questions.title, questions.question_prompt , questions.id,questions.type');
        $this->db->from('quiz_section_questions');
        $this->db->join('questions', 'quiz_section_questions.question_id = questions.id AND questions.status = 1 AND questions.delete_status = 1');
        $this->db->where("quiz_section_questions.section_id=" . $id);
        $query = $this->db->get();
        //  pr($this->db->last_query());
        //  exit;
        $data = $query->result_array();
        $i = 0;
        foreach ($data as $key => $row) {
            $i = $i + 1;
            $ans = $this->getOptionsByQuestion($row['question_id']);
            $data[$key]['options'] = $ans;
        }
        $alldata['question'] = $data;
        $alldata['total_question'] = $i;
        return $alldata;
    }
    

    function getOptionsByQuestion($id = "") {
        $this->db->select('question_responses.id,question_responses.response,question_responses.answer_status');
        $this->db->from('question_responses');
        $this->db->where(["question_responses.question_id" => $id, "question_responses.delete_status" => 1]);
        $query = $this->db->get();
        $data = $query->result_array();

        return $data;
    }
    
    
        /*
     * Fetch user data
     */
    function getQuizQuestionAll($request) {
        $course = $this->Common_model->getAllData('courses', 'id', true, array("slug" => trim($request->course_slug)));
        $quizType = $this->Common_model->getAllData('quizzes', 'type', true,['id'=>$request->quiz_id]);
        
        if(isset($request->module_slug) && $request->module_slug != ''){
            $module = $this->Common_model->getAllData('modules', 'id', true, array("slug" => trim($request->module_slug)));
            // for module
            $this->db->select('quiz_sections.id, quiz_sections.title, module_contain.quiz_type');
            $this->db->from('module_contain');
            $this->db->join('quiz_sections', 'quiz_sections.quiz_id = module_contain.quiz_id'); 
            $this->db->where("module_contain.course_id = ".$course->id." AND module_contain.module_id =".$module->id);
            $this->db->where("module_contain.quiz_id=" . $request->quiz_id);  
        }
        
        if(isset($request->lesson_slug) && $request->lesson_slug != ''){
            $lesson = $this->Common_model->getAllData('lessons', 'id', true, array("slug" => trim($request->lesson_slug)));
            // for lesson 
            $this->db->select('quiz_sections.id, quiz_sections.title,  lesson_contain.quiz_type');
            $this->db->from('lesson_contain');
            $this->db->join('quiz_sections', 'quiz_sections.quiz_id = lesson_contain.quiz_id'); 
            $this->db->where("lesson_contain.course_id = ".$course->id." AND lesson_contain.lesson_id =".$lesson->id);
            $this->db->where("lesson_contain.quiz_id=" . $request->quiz_id);
        }
        
        if(!isset($request->lesson_slug) && !isset($request->module_slug) ){
            // for course
            $this->db->select('quiz_sections.id, quiz_sections.title, course_contain.quiz_type');
            $this->db->from('course_contain');
            $this->db->join('quiz_sections', 'quiz_sections.quiz_id = course_contain.quiz_id'); 
            $this->db->where("course_contain.course_id = ".$course->id." AND course_contain.module_id IS NULL");
            $this->db->where("course_contain.quiz_id=" . $request->quiz_id);
        }    
        
       /* if($course->id && $module->id && !$lesson->id){
            $this->db->select('quiz_sections.id, quiz_sections.title, module_contain.quiz_type');
            $this->db->from('module_contain');
            $this->db->join('quiz_sections', 'quiz_sections.quiz_id = module_contain.quiz_id'); 
            $this->db->where("module_contain.course_id = ".$course->id." AND module_contain.module_id =".$module->id);
            $this->db->where("module_contain.quiz_id=" . $request->quiz_id);         
        }elseif($course->id && $lesson->id && !$module->id){
            $this->db->select('quiz_sections.id, quiz_sections.title, module_contain.quiz_type');
            $this->db->from('module_contain');
            $this->db->join('quiz_sections', 'quiz_sections.quiz_id = module_contain.quiz_id'); 
            $this->db->where("module_contain.course_id = ".$course->id." AND module_contain.lesson_id =".$lesson->id);
            $this->db->where("module_contain.quiz_id=" . $request->quiz_id);
        }elseif($course->id && !$module->id && !$lesson->id){
            $this->db->select('quiz_sections.id, quiz_sections.title, course_contain.quiz_type');
            $this->db->from('course_contain');
            $this->db->join('quiz_sections', 'quiz_sections.quiz_id = course_contain.quiz_id'); 
            $this->db->where("course_contain.course_id = ".$course->id." AND course_contain.module_id IS NULL");
            $this->db->where("course_contain.quiz_id=" . $request->quiz_id);
        }  */  

        $query = $this->db->get();
        $data = $query->result_array();
        $allSectionQuestionsCount = 0;       
        foreach ( $data as $key => $row) {
           
            $questions = $this->getQuestionBysection($row['id']);
            $data[$key]['questions'] = $questions['question'];
            $data[$key]['total_question'] = $questions['total_question'];
            $allSectionQuestionsCount += $questions['total_question'];
        }
        
        $data['quiz_type'] = $quizType;
        $result['questions'] = $data;
        $result['allSectionQuestionsCount'] = $allSectionQuestionsCount;
        return $result;
    }

    function getQuizQuestion($id = "") {
        
        $this->db->select('quiz_sections.id,quiz_sections.title');
        $this->db->from('quiz_sections');
        $this->db->where("quiz_sections.quiz_id=" . $id);
        $query = $this->db->get();
        $data = $query->result_array();
        $allSectionQuestionsCount = 0;
        
        foreach ($data as $key => $row) {
            $questions = $this->getQuestionBysection($row['id']);
            $data[$key]['questions'] = $questions['question'];
            $data[$key]['total_question'] = $questions['total_question'];
            $allSectionQuestionsCount += $questions['total_question'];
        }
        $result['questions'] = $data;
        $result['allSectionQuestionsCount'] = $allSectionQuestionsCount;
        return $result;
    }

    /***********************Answer****************/

    function getQuizQuestionWithAns($quiz_id = "", $user_id = '') {
        $this->db->select('quiz_sections.id,quiz_sections.title');
        $this->db->from('quiz_sections');
        $this->db->where("quiz_sections.quiz_id=" . $quiz_id);
        $query = $this->db->get();
        $data = $query->result_array();
        $allSectionQuestionsCount = 0;
        foreach ($data as $key => $row) {
            $questions = $this->getQuestionBysectionWithAns($row['id'], $user_id);
            $data[$key]['questions'] = $questions['question'];
            $data[$key]['total_question'] = $questions['total_question'];
            $allSectionQuestionsCount += $questions['total_question'];
        }
        $result['questions'] = $data;
        $result['allSectionQuestionsCount'] = $allSectionQuestionsCount;
        return $result;
    }

    function getQuestionBysectionWithAns($id, $user_id) {
        $this->db->select('quiz_section_questions.id,quiz_section_questions.question_id,questions.title, questions.question_prompt , questions.id,questions.type,gradebooks_evaluation.answered_id,if(gradebooks_evaluation.correct = 1,"true","false") as correct');
        $this->db->from('quiz_section_questions');
        $this->db->join('questions', 'quiz_section_questions.question_id = questions.id');        
        $this->db->join('gradebooks_evaluation', 'quiz_section_questions.question_id = gradebooks_evaluation.question_id AND gradebooks_evaluation.user_id = '.$user_id);        
        $this->db->where(["quiz_section_questions.section_id" => $id]);
        $query = $this->db->get();
        $data = $query->result_array();
        $i = 0;
        foreach ($data as $key => $row) {
            $i = $i + 1;
            $ans = $this->getSubmittedOptionsByQuestion($row['question_id'],$row['answered_id']);
            $data[$key]['options'] = $ans;
            
            if($row['correct'] == 'true'){
                $data[$key]['correct'] = TRUE; 
            }else{
                $data[$key]['correct'] = FALSE; 
            }
                    
        }
        $alldata['question'] = $data;
        $alldata['total_question'] = $i;
        return $alldata;
    }

    function getSubmittedOptionsByQuestion($id = "",$ans_id) {
        $this->db->select('question_responses.id,question_responses.response,question_responses.answer_status,,if(question_responses.id='.$ans_id.',"1","0") as selected_option');
        $this->db->from('question_responses');
        //$this->db->where("question_responses.question_id=" . $id );
        $this->db->where(["question_responses.question_id" => $id, "question_responses.delete_status" => 1]);
        $query = $this->db->get();
        $data = $query->result_array();

        return $data;
    }

    /* With multiple answer*/
    public function getQuizQuestionWithMultiAns($quiz_id = "", $user_id = '') {
        $this->db->select('quiz_sections.id,quiz_sections.title');
        $this->db->from('quiz_sections');
        $this->db->join('quiz_section_questions', 'quiz_sections.id = quiz_section_questions.section_id');  
        $this->db->where("quiz_sections.quiz_id=" . $quiz_id);
        $this->db->group_by('quiz_sections.id');
        // $this->db->order_by('quiz_sections.id ASC');

        $query = $this->db->get();
        $data = $query->result_array();
        $allSectionQuestionsCount = 0;
        foreach ($data as $key => $row) {
            $questions = $this->__getQuestionBysectionWithMulAns($row['id'], $user_id);
            $data[$key]['questions'] = $questions['question'];
            $data[$key]['total_question'] = $questions['total_question'];
            $allSectionQuestionsCount += $questions['total_question'];
        }
        $result['questions'] = $data;
        $result['allSectionQuestionsCount'] = $allSectionQuestionsCount;
        return $result;
    }

    private function __getQuestionBysectionWithMulAns($id, $user_id) {
        $this->db->select('quiz_section_questions.id,quiz_section_questions.question_id,questions.title, questions.question_prompt , questions.id,questions.type,gradebooks_evaluation.answered_id,if(gradebooks_evaluation.correct = 1,"1","0") as correct, 
            GROUP_CONCAT(gradebooks_evaluation.answered_id SEPARATOR "|") as multiple_answer
            ');
        $this->db->from('quiz_section_questions');
        $this->db->join('questions', 'quiz_section_questions.question_id = questions.id');        
        $this->db->join('gradebooks_evaluation', 'quiz_section_questions.question_id = gradebooks_evaluation.question_id AND gradebooks_evaluation.user_id = '.$user_id);        
        $this->db->where(["quiz_section_questions.section_id" => $id]);
        $this->db->group_by('gradebooks_evaluation.question_id');
        $this->db->order_by('quiz_section_questions.id ASC');
        $query = $this->db->get();
        $data = $query->result_array();
        $i = 0;
        foreach ($data as $key => $row) {
            $selectedAnswerArray = explode("|",$row['multiple_answer']);
            $i = $i + 1;
            $ans = $this->__getSubmittedOptionsByMulQuestion($row['question_id'],$selectedAnswerArray);
            $data[$key]['options'] = $ans;
         }
        $alldata['question'] = $data;
        $alldata['total_question'] = $i;
       
        return $alldata;
    }
     private function __getSubmittedOptionsByMulQuestion($questionID,  $multiple_answers = []) {
        $this->db->select('question_responses.id,question_responses.response,question_responses.answer_status');
        $this->db->from('question_responses');
        $this->db->where(["question_responses.question_id" => $questionID,"question_responses.delete_status"=>1]);
        $query = $this->db->get();
        $data = $query->result_array();
        $answerArray = [];
        foreach ($data as $key => $value) {
            $selected_option = (in_array($value['id'], $multiple_answers))?1:0;
            $answerArray[] = [
                'id' => $value['id'],
                'response'=> $value['response'],
                'answer_status'=> $value['answer_status'],
                'selected_option' => $selected_option
            ];
        }
        return $answerArray;
    }
    /*End*/


    public function getCheckQuizAttempt($postData = [])
    {
        $this->db->select('quizzes.id as quiz_id, quizzes.number_attempt,course_activities.contain_attempt,if(quizzes.number_attempt > 0 AND quizzes.number_attempt <= course_activities.contain_attempt,0,1) as play_allow');
        $this->db->from('quizzes');
        $this->db->join('course_activities', 'quizzes.id = course_activities.contain_id AND course_activities.contain_type = 1 AND course_activities.course_id = '.$postData['courseId'].' AND course_activities.section_id= '.$postData['sectionId'].' AND course_activities.user_id = '.$postData['user_id'].' AND course_activities.module_id= '.$postData['moduleId'].' AND course_activities.lesson_id ='.$postData['lessonId'],'left');
        $this->db->where("quizzes.id=" . $postData['quizz_id']);
        $query = $this->db->get();
        return $query->row();
    }

}

?>