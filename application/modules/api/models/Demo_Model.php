<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Demo_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
		
		//load database library
        $this->load->database();
    }

    /*
     * Fetch user data
     */
    function getRows($id = ""){
        if(!empty($id)){
            $query = $this->db->get_where('users', array('id' => $id));
            return $query->row_array();
        }else{
            $query = $this->db->get('users');
            return $query->result_array();
        }
    }
    
    /*
     * Insert user data
     */
    public function insert($data = array()) {
		if(!array_key_exists('created', $data)){
			$data['created'] = date("Y-m-d H:i:s");
		}
		if(!array_key_exists('modified', $data)){
			$data['modified'] = date("Y-m-d H:i:s");
		}
        $insert = $this->db->insert('users', $data);
        if($insert){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }
    
    /*
     * Update user data
     */
    public function update($data, $id) {
        if(!empty($data) && !empty($id)){
			if(!array_key_exists('modified', $data)){
				$data['modified'] = date("Y-m-d H:i:s");
			}
            $update = $this->db->update('users', $data, array('id'=>$id));
            return $update?true:false;
        }else{
            return false;
        }
    }
    
    /*
     * Delete user data
     */
    public function delete($id){
        $delete = $this->db->delete('users',array('id'=>$id));
        return $delete ? true : false;
    }


    function  getCourseMenuData($slug, $user_id){
       
        $this->db->select('courses.id');
        $this->db->from('courses');
        $this->db->where(['courses.slug' => $slug]);
        $course_id = $this->db->get()->row()->id;
        
        $this->db->select('course_activities.id');
        $this->db->from('course_activities');
        $this->db->where(['course_activities.course_id' => $course_id, 'course_activities.module_id' => 0, 'course_activities.lesson_id' => 0, 'course_activities.contain_id' => 0, 'course_activities.user_id' => $user_id]);
        $courseData['course_status']= $this->db->get()->row()->id;
                
        $this->db->select('modules.id, modules.name, modules.long_title, modules.slug, moduleactivity.id as module_status');
        $this->db->from('course_contain');
        $this->db->join('modules', 'course_contain.module_id = modules.id');
        $this->db->join('course_activities as moduleactivity', 'moduleactivity.lesson_id = 0 AND moduleactivity.contain_id = 0 AND  moduleactivity.course_id = '.$course_id.' AND moduleactivity.module_id = modules.id AND moduleactivity.user_id ='.$user_id, 'left');
               
        $this->db->where(['course_contain.course_id' => $course_id]);
        $this->db->order_by("course_contain.order_id", "asc"); 
                
        foreach($this->db->get()->result_array() as $items):
            $items['lessons'] = $this->get_lessons($items['id'], $course_id, $user_id);
            $courseData['modules'][] = $items;
        endforeach;  
      return $courseData;
    }

    function get_lessons($module_id = 0, $course_id, $user_id = 0){
            $this->db->select('DISTINCT(lessons.id), lessons.name, lessons.long_title, lessons.slug, lessonactivities.id as lesson_status');
            $this->db->from('module_contain');
            $this->db->join('lessons', 'module_contain.lesson_id = lessons.id');

            $this->db->join('course_activities as lessonactivities', 'lessonactivities.course_id = '.$course_id.' AND lessonactivities.module_id ='.$module_id.' AND lessonactivities.lesson_id = lessons.id AND lessonactivities.contain_id = 0 AND lessonactivities.user_id = '.$user_id, 'left');

            $this->db->where(['module_contain.module_id' => $module_id]);
            $this->db->where(['module_contain.course_id' => $course_id]);

            $this->db->order_by("module_contain.order_id", "asc");

            return $this->db->get()->result();
    }

    function get_courses($user_id = 0){
        $this->db->select('courses.id, courses.slug, courses.name,courses.introduction, courses.long_title, courses.code, courses.course_img, course_sections.name as section_name, course_sections.slug as section_slug');
        $this->db->from('course_assigned_to_users');
        $this->db->join('courses', 'course_assigned_to_users.course_id = courses.id');
        $this->db->join('course_sections', 'course_assigned_to_users.section_id = course_sections.id', 'left');
        $this->db->where(['course_assigned_to_users.user_id' => $user_id]);
        $this->db->order_by("course_assigned_to_users.id", "desc");
        return $this->db->get()->result();
    }

    function get_course($user_id, $courseSlug, $sectionSlug){
        $this->db->select('courses.id, courses.description,courses.introduction,courses.outcomes,courses.getting_started,courses.slug, courses.name, course_sections.end_date as course_end_date ,  courses.long_title, courses.code, courses.course_img, course_sections.id as section_id,course_sections.name as section_name, course_sections.slug as section_slug');
        $this->db->from('course_assigned_to_users');
        $this->db->join('courses', 'course_assigned_to_users.course_id = courses.id');
        $this->db->join('course_sections', 'course_assigned_to_users.section_id = course_sections.id', 'left');
        $this->db->where(['course_assigned_to_users.user_id' => $user_id, 'courses.slug' => $courseSlug, 'course_sections.slug' => $sectionSlug]);
        return $this->db->get()->row();
    }

}
?>