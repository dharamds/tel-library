<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Quiz_model extends CI_Model {

    public function __construct() {
        parent::__construct();

        //load database library
        $this->load->database();
    }



    function getQuestionBysection($id = "") {
        $this->db->select('quiz_section_questions.id,quiz_section_questions.question_id,questions.title, questions.question_prompt , questions.id,questions.type');
        $this->db->from('quiz_section_questions');
        $this->db->join('questions', 'quiz_section_questions.question_id = questions.id');
        $this->db->where("quiz_section_questions.section_id=" . $id);
        $this->db->group_by('quiz_section_questions.quiz_id, quiz_section_questions.section_id, quiz_section_questions.question_id');
        $query = $this->db->get();
        //  pr($this->db->last_query());
        //  exit;
        $data = $query->result_array();

        $i = 0;
        foreach ($data as $key => $row) {
            $i = $i + 1;
            $ans = $this->getOptionsByQuestion($row['question_id']);
            $data[$key]['options'] = $ans;
        }
        $alldata['question'] = $data;
        $alldata['total_question'] = $i;
        return $alldata;
    }
    

    function getOptionsByQuestion($id = "") {
        $this->db->select('question_responses.id,question_responses.response,question_responses.answer_status');
        $this->db->from('question_responses');
        $this->db->where("question_responses.question_id=" . $id);
        $query = $this->db->get();
        $data = $query->result_array();

        return $data;
    }
    
   


        function getQuizCyk($id) {

            $this->db->select('quiz_sections.id,quiz_sections.title');
            $this->db->from('quiz_sections');
            $this->db->where("quiz_sections.quiz_id=" . $id);
           // $this->db->limit(1);
            $query = $this->db->get();
            $data = $query->result();
       
            $allSectionQuestionsCount = 0;       
            foreach ($data as $key => $row) {
                $questions = $this->getQuestionBysection($row->id);
                $newdata[$key]['questions'] = $questions['question'];
                $newdata[$key]['total_question'] = $questions['total_question'];
             
                $allSectionQuestionsCount += $questions['total_question'];
            }
            
            $result['questions'] = $newdata;
            $result['allSectionQuestionsCount'] = $allSectionQuestionsCount;
            return $result;
        }
        /*
     * Fetch user data
     */

    function getQuizQuestion($request) {

        $course = $this->Common_model->getAllData('courses', 'id', true, array("slug" => trim($request->course_slug)));
        $module = $this->Common_model->getAllData('modules', 'id', true, array("slug" => trim($request->module_slug)));

        if($course->id && $module->id){
            $this->db->select('quiz_sections.id, quiz_sections.title, module_contain.quiz_type');
            $this->db->from('module_contain');
            $this->db->join('quiz_sections', 'quiz_sections.quiz_id = module_contain.quiz_id'); 
            $this->db->where(" module_contain.course_id = ".$course->id." AND module_contain.module_id =".$module->id);
            $this->db->where("module_contain.quiz_id=" . $request->quiz_id);         
        }elseif($course->id && !$module->id){
            $this->db->select('quiz_sections.id, quiz_sections.title, course_contain.quiz_type');
            $this->db->from('course_contain');
            $this->db->join('quiz_sections', 'quiz_sections.quiz_id = course_contain.quiz_id'); 
            $this->db->where(" course_contain.course_id = ".$course->id." AND course_contain.module_id IS NULL");
            $this->db->where("course_contain.quiz_id=" . $request->quiz_id);
        }    

        $query = $this->db->get();
        $data = $query->result_array();
        $allSectionQuestionsCount = 0;       
        foreach ($data as $key => $row) {
            $questions = $this->getQuestionBysection($row['id']);
            $data[$key]['questions'] = $questions['question'];
            $data[$key]['total_question'] = $questions['total_question'];
            $allSectionQuestionsCount += $questions['total_question'];
        }
        $result['questions'] = $data;
        $result['allSectionQuestionsCount'] = $allSectionQuestionsCount;
        return $result;
    }

    /***********************Answer****************/

    function getQuizQuestionWithAns($quiz_id = "", $user_id = '') {
        $this->db->select('quiz_sections.id,quiz_sections.title');
        $this->db->from('quiz_sections');
        $this->db->where("quiz_sections.quiz_id=" . $quiz_id);
        $this->db->where("quiz_sections.status='1'");
        $this->db->where("quiz_sections.delete_status='1'");
        $query = $this->db->get();
        $data = $query->result_array();
        $allSectionQuestionsCount = 0;

        foreach ($data as $key => $row) {
            $questions = $this->getQuestionBysectionWithAns($row['id'], $user_id,$quiz_id);
            $data[$key]['questions'] = $questions['question'];
            $data[$key]['total_question'] = $questions['total_question'];
            $allSectionQuestionsCount += $questions['total_question'];
        }

        $result['questions'] = $data;
        $result['allSectionQuestionsCount'] = $allSectionQuestionsCount;
        return $result;
    }

    function getQuestionBysectionWithAns($id, $user_id,$quiz_id) {
        
        $this->db->select('quiz_section_questions.id,quiz_section_questions.question_id,questions.title, questions.question_prompt , questions.id,questions.type,gradebooks_evaluation.answered_id,if(gradebooks_evaluation.correct = 1,"true","false") as correct');
        $this->db->from('quiz_section_questions');
        $this->db->join('questions', 'quiz_section_questions.question_id = questions.id');        
        $this->db->join('gradebooks_evaluation', 'quiz_section_questions.question_id = gradebooks_evaluation.question_id AND gradebooks_evaluation.reference_id = "'.$quiz_id.'" AND gradebooks_evaluation.user_id = '.$user_id);        
        $this->db->where(["quiz_section_questions.section_id" => $id]);
        $this->db->group_by('quiz_section_questions.id');
        $query = $this->db->get();
        $data = $query->result_array();
        $i = 0;
        foreach ($data as $key => $row) {
            $i = $i + 1;
            $ans = $this->getSubmittedOptionsByQuestion($row['question_id'],$row['answered_id']);
            $data[$key]['options'] = $ans;
            
            if($row['correct'] == 'true'){
                $data[$key]['correct'] = TRUE; 
            }else{
                $data[$key]['correct'] = FALSE; 
            }
                    
        }
        $alldata['question'] = $data;
        $alldata['total_question'] = $i;
        return $alldata;
               
        
    }
    function getSubmittedOptionsByQuestion($id = "",$ans_id) {
        $this->db->select('question_responses.id,question_responses.response,question_responses.answer_status,,if(question_responses.id='.$ans_id.',"1","0") as selected_option');
        $this->db->from('question_responses');
        $this->db->where("question_responses.question_id=" . $id);
        $query = $this->db->get();
        $data = $query->result_array();

        return $data;
    }

}

?>