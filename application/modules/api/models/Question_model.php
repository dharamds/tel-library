<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Question_model extends CI_Model {

    public function __construct() {
        parent::__construct();
		
		//load database library
        $this->load->database();
    }

   
    public function getOptionByQuestion($questionResponse = []){
       
       
        $this->db->select('question_responses.id,question_responses.question_id,question_responses.answer_status,question_responses.weight');
        $this->db->from('question_responses');         
        $this->db->where_in("question_responses.id", $questionResponse);
        $this->db->group_by("question_responses.question_id");
        $query = $this->db->get();
        return $query->result_array();

    }
    
   
}
?>