<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Gradebook_Model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();

        //load database library
        $this->load->database();
    }

    function  getGradebookData($user_id)
    {

        $course_data_query = $this->db->query("SELECT c.name as course_name,c.id as course_id,cs.name as section_name FROM `course_assigned_to_users` as cau
        JOIN courses as c ON (c.id=cau.course_id)
        LEFT JOIN course_sections as cs ON (cs.id=cau.course_id)
        WHERE cau.user_id='" . $user_id . "' AND c.status=1 AND c.delete_status=1
        GROUP BY cau.course_id");


        $course_data = $course_data_query->result_array();


        foreach ($course_data as $items) :
            $items['cyk_data'] = $this->get_cyk_data($items['course_id'], $user_id);
            $items['evidence_data'] = $this->get_evidence_data($items['course_id'], $user_id);
            $items['module_quiz_data'] = $this->get_module_quiz_data($items['course_id'], $user_id);
            //pr(vd());
            $courseData['data'][] = $items;
        endforeach;
        return $courseData;
    }

    private function get_cyk_data($course_id, $user_id)
    {
        //$cyk_data_query = $this->db->query("SELECT q.title,IF((SELECT COUNT(reference_id) from gradebooks_evaluation where user_id='" . $user_id . "' AND course_id='" . $course_id . "' AND reference_type = 1 AND reference_id=q.id AND quize_type=1 ) , '1', '0') as cyk_completed_status FROM gradebooks_evaluation AS ge JOIN quizzes AS q ON (ge.reference_id = q.id)  WHERE ge.user_id = '".$user_id."' and ge.course_id = '".$course_id."' AND ge.reference_type=1 AND ge.quize_type=1");
        //$cyk_data_query = $this->db->query("SELECT `quizzes`.`id` , `quizzes`.`title` ,IF((SELECT COUNT(reference_id) from gradebooks_evaluation where user_id='" . $user_id . "' AND course_id='" . $course_id . "' AND reference_type = 1 AND reference_id=quizzes.id AND quize_type=1 ) , '1', '0') as cyk_completed_status FROM `module_contain` JOIN `lessons` ON `module_contain`.`lesson_id` = `lessons`.`id` JOIN `quizzes` ON `quizzes`.`id` = `lessons`.`quiz_id` JOIN `gradebooks_evaluation` AS `ge` ON (`ge`.`course_id` = `module_contain`.`course_id`)  WHERE `module_contain`.`course_id` = '" . $course_id . "' AND `ge`.`user_id` = '" . $user_id . "' AND `ge`.`reference_type`=1 AND `ge`.`quize_type`=1 GROUP BY `quizzes`.`id`");
        $cyk_data_query = $this->db->query("SELECT
        `quizzes`.`id`,
        `quizzes`.`type`,
        `quizzes`.`title`,
        `gqag`.`grade`
    FROM
        `quizzes`
    LEFT JOIN `module_contain` ON `quizzes`.`id` = `module_contain`.`quiz_id`
    LEFT JOIN `lessons` ON `module_contain`.`lesson_id` = `lessons`.`id`
    LEFT JOIN `gradebook_quiz_assess_grade` AS `gqag`
    ON
        (
            `gqag`.`course_id` = `module_contain`.`course_id` AND `gqag`.`quiz_assess_id` = `quizzes`.`id` AND `gqag`.`quiz_assess_type` = 1 AND `gqag`.`user_id` = '" . $user_id . "'
        )
    WHERE
        (`module_contain`.`course_id`='" . $course_id . "') AND `quizzes`.`type` IN (1)
    ORDER BY
        `quizzes`.`id` ASC");
        return $cyk_data_query->result_array();
    }

    private function get_evidence_data($course_id, $user_id)
    {
        //$quiz_data_query = $this->db->query("SELECT q.title,IF((SELECT COUNT(reference_id) from gradebooks_evaluation where user_id='" . $user_id . "' AND course_id='" . $course_id . "' AND reference_type = 1 AND reference_id=q.id AND quize_type=3 ) , '1', '0') as cyk_completed_status FROM gradebooks_evaluation AS ge JOIN quizzes AS q ON (ge.reference_id = q.id)  WHERE ge.user_id = '" . $user_id . "' and ge.course_id = '" . $course_id . "' AND ge.reference_type=1 AND ge.quize_type=1");
        //$quiz_data_query = $this->db->query("SELECT `quizzes`.`id` , `quizzes`.`title` , `gqag`.`grade` FROM `module_contain` JOIN `lessons` ON `module_contain`.`lesson_id` = `lessons`.`id` JOIN `quizzes` ON `quizzes`.`id` = `lessons`.`quiz_id` JOIN `gradebooks_evaluation` AS `ge` ON (`ge`.`course_id` = `module_contain`.`course_id`) LEFT JOIN `gradebook_quiz_assess_grade` AS `gqag` ON ( `gqag`.`course_id` = `ge`.`course_id` AND `gqag`.`quiz_assess_id`=`quizzes`.`id` AND `gqag`.`quiz_assess_type`=3 AND `gqag`.`user_id`=`ge`.`user_id` )  WHERE `module_contain`.`course_id` = '" . $course_id . "' AND `ge`.`user_id` = '" . $user_id . "' AND `ge`.`reference_type`=1 AND `ge`.`quize_type`=3 GROUP BY `quizzes`.`id`");
        $quiz_data_query = $this->db->query("SELECT
            `quizzes`.`id`,
            `quizzes`.`type`,
            `quizzes`.`title`,
            `gqag`.`grade`
            FROM
                `quizzes`
            LEFT JOIN `module_contain` ON `quizzes`.`id` = `module_contain`.`quiz_id`
            LEFT JOIN `gradebook_quiz_assess_grade` AS `gqag`
            ON
                (
                    `gqag`.`course_id` = `module_contain`.`course_id` AND `gqag`.`quiz_assess_id` = `quizzes`.`id` AND `gqag`.`quiz_assess_type` = 2 AND `gqag`.`user_id` = '" . $user_id . "'
                )
            WHERE
                `module_contain`.`course_id`='" . $course_id . "' AND `quizzes`.`type` IN (3)
            ORDER BY
                `quizzes`.`id` ASC");
        return $quiz_data_query->result_array();
    }

    private function get_module_quiz_data($course_id, $user_id)
    {
        //$quiz_data_query = $this->db->query("SELECT q.title,IF((SELECT COUNT(reference_id) from gradebooks_evaluation where user_id='" . $user_id . "' AND course_id='" . $course_id . "' AND reference_type = 1 AND reference_id=q.id AND quize_type=3 ) , '1', '0') as cyk_completed_status FROM gradebooks_evaluation AS ge JOIN quizzes AS q ON (ge.reference_id = q.id)  WHERE ge.user_id = '" . $user_id . "' and ge.course_id = '" . $course_id . "' AND ge.reference_type=1 AND ge.quize_type=1");
        //$quiz_data_query = $this->db->query("SELECT `quizzes`.`id` , `quizzes`.`title` , `gqag`.`grade` FROM `module_contain` JOIN `lessons` ON `module_contain`.`lesson_id` = `lessons`.`id` JOIN `quizzes` ON ( `quizzes`.`id` = `lessons`.`quiz_id` AND `quizzes`.`type`=2) JOIN `gradebooks_evaluation` AS `ge` ON (`ge`.`course_id` = `module_contain`.`course_id`) LEFT JOIN `gradebook_quiz_assess_grade` AS `gqag` ON ( `gqag`.`course_id` = `ge`.`course_id` AND `gqag`.`quiz_assess_id`=`quizzes`.`id` AND `gqag`.`quiz_assess_type`=2 AND `gqag`.`user_id`=`ge`.`user_id` )  WHERE `module_contain`.`course_id` = '" . $course_id . "' AND `ge`.`user_id` = '".$user_id."' AND `ge`.`reference_type`=1 AND `ge`.`quize_type`=2 GROUP BY `quizzes`.`id`");
        $quiz_data_query = $this->db->query("SELECT
            `quizzes`.`id`,
            `quizzes`.`type`,
            `quizzes`.`title`,
            `gqag`.`grade`
            FROM
                `quizzes`
            LEFT JOIN `module_contain` ON `quizzes`.`id` = `module_contain`.`quiz_id`
            LEFT JOIN `gradebook_quiz_assess_grade` AS `gqag`
            ON
                (
                    `gqag`.`course_id` = `module_contain`.`course_id` AND `gqag`.`quiz_assess_id` = `quizzes`.`id` AND `gqag`.`quiz_assess_type` = 2 AND `gqag`.`user_id` = '" . $user_id . "'
                )
            WHERE
                `module_contain`.`course_id`='" . $course_id . "' AND `quizzes`.`type` IN (2)
            ORDER BY
                `quizzes`.`id` ASC");
        return $quiz_data_query->result_array();
    }
}
