<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Course_Model extends CI_Model {

    public function __construct() {
        parent::__construct();

        //load database library
        $this->load->database();
    }

    /*
     * Fetch user data
     */

    function getRows($id = "") {
        if (!empty($id)) {
            $query = $this->db->get_where('users', array('id' => $id));
            return $query->row_array();
        } else {
            $query = $this->db->get('users');
            return $query->result_array();
        }
    }

    /*
     * Insert user data
     */

    public function insert($data = array()) {
        if (!array_key_exists('created', $data)) {
            $data['created'] = date("Y-m-d H:i:s");
        }
        if (!array_key_exists('modified', $data)) {
            $data['modified'] = date("Y-m-d H:i:s");
        }
        $insert = $this->db->insert('users', $data);
        if ($insert) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    /*
     * Update user data
     */

    public function update($data, $id) {
        if (!empty($data) && !empty($id)) {
            if (!array_key_exists('modified', $data)) {
                $data['modified'] = date("Y-m-d H:i:s");
            }
            $update = $this->db->update('users', $data, array('id' => $id));
            return $update ? true : false;
        } else {
            return false;
        }
    }

    /*
     * Delete user data
     */

    public function delete($id) {
        $delete = $this->db->delete('users', array('id' => $id));
        return $delete ? true : false;
    }

    function getCourseMenuData($slug, $user_id) {

        $this->db->select('courses.id');
        $this->db->from('courses');
        $this->db->where(['courses.slug' => $slug]);
        $course_id = $this->db->get()->row()->id;

        $this->db->select('course_activities.id');
        $this->db->from('course_activities');
        $this->db->where(['course_activities.course_id' => $course_id, 'course_activities.module_id' => 0, 'course_activities.lesson_id' => 0, 'course_activities.contain_id' => 0, 'course_activities.user_id' => $user_id]);
        $courseData['course_status'] = $this->db->get()->row()->id;
        $courseData['quizzes'] = $this->__get_quizz(0, 0, $course_id, $user_id, 4);
        $courseData['assessment'] = $this->__get_assessment(0, 0, $course_id, $user_id, 4);

        $this->db->select('modules.id, modules.name, modules.long_title, modules.slug, moduleactivity.id as module_status');
        $this->db->from('course_contain');
        $this->db->join('modules', 'course_contain.module_id = modules.id AND modules.status = 1 AND modules.delete_status = 1');
        $this->db->join('course_activities as moduleactivity', 'moduleactivity.lesson_id = 0 AND moduleactivity.contain_id = 0 AND  moduleactivity.course_id = "' . $course_id . '" AND moduleactivity.module_id = modules.id AND moduleactivity.user_id = ' . $user_id, 'left');

        $this->db->where(['course_contain.course_id' => $course_id, 'course_contain.quiz_type' => 0, 'course_contain.contain_type' => 0]);
        $this->db->order_by("course_contain.order_id", "asc");
        
        foreach ($this->db->get()->result_array() as $items) :
            $items['lessons'] = $this->get_lessons($items['id'], $course_id, $user_id);
            $items['quizzes'] = $this->__get_quizz(0, $items['id'], $course_id, $user_id, 3);
            $items['assessment'] = $this->__get_assessment(0, $items['id'], $course_id, $user_id, 3);
            $courseData['modules'][] = $items;
        endforeach;
        //pr($courseData); exit;
        return $courseData;
    }

    private function __get_assessment($lessonID = 0, $moduleID = 0, $courseID, $user_id, $type = 0) {
        $this->db->select('assessments.id, assessments.title');
       
        if($courseID > 0 && $moduleID > 0 && $lessonID > 0){
            $this->db->from('lesson_contain');
            $this->db->join('assessments', 'lesson_contain.quiz_id = assessments.id AND assessments.status = 1 AND assessments.delete_status = 1', 'left');
            $this->db->join('gradebooks_evaluation', 'gradebooks_evaluation.reference_id = assessments.id AND gradebooks_evaluation.user_id = "' . $user_id . '" AND gradebooks_evaluation.course_id = "' . $courseID . '" AND gradebooks_evaluation.reference_type = 2', 'left');
            $this->db->where(['lesson_contain.module_id' => $moduleID, 'lesson_contain.course_id' => $courseID, 'lesson_contain.lesson_id' => $lessonID, 'lesson_contain.quiz_type' => $type, 'lesson_contain.contain_type' => 1]);    
        }

        if($courseID > 0 && $moduleID > 0 && $lessonID == 0){
            $this->db->from('module_contain');
            $this->db->join('assessments', 'module_contain.quiz_id = assessments.id AND assessments.status = 1 AND assessments.delete_status = 1', 'left');
            $this->db->join('gradebooks_evaluation', 'gradebooks_evaluation.reference_id = assessments.id AND gradebooks_evaluation.user_id = "' . $user_id . '" AND gradebooks_evaluation.course_id = "' . $courseID . '" AND gradebooks_evaluation.reference_type = 2', 'left');
            $this->db->where(['module_contain.module_id' => $moduleID, 'module_contain.course_id' => $courseID, 'module_contain.lesson_id' => NULL, 'module_contain.quiz_type' => $type, 'module_contain.contain_type' => 1]);    
        }
        
        if($courseID > 0 && $moduleID == 0 && $lessonID == 0){
            $this->db->from('course_contain');
            $this->db->join('assessments', 'course_contain.quiz_id = assessments.id AND assessments.status = 1 AND assessments.delete_status = 1', 'left');
            $this->db->join('gradebooks_evaluation', 'gradebooks_evaluation.reference_id = assessments.id AND gradebooks_evaluation.user_id = "' . $user_id . '" AND gradebooks_evaluation.course_id = "' . $courseID . '" AND gradebooks_evaluation.reference_type = 2', 'left');
            $this->db->where(['course_contain.module_id' => NULL, 'course_contain.course_id' => $courseID, 'course_contain.quiz_type' => $type, 'course_contain.contain_type' => 1]);    
       }
       
        $this->db->group_by('assessments.id');
        return $this->db->get()->result_array();
    }

    private function __get_quizz($lessonID = 0, $moduleID = 0, $courseID, $user_id, $type = 0) {
        $this->db->select('quizzes.id, quizzes.title,quizzes.instruction,IF ((SELECT COUNT(reference_id) from gradebooks_evaluation where user_id="' . $user_id . '" AND course_id="' . $courseID . '" AND reference_type = 1) , "1", "0") as quiz_status');
        $this->db->from('quizzes');
        if($courseID > 0 && $moduleID > 0 && $lessonID > 0){
            $this->db->join('lesson_contain', 'lesson_contain.quiz_id = quizzes.id AND lesson_contain.course_id = '.$courseID.' AND quizzes.status = 1 AND quizzes.delete_status = 1', 'left');
            $this->db->join('gradebooks_evaluation', 'gradebooks_evaluation.reference_id = quizzes.id AND gradebooks_evaluation.user_id = "' . $user_id . '" AND gradebooks_evaluation.course_id = "' . $courseID . '" AND gradebooks_evaluation.reference_type = 1', 'left');
            $this->db->where(['lesson_contain.module_id' => $moduleID, 'lesson_contain.course_id' => $courseID, 'lesson_contain.lesson_id' => $lessonID, 'lesson_contain.quiz_type' => $type, 'lesson_contain.contain_type' => 0]);
        }

        if($courseID > 0 && $moduleID > 0 && $lessonID == 0){
            $this->db->join('module_contain', 'module_contain.quiz_id = quizzes.id AND module_contain.course_id = '.$courseID.' AND quizzes.status = 1 AND quizzes.delete_status = 1', 'left');
            $this->db->join('gradebooks_evaluation', 'gradebooks_evaluation.reference_id = quizzes.id AND gradebooks_evaluation.user_id = "' . $user_id . '" AND gradebooks_evaluation.course_id = "' . $courseID . '" AND gradebooks_evaluation.reference_type = 1', 'left');
            $this->db->where(['module_contain.module_id' => $moduleID, 'module_contain.course_id' => $courseID, 'module_contain.lesson_id' => NULL, 'module_contain.quiz_type' => $type, 'module_contain.contain_type' => 0]);
        }
        
        if($courseID > 0 && $moduleID == 0 && $lessonID == 0){
            $this->db->join('course_contain', 'course_contain.quiz_id = quizzes.id AND course_contain.course_id = '.$courseID.' AND quizzes.status = 1 AND quizzes.delete_status = 1', 'left');
            $this->db->join('gradebooks_evaluation', 'gradebooks_evaluation.reference_id = quizzes.id AND gradebooks_evaluation.user_id = "' . $user_id . '" AND gradebooks_evaluation.course_id = "' . $courseID . '" AND gradebooks_evaluation.reference_type = 1', 'left');
            $this->db->where(['course_contain.module_id' => NULL, 'course_contain.course_id' => $courseID, 'course_contain.quiz_type' => $type, 'course_contain.contain_type' => 0]);
        }

        $this->db->group_by('quizzes.id');
        return $this->db->get()->result_array();
    }

    function get_lessons($module_id = 0, $course_id, $user_id = 0) {
        $this->db->select('DISTINCT(lessons.id), lessons.name, lessons.long_title, lessons.slug, lessonactivities.id as lesson_status');
        $this->db->from('module_contain');
        $this->db->join('lessons', 'module_contain.lesson_id = lessons.id AND lessons.status = 1 AND lessons.delete_status = 1');
        $this->db->join('course_activities as lessonactivities', 'lessonactivities.course_id = ' . $course_id . ' AND lessonactivities.module_id =' . $module_id . ' AND lessonactivities.lesson_id = lessons.id AND lessonactivities.contain_id = 0 AND lessonactivities.user_id = ' . $user_id, 'left');
        $this->db->where(['module_contain.module_id' => $module_id]);
        $this->db->where(['module_contain.course_id' => $course_id]);
        $this->db->order_by("module_contain.order_id", "asc");
        
        foreach ($this->db->get()->result_array() as $items) :
            $items['quizzes'] = $this->__get_quizz($items['id'], $module_id, $course_id, $user_id);
            $items['assessment'] = $this->__get_assessment($items['id'],$module_id, $course_id, $user_id, 2);
            $lessonData[] = $items;
        endforeach;
        
        return $lessonData;
    }

    function get_courses($user_id = 0, $containerId = 0,  $instructor = false) {
        $today = date('Y-m-d');
        $this->db->select('courses.id, courses.slug, courses.name,courses.introduction, courses.long_title, courses.code, courses.course_img, course_sections.name as section_name, course_sections.id as section_id ,course_sections.slug as section_slug');
        $this->db->from('course_assigned_to_users');
        if($instructor){
            $this->db->join('courses', 'course_assigned_to_users.course_id = courses.id AND course_assigned_to_users.is_instructor = 1');
        }else{
            $this->db->join('courses', 'course_assigned_to_users.course_id = courses.id');
        }
        $this->db->join('course_sections', 'course_assigned_to_users.section_id = course_sections.id', 'left');

        $this->db->where(['course_assigned_to_users.user_id' => $user_id, 'course_assigned_to_users.container_id' => $containerId, 'courses.status' => 1, 'courses.delete_status' => 1]);
        if($instructor == false){
            $this->db->where(['course_sections.start_date <= ' => $today, 'course_sections.end_date >= ' => $today]);
        }
        $this->db->group_by('courses.id');
        $this->db->order_by("course_assigned_to_users.id", "desc");

        return $this->db->get()->result();
    }
  

    function checkCourseSectionExpiry($courseId, $sectionId) { 
        $today = date('Y-m-d');
        $this->db->select('courses.id , courses.name , courses.slug');
        $this->db->from('courses');
        $this->db->join('course_sections', 'courses.id = course_sections.course_id');
        $this->db->where(['course_sections.id' => $sectionId, 'courses.id' => $courseId, 'course_sections.start_date <=' => $today, 'course_sections.end_date >=' => $today]);
        return $this->db->get()->row();
    }

    function get_course($user_id, $courseSlug, $sectionSlug) {
        $today = date('Y-m-d');
        $this->db->select('courses.id, courses.description,courses.introduction,courses.outcomes,courses.getting_started,courses.slug, courses.name, course_sections.end_date as course_section_end_date,courses.long_title, courses.code, courses.course_img, course_sections.id as section_id,course_sections.name as section_name, course_sections.slug as section_slug');
        $this->db->from('course_assigned_to_users');
        $this->db->join('courses', 'course_assigned_to_users.course_id = courses.id');
        $this->db->join('course_sections', 'course_assigned_to_users.section_id = course_sections.id', 'left');
        $this->db->where(['course_assigned_to_users.user_id' => $user_id, 'courses.slug' => $courseSlug, 'course_sections.slug' => $sectionSlug, 'course_sections.start_date <= ' => $today, 'course_sections.end_date >= ' => $today]);
        return $this->db->get()->row();
    }

    function get_lesson($request) {
        
        $sql = "SELECT course_id, courses.slug as coursesSlug, module_id, modules.slug as modulesSlug, lesson_id, order_id, lessons.*, previous_lesson.slug as previous_lesson_slug, next_lesson.slug as next_lesson_slug
        FROM module_contain mc1
        left join lessons on lessons.id = mc1.lesson_id
        left join lessons as previous_lesson on previous_lesson.id = (SELECT lesson_id FROM module_contain mc2
               WHERE mc2.course_id = courses.id AND mc2.module_id = modules.id AND mc2.lesson_id > 0 AND mc2.order_id < mc1.order_id
               ORDER BY order_id DESC LIMIT 1)
        left join lessons as next_lesson on next_lesson.id = (SELECT lesson_id FROM module_contain mc3
               WHERE mc3.course_id = courses.id AND mc3.module_id = modules.id AND mc3.lesson_id > 0 AND mc3.order_id > mc1.order_id
               ORDER BY order_id ASC LIMIT 1)
        left join modules on modules.id = mc1.module_id
        left join courses on courses.id = mc1.course_id      
        WHERE courses.slug = ? AND modules.slug = ? AND lessons.slug = ? ";
        
        
        
        $query = $this->db->query($sql, array($request->courseSlug, $request->moduleSlug, $request->lessonSlug));
        
        
    // echo $this->db->last_query();
        
        return $query->row();
    }

    public function get_public_all_courses() {
        $this->db->select('courses.id, courses.slug, courses.name,courses.long_title, courses.code,courses.course_img, ,courses.exp_status, courses.course_expiry, courses.created, course_sections.id as section_id ,course_sections.slug as section_slug');
        $this->db->from('courses');
        // $this->db->join('courses', 'course_assigned_to_users.course_id = courses.id');
        $this->db->join('course_sections', 'courses.id = course_sections.course_id', 'left');
        $this->db->where(['courses.status' => 1, 'courses.delete_status' => 1]);
        $this->db->group_by('courses.id');
        $this->db->order_by("courses.name", "desc");
        return $this->db->get()->result();
    }

}
