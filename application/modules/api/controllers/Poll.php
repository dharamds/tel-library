<?php

include('headers.php');
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//include Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class Poll extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->config('ion_auth');
        $this->lang->load('auth');
        //load models
        $this->load->model('Common_model');

        // cache settings
        $this->config->set_item('cache_path', APPPATH . '/cache/api/polls');
        $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'tel_')
        );
    }

    public function index_post() {

        $request = json_decode(file_get_contents('php://input'));
        switch ($request->method) {
            case "getPoll":
                $this->getPoll($request);
                break;
            case "get_poll_result":
                $this->get_poll_result($request);
                break;
            case "savePollResult":
                $this->savePollResult($request);
                break;
            case "getPollResultCount":
                $this->getPollResultCount($request);
                break;
            case "ifPollSubmitted":
                $this->ifPollSubmitted($request);
                break;
        }
    }

    public function getPoll($request) {

     //   $cache_key = 'getPoll_' . $request->poll_id;
      //  $data = $this->cache->get($cache_key);

        //if (!$data) {
            $data = $this->Common_model->getDataById('polls', '*', array('id' => $request->poll_id));
            // echo  $this->db->last_query();
            $options = $this->Common_model->getAllData('polls_options', '*', '', array('poll_id' => $request->poll_id));
            $data->opts = $options;
             if ($data) {
                // save cache
              //  $this->cache->save($cache_key, $data, CACHE_LIFE_TIME);
            }
            
            if (!empty($data->opts)) {
                $optionStatus = 1;
            } else {
                $optionStatus = 0;
            }
            
           // pr($optionStatus); exit('kk');
            
            

           
      //  }

        if (!empty($data)) {
            $this->response([
                'status' => true,
                'optionStatus' => $optionStatus,
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => false,
                'optionStatus' => $optionStatus,
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function savePollResult($request) {

        $post_data['poll_id'] = $request->poll_id;
        $post_data['user_id'] = $request->user_id;
        $post_data['poll_option_id'] = $request->option;

        if (!empty($this->Common_model->InsertData('polls_result', $post_data))) {
            $this->response([
                'status' => TRUE,
                'message' => 'Data Save successfully'
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Something went wrong!!!'
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function getPollResultCount($request) {

       // $cache_key = 'getPollResultCount_' . $request->poll_id;
       // $data = $this->cache->get($cache_key);


        //if (!$data) {
            $fields = 'count(polls_result.poll_option_id) as total,polls_result.poll_option_id,polls_options.options,polls.question';
            $group = 'polls_result.poll_option_id';
            $conditions = 'polls_result.poll_id=' . $request->poll_id;

            $this->db->select($fields);
            $this->db->from('polls_result');
            $this->db->where($conditions);
            $this->db->join('polls_options', 'polls_result.poll_option_id = polls_options.id');
            $this->db->join('polls', 'polls_result.poll_id = polls.id');
            $this->db->group_by($group);
            $query = $this->db->get();
            $data = $query->result();

            $total = array_sum(array_column($data, 'total'));

            if ($data) {
                // save cache
              //  $this->cache->save($cache_key, $data, CACHE_LIFE_TIME);
            }
    //    }

        if (!empty($data)) {
            foreach ($data as $key => $value) {
                //pr($value);exit;
                $per = ($value->total * 100) / $total;

                $data[$key]->percent = $per;
            }
        }

        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function ifPollSubmitted($request) {
      //  $cache_key = 'ifPollSubmitted_' . $request->poll_id;
      //  $data = $this->cache->get($cache_key);
        
      //  if(!$data){
        $data = $this->Common_model->getDataById('polls_result', '*', array('poll_id' => $request->poll_id, 'user_id' => $request->user_id));  
          if ($data) {
                // save cache
               // $this->cache->save($cache_key, $data, CACHE_LIFE_TIME);
            }
     //   }
        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
               // 'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
               // 'data' => $data
                    ], REST_Controller::HTTP_OK);
        }
    }

}
