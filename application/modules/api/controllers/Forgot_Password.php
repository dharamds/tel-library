<?php
include('headers.php');
if (!defined('BASEPATH')) exit('No direct script access allowed');
//include Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class Forgot_Password extends MY_Controller
{

	public function __construct()
	{

		parent::__construct();
		//$this->load->module('template');
		$this->load->model('common_model');
		//$this->load->model('Assignment_model');
		$this->load->library('form_validation');
		$this->load->helper(array('html', 'language', 'form', 'country_helper'));
		if (!$this->ion_auth->logged_in()) :
			redirect('users/auth', 'refresh');
		endif;

		if (!$this->ion_auth->is_admin()) :
			return show_error("You Must Be An Administrator To View This Page");
		endif;
	}

	public function reset_password_API()
	{

		$data['email'] = $this->uri->segment(4);
		$user_exist=$this->common_model->getDataById('users','id',array("username"=>base64_decode(urldecode($this->uri->segment(4)))));
		if(empty($user_exist)) {
			echo "Invalid Request"; exit;
		}
		$data['page'] = "api/forgot_password";
		$this->_render_page('api/forgot_password', $data);
		//$this->template->template_view($data);
	}

	public function _render_page($view, $data = null, $returnhtml = false) //I think this makes more sense
	{
		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);

		if ($returnhtml) {
			return $view_html;
		} //This will return html on 3rd argument being true
	}

	public function reset_password_functionality()
	{
		$this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
		$this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');
		if ($this->form_validation->run() == FALSE) {
			$ret = [
				[
					'status' => TRUE,
					'message' => validation_errors()
				]
			  ];
			echo json_encode($ret);
		} else {
			$username = base64_decode(urldecode(post('decryption')));
			$password = post('new_confirm');
			$this->common_model->UpdateDB('users', array("username" => $username), array("password" => $this->ion_auth->hash_password($password)));
			$ret = [
				[
					'status' => TRUE,
					'message' => 'Password updated successfully'
				]
			  ];
			echo json_encode($ret);
		}
	}
}
