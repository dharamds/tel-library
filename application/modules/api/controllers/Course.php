<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
include('headers.php');
//include Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class Course extends REST_Controller {

    public function __construct() {
        parent::__construct();

        ini_set('display_errors', 0);
        $this->load->config('ion_auth');
        $this->lang->load('auth');
        //load models
        $this->load->model('Common_model');
        $this->load->model('courses/Courses_modal');
        $this->load->model('Ion_auth_model');
        //load helper
        $this->load->helper('jwt');
        //load libraries
        $this->load->library('form_validation');
        //JWT AUTHENTICATION
        $data = json_decode(file_get_contents('php://input'));

        $db_token = $this->Common_model->getDataById('users', 'jwt_token', array("id" => $data->id));
        $request_token = $this->input->request_headers();

        if ($request_token['token'] == '' || $db_token->jwt_token != $request_token['token']) {
            $this->response([
                'status' => FALSE,
                'jwt_status' => FALSE,
                'message' => 'Invalid token.',
                    ], REST_Controller::HTTP_OK);
        }
        // cache settings
        $this->config->set_item('cache_path', APPPATH . '/cache/api/');
        $this->load->driver(
                'cache', array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'tel_')
        );
    }

    public function index_post() {
        $request = json_decode(file_get_contents('php://input'));
        switch ($request->method) {
            case "getAllCourses":
                $this->getAllCourses($request);
                break;
            case "courseAssignListbyUserID":
                $this->courseAssignListbyUserID($request);
                break;
            case "courseDetailsBySlugs":
                $this->courseDetailsBySlugs($request);
                break;
            case "courseDetailsBySlugs1":
                $this->courseDetailsBySlugs1($request);
                break;
            case "makefavoriteCourse":
                $this->makefavoriteCourse($request);
                break;
            case "checkFavCourseByUserId":
                $this->checkFavCourseByUserId($request);
                break;
            case "myFavoriteCourseListByUserId":
                $this->myFavoriteCourseListByUserId($request);
                break;
            case "notificationsByUserId":
                $this->notificationsByUserId($request);
                break;

            case "courseActivityCompletionMethod":
                $this->courseActivityCompletionMethod($request);
                break;
            case "getLessonByLessonSlug":
                $this->getLessonByLessonSlug($request);
                break;
            case "getLessonByLessonSlug1":
                $this->getLessonByLessonSlug1($request);
                break;
            case "getUserCourseActivityDetail":
                $this->getUserCourseActivityDetail($request);
                break;
            case "getUserCourseCompletionValue":
                $this->getUserCourseCompletionValue($request);
                break;
            case "getModuleListByCourseSlug":
                $this->getModuleListByCourseSlug($request);
                break;
            case "getStartLessonByModule":
                $this->getStartLessonByModule($request);
                break;
            case "getLessonOrders":
                $this->getLessonOrders($request);
                break;
            case "getGlossaryByLesson":
                $this->getGlossaryByLesson($request);
                break;
            case "courseAssignListbyInstructorID":
                $this->courseAssignListbyInstructorID($request);
                break;    
        }
    }

    public function courseAssignListbyInstructorID($request) {
        $this->load->model('Course_Model');
        $data = $this->Course_Model->get_courses($request->id, $request->container_id, true);
        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'message' => 'Data found.',
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                'message' => 'No data found.'
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function getAllCourses() {
        // cache settings
        $cache_key = 'getAllCourses';
        $data = $this->cache->get($cache_key);
        if (!$data) {
            $data = $this->Common_model->getAllData('courses', '*', '', array("delete_status" => 1));
            if ($data) {
                // save cache
                $this->cache->save($cache_key, $data, CACHE_LIFE_TIME);
            }
        }

        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'message' => 'Data found.',
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                'message' => 'No data found.'
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function checkFavCourseByUserId($request) {
        $select = $this->Common_model->getDataById('favorites', '*', array("course_id" => $request->course_id, "user_id" => $request->id));
        // echo    $this->db->last_query();
        if (!empty($select)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'fav_status' => TRUE,
                'message' => 'course is favorite.',
                    ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                'fav_status' => FALSE,
                'message' => 'course is not favorite.',
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function courseAssignListbyUserID($request) {
        $this->load->model('Course_Model');
        // $cache_key = 'courseAssignListbyUserID_' . $request->id;
        // $data = $this->cache->get($cache_key);
        // if(!$data){
        $data = $this->Course_Model->get_courses($request->id, $request->container_id);
        foreach ($data as $key => $dat) {
            $data[$key]->percent = (int) $this->Common_model->getCountProgressBar($request->id, $dat->id);
        }

        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'message' => 'Data found.',
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                'message' => 'No data found.'
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function courseDetailsBySlugs($request) {
        $this->load->model('Course_Model');
        $cache_key = 'courseDetailsBySlugs_' . $request->courseSlug . $request->id . $request->sectionSlug;
        $data = $this->cache->get($cache_key);
        if (!$data) {
            if ($data = $this->Course_Model->get_course($request->id, $request->courseSlug, $request->sectionSlug)) {

                $data->documents = $this->Common_model->getAllData('documents', 'documents.title, documents.filename', false, array("documents.reference_id" => $data->id, "documents.reference_type" => 1, "documents.delete_status" => 1));


                $courseId = $this->Common_model->getAllData('courses', 'id', true, array("slug" => trim($request->courseSlug)))->id;
                $sectionId = $this->Common_model->getAllData('course_sections', 'id', true, array("slug" => trim($request->sectionSlug)))->id;


                $data->assessment_data = $this->db->query("SELECT cc.quiz_id, a.title,a.instruction, if(count(ge_quiz.id) > 0,'1','0') as is_completed from course_contain as cc JOIN assessments as a ON (cc.quiz_id = a.id) LEFT JOIN gradebooks_evaluation as ge_quiz ON(ge_quiz.reference_id = cc.quiz_id AND ge_quiz.user_id = '" . $request->id . "' AND ge_quiz.course_id = '" . $courseId . "' AND ge_quiz.reference_type = 2) WHERE cc.course_id='" . $courseId . "' AND cc.contain_type=1 AND cc.quiz_type=4 GROUP BY cc.quiz_id ")->result();

                $data->quiz_data = $this->db->query("SELECT cc.quiz_id, q.title,q.instruction,q.number_attempt,q.view_result_btn,if(count(ge_quiz.id) > 0,'1','0') as is_completed, if(q.number_attempt = 0 OR q.number_attempt > COALESCE(ca.contain_attempt, 0), 0,1) as quiz_attempt_allow from course_contain as cc JOIN quizzes as q ON (cc.quiz_id = q.id) LEFT JOIN gradebooks_evaluation as ge_quiz ON(ge_quiz.reference_id = cc.quiz_id AND ge_quiz.user_id = '" . $request->user_id . "' AND ge_quiz.course_id = '" . $courseId . "' AND ge_quiz.reference_type = 1) 
                
                LEFT JOIN course_activities as ca ON ( ca.contain_id = q.id AND ca.contain_type = 1 AND ca.course_id = '".$courseId."' AND ca.section_id = '".$sectionId."' AND ca.user_id = '".$request->id."'  )
                
                 WHERE cc.course_id='" . $courseId . "' AND cc.contain_type=0 AND cc.quiz_type=4 GROUP BY cc.quiz_id ")->result();

                $this->cache->save($cache_key, $data, CACHE_LIFE_TIME);
            }
        }


        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'message' => 'Data found.',
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                'message' => 'No data found.'
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function courseDetailsBySlugs1($request) {
        $this->load->model('Course_Model');
        $cache_key = 'courseDetailsBySlugs_' . $request->id . $request->sectionSlug . $request->slug;
        $data = $this->cache->get($cache_key);

        if (!$data) {
            if ($data = $this->Course_Model->get_course($request->id, $request->courseSlug, $request->sectionSlug)) {
                $this->cache->save($cache_key, $data, CACHE_LIFE_TIME);
                $course_data = $this->Common_model->getDataById('courses', '*', array("slug" => $request->slug));
                $data['quiz_data'] = $this->db->query("SELECT cc.quiz_id,q.title from course_contain as cc JOIN quizzes as q WHERE cc.course_id='" . $course_data->course_id . "' AND lc.module_id='NULL' AND lc.contain_type=0 AND lc.quiz_type=3 ")->result();

                $data['assessment_data'] = $this->db->query("SELECT cc.quiz_id,q.title from course_contain as cc JOIN assessments as q WHERE cc.course_id='" . $course_data->course_id . "' AND lc.module_id='NULL' AND lc.contain_type=1 AND lc.quiz_type=4 ")->result();
                //array_push($data,"blue","yellow");
            }
        }
        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'message' => 'Data found.',
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                'message' => 'No data found.'
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function notificationsByUserId($request) {
        $select = $this->Common_model->getAllData('notifications', '*', '', array("receiver_id" => $request->id));
        if (!empty($select)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'list' => $select
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function courseActivityCompletionMethod($request) {
        $data = [];
        $courseId = $this->Common_model->getAllData('courses', 'id', true, array("slug" => trim($request->course_slug)))->id;
        $moduleId = $this->Common_model->getAllData('modules', 'id', true, array("slug" => trim($request->module_slug)))->id;
        $lessonId = $this->Common_model->getAllData('lessons', 'id', true, array("slug" => trim($request->lesson_slug)))->id;
        $sectionId = $this->Common_model->getAllData('course_sections', 'id', true, ["slug" => trim($request->section_slug), "status" => 1, "delete_status" => 1, "course_id" => $courseId])->id;
        
        $data['section_id'] = $sectionId;
        $data['created'] = date('Y-m-d h:i:sa');

        if ($courseId && $moduleId && $lessonId && !$request->contain_id && !$request->contain_type) {
            $data['course_id'] = $courseId;
            $data['user_id'] = $request->id;
            $data['module_id'] = $moduleId;
            $data['lesson_id'] = $lessonId;
            $data['contain_id'] = 0;
            $data['contain_type'] = 0;
        } elseif ($courseId && $moduleId && !$lessonId && !$request->contain_id && !$request->contain_type) {
            $data['course_id'] = $courseId;
            $data['user_id'] = $request->id;
            $data['module_id'] = $moduleId;
            $data['lesson_id'] = 0;
        } elseif ($courseId && !$moduleId && !$lessonId && !$request->contain_id && !$request->contain_type) {
            $data['course_id'] = $courseId;
            $data['user_id'] = $request->id;
            $data['module_id'] = 0;
            $data['lesson_id'] = 0;
        } elseif ($courseId && $moduleId && $lessonId && $request->contain_id && $request->contain_type) {
            $data['course_id'] = $courseId;
            $data['user_id'] = $request->id;
            $data['module_id'] = $moduleId;
            $data['lesson_id'] = $lessonId;

            $data['contain_id'] = $request->contain_id;
            $data['contain_type'] = $request->contain_type;

            if (isset($request->contain_answer) && !empty($request->contain_answer)) {
                $data['contain_answer'] = $request->contain_answer;
            }
        }
        $checkEntry = $this->Common_model->getAllData('course_activities', 'id', true, $data);

        if ($request->contain_type == 3) {
            $pollData['poll_id'] = $request->contain_id;
            $pollData['user_id'] = $request->id;
            $pollData['poll_option_id'] = $request->contain_answer;
            $this->Common_model->InsertData('polls_result', $pollData);
        }
        if (!empty($checkEntry)) {
            $this->response([
                'status' => false,
                'jwt_status' => TRUE,
                'message' => 'Activity has been already completed'
                    ], REST_Controller::HTTP_OK);
        } else {

            $select = $this->Common_model->InsertData('course_activities', $data);
            if ($request->contain_id && $request->contain_type) {
                
            } else {
                // Check all lessons has been completed or not
                $assosiateLessons = $this->Common_model->getAllData('module_contain', 'lesson_id', false, ['course_id' => $courseId, 'module_id' => $moduleId, 'lesson_id !=' => NULL], 'module_contain.order_id asc');

                $allassosiateLessonsCompleted = 0;
                foreach ($assosiateLessons as $lessons) {
                    $dataParam['course_id'] = $courseId;
                    $dataParam['section_id']    = $sectionId;
                    $dataParam['user_id'] = $request->id;
                    $dataParam['module_id'] = $moduleId;
                    $dataParam['lesson_id'] = $lessons->lesson_id;
                    if ($this->Common_model->getAllData('course_activities', 'id', true, $dataParam)->id) {
                        $allassosiateLessonsCompleted++;
                    }
                }
                // Record activity for module completion
                if (count($assosiateLessons) == $allassosiateLessonsCompleted) {
                    $completedModule = $moduleId;
                    $moduleParam['course_id'] = $courseId;
                    $moduleParam['section_id'] = $sectionId;
                    $moduleParam['user_id'] = $request->id;
                    $moduleParam['module_id'] = $moduleId;
                    $moduleParam['lesson_id'] = 0;
                    $moduleParam['contain_id'] = 0;
                    if (!$this->Common_model->getAllData('course_activities', 'id', true, $moduleParam)) {
                        $this->Common_model->InsertData('course_activities', $moduleParam);

                        // Check all modules has been completed or not
                        $assosiateModules = $this->Common_model->getAllData('course_contain', 'module_id', false, ['course_id' => $courseId, 'module_id !=' => NULL]);
                        $allassosiateModulesCompleted = 0;

                        foreach ($assosiateModules as $modules) {

                            $dataParam['course_id'] = $courseId;
                            $dataParam['section_id'] = $sectionId;
                            $dataParam['user_id'] = $request->id;
                            $dataParam['module_id'] = $modules->module_id;
                            $dataParam['lesson_id'] = 0;
                            $dataParam['contain_id'] = 0;
                            if ($this->Common_model->getAllData('course_activities', 'id', true, $dataParam)->id) {
                                $allassosiateModulesCompleted++;
                            }
                        }
                       if (count($assosiateModules) == $allassosiateModulesCompleted) {
                            $courseParam['course_id'] = $courseId;
                            $courseParam['section_id'] = $sectionId;
                            $courseParam['user_id'] = $request->id;
                            $courseParam['module_id'] = 0;
                            $courseParam['lesson_id'] = 0;
                            $courseParam['contain_id'] = 0;
                            if (!$this->Common_model->getAllData('course_activities', 'id', true, $courseParam)) {
                                $this->Common_model->InsertData('course_activities', $courseParam);
                            }
                        }
                    }
                }
            }

            if (!empty($select)) {
                $this->response([
                    'status' => TRUE,
                    'jwt_status' => TRUE,
                    'lesson_id' => $lessonId ,
                    'module_id' => $completedModule,
                    'message' => 'Activity saved successfully'
                        ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => FALSE,
                    'jwt_status' => TRUE,
                    'message' => 'Activity could not completed'
                        ], REST_Controller::HTTP_OK);
            }
        }
    }

    //$this->__getSaveQuizResult($request,$courseId,$checkEntry);
   /* private function __getCheckQuizResult($postData = [], $courseId, $checkEntry, $sectionId) {
       
        if ($postData->quize_type == 1) {
            $conditions = [
                'user_id' => $postData->id,
                'course_id' => $courseId,
                'section_id' => $sectionId,
                'reference_id' => $postData->reference_id
            ];
            $this->Common_model->DeleteDB('gradebooks_evaluation', $conditions);
            $this->__getSaveQuizResult($postData, $courseId, $sectionId);
            $gradebook_quiz_assess_grade_condition = [
                'user_id' => $postData->id,
                'course_id' => $courseId,
                'section_id' => $sectionId,
                'quiz_assess_id' => $postData->reference_id
            ];
            $this->Common_model->DeleteDB('gradebook_quiz_assess_grade', $gradebook_quiz_assess_grade_condition);
            $this->__getSaveGrading($postData, $courseId, $sectionId);
        } else {
            if (empty($checkEntry)) {
                $this->__getSaveQuizResult($postData, $courseId, $sectionId);
                $this->__getSaveGrading($postData, $courseId, $sectionId);
            }
        }
    }*/

    /* Start gradding code */

    /*public function __getSaveGrading($postData = [], $courseId, $sectionId) {
        // pr($postData);exit;
        $quizWait = $this->__getQuizWait($postData->reference_id);
        $totalGradePoints = $this->__getQuestionsWait($postData->quizFormData, $quizWait);

        $saveGradeData = [
            'user_id' => $postData->id,
            'course_id' => $courseId,
            'section_id' => $sectionId,
            'quiz_assess_id' => $postData->reference_id,
            'quiz_assess_type' => $postData->reference_type,
            'grade' => $totalGradePoints,
            'created' => date('Y-m-d h:i:sa')
        ];
        if ($saveGradeData) {
            $this->Common_model->InsertData('gradebook_quiz_assess_grade', $saveGradeData);
        }
    }*/

    /*private function __getQuizWait($quizzID) {
        if ($total_points = $this->Common_model->getAllData('quizzes', 'total_points', true, ["id" => $quizzID, "status" => 1, "delete_status" => 1])->total_points) {
            return $total_points;
        } else {
            return false;
        }
    }

    private function __getQuestionsWait($questions = [], $quizzWait) {
        $answers = [];
        foreach ($questions as $key => $value) {
            $answers[] = $value->optionId;
        }
        $this->load->model('Question_model');
        $answerResponse = $this->Question_model->getOptionByQuestion($answers);
        $grade = 0;
        foreach ($answerResponse as $key => $value) {
            if ($value['weight'] != '') {
                $grade += ($quizzWait * $value['weight']) / 100;
            }
        }
        return $grade;
    }*/

    /* End gradding code */

    /*private function __getSaveQuizResult($postData = [], $courseId, $sectionId) {
        $gradingEvalutionData = [];
        foreach ($postData->quizFormData as $key => $value) {
            $gradingEvalutionData[] = [
                'user_id' => $postData->id,
                'course_id' => $courseId,
                'section_id' => $sectionId,
                'reference_id' => $postData->reference_id,
                'reference_type' => $postData->reference_type,
                'quize_type' => $postData->quize_type,
                'question_id' => $value->qId,
                'answered_id' => $value->optionId,
                'correct' => $value->correct
            ];
        }
        if ($gradingEvalutionData) {
            $this->db->insert_batch('gradebooks_evaluation', $gradingEvalutionData);
        }
    }*/

    public function getUserCourseActivityDetail($request) {
        //        $cache_key = 'getUserCourseActivityDetail_' . $request->slug . "_" . $request->id;
        //        $data = $this->cache->get($cache_key);
        //  if (!$data) {
        $this->load->model('Course_Model');
        $data = $this->Course_Model->getCourseMenuData($request->slug, $request->id);
        if ($data) {
            // save cache
            //  $this->cache->save($cache_key, $data, CACHE_LIFE_TIME);
        }
        // }
        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function checkIfQuizSubmitted($data = array()) {
        $checkEntry = $this->Common_model->getAllData('gradebooks_evaluation', 'id', '', $data);
        if (!empty($checkEntry)) {
            return true;
        } else {
            return false;
        }
    }

    public function getLessonByLessonSlug_old($request) {

        $sectionId = $this->getSlugId('course_sections', $request->sectionSlug);

        $cache_key = 'getLessonByLessonSlug_' . $request->courseSlug . "_" . $request->moduleSlug . "_" . $request->lessonSlug . "_" . $request->id;
        $lessonData = $this->cache->get($cache_key);
        
        if (!$lessonData) {
            $this->load->model('Course_Model');
            $lessonData = $this->Course_Model->get_lesson($request);
            $checkEntry = $this->Common_model->getAllData('course_activities', 'id', true, array(
                'course_id' => $lessonData->course_id, 'module_id' => $lessonData->module_id,
                'lesson_id' => $lessonData->lesson_id, 'contain_id' => 0, 'user_id' => $request->id
            ));

            if (!empty($checkEntry)) {
                $lessonData->isCompleted = true;
            } else {
                $lessonData->isCompleted = false;
            }
            // default 
            $lessonData->sectionSlug = $request->sectionSlug;
            if ($lessonData) {
                // save cache
                $this->cache->save($cache_key, $lessonData, CACHE_LIFE_TIME);
            }
        }

        $quizPostData = ['user_id' => $request->id, 'course_id' => $lessonData->course_id, 'section_id' => $sectionId, 'reference_id' => $lessonData->quiz_id];
        $quizSubmitted = $this->checkIfQuizSubmitted($quizPostData);
        $lessonData->cyk_submitted = $quizSubmitted ? true : false;

        if (!empty($lessonData)) {
            $this->response([
                'status' => true,
                'message' => 'Data found.',
                'jwt_status' => TRUE,
                'data' => $lessonData
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => false,
                'jwt_status' => TRUE,
                'message' => 'No data found.',
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function getLessonByLessonSlug1($request) {

        $sectionId = $this->getSlugId('course_sections', $request->sectionSlug);

        //        $cache_key = 'getLessonByLessonSlug_' . $request->courseSlug . "_" . $request->moduleSlug . "_" . $request->lessonSlug . "_" . $request->id;
        //        $lessonData = $this->cache->get($cache_key);
        // if (!$lessonData) {
        $this->load->model('Course_Model');
        $lessonData = $this->Course_Model->get_lesson($request);
        $lessonData->watch_video_link = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i", "www.youtube.com/embed/$1", $lessonData->watch_video_link);

        $checkEntry = $this->Common_model->getAllData('course_activities', 'id', true, array(
            'course_id' => $lessonData->course_id, 'module_id' => $lessonData->module_id,
            'lesson_id' => $lessonData->lesson_id, 'contain_id' => 0, 'user_id' => $request->id
        ));

        if (!empty($checkEntry)) {
            $lessonData->isCompleted = true;
        } else {
            $lessonData->isCompleted = false;
        }
        // default 
        $lessonData->sectionSlug = $request->sectionSlug;
        if ($lessonData) {
            // save cache
            //   $this->cache->save($cache_key, $lessonData, CACHE_LIFE_TIME);
        }
        //   }

        $quizPostData = ['user_id' => $request->id, 'course_id' => $lessonData->course_id, 'section_id' => $sectionId, 'reference_id' => $lessonData->quiz_id];
        $quizPostData1 = ['lesson_id' => $lessonData->lesson_id, 'user_id' => $request->id, 'course_id' => $lessonData->course_id, 'section_id' => $sectionId, 'reference_id' => $lessonData->quiz_id, 'module_id' => $lessonData->module_id];

        $quizSubmitted = $this->checkIfQuizSubmitted($quizPostData);
        $lessonData->cyk_submitted = $quizSubmitted ? true : false;
        $lessonData->glossary_data = $this->getGlossaryByLesson($request->id);
        $lessonData->quiz_data = $this->getQuizByLesson($quizPostData1);
        if (!empty($lessonData)) {
            $this->response([
                'status' => true,
                'message' => 'Data found.',
                'jwt_status' => TRUE,
                'data' => $lessonData
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => false,
                'jwt_status' => TRUE,
                'message' => 'No data found.',
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function getLessonByLessonSlug($request) {
       // pr($request);
        /*$cache_key = 'getLessonByLessonSlug_' . $request->courseSlug . "_" . $request->moduleSlug . "_" . $request->lessonSlug;
        $lessonData = $this->cache->get($cache_key);*/

        /*$cache_key = 'courseDetailsBySlugs_' . $request->courseSlug . $request->id . $request->sectionSlug;
        $data = $this->cache->get($cache_key);*/

        //if (!$lessonData) {
            $this->load->model('Course_Model');
          $courseId = $this->Common_model->getAllData('courses', 'id', true, array("slug" => trim($request->courseSlug)))->id; 
            $sectionId = $this->Common_model->getAllData('course_sections', 'id', true, array("slug" => trim($request->sectionSlug)))->id; 
             $sectionExpiry = $this->Course_Model->checkCourseSectionExpiry($courseId, $sectionId);
             
            if (empty($sectionExpiry)) {
                 $this->response([
                        'status' => false,
                        'jwt_status' => TRUE,
                        'message' => 'No data found.',
                            ], REST_Controller::HTTP_OK);
            } else {

                $lessonData = $this->Course_Model->get_lesson($request);
                
                $lessonData->watch_video_link = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i", "www.youtube.com/embed/$1", $lessonData->watch_video_link);
                if (!preg_match('/^http(s)?:\/\/[a-z0-9-]+(\.[a-z0-9-]+)*(:[0-9]+)?(\/.*)?$/i', $lessonData->watch_video_link)) {
                    $lessonData->watch_video_link = ($lessonData->watch_video_link) ? "https://" . $lessonData->watch_video_link : '';
                }

                $checkEntry = $this->Common_model->getAllData('course_activities', 'id', true, array(
                    'course_id' => $lessonData->course_id, 'module_id' => $lessonData->module_id,
                    'lesson_id' => $lessonData->lesson_id, 'contain_id' => 0, 'user_id' => $request->id
                ));

                if (!empty($checkEntry)) {
                    $lessonData->isCompleted = true;
                } else {
                    $lessonData->isCompleted = false;
                }
                // default 
                $lessonData->sectionSlug = $request->sectionSlug;
                $quizPostData = ['user_id' => $request->id, 'course_id' => $lessonData->course_id, 'section_id' => $sectionId, 'reference_id' => $lessonData->quiz_id];
                $quizPostData1 = ['lesson_id' => $lessonData->lesson_id, 'user_id' => $request->id, 'course_id' => $lessonData->course_id, 'section_id' => $sectionId, 'reference_id' => $lessonData->quiz_id, 'module_id' => $lessonData->module_id];

                $quizSubmitted = $this->checkIfQuizSubmitted($quizPostData);
                $lessonData->cyk_submitted = $quizSubmitted ? true : false;
                $lessonData->glossary_data = $this->getGlossaryByLesson($request->id);
                $lessonData->quiz_data = $this->getQuizByLesson($quizPostData1);
                $lessonData->assessment_data = $this->getAssessmentByLesson($quizPostData1);

                if ($lessonData) {
                    // save cache
                  //  $this->cache->save($cache_key, $lessonData, CACHE_LIFE_TIME);
                }
            }

            

            /* $data['assessment_data'] = $this->db->query("SELECT cc.quiz_id,q.title from course_contain as cc JOIN assessments as q WHERE cc.course_id='" . $course_data->course_id . "' AND lc.module_id='NULL' AND lc.contain_type=1 AND lc.quiz_type=4 ")->result(); */
           // }

            if (!empty($lessonData)) {
                $this->response([
                    'status' => true,
                    'message' => 'Data found.',
                    'jwt_status' => TRUE,
                    'data' => $lessonData
                        ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => false,
                    'jwt_status' => TRUE,
                    'message' => 'No data found.',
                        ], REST_Controller::HTTP_OK);
            }
        
    }

    private function getGlossaryByLesson($lesson_id) {
        return $this->db->query("SELECT DISTINCT glossaries.id, glossaries.term, glossaries.long_title, glossaries.definitions from glossaries where glossaries.id IN(SELECT DISTINCT `reference_id` FROM `category_assigned` WHERE  (`reference_type` = " . META_SYSTEM . " and `reference_sub_type` = " . META_GLOSSARY . " AND `category_id` IN( SELECT `category_id` FROM `category_assigned` WHERE `reference_id` = '" . $lesson_id . "' and `reference_type` = " . META_SYSTEM . " and `reference_sub_type` = " . META_SYSTEM_GLOSSARY . ")) OR (`reference_type` = " . META_GLOSSARY . " and `reference_sub_type` = " . META_GLOSSARY . " AND `category_id` IN( SELECT `category_id` FROM `category_assigned` WHERE `reference_id` = '" . $lesson_id . "' and `reference_type` = " . META_GLOSSARY . " and `reference_sub_type` = " . META_SYSTEM_GLOSSARY . "))) OR glossaries.id IN (SELECT DISTINCT `reference_id` FROM `tag_assigned` WHERE  (`reference_type` = " . META_SYSTEM . " and `reference_sub_type` = " . META_GLOSSARY . " AND `tag_id` IN( SELECT `tag_id` FROM `tag_assigned` WHERE `reference_id` = '" . $lesson_id . "' and `reference_type` = " . META_SYSTEM . " and `reference_sub_type` = " . META_SYSTEM_GLOSSARY . ")) OR (`reference_type` = " . META_GLOSSARY . " and `reference_sub_type` = " . META_GLOSSARY . " AND `tag_id` IN( SELECT `tag_id` FROM `tag_assigned` WHERE `reference_id` = '" . $lesson_id . "' and `reference_type` = " . META_GLOSSARY . " and `reference_sub_type` = " . META_SYSTEM_GLOSSARY . "))) AND glossaries.status= 1 and glossaries.delete_status = 1 LIMIT 10")->result();
    }

    private function getQuizByLesson($quizPostData) {

       /*return $this->db->query("SELECT lc.quiz_id,q.view_result_btn,q.title,q.instruction,if(count(ge_quiz.id) > 0,'1','0') as is_completed from lesson_contain as lc JOIN quizzes as q ON (lc.quiz_id = q.id) LEFT JOIN gradebooks_evaluation as ge_quiz ON(ge_quiz.reference_id = lc.quiz_id AND ge_quiz.user_id = '" . $quizPostData['user_id'] . "' AND ge_quiz.course_id = '" . $quizPostData['course_id'] . "' AND ge_quiz.reference_type = 1) WHERE lc.course_id='" . $quizPostData['course_id'] . "' AND lc.module_id='" . $quizPostData['module_id'] . "' AND lc.lesson_id='" . $quizPostData['lesson_id'] . "' AND lc.contain_type=0 AND lc.quiz_type=0 GROUP BY lc.quiz_id  ")->result();*/

       /* return $this->db->query("SELECT lc.quiz_id,q.title from lesson_contain as lc JOIN quizzes as q ON (lc.quiz_id = q.id) WHERE lc.course_id='" . $quizPostData['course_id'] . "' AND lc.module_id='" . $quizPostData['module_id'] . "' AND lc.lesson_id='" . $quizPostData['lesson_id'] . "' AND lc.contain_type=0 AND lc.quiz_type=0 GROUP BY lc.quiz_id  ")->result(); */


        return $this->db->query("SELECT lc.quiz_id,q.number_attempt,ca.contain_attempt,q.view_result_btn,q.title,q.instruction,if(count(ge_quiz.id) > 0,'1','0') as is_completed, if(q.number_attempt = 0 OR q.number_attempt > COALESCE(ca.contain_attempt, 0), 0,1) as quiz_attempt_allow from lesson_contain as lc JOIN quizzes as q ON (lc.quiz_id = q.id) LEFT JOIN gradebooks_evaluation as ge_quiz ON(ge_quiz.reference_id = lc.quiz_id AND ge_quiz.user_id = '" . $quizPostData['user_id'] . "' AND ge_quiz.course_id = '" . $quizPostData['course_id'] . "' AND ge_quiz.reference_type = 1) 
            LEFT JOIN course_activities as ca ON ( ca.contain_id = q.id AND ca.contain_type = 1 AND ca.course_id = ".$quizPostData['course_id']." AND ca.section_id = ".$quizPostData['section_id']." AND ca.user_id = ".$quizPostData['user_id']." AND ca.module_id=".$quizPostData['module_id']." AND ca.lesson_id=".$quizPostData['lesson_id']." )
            WHERE lc.course_id='" . $quizPostData['course_id'] . "' AND lc.module_id='" . $quizPostData['module_id'] . "' AND lc.lesson_id='" . $quizPostData['lesson_id'] . "' AND lc.contain_type=0 AND lc.quiz_type=0 GROUP BY lc.quiz_id  ")->result();


        //pr(vd());
    }

    private function getAssessmentByLesson($quizPostData) {

        /* return $this->db->query("SELECT lc.quiz_id,a.title from lesson_contain as lc JOIN assessments as a ON (lc.quiz_id = a.id) WHERE lc.course_id='" . $quizPostData['course_id'] . "' AND lc.module_id='" . $quizPostData['module_id'] . "' AND lc.lesson_id='" . $quizPostData['lesson_id'] . "' AND lc.contain_type=1 AND lc.quiz_type=2 GROUP BY lc.quiz_id  ")->result(); */
        return $this->db->query("SELECT lc.quiz_id,a.title,a.instruction,if(count(ge_quiz.id) > 0,'1','0') as is_completed  from lesson_contain as lc JOIN assessments as a ON (lc.quiz_id = a.id)  LEFT JOIN gradebooks_evaluation as ge_quiz ON(ge_quiz.reference_id = lc.quiz_id AND ge_quiz.user_id = '" . $quizPostData['user_id'] . "' AND ge_quiz.course_id = '" . $quizPostData['course_id'] . "' AND ge_quiz.reference_type = 2) WHERE lc.course_id='" . $quizPostData['course_id'] . "' AND lc.module_id='" . $quizPostData['module_id'] . "' AND lc.lesson_id='" . $quizPostData['lesson_id'] . "' AND lc.contain_type=1 AND lc.quiz_type=2 GROUP BY lc.quiz_id  ")->result();
    }

    public function myFavoriteCourseListByUserId($request) {

        $data = $this->Common_model->DJoin('courses.id,courses.name,courses.status,courses.description,courses.course_img', 'favorites', 'courses', 'favorites.course_id=courses.id', '', '', array("courses.delete_status" => 1, "favorites.user_id" => $request->id), '', 'favorites.course_id');

        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'message' => 'Data found.',
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                'message' => 'No data found.'
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function makefavoriteCourse($request) {
        $data = [
            'user_id' => trim($request->id),
            'course_id' => trim($request->course_id),
            'module_id' => trim($request->module_id),
            'lesson_id' => trim($request->lesson_id),
        ];
        $select = $this->Common_model->getDataById('favorites', '*', array("course_id" => $request->course_id, "user_id" => $request->id));
        if (!empty($select)) {
            $delete = $this->Common_model->DeleteDB('favorites', array("course_id" => $request->course_id, "user_id" => $request->id));
            if ($delete) {
                http_response_code(200);
                $this->response([
                    'status' => TRUE,
                    'jwt_status' => TRUE,
                    'fav_status' => FALSE,
                    'message' => 'Data deleted successfully.'
                        ], REST_Controller::HTTP_OK);
            } else {
                http_response_code(200);
                $this->response([
                    'status' => FALSE,
                    'jwt_status' => TRUE,
                    'message' => 'Something went wrong!!!'
                        ], REST_Controller::HTTP_OK);
            }
        } else {
            if ($this->Common_model->InsertData('favorites', $data)) {
                http_response_code(200);
                $this->response([
                    'status' => TRUE,
                    'jwt_status' => TRUE,
                    'fav_status' => TRUE,
                    'message' => 'Data inserted successfully.'
                        ], REST_Controller::HTTP_OK);
            } else {
                http_response_code(200);
                $this->response([
                    'status' => FALSE,
                    'jwt_status' => TRUE,
                    'message' => 'Something went wrong!!!'
                        ], REST_Controller::HTTP_OK);
            }
        }
    }

    public function getUserCourseCompletionValue($request) {

        // $cache_key = 'getUserCourseCompletionValue_' . $request->course_slug . "_" . $request->id;
        //  $all_data = $this->cache->get($cache_key);

        $course = $this->Common_model->getAllData('courses', 'id', true, array("slug" => trim($request->course_slug)));

        //   if (!$all_data) {
        $all_data = $this->Common_model->getAllData('module_contain', 'course_id, module_id', '', array("course_id" => $course->id), '', '', 'module_id');

        if ($all_data) {
            // save cache
            // $this->cache->save($cache_key, $all_data, CACHE_LIFE_TIME);
        }
        //  }

        $comp_count = 0;
        foreach ($all_data as $check_data) {
            $user_completed_data = $this->Common_model->getAllData('course_activities', 'module_id', '', array("user_id" => $request->id, "course_id" => $course->id, "module_id" => $check_data->module_id, "lesson_id" => 0, "contain_id" => 0, "contain_type" => 0), '', '', 'user_id,course_id,module_id');

            if (isset($user_completed_data[0]->module_id)) {
                $comp_count += 1;
            }
        }
        $percent = ($comp_count / sizeof($all_data)) * 100;
        if (!empty($all_data)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'message' => 'Data found.',
                'percentage' => round($percent)
                    ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                'message' => 'No data found.',
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function getModuleListByCourseSlug($request) {

        // $cache_key = 'getModuleListByCourseSlug_' . $request->course_slug . "_" . $request->id;
        // $data = $this->cache->get($cache_key);

        $course = $this->Common_model->getAllData('courses', 'id', true, array("slug" => trim($request->course_slug)));

        //   if (!$data) {
        $data = $this->Common_model->DJoin('modules.id, modules.slug', 'course_contain', 'modules', 'course_contain.module_id = modules.id', true, '', array("course_contain.course_id" => $course->id, "modules.status" => 1, "modules.delete_status" => 1), 'course_contain.order_id');
        if ($data) {
            // save cache
            //   $this->cache->save($cache_key, $data, CACHE_LIFE_TIME);
        }
        //  }

        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'message' => 'Data found.',
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                'message' => 'No data found.',
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function getSlugId($table, $slug) {
        return $this->Common_model->getAllData($table, 'id', true, array("slug" => trim($slug)))->id;
    }

    public function getLessonOrders($request) {
        $moduleId = $this->getSlugId('modules', $request->module_slug);
        $courseId = $this->getSlugId('courses', $request->course_slug);
        $lessonId = $this->getSlugId('lessons', $request->lesson_slug);


        $next = $this->Common_model->DJoin('lessons.slug , module_contain.order_id', 'lessons', 'module_contain', 'module_contain.lesson_id = lessons.id', true, '', array("module_contain.course_id" => $courseId, "module_contain.module_id" => $moduleId, "module_contain.lesson_id > " => $lessonId), 'module_contain.order_id desc', '', '1');

        $previous = $this->Common_model->DJoin('lessons.slug , module_contain.order_id', 'lessons', 'module_contain', 'module_contain.lesson_id = lessons.id', true, '', array("module_contain.course_id" => $courseId, "module_contain.module_id" => $moduleId, "module_contain.lesson_id < " => $lessonId), 'module_contain.order_id ', '', '1');
        //echo $this->db->last_query();
        //pr($arr);exit;
        //        $data = [];
        //        foreach ($arr as $key => $val) {
        //            $data[$val->order_id] = $val->slug;
        //        }
        // SELECT * FROM Example WHERE id > 3 ORDER BY id LIMIT 1
        $data = [];
        $data['next'] = $next->slug;
        $data['previous'] = $previous->slug;

        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'message' => 'Data found.',
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                'message' => 'No data found.',
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function getStartLessonByModule($request) {
        
        $cache_key = 'get_module_detail_by_slug_' . $request->course_slug . "_" . $request->module_slug;
        $data = $this->cache->get($cache_key);
        if (!$data) {
            $moduleId = $this->getSlugId($table = 'modules', $request->module_slug);
            $courseId = $this->getSlugId($table = 'courses', $request->course_slug);
            $data = $this->Common_model->DJoin('lessons.slug , module_contain.order_id', 'lessons', 'module_contain', 'module_contain.lesson_id = lessons.id', true, '', array("module_contain.course_id" => $courseId, "module_contain.module_id" => $moduleId), 'module_contain.order_id');
            if ($data) {
                // save cache
                $this->cache->save($cache_key, $data, CACHE_LIFE_TIME);
            }
        }
        
        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'message' => 'Data found.',
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                'message' => 'No data found.',
                    ], REST_Controller::HTTP_OK);
        }
    }

}
