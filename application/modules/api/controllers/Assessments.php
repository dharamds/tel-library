<?php
include('headers.php');
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//include Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class Assessments extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->config('ion_auth');
        $this->lang->load('auth');
        //load models
        $this->load->model('Common_model');

        // cache settings
        $this->config->set_item('cache_path', APPPATH . '/cache/api/');
        $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'tel_')
        );
    }

    public function index_post() {

        $request = json_decode(file_get_contents('php://input'));
        switch ($request->method) {
            case "getAssessment":
                $this->getAssessment($request);
                break;
            case "get_Assessment_grade":
                $this->get_Assessment_grade($request);
                break;
        }
    }

    public function getAssessment($request) {

        $courseId = $this->Common_model->getAllData('courses', 'id', true, array("slug" => trim($request->courseSlug)))->id;
        $sectionId = $this->Common_model->getAllData('course_sections', 'id', true, array("slug" => trim($request->sectionSlug)))->id;
        if($request->moduleSlug){
        $moduleId = $this->Common_model->getAllData('modules', 'id', true, array("slug" => trim($request->moduleSlug)))->id;
        }
        if($request->lessonSlug){
        $lessonId = $this->Common_model->getAllData('lessons', 'id', true, array("slug" => trim($request->lessonSlug)))->id;
        }
        
     //   $cache_key = 'getPoll_' . $request->poll_id;
      //  $data = $this->cache->get($cache_key);

        $columns      = 'users.id, users.email, users.first_name, users.last_name';
        $condition    = ['users.id' => $request->id];

        //if (!$data) {
            $user = $this->Common_model->getAllData('users', $columns, true, $condition);
            $assessmentDetails = $this->Common_model->DJoin('assessments.id, assessments.title, assessments.long_title, courses.id as course_id, courses.name, courses.long_title', 'assessments', 'courses', 'assessments.course_id = courses.id', true, '', ['assessments.id' => $request->recordID]);
            if($assessmentDetails):
            /****************** Peerceptiv code***********************/
                # ------------------------------
                # START CONFIGURATION SECTION
                # 
                $launch_data = array(
                    "user_id"                               => $user->id,
                    "roles"                                 => "Learner",
                    //"roles"                               => "Instructor",
                    "context_id"                            => CONTEXT_START + (int)$assessmentDetails->course_id,
                    "context_title"                         => $assessmentDetails->name,
                    "context_label"                         => $assessmentDetails->name,
                    "resource_link_id"                      => (RESOURCE_LINK_START + (int)$assessmentDetails->id).'-e1919-bb3456',
                    "resource_link_title"                   => $assessmentDetails->title,
                    "resource_link_description"             => $assessmentDetails->long_title,
                    "lis_person_name_full"                  => $user->first_name." ".$user->last_name,
                    "lis_person_name_family"                => $user->last_name,
                    "lis_person_name_given"                 => $user->first_name,
                    "lis_person_contact_email_primary"      => $user->email,
                    "lis_outcome_service_url"               => base_url('/assessments/lti_grading'),
                    "lis_person_sourcedid"                  => "school.edu:user",
                    "lis_result_sourcedid"                  => $assessmentDetails->id.':'.$user->id.':'.$courseId.':'.$sectionId.':'.$moduleId.':'.$lessonId
                    );
                
                    #
                    # END OF CONFIGURATION SECTION
                    # ------------------------------
                    $now = new DateTime();
                    $launch_data["lti_version"]             = "LTI-1p0";
                    $launch_data["lti_message_type"]        = "basic-lti-launch-request";
                    # Basic LTI uses OAuth to sign requests
                    # OAuth Core 1.0 spec: http://oauth.net/core/1.0/
                    $launch_data["oauth_callback"]          = "about:blank";
                    $launch_data["oauth_consumer_key"]      = LTI_KEY;
                    $launch_data["oauth_version"]           = "1.0";
                    $launch_data["oauth_nonce"]             = uniqid('', true);
                    $launch_data["oauth_timestamp"]         = $now->getTimestamp();
                    $launch_data["oauth_signature_method"]  = "HMAC-SHA1";
                    # In OAuth, request parameters must be sorted by name
                    $launch_data_keys                       = array_keys($launch_data);
                    sort($launch_data_keys);
                    $launch_params                          = array();
                    foreach ($launch_data_keys as $key) {
                    array_push($launch_params, $key . "=" . rawurlencode($launch_data[$key]));
                    }
                    $base_string = "POST&".urlencode(LTI_LAUNCH_URL)."&".rawurlencode(implode("&", $launch_params));
                    $data['secret']         = urlencode(LTI_SECRET) . "&";
                    $data['signature']      = base64_encode(hash_hmac("sha1", $base_string, LTI_SECRET, true));
                    $data['launch_data']    = $launch_data;
                    $data['launch_url']     = LTI_LAUNCH_URL; 
                   /****************** Peerceptiv code***********************/
            
                endif;
             if ($data) {
                // save cache
              //  $this->cache->save($cache_key, $data, CACHE_LIFE_TIME);
            }
           
      //  }

        if (!empty($data)) {
            $this->response([
                'status' => true,
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => false,
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        }
    }
}
