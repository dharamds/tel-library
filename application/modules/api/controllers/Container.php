<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
include('headers.php');

//include Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class Container extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->config('ion_auth');
        $this->lang->load('auth');

        //load models
        $this->load->model('Common_model');
        $this->load->model('courses/Courses_modal');

        // cache settings
        $this->config->set_item('cache_path', APPPATH . '/cache/api/');
        $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'tel_')
        );
    }

    public function index_post() {
        $request = json_decode(file_get_contents('php://input'));

        switch ($request->method) {
            case "get_container_uri":
                $this->get_container_uri($request);
                break;
            case "getAllCourses":
                $this->getAllCourses($request);
                break;

            case "verifySiteDomain":
                $this->verifySiteDomain($request);
                break;
            case "getCourseByCourseSlug":
                $this->getCourseByCourseSlug($request);
                break;
            case "get_course_menu_details":
                $this->get_course_menu_details($request);
                break;
            case "getModuleBySlug_OLD":
                $this->getModuleBySlug_OLD($request);
                break;
            case "getModuleBySlug":
                $this->getModuleBySlug($request);
                break;
            case "getLessonBySlug":
                $this->getLessonBySlug($request);
                break;
        }
    }

    public function get_course_menu_details($request) {
        //cache settings
        //  $cache_key = $request->slug.'_'.$request->user_id;
        //  $data = $this->cache->get($cache_key);
        //  if(!$data){  
        $this->load->model('Course_Model');
        $data = $this->Course_Model->getCourseMenuData($request->slug, $request->user_id);
        if ($data) {
            // save cache
            //   $this->cache->save($cache_key, $data, CACHE_LIFE_TIME);
        }
        // }

        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'message' => 'Data found.',
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Data found.',
                'data' => '',
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function get_container_uri($request) {
        // set cache
        $cache_key = md5($request->domain);
        $data = $this->cache->get($cache_key);
        if (!$data) {
            $data = $this->Common_model->getAllData('containers', 'id,name, domain, slogan, slug, home_page_text, home_footer_text, theme_style,' . ' street_1,street_2,state,city,zip', true, array("status_delete" => 1, "status" => 1, 'domain' => $request->domain));
            $data->theme_style = unserialize($data->theme_style);

            $street1    = ($data->street_1) ? $data->street_1 . ',' : '';
            $street2    = ($data->street_2) ? $data->street_2 . ',' : '';
            $city       = ($data->city) ? $data->city . ',' : '';
            $state      = ($data->state) ? $data->state . ',' : '';
            $zip        = ($data->zip) ? $data->zip : '';

            $data->address = $street1 . $street2 . $city . $state . $zip;

            if ($data) {
                // save cache
                $this->cache->save($cache_key, $data, CACHE_LIFE_TIME);
            }
        }
        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'message' => 'Data found.',
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'No data found.'
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function verifySiteDomain($request) {

        $data = $this->Common_model->getAllData('containers', 'name, domain, slogan, slug, home_page_text, home_footer_text, theme_style, ', true, array("status_delete" => 1, "status" => 1, 'domain' => $request->domain));

        if (!empty($data)) {
            $this->response([
                'status' => true,
                'message' => 'Data found.',
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Data not found.',
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function getAllCourses() {
        // cache settings
        $cache_key = 'getAllCourses';
        $data = $this->cache->get($cache_key);
        if (!$data) {
            $this->load->model('Course_Model');
            $data = $this->Course_Model->get_public_all_courses();
            $data = $this->Common_model->getAllData('courses', 'id, slug, name, long_title, code, course_img, exp_status, course_expiry, created', '', array("delete_status" => 1, 'status' => 1), '', 6);
            if ($data) {
                // save cache
                $this->cache->save($cache_key, $data, CACHE_LIFE_TIME);
            }
        }
        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'message' => 'Data found.',
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'No data found.'
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function getCourseByCourseSlug($request) {

        // cache settings
        $cache_key = 'courses_' . md5($request->slug);
        $data = $this->cache->get($cache_key);
        if (!$data) {
            $data = $this->Common_model->getAllData('courses', '*', true, array("delete_status" => 1, "status" => 1, "slug" => $request->slug));
            $data->documents = $this->Common_model->getAllData('documents', 'documents.title, documents.filename', false, array("documents.reference_id" => $data->id, "documents.reference_type" => 1, "documents.delete_status" => 1));

            if ($data) {
                // save cache
                $this->cache->save($cache_key, $data, CACHE_LIFE_TIME);
            }
        }

        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'message' => 'Data found.',
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'message' => 'No data found.'
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function getModuleBySlug123($request) {
        $this->load->model('Course_Model');
        // cache settings
        $cache_key = 'moduleDetailsBySlugs_' . $request->courseSlug . $request->slug;
        $data = $this->cache->get($cache_key);
        if(!$data){    
            // pr($request);exit;
            $courseId = $this->Common_model->getAllData('courses', 'id', true, array("slug" => trim($request->course_slug)))->id;

            $moduleId = $this->Common_model->getAllData('modules', 'id', true, array("slug" => trim($request->slug)))->id;

            $data = $this->Common_model->getAllData('modules', '*', true, array("slug" => $request->slug, "delete_status" => 1, "status" => 1));
            $data->documents = $this->Common_model->getAllData('documents', 'documents.title, documents.filename', false, array("documents.reference_id" => $data->id, "documents.reference_type" => 2, "documents.delete_status" => 1));
            $userId = $request->user_id;
            /* $data->quiz_data = $this->db->query("SELECT mc.quiz_id, a.title from module_contain as mc JOIN quizzes as a ON (mc.quiz_id = a.id) WHERE mc.course_id='" . $courseId . "'  AND mc.module_id='" . $moduleId . "' AND mc.contain_type=0 AND mc.quiz_type=3 GROUP BY mc.quiz_id")->result();
             */
            $data->quiz_data = $this->db->query("SELECT mc.quiz_id, a.title,a.instruction, if(count(ge_quiz.id) > 0,'1','0') as is_completed from module_contain as mc JOIN quizzes as a ON (mc.quiz_id = a.id) LEFT JOIN gradebooks_evaluation as ge_quiz ON(ge_quiz.reference_id = mc.quiz_id AND ge_quiz.user_id = '" . $userId . "' AND ge_quiz.course_id = '" . $courseId . "' AND ge_quiz.reference_type = 1) WHERE mc.course_id='" . $courseId . "'  AND mc.module_id='" . $moduleId . "' AND mc.contain_type=0 AND mc.quiz_type=3 GROUP BY mc.quiz_id")->result();

            /* $data->assessment_data = $this->db->query("SELECT mc.quiz_id, a.title from module_contain as mc JOIN assessments as a ON (mc.quiz_id = a.id)  WHERE mc.course_id='" . $courseId . "'  AND mc.module_id='" . $moduleId . "' AND mc.contain_type=1 AND mc.quiz_type=3 GROUP BY mc.quiz_id  ")->result(); */

            $data->assessment_data = $this->db->query("SELECT mc.quiz_id, a.title,  if(count(ge_quiz.id) > 0,'1','0') as is_completed from module_contain as mc JOIN assessments as a ON (mc.quiz_id = a.id) LEFT JOIN gradebooks_evaluation as ge_quiz ON(ge_quiz.reference_id = mc.quiz_id AND ge_quiz.user_id = '" . $userId . "' AND ge_quiz.course_id = '" . $courseId . "' AND ge_quiz.reference_type = 2) WHERE mc.course_id='" . $courseId . "'  AND mc.module_id='" . $moduleId . "' AND mc.contain_type=1 AND mc.quiz_type=3 GROUP BY mc.quiz_id  ")->result();

            //echo $this->db->last_query();
            //pr($data); exit;
            if ($data) {
                // save cache
                $this->cache->save($cache_key, $data, CACHE_LIFE_TIME);
            }
        }
        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'message' => 'Data found.',
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'No data found.',
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function getModuleBySlug($request) {
        $this->load->model('Course_Model');
        $courseId = $this->Common_model->getAllData('courses', 'id', true, array("slug" => trim($request->course_slug)))->id;
        $sectionId = $this->Common_model->getAllData('course_sections', 'id', true, array("slug" => trim($request->section_slug)))->id;
        $moduleId = $this->Common_model->getAllData('modules', 'id', true, array("slug" => trim($request->slug)))->id;
        $sectionExpiry = $this->Course_Model->checkCourseSectionExpiry($courseId, $sectionId);
        
        //pr($sectionExpiry);
        
        if (empty($sectionExpiry)) {
            $this->response([
                'status' => FALSE,
                'message' => 'No data found.',
                    ], REST_Controller::HTTP_OK);
        } else {
            $data = $this->Common_model->getAllData('modules', '*', true, array("slug" => $request->slug, "delete_status" => 1, "status" => 1));
            $data->documents = $this->Common_model->getAllData('documents', 'documents.title, documents.filename', false, array("documents.reference_id" => $data->id, "documents.reference_type" => 2, "documents.delete_status" => 1));
            $userId = $request->user_id;

            //$data->quiz_data = $this->db->query("SELECT mc.quiz_id, a.title,a.instruction, if(count(ge_quiz.id) > 0,'1','0') as is_completed from module_contain as mc JOIN quizzes as a ON (mc.quiz_id = a.id) LEFT JOIN gradebooks_evaluation as ge_quiz ON(ge_quiz.reference_id = mc.quiz_id AND ge_quiz.user_id = '" . $userId . "' AND ge_quiz.course_id = '" . $courseId . "' AND ge_quiz.reference_type = 1) WHERE mc.course_id='" . $courseId . "'  AND mc.module_id='" . $moduleId . "' AND mc.contain_type=0 AND mc.quiz_type=3 GROUP BY mc.quiz_id")->result();

            $data->quiz_data = $this->db->query("SELECT mc.quiz_id,q.number_attempt,ca.contain_attempt,q.view_result_btn,q.title,q.instruction, if(count(ge_quiz.id) > 0,'1','0') as is_completed, if(q.number_attempt = 0 OR q.number_attempt > COALESCE(ca.contain_attempt, 0), 0,1) as quiz_attempt_allow from module_contain as mc JOIN quizzes as q ON (mc.quiz_id = q.id) LEFT JOIN gradebooks_evaluation as ge_quiz ON(ge_quiz.reference_id = mc.quiz_id AND ge_quiz.user_id = '" . $request->user_id . "' AND ge_quiz.course_id = '" . $courseId . "' AND ge_quiz.reference_type = 1) 
            
            LEFT JOIN course_activities as ca ON ( ca.contain_id = q.id AND ca.contain_type = 1 AND ca.course_id = ".$courseId." AND ca.section_id = ".$sectionId." AND ca.user_id = ".$request->user_id." AND ca.module_id=".$moduleId." )

            WHERE mc.course_id='" . $courseId . "'  AND mc.module_id='" . $moduleId . "' AND mc.contain_type=0 AND mc.quiz_type=3 GROUP BY mc.quiz_id")->result();

            $data->assessment_data = $this->db->query("SELECT mc.quiz_id, a.title, a.instruction , if(count(ge_quiz.id) > 0,'1','0') as is_completed from module_contain as mc JOIN assessments as a ON (mc.quiz_id = a.id) LEFT JOIN gradebooks_evaluation as ge_quiz ON(ge_quiz.reference_id = mc.quiz_id AND ge_quiz.user_id = '" . $userId . "' AND ge_quiz.course_id = '" . $courseId . "' AND ge_quiz.reference_type = 2) WHERE mc.course_id='" . $courseId . "'  AND mc.module_id='" . $moduleId . "' AND mc.contain_type=1 AND mc.quiz_type=3 GROUP BY mc.quiz_id  ")->result();

            if (!empty($data)) {
                $this->response([
                    'status' => TRUE,
                    'message' => 'Data found.',
                    'data' => $data
                        ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No data found.',
                        ], REST_Controller::HTTP_OK);
            }
        }
    }

    public function getModuleBySlug1($request) {
        // cache settings
        //   $cache_key = md5($request->slug);
        //  $data = $this->cache->get($cache_key);
        //  if(!$data){    
        $data = $this->Common_model->getAllData('modules', '*', true, array("slug" => $request->slug, "delete_status" => 1, "status" => 1));
        $course_data = $this->Common_model->getAllData('courses', 'id', true, array("delete_status" => 1, "status" => 1, "slug" => $request->course_slug));

        // $data->quiz_data=$this->db->query("SELECT lc.quiz_id,q.title from lesson_contain as lc JOIN quizzes as q WHERE lc.course_id='".$course_data->id."' AND lc.module_id='".$data->id."' AND lc.contain_type=0 AND lc.quiz_type=2 ")->result();
        $data->quiz_data = $this->db->query("SELECT mc.quiz_id, a.title from module_contain as mc JOIN quizzes as a ON (mc.quiz_id = a.id) WHERE mc.course_id='" . $course_data->id . "'  AND mc.module_id='" . $data->id . "' AND mc.contain_type=0 AND mc.quiz_type=3 GROUP BY mc.quiz_id  ")->result();
        //pr(vd());
        if ($data) {
            // save cache
            // $this->cache->save($cache_key, $data, CACHE_LIFE_TIME);
        }
        // }
        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'message' => 'Data found.',
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'No data found.',
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function getLessonBySlug($request) {
        // cache settings
        // $cache_key = md5($request->slug);
        // $data = $this->cache->get($cache_key);
        //if(!$data){ 
        $data = $this->Common_model->getAllData('lessons', '*', true, array("slug" => $request->slug, "delete_status" => 1, "status" => 1));
        if ($data) {
            // save cache
            //  $this->cache->save($cache_key, $data, CACHE_LIFE_TIME);
        }
        //  }    
        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'message' => 'Data found.',
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'message' => 'No data found.'
                    ], REST_Controller::HTTP_OK);
        }
    }

}
