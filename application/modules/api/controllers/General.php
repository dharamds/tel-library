<?php
include('headers.php');
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//include Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class General extends REST_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->config('ion_auth');
        $this->lang->load('auth');
        //load models
        $this->load->model('Common_model');
        $this->load->model('Ion_auth_model');
        //load helper
        $this->load->helper('jwt');
        // $this->load->helper('validation');
        //load libraries
        $this->load->library('form_validation');
        //JWT AUTHENTICATION
        $data = json_decode(file_get_contents('php://input'));

        $db_token = $this->Common_model->getDataById('users', 'jwt_token', array("id" => $data->id));
        $request_token = $this->input->request_headers();
        $request_token['token'];

        if ($request_token['token'] == '' || $db_token->jwt_token != $request_token['token']) {
            $this->response([
                'status' => FALSE,
                'jwt_status' => FALSE,
                'message' => 'Invalid token.',
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function index_post() {
        $request = json_decode(file_get_contents('php://input'));
        switch ($request->method) {
            case "courseList":
                $this->courseList($request);
                break;
            case "courseListbyID":
                $this->courseListbyID($request);
                break;
            case "courseAssignListbyUserID":
                $this->courseAssignListbyUserID($request);
                break;
            case "get_courses":
                $this->get_courses($request);
                break;
            case "makefavorite":
                $this->makefavorite($request);
                break;
            case "checkFavCourseByUserId":
                $this->checkFavCourseByUserId($request);
                break;
            case "myFavoriteCourseListByUserId":
                $this->myFavoriteCourseListByUserId($request);
                break;
            case "notificationsByUserId":
                $this->notificationsByUserId($request);
                break;
            case "getMessagesByUserId":
                $this->getMessagesByUserId($request);
                break;
            case "getMessageByMessageId":
                $this->getMessageByMessageId($request);
                break;
        }
    }

    public function courseList() {
        $data = $this->Common_model->getAllData('courses', '*', '', array("delete_status" => 1));
        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'message' => 'Data found.',
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                'message' => 'No data found.'
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function courseListbyID($request) {
        $data = $this->Common_model->getDataById('courses', '*', array("delete_status" => 1, "id" => $request->course_id));
        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'message' => 'Data found.',
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                'message' => 'No data found.'
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function checkFavCourseByUserId($request) {
        $select = $this->Common_model->getDataById('favorites', '*', array("course_id" => $request->course_id, "user_id" => $request->id));
        // echo    $this->db->last_query();
        if (!empty($select)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'fav_status' => TRUE,
                'message' => 'course is favorite.',
                    ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                'fav_status' => FALSE,
                'message' => 'course is not favorite.',
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function courseAssignListbyUserID($request) {

        $data = $this->Common_model->DJoin('courses.id,courses.name,courses.status,courses.description,courses.course_img,course_assigned_to_users.is_completed', 'course_assigned_to_users', 'courses', 'course_assigned_to_users.course_id=courses.id', '', '', array("courses.delete_status" => 1, "course_assigned_to_users.user_id" => $request->id), '', 'course_assigned_to_users.course_id');
        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'message' => 'Data found.',
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                'message' => 'No data found.'
                    ], REST_Controller::HTTP_OK);
        }
    }
    
     
    public function notificationsByUserId($request) {
        $select = $this->Common_model->getAllData('notifications','*','',array("receiver_id" => $request->id));
        if (!empty($select)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'list'=>$select
                    ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                 'list'=>$select
                    ], REST_Controller::HTTP_OK);
        }
    }
    
    public function getMessagesByUserId($request) {
        $select = $this->Common_model->getAllData('messages','*','',array("receiver_id" => $request->id));
        if (!empty($select)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'list'=>$select
                    ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                 'list'=>$select
                    ], REST_Controller::HTTP_OK);
        }
    }
    
    public function getMessageByMessageId($request) {
        $select = $this->Common_model->getAllData('messages','*',true,array("id" => $request->messageId , "receiver_id" => $request->id));
        if (!empty($select)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'list'=>$select
                    ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                 'list'=>$select
                    ], REST_Controller::HTTP_OK);
        }
    }
    
     public function myFavoriteCourseListByUserId($request) {
        $data = $this->Common_model->DJoin('courses.id,courses.name,courses.status,courses.description,courses.course_img', 'favorites', 'courses', 'favorites.course_id=courses.id',
                '', '', array("courses.delete_status" => 1, "favorites.user_id" => $request->id), '', 'favorites.course_id');
        
        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'message' => 'Data found.',
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                'message' => 'No data found.'
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function makefavorite($request) {
        $data = [
            'user_id' => trim($request->id),
            'course_id' => trim($request->course_id),
            'module_id' => trim($request->module_id),
            'lesson_id' => trim($request->lesson_id),
        ];
        $select = $this->Common_model->getDataById('favorites', '*', array("course_id" => $request->course_id, "user_id" => $request->id));
        if (!empty($select)) {
            $delete = $this->Common_model->DeleteDB('favorites', array("course_id" => $request->course_id, "user_id" => $request->id));
            if ($delete) {
                http_response_code(200);
                $this->response([
                    'status' => TRUE,
                    'jwt_status' => TRUE,
                    'fav_status' => FALSE,
                    'message' => 'Data deleted successfully.'
                        ], REST_Controller::HTTP_OK);
            } else {
                http_response_code(200);
                $this->response([
                    'status' => FALSE,
                    'jwt_status' => TRUE,
                    'message' => 'Something went wrong!!!'
                        ], REST_Controller::HTTP_OK);
            }
        } else {
            if ($this->Common_model->InsertData('favorites', $data)) {
                http_response_code(200);
                $this->response([
                    'status' => TRUE,
                    'jwt_status' => TRUE,
                    'fav_status' => TRUE,
                    'message' => 'Data inserted successfully.'
                        ], REST_Controller::HTTP_OK);
            } else {
                http_response_code(200);
                $this->response([
                    'status' => FALSE,
                    'jwt_status' => TRUE,
                    'message' => 'Something went wrong!!!'
                        ], REST_Controller::HTTP_OK);
            }
        }
    }

}
