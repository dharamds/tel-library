<?php
include('headers.php');
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//include Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class Messages extends REST_Controller
{

    public function __construct()
    {

        parent::__construct();
        $this->load->config('ion_auth');
        $this->lang->load('auth');
        //load models
        $this->load->model('Common_model');
        $this->load->model('Messages_model');
        $this->load->model('Ion_auth_model');
        //load helper
        $this->load->helper('jwt');
        // $this->load->helper('validation');
        //load libraries
        $this->load->library('form_validation');
        //JWT AUTHENTICATION
        $data = json_decode(file_get_contents('php://input'));

        $db_token = $this->Common_model->getDataById('users', 'jwt_token', array("id" => $data->id));
        $request_token = $this->input->request_headers();
        $request_token['token'];

        if ($request_token['token'] == '' || $db_token->jwt_token != $request_token['token']) {
            $this->response([
                'status' => FALSE,
                'jwt_status' => FALSE,
                'message' => 'Invalid token.',
            ], REST_Controller::HTTP_OK);
        }
    }

    public function index_post()
    {
        $request = json_decode(file_get_contents('php://input'));
        switch ($request->method) {
            case "messagesListByLimit":
                $this->messagesListByLimit($request);
                break;
            case "messagesCount":
                $this->messagesCount($request);
                break;
            case "messagesList":
                $this->messagesList($request);
                break;
        }
    }

    public function messagesListByLimit($request)
    {
        $data = $this->Common_model->getAllData('messages', '*', '', array("receiver_id" => $request->id), 'id DESC', '10');
        //pr($data , "h");
        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'message' => 'Data found.',
                'data' => $data
            ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                'message' => 'No data found.'
            ], REST_Controller::HTTP_OK);
        }
    }

    public function messagesCount($request)
    {
        $data = $this->Common_model->getAllData('messages', 'count(id) as count', '', array("receiver_id" => $request->id,"is_read" => 0), 'id DESC');
        $count = $data[0]->count;
        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'message' => 'Data found.',
                'data' => $count
            ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                'message' => 'No data found.'
            ], REST_Controller::HTTP_OK);
        }
    }

    public function messagesList($request)
    {
        $data = $this->Common_model->getAllData('messages', '*', '', array("receiver_id" => $request->id), 'id DESC');
        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'message' => 'Data found.',
                'data' => $data
            ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                'message' => 'No data found.'
            ], REST_Controller::HTTP_OK);
        }
    }

}
