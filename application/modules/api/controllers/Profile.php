<?php

include('headers.php');
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
//include Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class Profile extends REST_Controller {

    var $method = "";
    var $id = "";

    public function __construct() {

        parent::__construct();
        //ini_set('display_errors', 1);
        $this->load->config('ion_auth');
        $this->lang->load('auth');
        //load models
        $this->load->model('Common_model');
        // $this->load->model('users/Users_modal');
        $this->load->model('Ion_auth_model');
        //load helper
        $this->load->helper('jwt');
        //load libraries
        $this->load->library('form_validation');
        //JWT AUTHENTICATION

        $data = json_decode(file_get_contents('php://input'));
        if (empty($_FILES['user_img_file'])) {
            $db_token = $this->Common_model->getDataById('users', 'jwt_token', array("id" => $data->id));
        } else {
            $postId = str_replace('"', '', $_POST['id']);
            $db_token = $this->Common_model->getDataById('users', 'jwt_token', array("id" => $postId));
            //echo $this->db->last_query();
        }

        $request_token = $this->input->request_headers();

        //echo $db_token->jwt_token;echo nl2br("\n");
        //  echo $request_token['token'];
        // exit;

        if ($request_token['token'] == '' || $db_token->jwt_token != $request_token['token']) {
            $this->response([
                'status' => FALSE,
                'jwt_status' => FALSE,
                'message' => 'Invalid token.',
                    ], REST_Controller::HTTP_OK);
        }

        if (empty($_FILES['user_img_file'])) {
            
        } else {
            $this->id = json_decode($_POST['id'], true);
            $this->method = $_POST['method'];
            $db_token = $this->Common_model->getDataById('users', 'jwt_token', array("id" => $this->id));
            $request_token = $this->input->request_headers();
            if ($request_token['token'] == '' || $db_token->jwt_token != $request_token['token']) {
                $this->response([
                    'status' => FALSE,
                    'jwt_status' => FALSE,
                    'message' => 'Invalid token.',
                        ], REST_Controller::HTTP_OK);
            }
        }
    }

    public function index_post() {
        $request = json_decode(file_get_contents('php://input'));
        if (empty($_FILES['user_img_file'])) {
            $request->method = $request->method;
        } else {
            $request->method = str_replace('"', '', $this->method);
        }

        switch ($request->method) {

            case "ProfileUpdate":
                $this->ProfileUpdate($request);
                break;
            case "UserProfilePictureUpdate":
                $this->UserProfilePictureUpdate($request);
                break;
            case "getUserData":
                $this->getUserData($request);
                break;
            case "getStates":
                $this->getStates($request);
                break;
            case "getCities":
                $this->getCities($request);
                break;
        }
    }

    public function getUserData($request) {
        $data = $this->Common_model->getDataById('users', '', array("id" => $request->id));
        if ($data) {
            $this->response([
                'status' => TRUE,
                'message' => 'User found.',
                'data' => $data,
                    ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'message' => 'No user were found.'
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function UserProfilePictureUpdate() {
        //print_r($_POST);
        $userId = str_replace('"', '', $_POST['id']);
        // exit;
        if (is_uploaded_file($_FILES['user_img_file']['tmp_name'])) {
            $uploadPath = 'uploads/user_profile/' . $_FILES['user_img_file']['name'];
            $allowed_types = array('image/jpeg', 'image/jpg', 'image/png');
            $fileInfo = finfo_open(FILEINFO_MIME_TYPE);
            $detected_type = finfo_file($fileInfo, $_FILES['user_img_file']['tmp_name']);
            if (!in_array($detected_type, $allowed_types)) {
                die('Please upload a image');
            }
            if (move_uploaded_file($_FILES['user_img_file']['tmp_name'], $uploadPath)) {
                //echo FCPATH.$uploadPath;
                resizeImage('user_profile/' . $_FILES['user_img_file']['name'], 'user_profile/thumb/' . $_FILES['user_img_file']['name'], 250, 200);
            }

            $update = $this->Common_model->UpdateDB('users', 'id=' . $_POST['id'], array("user_img" => $_FILES['user_img_file']['name']));
            if ($update) {
                $data = $this->Common_model->getDataById('users', 'user_img', array("id" => $userId));
                $this->response([
                    'status' => TRUE,
                    'message' => 'Profile Upload.',
                    'data' => $data,
                        ], REST_Controller::HTTP_OK);
            }
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'message' => 'Profile image not upload.'
                    ], REST_Controller::HTTP_OK);
        };
    }

    public function ProfileUpdate($request) {
        $fields = [
            //"username" => trim($request->email),
            "first_name" => trim($request->first_name),
            "middle_name" => trim($request->middle_name),
            "last_name" => trim($request->last_name),
            //"email" => $request->email,
            //  "user_img" => $request->user_img,
            "phone" => $request->phone,
            "secondary_email" => $request->secondary_email,
            "street_address_1" => $request->street_address_1,
            "street_address_2" => $request->street_address_2,
            "city" => $request->city,
            "state" => $request->state
        ];


        if ($this->Common_model->UpdateDB('users', array("id" => $request->id), $fields)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'message' => 'Updated successfully.'
                    ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                'message' => 'Not updated.'
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function getStatesOLD($request) {
        $data = $this->Common_model->getAllData('states', ['name', 'code'], '', ['status' => 1, 'status_delete' => 1]);
        if ($data) {
            $this->response([
                'status' => TRUE,
                'message' => 'data found.',
                'data' => $data,
                    ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'message' => 'No data were found.'
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function getStates($request) {
        $stateList = [];
        $data = $this->Common_model->getAllData('states', ['name', 'code'], '', ['status' => 1, 'status_delete' => 1]);

        $i = 0;
        foreach ($data as $key => $val) {
            $stateList[$i] = $val->name;
            $i++;
        }
        if ($data) {
            $this->response([
                'status' => TRUE,
                'message' => 'data found.',
                'data' => $data,
                'stateList' => $stateList,
                    ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'message' => 'No data were found.'
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function getCities($request) {
        $condition = [];
        if (isset($request->state) && $request->state != '') {
            $condition['state_code'] = $request->state;
        }
        $data = $this->Common_model->getAllData('cities', ['name', 'state_code'], '', $condition);
        if ($data) {
            $this->response([
                'status' => TRUE,
                'message' => 'data found.',
                'data' => $data,
                    ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'message' => 'No data were found.'
                    ], REST_Controller::HTTP_OK);
        }
    }

}
