<?php

include('headers.php');
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//include Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class Quiz extends REST_Controller {

    public function __construct() {
        parent::__construct();
         ini_set('display_errors', 0);
        $this->load->config('ion_auth');
        $this->lang->load('auth');
        //load models
        $this->load->model('Quiz_model');
        $this->load->model('Common_model');

        // cache settings
        $this->config->set_item('cache_path', APPPATH . '/cache/api/quiz');
        $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'tel_')
        );
    }

    public function index_post() {

        $request = json_decode(file_get_contents('php://input'));
        switch ($request->method) {
            case "getQuizQuestion":
                $this->getQuizQuestion($request);
                break;
            case "getQuizIdByLesson":
                $this->getQuizIdByLesson($request);
                break;
            case "getLastQuizResult":
                $this->getLastQuizResult($request);
                break;
             case "getQuizMultipleLastResult":
                $this->getQuizMultipleLastResult($request);
                break;
            case "getQuizQuestionAll":
                $this->getQuizQuestionAll($request);
                break;
            case "courseActivityCompletionMethod":
                $this->courseActivityCompletionMethod($request);
                break;
             case "getSaveCYKResult":
                $this->getSaveCYKResult($request);
                break;                
        }
    }


    public function getQuizQuestionAll($request) {
        $quizzData = $this->__getCheckAbleToPlayQuiz($request);
        
        if($quizzData->play_allow == 0){
            $result['play_allow'] = $quizzData->play_allow;
            $this->response([

                'status' => FALSE,
                'data' => $result
                    ], REST_Controller::HTTP_OK);
         //   exit;
        }
        //   $cache_key = 'getQuizQuestion_' . $request->id;
        //  $data = $this->cache->get($cache_key);
        //  if (!$data) {
        $data = $this->Quiz_model->getQuizQuestionAll($request);
      
        if ($data['allSectionQuestionsCount'] == 0) {
            $this->response([
                'status' => false,
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        }
        if ($data['questions']) {
            // save cache
            // $this->cache->save($cache_key, $data, CACHE_LIFE_TIME);
        }
        //   }

        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function getQuizQuestion($request) {
        $quizzData = $this->__getCheckAbleToPlayQuiz($request);

        if($quizzData->play_allow == 0){
             $result['play_allow'] = $quizzData->play_allow;
             $this->response([
                'status' => FALSE,
                'data' => $result
                    ], REST_Controller::HTTP_OK);
             exit;
        }
        //   $cache_key = 'getQuizQuestion_' . $request->id;
        //  $data = $this->cache->get($cache_key);
        //  if (!$data) {
        $data = $this->Quiz_model->getQuizQuestion($request->quiz_id);
      
        if ($data['allSectionQuestionsCount'] == 0) {
            $this->response([
                'status' => false,
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        }
        if ($data['questions']) {
            // save cache
            // $this->cache->save($cache_key, $data, CACHE_LIFE_TIME);
        }
        //   }

        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'data' => []
                    ], REST_Controller::HTTP_OK);
        }
    }

    private function __getCheckAbleToPlayQuiz($requestData = [])
    {
        $data = [];
        $data['courseId'] = $this->Common_model->getAllData('courses', 'id', true, array("slug" => trim($requestData->course_slug)))->id;
        $data['moduleId'] = $this->Common_model->getAllData('modules', 'id', true, array("slug" => trim($requestData->module_slug)))->id;
        $data['lessonId'] = $this->Common_model->getAllData('lessons', 'id', true, array("slug" => trim($requestData->lesson_slug)))->id;
        $data['sectionId'] = $this->Common_model->getAllData('course_sections', 'id', true, ["slug" => trim($requestData->section_slug), "status" => 1, "delete_status" => 1, "course_id" => $data['courseId']])->id;
        $data['quizz_id'] = $requestData->quiz_id;
        $data['user_id'] = $requestData->user_id;
        if(!$data['moduleId']){
            $data['moduleId'] = 0;
        }
        if(!$data['lessonId']){
            $data['lessonId'] = 0;
        }
        return $this->Quiz_model->getCheckQuizAttempt($data);
    }

    public function getLastQuizResult($request) {
        $data = $this->Quiz_model->getQuizQuestionWithAns($request->quiz_id, $request->user_id);

        if ($data['allSectionQuestionsCount'] == 0) {
            $this->response([
                'status' => false,
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        }


        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function getQuizIdByLesson($request) {
        // $cache_key = 'getQuizIdByLesson_' . $request->lesson_slug;
        //$data = $this->cache->get($cache_key);
        // if (!$data) {
        $data = $this->Common_model->getAllData('lessons', 'quiz_id', true, array("slug" => trim($request->lesson_slug)));
        if ($data) {
            // save cache
            //  $this->cache->save($cache_key, $data, CACHE_LIFE_TIME);
        }
        //  }
//       echo  $this->db->last_query();
        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function courseActivityCompletionMethod($request)
    {
        $data = [];
        $courseId = $this->Common_model->getAllData('courses', 'id', true, array("slug" => trim($request->course_slug)))->id;
        $moduleId = $this->Common_model->getAllData('modules', 'id', true, array("slug" => trim($request->module_slug)))->id;
        $lessonId = $this->Common_model->getAllData('lessons', 'id', true, array("slug" => trim($request->lesson_slug)))->id;
        $sectionId = $this->Common_model->getAllData('course_sections', 'id', true, ["slug" => trim($request->section_slug), "status" => 1, "delete_status" => 1, "course_id" => $courseId])->id;
       // pr($sectionId);exit;
        if ($courseId && !$moduleId && !$lessonId && $request->contain_id && $request->contain_type) {
            $data['course_id']  = $courseId;
            $data['user_id']    = $request->id;
            $data['module_id']  = 0;
            $data['lesson_id'] = 0;
        } elseif ($courseId && $moduleId && !$lessonId && $request->contain_id && $request->contain_type) {
            $data['course_id'] = $courseId;
            $data['user_id']    = $request->id;
            $data['module_id'] = $moduleId;
            $data['lesson_id'] = 0;
        } elseif ($courseId && $moduleId && $lessonId && $request->contain_id && $request->contain_type) {
            $data['course_id'] = $courseId;
            $data['user_id'] = $request->id;
            $data['module_id'] = $moduleId;
            $data['lesson_id'] = $lessonId;
        }
            $data['contain_id'] = $request->contain_id;
            $data['contain_type'] = $request->contain_type;
            $data['section_id'] = $sectionId;
            
        $course_activities =  $this->Common_model->getAllData('course_activities', 'id, contain_attempt, count(id) as record_count', true, $data);
        //pr($course_activities);exit;
        if($course_activities->record_count == 0){
            $data['contain_attempt'] = 1;
            $data['created'] = date('Y-m-d h:i:sa');
            if($select = $this->Common_model->InsertData('course_activities', $data)){
               $this->__getSaveQuizResult($request, $courseId, $sectionId);
            }
        }else{
            $update_contain_attempt['contain_attempt'] = $course_activities->contain_attempt + 1;
            if($select =$this->Common_model->UpdateDB('course_activities', ['id' => $course_activities->id], $update_contain_attempt)){
               $this->__getSaveQuizResult($request, $courseId, $sectionId);
            }
        }

        if (!empty($select)) {
            $view_result_btn = $this->Common_model->getAllData('quizzes', 'view_result_btn', true, ['id'=>$request->contain_id])->view_result_btn;
            $this->response([
                'view_result_btn' => $view_result_btn,
                'status' => TRUE,
                'jwt_status' => TRUE,
                'message' => 'Activity saved successfully'
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                'message' => 'Activity could not completed'
            ], REST_Controller::HTTP_OK);
        }
    }

    private function __getSaveQuizResult($postData = [], $courseId, $sectionId)
    {   
        
        $gradingEvalutionData = [];
        $answered_ids = [];
        foreach ($postData->quizFormData as $key => $qValue) {
            $quizOption = $qValue->quizOption;            
            foreach ($quizOption as $keyOp => $valueOp) {
                $gradingEvalutionData[] = [
                    'user_id' => $postData->id,
                    'course_id' => $courseId,
                    'section_id' => $sectionId,
                    'reference_id' => $postData->reference_id,
                    'reference_type' => $postData->reference_type,
                    'quize_type' => $postData->quiz_type,
                    'question_id' => $qValue->quiz_question_id,
                    'answered_id' => $valueOp->id,
                    'correct' => $qValue->correct
                ];
                $answered_ids[] = $valueOp->id;
                $answerData[$valueOp->id] =  $qValue->correct;
            }
        }

        $conditionRemove = [
            'user_id' => $postData->id,
            'course_id' => $courseId,
            'section_id' => $sectionId,
            'reference_id' => $postData->reference_id,
            'reference_type' => $postData->reference_type,
            'quize_type' => $postData->quiz_type
        ];
        if ($gradingEvalutionData) {
            $this->Common_model->DeleteDB('gradebooks_evaluation',$conditionRemove);
            $this->db->insert_batch('gradebooks_evaluation_log', $gradingEvalutionData);
            $this->db->insert_batch('gradebooks_evaluation', $gradingEvalutionData);
            $this->__getSaveGrading($postData, $courseId, $sectionId, $answered_ids,$answerData); 
        }
    }


    /* Start gradding code */

    public function __getSaveGrading($postData = [], $courseId, $sectionId,$answered_ids = [],$answerData = [])
    {
        $quesWeight = $this->__getQuizWait($postData->reference_id);
        //pr($answered_ids);exit;
        $totalGradePoints = $this->__getQuestionsWait($answered_ids, $quesWeight->question_weight,$answerData);
        $saveGradeData = [
            'user_id' => $postData->id,
            'course_id' => $courseId,
            'section_id' => $sectionId,
            'quiz_assess_id' => $postData->reference_id,
            'quiz_assess_type' => $postData->quiz_type,
            'grade' => $totalGradePoints,
            'created' => date('Y-m-d h:i:sa')
        ];
        if ($saveGradeData) {
            $this->Common_model->InsertData('gradebook_quiz_assess_grade_attempts', $saveGradeData);
            $condition_gradebook_quiz_assess_grade = [
                'user_id' => $postData->id,
                'course_id' => $courseId,
                'section_id' => $sectionId,
                'quiz_assess_id' => $postData->reference_id,
                'quiz_assess_type' => $postData->quiz_type
            ];
            $this->Common_model->DeleteDB('gradebook_quiz_assess_grade',$condition_gradebook_quiz_assess_grade);
            $quizzData = $this->Common_model->getAllData('quizzes', 'gradebook_record', true, ['id'=>$condition_gradebook_quiz_assess_grade['quiz_assess_id']])->gradebook_record;
            
            $gradeDataSave = [];
            // 1=>Highest attempt, 2=>Last Attempt, 3=>Average
            switch($quizzData){
                case 1:
                    $gradeBooks =  $this->Common_model->getAllData('gradebook_quiz_assess_grade_attempts', 'MAX(grade) as grade,user_id,course_id,section_id,quiz_assess_id,quiz_assess_type', true, $condition_gradebook_quiz_assess_grade);
                     $this->__getFinalGradingSave($gradeBooks);
                break;

                case 2:
                    $gradeBooks =  $this->Common_model->getAllData('gradebook_quiz_assess_grade_attempts', '', true, $condition_gradebook_quiz_assess_grade,'id DESC','1');
                    $this->__getFinalGradingSave($gradeBooks);
                break;

                case 3:
                    $gradeBooks =  $this->Common_model->getAllData('gradebook_quiz_assess_grade_attempts', 'AVG(grade) as grade,user_id,course_id,section_id,quiz_assess_id,quiz_assess_type', true, $condition_gradebook_quiz_assess_grade);
                     $this->__getFinalGradingSave($gradeBooks);
                break;

            }
            clean_cache('api', 'tel_courseDetailsBySlugs_'.$postData->course_slug);
            clean_cache('api', 'tel_getLessonByLessonSlug_'.$postData->course_slug);
            clean_cache('api', 'tel_moduleDetailsBySlugs_'.$postData->course_slug);
        }
    }

    private function __getFinalGradingSave($gradeBookData = []){
        $gradeDataSave = [
            'user_id' => $gradeBookData->user_id,
            'course_id' => $gradeBookData->course_id,
            'section_id' => $gradeBookData->section_id,
            'quiz_assess_id' => $gradeBookData->quiz_assess_id,
            'quiz_assess_type' => $gradeBookData->quiz_assess_type,
            'grade' => $gradeBookData->grade,
            'created' => date('Y-m-d h:i:sa')
        ];
        $this->Common_model->InsertData('gradebook_quiz_assess_grade', $gradeDataSave);
    }

    private function __getQuizWait($quizzID)
    {
       /*return $this->db->query("SELECT quizzes.total_points,quizzes.id,count(quiz_section_questions.id) AS quiz_question_count, (quizzes.total_points / count(quiz_section_questions.id)) AS question_weight  from quizzes 
            LEFT JOIN quiz_section_questions ON (quizzes.id = quiz_section_questions.quiz_id) 
            LEFT JOIN questions ON (quiz_section_questions.question_id = questions.id AND questions.status=1 AND questions.delete_status = 1) WHERE quizzes.id = $quizzID AND quizzes.status = 1 AND quizzes.delete_status = 1")->row();*/

        return $this->db->query("SELECT quizzes.total_points,quizzes.id,count(questions.id) AS quiz_question_count, (quizzes.total_points / count(questions.id)) AS question_weight  from quizzes 
            LEFT JOIN quiz_section_questions ON (quiz_section_questions.quiz_id = quizzes.id) 
            LEFT JOIN questions ON (quiz_section_questions.question_id = questions.id AND questions.status=1 AND questions.delete_status = 1) WHERE quizzes.id = $quizzID AND quizzes.status = 1 AND quizzes.delete_status = 1")->row();

   }

    /*private function __getQuizWait($quizzID)
    {
        if ($total_points = $this->Common_model->getAllData('quizzes', 'total_points', true, ["id" => $quizzID, "status" => 1, "delete_status" => 1])->total_points) {
            return $total_points;
        } else {
            return false;
        }
    }*/

    private function __getQuestionsWait($answered_ids, $questionWeight,$answerData = [])
    {
        
        $this->load->model('Question_model');
        $answerResponse = $this->Question_model->getOptionByQuestion($answered_ids);
        $grade = 0;
        foreach ($answerResponse as $key => $value) {
            if ($value['weight'] != '') {
                    $grade += ($questionWeight * $value['weight']) / 100;
            }else{
                if($answerData[$value['id']] == 1){
                     $grade += ($questionWeight * 100) / 100;
                }
            }
        }
        return $grade;
        
    }

    /* End gradding code */
    
    public function getQuizMultipleLastResult($request) {
        $data = $this->Quiz_model->getQuizQuestionWithMultiAns($request->quiz_id, $request->user_id);
       //  pr($data);exit;
        if ($data['allSectionQuestionsCount'] == 0) {
            $this->response([
                'status' => false,
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        }


        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        }
    }

    /*CYK Quiz*/

    public function getSaveCYKResult($request=[])
    {

        $data = [];
        $courseId = $this->Common_model->getAllData('courses', 'id', true, array("slug" => trim($request->course_slug)))->id;
        $moduleId = $this->Common_model->getAllData('modules', 'id', true, array("slug" => trim($request->module_slug)))->id;
        $lessonId = $this->Common_model->getAllData('lessons', 'id', true, array("slug" => trim($request->lesson_slug)))->id;
        $sectionId = $this->Common_model->getAllData('course_sections', 'id', true, ["slug" => trim($request->section_slug), "status" => 1, "delete_status" => 1, "course_id" => $courseId])->id;

        $data['course_id'] = $courseId;
        $data['user_id'] = $request->id;
        $data['module_id'] = $moduleId;
        $data['lesson_id'] = $lessonId;
        $data['contain_id'] = $request->contain_id;
        $data['contain_type'] = $request->contain_type;
        $data['section_id'] = $sectionId;

        $course_activities =  $this->Common_model->getAllData('course_activities', 'id, contain_attempt, count(id) as record_count', true, $data);
        if($course_activities->record_count == 0){
            $data['contain_attempt'] = 1;
            $data['created'] = date('Y-m-d h:i:sa');
            if($select = $this->Common_model->InsertData('course_activities', $data)){
               $this->__getSaveCYKQuizResult($request, $courseId, $sectionId);
            }
        }else{
            $update_contain_attempt['contain_attempt'] = $course_activities->contain_attempt + 1;
            if($select = $this->Common_model->UpdateDB('course_activities', ['id' => $course_activities->id], $update_contain_attempt)){
                $this->__getSaveCYKQuizResult($request, $courseId, $sectionId);
            }
        }

        if (!empty($select)) {
            $view_result_btn = $this->Common_model->getAllData('quizzes', 'view_result_btn', true, ['id'=>$request->contain_id])->view_result_btn;
            $this->response([
                'view_result_button' => $view_result_btn,
                'status' => TRUE,
                'jwt_status' => TRUE,
                'message' => 'Activity saved successfully'
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                'message' => 'Activity could not completed'
            ], REST_Controller::HTTP_OK);
        }
    }

    private function __getSaveCYKQuizResult($postData = [], $courseId, $sectionId)
    {   
        
        $gradingEvalutionData = [];
        $answered_ids = [];
        foreach ($postData->quizFormData as $key => $value) {
            $gradingEvalutionData[] = [
                'user_id' => $postData->id,
                'course_id' => $courseId,
                'section_id' => $sectionId,
                'reference_id' => $postData->reference_id,
                'reference_type' => $postData->reference_type,
                'quize_type' => $postData->quize_type,
                'question_id' => $value->qId,
                'answered_id' => $value->optionId,
                'correct' => $value->correct
            ];
            $answered_ids[] = $value->optionId;
            $answerData[$valueOp->id] =  $qValue->correct;
        }

        $conditionRemove = [
            'user_id' => $postData->id,
            'course_id' => $courseId,
            'section_id' => $sectionId,
            'reference_id' => $postData->reference_id,
            'reference_type' => $postData->reference_type,
            'quize_type' => $postData->quiz_type
        ];
        if ($gradingEvalutionData) {
            $this->Common_model->DeleteDB('gradebooks_evaluation',$conditionRemove);
            $this->db->insert_batch('gradebooks_evaluation_log', $gradingEvalutionData);
            $this->db->insert_batch('gradebooks_evaluation', $gradingEvalutionData);
            $this->__getSaveCYKGrading($postData, $courseId, $sectionId, $answered_ids,$answerData); 
        }
    }


    public function __getSaveCYKGrading($postData = [], $courseId, $sectionId,$answered_ids = [],$answerData = [])
    {   
        $quesWeight = $this->__getQuizWait($postData->reference_id);
        $totalGradePoints = $this->__getQuestionsWait($answered_ids, $quesWeight->question_weight,$answerData);
        $saveGradeData = [
            'user_id' => $postData->id,
            'course_id' => $courseId,
            'section_id' => $sectionId,
            'quiz_assess_id' => $postData->reference_id,
            'quiz_assess_type' => $postData->quize_type,
            'grade' => $totalGradePoints,
            'created' => date('Y-m-d h:i:sa')
        ];

        if ($saveGradeData) {
            $this->Common_model->InsertData('gradebook_quiz_assess_grade_attempts', $saveGradeData);
            $condition_gradebook_quiz_assess_grade = [
                'user_id' => $postData->id,
                'course_id' => $courseId,
                'section_id' => $sectionId,
                'quiz_assess_id' => $postData->reference_id,
                'quiz_assess_type' => $postData->quize_type
            ];
            $this->Common_model->DeleteDB('gradebook_quiz_assess_grade',$condition_gradebook_quiz_assess_grade);
            $quizzData = $this->Common_model->getAllData('quizzes', 'gradebook_record', true, ['id'=>$condition_gradebook_quiz_assess_grade['quiz_assess_id']])->gradebook_record;
            
            $gradeDataSave = [];
            // 1=>Highest attempt, 2=>Last Attempt, 3=>Average
            switch($quizzData){
                case 1:
                    $gradeBooks =  $this->Common_model->getAllData('gradebook_quiz_assess_grade_attempts', 'MAX(grade) as grade,user_id,course_id,section_id,quiz_assess_id,quiz_assess_type', true, $condition_gradebook_quiz_assess_grade);
                     $this->__getFinalGradingSave($gradeBooks);
                break;

                case 2:
                    $gradeBooks =  $this->Common_model->getAllData('gradebook_quiz_assess_grade_attempts', '', true, $condition_gradebook_quiz_assess_grade,'id DESC','1');
                    $this->__getFinalGradingSave($gradeBooks);
                break;

                case 3:
                    $gradeBooks =  $this->Common_model->getAllData('gradebook_quiz_assess_grade_attempts', 'AVG(grade) as grade,user_id,course_id,section_id,quiz_assess_id,quiz_assess_type', true, $condition_gradebook_quiz_assess_grade);
                     $this->__getFinalGradingSave($gradeBooks);
                break;

            }
            clean_cache('api', 'tel_courseDetailsBySlugs_'.$postData->course_slug);
            clean_cache('api', 'tel_getLessonByLessonSlug_'.$postData->course_slug);
            clean_cache('api', 'tel_moduleDetailsBySlugs_'.$postData->course_slug);
        }
    }
    /*CYK QUiz end*/

}
