<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
include('headers.php');

//include Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class Demo extends REST_Controller {

    public function __construct() {
        parent::__construct();
         ini_set('display_errors', 1);
        $this->load->config('ion_auth');
        $this->lang->load('auth');
        //load models
        $this->load->model('Common_model');
        $this->load->model('Demo_model');
        $this->load->model('Ion_auth_model');
        //load helper
        //$this->load->helper('jwt');
        //load libraries
        $this->load->library('form_validation');
        //JWT AUTHENTICATION
        $data = json_decode(file_get_contents('php://input'));

        $courseTable = 'courses';
        $moduleTable = 'modules';
        $lessonTable = 'lessons';

        $db_token = $this->Common_model->getDataById('users', 'jwt_token', array("id" => $data->id));
        $request_token = $this->input->request_headers();

        // if ($request_token['token'] == '' || $db_token->jwt_token != $request_token['token']) {
        //     $this->response([
        //         'status' => FALSE,
        //         'jwt_status' => FALSE,
        //         'message' => 'Invalid token.',
        //             ], REST_Controller::HTTP_OK);
        // }
        // cache settings
        $this->config->set_item('cache_path', APPPATH . '/cache/api/courses');
        $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'tel_')
        );
    }

    public function index_post() {
        $request = json_decode(file_get_contents('php://input'));

        switch ($request->method) {
            case "getAllCourses":
                $this->getAllCourses($request);
                break;
            case "courseAssignListbyUserID":
                $this->courseAssignListbyUserID($request);
                break;
            case "courseDetailsBySlugs":
                $this->courseDetailsBySlugs($request);
                break;          
            case "makefavoriteCourse":
                $this->makefavoriteCourse($request);
                break;
            case "checkFavCourseByUserId":
                $this->checkFavCourseByUserId($request);
                break;
            case "myFavoriteCourseListByUserId":
                $this->myFavoriteCourseListByUserId($request);
                break;
            case "notificationsByUserId":
                $this->notificationsByUserId($request);
                break;
            case "getMessagesByUserId":
                $this->getMessagesByUserId($request);
                break;
            case "getMessageByMessageId":
                $this->getMessageByMessageId($request);
                break;
            case "courseActivityCompletionMethod":
                $this->courseActivityCompletionMethod($request);
                break;
            case "getLessonByLessonSlug":
                $this->getLessonByLessonSlug($request);
                break;
            case "getUserCourseActivityDetail":
                $this->getUserCourseActivityDetail($request);
                break;
            case "getUserCourseCompletionValue":
                $this->getUserCourseCompletionValue($request);
                break;
            case "getModuleListByCourseSlug":
                $this->getModuleListByCourseSlug($request);
                break;
            case "getStartLessonByModule":
                $this->getStartLessonByModule($request);
                break;
            case "getLessonOrders":
                $this->getLessonOrders($request);
                break;
        }
    }

    public function getAllCourses() {
        // cache settings
        $cache_key = 'getAllCourses';
        $data = $this->cache->get($cache_key);
        if (!$data) {
            $data = $this->Common_model->getAllData('courses', '*', '', array("delete_status" => 1));
            if ($data) {
                // save cache
                $this->cache->save($cache_key, $data, CACHE_LIFE_TIME);
            }
        }
        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'message' => 'Data found.',
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                'message' => 'No data found.'
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function checkFavCourseByUserId($request) {
        $select = $this->Common_model->getDataById('favorites', '*', array("course_id" => $request->course_id, "user_id" => $request->id));
        // echo    $this->db->last_query();
        if (!empty($select)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'fav_status' => TRUE,
                'message' => 'course is favorite.',
                    ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                'fav_status' => FALSE,
                'message' => 'course is not favorite.',
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function courseAssignListbyUserID($request) {
        $this->load->model('Course_model');
        $cache_key = 'courseAssignListbyUserID_' . $request->id;
       // $arr = $this->cache->get($cache_key);

        if (!$arr) {
            $arr = $this->Course_model->get_courses($request->id);

           /* foreach ($data as $key => $dat) {
                $arr[] = $dat;
                $all_data = $this->Common_model->getAllData('module_contain', 'course_id,module_id', '', array("course_id" => $dat->id), '', '', 'module_id');

                $comp_count = 0;
                foreach ($all_data as $check_data) {
                    $user_completed_data = $this->Common_model->getAllData('course_activities', 'module_id', '', array("user_id" => $request->id, "course_id" => $dat->id, "module_id" => $check_data->module_id, "lesson_id" => 0, "contain_id" => 0, "contain_type" => 0), '', '', 'user_id,course_id,module_id');

                    if (isset($user_completed_data[0]->module_id)) {
                        $comp_count += 1;
                    }
                }
                $arr[$key]->percent = round(($comp_count / sizeof($all_data)) * 100);
            }
            */
            if ($arr) {
                // save cache
                $this->cache->save($cache_key, $arr, CACHE_LIFE_TIME);
            }
        }

        //pr($arr); exit;
        
        

        if (!empty($arr)) {
            $this->response([
                'status' => TRUE,
                'message' => 'Data found.',
                'data' => $arr
                    ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'message' => 'No data found.'
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function courseDetailsBySlugs($request) {
        $this->load->model('Course_model');
        $cache_key = 'courseDetailsBySlugs_' . $request->id;
       // $arr = $this->cache->get($cache_key);

        if (!$arr) {
            $arr = $this->Course_model->get_course($request->id, $request->courseSlug, $request->sectionSlug);
        
            if ($arr) {
                // save cache
                $this->cache->save($cache_key, $arr, CACHE_LIFE_TIME);
            }
        }

        if (!empty($arr)) {
            $this->response([
                'status' => TRUE,
                'message' => 'Data found.',
                'data' => $arr
                    ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'message' => 'No data found.'
                    ], REST_Controller::HTTP_OK);
        }
    }


    public function notificationsByUserId($request) {
        $select = $this->Common_model->getAllData('notifications', '*', '', array("receiver_id" => $request->id));
        if (!empty($select)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'list' => $select
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function courseActivityCompletionMethod($request) {
        
        $data = [];
        $courseId = $this->Common_model->getAllData('courses', 'id', true, array("slug" => trim($request->course_slug)))->id;
        $moduleId = $this->Common_model->getAllData('modules', 'id', true, array("slug" => trim($request->module_slug)))->id;
        $lessonId = $this->Common_model->getAllData('lessons', 'id', true, array("slug" => trim($request->lesson_slug)))->id;

        if ($courseId && $moduleId && $lessonId && !$request->contain_id && !$request->contain_type) {
            $data['course_id'] = $courseId;
            $data['user_id'] = $request->id;
            $data['module_id'] = $moduleId;
            $data['lesson_id'] = $lessonId;
            $data['contain_id'] = 0;
            $data['contain_type'] = 0;
        } elseif ($courseId && $moduleId && !$lessonId && !$request->contain_id && !$request->contain_type) {
            $data['course_id'] = $courseId;
            $data['user_id'] = $request->id;
            $data['module_id'] = $moduleId;
            $data['lesson_id'] = 0;
        } elseif ($courseId && !$moduleId && !$lessonId && !$request->contain_id && !$request->contain_type) {
            $data['course_id'] = $courseId;
            $data['user_id'] = $request->id;
            $data['module_id'] = 0;
            $data['lesson_id'] = 0;
        } elseif ($courseId && $moduleId && $lessonId && $request->contain_id && $request->contain_type) {
            $data['course_id'] = $courseId;
            $data['user_id'] = $request->id;
            $data['module_id'] = $moduleId;
            $data['lesson_id'] = $lessonId;

            $data['contain_id'] = $request->contain_id;
            $data['contain_type'] = $request->contain_type;
        }
        $checkEntry = $this->Common_model->getAllData('course_activities', 'id', true, $data);
        $this->__getCheckQuizResult($request,$courseId,$checkEntry);

        if (!empty($checkEntry)) {
            $this->response([
                'status' => false,
                'jwt_status' => TRUE,
                'message' => 'Activity has been already completed'
                    ], REST_Controller::HTTP_OK);
        } else {

            $select = $this->Common_model->InsertData('course_activities', $data);
            if ($request->contain_id && $request->contain_type) {
                
            } else {
                // Check all lessons has been completed or not
                $assosiateLessons = $this->Common_model->getAllData('module_contain', 'lesson_id', false, ['course_id' => $courseId, 'module_id' => $moduleId]);

                $allassosiateLessonsCompleted = true;

                foreach ($assosiateLessons as $lessons) {

                    $dataParam['course_id'] = $courseId;
                    $dataParam['user_id'] = $request->id;
                    $dataParam['module_id'] = $moduleId;
                    $dataParam['lesson_id'] = $lessons->lesson_id;
                    if (!$this->Common_model->getAllData('course_activities', 'id', true, $dataParam)->id) {
                        $allassosiateLessonsCompleted = false;
                    }
                }
                // Record activity for module completion
                if (true == $allassosiateLessonsCompleted) {
                    $moduleParam['course_id'] = $courseId;
                    $moduleParam['user_id'] = $request->id;
                    $moduleParam['module_id'] = $moduleId;
                    $moduleParam['lesson_id'] = 0;
                    $moduleParam['contain_id'] = 0;
                    if (!$this->Common_model->getAllData('course_activities', 'id', true, $moduleParam)) {
                        $this->Common_model->InsertData('course_activities', $moduleParam);

                        // Check all modules has been completed or not
                        $assosiateModules = $this->Common_model->getAllData('course_contain', 'module_id', false, ['course_id' => $courseId]);
                        $allassosiateModulesCompleted = true;

                        foreach ($assosiateModules as $modules) {

                            $dataParam['course_id'] = $courseId;
                            $dataParam['user_id'] = $request->id;
                            $dataParam['module_id'] = $modules->module_id;
                            $dataParam['lesson_id'] = 0;
                            $dataParam['contain_id'] = 0;
                            if (!$this->Common_model->getAllData('course_activities', 'id', true, $dataParam)->id) {
                                $allassosiateModulesCompleted = false;
                            }
                        }

                        if (true == $allassosiateModulesCompleted) {
                            $courseParam['course_id'] = $courseId;
                            $courseParam['user_id'] = $request->id;
                            $courseParam['module_id'] = 0;
                            $courseParam['lesson_id'] = 0;
                            $courseParam['contain_id'] = 0;
                            if (!$this->Common_model->getAllData('course_activities', 'id', true, $courseParam)) {
                                $this->Common_model->InsertData('course_activities', $courseParam);
                            }
                        }
                    }
                }
            }

            if (!empty($select)) {
                $this->response([
                    'status' => TRUE,
                    'jwt_status' => TRUE,
                    'message' => 'Activity saved successfully'
                        ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => FALSE,
                    'jwt_status' => TRUE,
                    'message' => 'Activity could not completed'
                        ], REST_Controller::HTTP_OK);
            }
        }
    }

    //$this->__getSaveQuizResult($request,$courseId,$checkEntry);
    private function __getCheckQuizResult($postData = [], $courseId, $checkEntry) {
        if($postData->quize_type == 1){
            $conditions = [
                'user_id' => $postData->id,
                'course_id' => $courseId,
                'section_id' => $postData->section_id,
                'reference_id' => $postData->reference_id
            ];
            $this->Common_model->DeleteDB('gradebooks_evaluation',$conditions);
            $this->__getSaveQuizResult($postData, $courseId);
        }else{
            if(empty($checkEntry)){
                $this->__getSaveQuizResult($postData, $courseId);
            }
        }
    }

    private function __getSaveQuizResult($postData = [], $courseId){
        $gradingEvalutionData = [];
        foreach ($postData->quizFormData as $key => $value) {
            $gradingEvalutionData[] = [
                'user_id' => $postData->id,
                'course_id' => $courseId,
                'section_id' => $postData->section_id,
                'reference_id' => $postData->reference_id,
                'reference_type' => $postData->reference_type,
                'quize_type' => $postData->quize_type,
                'question_id' => $value->qId,
                'answered_id' => $value->optionId,
            ];
        }
        if($gradingEvalutionData){
            $this->db->insert_batch('gradebooks_evaluation', $gradingEvalutionData);
        }
    }

    public function getUserCourseActivityDetail($request) {
        $cache_key = 'getUserCourseActivityDetail_' . $request->slug . "_" . $request->id;
        $data = $this->cache->get($cache_key);
        if (!$data) {
            $this->load->model('Course_Model');
            $data = $this->Course_Model->getCourseMenuData($request->slug, $request->id);
            if ($data) {
                // save cache
                $this->cache->save($cache_key, $data, CACHE_LIFE_TIME);
            }
        }
        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function getLessonByLessonSlug($request) {

        $cache_key = 'getLessonByLessonSlug_' . $request->lessonSlugg . "_" . $request->id;
        $data = $this->cache->get($cache_key);

        if (!$data) {
            $data = $this->Common_model->getAllData('lessons', '*', true, array("slug" => $request->lessonSlugg, "delete_status" => 1));
            if ($data) {
                // save cache
                $this->cache->save($cache_key, $data, CACHE_LIFE_TIME);
            }
        }

        $course = $this->Common_model->getAllData('courses', 'id', true, array("slug" => trim($request->courseSlugg)));
        $module = $this->Common_model->getAllData('modules', 'id', true, array("slug" => trim($request->moduleSlugg)));
        $lesson = $this->Common_model->getAllData('lessons', 'id', true, array("slug" => trim($request->lessonSlugg)));
       
        $courseId = $course->id;
        $moduleId = $module->id;
        $lessonId = $lesson->id;

        $checkEntry = $this->Common_model->getAllData('course_activities', 'id', true, array(
            'course_id' => $courseId, 'module_id' => $moduleId,
            'lesson_id' => $lessonId, 'contain_id' => 0, 'user_id' => $request->id
        ));

        if (!empty($checkEntry)) {
            $isCompleted = true;
        } else {
            $isCompleted = false;
        }
        //$arr = $this->Common_model->DJoin('lessons.slug , module_contain.order_id', 'lessons', 'module_contain', 'module_contain.lesson_id = lessons.id', true, '', array("module_contain.course_id" => $courseId, "module_contain.module_id" => $moduleId, "module_contain.lesson_id" => $lessonId, "module_contain.order_id" => $lessonId), 'module_contain.order_id ');


//pr($data); exit;


        if (!empty($data)) {
            $this->response([
                'status' => true,
                'message' => 'Data found.',
                'is_completed' => $isCompleted,
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => false,
                'message' => 'No data found.',
                'is_completed' => false
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function getMessagesByUserId($request) {
        $select = $this->Common_model->getAllData('messages', '*', '', array("receiver_id" => $request->id));
        if (!empty($select)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'list' => $select
                    ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                'list' => $select
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function getMessageByMessageId($request) {
        $select = $this->Common_model->getAllData('messages', '*', true, array("id" => $request->messageId, "receiver_id" => $request->id));
        if (!empty($select)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'list' => $select
                    ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                'list' => $select
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function myFavoriteCourseListByUserId($request) {

        $data = $this->Common_model->DJoin('courses.id,courses.name,courses.status,courses.description,courses.course_img', 'favorites', 'courses', 'favorites.course_id=courses.id', '', '', array("courses.delete_status" => 1, "favorites.user_id" => $request->id), '', 'favorites.course_id');

        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'message' => 'Data found.',
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                'message' => 'No data found.'
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function makefavoriteCourse($request) {
        $data = [
            'user_id' => trim($request->id),
            'course_id' => trim($request->course_id),
            'module_id' => trim($request->module_id),
            'lesson_id' => trim($request->lesson_id),
        ];
        $select = $this->Common_model->getDataById('favorites', '*', array("course_id" => $request->course_id, "user_id" => $request->id));
        if (!empty($select)) {
            $delete = $this->Common_model->DeleteDB('favorites', array("course_id" => $request->course_id, "user_id" => $request->id));
            if ($delete) {
                http_response_code(200);
                $this->response([
                    'status' => TRUE,
                    'jwt_status' => TRUE,
                    'fav_status' => FALSE,
                    'message' => 'Data deleted successfully.'
                        ], REST_Controller::HTTP_OK);
            } else {
                http_response_code(200);
                $this->response([
                    'status' => FALSE,
                    'jwt_status' => TRUE,
                    'message' => 'Something went wrong!!!'
                        ], REST_Controller::HTTP_OK);
            }
        } else {
            if ($this->Common_model->InsertData('favorites', $data)) {
                http_response_code(200);
                $this->response([
                    'status' => TRUE,
                    'jwt_status' => TRUE,
                    'fav_status' => TRUE,
                    'message' => 'Data inserted successfully.'
                        ], REST_Controller::HTTP_OK);
            } else {
                http_response_code(200);
                $this->response([
                    'status' => FALSE,
                    'jwt_status' => TRUE,
                    'message' => 'Something went wrong!!!'
                        ], REST_Controller::HTTP_OK);
            }
        }
    }

    public function getUserCourseCompletionValue($request) {

        $cache_key = 'getUserCourseCompletionValue_' . $request->course_slug . "_" . $request->id;
        $all_data = $this->cache->get($cache_key);

        $course = $this->Common_model->getAllData('courses', 'id', true, array("slug" => trim($request->course_slug)));

        if (!$all_data) {
            $all_data = $this->Common_model->getAllData('module_contain', 'course_id,module_id', '', array("course_id" => $course->id), '', '', 'module_id');

            if ($all_data) {
                // save cache
                $this->cache->save($cache_key, $all_data, CACHE_LIFE_TIME);
            }
        }

        $comp_count = 0;
        foreach ($all_data as $check_data) {
            $user_completed_data = $this->Common_model->getAllData('course_activities', 'module_id', '', array("user_id" => $request->id, "course_id" => $course->id, "module_id" => $check_data->module_id, "lesson_id" => 0, "contain_id" => 0, "contain_type" => 0), '', '', 'user_id,course_id,module_id');

            if (isset($user_completed_data[0]->module_id)) {
                $comp_count += 1;
            }
        }
        $percent = ($comp_count / sizeof($all_data)) * 100;
        if (!empty($all_data)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'message' => 'Data found.',
                'percentage' => round($percent)
                    ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                'message' => 'No data found.',
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function getModuleListByCourseSlug($request) {

        $cache_key = 'getModuleListByCourseSlug_' . $request->course_slug . "_" . $request->id;
        $data = $this->cache->get($cache_key);

        $course = $this->Common_model->getAllData('courses', 'id', true, array("slug" => trim($request->course_slug)));

        if (!$data) {
            $data = $this->Common_model->DJoin('modules.id, modules.slug', 'course_contain', 'modules', 'course_contain.module_id = modules.id', true, '', array("course_contain.course_id" => $course->id), 'course_contain.order_id');
            if ($data) {
                // save cache
                $this->cache->save($cache_key, $data, CACHE_LIFE_TIME);
            }
        }

        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'message' => 'Data found.',
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                'message' => 'No data found.',
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function getSlugId($table, $slug) {
        return $data = $this->Common_model->getAllData($table, 'id', true, array("slug" => trim($slug)))->id;
    }

    public function getLessonOrders($request) {


        $moduleId = $this->getSlugId($table = 'modules', $request->module_slug);
        $courseId = $this->getSlugId($table = 'courses', $request->course_slug);
        $lessonId = $this->getSlugId($table = 'lessons', $request->lesson_slug);


        $next = $this->Common_model->DJoin('lessons.slug , module_contain.order_id', 'lessons', 'module_contain', 'module_contain.lesson_id = lessons.id', true, '', array("module_contain.course_id" => $courseId, "module_contain.module_id" => $moduleId, "module_contain.lesson_id > " => $lessonId), 'module_contain.order_id desc', '', '1');
        
        $previous = $this->Common_model->DJoin('lessons.slug , module_contain.order_id', 'lessons', 'module_contain', 'module_contain.lesson_id = lessons.id', true, '', array("module_contain.course_id" => $courseId, "module_contain.module_id" => $moduleId, "module_contain.lesson_id < " => $lessonId), 'module_contain.order_id ', '', '1');
          echo    $this->db->last_query();
        //pr($arr);exit;
//        $data = [];
//        foreach ($arr as $key => $val) {
//            $data[$val->order_id] = $val->slug;
//        }
        // SELECT * FROM Example WHERE id > 3 ORDER BY id LIMIT 1
        $data = [];
        $data['next'] = $next->slug;
        $data['previous'] = $previous->slug;

        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'message' => 'Data found.',
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                'message' => 'No data found.',
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function getStartLessonByModule($request) {
        $moduleId = $this->getSlugId($table = 'modules', $request->module_slug);
        $courseId = $this->getSlugId($table = 'courses', $request->course_slug);

        $arr = $this->Common_model->DJoin('lessons.slug , module_contain.order_id', 'lessons', 'module_contain', 'module_contain.lesson_id = lessons.id', true, '', array("module_contain.course_id" => $courseId, "module_contain.module_id" => $moduleId), 'module_contain.order_id ');
        //  echo    $this->db->last_query();
        //   pr($arr);exit;
//        $data = [];
//        foreach ($arr as $key => $val) {
//            $data[$val->order_id] = $val->slug;
//        }
        if (!empty($arr)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'message' => 'Data found.',
                'data' => $arr
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                'message' => 'No data found.',
                    ], REST_Controller::HTTP_OK);
        }
    }

}
