<?php

include('headers.php');
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//include Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class Notifications extends REST_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->config('ion_auth');
        $this->lang->load('auth');
        //load models
        $this->load->model('Common_model');
        $this->load->model('Notifications_model');
        $this->load->model('Ion_auth_model');
        //load helper
        $this->load->helper('jwt');
        // $this->load->helper('validation');
        //load libraries
        $this->load->library('form_validation');
        //JWT AUTHENTICATION
        $data = json_decode(file_get_contents('php://input'));

        $db_token = $this->Common_model->getDataById('users', 'jwt_token', array("id" => $data->id));
        $request_token = $this->input->request_headers();
        $request_token['token'];

        if ($request_token['token'] == '' || $db_token->jwt_token != $request_token['token']) {
            $this->response([
                'status' => FALSE,
                'jwt_status' => FALSE,
                'message' => 'Invalid token.',
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function index_post() {
        $request = json_decode(file_get_contents('php://input'));

        switch ($request->method) {
            case "notificationsListByLimit":
                $this->notificationsListByLimit($request);
                break;
            case "notificationsCount":
                $this->notificationsCount($request);
                break;
            case "notificationsList":
                $this->notificationsList($request);
                break;
            case "updateReadStatus":
                $this->updateReadStatus($request);
                break;
        }
    }

    public function notificationsListByLimit($request) {


        $data = $this->Common_model->getAllData('notifications', '*', '', array("receiver_id" => $request->id), 'id DESC', '10');
        $notification_Id = [];
        foreach ($data as $key => $val) {
            if ($val->is_read == 0) {
                array_push($notification_Id, $val->id);
            }
        }

        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'message' => 'Data found.',
                'data' => $data,
                'notifications' => $notification_Id
                    ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                'message' => 'No data found.'
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function notificationsCount($request) {
        $notification_ids = [];
        $i=0;
        $data = $this->Common_model->getAllData('notifications', 'id', false, array("receiver_id" => $request->id, "is_read" => 0), 'id DESC');
        foreach($data as $key => $val){
            foreach($val as $k => $v){
                $notification_ids[$i] = $v;
                $i++;
            }
        }
        $count = count($notification_ids);
        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'message' => 'Data found.',
                'count' => $count,
                'data' => $notification_ids
                    ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                'message' => 'No data found.'
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function notificationsList($request) {
        $data = $this->Common_model->getAllData('notifications', '*', '', array("receiver_id" => $request->id), 'id DESC');
        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'message' => 'Data found.',
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                'message' => 'No data found.'
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function updateReadStatus($request) {

        $this->db->where_in('id', $request->notification_id);

        $data = $this->db->update('notifications', array("is_read" => 1));
        //$this->db->last_query();
        if ($data) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'message' => 'Updated successfully.',
                'data' => $data
                    ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
                'message' => 'Could not updated.'
                    ], REST_Controller::HTTP_OK);
        }
    }

}
