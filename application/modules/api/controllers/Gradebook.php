<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
include('headers.php');
//include Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';
class Gradebook extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        
       // ini_set('display_errors', 0);
        $this->load->config('ion_auth');
        $this->lang->load('auth');

        //load models
        $this->load->model('Common_model');
        $this->load->model('courses/Courses_modal');
       
        $this->load->model('Ion_auth_model');
        
        //load helper
        $this->load->helper('jwt');
        
        //load libraries
        $this->load->library('form_validation');
        
        //JWT AUTHENTICATION
        $data = json_decode(file_get_contents('php://input'));

        $db_token = $this->Common_model->getDataById('users', 'jwt_token', array("id" => $data->id));
        $request_token = $this->input->request_headers();

        if ($request_token['token'] == '' || $db_token->jwt_token != $request_token['token']) {
            $this->response([
                'status' => FALSE,
                'jwt_status' => FALSE,
                'message' => 'Invalid token.',
            ], REST_Controller::HTTP_OK);
        }
        
        // cache settings
        $this->config->set_item('cache_path', APPPATH . '/cache/api/');
        $this->load->driver(
            'cache',
            array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'tel_')
        );
    }

    public function index_post()
    {
        $request = json_decode(file_get_contents('php://input'));
        switch ($request->method) {
            case "getGradebookData":
                $this->getGradebookData($request);
                break;
            case "getInstructorGradebookData":
                $this->getInstructorGradebookData($request);
                break;
            
        }
    }
    # function to fectch student gradebook

    public function getGradebookData($request)
    {
       
        $courseId = $this->Common_model->getAllData('courses', 'id', true, array("slug" => trim($request->courseSlug)))->id;
        $sectionId = $this->Common_model->getAllData('course_sections', 'id', true, ["slug" => trim($request->sectionSlug), "status" => 1, "delete_status" => 1, "course_id" => $courseId])->id;
             
        $this->load->model('gradebook/Gradebook_model');
        $data = $this->Gradebook_model->studentGradebook($courseId, $sectionId, $request->id);
        
        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'data' => $data
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
            ], REST_Controller::HTTP_OK);
        }
    }


    public function getInstructorGradebookData($request)
    {       
        $this->load->model('gradebook/Gradebook_model');
        $this->load->model('gradebook/common_model');
        $data = $this->Gradebook_model->get_gradebook($request->courseId, $request->sectionId);
        
        if (!empty($data)) {
            $this->response([
                'status' => TRUE,
                'jwt_status' => TRUE,
                'data' => $data
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'jwt_status' => TRUE,
            ], REST_Controller::HTTP_OK);
        }
    }




}
