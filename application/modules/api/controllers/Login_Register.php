<?php

include('headers.php');
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//include Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class Login_Register extends REST_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->config('ion_auth');
        $this->lang->load('auth');
        //load models
        $this->load->model('users/Users_modal');
        $this->load->model('common_model');
        //load helper
        $this->load->helper('jwt');
        $this->load->helper('validation');
        //load libraries
        $this->load->library('form_validation');
        //get data from web api
        $data = file_get_contents('php://input');
    }

    public function index_post() {
        $request = json_decode(file_get_contents('php://input'));
        switch ($request->method) {
            case "Login":
                $this->login($request);
                break;
            case "register":
                $this->register($request);
                break;
            case "forgotPassword":
                $this->forgotPassword($request);
                break;
            case "updateJWT":
                $this->updateJWT($request);
                break;
            case "checkForgotPasswordUser":
                $this->checkForgotPasswordUser($request);
                break;
            case "newPassword":
                $this->newPassword($request);
                break;
        }
    }

    public function updateJWT($request) {
        $this->common_model->UpdateDB('users', array("id" => $id), array("jwt_token" => $token));
        http_response_code(200);
        $this->response([
            'status' => TRUE,
            'message' => 'token updated.',
                ], REST_Controller::HTTP_OK);
    }

    public function login($request) {
        $remember = (bool) $request->remember;
        if ($this->ion_auth->login(trim($request->username), trim($request->password), $remember)) {
            $data = $this->common_model->DJoin('users.id,users.username,users.email,users.street_address_1 ,users.city , users.state ,users.street_address_2, users.secondary_email,users.active,users.first_name,users.middle_name,users.last_name,users.company,users.phone,users.bio,users.user_img,roles.id as role_id,roles.name as role_name', 'users', 'containers_users_roles', 'containers_users_roles.user_id = users.id and containers_users_roles.container_id = ' . $request->container_id, true, ['roles' => 'roles.id = containers_users_roles.role_id'], array("users.email" => trim($request->username)));
            
        
            $token = ["id" => $data->id, "unique" => uniqid()];
            
            $this->common_model->UpdateDB('users', array("id" => $data->id), array("jwt_token" => JWT::encode($token, $this->config->item('jwt_key'))));
            http_response_code(200);
            if (!empty($data->street_address_1) && (!empty($data->street_address_2))) {
                $data->address = $data->street_address_1 . ' , ' . $data->street_address_2;
            } elseif (!empty($data->street_address_1) && (empty($data->street_address_2))) {
                $data->address = $data->street_address_1;
            }else{ 
                 $data->address = ''; 
            }
            
            $this->response([
                'status' => TRUE,
                'message' => 'User found.',
                'data' => $data,
                'token' => JWT::encode($token, $this->config->item('jwt_key'))
                    ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'message' => 'No user were found.'
                    ], REST_Controller::HTTP_OK);
        }
    }
    
    
    

    public function register($request) {
        $realPostdata = [
            'username' => trim($request->email),
            'first_name' => trim($request->first_name),
            'middle_name' => trim($request->middle_name),
            'last_name' => trim($request->last_name),
            'email' => trim($request->email),
            'phone' => trim($request->phone),
            'password' => $this->ion_auth->hash_password($request->password),
            'active' => 1,
            'unique_id' => $this->Users_modal->generate_unique_user_id()
        ];

        if ($this->common_model->InsertData('users', $realPostdata)) {
            http_response_code(200);
            $this->response([
                'status' => TRUE,
                'message' => 'Data inserted successfully.'
                    ], REST_Controller::HTTP_OK);
        } else {
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'message' => 'Something went wrong!!!'
                    ], REST_Controller::HTTP_OK);
        }
        //check if the user data exists
    }

    public function forgotPassword($request) {
        $user_exist = $this->common_model->getDataById('users', 'id,first_name,last_name', array("username" => trim($request->username)));
        
      // echo   $this->db->last_query();
       
       // echo  $request->domain;
       
    //   exit;
        
        

        if (empty($user_exist)) {
            $msg = 'User not found';
            http_response_code(200);
            $this->response([
                'status' => FALSE,
                'message' => $msg
                    ], REST_Controller::HTTP_OK);
        } else {
            $code = $user_exist->id . "" . $request->username . time();
            $code = md5($code);
            $data['forgotten_password_code'] = $code;
            $this->common_model->UpdateDB('users', ['id' => $user_exist->id], $data);
            $encrypted_email = urlencode(base64_encode(trim($request->username)));
            $email_data['template_code'] = 'FORGOT_PASSWORD';
            $email_data['variables']['user_name'] = $user_exist->first_name . ' ' . $user_exist->last_name;
            $email_data['email'] = $request->username;
            $siteDomain = $request->domain;
           // $siteDomain = 'localhost:4200';

           // echo $siteDomain;
           // exit('appp');
            
            //   $email_data['variables']['forgot_password_link'] = sprintf(lang('email_forgot_password_subheading'), anchor('http://api/Forgot_Pasjsword/reset_password_API/' . $encrypted_email, lang('email_forgot_password_link')));
            
//            $email_data['variables']['forgot_password_link'] = sprintf(lang('email_forgot_password_subheading'), anchor( 'http://'. $siteDomain . '/newpassword/' . $code, lang('email_forgot_password_link')));
            
                $email_data['variables']['forgot_password_link'] = sprintf(lang('email_forgot_password_subheading'), anchor( 'http://'.$siteDomain . '/newpassword/' . $code, lang('email_forgot_password_link')));

            if (sendgrid_email($email_data)) {
                $msg = 'Please Check Your Email to Verify your Account';
                http_response_code(200);
                $this->response([
                    'status' => TRUE,
                    'message' => $msg
                        ], REST_Controller::HTTP_OK);
            } else {
                $msg = 'Email Can not Send';
                http_response_code(200);
                $this->response([
                    'status' => FALSE,
                    'message' => $msg
                        ], REST_Controller::HTTP_OK);
            }
        }
    }

    public function user_post() {
        $userData = array();
        $userData['first_name'] = $this->post('first_name');
        $userData['last_name'] = $this->post('last_name');
        $userData['email'] = $this->post('email');
        $userData['phone'] = $this->post('phone');
        if (!empty($userData['first_name']) && !empty($userData['last_name']) && !empty($userData['email']) && !empty($userData['phone'])) {
            //insert user data
            $insert = $this->Users_modal->insert($userData);

            //check if the user data inserted
            if ($insert) {
                //set the response and exit
                $this->response([
                    'status' => TRUE,
                    'message' => 'User has been added successfully.'
                        ], REST_Controller::HTTP_OK);
            } else {
                //set the response and exit
                $this->response("Some problems occurred, please try again.", REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            //set the response and exit
            //BAD_REQUEST (400) being the HTTP response code
            $this->response("Provide complete user information to create.", REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function checkForgotPasswordUser($request) {
        if (!empty($request->code)) {
            $user_exist = $this->common_model->getDataById('users', 'id,first_name,last_name', array("forgotten_password_code" => trim($request->code)));
            if (!empty($user_exist)) {
                $this->response([
                    'status' => 'true',
                    'message' => 'User found.',
                    'userData' => $user_exist
                        ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => 'false',
                    'message' => 'User not found.'
                        ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response([
                'status' => 'false',
                'message' => 'User not found.'
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function newPassword($request) {
        
        
       // pr($request);
     //   exit;
        
        
        if (!empty($request)) {
            if ($request->newPassword == $request->cnfPassword) {
                $data['password'] = $this->ion_auth->hash_password($request->newPassword);
                
              //  pr($data); exit('apple');
                
                
                $update = $this->common_model->UpdateDB('users', ['forgotten_password_code' => $request->code], $data);
                
            //  echo  $this->db->last_query();
              //exit;
                
                
                if ($update) { //echo "up";// exit;
                    $this->response([
                        'status' => true,
                        'msg' => 'Password has been updated sucessfully.'
                            ], REST_Controller::HTTP_OK);
                } else { //echo "down"; exit;
                    $this->response([
                        'status' => false,
                        'msg' => 'Error ! Please try again.'
                            ], REST_Controller::HTTP_OK);
                }
            } else { 
                $this->response([
                    'status' => false,
                    'msg' => 'Password does not match.'
                        ], REST_Controller::HTTP_OK);
            }
        }
    }

    public function user_put() {
        $userData = array();
        $id = $this->put('id');
        $userData['first_name'] = $this->put('first_name');
        $userData['last_name'] = $this->put('last_name');
        $userData['email'] = $this->put('email');
        $userData['phone'] = $this->put('phone');
        if (!empty($id) && !empty($userData['first_name']) && !empty($userData['last_name']) && !empty($userData['email']) && !empty($userData['phone'])) {
            //update user data
            $update = $this->API_User->update($userData, $id);

            //check if the user data updated
            if ($update) {
                //set the response and exit
                $this->response([
                    'status' => TRUE,
                    'message' => 'User has been updated successfully.'
                        ], REST_Controller::HTTP_OK);
            } else {
                //set the response and exit
                $this->response("Some problems occurred, please try again.", REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            //set the response and exit
            $this->response("Provide complete user information to update.", REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function user_delete($id) {
        //check whether post id is not empty
        if ($id) {
            //delete post
            $delete = $this->Users_modal->delete($id);

            if ($delete) {
                //set the response and exit
                $this->response([
                    'status' => TRUE,
                    'message' => 'User has been removed successfully.'
                        ], REST_Controller::HTTP_OK);
            } else {
                //set the response and exit
                $this->response("Some problems occurred, please try again.", REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            //set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No user were found.'
                    ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

}
