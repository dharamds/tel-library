<?php
header('Access-Control-Allow-Origin: * ');
header("Access-Control-Allow-Headers:X-API-KEY, token, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Content-Type: application/json; charset=UTF-8");