<?php
include('headers.php');
if (!defined('BASEPATH')) exit('No direct script access allowed');

//include Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class Users extends REST_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->config('ion_auth');
		$this->lang->load('auth');
		//load models
		$this->load->model('Common_model');
		$this->load->model('users/Users_modal');
		$this->load->model('Ion_auth_model');
		//load helper
		 $this->load->helper('jwt');
		//load libraries
		$this->load->library('form_validation');
		//JWT AUTHENTICATION
		$data = json_decode(file_get_contents('php://input'));
		$db_token = $this->Common_model->getDataById('users', 'jwt_token', array("id" => $data->id));
		$request_token = $this->input->request_headers();
		
		if ($request_token['token']=='' || $db_token->jwt_token != $request_token['token']) {
//                     echo $request_token['token']; echo nl2br("\n");
//                echo $db_token->jwt_token; echo nl2br("\n");
//			exit('ff');
			$this->response([
				'status' => FALSE,
				'jwt_status' => FALSE,
				'message' => 'Invalid token.',
			], REST_Controller::HTTP_OK);
		}
	}

	public function index_post()
	{
		$request = json_decode(file_get_contents('php://input'));
		switch ($request->method) {
			case "UsersList":
				$this->UsersList($request);
				break;
			case "usersListbyID":
				$this->usersListbyID($request);
				break;
		}
	}

	public function UsersList()
	{
		$data = $this->Common_model->getAllData('users', 'id,username,email,secondary_email,active,first_name,middle_name,last_name,company,phone,bio,user_img','' ,array("delete_status" => 1));

		if (!empty($data)) {
			$this->response([
				'status' => TRUE,
				'jwt_status' => TRUE,
				'message' => 'Data found.',
				'data' => $data
			], REST_Controller::HTTP_OK);
		} else {
			http_response_code(200);
			$this->response([
				'status' => FALSE,
				'jwt_status' => TRUE,
				'message' => 'No data found.'
			], REST_Controller::HTTP_OK);
		}
	}

	public function usersListbyID($request)
	{
		$data = $this->Common_model->getAllData('users', 'id,username,email,secondary_email,active,first_name,middle_name,last_name,company,phone,bio,user_img','' ,array("delete_status" => 1,"id" => $request->id));
		//pr($data,"j");
		if (!empty($data)) {
			$this->response([
				'status' => TRUE,
				'jwt_status' => TRUE,
				'message' => 'Data found.',
				'data' => $data
			], REST_Controller::HTTP_OK);
		} else {
			http_response_code(200);
			$this->response([
				'status' => FALSE,
				'jwt_status' => TRUE,
				'message' => 'No data found.'
			], REST_Controller::HTTP_OK);
		}
	}
}
