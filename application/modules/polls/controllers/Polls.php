<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Polls extends MY_Controller
{

    public function __construct()
    {

        //ini_set('display_errors', 1);
        parent::__construct();
        $this->load->module('template');
        $this->load->model(['Common_model', 'Polls_model']);
        $this->load->helper(array('html', 'form'));
        $this->load->library('form_validation');

        if (!$this->ion_auth->logged_in()) :
            redirect('users/auth', 'refresh');
        endif;
          // get controller permissions
          if ($this->current_user_permissions = $this->get_permissions()) {
            $this->add_permission = $this->current_user_permissions->add_per;
            $this->edit_permission = $this->current_user_permissions->edit_per;
            $this->delete_permission = $this->current_user_permissions->delete_per;
            $this->view_permission = $this->current_user_permissions->view_per;
            $this->list_permission = $this->current_user_permissions->list_per;
        }
    }

    /**
     * index method
     * @description this function use to display list of polls
     * @return void
     */
    public function index()
    {
        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Polls', 'link' => '', 'class' => 'active'];
        $data['page'] = "polls/list";
        if (!$this->list_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect('users/auth', 'refresh');
        endif;
        $data['permissions'] = $this->current_user_permissions;   

        $this->template->template_view($data);
    }

    /**
     * getLists method 
     * @description this function called via ajax request, use to display list of course
     * @return void
     */
    public function getLists()
    {
        $pollsData = $this->Polls_model->getRows($_POST);
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $pollsData['total'],
            "recordsFiltered" => $pollsData['total'],
            "data" => $pollsData['result'],
        );
        echo json_encode($output);
    }

    /**
     * add/edit method 
     * @description this method is use to add/edit new poll
     * @return void
     */
    public function add($id = NULL)
    {   
        if (!$this->add_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect($_SERVER['HTTP_REFERER'], 'refresh');
        endif;

        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Polls', 'link' => base_url('polls'), 'class' => ''];
        if ($this->input->post()) {

            $this->form_validation->set_rules('title', 'Title', 'trim|required');
            $this->form_validation->set_rules('question', 'Question', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $data['page'] = "polls/add";
                set_flashdata('error', 'Unable to add poll, try again');
                $this->template->template_view($data);
            } else {

                $data = array(
                    'created_by' => $this->session->userdata('user_id'),
                    'title' => post('title'),
                    'long_title' => post('long_title'),
                    'question' => post('question'),
                    'status' => post('status')
                );
                if (post('poll_id') != '') {

                    $this->Common_model->UpdateDB('polls', array("id" => post('poll_id')), $data);
                    $lastInsertId = post('poll_id');
                } else {
                    $this->Common_model->InsertData('polls', $data);
                    $lastInsertId = $this->db->insert_id();
                }
                $resp = post('poll_response');
                foreach ($resp as $key => $value) {
                    $poll_resp[$key]['poll_id'] = $lastInsertId;
                    $poll_resp[$key]['options'] = $value;
                }
               
                if (post('poll_id') != '') {
                    $this->Common_model->DeleteDB('polls_options', array("poll_id" => post('poll_id')));
                }

                $this->Common_model->InsertBatchData('polls_options', $poll_resp);
                echo $lastInsertId;
            }
        } else if (!empty($id)) {

            $data['page'] = "polls/add";
            $data['PollsData'] = $this->Common_model->DJoin('polls.title,polls.long_title,polls.question,polls.total_votes,polls.status,polls.delete_status,polls_options.options', 'polls', 'polls_options', 'polls.id=polls_options.poll_id', '', '', ['polls.delete_status' => 1, "polls.id" => $id]); // for poll type = 3
            $data['rescnt'] = count($data['PollsData']);
            $data['poll_id'] = $id;
            $this->template->template_view($data);
        } else {
            $data['breadcrumb'][]           = ['title' => 'Add', 'link' => '', 'class' => 'active'];
            $data['page'] = "polls/add";
            $data['categoryData'] = $this->Common_model->getAllData('categories', '*', '', ['type' => 3]); // for poll type = 3

            $tagData = $this->Common_model->getAllData('tags', '*', '', ['type' => 1, 'delete_status' => 1]); // for poll type = 3
            $tagDataStr = "[ ";
            foreach ($tagData as $key => $tag) {
                $tagDataStr .= "'" . $tag->name . "', ";
            }
            $tagDataStr = substr($tagDataStr, 0, -2);
            $tagDataStr .= " ]";
            $data['tags'] = $tagDataStr;

            $this->template->template_view($data);
        }
    }

    public function edit($id = NULL)
    {
        if (!$this->edit_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect($_SERVER['HTTP_REFERER'], 'refresh');
        endif;

        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Polls', 'link' => base_url('polls'), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Edit', 'link' => '', 'class' => ''];
        if ($id != null) {
            $data['page'] = "polls/add";
            $data['PollsData'] = $this->Common_model->DJoin('polls.title,polls.long_title,polls.question,polls.total_votes,polls.status,polls.delete_status,polls_options.options', 'polls', 'polls_options', 'polls.id=polls_options.poll_id', '', '', ['polls.delete_status' => 1, "polls.id" => $id]); // for poll type = 3
            $data['rescnt'] = count($data['PollsData']);
            $data['poll_id'] = $id;
        }
        $data['page'] = "polls/add";
        $this->template->template_view($data);
    }

    public function setAllSelected()
    {
        $selectedDataids = array_filter(explode("|", $this->input->post('selectedData')));
        $data = [];
        $status = $this->input->post('status');
        foreach ($selectedDataids as $val) {
            $data['status'] = $status;
            $this->Polls_model->update_status($val, $data);
        }
        echo json_encode(['msg' => 'Updated']);
        exit;
    }
    /**
     * delete method
     * @description this function use to delete tempray record 
     * @return void
     */
    function delete()
    {
        if (!$this->delete_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect($_SERVER['HTTP_REFERER'], 'refresh');
        endif;

       
        $id = $this->uri->segment(3);
        $data = ['delete_status' => 0];
        if ($id) {
            $this->Common_model->UpdateDB('polls', ['id' => $id], $data);
            $this->Common_model->UpdateDB('polls_options', ['poll_id' => $id], $data);
            $this->session->set_flashdata('success', $this->ion_auth->messages());
            $msg = "deleted";
            echo json_encode(array("msg" => $msg));
            exit;
        } else {
            set_flashdata('error', 'Unable to delete poll, try again');
            redirect('polls/', 'refresh');
        }
    }

    public function detail()
    {   
        if (!$this->view_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect($_SERVER['HTTP_REFERER'], 'refresh');
        endif;


        $id = $this->uri->segment(3);
        if (isset($id)) {
            $data['pollsData'] = $this->Common_model->getDataById('polls', '*', ['id' => $id]);
            $data['optionsData'] = $this->Common_model->getAllData('polls_options', '*', '', ['poll_id' => $id, 'delete_status' => 1]);

            $data['page'] = "polls/detail";
            $this->template->template_view($data);
        }
    }

    public function removeResponse()
    {
        ini_set('display_errors', 1);
        if ($this->input->post()) {
            $id = $this->input->post('id');
            $data = ['delete_status' => 0];
            $this->Common_model->UpdateDB('polls_options', ['id' => $id], $data);
            echo 1;
        } else
            echo 0;
    }

    /**
     * getPollsCategory method
     * @description this function call via ajax request, use to fetch poll categories
     * @return void
     */
    public function getPollsCategory()
    {
        if ($this->input->post()) {
            $data = $this->Polls_model->getCategories();
            echo json_encode($data);
            exit;
        }
    }

    /**
     * getPollsTags method
     * @description this function call via ajax request, use to fetch poll tags
     * @return void
     */
    public function getPollsTags()
    {
        if ($this->input->post()) {
            $data = $this->Polls_model->getTags();
            echo json_encode($data);
            exit;
        }
    }

    public function addTags()
    {
        if ($this->input->post()) {
            $data = [
                'created_by' => $this->session->userdata('user_id'),
                'type' => 2,
                'name' => post('tag_val'),
            ];
            $this->Common_model->InsertData('tags', $data);
            $lastInsertId = $this->db->insert_id();
            $data = ['id' => $lastInsertId, 'name' => post('tag_val')];
            echo json_encode($data);
            exit;
        }
    }

    public function addCategory()
    {
        if ($this->input->post()) {
            $catdata = [
                'created_by' => $this->session->userdata('user_id'),
                'type' => 2,
                'name' => post('cat_val'),
            ];
            $this->Common_model->InsertData('categories', $catdata);
            $lastInsertId = $this->db->insert_id();
            $data = ['id' => $lastInsertId, 'name' => post('cat_val')];
            echo json_encode($data);
            exit;
        }
    }


    //update status function 
    public function update_status($id, $action)
    {
        $status = ($action == 'activate') ? 1 : 0;
        $data = array('status' => $status);
        $this->Polls_model->update_status($id, $data);
        $msg = "Updated";
        echo json_encode(['msg' => $msg]);
        exit;
    }

    public function get_lesson_used($poll_id)
    {
        $data['typeid'] = $poll_id;
        list($poll_id, $type) = explode("_", $poll_id);
        $data['value'] = $this->Polls_model->get_lessons_used($poll_id);
        echo json_encode($data); 
        exit;
    } 
}
