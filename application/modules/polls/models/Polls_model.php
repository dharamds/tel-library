<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
  Author Akash Hedaoo
  Date 16/04/2019
 */

class Polls_model extends CI_Model {

    function __construct() {
        // Set table name
        $this->table = 'polls';
        // Set orderable column fields
        $this->column_order = array(null,null,'polls.status', 'polls.title', 'polls.long_title', 'poll_category.name', 'poll_tags.name', 'categories.name', 'tags.name', null);
        // Set searchable column fields
        $this->column_search = array('polls.title', 'polls.long_title');
        // Set default order
        $this->order = array('polls.created' => 'desc');
    }

    /*
     * Fetch members data from the database
     * @param $_POST filter data based on the posted parameters
     */

    public function getRows($postData) {

        $this->_get_datatables_query($postData);
        if ($postData['length'] != -1) {
            $this->db->limit($postData['length'], $postData['start']);
        }
        $query = $this->db->get();
        $data['result'] = $query->result();
        $data['total'] = $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total;
        return $data;
    }

    /*
     * Perform the SQL queries needed for an server-side processing requested
     * @param $_POST filter data based on the posted parameters
     */

    private function _get_datatables_query($postData) {
        
        $current_container_id = get_contener_id();
        $this->db->select('SQL_CALC_FOUND_ROWS ' . $this->table . '.id, ' . $this->table . '.title, ' . $this->table . '.long_title, ' . $this->table . '.status', false);
        $this->db->select('DATE_FORMAT(' . $this->table . '.created, "%m/%d/%Y") as created');
        $this->db->select(' COUNT(polls_result.poll_id) as poll_vote');
        // courses metadata
        if ((isset($postData['system_category']) && count($postData['system_category']) > 0) || $postData['order']['0']['column'] == 7) 
        {
            $this->db->select('GROUP_CONCAT( DISTINCT categories.name SEPARATOR \'<br> \')  as system_categories');
        }else{
            $this->db->select('"..." as system_categories');
        }

        if ((isset($postData['system_tag']) && count($postData['system_tag']) > 0) || $postData['order']['0']['column'] == 8) 
        {
            $this->db->select('GROUP_CONCAT( DISTINCT tags.name SEPARATOR \'<br> \')  as system_tags');
        }else{
            $this->db->select('"..." as system_tags');
        }       

        // system metadata
        if ((isset($postData['poll_category']) && count($postData['poll_category']) > 0) || $postData['order']['0']['column'] == 5) 
        {            
           $this->db->select('GROUP_CONCAT( DISTINCT poll_category.name SEPARATOR \'<br> \')  as poll_categories');
        }else{
            $this->db->select('"..." as poll_categories');
        }
        if ((isset($postData['poll_tag']) && count($postData['poll_tag']) > 0) || $postData['order']['0']['column'] == 6) 
        {
            $this->db->select('GROUP_CONCAT( DISTINCT poll_tags.name SEPARATOR \'<br> \')  as poll_tags');
        }else{
            $this->db->select('"..." as poll_tags');
        }
            
        $this->db->from($this->table);
        if ((isset($postData['system_category']) && count($postData['system_category']) > 0) || $postData['order']['0']['column'] == 7) 
        {
        $this->db->join('category_assigned', 'category_assigned.reference_id = '.$this->table.'.id AND category_assigned.reference_type = '.META_SYSTEM.' AND  category_assigned.reference_sub_type ='.META_POLL, 'left');
        $this->db->join('categories', 'categories.id = category_assigned.category_id AND categories.delete_status = 1 AND categories.status = 1', 'left');
        }
        if ((isset($postData['system_tag']) && count($postData['system_tag']) > 0) || $postData['order']['0']['column'] == 8) 
        {
        $this->db->join('tag_assigned', 'tag_assigned.reference_id = '.$this->table.'.id AND tag_assigned.reference_type = '.META_SYSTEM.' AND  tag_assigned.reference_sub_type ='.META_POLL, 'left');
        $this->db->join('tags', 'tags.id = tag_assigned.tag_id  AND tags.delete_status = 1 AND tags.status = 1', 'left');
        }
        if ((isset($postData['poll_category']) && count($postData['poll_category']) > 0) || $postData['order']['0']['column'] == 5) 
        {
        $this->db->join('category_assigned as assigned_poll_category', 'assigned_poll_category.reference_id =  '.$this->table.'.id AND assigned_poll_category.reference_type = '.META_POLL, 'left');
        $this->db->join('categories as poll_category', 'poll_category.id = assigned_poll_category.category_id AND poll_category.delete_status = 1 AND poll_category.status = 1', 'left');      
        }    
        if ((isset($postData['poll_tag']) && count($postData['poll_tag']) > 0) || $postData['order']['0']['column'] == 6) 
        {
        $this->db->join('tag_assigned as assigned_poll_tags', 'assigned_poll_tags.reference_id =  '.$this->table.'.id AND assigned_poll_tags.reference_type = '.META_POLL, 'left');
        $this->db->join('tags as poll_tags', 'poll_tags.id = assigned_poll_tags.tag_id AND poll_tags.delete_status = 1 AND poll_tags.status = 1', 'left');
        }

        $this->db->join('polls_result', 'polls_result.poll_id = ' . $this->table . '.id', 'left');


        $this->db->where($this->table . '.delete_status = 1');

        $i = 0;
        // loop searchable columns 
        foreach($this->column_search as $item){
		
            // if datatable send POST for search
            if($postData['search']['value']){
                // first loop
                if($i === 0){
				    // open bracket 
					$this->db->group_start();
					//pr( $item);	
                    $this->db->like($item, $postData['search']['value'], 'both');
                }else{
                    $this->db->or_like($item, $postData['search']['value'], 'both');
                }
                    // last loop
                if(count($this->column_search) - 1 == $i){
                    // close bracket
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if($postData['system_category']) {
            $this->db->where_in('category_assigned.category_id', $postData['system_category']);
        }
        if($postData['system_tag']) {
            $this->db->where_in('tag_assigned.tag_id', $postData['system_tag']);
        }
        if($postData['poll_category']) {
            $this->db->where_in('assigned_poll_category.category_id', $postData['poll_category']);
        }
        if($postData['poll_tag']) {
            $this->db->where_in('assigned_poll_tags.tag_id', $postData['poll_tag']);
        }

        $this->db->group_by($this->table . '.id');

        if (isset($postData['order'])) {
            $this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    /* Fetching default categories */

    public function getCategories() {
        $this->db->select('id, name');
        $this->db->from('categories');
        $this->db->where(['type' => 2, 'delete_status' => 1, 'status' => 1]);
        $this->db->like('name', $this->input->post('search'), 'after');
        $result = $this->db->get()->result_array();
        return $result;
    }

    /* Fetching default tags */

    public function getTags() {
        $this->db->select('id, name');
        $this->db->from('tags');
        $this->db->where(['type' => 2, 'delete_status' => 1, 'status' => 1]);
        $this->db->like('name', $this->input->post('search'), 'after');
        $result = $this->db->get()->result_array();
        return $result;
    }

  //update status function 
    public function update_status($id = null, $data) {
        $this->db->where('id', $id);
        $delete = $this->db->update('polls', $data);
        if ($delete):
            return true;
        endif;
    }

    public function get_lessons_used($pollID = null)
    {
        $sql = "SELECT COALESCE(GROUP_CONCAT(DISTINCT lessons.name ORDER BY lessons.name ASC SEPARATOR '<br>'), 'NA')  as lesson_name from lessons 
        JOIN polls ON polls.id = lessons.poll_id
        WHERE lessons.poll_id = " . $pollID . ' group by lessons.poll_id';

        //return $sql;
        $query =  $this->db->query($sql);
        return ($query->row()->lesson_name) ? $query->row()->lesson_name : 'NA';
    }

}

/* End of file Users_modal.php */
