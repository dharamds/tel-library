<!-- DataTables CSS library -->
<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            <ol class="breadcrumb">
                <li class=""><a href="">Home</a></li>
                <li class=""><a href="">Polls</a></li>
                <li class="active"><a href="">View Polls</a></li>
            </ol>
            <div class="container-fluid">

                <br>
                <div data-widget-group="group1">
                    <div class="row">
                        <div class="col-md-12 ">
                            <a class="btn btn-success" href="<?= base_url('polls/polls/save') ?>">New</a>
                        </div>
                        <div style="clear:both;"></div><br>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2>Poll's Records</h2>
                                <div class="panel-ctrls"></div>
                                <a href="<?= bs('users/print_with_dompdf') ?>">
                                    <i class="fa fa-print" style="padding-left: 1%;color: black"></i>
                                </a>
                            </div>
                            <div class="panel-body no-padding">


                                <table id="memListTable" class="table table-bordered table-striped table-hover" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Sr. No.</th>
                                            <th>Poll's Name</th>
                                            <?php if ($this->session->userdata("role_id") == 1) : ?>
                                                <th>Status</th>
                                                <th>Action</th>
                                            <?php endif ?>
                                        </tr>
                                    </thead>
                                    <!-- <tbody>
                                        <?php
                                        foreach ($polls as $list) : ?>
                                            <tr>
                                                <td> <?php echo htmlspecialchars($list->id, ENT_QUOTES, 'UTF-8'); ?> </td>
                                                <td> <?php echo htmlspecialchars($list->name, ENT_QUOTES, 'UTF-8'); ?> </td>
                                                <?php if ($this->session->userdata("role_id") == 1) : ?>
                                                    <td>
                                                        <?php if ($list->status == 1) : ?>

                                                            <a href="<?php bs() ?>Polls/Polls/update_status/<?php echo $list->id ?>/deactivate" data-toggle="tooltip" data-placement="top" title="Click to Change Status">
                                                                <button type="button" class="btn btn-primary-alt btn-sm">Active</button>
                                                            </a>

                                                        <?php else : ?>

                                                            <a href="<?php bs() ?>Polls/Polls/update_status/<?php echo $list->id ?>/activate" data-toggle="tooltip" data-placement="top" title="Click to Change Status">
                                                                <button type="button" class="btn btn-danger-alt btn-sm">Inactive</button>
                                                            </a>

                                                        <?php endif ?>
                                                    </td>
                                                    <td>
                                                        <a class="btn btn-success-alt btn-sm" href="<?php bs() ?>Polls/Polls/edit/<?php echo $list->id ?>">
                                                            <i class="ti ti-pencil"></i>
                                                        </a>
                                                        <?php $url="'".base_url().'Polls/Polls/delete/'.$list->id."'"; ?>
                                                        <a href="javascript:void(0);" class="btn btn-danger-alt btn-sm" onclick=craftpipConfirm(this.id,<?php echo $url ?>,"Delete?");>
                                                            <i class="ti ti-close"></i>
                                                        </a>
                                                    </td>

                                                <?php endif ?>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody> -->
                                </table>
                            </div>
                            <div class="panel-footer"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- .container-fluid -->
    </div>
    <!-- #page-content -->
</div>
<script type='text/javascript' src="<?php echo base_url(); ?>public\assets\js\Polls\Polls_view.js"></script>
<?php
$jsVars = [
    'ajax_call_root' => base_url()
];    
?>
<script>
    jQuery(document).ready(function() { 
        Polls_view.init(<?php echo json_encode($jsVars); ?>);
    });
</script>