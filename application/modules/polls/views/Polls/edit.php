<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            <ol class="breadcrumb">
                <li class=""><a href="">Home</a></li>
                <li class=""><a href="">Polls</a></li>
                <li class="active"><a href="">Edit Poll</a></li>
            </ol>
            <div class="container-fluid">
                <div class="panel panel-info" data-widget='{"draggable": "false"}'>
                    <div class="panel-heading">
                        <h2><i class="fa fa-user"></i>Update Poll</h2>
                        <div class="panel-ctrls" data-actions-container="" data-action-collapse='{"target": ".panel-body"}'></div>
                    </div>
                    <div class="panel-body">
                        <?= form_open('Polls/Polls/update', array('id' => 'polls_create_edit_form_validation', 'class' => 'form-horizontal')); ?>
                        <div class="form-group col-md-12">
                            <input type="hidden" name="id" value="<?php echo $id ?>">
                            <input type="hidden" id="base_url" value="<?php echo bs() ?>">
                            <div  class="confirm-div" ></div>
                            <div class="col-md-6">
                                <label for="fieldname" class="col-md-12 control-label">Poll's Name</label>
                                <?php 
                                $data = array(
                                    'name'        => 'name',
                                    'id'          => 'name',
                                    'value'       => $data->name,
                                    'rows'        => '5',
                                    'cols'        => '50',
                                    'class'       => 'form-control'
                                );
                                echo form_textarea($data); ?>
                            </div>
                            <div class="col-md-4">
                                <label for="fieldname" class="col-md-12 control-label">Poll's Options</label>
                                <input type="text" id="poll_opt"  class='form-control col-md-9' placeholder="Enter Poll's Options">
                                <label id="poll_opt_error" class="error" for="name">Please enter poll option</label>
                            </div>
                            <div class="col-md-2">
                                <input type="button" class="add-row btn btn-primary col-md-3"  value="Add Row">
                            </div>
                        </div>
                        <div class="form-group col-md-10">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Select</th>
                                        <th>Poll Options</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php  foreach ($poll_options as $opt) { ?>
                                    <tr id="row_<?php echo $opt->id ?>">
                                        <th><a  onclick="delete_option(<?php echo $opt->id ?>)" ><span class="btn btn-danger">Delete</span></a></th>
                                        <th><?php echo $opt->poll_option; ?></th>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            
                            <button type='button' class='delete-row btn btn-danger'>Delete Row</button>
                        </div>
                        
                        <div class="form-group">
                            
                            <div class="col-md-6">
                                <input type="submit" class="finish btn-success btn" value="Update">
                            </div>
                            </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .container-fluid -->
</div>
<!-- #page-content -->
</div>
<script type='text/javascript' src="<?php echo base_url(); ?>public\assets\js\Polls\Polls_create_edit.js"></script>
<?php
$jsVars = [
    'ajax_call_root' => base_url()
];    
?>
<script>
    jQuery(document).ready(function() { 
        Polls_create_edit.init(<?php echo json_encode($jsVars); ?>);
    });
</script>
<script type="text/javascript">

    $(document).ready(function(){
        $("#poll_opt_error").hide();
        $(".delete-row").hide();
        $(".add-row").click(function(){
            var name = $("#poll_opt").val();
            if(name != '') {
                var markup = "<tr><td><input class='form-control' type='checkbox' name='record'></td><td> <input class='form-control' type='input' readonly='readonly' name='poll_options[]' value='" + name + "'></td></tr>";
                $("table tbody").append(markup);
                $(".delete-row").show();
            } else {
                
                $("#poll_opt_error").show();
                setTimeout(function() { $("#poll_opt_error").hide(); }, 3000);
            }
        });
        
        // Find and remove selected table rows
        $(".delete-row").click(function(){
            $("table tbody").find('input[name="record"]').each(function(){
            	if($(this).is(":checked")){
                    $(this).parents("tr").remove();
                }
            });
        });
        
        

    });    
    function delete_option(id) {
    var base_url=$('#base_url').val();
    //alert(id);    
        if (confirm('Are you sure you want to delete this?')) {
            $.ajax({
                url: base_url +'polls/polls/delete_option',
                type: 'POST',
                data: {id:id},
                success: function(data) {
                    $('#row_'+id).hide();
                    ajax_notification("Poll option deleted successfully");
       
      
   
    
   

                    //called when successful

                },
                error: function(e) {
                    alert("Something went wrong.Please try again!!!");
                      //called when there is an error
                      //console.log(e.message);
                }
            });
        }
            
        }
        
        function ajax_notification(message) {
            $.notify({
                icon: 'glyphicon glyphicon-info-sign',
                title: '<b><i class="fa fa-exclamation-circle"></i> Notification</b><br>',
                message: message,
            },{
                type: "success success-noty col-md-3",
                allow_dismiss: true,
                placement: {
                    from: "top",
                    align: "right"
                },
                offset: 20,
                spacing: 10,
                z_index: 1431,
                delay: 5000,
                timer: 1000,
                animate: {
                    enter: 'animated bounceInDown',
                    exit: 'animated bounceOutUp'
                }
            });
        }
</script>