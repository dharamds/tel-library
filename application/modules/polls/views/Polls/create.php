<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            <ol class="breadcrumb">
                <li class=""><a href="">Home</a></li>
                <li class=""><a href="">Polls</a></li>
                <li class="active"><a href="">Create Poll</a></li>
            </ol>

            <div class="container-fluid">
                <div class="panel panel-info" data-widget='{"draggable": "false"}'>
                    <div class="panel-heading">
                        <h2><i class="fa fa-user"></i>Create Poll</h2>
                        <div class="panel-ctrls" data-actions-container="" data-action-collapse='{"target": ".panel-body"}'></div>
                    </div>
                    <div class="panel-body">
                        <?=form_open('Polls/Polls/save', array('id' => 'polls_create_edit_form_validation', 'class' => 'form-horizontal'));?>
                        <div class="form-group col-md-10">

                            <div class="col-md-6">
                                <label for="fieldname" class="col-md-12 control-label">Poll's Name</label>
                                <?php
                                    $data = array(
                                        'name' => 'name',
                                        'id' => 'name',
                                        'value' => set_value('name'),
                                        'rows' => '5',
                                        'cols' => '50',
                                        'class' => 'form-control',
                                        'required' => 'required',
                                    );
                                    echo form_textarea($data); ?>
                            </div>
                            <div class="col-md-6">
                                <label for="fieldname" class="col-md-12 control-label">Poll's Options</label>
                                <input type="text" id="poll_opt"  class='form-control col-md-4' placeholder="Enter Poll's Options">
                                <input type="button" class="add-row btn btn-primary col-md-2"  value="Add Row">
                                <span id="poll_opt_error" style="color:red;">Please enter poll name</span>
                            </div>
                        </div>
                        <div class="form-group col-md-10">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Select</th>
                                        <th>Poll Options</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>

                            <button type='button' class='delete-row btn btn-danger'>Delete Row</button>
                        </div>

                        <div class="form-group">

                            <div class="col-md-6">
                                <input type="submit" class="finish btn-success btn" value="Add">
                            </div>
                            </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .container-fluid -->
</div>
<!-- #page-content -->
</div>
<script type='text/javascript' src="<?php echo base_url(); ?>public\assets\js\Polls\Polls_create_edit.js"></script>
<?php
$jsVars = [
    'ajax_call_root' => base_url()
];    
?>
<script>
    jQuery(document).ready(function() { 
        Polls_create_edit.init(<?php echo json_encode($jsVars); ?>);
    });
</script>
<script type="text/javascript">

    $(document).ready(function(){
$(".delete-row").hide();
$("#poll_opt_error").hide();
        $(".add-row").click(function(){
            var name = $("#poll_opt").val();
            if(name != '') {
                var markup = "<tr><td><input class='form-control' type='checkbox' name='record'></td><td> <input class='form-control' type='input' readonly='readonly' name='poll_options[]' value='" + name + "'></td></tr>";
                $("table tbody").append(markup);
                $(".delete-row").show();

            } else {
                $("#poll_opt_error").show();
                setTimeout(function() { $("#poll_opt_error").hide(); }, 3000);
            }

        });

        // Find and remove selected table rows
        $(".delete-row").click(function(){
            $("table tbody").find('input[name="record"]').each(function(){
            	if($(this).is(":checked")){
                    $(this).parents("tr").remove();
                }
            });
        });

    });


</script>