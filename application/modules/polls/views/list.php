<div class="container-fluid">
                <div data-widget-group="group1">
                <div class="row">
        <div class="col-md-12">
            <div class="filter-container flex-row">
                <div class="flex-col-sm-3 selected_condition" id="system_cat">

                </div>
                <div class="flex-col-sm-2 selected_condition" id="system_tag">

                </div>
                <div class="flex-col-sm-3 selected_condition" id="poll_category">

                </div>
                <div class="flex-col-sm-2 selected_condition" id="poll_tag">

                </div>
                <div class="flex-col-12 flex-col-md-auto ml-auto">
                    <a class="btn btn-danger pt-2 reset_filter" id="reset_form" href="javascript:void(0)"><i class="flaticon-close-1 f21 fw100"></i></a>
                </div>
        </div>
    </div>
</div>          
    <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default panel-grid">
               
                            <div class="panel-heading">
                        <?php if ($this->ion_auth->is_admin() || $permissions->add_per): ?>
                        <a class="btn btn-primary" href="<?=base_url('polls/add')?>">Add New</a>
                        <?php endif; ?>

                        <?php if ($this->ion_auth->is_admin() || $permissions->delete_per): ?>
                            <a href="<?php echo base_url('polls/setAllSelected') ?>" class="btn btn-primary bulk_lesson" id="1">
                                Set all selected as available
                            </a>
                        <?php endif; ?>
                        <?php if ($this->ion_auth->is_admin() || $permissions->delete_per): ?>
                            <a href="<?php echo base_url('polls/setAllSelected') ?>" class="btn btn-primary bulk_lesson" id="0">
                                Set all selected as unavailable
                            </a>
                        <?php endif; ?>

                        <div class="panel-ctrls"></div>
                    </div>

                            
                            <div class="panel-body pl-0 pr-0">
                                <div class="row m-0">
                                    <div class="col-md-12">
                                        <table id="pollsListTable" class="table table-bordered table-striped table-hover" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th style="min-width: 50px;">
                                                       <label class="checkbox-tel"> <input type="checkbox" name="checkAll" class="checkAll"></label>
                                                    </th>
                                                    <th style="min-width: 100px;">Actions</th>
                                                    <th style="min-width: 150px;">Poll Available</th>
                                                    <th style="min-width: 250px;">Poll Title</th>
                                                    <th style="min-width: 250px;">Long Title</th>
                                                    <th style="min-width: 180px;">Poll Categories</th>
                                                    <th style="min-width: 180px;">Poll Tags</th>
                                                    <th style="min-width: 180px;">System Categories</th>
                                                    <th style="min-width: 180px;">System Tags</th>
                                                    <th style="min-width: 100px;">Total Votes</th>
                                                    <th style="min-width: 180px;">Used in These Lesson</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.checkAll').click(function () {
            $(':checkbox.checkLesson').prop('checked', this.checked);
        });

        
        CommanJS.get_metadata_options("System category", 1, <?php echo META_SYSTEM;?>, "system_cat");
        CommanJS.get_metadata_options("System tag", 2, <?php echo META_SYSTEM;?>, "system_tag");
        CommanJS.get_metadata_options("Poll category", 1, <?php echo META_POLL;?>, "poll_category");
        CommanJS.get_metadata_options("Poll tag", 2, <?php echo META_POLL;?>, "poll_tag");
        
        var table = table = $('#pollsListTable').DataTable({
            // Processing indicator
            "processing": true,
            // DataTables server-side processing mode
            "serverSide": true,
            // Initial no order.
            "iDisplayLength": 10,
            "bPaginate": true,
            "autoWidth": false,
            "scrollX": true,
            "order": [],
            "drawCallback": function(settings) {
                $('.load_meta_data').each(function(key, item) {
                    $.getJSON("<?php echo bs('questions/get_meta_tags/'); ?>" + $(this).attr('id'), function(data) {
                           if(data) $("#"+data.typeid).html(data.value);
                    });
				});
				$('.load_meta_category').each(function(key, item) {
                    $.getJSON("<?php echo bs('questions/get_meta_categories/'); ?>" + $(this).attr('id'), function(data) {
                           if(data) $("#"+data.typeid).html(data.value);
                    });
				});
                $('.load_lesson_data').each(function(key, item) {
                    $.getJSON("<?php echo bs('polls/get_lesson_used/'); ?>" + $(this).attr('id'), function(data) {
                        if (data) $("#" + data.typeid).html(data.value);
                    });
                });

            },
            // Load data from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('polls/getLists'); ?>",
                "type": "POST",
                "data": function (data) {
                    data.system_category    = CommanJS.getMetaCallBack('system_category');
                    data.poll_category      = CommanJS.getMetaCallBack('poll_category');
                    data.system_tag         = CommanJS.getMetaCallBack('system_tag');
                    data.poll_tag           = CommanJS.getMetaCallBack('poll_tag');
                    data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                },
            },
            "columnDefs": [
                {
                    "targets": 0,
                    // "searchable": false,
                    "orderable": false,
                    "data": null,
                    // "width": "4%",
                    "render": function (data, type, full, meta) {
                        var data = '';
                        if (type == 'display') {
                            data = '<label class="checkbox-tel"><input type="checkbox" class="checkLesson" name="item[]" value="' + full['id'] + '"></label>';
                        }
                        $(window).scrollTop(0);
                        return data;
                    }
                },
                {
                    "targets": 1,
                    "orderable": false,
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                            data = '';
                            <?php if ($this->ion_auth->is_admin() || $permissions->edit_per): ?>
                                data = '<a class="btn btn-primary btn-sm" href="<?=base_url('polls/edit/')?>' + full['id'] + '"><i class="ti ti-pencil"></i></a>';
                            <?php endif;
                            if ($this->ion_auth->is_admin() || $permissions->view_per):
                            ?>
                            // data += '<a class="btn btn-info btn-sm " href="<?=base_url('polls/detail/')?>' + full['id'] + '"><i class="ti ti-eye"></i></a>';
                            <?php endif;
                            if ($this->ion_auth->is_admin() || $permissions->delete_per):
                            ?>
                            data += '<a href="<?=base_url('polls/delete/')?>' + full['id'] + '" class="btn btn-danger btn-sm delete_item"><i class="ti ti-trash"></i></a>';
                            <?php endif;?>
                        }
                        return data;
                    }
                },
                {
                    "targets": 2,
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                            if (full['status'] == '1') {
                                data = '<a href="<?php bs('polls/Polls/update_status/') ?>' + full['id'] + '/deactivate" data-toggle="tooltip" data-placement="top" title="Click to Change Status" class="text-success f18 change_status"><i class="flaticon-checked"></i></a>';
                            } else {
                                data = '<a href="<?php bs('polls/Polls/update_status/') ?>' + full['id'] + '/activate" data-toggle="tooltip" data-placement="top" title="Click to Change Status" class="text-danger f18 change_status"><i class="flaticon-close"></i></a>';
                            }
                        }
                        return data;
                    }
                },
				{
                    "targets": [5],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                         data = '<span id="' + full['id'] +'_<?php echo META_POLL; ?>_<?php echo META_POLL; ?>_cat" data-type="course_category" class="load_meta_category"> Loading...</span>';
						}
                        return data;
                    }
                },
				{
                    "targets": [6],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                           data = '<span id="' + full['id'] +'_<?php echo META_POLL; ?>_<?php echo META_POLL; ?>" data-type="course_tags" class="load_meta_data"> Loading...</span>';
                        }
                        return data;
                    }
                },
                {
                    "targets": [7],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                           data = '<span id="' + full['id'] +'_<?php echo META_POLL ; ?>_<?php echo META_SYSTEM; ?>_syscat" data-type="system_category" class="load_meta_category"> Loading...</span>';
						}
                        return data;
                    }
                },
                {
                    "targets": [8],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                           data = '<span id="' + full['id'] +'_<?php echo META_POLL ; ?>_<?php echo META_SYSTEM; ?>_systag" data-type="system_tags" class="load_meta_data"> Loading...</span>';
                        }
                        return data;
                    }
                },
                {
                    "targets": [10],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                            data = '<span id="' + full['id'] + '_lesson"  class="load_lesson_data"> Loading...</span>';
                        }
                        return data;
                        //return "NA";
                    }
                }
               
            ],
            "columns": [{
                    "data": "id",
                },
                {
                    "data": "id",
                },
                {
                    "data": "id",
                },
                {
                    "data": "title",
                },
                {
                    "data": "long_title",
                },
                {
                    "data": "poll_categories",
                },
                {
                    "data": "poll_tags",
                },
                {
                    "data": "system_categories",
                },
                {
                    "data": "system_tags",
                },
                {
                    "data": "poll_vote",
                },              
                {
                    "data": "id"
                }
            ]
        });

        $(".bulk_lesson").click(function (e) {
            e.preventDefault();
            var selected_user_id = "";
            var availability_status = $(this).attr('id');
            $('input[name="item[]"]:checked').each(function () {
                //  console.log(this.value);
                selected_user_id = this.value + '|' + selected_user_id;
            });
            if (selected_user_id != "") {
                $('#check').val(selected_user_id);
            } else {
                CommanJS.getDisplayMessgae(400, 'Please select atleast one poll.')
                return false;
            }
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('polls/setAllSelected'); ?>",
                data: {
                    selectedData: selected_user_id,
                    status: availability_status
                },
                dataType: 'json',
                success: function (data) {
                    //console.log(data);
                    if (data.msg === 'Updated') {
                        $('.checkAll').prop('checked', false);
                        table.ajax.reload(); //just reload table
                    }
                }
            });
        });

        $(document).on('click', '.change_status', function (e) {
            e.preventDefault();
            var scope = $(this);
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure?',
                buttons: {
                    confirm: function () {
                        $.get(scope.attr("href"), // url
                    function (data, textStatus, jqXHR) { // success callback
                        var obj = JSON.parse(data);
                        if (obj.msg === 'Updated') {
                            table.ajax.reload();  //just reload table
                        }
                    });
                        return true;
                    },
                    cancel: function () {
                        return true;
                    }
                }
            });

            
            
        });

        // To delete record
        $(document).on('click', '.delete_item', function (e) {
            e.preventDefault();
            var scope = $(this);
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure?',
                buttons: {
                    confirm: function () {
                        $.get(scope.attr("href"), // url
                                function (data, textStatus, jqXHR) { // success callback
                                    var obj = JSON.parse(data);
                                    if (obj.msg === 'deleted') {
                                        table.ajax.reload(); //just reload table
                                    }
                                });
                        return true;
                    },
                    cancel: function () {
                        return true;
                    }
                }
            });
        });

        $('.filter-container').on('change','.meta_data_filter', function(){
            table.search('').draw();
        });

        $(".reset_filter").on('click', function(){
            $(".meta_data_filter").prop("checked",false);
            $(".selectFilterMode span").text('');
            table.search('').draw();
        });

    });
</script>