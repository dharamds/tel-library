<link rel="stylesheet" href="<?php bs('public/assets/plugins/multiselect/sumoselect.min.css') ?>" />

         <?php
         $cnt_res_fld = ['type' => 'hidden', 'name' => 'cnt_res', 'id' => 'cnt_res', 'value' => $rescnt];
         echo form_input($cnt_res_fld);
         ?>
         <div class="container-fluid">
            <div class="panel panel-default">
               <div class="panel-heading">
                  <ul class="nav nav-tabs" role="tablist" id="create_polls_tab">
                     <li role="presentation" class="active">
                        <a href="#poll_content" role="tab" data-toggle="tab">
                           Content
                        </a>
                     </li>
                     <li role="presentation">
                        <a href="#poll_metadata" id="metadata_id" role="tab" data-toggle="tab">
                           Metadata
                        </a>
                     </li>
                  </ul>
               </div>
               <?php
               echo form_open('polls/add/', [
                  'name' => 'frmAddPolls',
                  'id' => 'frmAddPolls',
                  'class' => 'form-horizontal',
                  'enctype' => 'multipart/form-data'
               ]);
               $poll_id = ['type' => 'hidden', 'name' => 'poll_id', 'id' => 'poll_id', 'value' => $poll_id];
               echo form_input($poll_id);
               ?>
               <!--//  Panel Body Start  //-->
               <div class="panel-body">
                  <div class="tab-content">
                     <!--//  Tab Content Start  //-->
                     <div role="tabpanel" class="tab-pane active" id="poll_content">
                        <div class="row mt-3 mb-3">
                           <div class="col-sm-4">
                              <div class="form-group m-0">
                                 <label for="coursetitle" class="control-label">
                                    Title <span class="field-required">*</span>
                                 </label>
                                 <?php
                                 echo form_input([
                                    'class' => 'form-control',
                                    'name' => 'title',
                                    'id' => 'title',
                                    'value' => $PollsData[0]->title
                                 ]);
                                 ?>
                              </div>
                           </div>
                           <div class="col-sm-8">
                              <div class="form-group m-0">
                                 <label for="longtitle" class="control-label">
                                    Long Title
                                 </label>
                                 <?php
                                 echo form_input([
                                    'name' => 'long_title',
                                    'id' => 'long_title',
                                    'class' => 'form-control',
                                    'value' => $PollsData[0]->long_title
                                 ]);
                                 ?>
                              </div>
                           </div>
                        </div>

                        <div class="row m-0 mb-3">
                           <div class="col-sm-8">
                              <div class="form-group">
                                 <label for="coursedescription" class="control-label">
                                    Question or Prompt <span class="field-required">*</span>
                                 </label>
                                 <div>
                                    <?php echo form_textarea([
                                       'name' => 'question',
                                       'id' => 'question',
                                       'class' => 'form-control editor',
                                       'rows' => '5',
                                       'value' => $PollsData[0]->question
                                    ]); ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <?php
                        if (!empty($PollsData)) {
                           $i = 1;
                           foreach ($PollsData as $resp) { ?>
                              <div class="row m-0 mb-3" id="divRes_<?php echo $i; ?>">
                                 <div class="col-sm-8">
                                    <div class="form-group">
                                       <label for="coursedescription" class="control-label">
                                          Response <?php echo $i; ?><span class="field-required">*</span>
                                       </label>
                                       <div>
                                          <?php echo form_textarea([
                                             'name' => "poll_response[]",
                                             'id' => "poll_response_" . $i,
                                             'class' => 'form-control editor res',
                                             'rows' => '5',
                                             'value' => strip_tags($resp->options)
                                          ]); ?>
                                       </div>
                                    </div>
                                 </div>
                              </div>


                              <?php
                              $i++;
                           }
                        } else { ?>
                           <div class="row m-0 mb-3">
                              <div class="col-sm-8">
                                 <div class="form-group">
                                    <label for="coursedescription" class="control-label">
                                       Response 1<span class="field-required">*</span>
                                    </label>
                                    <div>
                                       <?php echo form_textarea([
                                          'name' => 'poll_response[]',
                                          'id' => 'poll_response_1',
                                          'class' => 'form-control editor res',
                                          'rows' => '5'
                                       ]); ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row m-0 mb-3">
                              <div class="col-sm-8">
                                 <div class="form-group">
                                    <label for="coursedescription" class="control-label">
                                       Response 2<span class="field-required">*</span>
                                    </label>
                                    <div>
                                       <?php echo form_textarea([
                                          'name' => 'poll_response[]',
                                          'id' => 'poll_response_2',
                                          'class' => 'form-control editor res',
                                          'rows' => '5'
                                       ]); ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        <?php } ?>
                        <div id="responseDiv">

                        </div>
                        <div class="row mb-12">
                           <div class="col-sm-6">
                              <button id="addResp" onClick="addResponses();" type="button" class="btn btn-primary">Add Another Response</button>
                           </div>
                           <div class="col-sm-6">
                              <button id="removeResp" onClick="removeResponses();" type="button" class="btn btn-danger">Remove Response</button>
                           </div>
                        </div>
<br>
                        <div class="col-md-12 p-0">
                                <div class="form-group col-xs-12 pt-3">
                                    <label class="control-label block">
                                        Poll Availability
                                    </label>
                                    <div class="col-md-12 p-0 pt-3">
                                             <label class="radio-tel m-0">
                                            <input type="radio" id="abc1" name="status" value="1" <?php if($PollsData[0]->status == 1): echo 'checked'; endif;?> class="mr-2 t2"> Available
                                        </label>
                                        <label class="radio-tel m-0 ml-4">
                                            <input type="radio" id="abc2" name="status" value="0" <?php if($PollsData[0]->status == 0): echo 'checked'; endif;?> class="mr-2 t2"> Hidden
                                        </label>
                                    </div>
                                </div>
                            </div>

                        <div class="row m-0 mt-5 mb-3">
                           <div class="hr-line col-xs-12 mb-4"></div>
                           <div class="col-xs-12 text-right">
                              <button class="btn btn-primary" type="submit">Next</button>
                           </div>
                        </div>
                        </form>
                     </div>
                     <!--//  Tab Content End  //-->
                     <!--//  Tab Metadata Start  //-->
                     <div role="tabpanel" class="tab-pane" id="poll_metadata">
                        <div class="pl-4 col-md-12 row">
                           <div class="col-md-6 pl-0">
                              <div class="panel panel-grey">
                                 <div class="panel-heading mb-3">
                                    <h2><span>Poll Categories</span></h2>
                                 </div>
                                 <div class="panel-body" id="polls_cat_section">

                                 </div>
                              </div>
                           </div>
                           <div class="col-md-6 pl-0">
                              <div class="panel panel-grey">
                                 <div class="panel-heading">
                                    <h2><span>Poll Tags</span></h2>
                                    <div class="panel-ctrls button-icon-bg">
                                    </div>
                                 </div>
                                 <div class="panel-body" id="polls_tags_section">

                                 </div>
                              </div>
                           </div>
                        </div>

                        <div class="hr-line mt-4 mb-4 col-xs-12"></div>

                        <div class="pl-4 col-md-12 row">
                           <div class="col-md-6 pl-0 pt-5">
                              <div class="panel panel-grey">
                                 <div class="panel-heading mb-3">
                                    <h2><span>System Categories</span></h2>
                                 </div>
                                 <div class="panel-body panel-collapse-body" id="system_cat_section">

                                 </div>
                              </div>
                           </div>
                           <div class="col-md-6 pl-0 pt-5">
                              <div class="panel panel-grey">
                                 <div class="panel-heading">
                                    <h2><span>System Tags</span></h2>
                                    <div class="panel-ctrls button-icon-bg">
                                    </div>
                                 </div>
                                 <div class="panel-body" id="system_tags_section">

                                 </div>
                              </div>
                           </div>
                        </div>


                     </div>
                     <!--//  Tab Metadata End  //-->
                  </div>
               </div>

               <!--//  Panel Body End  //-->
            </div>
         </div>
      </div>
   </div>
</div>

<script type="text/javascript" src="<?php bs('public/assets/js/Polls/add.js') ?>"></script>
<?php
$jsVars = [
   'ajax_call_root' => base_url()
];
?>

<script type="text/javascript">
   var config = { height: 120, toolbar: 'short', allowedContent :true};
   $(document).ready(function() {
      $('#metadata_id').click(function() {
         var poll_id = $('#poll_id').val();
         if (poll_id != '') {
            set_metadata(poll_id);
         } else {
            return false;
         }
      });
      
      // ck editor
      $('.editor').each(function(){
         CKEDITOR.replace( this.id, config); 
      });
      $('#removeResp').hide();
      var rescnt = ($('.res').length);
      if (rescnt > 2) {
         $('#removeResp').show();
      }

      $("#frmAddPolls").validate({
         ignore: [],
         rules: {
            title: {
               required: true
            },
            question: {
               required: function(textarea) {
                  CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                  var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                  return editorcontent.length === 0;
               }
            },            
            'poll_response[]': {

               required: function(textarea) {
                  CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                  var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                  return editorcontent.length === 0;
               }
            },
         },
         messages: {
            title: "Please enter poll title",
            question: "Please enter question",
            'poll_response[]': "Please enter responses"
         },
         errorPlacement: function(error, $elem) {
            if ($elem.is('textarea')) {
               $elem.insertAfter($elem.next('div'));
            }
            error.insertAfter($elem);
         },
         submitHandler: function(form) {
            // D fixesss   
            for (instance in CKEDITOR.instances) {
               CKEDITOR.instances[instance].updateElement();
            }

            $.ajax({
               url: "<?php echo bs('polls/add'); ?>",
               method: "POST",
               data: $('#frmAddPolls').serializeArray(),
               dataType: "json",
               complete: function(xhr, status) {},
               success: function(data) {
                  $('#poll_id').val(data);
                  $('#create_polls_tab a[href="#poll_metadata"]').tab('show');
                  pageScrollTop();
                  set_metadata(data);
                  CommanJS.getDisplayMessgae(200, 'Poll saved successfully.');
               }
            });
            return false;
         }
      });

      function set_metadata(id) {
         CommanJS.getCatSection(<?=META_POLL;?>, 'polls_cat_section', 'polls_default_cat', id,<?=META_POLL;?>);         
         CommanJS.getTagSection(<?=META_POLL;?>, 'polls_tags_section', 'polls_tags', id,<?=META_POLL;?>);
         CommanJS.getCatSection(<?=META_SYSTEM;?>, 'system_cat_section', 'polls_system_cat', id,<?=META_POLL;?>);
         CommanJS.getTagSection(<?=META_SYSTEM;?>, 'system_tags_section', 'system_tags', id,<?=META_POLL;?>);
      }
   });

   /* Add extra responses */
   function addResponses() {
      var rescnt = ($('.res').length) + 1;
      if (rescnt > 2) {
         $('#removeResp').show();
      }
      $("#cnt_res").val(rescnt);
      $("#responseDiv").append('<div id="divRes_' + rescnt + '"><div class="row mb-3"> <div class="col-sm-8"> <label for="coursedescription" class="control-label" for="poll_response_' + rescnt + '" > Response ' + rescnt + ' </label><textarea name="poll_response[]"  id="poll_response_' + rescnt + '" class="form-control editor res" rows="3"></textarea></div></div>');
      CKEDITOR.replace('poll_response_' + rescnt, config);
   }

   /* Add extra responses */
   function removeResponses() {
      var rescnt = ($('.res').length);

      if (rescnt <= 3) {
         $('#removeResp').hide();
      }
      if (rescnt > 2) {
         $('#divRes_' + rescnt).remove();
         rescnt--;
         $("#cnt_res").val(rescnt);

      } else {
         alert('At-least 2 responses is mandatory');

      }
   }

   $(document).ready(function() {
      // Jquery validations
      AddPolls.init(<?php echo json_encode($jsVars); ?>);
      $('.btnNext').click(function() {
         $('#create_polls_tab.nav-tabs > .active').next('li').find('a').trigger('click');
         pageScrollTop();
      });

      $('.back_button').click(function() {
         $('#create_polls_tab a[href="#poll_content"]').tab('show');
        // pageScrollTop();
      });
   });

   function pageScrollTop() {
      $("html, body").animate({
         scrollTop: 0
      }, "slow");
      return false;
   }
</script>