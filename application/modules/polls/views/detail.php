<ol class="breadcrumb">
   <li class=""><a href="<?php bs(); ?>">Home</a></li>
   <li class=""><a href="<?php bs(); ?>polls/">Polls</a></li>
   <li class="active"><a href="">Detail</a></li>
</ol>
<div class="container-fluid">
   <div class="tab-pane active" id="tab-about">
      <div class="panel panel-default panel-grid">
         <div class="panel-heading brd-0 pt-1"></div>
         <div class="panel-body">
            <div class="about-area">

               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label fw600">Title</label>
                        <div><?php echo $pollsData->title;?></div>
                        <div class="hr-line mt-4 mb-4"></div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label fw600">Long title</label>
                        <div><?php echo $pollsData->long_title;?></div>
                        <div class="hr-line mt-4 mb-4"></div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label fw600">Question</label>
                        <div><?php echo $pollsData->question;?></div>
                        <div class="hr-line mt-4 mb-4"></div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label fw600">Total votes</label>
                        <div><?php echo $pollsData->total_votes;?></div>
                        <div class="hr-line mt-4 mb-4"></div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <label class="control-label fw600">Options</label>
                        <div>
                           <?php  //pr($answerData);
                           foreach ($optionsData as $key => $option) {
                              ?>
                              <div class="row m-0">
                                 <div class="col-md-12 p-0"><?php echo $option->options; ?></div>
                              </div>
                              <?php                                         
                           }
                           ?>
                        </div>
                        <div class="hr-line mt-4 mb-4"></div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label fw600">Status</label>
                        <div><?php echo ($pollsData->status==1) ? 'Publish' : 'Un-publish';?></div>
                        <div class="hr-line mt-4 mb-4"></div>
                     </div>
                  </div>
               </div>

               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label fw600">Created by</label>
                        <div><?php 
                              $authorData = $this->ion_auth->user($pollsData->created_by)->row(); 
                              echo ucfirst($authorData->first_name); echo " ";  
                              echo ucfirst($authorData->last_name);
                              ?></div>
                        <div class="hr-line mt-4 mb-4"></div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label fw600">Created on</label>
                        <div><?php echo date('m/d/Y',strtotime($pollsData->created)); ?></div>
                        <div class="hr-line mt-4 mb-4"></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
