<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
Author Akash Hedaoo
Date 16/04/2019
 */

class Discussion_model extends CI_Model
{
    
    function __construct() {
        // Set table name
        $this->table = 'discussions';
        // Set orderable column fields
        $this->column_order = array('id', 'discussions.name', 'discussions.description', 'created_by', 'created', 'status');
        // Set searchable column fields
        $this->column_search = array('discussions.name', 'discussion.description');
        // Set default order
        $this->order = array('created' => 'desc');
    }

    /*
     * Fetch data from the database
     * @param $_POST filter data based on the posted parameters
     */
    public function discussions_list()
    {
        $this->db->where('delete_status', 1);
        $query = $this->db->get('discussions');
        return $query->result();
    }
    
      public function getRows($postData) {      
        $this->_get_datatables_query($postData);          
        if($postData['length'] != -1){
            $this->db->limit($postData['length'], $postData['start']);
        }
        $query = $this->db->get();
		$data['result'] = $query->result();
		$data['total'] = $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total;
        return $data;
    }
    
      private function _get_datatables_query($postData){
        $this->db->select('SQL_CALC_FOUND_ROWS '. $this->table.'.id,  '.$this->table.'.name, '.$this->table.'.description, '.$this->table.'.created_by, '.$this->table.'.created, '.$this->table.'.status as status_button, users.first_name, users.last_name AS type_name',false); 
        $this->db->select('DATE_FORMAT('.$this->table.'.created, "%m/%d/%Y") as created');
        $this->db->select("IF(".$this->table.".status = 1, 'Publish', 'Unpublish') as status");
      
        $this->db->select("CONCAT_WS(' ', users.first_name, users.middle_name, users.last_name) as user_name");
        $this->db->from($this->table);
  
        $this->db->join('users', $this->table.'.created_by = users.id', 'left');
        $this->db->where($this->table.'.delete_status = 1');

        $i = 0;
        // loop searchable columns 
        foreach($this->column_search as $item){
            // if datatable send POST for search
            if($postData['search']['value']){
                // first loop
                if($i===0){
                    // open bracket
                    $this->db->group_start();
                    $this->db->like($item, $postData['search']['value']);
                }else{
                    $this->db->or_like($item, $postData['search']['value']);
                }
                
                // last loop
                if(count($this->column_search) - 1 == $i){
                    // close bracket
                    $this->db->group_end();
                }
            }
            $i++;
        }
        
        if(isset($postData['status']) && $postData['status'] < 2){
            $this->db->where($this->table.'.status', $postData['status']);
        }        
       
        if(isset($postData['order'])){
            $this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        }else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
    }


    /*
     * Fetch data from the database
     * @param $id filter data based on the particular id from url
     */
    public function edit($id = null)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('discussions');
        return $query->row();
    }

    

    /*
     * Update data into the database
     * @param $id filter data based on the particular id from url
     */
    public function update($id = null, $data)
    {
        $this->db->where('id', $id);
        $update = $this->db->update('discussions', $data);
        if ($update):
            return true;
        endif;

    }

    /*
     * update delete_status for active inactive skills from the database
     * @param $id filter data based on the particular id from url
     */
      public function update_status($id = null, $data)
    {
        $this->db->where('id', $id);
        $delete = $this->db->update('discussions', $data);
        if ($delete):
            return true;
        endif;
    }

    

}

/* End of file Users_modal.php */
