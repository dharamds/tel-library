<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Discussion extends MY_Controller
{

    public function __construct()
    {

        parent::__construct();
        //Do your magic here

        $this->load->module('template');
        $this->load->model('common_model');
        $this->load->model('Discussion_model');
        $this->load->library('form_validation');
        $this->load->helper(array('html', 'language', 'form', 'country_helper'));
        if (!$this->ion_auth->logged_in()) :
            redirect('users/auth', 'refresh');
        endif;
    }

    /**
     * [Load poll list]
     * @return [void]
     */
    public function index()
    {
        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Discussions', 'link' => '', 'class' => 'active'];
        $data['Gradebook'] = $this->Discussion_model->discussions_list();
        $data['page'] = "Discussions/Discussions/view";
        $this->template->template_view($data);
    }

    public function getLists()
    {

        // exit('sdsds');
        $discussionData = $this->Discussion_model->getRows($_POST);

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $discussionData['total'],
            "recordsFiltered" => $discussionData['total'],
            "data" => $discussionData['result'],
        );
        // Output to JSON format
        echo json_encode($output);
    }

    public function detail()
    {
        $this->benchmark->mark('code_start');
        if (!$this->ion_auth->is_admin()) {
            return show_error('You Must Be an Administrator To view This Page');
        }
        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Discussions', 'link' => base_url('discussions/discussion'), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'View', 'link' => '', 'class' => 'active'];

        if ($this->uri->segment(4) == '') {
            $data['page'] = 'Discussions/Discussions/detail';
            $this->template->template_view($data);
        } else {
            $data['data'] = $this->Discussion_model->edit($this->uri->segment(4));
            $data['id'] = $this->uri->segment(4);
            $data['discussion_data'] = $this->Discussion_model->edit($this->uri->segment(4));
            $data['page'] = 'Discussions/Discussions/detail';
            $this->template->template_view($data);
        }
    }


    //create Discussion page
    public function save()
    {
        $this->benchmark->mark('code_start');
        if (!$this->ion_auth->is_admin()) {
            return show_error('You Must Be an Administrator To view This Page');
        }
        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Discussions', 'link' => base_url('discussions/discussion'), 'class' => ''];
        
        if ($this->uri->segment(4) == '') {
            $data['page'] = 'Discussions/Discussions/create';
            $data['breadcrumb'][]           = ['title' => 'Add', 'link' => '', 'class' => 'active'];
            $this->template->template_view($data);
        } else {
            $data['data'] = $this->Discussion_model->edit($this->uri->segment(4));
            $data['id'] = $this->uri->segment(4);
            $data['assignments_data'] = $this->Discussion_model->edit($this->uri->segment(4));
            $data['page'] = 'Discussions/Discussions/create';
            $data['breadcrumb'][]           = ['title' => 'Edit', 'link' => '', 'class' => 'active'];
            $this->template->template_view($data);
        }
    }
    /**
     * [Add New data]
     */
    public function add()
    {
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('description', 'Description', 'trim|required');

        //pr("h"); exit;
        if ($this->form_validation->run() == false) {
            $msg = 'Fields should not be blank.';
            $this->session->set_flashdata('error', $msg);
        } else {

            $data = array(
                'created_by' => $this->session->userdata('user_id'),
                'name' => post('name'),
                'description' => post('description'),
                'status' => 1,
                'delete_status' => 1,
                'created' => date("Y-m-d H:i:s"),
            );
            if (post(id) == '') {
                $this->common_model->InsertData('discussions', $data); //insert data
                set_flashdata('success', 'Added Successfully');
            } else {
                $where = array(
                    "id" => post(id)
                );
                $this->common_model->UpdateDB('discussions', $where, $data); //update data
                set_flashdata('success', 'Updated Successfully');
            }
        }
        redirect('Discussions/Discussion/index', 'refresh');
    }


    // update Gradebook functionality
    public function update()
    {
        $this->form_validation->set_rules('category_id', 'Gradebook\'s Name', 'trim|required');


        if ($this->form_validation->run() === false) {
            $msg = 'Gradebook\'s name should not be blank.';
            $this->session->set_flashdata('error', $msg);
            redirect('Discussions/Discussion/' . $this->input->post('id'));
        } else {

            $data = array(
                'created_by' => $this->session->userdata('user_id'),
                'category_id' => post('category_id'),
                'template' => post('template'),
                'description' => post('description'),
                'status' => 1,
                'delete_status' => 1,
                'updated_at' => date("Y-m-d H:i:s"),
            );
            //pr(); exit;
            if ($this->Gradebook_model->update($this->input->post('id'), $data)) {
                $msg = "Updated successfully";
                $this->session->set_flashdata('success', $msg);
                redirect('Discussions/Discussion/', 'refresh');
            } else {
                $this->session->set_flashdata('error', 'Something went wrong');
                redirect('Discussions/Discussion/', 'refresh');
            }
        }
    }

    //delete Discussion
    public function delete()
    {
        $this->session->set_flashdata('success', $this->ion_auth->messages());
        if ($this->input->post('status') == '') {
            $status = 0;
        } else {
            $status = 1;
        }
        $additional_data = array(
            'delete_status' => $status,
        );
        $this->Discussion_model->update($this->uri->segment(4), $additional_data);

        $msg = "Deleted successfully";
        echo json_encode(array("msg" => $msg));
        exit;
        // $this->session->set_flashdata('success', $msg);
        //  redirect('Discussions/Discussion/', 'refresh');
    }




    //update Discussion status
    public function update_status($id, $action)
    {
        $status = ($action == 'activate') ? 1 : 0;
        $data = array('status' => $status);
        $this->Discussion_model->update_status($id, $data);
        $msg = "Updated";
        echo json_encode(['msg' => $msg]);
        exit;
    }
}

/* End of file Posts.php */
/* Location: ./application/modules/blog/controllers/Posts.php */
