
            <div class="container-fluid">

                <br>
                <div data-widget-group="group1">
                    <div class="row">
                        <div class="col-md-12">
                            <a class="btn btn-primary" href="<?= base_url('discussions/discussion/save') ?>">Add New Discussion Topic</a> 
                        </div> 
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h2>Discussion Topic's Records</h2>
                                    <div class="panel-ctrls"></div>
                                    <a href="<?= bs('users/print_with_dompdf') ?>">
                                        <i class="fa fa-print" style="padding-left: 1%;color: black"></i>
                                    </a>	
                                </div>
                                <div class="panel-body no-padding">

                                    <table id="discussionTable" class="table table-bordered table-striped table-hover" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Sr. No</th>
                                                <th>Name</th>
                                                
                                              
                                                    <th>Status</th>
                                                    <th>Action</th>
                                               
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="panel-footer"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- .container-fluid -->


<script>
    $(document).ready(function () {
        var table = table = table =  $('#discussionTable').DataTable({
            // Processing indicator
            "processing": true,
            // DataTables server-side processing mode
            "serverSide": true,
            // Initial no order.
            "iDisplayLength": 10,
            "bPaginate": true,
            "order": [],
            // Load data from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('discussions/discussion/getLists/'); ?>",
                "type": "POST",
                "data": function (data) {
                    //alert("h");
                },
            },
            //Set column definition initialisation properties
            "columnDefs": [{
                    "targets": 2,
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                            if (full['status_button'] == '1') {
                                data = '<a href="<?php bs('discussions/Discussion/update_status/') ?>' + full['id'] + '/deactivate" data-toggle="tooltip" data-placement="top" title="Click to Change Status" class="btn btn-midnightblue-alt btn-sm change_status">Active</a>';
                            } else {
                                data = '<a href="<?php bs('discussions/Discussion/update_status/') ?>' + full['id'] + '/activate" data-toggle="tooltip" data-placement="top" title="Click to Change Status" class="btn btn-midnightblue-alt btn-sm change_status">Inactive</a>';
                            }
                        }
                        return data;

                    }

                },
                {
                    "targets": 3,
                    "data": null,
                    "render": function (data, type, full, meta) {
                        //console.log(full);
                        data = '<a class="btn btn-midnightblue-alt btn-sm" href="<?= base_url('Discussions/Discussion/save/') ?>' + full['id'] + ' "> <i class="ti ti-pencil"></i></a>';
                        data += '<a href="<?= base_url('discussions/Discussion/delete/') ?>' + full['id'] + '" class="btn btn-midnightblue-alt btn-sm delete_item"><i class="ti ti-close"></i></a>';
                        data += '<a class="btn btn-midnightblue-alt btn-sm" href="<?= base_url('Discussions/Discussion/detail/') ?>' + full['id'] + '"><i class="ti ti-eye"></i></a>';
                        return data;
                    }
                }

            ],
            "columns": [
                {
                    "data": "sr_no", render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                    "autoWidth": true
                },
                {"data": "name", "autoWidth": true}
                
            ]
        });
        
        
        
         $(document).on('click', '.change_status', function (e) {
                    e.preventDefault();
                          $.get($(this).attr("href"), // url
                                function (data, textStatus, jqXHR) { // success callback
                                    var obj = JSON.parse(data);
                                    if (obj.msg === 'Updated') {
                                        table.ajax.reload();  //just reload table
                                    }                                });
        });
        
        

        $(document).on('click', '.delete_item', function (e) {
            e.preventDefault();
            var scope = $(this);
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure?',
                buttons: {
                    confirm: function () {
                        $.get(scope.attr("href"), // url
                                function (data, textStatus, jqXHR) { // success callback
                                    var obj = JSON.parse(data);
                                    console.log(obj.msg); 
                                    if (obj.msg === 'Deleted successfully') {
                                        table.ajax.reload(); //just reload table
                                    }
                                });
                        return true;
                    },
                    cancel: function () {
                        return true;
                    }
                }
            });
        });
    });
</script>
