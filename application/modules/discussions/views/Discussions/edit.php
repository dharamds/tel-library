
            <div class="container-fluid">
                <div class="panel panel-info" data-widget='{"draggable": "false"}'>
                    <div class="panel-heading">
                        <h2><i class="fa fa-user"></i>Edit Gradebooks</h2>
                        <div class="panel-ctrls" data-actions-container="" data-action-collapse='{"target": ".panel-body"}'></div>
                    </div>
                    <div class="panel-body">
                        <?= form_open('Gradebook/Gradebook/update', array('id' => 'gradebook_create_form_validation', 'class' => 'form-horizontal')); ?>
                        <input type="hidden" name="id" value="<?php echo $Gradebook_data->id ?>">
                        <div class="form-group col-md-12">

                            <div class="col-md-6">
                                <label for="fieldname" class="col-md-12 control-label">Grading's Category</label>
                                <?php
                                $extra = array(
                                    'name' => 'category_id',
                                    'id' => 'category_id',
                                    'rows' => '5',
                                    'cols' => '50',
                                    'class' => 'form-control'
                                );
                                echo form_dropdown('category_id', $Gradebook_options, $Gradebook_data->category_id, $extra);
                                ?>
<?php echo form_error('category_id', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="col-md-6">
                                <label for="fieldname" class="col-md-12 control-label">Grading's Template</label>
                                <?php
                                echo form_textarea([
                                    'name' => 'template',
                                    'id' => 'template',
                                    'class' => 'form-control editor',
                                    'rows' => '5',
                                    "value" => $Gradebook_data->template]);
                                ?>
<?php echo form_error('template', '<div class="error">', '</div>'); ?>
                            </div>

                        </div>
                        <div class="form-group col-md-12">

                            <div class="col-md-6">
                                <label for="fieldname" class="col-md-12 control-label">Grading's Description</label>
                                <?php
                                echo form_textarea([
                                    'name' => 'description',
                                    'id' => 'description',
                                    'class' => 'form-control editor',
                                    'rows' => '5',
                                    "value" => $Gradebook_data->description]);
                                ?>
<?php echo form_error('description', '<div class="error">', '</div>'); ?>
                            </div>


                        </div>


                        <div class="form-group">

                            <div class="col-md-6">
                                <input type="submit" class="finish btn-success btn" value="Update">
                            </div>
                            </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<script type='text/javascript' src="<?php echo base_url(); ?>public\assets\js\Gradebook\Gradebook_create.js"></script>
<?php
$jsVars = [
    'ajax_call_root' => base_url()
];
?>
<script>
    jQuery(document).ready(function () {
        Gradebook_create.init(<?php echo json_encode($jsVars); ?>);
    });
</script>