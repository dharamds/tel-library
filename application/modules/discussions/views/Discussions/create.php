
            <div class="container-fluid">
                <div class="panel panel-info" data-widget='{"draggable": "false"}'>
                    <div class="panel-heading">
                        <h2><i class="fa fa-user"></i>
                            <?php
                            if (empty($id)) {
                                echo 'Create Discussion Topic';
                            } else {
                                echo 'Edit Discussion Topic';
                            }
                            ?>

                        </h2>
                        <div class="panel-ctrls" data-actions-container="" data-action-collapse='{"target": ".panel-body"}'></div>
                    </div>
                    <div class="panel-body">
                        <?= form_open('Discussions/Discussion/add', array('id' => 'Discussion_create_form_validation', 'class' => 'form-horizontal')); ?>
                        <input type="hidden" name="id" value="<?php echo $id ?>" >
                        <div class="form-group col-md-12">
                            <div class="col-md-4">
                                <label for="fieldname" class="col-md-12 control-label">Name</label>
                                <?php
                                $extra = array(
                                    'name' => 'name',
                                    'id' => 'name',
                                    'value' => set_value('name'),
                                    'class' => 'form-control',
                                    'value' => $data->name
                                );
                                echo form_input($extra);
                                ?>
                                <?php echo form_error('name', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="col-md-8">
                                <label for="fieldname" class="col-md-12 control-label">Description</label>
                                <?php
                                echo form_textarea([
                                    'name' => 'description',
                                    'id' => 'description',
                                    'class' => 'form-control editor',
                                    'value' => $data->description
                                ]);
                                ?>
                                <?php echo form_error('description', '<div class="error">', '</div>'); ?>
                            </div>

                        </div>

                        <div class="form-group">

                            <div class="col-md-6">
                                <input type="submit" class="finish btn-success btn" value="Save">
                            </div>
                            </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<script>
    $(document).ready(function () {
        
        jQuery.validator.addMethod("noSpace", function (value, element, param) {
            return value.match(/^(?=.*\S).+$/);
        }, "No space please and don't leave it empty");

        CKEDITOR.replace('description', {
            toolbar: 'short',
        });

        $("#Discussion_create_form_validation").validate({
            
             ignore: [],
            
            rules: {
                name: {
                    required: true,
                    noSpace: true
                },
                description: {
                    required: function (textarea) {
                        CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                        return editorcontent.length === 0;
                    }
                }
            },
            messages: {
                name: {
                    required: "Please enter name"
                },
                description: {
                    required: "Please enter description"
                }
            },
//            submitHandler: function (form) {
//                $("#Discussion_create_form_validation").submit();
//               return true;
//            },
                errorPlacement: function(error, $elem) {
            if ($elem.is('textarea')) {
                $elem.insertAfter($elem.next('div'));
            }
            error.insertAfter($elem);
        },
        });
    });
</script>
