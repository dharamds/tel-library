<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SchoolsMetadatas extends MY_Controller 
{
	public function __construct()
	{

		parent::__construct();
		//Do your magic here
		$this->load->module('template'); 
	    $this->load->model('common_model');  
		$this->load->library('form_validation');
		
		// cache settings
		$this->config->set_item('cache_path', APPPATH. '/cache/metadata/');
		$this->load->driver('cache',
        array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'tel_')
		);

		if (!$this->ion_auth->logged_in()):
		    redirect('users/auth', 'refresh');
		endif;
	}


	/**
     * saveMetadata method
     * @description this function use to save metadata 
     * @param string
     * @return json array
     */	
	public function saveMetadata()
	{	
		if($this->input->post())
		{	
			$catRemoveCon = [
				'reference_id'   => $this->session->userdata('siteID'),
				'reference_type' => 5
			];
			$this->common_model->DeleteDB('category_assigned',$catRemoveCon);

			$categoriesData = [];
			foreach ($this->input->post('institutional_cat') as $key => $value) {
				$categoriesData[] = [
					'reference_id'   => $this->session->userdata('siteID'),
					'reference_type' => 5,
					'type'           => DEFAULT_CAT,
					'category_id'    =>  $value
				];
			}
			foreach ($this->input->post('institutional_sys_cat') as $key => $value) {
				$categoriesData[] = [
					'reference_id'   => $this->session->userdata('siteID'),
					'reference_type' => 5,
					'type'           => SYSTEM_CAT,
					'category_id'    =>  $value
				];
			}
			$this->common_model->InsertBatchData('category_assigned',$categoriesData);
		}
		exit;
			
	}

	/**
     * saveAssignTag method
     * @description this function use to save and assigned tag
     * @return json array
     */	
	public function saveAssignTag() 
	{
		if($this->input->post())
		{	
	
			$assignedTag = [
				'reference_id'   	=> $this->input->post('post_data')['referenceID'],
				'reference_type' 	=> $this->input->post('post_data')['referencetype'],
				'reference_sub_type'=> $this->input->post('post_data')['reference_sub_type'],
				'tag_id'         	=>  $this->__getTagId($this->input->post('tag'),$this->input->post('post_data')['referencetype'])
			];
			// delete cache
			$cache_key = 'meta_Tag_Assigned_cache_'.$assignedTag['reference_id']."_".$assignedTag['reference_type']."_".$assignedTag['reference_sub_type'];
			$this->cache->delete($cache_key);
			$cache_key1 = 'meta_filter_cache_'.$this->input->post('post_data')['referencetype']."_".$this->input->post('post_data')['reference_sub_type'];
			$this->cache->delete($cache_key1);
			if($tagData = $this->common_model->getRecordByID('tag_assigned',$assignedTag)) {
				echo json_encode(['code'=>202]);
				exit;
			}

			if($this->common_model->InsertData('tag_assigned',$assignedTag)) {
				echo json_encode(['code'=>200,'id'=>$this->db->insert_id(),'name'=>$this->input->post('tag')]);
				exit;
			}
		}	
		exit;
	}

	/**
     * getTagId method
     * @description this function use to save and get id of tag
     * @return int
     */	
	private function __getTagId($tag,$reference_type) 
	{	
		$condition = [
			'tags.name' => $tag,
			'tags.type' => $reference_type,
			'tags.delete_status' => 1
		];
		if($tagData = $this->common_model->getRecordByID('tags',$condition)) {
			$id = $tagData->id;
		}else{
			$tagPostData = [
				'created_by' =>  $this->session->userdata('user_id'),
				'name'   => $this->input->post('tag'),
				'type' => $reference_type,
				'created' => date("Y-m-d H:i:s"),
			];
			if($this->common_model->InsertData('tags',$tagPostData)) {
				$id = $this->db->insert_id();
			}
		}
		return $id;
	}


	/**
     * getAssignedTag method
     * @description this function use to get record
     * @param int
     * @return  array
     */
	public function getAssignedTag()
	{
		if($this->input->post())
		{	$postData = $this->input->post('post_data');
			$cache_key = 'meta_Tag_Assigned_cache_'.$postData['referenceID']."_".$postData['referencetype']."_".$postData['reference_sub_type'];

			$assignedTagData = $this->cache->get($cache_key);
			if(!$assignedTagData){ 
				$this->load->model('Container_model'); 
				$assignedTagData = [];
				if($assignedTagData = $this->Container_model->getAssignedTag($this->input->post('post_data'))) {
					// save cache	
					if($assignedTagData){
						$this->cache->save($cache_key, $assignedTagData, CACHE_LIFE_TIME);
					}						
					echo json_encode(['data'=>$assignedTagData]);
					exit;
				}
			}	
				echo json_encode(['data'=>$assignedTagData]);
				exit;
		}
		exit;
	}

	/**
     * removeAssignTag method
     * @description this function use to remove tag
     * @param int
     * @return json array
     */
	public function removeAssignTag()
	{
		
		if($this->input->post())
		{
			// delete cache
			$gettagdata = $this->common_model->getAllData('tag_assigned', 'reference_id, reference_type, reference_sub_type', true, ['id' => $this->input->post('id')]);
			$cache_key = 'meta_Tag_Assigned_cache_'.$gettagdata->reference_id."_".$gettagdata->reference_type."_".$gettagdata->reference_sub_type;
			$this->cache->delete($cache_key);
			/*$cache_key1 = 'meta_filter_cache_'.$this->input->post('post_data')['referencetype']."_".$this->input->post('post_data')['reference_sub_type'];
			$this->cache->delete($cache_key1);*/
			if($result = $this->common_model->DeleteDB('tag_assigned',['id'=>$this->input->post('id')])) {
				echo json_encode(['code'=>200,'message'=>'You have succefully deleted tag']);
			
				exit;
			}else{
				echo json_encode(['code'=>400,'message'=>"Something went wrong, please try again!"]);
				exit;
			}				

		}
		exit;	
	}

	/**
     * get_tag_section method
     * @description this function use to display tag view
     * @return html
     */
	public function get_tag_section()
	{	
		$data['page'] = "containers/get_tag_section";
      	$this->template->ajax_view($data,true);
	}

	private function __getMakeParentChild($element = [], $parentId = 0)
	{	
		$returnArray = [];
		foreach ($element as $key => $value) {	
			if ($value['parent'] == $parentId) 
			{
	            $children = $this->__getMakeParentChild($element, $value['id']);
	            
				if ($children) 
				{
	                $value['children'] = $children;
	            }
				$returnArray[] = $value;
	        }
		}
		return $returnArray;

	}


	public function get_cat_filter()
	{
		// cache settings
		$cache_key = 'meta_filter_cache_'.post('type')."_".post('sub_type');
		$data['categories'] = $this->cache->get($cache_key);
		if(!$data['categories']) {
			if(post('type')): 					
				switch(post('type'))
					{
						case '1':
						$data['categories'] = $this->common_model->getAllData('categories', 'id, name, parent', '', ['type' => post('sub_type'), 'delete_status' => 1,'status'=>1],'categories.name ASC');
						$data['selector'] = ucfirst(post('selector'));
						break;

						case '2':
						$data['categories'] = $this->common_model->getAllData('tags', 'id, name, type', '', ['type' => post('sub_type'), 'delete_status' => 1,'status'=>1],'tags.name ASC');
						$data['selector'] = ucfirst(post('selector'));
						break;

						default:
						$data['categories'] = $this->common_model->getAllData('categories', 'id, name, parent', '', ['type' => post('sub_type'), 'delete_status' => 1,'status'=>1],'categories.name ASC');
						$data['selector'] = ucfirst(post('selector'));
					}
			endif;
			$data['categories'] = json_decode(json_encode($data['categories']), True);
			
			//$data['categories'] = $this->__getMakeParentChild($data['categories']);
			if($data['categories']){
				$this->cache->save($cache_key, $data['categories'], CACHE_LIFE_TIME);
			}
	}
	//pr($data); exit;
		$data['page'] = "containers/get_category_filters";
      	$this->template->ajax_view($data,true);
	}

	/**
     * get_tag_section method
     * @description this function use to display tag view
     * @return html
     */
	public function get_cat_section()
	{	
		if($this->input->post())
		{
			// cache settings
			$cache_key = 'meta_cat_cache_'.post('referenceID').'_'.post('referencetype')."_".post('reference_sub_type');
			$data = $this->cache->get($cache_key);
			if(!$data) {
			$this->load->model('Container_model'); 
			$result = $this->common_model->getAllData('categories', 'id,name,parent', '', ['categories.type' => $this->input->post('referencetype'),'categories.delete_status' => 1,'categories.status'=>1],'categories.name ASC');	
			foreach ($result as $key => $value) {
				$data['categories'][$value->id] = [
					'id' => $value->id,
					'name' => $value->name,
					'parent' =>  $value->parent
				];
			}

			$data['categories'] = $this->__getMakeParentChild($data['categories']);
			$selectedResult = $this->common_model->getAllData('category_assigned', 'category_id', '', ['category_assigned.reference_type' => $this->input->post('referencetype'),'category_assigned.reference_id' => $this->input->post('referenceID'),'category_assigned.reference_sub_type' => $this->input->post('reference_sub_type')]);
			foreach ($selectedResult as $key => $value) {
				$data['selectedData'][] = $value->category_id;
			}
			if($data){
				$this->cache->save($cache_key, $data, CACHE_LIFE_TIME);	
			}						
		}
	}
		$data['page'] = "containers/get_category_section";
      	$this->template->ajax_view($data,true);
	}

	/**
     * saveAssignCategories method
     * @description this function use to save data in assign categories
     * @param int
     * @return json array
     */
	public function saveAssignCategories()
	{
		if($this->input->post())
		{	
			$postData = $this->input->post();

			$this->common_model->DeleteDB('category_assigned', [
				'reference_id' => $postData['post_data']['referenceID'],
				'reference_type' => $postData['post_data']['referencetype'],
				'reference_sub_type' => $postData['post_data']['reference_sub_type']
			]);
		// delete cache	
		$this->cache->delete('meta_cat_cache_'.$postData['post_data']['referenceID'].'_'.$postData['post_data']['referencetype']."_".$postData['post_data']['reference_sub_type']);	

		if($postData['categoriesIDS'])
			{
				foreach ($postData['categoriesIDS'] as $key => $value) {
					$saveData[] = [
						'reference_id'   => $postData['post_data']['referenceID'],
						'reference_type' => $postData['post_data']['referencetype'],
						'category_id'    => $value,
						'reference_sub_type' => $postData['post_data']['reference_sub_type']
					];
				}
		        

				if($saveData)
				{
					if($this->db->insert_batch('category_assigned', $saveData))
					{
						if($postData['actionType'] == 'save'){
							echo json_encode(['code'=>200,'message'=>'You have succefully save category']);
							exit;
						}else{
							echo json_encode(['code'=>200,'message'=>'You have succefully removed category']);
							exit;
						}
						
					}else{
						echo json_encode(['code'=>400,'message'=>'Something went wrong, please try again!']);
						exit;
					}	
				}
			}
				
				echo json_encode(['code'=>200,'message'=>'You have succefully removed category']);
				exit;	
		}
		exit;	
	}

	/**
     * get_all_active_meta_tags method
     * @description this function use to get all tags(only active)
     * @param int
     * @return json array
     */
	public function get_all_active_meta_tags(){
		$type = $this->input->post('sectionData')['referencetype'];
		$keyword = $this->input->post('keyword')['term'];

			$cache_key = 'get_all_active_meta_tags_'.$type."_".md5($keyword);
			$result = $this->cache->get($cache_key);
			if(!$result){
				$this->load->model('Container_model'); 
				$data = $this->Container_model->getTags(['tags.type'=>$type,'tags.delete_status' => 1,'tags.status'=>1],$keyword);
				$result = [];
				foreach ($data as $key => $value) {
					$result[] = [
						'value'=>$value->id,
						'label'=>$value->name
					];
			}
			// save caches
			if($result){
				$this->cache->save($cache_key, $result, CACHE_LIFE_TIME);
			}
		}
		echo json_encode($result);
		exit;
	}

}	




