<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SchoolsCourse extends MY_Controller 
{
	public function __construct()
	{

		parent::__construct();
		//Do your magic here
		error_reporting(E_ALL ^ E_DEPRECATED);
		ini_set('display_errors', 0);

		$this->load->module('template'); 
	    $this->load->model('common_model');  
	    $this->load->library('form_validation');

		if (!$this->ion_auth->logged_in()):
		    redirect('users/auth', 'refresh');
		endif;

		/*if (!$this->ion_auth->is_admin()): 
	      return show_error("You Must Be An Administrator To View This Page");
	    endif; */
	}

	

	/**
     * assignedCourse method
     * @description this function use assigned course to site (container)
     * @param int
     * @return json array
     */	
	public function assignedCourse(){
		if ($this->input->post()) 
		{	
			switch($this->input->post('actionType')) {
				case 'add' :
					$courseData = [
						'assigned_by' 	=> $this->session->userdata('user_id'),
						'container_id'  => $this->input->post('siteID'),
						'course_id'     => $this->input->post('courseID'),
						'created'   	=> date("Y-m-d H:i:s"),
					];
					if($this->common_model->InsertData('container_courses',$courseData))
					{	
						echo json_encode(['code'=>200,'message'=>"Course has been successfully assigned"]);
						exit;
					}	
				break;

				case 'remove' :
					$conditions = [
						'container_id'  => $this->input->post('siteID'),
						'course_id'     => $this->input->post('courseID'),
					];
					if($this->common_model->DeleteDB('container_courses',$conditions))
					{	
						echo json_encode(['code'=>200,'message'=>"Course has been successfully removed"]);
						exit;
					}	
				break;
			}
		}	
		exit;
	}

	/**
     * saveCourseSections method
     * @description this function use save course section
     * @param post array
     * @return json array
     */	
	public function saveCourseSections(){
		if ($this->input->post()) 
		{
			$this->form_validation->set_rules('section', 'Section','trim|required');
			$this->form_validation->set_rules('course_section', 'Course_section','trim|required');
			$this->form_validation->set_rules('section_start_date', 'Section_start_date','trim|required');
			$this->form_validation->set_rules('section_end_date', 'Section_end_date','trim|required');
			
			if ($this->form_validation->run() == FALSE) 
			{
				echo json_encode(['code'=>400,'message'=>"Something went wrong, please try again!"]);
                exit;
			} 
			
			$secID = ($this->input->post('section_id'))?$this->input->post('section_id'):0;
			$postData = [
				'institute_id'   => $this->input->post('siteID'),
				'course_id'      => $this->input->post('course_section'),
				'name'      	 => $this->input->post('section'),
				'slug'      	 => $this->__getCreateSlug($secID, $this->input->post('section')),
				'start_date'   	 => date("Y-m-d", strtotime($this->input->post('section_start_date'))),
				'end_date'   	 => date("Y-m-d", strtotime($this->input->post('section_end_date'))),
				'created'        =>  date('Y-m-d')
			];

			$condition = [
				'institute_id'  => $this->input->post('siteID'),
				'course_id'     => $this->input->post('course_section'),
				'name'          => $this->input->post('section')
			];

			//unset($saveData['created_by'], $saveData['type']);
            if ($this->input->post('section_id'))
            {
            	$conditionCheckSection = [
            		'id' => $this->input->post('section_id'),
            		'institute_id'  => $this->input->post('siteID')
            	];
            	if($this->common_model->getCountRecord('course_sections',$conditionCheckSection) == 0)
				{
					echo json_encode(['code'=>400,'message'=>"Something went wrong, please try again!"]);
                	exit;
				}
				unset($postData['created']);	
            	if($this->common_model->UpdateDB('course_sections', ['id' => post('section_id')], $postData)){
            		//clean_cache('api', 'tel_courseDetailsBySlugs_');
            		clean_cache('api', 'tel_courseDetailsBySlugs_');
	               // clean_cache('api', 'tel_getLessonByLessonSlug_');
	                //clean_cache('api', 'tel_moduleDetailsBySlugs_');  
            		echo json_encode(['code'=>200,'message'=>"Course section has been updated successfully"]);
                	exit;
            	}	
            }	


			if($this->common_model->getCountRecord('course_sections',$condition) != 0)
			{	
				echo json_encode(['code'=>400,'message'=>"Section is already exist, Please try with new"]);
                exit;
			}	

			if($this->common_model->InsertData('course_sections',$postData))
			{	
				echo json_encode(['code'=>200,'message'=>"Course section has been successfully added"]);
                exit;
			}
				echo json_encode(['code'=>400,'message'=>"Something went wrong, please try again!"]);
                exit;	
		}
		exit;	
	}

	/**
     * __getCreateSlug method
     * @description this function is use to create uniq slug base on title
     * @param  int, string
     * @return string
     */	
	private function __getCreateSlug($id,$name){
		$count = 0;
	    $name = strtolower(url_title($name));
	    $slug_name = $name;            
	    while(true) 
	    {
	        $this->db->select('id');
	        if($id>0){
	        	$this->db->where('id !=', $id);
	        }
	        $this->db->where('slug', $slug_name);   
	        $query = $this->db->get('course_sections');
	        if ($query->num_rows() == 0) break;
	        $slug_name = $name . '-' . (++$count);  
	    }
	    return $slug_name;      
	}
	/**
     * selectedCourse method
     * @description this function use to get selected course
     * @return json array
     */	
	public function selectedCourse() {
		$selectedData = $this->common_model->getAllData('container_courses', ['course_id','container_id'],'',['container_id' => post('siteID'),'status' => 1]);
		foreach ($selectedData as $key => $value) {
			$data['courseAssigned'][] = $value->course_id;
		}

		if(!empty($data['courseAssigned']))
		{
			$coursesCond = [
				'status' =>  1
			];
			if($selectedCourse = $this->common_model->getDatawithIncluse('courses', ['name','id'],$data['courseAssigned'],'id',$coursesCond)){
				$data['courseSelected'] = $selectedCourse;
			}
		}else{
			$data['courseSelected'] = [];
		}
		echo json_encode(['data'=>$data['courseSelected']]);
		exit;
	}


	/**
     * removeSection method
     * @description this function use to remove section from db
     * @param int
     * @return json array
     */	
	public function removeSection() 
	{	
		
		if($this->input->post())
		{
			if($this->common_model->DeleteDB('course_assigned_to_users',['container_id'  => post('siteID'),'section_id'  => base64_decode($this->input->post('id'))])){
				$conditions = [
					'institute_id'  => post('siteID'),
					'id'     => base64_decode($this->input->post('id'))
				];
				if($this->common_model->DeleteDB('course_sections',$conditions))
				{	
					clean_cache('api', 'tel_courseDetailsBySlugs_');
					echo json_encode(['code'=>200,'message'=>"Section has been successfully removed"]);
					exit;
				}
			}
			echo json_encode(['code'=>400,'message'=>"Something went wrong, please try again!"]);
            exit;	
		}
		exit;
	}


	/**
     * get_section_data method
     * @description this function use to get section data by section id
     * @param int
     * @return json array
     */	
	public function get_section_data(){
		if($this->input->post())
		{
			if($result = $this->common_model->getRecordByID('course_sections', ['id' => $this->input->post('id')
			]))
			{	
				$result->start_date = date('m/d/Y', strtotime($result->start_date));
				$result->end_date = date('m/d/Y', strtotime($result->end_date));
				echo json_encode(['code'=>200,'data'=>$result]);
				exit;
			}
				echo json_encode(['code'=>400,'message'=>"Something went wrong, please try again!"]);
            	exit;
		}
		exit;
	}

	/**
     * get_section_data method
     * @description this function use to get section data by section id
     * @param int
     * @return json array
     */

    public function get_course_sections_by_id()
    {
		if($this->input->post())
		{	
			if(post('course_id') == 0)
			{
				echo json_encode(['code'=>202]);
				exit;
			}

			if($result = $this->common_model->getAllData('course_sections',['name', 'id'],'',['delete_status' => 1, 'status' => 1,'institute_id'=>post('siteID'),'course_id'=>post('course_id')]))
			{
				echo json_encode(['code'=>200,'data'=>$result]);
				exit;
			}
				echo json_encode(['code'=>400]);
				exit;
		}
		exit;
	} 	

}	




