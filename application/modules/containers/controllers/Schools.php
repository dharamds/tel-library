<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Schools extends MY_Controller
{

    public function __construct()
    {

        parent::__construct();
        //Do your magic here
        error_reporting(E_ALL ^ E_DEPRECATED);
        ini_set('display_errors', 0);

        $this->load->module('template');
        $this->load->model('common_model');
        $this->load->model('Container_model');
        $this->load->library('form_validation');

        if (!$this->ion_auth->logged_in()) :
            redirect('users/auth', 'refresh');
        endif;
       // get controller permissions
       if ($this->current_user_permissions = $this->get_permissions()) {
        $this->add_permission = $this->current_user_permissions->add_per;
        $this->edit_permission = $this->current_user_permissions->edit_per;
        $this->delete_permission = $this->current_user_permissions->delete_per;
        $this->view_permission = $this->current_user_permissions->view_per;
        $this->list_permission = $this->current_user_permissions->list_per;
        }
    // get container_id
    $this->container_id = get_container_id();
       
    }

    public function edit($id = 0)
    {

        if ($id && !$this->edit_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect($_SERVER['HTTP_REFERER'], 'refresh');
        endif;
        $this->session->set_userdata('action', 'edit');

        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Institutional/Sites', 'link' => base_url('institutions'), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Edit', 'link' => '', 'class' => 'active'];
        $id = base64_decode($id);
        $data['siteID'] = $id;
        $data['action'] = "edit";
       
        $condition = [
            'id' => $id,
            'status_delete' => 1
        ];
        if ($this->common_model->getCountRecord('containers', $condition) == 0) {
            $this->session->set_flashdata('error', "Something went wrong");
            redirect('institutions', 'refresh');
        }
        $data['page'] = "containers/setup";
        $this->template->template_view($data, true);
    }

    /**
     * setup method
     * @description this function use to create container
     * @return void
     */
    public function setup()
    {
        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Institutional/Sites', 'link' => base_url('institutions'), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Create', 'link' => '', 'class' => 'active'];
        
        $this->session->set_userdata('action', 'add');

        $data['page']                   = "containers/setup";
        $this->template->template_view($data, true);
        //exit;
    }

    /**
     * other_load_view method
     * @description this function use to load view in tab
     * @return html
     */
    public function other_load_view($type = '', $recordID = '', $siteID = '')
    {   
        $data = [];
        switch ($type) {
            case 'courses':
                $template_path = "containers/setup_course_section_details";
                if ($recordID > 0) {
                   // $this->load->model('Container_model');
                    $data['sectionData'] = $this->Container_model->getSectionDetails($recordID);
                    $this->session->set_userdata('sectionID', $recordID);
                    $this->session->set_userdata('courseID', $data['sectionData']->course_id);
                }
                break;

            case 'groups':
                $data['containerDetails'] = $this->common_model->getRecordByIdType($siteID, 1, 'name');
                $courseData = $this->Container_model->getAllActiveCourseBySiteID($siteID);
                $data['courses'][''] = '-- Courses --';
                foreach ($courseData as $key => $value) {
                    $data['courses'][$value['id']] = $value['name'];
                }
                if ($recordID > 0 || $recordID != 0 || $recordID != '') {
                    // $this->load->model('Groups_model');
                    $groupData = $this->common_model->getRecordByID('container_groups', ['container_groups.id' => $recordID], 'course_id,section_id,name,display_avaibility,id');
                    $data['groupData'] = $groupData;
                    $data['recordID'] = $recordID;
                } else {
                    $data['recordID'] = $recordID;
                    $data['groupData'] = '';
                }
                $template_path = "containers/groups/manually_create";

                break;

            case 'groups_set_view':
               // $data['containerDetails'] = $this->common_model->getRecordByIdType($siteID, 1, 'name');
               // $this->load->model('Container_model');
                $courseData = $this->Container_model->getAllActiveCourseBySiteID($siteID);
                $data['courses'][''] = '-- Courses --';
                foreach ($courseData as $key => $value) {
                    $data['courses'][$value['id']] = $value['name'];
                }
                if ($recordID > 0 || $recordID != 0 || $recordID != '') {
                    // $this->load->model('Groups_model');
                    $groupData = $this->common_model->getRecordByID('container_groups', ['container_groups.id' => $recordID], 'course_id,section_id,name,display_avaibility,id');
                    $data['groupData'] = $groupData;
                    $data['recordID'] = $recordID;
                } else {
                    $data['recordID'] = $recordID;
                    $data['groupData'] = '';
                }
                $template_path = "containers/groups/create_group_set";

                break;
        }
        $data['siteID'] = $siteID;
        echo $this->load->view($template_path, $data, true);
        exit;
    }

    /**
     * load_view method
     * @description this function use to load view in tab
     * @return html
     */
    public function load_view($type = '', $siteID = '')
    {   
        switch ($type) {
            case 'identity':
                $template_path = "containers/setup_identity";
                if ($siteID) {
                    $column = 'containers.name,containers.id,containers.domain,containers.slogan,containers.home_page_text,containers.home_footer_text';
                    $data['schoolData'] = $this->common_model->getRecordByIdType($siteID, 1, $column);
                } else {
                    $data = [];
                }
                break;

            case 'courses':
                $template_path = "containers/setup_course";
                $data['courses'] = $this->common_model->getAllData('courses', ['name', 'id'], '', ['delete_status' => 1, 'status' => 1]);
                if ($siteID) {
                    $selectedData = $this->common_model->getAllData('container_courses', ['course_id', 'container_id'], '', ['container_id' => $siteID, 'status' => 1]);
                    foreach ($selectedData as $key => $value) {
                        $data['courseAssigned'][] = $value->course_id;
                    }

                    if (!empty($data['courseAssigned'])) {
                        $coursesCond = [
                            'status' => 1
                        ];
                        if ($selectedCourse = $this->common_model->getDatawithIncluse('courses', ['name', 'id'], $data['courseAssigned'], 'id', $coursesCond)) {
                            $data['courseSelected'] = $selectedCourse;
                        }
                    } else {
                        $data['courseSelected'] = [];
                    }
                }
                if (empty($data['courses']) && empty($data['courseAssigned'])) {
                    $data = [];
                }
                break;

            case 'contactInfo':
                $template_path = "containers/setup_contact";
                $state = $this->common_model->getAllData('states', ['name', 'id'], '', ['status_delete' => 1, 'status' => 1]);
                foreach ($state as $key => $value) {
                    $states[] = $value->name;
                }
                $data['states'] = $states;
                if ($siteID) {
                    $condition = [
                        'institution_contacts.institution_id' => $siteID
                    ];
                    if ($contactData = $this->common_model->getAllData('institution_contacts', '*', '',$condition)) {
                        $contactDetails = [];
                        foreach ($contactData as $key => $value) {
                            $contactDetails[$value->type] = [
                                'same_as_primary' => $value->same_as_primary,
                                'name' => $value->name,
                                'title' => $value->title,
                                'email' => $value->email,
                                'pri_phone' => $value->pri_phone,
                                'sec_phone' => $value->sec_phone,
                            ];
                        }
                        $data['contactData'] = $contactDetails;
                    }else {
                        $data['contactData'] = [];
                    }
                    $columns = 'street_1,street_2,state,city,zip,comments';
                    $containerData = $this->common_model->getRecordByIdType($siteID,1,$columns);
                    $data['siteContactData'] = $containerData;

                }
                break;

            case 'visuals':
                $template_path = "containers/setup_visual";
                if ($siteID) {
                    $column = 'containers.theme_style';
                    $data['themeData'] = $this->common_model->getRecordByIdType($siteID, 1, $column);
                    $data['themeData'] = unserialize($data['themeData']->theme_style);
                } else {
                    $data['themeData'] = [];
                }
                break;

            case 'metadata':
                $template_path = "containers/setup_metadata";
                $data = [];
                /* $data['categories'] = $this->common_model->getAllData('categories', ['name','id'],'',['delete_status' => 1,'status' => 1, 'type'=>1]);
                  $data['systemCategories'] = $this->common_model->getAllData('categories', ['name','id'],'',['delete_status' => 1,'status' => 1, 'type'=>2]);

                  $data['tags'] = $this->common_model->getAllData('tags', ['name','id'],'',['delete_status' => 1,'status' => 1, 'type'=>1]);
                  $data['systemTags'] = $this->common_model->getAllData('tags', ['name','id'],'',['delete_status' => 1,'status' => 1, 'type'=>2]); */
                break;

            case 'users':
                $template_path = "containers/setup_users";
               // $this->load->model('Container_model');
                $ignore = [1, SUPER_ADMIN, SYSTEM_ADMIN,SYSTEM_EDITOR,GRADER,GRADE_ADMINISTRATOR];
                $roles = $this->Container_model->getRoles($ignore)->result_array();
                $rolelist[0] = '--User roles--';
                foreach ($roles as $role) {
                    $rolelist[$role['id']] = $role['name'];
                }
                $data['roles_list'] = $rolelist;
                unset($rolelist[0]);
                $data['roles'] = $rolelist;
                $this->session->unset_userdata('sectionID');
                $this->session->unset_userdata('courseID');
                $this->load->model('users/Users_modal');
                $data['unique_id'] = $this->Users_modal->generate_unique_user_id();
                break;

            case 'section_list':
                //$this->load->model('Container_model');
                $template_path = "containers/setup_course_section";
                if ($siteID) {
                    $selectedData = $this->Container_model->getSectionsListContainer($siteID);
                    $data['course_section'] = $selectedData;
                } else {
                    $data['course_section'] = [];
                }
                break;

            case 'groups':
                $template_path = "containers/setup_groups";
                if ($siteID) {
                    $data['containerDetails'] = $this->common_model->getRecordByIdType($siteID, 1, 'name');
                   // $this->load->model('Container_model');
                    $courseData = $this->Container_model->getAllActiveCourseBySiteID($siteID);
                    $data['courses'][''] = '-- Courses --';
                    foreach ($courseData as $key => $value) {
                        $data['courses'][$value['id']] = $value['name'];
                    }
                }

                break;
        }
        $data['siteID'] = $siteID;
        $data['containerDetails'] = $this->common_model->getRecordByIdType($siteID, 1, 'name');
        echo $this->load->view($template_path, $data, true);
        exit;
    }

    /**
     * getCheckExist method
     * @description this method is use to check name is already exist or not
     * @param string
     * @return json array
     */
    public function getCheckExist()
    {
        
        $this->form_validation->set_rules('domain', 'Domain', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->setup();
        } else {
            $domain = $this->input->post('domain').MAIN_DOMAIN_SITE_CREATION;
            if ($this->common_model->getCheckExist('domain', 'containers', $this->__cleanURL($domain), 1, $this->input->post('siteID')) == 0) {
                echo 'true';
                exit;
            } else {
                echo 'false';
                exit;
            }
        }
        exit;
    }

    /**
     * __cleanURL method
     * @description this method is use to clean url from input string
     * @param string (url)
     * @return string (url)
     */
    private function __cleanURL($input)
    {
        $input = trim($input, '/');
        if (!preg_match('#^http(s)?://#', $input)) {
            $input = 'http://' . $input;
        }
        $urlParts = parse_url($input);
        $domain = preg_replace('/^www\./', '', $urlParts['host']);

        return $domain;
    }

    /**
     * saveIdentity method
     * @description this method is use to insert site creatation data in databse
     * @param string, numbers
     * @return void
     */
    public function saveIdentity()
    {

        if ($this->input->post()) {


            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('slogan', 'Slogan', 'trim|required');
            $this->form_validation->set_rules('domain', 'Domain', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                echo json_encode(['code' => 400, 'message' => "Something went wrong, please try again!"]);
                exit;
            }
            $domain = $this->input->post('domain').MAIN_DOMAIN_SITE_CREATION;
            if ($this->common_model->getCheckExist('domain', 'containers', $this->__cleanURL($domain), 1, post('siteID')) != 0) {
                echo json_encode(['code' => 400, 'message' => "Site is already exist"]);
                exit;
            }

            $saveData = [
                'created_by' => $this->session->userdata('user_id'),
                'type' => 1,
                'name' => post('name'),
                'domain' => $this->__cleanURL($domain),
                'slogan' => post('slogan'),
                'slug' => slugify(post('name')),
                'home_page_text' => post('home_page_text'),
                'home_footer_text' => post('home_footer_text')
            ];

            // clear all courses cache
           // clean_cache_by_key('api', 'tel_getAllCourses');
              

            if (post('siteID')) {
                unset($saveData['created_by'], $saveData['type']);
                if ($this->common_model->UpdateDB('containers', ['id' => post('siteID')], $saveData)) {
                    // clear cache as per domain name key
                    clean_cache_by_key('api', 'tel_'.md5($saveData['domain']));
                    echo json_encode(['code' => 200, 'message' => "Identity step has been successfully updated", 'containerID' => post('siteID')]);
                    exit;
                }
            } else {
                if ($this->common_model->InsertData('containers', $saveData)) {
                    echo json_encode(['code' => 200, 'message' => "Identity step has been successfully completed", 'containerID' => $this->db->insert_id()]);
                    exit;
                }
            }
            // cache delete



            echo json_encode(['code' => 400, 'message' => "Something went wrong, please try again!"]);
            exit;
        }
        exit;
    }

    /**
     * saveContact method
     * @description this method is use to save contact data in databse
     * @param string, numbers
     * @return void
     */
    public function saveContact()
    {
        if ($this->input->post()) {
            $siteID = post('siteID');
            $postData = [];
            $postData['institution_address'] = $this->input->post('institution')['institution_address'];
            $postData['institution_address']['comments'] = $this->input->post('institution')['comments'];
            $postData['primary'] = $this->input->post('institution')['primary'];
            $postData['primary']['type'] = 'primary';
            $postData['primary']['institution_id'] = $siteID;
            $postData['instructional_support']['same_as_primary'] = 0;

            if (isset($this->input->post('institution')['instructional_support']['same_as_primary']) && $this->input->post('institution')['instructional_support']['same_as_primary'] != '') {
                $postData['instructional_support'] = $this->input->post('institution')['primary'];
                $postData['instructional_support']['same_as_primary'] = 1;
                $postData['instructional_support']['type'] = 'instructional_support';
                $postData['instructional_support']['institution_id'] = $siteID;
            } else {
                $postData['instructional_support'] = $this->input->post('institution')['instructional_support'];
                $postData['instructional_support']['type'] = 'instructional_support';
                $postData['instructional_support']['institution_id'] = $siteID;
            }

            if (isset($this->input->post('institution')['technical']['same_as_primary']) && $this->input->post('institution')['technical']['same_as_primary'] != '') {
                $postData['technical'] = $this->input->post('institution')['primary'];
                $postData['technical']['same_as_primary'] = 1;
                $postData['technical']['type'] = 'technical';
                $postData['technical']['institution_id'] = $siteID;
            } else {
                $postData['technical'] = $this->input->post('institution')['technical'];
                $postData['technical']['type'] = 'technical';
                $postData['technical']['institution_id'] = $siteID;
            }

            if (isset($this->input->post('institution')['billing']['same_as_primary']) && $this->input->post('institution')['billing']['same_as_primary'] != '') {
                $postData['billing'] = $this->input->post('institution')['primary'];
                $postData['billing']['same_as_primary'] = 1;
                $postData['billing']['type'] = 'billing';
                $postData['billing']['institution_id'] = $siteID;
            } else {
                $postData['billing'] = $this->input->post('institution')['billing'];
                $postData['billing']['type'] = 'billing';
                $postData['billing']['institution_id'] = $siteID;
            }

            if (isset($this->input->post('institution')['site_manager_1']['same_as_primary']) && $this->input->post('institution')['site_manager_1']['same_as_primary'] != '') {
                $postData['site_manager_1'] = $this->input->post('institution')['primary'];
                $postData['site_manager_1']['same_as_primary'] = 1;
                $postData['site_manager_1']['type'] = 'site_manager_1';
                $postData['site_manager_1']['institution_id'] = $siteID;
            } else {
                $postData['site_manager_1'] = $this->input->post('institution')['site_manager_1'];
                $postData['site_manager_1']['type'] = 'site_manager_1';
                $postData['site_manager_1']['institution_id'] = $siteID;
            }

            if (isset($this->input->post('institution')['site_manager_2']['same_as_primary']) && $this->input->post('institution')['site_manager_2']['same_as_primary'] != '') {
                $postData['site_manager_2'] = $this->input->post('institution')['primary'];
                $postData['site_manager_2']['same_as_primary'] = 1;
                $postData['site_manager_2']['type'] = 'site_manager_2';
                $postData['site_manager_2']['institution_id'] = $siteID;
            } else {
                $postData['site_manager_2'] = $this->input->post('institution')['site_manager_2'];
                $postData['site_manager_2']['type'] = 'site_manager_2';
                $postData['site_manager_2']['institution_id'] = $siteID;
            }

            $condition = [
                'institution_id' => $siteID
            ];
            
            if ($this->common_model->UpdateDB('containers', ['id' => $siteID], $postData['institution_address'])) {
                $siteDetails = $this->common_model->getRecordByID('containers',['id' => $siteID],'domain');
                // clear cache as per domain name key
                clean_cache_by_key('api', 'tel_'.md5($siteDetails->domain));
                $this->common_model->DeleteDB('institution_contacts',$condition);
                $contactLabel = [
                    'primary','instructional_support','technical','billing','site_manager_1','site_manager_2'
                ];
                foreach ($postData as $key => $value) {
                    if(in_array($key, $contactLabel)){
                        if($value['name'] != '' || $value['email'] != '' ){
                            $contactArray[] = $value;
                        }
                    }
                }
                if($contactArray){
                    foreach ($contactArray as $key => $value) {
                            $saveContact[] = [
                                'institution_id' => $value['institution_id'],
                                'same_as_primary' =>(isset($value['same_as_primary'])?$value['same_as_primary']:0),
                                'name' =>$value['name'],
                                'title' =>$value['title'],
                                'email' =>$value['email'],
                                'pri_phone' =>$value['pri_phone'],
                                'sec_phone' =>$value['sec_phone'],
                                'type' =>$value['type']
                            ];
                    }
                    
                }
                
                if($this->db->insert_batch('institution_contacts', $saveContact))
                {
                   /*if($createUser == 1){
                        if($this->input->post('institution')['instructional_support']['name'] == '' || $this->input->post('institution')['instructional_support']['email']  == '')
                        {
                            if(!isset($this->input->post('institution')['instructional_support']['same_as_primary'])){

                                if ($userID = $this->__createUser($this->input->post('institution')['primary'])) {
                                    $userRoleData = [
                                        'user_id' => $userID,
                                        'role_id' => STUDENT,
                                        'container_id' => $siteID
                                    ];
                                    if ($this->common_model->InsertData('containers_users_roles', $userRoleData)) {
                                        unset($userRoleData['container_id']);
                                        if ($this->common_model->InsertData('users_roles', $userRoleData)) {
                                            echo json_encode(['code' => 200, 'message' => "Contact step has been successfully completed"]);
                                            exit;
                                        }
                                    }
                                }
                            }else{
                                echo json_encode(['code' => 200, 'message' => "Contact step has been successfully completed"]);
                                exit;
                            }
                        }else{
                            echo json_encode(['code' => 200, 'message' => "Contact step has been successfully completed"]);
                            exit;
                        }
                        echo json_encode(['code' => 400, 'message' => "Something went wrong, please try again!"]);
                        exit;
                    } */
                    echo json_encode(['code' => 200, 'message' => "Contact step has been successfully completed"]);
                    exit; 
                }
                echo json_encode(['code' => 400, 'message' => "Something went wrong, please try again!"]);
                exit;
            }
        }
        exit;
    }

    /**
     * createUser method
     * @description this function use to create user account
     * @return int
     */
    private function __createUser($data = [])
    {
        $this->load->model('users/Users_modal');
        if ($userDetails = $this->Users_modal->get_details_by_email($data['email'])) {
            return $userDetails->id;
        } else {

            $userPostData = [
                'first_name' => $data['name'],
                'email' => $data['email'],
                'phone' => $data['pri_phone'],
                'username' => $data['email'],
                'password' => $this->ion_auth->hash_password("password"),
                'date' => date('Y-m-d'),
                'active' => 1,
                'unique_id' => $this->Users_modal->generate_unique_user_id()
            ];
            if ($this->common_model->InsertData('users', $userPostData)) {
                return $this->db->insert_id();
            }
            return false;
        }
    }

    /**
     * get_city_per_state method
     * @description this function use to get cities as per state
     * @return json array
     */
    public function get_city_per_state()
    {
        //$this->load->model('Container_model');
        $citiesArray = $this->Container_model->getCitiesByState($this->input->post('state'));
        foreach ($citiesArray as $key => $value) {
            $cities[] = $value['name'];
        }
        if ($cities) {
            echo json_encode(['code' => 200, 'data' => $cities, 'message' => 'OK']);
            exit;
        }
        echo json_encode(['code' => 400, 'data' => '', 'message' => 'No result found']);
        exit;
    }

    /**
     * saveVisual method
     * @description this function use to save theme setting
     * @return json array
     */
    public function saveVisual()
    {
        if ($this->input->post()) {
            $siteID = post('siteID');
            $config['upload_path'] = FCPATH . 'uploads/containers/logo/';
            $config['allowed_types'] = 'jpeg|jpg|png';            //$config['overwrite'] = TRUE;

            $this->load->library('upload', $config);
            if (!empty($_FILES['logo']['name'])) {
                if (!$this->upload->do_upload('logo')) {
                    echo json_encode(['code' => 400, 'message' => $this->upload->display_errors()]);
                    exit;
                } else {
                    //$data = array('upload_data' => $this->upload->data());
                    //$image = $data['upload_data']['file_name'];
                    $image = post('uploaded_img_name');
                    resizeImage('containers/logo/' . $image, 'containers/logo/thumb/', 250, 200);
                    if (post('old_img')) {
                        unlink(FCPATH . "uploads/containers/logo/" . post('old_img'));
                        unlink(FCPATH . "uploads/containers/logo/thumb/" . post('old_img'));
                    }
                }
            } else {
                $image = post('old_img');
            }

            $postData = $this->input->post('theme');
            $postData['logo'] = $image;
            
            $saveData = [
                'theme_style' => serialize($postData)
            ];

            if ($this->common_model->UpdateDB('containers', ['id' => $siteID], $saveData)) {
                $siteDetails = $this->common_model->getRecordByID('containers',['id' => $siteID],'domain');
                // clear cache as per domain name key
                clean_cache_by_key('api', 'tel_'.md5($siteDetails->domain));
                echo json_encode(['code' => 200, 'message' => "Theme setting has been successfully added"]);
                exit;
            }
            echo json_encode(['code' => 400, 'message' => "Something went wrong, please try again!"]);
            exit;
        }
    }

    

    /**
     * index method
     * @description this method is use to insert site creatation data in databse
     * @param string, numbers
     * @return array
     */
    public function manage()
    {   

        // check permissions
        if (!$this->list_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect('users/auth', 'refresh');
        endif;

        if($this->container_id > 0){
             redirect('containers/schools/site', 'refresh');
        }

        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Institutional/Sites', 'link' => '', 'class' => 'active'];

        //$data['containers'] = $this->common_model->getAllData('containers', '*', '', ['status_delete' => 1]);
        $data['page'] = "containers/manage_schools";
        $this->template->template_view($data);
    }

    /**
     * datatable method
     * @get data for server side datatable
     * @param string, numbers
     * @return array
     */
    public function getLists()
    {
        // Fetch records
        $data = $this->Container_model->getRows($_POST);
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $data['total'],
            "recordsFiltered" => $data['total'],
            "data" => $data['result']
        );

        // Output to JSON format
        echo json_encode($output);
    }

   

    /**
     * get_make_action_request method
     * @description this method is use to make action as per request(like status change)
     * @param int
     * @return json array
     */
    public function get_make_action_request()
    {
        if ($this->input->post()) {
            $updateData = [];
            switch ($this->input->post('actionType')) {
                case 'status':
                    $updateData = [
                        'status' => post('status')
                    ];
                    $message = "Status changed successfully";
                    break;

                case 'delete':
                    $updateData = [
                        'status_delete' => post('status')
                    ];
                    $message = "Record removed successfully";
                    break;
            }
            if ($this->common_model->UpdateDB('containers', ['id' => post('id')], $updateData)) {
                echo json_encode(['code' => 200, 'message' => $message]);
                exit;
            }
            echo json_encode(['code' => 400, 'message' => "Something went wrong, please try again!"]);
            exit;
        }
        exit;
    }


    public function get_remove_site()
    {
        if ($this->input->post()) {
            $postArray = [];
            foreach ($this->input->post('selected_record') as $key => $value) {
                $postArray[] = [
                    'id'            => $value,
                    'status_delete' => 0
                ];
            }
            if ($this->common_model->multiUpdateRecords('containers', $postArray, 'id')) {
                echo json_encode(['code' => 200, 'message' => "You have been removed record successfully"]);
                exit;
            }
            echo json_encode(['code' => 400, 'message' => "Something went wrong, please try again!"]);
            exit;
        }
        exit;
    }

    public function get_change_status()
    {
        if ($this->input->post()) {
            $postArray = [];
            foreach ($this->input->post('selected_record') as $key => $value) {
                $postArray[] = [
                    'id'            => $value,
                    'status' => 0
                ];
                $postArrayIDs[] = $value;
            }

            if ($this->common_model->getCountRecordUsingincluse('containers', '', $postArrayIDs, 'id', ['status' => 1]) == 0) {
                echo json_encode(['code' => 400, 'message' => "Selected record is already deactivated"]);
                exit;
            }

            if ($this->common_model->multiUpdateRecords('containers', $postArray, 'id')) {
                echo json_encode(['code' => 200, 'message' => "You have been deactivated successfully"]);
                exit;
            }
            echo json_encode(['code' => 400, 'message' => "Something went wrong, please try again!"]);
            exit;
        }
        exit;
    }

    /**
     * get_make_action_request method
     * @description this method is use to make action as per request(like status change)
     * @param int
     * @return json array
     */
    public function img_crop_upload()
    {

        if (isset($_POST["image"])) {
            $data = $_POST["image"];
            $image_array_1 = explode(";", $data);
            $image_array_2 = explode(",", $image_array_1[1]);
            $data = base64_decode($image_array_2[1]);
            $imageName = time() . '.png';
            file_put_contents("uploads/containers/logo/".$imageName, $data);
            $arr=[
                "img_full_name" => base_url()."uploads/containers/logo/".$imageName,
                "img_name" => $imageName
            ];
            echo json_encode($arr); exit;
        }
    }


    /*Site Manage admin area*/
     /**
     * setup method
     * @description this function use to create container
     * @return void
     */
    public function site()
    {
        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Institutional/Site', 'link' => base_url('/containers/schools/site'), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Edit', 'link' => '', 'class' => 'active'];
        $data['siteID'] = $this->container_id;
       
        $condition = [
            'id' => $this->container_id,
            'status_delete' => 1
        ];
        if ($this->common_model->getCountRecord('containers', $condition) == 0) {
            $this->session->set_flashdata('error', "Something went wrong");
            redirect('institutions', 'refresh');
        }
        $data['page'] = "containers/setup";
        $this->template->template_view($data, true);
    }

    public function groups()
    {   
        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Institutional/Sites', 'link' => '', 'class' => 'active'];
        
        if ($this->container_id) {
            $courseData = $this->Container_model->getAllActiveCourseBySiteID($this->container_id);
            $data['courses'][''] = '-- Courses --';
            foreach ($courseData as $key => $value) {
                $data['courses'][$value['id']] = $value['name'];
            }
            $data['siteID'] = $this->container_id;
        }
        $data['page'] = "containers/site_group_list";
        $this->template->template_view($data, true);
    }

    public function get_export_site()
    {
        if($this->input->post())
        {   
            //$data = $this->Container_model->getRows($_POST);

            $file_name = 'institutional_list'.strtotime("now"). '.csv';
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$file_name");
            header("Content-Type: application/csv;");
            // get data
            $_POST['export_csv'] = true;
            $_POST['selected_site_ids'] = $this->input->post('selected_record');
            /*$data = $this->Container_model->getContainerRowsForExport($_POST);*/
            $data = $this->Container_model->getContainerRowsForExport($_POST);
            // file creation
            $file = fopen('php://output', 'w');

            $header = array("Sr. No", "Name",/*"URL",*/ "Primary Contact", "No of Courses", "No of Sections", "No of Students", "No of Enrolled Students", "Status");
            fputcsv($file, $header);
            $i = 1;
            foreach ($data['result'] as $key => $value) {
                $nstatus = ($value->status == 1) ? 'Active' : 'Inactive';
                $narray = [
                            $i, 
                            $value->name, 
                           /* $value->domain, */
                            $value->primary_contact_name, 
                            $value->total_course, 
                            $value->total_section, 
                            $value->total_user, 
                            $value->total_enroll_user,
                            $nstatus
                        ];
                fputcsv($file, $narray);
                $i++;
            }
            fclose($file);
            exit;
        }   
       exit; 
    }


    public function get_number_current_courses($recordID)
    {
        $data['typeid'] = $recordID;
        list($recordID, $type) = explode("_", $recordID);
        $data['value'] = $this->Container_model->get_number_current_courses($recordID);
        echo json_encode($data); 
        exit;
    }

    public function get_number_current_section($recordID)
    {
        $data['typeid'] = $recordID;
        list($recordID, $type) = explode("_", $recordID);
        $data['value'] = $this->Container_model->get_number_current_section($recordID);
        echo json_encode($data); 
        exit;
    }

    public function get_number_current_student($recordID)
    {
        $data['typeid'] = $recordID;
        list($recordID, $type) = explode("_", $recordID);
        $data['value'] = $this->Container_model->get_number_current_student($recordID);
        echo json_encode($data); 
        exit;
    }

    public function get_number_current_enrollment($recordID)
    {
        $data['typeid'] = $recordID;
        list($recordID, $type) = explode("_", $recordID);
        $data['value'] = $this->Container_model->get_number_current_enrollment($recordID);
        echo json_encode($data); 
        exit;
    }
    
    
}
