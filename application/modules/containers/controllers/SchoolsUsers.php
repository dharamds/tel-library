<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SchoolsUsers extends MY_Controller
{
	public function __construct()
	{

		parent::__construct();
		//Do your magic here
		error_reporting(E_ALL ^ E_DEPRECATED);
		ini_set('display_errors', 0);


		$this->load->module('template');
		$this->load->model('common_model');
		$this->load->library('form_validation');

		if (!$this->ion_auth->logged_in()) :
			redirect('users/auth', 'refresh');
		endif;

		/*if (!$this->ion_auth->is_admin()): 
	      return show_error("You Must Be An Administrator To View This Page");
	    endif; */
	}

	/**
	 * getUploadUserView method
	 * @description this function use to get view of upload user section
	 * @return html
	 */
	public function getUploadUserView($siteID = '', $callBackRequest = '', $uploadLocation = 0)
	{
		$template_path = "containers/ajax_upload_users";
		if ($uploadLocation == 1) {
			$data['uploadLocation'] = $uploadLocation;
			$userRoles = [STUDENT, INSTRUCTOR, SITE_MANAGER, PARENTS, GROUP_LEADER];
			$roles = $this->db->where_in('id', $userRoles)->get('roles')->result();
			//$roles= $this->ion_auth->where(['id' => INSTRUCTOR])->roles()->result();
		} else if ($uploadLocation == 2) {
			$data['uploadLocation'] = $uploadLocation;
			$userRoles = [STUDENT, INSTRUCTOR];
			$roles = $this->db->where_in('id', $userRoles)->get('roles')->result();
		} else {
			$roles = $this->ion_auth->where(['id >' => 1, 'id > ' => currentGroup()->level])->roles()->result();
		}

		$data['roles'][] = '-- Roles --';
		foreach ($roles as $item) {
			$data['roles'][$item->id] = $item->name;
		}
		$data['institutions'][] = '-- Institutions --';
		foreach ($this->common_model->getAllData('containers', 'id, name', '', ['type' => 1, 'status_delete' => 1, 'status' => 1]) as $item) {
			$data['institutions'][$item->id] = $item->name;
		}

		$data['siteID'] = $siteID;
		if ($callBackRequest) {
			$data['callBackRequest'] = $callBackRequest;
		} else {
			$data['callBackRequest'] = '';
		}

		echo $this->load->view($template_path, $data, true);
		exit;
	}

	/**
	 * uploadUsers method
	 * @description this function use to upload users from files
	 * @param file
	 * @return json array
	 */
	public function uploadUsers()
	{
		ini_set('max_execution_time', 0);
		$this->load->model('users/Users_modal');
		if ($this->input->post()) {
			$allowed_ext = ["csv", "xls", "xlsx"];
			$splitFiles = explode(".", $_FILES["bulkupload"]["name"]);
			$extension = end($splitFiles);
			$userRoleData = [];
			$cont_users_roles_data = [];
			$users_roles_data = [];
			$assign_data_array = [];
			/*$siteID = $this->input->post('container_id');
      		$course_id = $this->input->post('course_id');
      		$section_id = $this->input->post('section_id');*/

			/*if( $this->input->post('upload_loc') == 1 )
      		{
      			$userRoll = STUDENT;
      		}
      		else
      		{
      			$userRoll = $this->input->post('user_type');
      		}*/

			if (in_array($extension, $allowed_ext)) {
				switch ($extension) {
					case 'csv':
						$this->__getUploadUsersFromCSV($_FILES['bulkupload']['tmp_name'], $this->input->post('user_type'), $this->input->post('container_id'), $this->input->post('course_id'), $this->input->post('section_id'), $this->input->post('group_id'));
						break;

					case 'xls':
						$this->__getUploadUsersFromXLS($_FILES['bulkupload']['tmp_name'], $this->input->post('user_type'), $this->input->post('container_id'), $this->input->post('course_id'), $this->input->post('section_id'), $this->input->post('group_id'));
						break;

					case 'xlsx':
						$this->__getUploadUsersFromXLS($_FILES['bulkupload']['tmp_name'], $this->input->post('user_type'), $this->input->post('container_id'), $this->input->post('course_id'), $this->input->post('section_id'), $this->input->post('group_id'));
						break;
				}
			} else {
				echo json_encode(['code' => 400, 'message' => "Something went wrong, please try again!"]);
				exit;
			}
		}
		exit;
	}


	/**
	 * __getUploadUsersFromXLS method
	 * @description this function use to upload users from XLS/XLSX files
	 * @param file, int
	 * @return json array
	 */
	public function __getUploadUsersFromXLS($tempFile, $rollID, $containerID = 0, $courseID = 0, $sectionID = 0)
	{
		$assign_data_array = [];
		$fileData = [];
		require(APPPATH . 'third_party/spreadsheet/vendor/autoload.php');

		$fxls = $tempFile;
		$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($fxls);
		$xls_data = $spreadsheet->getActiveSheet()->toArray();
		//$data = array_map('array_filter', $xls_data);
		$count = 1;
		foreach ($xls_data as $key => $value) {
			if ($count > 1) {
				$fileData = [
					'first_name'   => (!empty($value[0]) ? $value[0] : null),
					'middle_name'  => (!empty($value[1]) ? $value[1] : null),
					'last_name'    => (!empty($value[2]) ? $value[2] : null),
					'username'     => (!empty($value[3]) ? $value[3] : null),
					'email'        => (!empty($value[3]) ? $value[3] : null),
					'secondary_email' => (!empty($value[4]) ? $value[4] : null),
					'institutional_unique_id' 	=> (!empty($value[5]) ? $value[5] : null),
					'phone'        => (!empty($value[6]) ? $value[6] : null),
					'cellphone'    => (!empty($value[7]) ? $value[7] : null),
					'password'     => (!empty($value[8]) ? $value[8] : $this->ion_auth->hash_password(DEFAULT_PASSWORD)),
					'date'         =>  date('Y-m-d'),
					'active'       => 1,
					'unique_id'    => $this->Users_modal->generate_unique_user_id(),

				];

				if(!empty($value[9])) {
					$xls_date = $value[9];
					$unix_date = ($xls_date - 25569) * 86400;
					$xls_date = 25569 + ($unix_date / 86400);
					$unix_date = ($xls_date - 25569) * 86400;
					$fileData['dob'] = date("Y-m-d", $unix_date);
				}
				
				if ($fileData['email']) {
					if ($userRoleData = $this->__getUserId($fileData, $rollID, $containerID)) {

						if ($containerID > 0) {
							$cont_users_roles_data[] =  $this->__getCollectContainerUserRoles($userRoleData['user_id'], $rollID, $containerID);
						}

						$users_roles_data[] = $this->__getCollectUserRoles($userRoleData['user_id'], $rollID);

						if ($courseID > 0 && $sectionID > 0) {
							$assign_data_array[] = $this->__getCollectCourseAssignUser($userRoleData['user_id'], $containerID, 1, $courseID, $sectionID, $rollID);
						}
					}
				}
			}
			$count++;
		}

		if ($cont_users_roles_data[0]) {
			$this->db->insert_batch('containers_users_roles', array_filter($cont_users_roles_data));
		}
		if ($users_roles_data[0]) {
			$this->db->insert_batch('users_roles', array_filter($users_roles_data));
		}
		if ($assign_data_array[0]) {
			$this->db->insert_batch('course_assigned_to_users', array_filter($assign_data_array));
		}
		echo json_encode(['code' => 200, 'message' => "User has been successfully imported."]);
		exit;
	}

	private function convert_dob($date) {
		return  date("Y-m-d", strtotime($date));
	}

	/**
	 * __getUploadUsersFromCSV method
	 * @description this function use to upload users from csv files
	 * @param file, int
	 * @return json array
	 */
	public function __getUploadUsersFromCSV($tempFile, $rollID, $containerID = 0, $courseID = 0, $sectionID = 0, $groupID = 0)
	{

		$count = 0;
		$fp = fopen($tempFile, 'r') or die("can't open file");

		if (($handle = fopen($tempFile, 'r')) !== FALSE) {
			while ($csv_line = fgetcsv($fp, 1024)) {
				for ($i = 1, $j = count($csv_line); $i < $j; $i++) {
					if ($csv_line[3] != '') {
						$insert_csv = [];
						$insert_csv = [];
						$insert_csv['first_name']				= (!empty($csv_line[0]) ? $csv_line[0] : null);
						$insert_csv['middle_name'] 				= (!empty($csv_line[1]) ? $csv_line[1] : null);
						$insert_csv['last_name'] 				= (!empty($csv_line[2]) ? $csv_line[2] : null);
						$insert_csv['email'] 					= (!empty($csv_line[3]) ? $csv_line[3] : null);
						$insert_csv['secondary_email'] 			= (!empty($csv_line[4]) ? $csv_line[4] : null);
						$insert_csv['institutional_unique_id'] 	= (!empty($csv_line[5]) ? $csv_line[5] : null);
						$insert_csv['phone'] 					= (!empty($csv_line[6]) ? $csv_line[6] : null);
						$insert_csv['cellphone'] 				= (!empty($csv_line[7]) ? $csv_line[7] : null);
						$insert_csv['password'] 				= (!empty($csv_line[8]) ? $csv_line[8] : $this->ion_auth->hash_password(DEFAULT_PASSWORD));
						
						if($csv_line[9]!='dob' || !empty($csv_line[9])) {
							$insert_csv['dob'] = $this->convert_dob($csv_line[9]);
						} else {
							$insert_csv['dob'] = date('Y-m-d');
						}
					}
				}
				
				
				$realPostdata = [
					'username'     => $insert_csv['email'],
					'first_name'   => $insert_csv['first_name'],
					'middle_name'  => $insert_csv['middle_name'],
					'last_name'    => $insert_csv['last_name'],
					'email'        => $insert_csv['email'],
					'secondary_email' => $insert_csv['secondary_email'],
					'institutional_unique_id' => $insert_csv['institutional_unique_id'],
					'phone'        => $insert_csv['phone'],
					'cellphone'    => $insert_csv['cellphone'],
					'password'     => ($this->ion_auth->hash_password($insert_csv['password'])) ? $this->ion_auth->hash_password($insert_csv['password']) : $this->ion_auth->hash_password(DEFAULT_PASSWORD) ,
					'date'         =>  date('Y-m-d'),
					'dob'         =>  $insert_csv['dob'],
					'active'       => 1,
					'unique_id'    => $this->Users_modal->generate_unique_user_id()
				];
				
				if ($count > 0 & $realPostdata['email'] != '') {

					if ($userRoleData = $this->__getUserId($realPostdata, $rollID, $containerID)) {
						if ($containerID > 0) {
							$cont_users_roles_data[] =  $this->__getCollectContainerUserRoles($userRoleData['user_id'], $rollID, $containerID);
						}

						$users_roles_data[] = $this->__getCollectUserRoles($userRoleData['user_id'], $rollID);

						if ($courseID > 0 && $sectionID > 0) {
							$assign_data_array[] = $this->__getCollectCourseAssignUser($userRoleData['user_id'], $containerID, 1, $courseID, $sectionID, $rollID);
						}

						if ($groupID > 0) {
							$group_array[] = $this->insertGroupAssignUser($userRoleData['user_id'], $containerID, $courseID, $sectionID, $groupID);
						}
					}
				}
				$count++;
			}//exit;
		}
		
		if ($cont_users_roles_data[0]) {
			$this->db->insert_batch('containers_users_roles', array_filter($cont_users_roles_data));
		}
		if ($users_roles_data[0]) {
			$this->db->insert_batch('users_roles', array_filter($users_roles_data));
		}

		if ($assign_data_array[0]) {
			$this->db->insert_batch('course_assigned_to_users', array_filter($assign_data_array));
		}
		
		if ($group_array[0]) {
			$this->db->insert_batch('container_groups_users', array_filter($group_array));
		}
		echo json_encode(['code' => 200, 'message' => "User has been successfully imported."]);
		exit;
	}


	/**
	 * __getCollectCourseAssignUser method
	 * @description this function use to collect user assigned ids
	 * @param int
	 * @return array
	 */
	private function __getCollectCourseAssignUser($userID, $containerID, $type, $courseID, $sectionID, $rollID)
	{
		$userSectionAssignData = $this->db->select('course_assigned_to_users.id')
			->from('course_assigned_to_users')
			->where('user_id', $userID)
			->where('course_id', $courseID)
			->where('container_id', $containerID)
			->where('section_id', $sectionID)
			->get()
			->row();
		
		if ($rollID == INSTRUCTOR) {
			$is_instructor = 1;
		} else {
			$is_instructor = 0;
		}
		if (empty($userSectionAssignData)) {
			$data = [
				'user_id' => $userID,
				'course_id' => $courseID,
				'container_id' => $containerID,
				'section_id' => $sectionID,
				'is_instructor'  => $is_instructor
			];
			return $data;
		} else {
			$conditions = [
				'user_id' => $userID,
				'course_id' => $courseID,
				'container_id' => $containerID,
				'section_id' => $sectionID
			];

			$this->common_model->UpdateDB('course_assigned_to_users', $conditions, ['is_instructor' => $is_instructor]);
			return false;
		}
	}

	/**
	 * __getCollectUserRoles method
	 * @description this function use to collect user rolls
	 * @param int
	 * @return array
	 */
	private function __getCollectUserRoles($usersID, $rollID)
	{
		$roleExistData = $this->db->select('users_roles.id')
			->from('users_roles')
			->where('user_id', $usersID)
			->where('role_id', $rollID)
			->get()
			->row();

		if (empty($roleExistData)) {
			$data = [
				'user_id' => $usersID,
				'role_id' => $rollID
			];
			return $data;
		}
	}

	/**
	 * __getCollectContainerUserRoles method
	 * @description this function use to collect container user rolls
	 * @param int
	 * @return array
	 */
	private function __getCollectContainerUserRoles($usersID, $rollID, $containerID)
	{
		$containerExistData = $this->db->select('containers_users_roles.id')
			->from('containers_users_roles')
			->where('user_id', $usersID)
			->where('role_id', $rollID)
			->where('container_id', $containerID)
			->get()
			->row();

		if (empty($containerExistData)) {
			$data = [
				'user_id' => $usersID,
				'role_id' => $rollID,
				'container_id' => $containerID
			];
			return $data;
		}
	}

	/**
	 * __getUserId method
	 * @description this function use to insert user data or get info if found
	 * @param string email
	 * @return array
	 */
	private function __getUserId($fileData = [], $rollID, $containerID = 0)
	{

		$existData = $this->db->select('users.id')
			->from('users')
			->where('email', $fileData['email'])
			->get()
			->row();

		if ($existData) {
			$userRoleData = [
				'user_id' => $existData->id,
				'role_id' => $rollID,
				'container_id' => $containerID
			];
		} else {
			$this->db->insert('users', $fileData);
			$userRoleData = [
				'user_id' 	   => $this->db->insert_id(),
				'role_id' 	   => $rollID,
				'container_id' => $containerID
			];
		}
		return $userRoleData;
	}


	/**
	 * get_user_enroll_user method
	 * @description this function use to get user section in section details page with users
	 * @return html
	 */
	public function get_user_enroll_user()
	{
		$this->load->model('Container_model');
		$template_path = "containers/enroll_user_section";
		$data['siteID'] = post('siteID');
		echo $this->load->view($template_path, $data, true);
		exit;
	}



	/**
	 * getUsers method
	 * @description this function use get enroll and institute user
	 * @param int
	 * @return html
	 */
	public function getUsers($page = 0)
	{

		$this->load->model('Container_model');
		$this->load->library('pagination');

		//if($this->input->post()){
		$siteID = post('siteID');
		$config['base_url'] = base_url() . 'containers/schoolsUsers/getUsers';
		$config['total_rows'] = $this->Container_model->getUsersByContainerID($siteID, true, '', '');
		$config['per_page'] 	= 15;
		$config['full_tag_open'] = '<ul class="pagination" >';
		$config['full_tag_close'] = '</ul>';
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['next_link'] = '&raquo;';




		$html = '<ul class="ms-list">';

		if ($this->input->post('utype') == 'enroll') {
			$this->pagination->initialize($config);
			$data['records'] = $this->Container_model->getUsersByContainerID($siteID, false, $config['per_page'], $page);
			foreach ($data['records'] as $key => $options) {
				$html .= '<li class="ms-elem-selection " title="' . $options->id . '"><span>' . $options->first_name . ' ' . $options->last_name . '<br />' . $options->email . '</span><a href="javascript:void(0)" onclick="SectionDetailsUsers.multiselect(' . $options->id . ',2)" class="btn btn-primary btn-xs"><i class="ti ti-trash"></i></a></li>';
			}
		} else if ($this->input->post('utype') == 'instructor') {
			$this->pagination->initialize($config);
			$data['records'] = $this->Container_model->getUsersByContainerID($siteID, false, $config['per_page'], $page);
			foreach ($data['records'] as $key => $options) {
				$html .= '<li class="ms-elem-selection " title="' . $options->id . '"><span>' . $options->first_name . ' ' . $options->last_name . '<br />' . $options->email . '</span><a href="javascript:void(0)" onclick="SectionDetailsUsers.multiselect(' . $options->id . ',4)" class="btn btn-primary btn-xs"><i class="ti ti-trash"></i></a></li>';
			}
		} else if ($this->input->post('utype') == 'all_for_instructor') {
			$config['total_rows'] = $this->Container_model->getActiveInstructor(true, '', '');
			$this->pagination->initialize($config);
			$data['records'] = $this->Container_model->getActiveInstructor(false, $config['per_page'], $page);
			foreach ($data['records'] as $key => $options) {
				$html .= '<li class="ms-elem-selectable " title="' . $options->id . '"><span>' . $options->first_name . ' ' . $options->last_name . '<br />' . $options->email . '</span><a href="javascript:void(0)" onclick="SectionDetailsUsers.multiselect(' . $options->id . ',3)" class="btn btn-primary btn-xs"><i class="ti ti-arrow-right f10"></i></a></li>';
			}
		} else {
			$this->pagination->initialize($config);
			$data['records'] = $this->Container_model->getUsersByContainerID($siteID, false, $config['per_page'], $page);
			foreach ($data['records'] as $key => $options) {
				$html .= '<li class="ms-elem-selectable " title="' . $options->id . '"><span>' . $options->first_name . ' ' . $options->last_name . '<br />' . $options->email . '</span><a href="javascript:void(0)" onclick="SectionDetailsUsers.multiselect(' . $options->id . ',1)" class="btn btn-primary btn-xs"><i class="ti ti-arrow-right f10"></i></a></li>';
			}
		}

		$html .= '</ul>';
		$html .= $this->pagination->create_links();
		echo $html;
		exit;

		//	}
	}

	/**
	 * get_manage_users method
	 * @description this function use to enroll/unroll/make instructor 
	 * @param int
	 * @return json array
	 */
	public function get_manage_users()
	{
		//type 1->enroll, 2->unroll,3->make instructor,4->remove instructor
		/*$this->session->unset_userdata('siteID');
		$this->session->unset_userdata('sectionID');
		$this->session->unset_userdata('courseID');*/
		if ($this->input->post()) {
			$siteID = post('siteID');
			$response = [];
			switch ($this->input->post('type')) {
				case '1':
					$condition = [
						'user_id' => $this->input->post('userID'),
						'section_id' => $this->session->userdata('sectionID')
					];
					if ($this->common_model->getCountRecord('course_assigned_to_users', $condition) == 0) {
						$response = $this->__getEnrollStudent($this->input->post('userID'), $siteID, $this->session->userdata('courseID'), $this->session->userdata('sectionID'));
					} else {
						$response = [
							'code' => 400,
							'message' => 'This user is already enrolled'
						];
					}
					break;

				case '2':
					$condition = [
						'user_id' => $this->input->post('userID'),
						'section_id' => $this->session->userdata('sectionID')
					];
					if ($this->common_model->DeleteDB('course_assigned_to_users', $condition)) {
						$response = [
							'code' => 200,
							'message' => 'You have been successfully un-enroll'
						];
					} else {
						$response = [
							'code' => 400,
							'message' => 'This user is already instructor'
						];
					}
					break;

				case '3':
					$condition = [
						'user_id' => $this->input->post('userID'),
						'section_id' => $this->session->userdata('sectionID'),
						'is_instructor' => 1
					];
					if ($this->common_model->getCountRecord('course_assigned_to_users', $condition) == 0) {
						$response = $this->__getMakeInstructor($this->input->post('userID'), $this->session->userdata('sectionID'), $this->session->userdata('courseID'), $siteID);
					} else {
						$response = [
							'code' => 400,
							'message' => 'This user is already instructor'
						];
					}
					break;

				case '4':
					$response = $this->__getRemoveInstructor($this->input->post('userID'), $this->session->userdata('sectionID'), $siteID);
					break;
			}

			echo json_encode($response);
			exit;
		}
		exit;
	}

	/**
	 * __getMakeInstructor method
	 * @description this is private function use to create user as instructor 
	 * @param int
	 * @return json array
	 */
	private function __getMakeInstructor($userID, $sectionID, $courseID, $containerID)
	{
		$saveData = [
			'user_id'   	  => $userID,
			'course_id'   	  => $courseID,
			'container_id'    => $containerID,
			'container_type'  => 1,
			'section_id'   	  => $sectionID,
			'is_instructor'   => 1
		];
		if ($this->common_model->InsertData('course_assigned_to_users', $saveData)) {
			$response = [
				'code' => 200,
				'message' => 'Instructor successfully added'
			];
		} else {
			$response = [
				'code' => 400,
				'message' => "Something went wrong, please try again!"
			];
		}
		return $response;
	}

	/**
	 * __getRemoveInstructor method
	 * @description this is private function use to remove user as instructor 
	 * @param int
	 * @return json array
	 */
	private function __getRemoveInstructor($userID, $sectionID, $containerID)
	{
		$conditions = [
			'user_id'   	  => $userID,
			'section_id'   	  => $sectionID,
			'container_id' => $containerID
		];

		if ($this->common_model->DeleteDB('course_assigned_to_users', $conditions)) {

			$roleDataConditions = [
				'user_id' => $userID,
				'role_id' => INSTRUCTOR,
				'container_id' => $containerID
			];

			$response = [
				'code' => 200,
				'message' => 'Successfully removed instructor'
			];
			/*if($this->common_model->DeleteDB('containers_users_roles',$roleDataConditions))
			{
				$response = [
					'code' => 200,
					'message' => 'Your have been make successfully remove instructor'
				];
			}	*/
		} else {
			$response = [
				'code' => 400,
				'message' => "Something went wrong, please try again!"
			];
		}
		return $response;
	}


	/**
	 * __getEnrollStudent method
	 * @description this is private function use to add user in course
	 * @param int
	 * @return json array
	 */
	private function __getEnrollStudent($userID, $siteID, $courseID, $sectionID)
	{
		$saveData = [
			'user_id'   	  => $userID,
			'course_id'   	  => $courseID,
			'container_id'    => $siteID,
			'container_type'  => 1,
			'section_id'   	  => $sectionID
		];

		if ($this->common_model->InsertData('course_assigned_to_users', $saveData)) {
			// 1 = for course, 1 = Enrolls in Course (notification_triggers)
			$this->sendStudentNotification($userID, $courseID, $siteID, 1, 1);
			$response = [
				'code' => 200,
				'message' => 'Your have been successfully enrolled'
			];
		} else {
			$response = [
				'code' => 400,
				'message' => "Something went wrong, please try again!"
			];
		}
		return $response;
	}

	private function sendStudentNotification($std_id, $course_id, $inst_id, $type, $trigger_type) {
		//ini_set('display_errors', 1);
		# Student data
		$studentData = $this->common_model->getAllData('users', 'first_name, last_name, email, secondary_email', true, [ 'id' => $std_id ]);

		# Course data 
		$courseData = $this->common_model->getAllData('courses', '*', true, [ 'id' => $course_id ]);

		# institution_contacts data 
		$instituteContactData = $this->common_model->getAllData('institution_contacts', '*', true, [ 'institution_id' => $inst_id, 'type' => 'primary' ]);

		# Notification data
		$notificationData = $this->common_model->getAllData('notifications_auto', '*', true, [ 'course_id' => $course_id, 'type' => $type, 'trigger_type' => $trigger_type, 'institution_id' => $inst_id ]);
		switch ($type) {
			case '1':
				# Course
				# Send mail basic variables 
				$template_data->subject 	= $notificationData->title;			// Subject
				$template_data->message 	= $notificationData->body;			// Message body
				$template_data->from_email 	= $instituteContactData->email;		// from email
				$template_data->from_name 	= $instituteContactData->name;		// from name
				$data['variables'] 			= [ 'student_name' => ucfirst($studentData->first_name) ];
				$data['email'] 				= $studentData->email;
				$data['template_data'] 		= $template_data;
				break;
			case '2':
				
				break;
			case '3':
				break;
		}
		sendgrid_email($data);
	}


	/* =============== Notification =================== */

	

	/* =============== Notification end =================== */

	/**
	 * getlist method
	 * @description this function use to get user list
	 * @return json array
	 */
	public function getlist()
	{
		$_POST['container_selected'] = post('siteID');
		$_POST['course_selected'] = $this->session->userdata('courseID');
		$_POST['section_selected'] = $this->session->userdata('sectionID');
		$enrolledUser = $this->common_model->getRows($_POST);

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $enrolledUser['total'],
			"recordsFiltered" => $enrolledUser['total'],
			"data" => $enrolledUser['result'],
		);
		echo json_encode($output);
		exit;
	}

	/**
	 * get_unenroll_users method
	 * @description this function use to remove user from course 
	 * @param int
	 * @return json array
	 */
	public function get_unenroll_users()
	{
		if ($this->input->post()) {
			$where = [
				'section_id' => $this->session->userdata('sectionID')
			];

			if ($this->common_model->MultiDeleteDB('course_assigned_to_users', $where, $this->input->post('users'))) {
				$response = [
					'code' => 200,
					'message' => 'Your have been successfully enrolled'
				];
			} else {
				$response = [
					'code' => 400,
					'message' => "Something went wrong, please try again!"
				];
			}
			echo json_encode($response);
			exit;
		}
		echo json_encode([
			'code' => 400,
			'message' => "Something went wrong, please try again!"
		]);
		exit;
	}

	/**
	 * get_unenroll_users_from_group method
	 * @description this function use to remove user from group 
	 * @param int
	 * @return json array
	 */
	public function get_unenroll_users_from_group()
	{
		if ($this->input->post()) {
			$where = [
				'container_group_id' => post('groupID')
			];

			if ($this->common_model->MultiDeleteDB('container_groups_users', $where, $this->input->post('recordIDS'))) {
				$response = [
					'code' => 200,
					'message' => 'Your have been successfully enrolled'
				];
			} else {
				$response = [
					'code' => 400,
					'message' => "Something went wrong, please try again!"
				];
			}
			echo json_encode($response);
			exit;
		}
		echo json_encode([
			'code' => 400,
			'message' => "Something went wrong, please try again!"
		]);
		exit;
	}

	/**
	 * get_count_enroll_users method
	 * @description this function use to count enroll user from corse section
	 * @return json array
	 */
	public function get_count_enroll_users()
	{
		$conditions = [
			'section_id' => $this->session->userdata('sectionID'),
			'container_id' => post('siteID'),
		];
		$count = $this->common_model->getCountRecord('course_assigned_to_users', $conditions);
		echo json_encode(['code' => 200, 'count' => $count]);
		exit;
	}

	/**
	 * get_user_by_email method
	 * @description this function use to get user details by email
	 * @param string
	 * @return json array
	 */
	public function createUser()
	{
		if ($this->input->post()) {
			$this->load->model('users/Users_modal');
			$postData = [
				'username'     => $this->input->post('email'),
				'first_name'   => $this->input->post('first_name'),
				'middle_name'  => $this->input->post('middle_name'),
				'last_name'    => $this->input->post('last_name'),
				'email'        => $this->input->post('email'),
				'phone'        => $this->input->post('phone'),
				'password'     => $this->ion_auth->hash_password(DEFAULT_PASSWORD),
				'date'         => date('Y-m-d'),
				'active'       => 1,
				'unique_id'    => $this->Users_modal->generate_unique_user_id()
			];
			$rollID = $this->input->post('roles');
			if ($userRoleData = $this->__getUserId($postData, $rollID, post('siteID'))) {
				if ($this->common_model->getCountRecord('containers_users_roles', $userRoleData) == 0) {
					$this->common_model->InsertData('containers_users_roles', $userRoleData);
				}
				unset($userRoleData['container_id']);
				if ($this->common_model->getCountRecord('users_roles', $userRoleData) == 0) {
					$this->common_model->InsertData('users_roles', $userRoleData);
				}
				$response = [
					'code' => 200,
					'message' => 'You have been successfully added user'
				];
			} else {
				$response = [
					'code' => 400,
					'message' => 'Something went wrong, please try again!'
				];
			}

			echo json_encode($response);
			exit;
		}
		exit;
	}

	/**
	 * get_container_user_list method
	 * @description this function use to get user list by container id
	 * @param int
	 * @return json array
	 */
	public function get_container_user_list()
	{
		$this->load->model('users/Users_modal');
		$this->Users_modal->set_user_container_id(post('siteID'));
		//	echo "fdsfds";
		$_POST['container_selected'] = post('siteID');
		$userList = $this->Users_modal->getRowsInstituteUsers($_POST);
		
		/*$i = $_POST['start'];
        $data = [];
        foreach($userList['result'] as $user){                 
            $i++;
            $data[] = array(
                    "user_id"           => $user->id, 
                    "user_name"         => $user->user_name, 
                    "email"             => $user->email,
                    "roles"             => '<span class="btn btn-info btn-status btn-sm mt-1 mb-1">'.str_replace('|', '</span><span class="btn btn-info btn-status btn-sm mt-1 mb-1">', $user->roles)."</span>"
                );
        }
*/
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $userList['total'],
			"recordsFiltered" => $userList['total'],
			"data" => $userList['result'],
		);
		// Output to JSON format
		echo json_encode($output);
		exit;
	}


	public function remove_users()
	{

		if ($this->input->post()) {

			if ($this->input->post('users') == '') {
				echo json_encode([
					'code' => 400,
					'message' => "Please select at least one user"
				]);
				exit;
			}

			$where = [
				'container_id' => post('siteID')
			];
			if ($this->common_model->MultiDeleteDB('containers_users_roles', $where, $this->input->post('users'))) {
				if ($this->common_model->MultiDeleteDB('course_assigned_to_users', $where, $this->input->post('users'))) {
					$response = [
						'code' => 200,
						'message' => 'Your have been successfully enrolled'
					];
				}
				$response = [
					'code' => 200,
					'message' => 'Your have been successfully enrolled'
				];
			} else {
				$response = [
					'code' => 400,
					'message' => "Something went wrong, please try again!"
				];
			}
			echo json_encode($response);
			exit;
		}
		echo json_encode([
			'code' => 400,
			'message' => "Something went wrong, please try again!"
		]);
		exit;
	}


	/**
	 * get_site_active_userd method
	 * @description this function use to get all users(only active)
	 * @param int
	 * @return json array
	 */
	public function get_site_active_users()
	{
		if ($this->input->post()) {
			$siteID = $this->input->post('siteID');
			$keyword = $this->input->post('keyword')['term'];
			$this->load->model('Users_model');
			$data = $this->Users_model->getUsers(['containers_users_roles.container_id' => $siteID, 'containers_users_roles.role_id' => STUDENT], $keyword);
			foreach ($data as $key => $value) {
				$result[] = [
					'id' => $value->id,
					'label' => $value->email,
					'name' => $value->user_name
				];
			}
			echo json_encode($result);
			exit;
		}
		exit;
	}


	/**
	 * get_site_active_users_by_name method
	 * @description this function use to get all users(only active)
	 * @param int
	 * @return json array
	 */
	public function get_site_active_users_by_name()
	{
		if ($this->input->post()) {
			$siteID = $this->input->post('siteID');
			$keyword = $this->input->post('keyword')['term'];
			$this->load->model('Users_model');
			$data = $this->Users_model->getUsersName(['containers_users_roles.container_id' => $siteID, 'containers_users_roles.role_id' => STUDENT], $keyword);
			foreach ($data as $key => $value) {
				$result[] = [
					'id' => $value->id,
					'email' => $value->email,
					'label' => $value->user_name
				];
			}
			echo json_encode($result);
			exit;
		}
		exit;
	}

	/**
	 * getgroupuserslist method
	 * @description this function use to get user list
	 * @return json array
	 */
	public function getgroupuserslist()
	{
		if ($this->input->post()) {
			$this->load->model('Users_model');
			$enrolledUser = $this->Users_model->getGroupUsers($_POST);
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $enrolledUser['total'],
				"recordsFiltered" => $enrolledUser['total'],
				"data" => $enrolledUser['result'],
			);
			echo json_encode($output);
			exit;
		}
		exit;
	}

	/**
	 * __getCollectCourseAssignUser method
	 * @description this function use to collect user assigned ids
	 * @param int
	 * @return array
	 */
	private function insertGroupAssignUser($userID, $containerID, $courseID, $sectionID, $groupID)
	{
		$course_assigned_to_users = $this->db->select('course_assigned_to_users.id')
			->from('container_groups_users')
			->where('user_id', $userID)
			->where('course_id', $courseID)
			->where('container_id', $containerID)
			->where('container_group_id', $groupID)
			->where('section_id', $sectionID)
			->get()
			->row();
		
		if (empty($course_assigned_to_users)) {
			$data = [
				'user_id' => $userID,
				'course_id' => $courseID,
				'container_id' => $containerID,
				'section_id' => $sectionID,
				'container_group_id' => $groupID,
			];
			return $data;
		} else {
			$conditions = [
				'user_id' => $userID,
				'course_id' => $courseID,
				'container_id' => $containerID,
				'section_id' => $sectionID,
				'container_group_id' => $groupID,
			];

			$this->common_model->UpdateDB('course_assigned_to_users', $conditions);
			return false;
		}
	}

	public function get_enroll_courses($userID){
        $data['typeid'] = $userID;
        list($userID, $type) = explode("_", $userID);
        $this->load->model('Users_model');
        $data['value'] = $this->Users_model->get_enroll_courses($userID);
        echo json_encode($data); 
        exit;
    }

    public function get_enroll_section($userID){
        $data['typeid'] = $userID;
        list($userID, $type) = explode("_", $userID);
        $this->load->model('Users_model');
        $data['value'] = $this->Users_model->get_enroll_section($userID);
        echo json_encode($data); 
        exit;
    }
}
