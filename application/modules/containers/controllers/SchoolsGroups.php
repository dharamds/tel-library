<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SchoolsGroups extends MY_Controller 
{
	public function __construct()
	{

		parent::__construct();
		//Do your magic here
		error_reporting(E_ALL ^ E_DEPRECATED);
		ini_set('display_errors', 1);

		$this->load->module('template'); 
	    $this->load->model('common_model');  
	    $this->load->library('form_validation');

		if (!$this->ion_auth->logged_in()):
		    redirect('users/auth', 'refresh');
		endif;

		/*if (!$this->ion_auth->is_admin()): 
	      return show_error("You Must Be An Administrator To View This Page");
	    endif; */
	}

	/**
     * saveGroups method
     * @description this function use to save group
     * @param form data
     * @return json array
     */	
	public function saveGroups()
	{
		if($this->input->post())
		{
			$saveData = [
                'created_by' => $this->session->userdata('user_id'),
                'container_id' => post('siteID'),
                'course_id' => post('course_id'),
                'section_id' => post('group_section_id'),
                'name' => post('group_name'),
                'display_avaibility' => post('group_avaibility'),
                'created' => date("Y-m-d H:i:s")
            ];
            if(post('group_id'))
            {
            	if ($this->common_model->UpdateDB('container_groups', ['id' => post('group_id')], $saveData)) {
            		$updateData = [
            			'course_id' => post('course_id'),
                		'section_id' => post('group_section_id')
            		];
            		$this->common_model->UpdateDB('container_groups_users', ['container_group_id' => post('group_id'),'container_id' => post('siteID')], $updateData);

                    echo json_encode(['code' => 200, 'message' => "Group have been created successfully",'saveType'=>post('submit_type'),'group_id'=>post('group_id')]);
                    exit;
                }else{
                	echo json_encode(['code' => 400, 'message' => "Something went wrong, please try again!"]);
            		exit;
                }
            }else{
            	if ($this->common_model->InsertData('container_groups', $saveData)) 
	            {
	            	echo json_encode(['code' => 200, 'message' => "Group have been created successfully",'saveType'=>post('submit_type'),'group_id'=>$this->db->insert_id()]);
	                exit;
	            }else{
	            	echo json_encode(['code' => 400, 'message' => "Something went wrong, please try again!"]);
            		exit;
	            }	
            }
				
		}
		exit;
	}
	/**
     * getCheckGroupName method
     * @description this function use to check group name is already exist or not
     * @param int
     * @return json array
     */	
	public function getCheckGroupName()
	{
		if($this->input->post())
		{
			if( post('course_id') == '' || post('group_section_id') == '' )
			{
				echo  'false';
				exit;
			}
			$conditions = [
				'course_id' => post('course_id'),
				'section_id' => post('group_section_id'),
				'container_id' => post('siteID'),
				'name' => post('group_name')
			];
			if( post('group_id'))
			{
				$conditions['id !='] =  post('group_id');
			}	
			
			if($this->common_model->getCountRecord('container_groups',$conditions) == 0)
			{
				echo  'true';
				exit;
			}	
				echo  'false';
				exit;
		}
		exit;
	}

	/**
     * getCheckGroupName method
     * @description this function use to check group name is already exist or not
     * @param int
     * @return json array
     */	
	public function getCheckGroupNameForGroupSet()
	{
		if($this->input->post())
		{
			if( post('course_id') == '' || post('group_section_id') == '' )
			{
				echo  'false';
				exit;
			}
			$conditions = [
				'course_id' => post('course_id'),
				'section_id' => post('group_section_id'),
				'container_id' => post('siteID'),
				'name' => post('group_name')[0]
			];
			if( post('group_id'))
			{
				$conditions['id !='] =  post('group_id');
			}	
			
			if($this->common_model->getCountRecord('container_groups',$conditions) == 0)
			{
				echo  'true';
				exit;
			}	
				echo  'false';
				exit;
		}
		exit;
	}


	/**
     * get_group_user_enroll_section method
     * @description this function use to display the group user enroll section
     * @param int
     * @return html
     */	
	public function get_group_user_enroll_section(){
		if($this->input->post())
		{	
			$template_path = "containers/groups/get_group_user_enroll_section";
			$data['siteID'] = post('siteID');
			$data['courseID'] = post('courseID');
			$data['sectionID'] = post('sectionID');
			$data['groupID']   = post('groupID');
	        echo $this->load->view($template_path, $data, true);
	        exit;
		}
		exit;	
	}

	/**
     * get_group_user_enroll_listing method
     * @description this function use to display the group user enroll section
     * @param int
     * @return html
     */	
	public function get_group_user_enroll_listing(){
		if($this->input->post())
		{	
			$template_path = "containers/groups/get_group_user_enroll_listing";
			$data['siteID'] = post('siteID');
			$data['courseID'] = post('courseID');
			$data['sectionID'] = post('sectionID');
			$data['groupID']   = post('groupID');
	        echo $this->load->view($template_path, $data, true);
	        exit;
		}
		exit;	
	}


	/**
     * get_enroll_users_in_group method
     * @description this function use to enroll/unroll/make instructor 
     * @param int
     * @return json array
     */	
	public function get_enroll_users_in_group()
	{	
		
		if($this->input->post()) 
		{	
			$response = [];
			//1=>make enroll and group leader, 0=>remove leader, 2=>enroll in group

			$postData = [
				'user_id' => post('userID'),
				'section_id' => post('sectionID'),
				'container_id' => post('siteID'),
				'course_id' => post('courseID'),
				'container_group_id' => post('groupID'),
				'is_leader' => post('actionType')
			];

			
			switch(post('actionType'))
			{
				case '0':
					$response = $this->__removeMakeLeader($postData);
				break;

				case '1':
					unset($postData['is_leader']);
					$response = $this->__getEnrollUser($postData,'leader');
				break;

				case '2':
					unset($postData['is_leader']);
					$response = $this->__getEnrollUser($postData,'student');
				break;

				case '3':
					unset($postData['is_leader']);
					$response = $this->__removeUser($postData);
				break;
			}
			
			echo json_encode($response);
			exit;

		}
		exit;
	}


	private function __removeUser($postData = [])
	{
		if($this->common_model->DeleteDB('container_groups_users',$postData))
		{
			$response = [
				'code' => 200,
				'message' => 'You have been successfully removed user'
			];
		}else{
			$response = [
				'code' => 400,
				'message' => 'Something went wrong, please try again'
			];
		}

		return $response;
	}

	private function __removeMakeLeader($postData = [])
	{	
		unset($postData['is_leader']);
		if ($this->common_model->UpdateDB('container_groups_users', $postData, ['is_leader'=>0])) {
			$response = [
				'code' => 200,
				'message' => 'You has been removed group leader'
			];
		}else{
			$response = [
				'code' => 400,
				'message' => 'Something went wrong, please try again'
			];
		}
		return $response;
	}

	private function __getEnrollUser($postData = [],$type)
	{
		
		switch($type){
			case 'leader':
				if($this->__getCheckUserCount($postData) == 0)
				{	
					$postData['is_leader'] = 1;
					if($this->__getEnrollStudent($postData))
					{
						$response = [
							'code' => 200,
							'message' => 'This user have been group leader'
						];
					}else{
						$response = [
							'code' => 400,
							'message' => 'Something went wrong, please try again'
						];
					}
				}else{
					$postData['is_leader'] = 1;
					if($this->__getCheckUserCount($postData) == 0)
					{	
						unset($postData['is_leader']);
						if($this->common_model->UpdateDB('container_groups_users', $postData, ['is_leader'=>1]))
						{
							$response = [
								'code' => 200,
								'message' => 'This user have been group leader'
							];
						}	
					}else{
						$response = [
							'code' => 400,
							'message' => 'This user is already group leader'
						];
					}
				}
			break;

			case 'student':
				if($this->__getCheckUserCount($postData) == 0)
				{
					if($this->__getEnrollStudent($postData))
					{
						$response = [
							'code' => 200,
							'message' => 'This user have been enrolled'
						];
					}else{
						$response = [
							'code' => 400,
							'message' => 'Something went wrong, please try again'
						];
					}
				}else{
					$response = [
						'code' => 400,
						'message' => 'This user is already enrolled'
					];	
				}	
			break;
		}

		return $response;
	}


	private function __getCheckUserCount($postData = [])
	{
		return $this->common_model->getCountRecord('container_groups_users',$postData);
	}

	private function __getEnrollStudent($postData = [])
	{
		if($this->common_model->InsertData('container_groups_users',$postData))
		{
			return true;
		}
			return false;	
	}

	public function getSiteUsers($page = 0)
	{
		$this->load->model('Container_model');
		$this->load->library('pagination');
		if($this->input->post()){
			$siteID = post('siteID');
			$config['base_url'] = base_url().'containers/SchoolsGroups/getSiteUsers';
			$config['total_rows'] = $this->Container_model->getContainerUsersForGroup($this->input->post(),true, '', '');
			$config['per_page'] 	= 15;
			$config['full_tag_open'] = '<ul class="pagination" >';
			$config['full_tag_close'] = '</ul>';
			$config['prev_link'] = '&laquo;';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['next_link'] = '&raquo;';
			$config['first_link'] = false;
			$config['last_link'] = false;

			$this->pagination->initialize($config);
			$data['records'] = $this->Container_model->getContainerUsersForGroup($this->input->post(), false, $config['per_page'], $page);

			$html = '<ul class="ms-list">';
			if($this->input->post('utype') == 'group_leader'){
				foreach ($data['records'] as $key => $options) {
					$html .= '<li class="ms-elem-selection " title="'.$options->id.'"><span>'.$options->first_name.' '.$options->last_name.'<br />'.$options->email.'</span><a href="javascript:void(0)" onclick="SchoolGroupsUserEnrollSection.getMakeEnrollUserInGroup('.$options->id.',this)" class="btn btn-danger btn-xs"><i class="ti ti-trash"></i></a></li>';
				}
			}
			else if($this->input->post('utype') == 'student'){
				foreach ($data['records'] as $key => $options) {
					$html .= '<li class="ms-elem-selection " title="'.$options->id.'"><span>'.$options->first_name.' '.$options->last_name.'<br />'.$options->email.'</span><a href="javascript:void(0)" onclick="SchoolGroupsUserEnrollSection.getMakeEnrollUserInGroup('.$options->id.',this)" class="btn btn-danger btn-xs"><i class="ti ti-trash"></i></a></li>';
				}
			}
			else
			{
				foreach ($data['records'] as $key => $options) {
					$html .= '<li class="ms-elem-selectable " title="'.$options->id.'"><span>'.$options->first_name.' '.$options->last_name.'<br />'.$options->email.'</span><a href="javascript:void(0)" onclick="SchoolGroupsUserEnrollSection.getMakeEnrollUserInGroup('.$options->id.',this)" class="btn btn-primary btn-xs"><i class="ti ti-arrows-horizontal"></i></a></li>';	
				}
			}
			
			$html .= '</ul>';
			$html .= $this->pagination->create_links();
			echo $html;
			exit;
		}
		exit;	
	}


	public function getLists() {
        // Fetch records
       	$this->load->model('Groups_model');  
        $data = $this->Groups_model->getGroupRows($_POST);
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Groups_model->count_group_container(),
            "recordsFiltered" => $this->Groups_model->countGroupFiltered($_POST),
            "data" => $data,
        );
       
        // Output to JSON format
        echo json_encode($output);
    }


    /**
     * get_make_action_request method
     * @description this method is use to make action as per request(like status change)
     * @param int
     * @return json array
     */
    public function get_make_action_request(){
        if($this->input->post()){
            $updateData = [];
            switch($this->input->post('actionType')){
                case 'status':
                    $updateData = [
                        'status' => post('status')
                    ];
                    $message = "You has been changed status successfully";
                break;

                case 'delete':
                    $updateData = [
                        'delete_status' => post('status')
                    ];
                     $message = "You has been removed record successfully";
                break;
            }    
            if ($this->common_model->UpdateDB('container_groups', ['id' => post('id')], $updateData)) {
                echo json_encode(['code' => 200, 'message' => $message]);
                exit;
            }  
                echo json_encode(['code' => 400, 'message' => "Something went wrong, please try again!"]);
                exit;  
        }
        exit;
    }

     public function get_remove_site()
    {
        if($this->input->post())
        {
            $postArray = [];
            foreach ($this->input->post('selected_record') as $key => $value) {
               $postArray[] = [
                    'id'            => $value,
                    'delete_status' => 0
               ];
            }
            if($this->common_model->multiUpdateRecords('container_groups',$postArray,'id')){
                echo json_encode(['code' => 200, 'message' => "You have been removed record successfully"]);
                exit;
            }
            echo json_encode(['code' => 400, 'message' => "Something went wrong, please try again!"]);
            exit; 
        }    
        exit;
    }

    /**
     * get_groups_leaders_by_id method
     * @description this function use to get group leader data by section id and course id
     * @param int
     * @return json array
     */

    public function get_groups_leaders_by_id()
    {
		if($this->input->post())
		{	
			if(post('course_id') == 0 || post('section_id') == 0)
			{
				echo json_encode(['code'=>202]);
				exit;
			}
			$this->load->model('Users_model');	
			if($result = $this->Users_model->getLeaderByCourseSection($this->input->post()))
			{
				echo json_encode(['code'=>200,'data'=>$result]);
				exit;
			}
				echo json_encode(['code'=>400]);
				exit;
		}
		exit;
	} 


	/**
     * getUploadUserView method
     * @description this function use to get view of upload user section
     * @return html
     */	
	public function getGroupSetView($siteID = '',$callBackRequest = '')
	{	
		
		$template_path = "containers/groups/group_set_form";
		
		if($callBackRequest){
			$data['callBackRequest'] = $callBackRequest;
		}else{
			$data['callBackRequest'] = '';
		}
		$this->load->model('Groups_model');
		$data['userList'] = $this->Groups_model->get_users_from_site(post('siteID'));
		echo $this->load->view($template_path,$data,true);
		exit;	
	}

	/**
     * saveGroupSet method
     * @description this function use to save setup group data
     * @param int, string
     * @return json array
     */	
	public function saveGroupSet()
	{
		if($this->input->post())
		{	
			$studentGroupData = [];
			$groupData = [];
			$groups_name = $this->input->post('groups_name');
			foreach ($this->input->post('group') as $key => $groupdatavalue) 
			{
				$groupData = [
					'container_id' => $this->input->post('siteID'),
					'course_id' => $this->input->post('course_id'),
					'section_id' => $this->input->post('group_section_id'),
					'name' => $groups_name[$key],
					'display_avaibility' => $this->input->post('group_avaibility'),
					'created_by' => $this->session->userdata('user_id'),
					'created' => date("Y-m-d H:i:s")
				];

				if ($this->common_model->InsertData('container_groups', $groupData)) 
	            {	
	            	$lastInsertID = $this->db->insert_id();	            	
	            	foreach ($groupdatavalue as $key => $value) {
	            		if($value['id'])
		            	{
		            		$studentGroupData[] = [
		            			'container_group_id' => $lastInsertID,
			            		'container_id' => $this->input->post('siteID'),
								'course_id' => $this->input->post('course_id'),
								'section_id' => $this->input->post('group_section_id'),
								'user_id' => $value['id']
			            	];
		            	}
	            	}
	            }
				
	           
			}
			
			if($studentGroupData)
			{
				if($this->db->insert_batch('container_groups_users', $this->__multi_unique($studentGroupData)))
				{
					echo json_encode(['code' => 200, 'message' => "Groups have been created successfully"]);
                	exit;
				}	
			}else{
				echo json_encode(['code' => 400, 'message' => "Something went wrong, please try again!"]);
            	exit; 
			}
		}	
		exit;
	}

	private function __multi_unique($src)
	{
		$output = array_map("unserialize",
			array_unique(array_map("serialize", $src)));
		return $output;
	}


	public function get_groups_by_course_section()
	{	
		if($this->input->post())
		{	
			$this->load->model('Groups_model'); 
			echo json_encode($this->Groups_model->get_groups_by_course_section($this->input->post('container_id'), $this->input->post('course_id'),$this->input->post('section_id')));
		}


	}


	

}	




