<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Notifications extends MY_Controller
{

    public function __construct()
    {

        parent::__construct();
        //Do your magic here
        error_reporting(E_ALL ^ E_DEPRECATED);
        ini_set('display_errors', 1);

        $this->load->module('template');
        $this->load->model('common_model');
        $this->load->library('form_validation');

        if (!$this->ion_auth->logged_in()) :
            redirect('users/auth', 'refresh');
        endif;
        $this->container_id = get_container_id();
        /*if (!$this->ion_auth->is_admin()) :
            return show_error("You Must Be An Administrator To View This Page");
        endif;*/
    }

    public function send_notification()
    {
        if($this->input->post())
        {   
            $groupIDs = explode(',', $this->input->post('groupIds'));
            $this->load->model('Users_model');
            $userData = $this->Users_model->getUsersByGroupID($groupIDs,$this->input->post('siteID'));
            foreach ($userData as $key => $value) {
               $email_data['template_code'] = 'GROUP_NOTIFICATION';
               $email_data['from_email'] = $this->config->item('admin_email', 'ion_auth');
               $email_data['email'] = $value['email'];
               $email_data['variables']['user_name'] = $value['user_name'];
               $email_data['variables']['body'] = post('msg');

                $notification = array(
                    "sender_id" => $this->ion_auth->user()->row()->id,
                    "receiver_id" => $value['id'],
                    "status" => 1,
                    "type" => '0',
                    "subject" => post('subject'),
                    "body" => post('msg'),
                    "priority" => 1,
                    "is_read" => 0,
                    "created" => date('Y-m-d H:i:s')
                );
                if ($value) {
                    sendgrid_email($email_data);
                    $this->load->model('common_model');
                    $this->common_model->InsertData('notifications', $notification);
                }
            }
            echo json_encode(['code' => 200, 'message' => "Notification has been sent successfully."]);
            exit;
        }
        echo json_encode(['code' => 400, 'message' => "Something went wrong, please try again!"]);
        exit;
    }
    
}
