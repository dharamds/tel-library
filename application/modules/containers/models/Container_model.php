<?php
/**
 *
 */
class Container_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
		//$this->load->model('common_model');
		// Set table name
		$this->table = 'containers';
		// Set orderable column fields
		$this->column_order = array(null,null,'containers.status', 'containers.name', 'institution_contacts.name', 'total_course','total_section','total_user','total_enroll_user');
		// Set searchable column fields
		$this->column_search = array('containers.name');
		// Set default order
		$this->order = array('containers.id' => 'desc');
	}


	public function getEnrollUsersBySection($type)
	{	
//pr($this->session->userdata('sectionID')); exit;
		$this->db->select('course_assigned_to_users.user_id');
		$this->db->from('course_assigned_to_users');
		$this->db->where(['course_assigned_to_users.section_id' => $this->session->userdata('sectionID'), 'course_assigned_to_users.course_id' => $this->session->userdata('courseID')]);
		$return = [];
		if($type == 'instructor')
		{
			$this->db->where(['course_assigned_to_users.is_instructor' => 1]);
		}else{
			$this->db->where(['course_assigned_to_users.is_instructor' => 0]);
		}	
		$query = $this->db->get();
		foreach ($query->result_array() as $key => $value) {
			$return[] = $value['user_id'];
		}
		return $return;
	}

	public function getEnrollUsersInSection()
	{	

		$this->db->select('course_assigned_to_users.user_id');
		$this->db->from('course_assigned_to_users');
		$this->db->where(['course_assigned_to_users.section_id' => $this->session->userdata('sectionID')]);
		$return = [];
		$query = $this->db->get();
		foreach ($query->result_array() as $key => $value) {
			$return[] = $value['user_id'];
		}
		return $return;
	}


	/**
	 * getAllActiveCourseBySiteID method
	 * @description this method is use to get active courses by site ID
	 * @param int
	 * @return array
	 */
	public function getAllActiveCourseBySiteID($siteID)
	{
		$this->db->select('courses.name,courses.id');
		$this->db->from('container_courses');
		$this->db->join('courses', 'container_courses.course_id = courses.id', 'left');
		$this->db->where(['container_courses.container_id' => $siteID,'courses.status' => 1,'courses.delete_status' => 1]);
		$query = $this->db->get();
		return $query->result_array();
	}


	/**
	 * getCitiesByState method
	 * @description this method is use to get cities by state
	 * @param string
	 * @return array
	 */
	public function getCitiesByState($state)
	{
		$this->db->select('cities.name');
		$this->db->from('states');
		$this->db->join('cities', 'states.code = cities.state_code', 'left');
		$this->db->where(['states.name' => $state]);
		$query = $this->db->get();
		return $query->result_array();
	}

	/**
	 * getSectionsListContainer method
	 * @description this method is use to get section by container
	 * @param int
	 * @return array
	 */
	public function getSectionsListContainer($containerID)
	{
		$this->db->select('course_sections.name as section_name,course_sections.start_date,course_sections.end_date,course_sections.id,courses.name,courses.id as course_id');
		$this->db->from('course_sections');
		$this->db->join('container_courses', 'course_sections.course_id = container_courses.course_id', 'inner');
		$this->db->join('courses', 'container_courses.course_id = courses.id', 'inner');
		$this->db->where(['course_sections.institute_id' => $containerID]);
		$this->db->where(['container_courses.container_id' => $containerID]);
		//$this->db->group_by('courses.id');
		$query = $this->db->get();
		return $query->result_array();
	}

	/**
	 * getAssignedTag method
	 * @description this method is use to get assigned tags
	 * @param int
	 * @return array
	 */
	public function getAssignedTag($postData = [])
	{   
		$this->db->select('tags.name,tag_assigned.id');
		$this->db->from('tag_assigned');
		$this->db->join('tags', 'tag_assigned.tag_id = tags.id', 'left');
		$this->db->where(['tag_assigned.reference_id' => $postData['referenceID'], 'tag_assigned.reference_type' => $postData['referencetype'],'tag_assigned.reference_sub_type' => $postData['reference_sub_type'],'tags.delete_status' => 1, 'tags.status'=>1]);
		$query = $this->db->get();
		return $query->result_array();
	}

	/**
	 * getSectionDetails method
	 * @description this method is use to get cities by state
	 * @param string
	 * @return array
	 */
	public function getSectionDetails($recordID)
	{
		$this->db->select('course_sections.name as sectionname,course_sections.course_id as course_id,course_sections.start_date,course_sections.end_date,containers.name as container_name, courses.name as course_name');
		$this->db->from('course_sections');
		$this->db->join('containers', 'course_sections.institute_id = containers.id', 'left');
		$this->db->join('courses', 'course_sections.course_id = courses.id', 'left');
		$this->db->where(['course_sections.id' => $recordID]);
		$query = $this->db->get();
		return $query->row();
	}


	/**
	 * getUsersByContainerID method
	 * @description this method is use to get users by contaner id
	 * @param string,int
	 * @return array
	 */
	public function getActiveInstructor($type = false, $limit, $page)
	{	

		$enrolledInstructor = $this->getEnrollUsersBySection('instructor');
		$enrolledInstructor = ($enrolledInstructor)?$enrolledInstructor:0;

		$this->db->select('users.first_name,users.middle_name,users.last_name,users.email,users.id')->from('users');
		$this->db->join('users_roles', 'users.id = users_roles.user_id AND users_roles.role_id='.INSTRUCTOR, 'inner');
		$this->db->where(['users.active'=>1, 'users.delete_status'=>1]);
		if($enrolledInstructor != 0)
		{
			$this->db->where_not_in('users.id', array_unique($enrolledInstructor));
		}
		
		if($this->input->post('keyword') && $this->input->post('keyword') != ''){
			$this->db->group_start();
			$this->db->like('users.first_name', $this->input->post('keyword'),'both');
			$this->db->or_like('users.last_name', $this->input->post('keyword'),'both');
			$this->db->or_like('users.email',$this->input->post('keyword'),'both');
			$this->db->group_end();
		}
		$this->db->order_by('users.id', 'desc');
		if ($type) {
			return $this->db->get()->num_rows();
		} else {	
			$this->db->limit($limit, $page);
			return $this->db->get()->result();
		}
	}	

	/**
	 * getUsersByContainerID method
	 * @description this method is use to get users by contaner id
	 * @param string,int
	 * @return array
	 */
	public function getUsersByContainerID($containerID, $type = false, $limit, $page)
	{	
		//pr($this->input->post('utype')); exit;
		if($this->input->post('utype')){
			$enrolledUsers1[0] = 0;
			$enrolledUsers = $this->getEnrollUsersBySection($this->input->post('utype'));
			$enrolledUsers = ($enrolledUsers)?$enrolledUsers:$enrolledUsers1;
		}else{
			$enUsers = $this->getEnrollUsersInSection();
		}
		
		$this->db->select('users.first_name,users.middle_name,users.last_name,users.email,users.id')
			->from('users');
			
			if(isset($enrolledUsers)){
				$this->db->where_in('users.id', $enrolledUsers);
			}else{
				$this->db->join('containers_users_roles', 'users.id = containers_users_roles.user_id AND (containers_users_roles.role_id ='.STUDENT .' OR containers_users_roles.role_id='.INSTRUCTOR.')', 'inner');		
				
				if($enUsers){
					$this->db->where_not_in('users.id', array_unique($enUsers));
				}
				$this->db->where('containers_users_roles.container_id', $containerID);
			}
			if($this->input->post('keyword') && $this->input->post('keyword') != ''){
				$this->db->group_start();
		        $this->db->like('users.first_name', $this->input->post('keyword'),'both');
		        $this->db->or_like('users.last_name', $this->input->post('keyword'),'both');
		        $this->db->or_like('users.email',$this->input->post('keyword'),'both');
		        $this->db->group_end();
			}
			$this->db->order_by('users.id', 'desc');
		if ($type) {
			return $this->db->get()->num_rows();
		} else {	
			$this->db->limit($limit, $page);
			return $this->db->get()->result();
		}
	}


	/*
     * Perform the SQL queries needed for an server-side processing requested
     * @param $_POST filter data based on the posted parameters
     */
	private function _get_datatables_query($postData)
	{	

		
   	    $this->db->select('SQL_CALC_FOUND_ROWS containers.id,containers.name,containers.domain,containers.status,containers.status_delete,institution_contacts.name as primary_contact_name', false);   
   	  	
   	  	if ($postData['order']['0']['column'] == 5) 
        {  	    
   	    	$this->db->select('COUNT(DISTINCT container_courses.course_id) as total_course');
   	    }else{
            $this->db->select('"..." as total_course');
        }

        if ($postData['order']['0']['column'] == 6) 
        {  
   	    	$this->db->select('COUNT(DISTINCT course_sections.id) as total_section');
   	    }else{
            $this->db->select('"..." as total_section');
        }

        if($postData['order']['0']['column'] == 7) 
        {  
   	    	$this->db->select('COUNT(DISTINCT containers_users_roles.user_id) as total_user');
   	    }else{
            $this->db->select('"..." as total_user');
        }

   	    /*$this->db->select('IF(containers_users_roles.role_id = STUDENT, COUNT(DISTINCT containers_users_roles.user_id),0) as total_user');*/

   	    if($postData['order']['0']['column'] == 8) 
        {  
   	    	$this->db->select('COUNT(DISTINCT course_assigned_to_users.user_id) as total_enroll_user');
   	    }else{
   	    	$this->db->select('"..." as total_enroll_user');
   	    }

		//$this->db->select('DATE_FORMAT('.$this->table.'.created_at, "%m/%d/%Y") as created_at');
        $this->db->from($this->table);
   	    if ($postData['order']['0']['column'] == 7) {   
   	    $this->db->join('containers_users_roles', 'containers_users_roles.container_id = '.$this->table.'.id AND containers_users_roles.role_id ='.STUDENT, 'left');
   		}
   	    
   	    if ($postData['order']['0']['column'] == 5) {
   	    	$this->db->join('container_courses', 'container_courses.container_id = '.$this->table.'.id', 'left');
   		}

   		if ($postData['order']['0']['column'] == 6) {
   			$this->db->join('container_courses', 'container_courses.container_id = '.$this->table.'.id', 'left');
   	    	$this->db->join('course_sections', 'course_sections.institute_id = '.$this->table.'.id AND course_sections.course_id = container_courses.course_id', 'left');
   		}

   		if ($postData['order']['0']['column'] == 8) {
   	    	$this->db->join('course_assigned_to_users', 'course_assigned_to_users.container_id = '.$this->table.'.id', 'left');
   		}

   	    $this->db->join('institution_contacts', 'institution_contacts.institution_id = '.$this->table.'.id AND institution_contacts.type = "primary"', 'left');


   	    if ((isset($postData['system_category']) && $postData['system_category'] != '') || (isset($postData['institution_category']) && $postData['institution_category'] != ''))
   	    {
   	    	$this->db->join('category_assigned', 'category_assigned.reference_id = '.$this->table.'.id', 'right');
   	    } 
		if ((isset($postData['institution_tag']) && $postData['institution_tag'] != '') || (isset($postData['system_tag']) && $postData['system_tag'] != ''))
		{
			$this->db->join('tag_assigned', 'tag_assigned.reference_id = '.$this->table.'.id', 'right');
		}

		if (isset($postData['system_category']) && $postData['system_category'] != '') 
		{	
			$this->db->where_in('category_assigned.category_id', $postData['system_category']);
		}

		if (isset($postData['system_tag']) && $postData['system_tag'] != '') 
		{
			$this->db->where_in('tag_assigned.tag_id' , $postData['system_tag']);
		}

		if (isset($postData['institution_category']) && $postData['institution_category'] != '') 
		{
			$this->db->where_in('category_assigned.category_id', $postData['institution_category']);
		}

		if (isset($postData['institution_tag']) && $postData['institution_tag'] != '') 
		{
			$this->db->where_in('tag_assigned.tag_id', $postData['institution_tag']);
		}
		
		$condition= [
			"containers.status_delete"=>1
		];
		$this->db->where($condition);

		$i = 0;
		// loop searchable columns 

		foreach($this->column_search as $item){
		
            // if datatable send POST for search
            if($postData['search']['value']){
                // first loop
                if($i === 0){
				    // open bracket 
					$this->db->group_start();
					//pr( $item);	
                    $this->db->like($item, $postData['search']['value'], 'both');
                }else{
                    $this->db->or_like($item, $postData['search']['value'], 'both');
                }
                    // last loop
                if(count($this->column_search) - 1 == $i){
                    // close bracket
                    $this->db->group_end();
                }
            }
            $i++;
        }

		$this->db->group_by('containers.id');

		if (isset($postData['order'])) 
		{
			$this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
		} 
		else if (isset($this->order)) 
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	

	/**
	 * count_container method
	 * @description this method is use to get count of container
	 * @return int
	 */
	public function count_container()
	{
		$condition=array(
			"status_delete"=>1
		);
		$this->db->select('*'); 
        $this->db->from($this->table);
		$this->db->where($condition);
		return $this->db->count_all_results();
	}

	/*
     * Count records based on the filter params
     * @param $_POST filter data based on the posted parameters
     */
	public function countFiltered($postData)
	{
		$this->_get_datatables_query($postData);
		
		$query = $this->db->get();
		
		return $query->num_rows();
	}

	/*
     * Fetch containers data from the database
     * @param $_POST filter data based on the posted parameters
     */
    public function getRows($postData){		
		$this->_get_datatables_query($postData);		
        if($postData['length'] != -1){
            $this->db->limit($postData['length'], $postData['start']);
        }
		$query = $this->db->get();
		$data['result'] = $query->result();
		//pr($this->db->last_query());exit;
		$data['total'] = $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total;
		return $data;
    }


    /**
	 * getRoles method
	 * @description this method is use to get users roles
	 * @return array
	 */
    public function getRoles($ignore = [])
    {	
    	$this->db->from('roles');
    	$this->db->where_not_in('roles.id',$ignore);
		return $this->db->get();
    }

    /**
	 * getTags method
	 * @description this method is use to get all tag to matching search data
	 * @param string,int
	 * @return array
	 */
    public function getTags($conditions,$keyword)
    {	
    	$this->db->select('tags.id,tags.name')->from('tags');
    	$this->db->where($conditions);
    	$this->db->like('tags.name', $keyword,'both');
    	$this->db->limit(15); 
    	return $this->db->get()->result();
    }


    /**
	 * getContainerUsersForGroup method
	 * @description this method is use to get users by contaner id
	 * @param string,int
	 * @return array
	 */
	public function getContainerUsersForGroup($postData = [], $type = false, $limit, $page)
	{	
		$containerID = $postData['siteID'];
		if($this->input->post('utype')){
			$enrolledUsers1[0] = 0;
			$enrolledUsers = $this->__getEnrollUsersInGroup($postData);
			$enrolledUsers = ($enrolledUsers)?$enrolledUsers:$enrolledUsers1;
		}
		
		$this->db->select('users.first_name,users.middle_name,users.last_name,users.email,users.id')
			->from('users');
			
			if(isset($enrolledUsers)){
				$this->db->where_in('users.id', $enrolledUsers);
			}else{
				$this->db->join('containers_users_roles', 'users.id = containers_users_roles.user_id AND containers_users_roles.role_id = 11', 'inner');
				$this->db->where('containers_users_roles.container_id', $containerID);
			}
			if($this->input->post('keyword') && $this->input->post('keyword') != ''){
				$this->db->group_start();
		        $this->db->like('users.first_name', $this->input->post('keyword'),'both');
		        $this->db->or_like('users.last_name', $this->input->post('keyword'),'both');
		        $this->db->or_like('users.email',$this->input->post('keyword'),'both');
		        $this->db->group_end();
			}
			$this->db->order_by('users.id', 'desc');
		if ($type) {
			return $this->db->get()->num_rows();
		} else {	
			$this->db->limit($limit, $page);
			return $this->db->get()->result();
		}
	}


	private function __getEnrollUsersInGroup($postPassData = [])
	{	
		
		$conditions = [
			'container_group_id' => $postPassData['groupID'],
			'container_id' => $postPassData['siteID'],
			'course_id' => $postPassData['courseID'],
			'section_id' => $postPassData['sectionID']
		];
		$this->db->select('container_groups_users.user_id');
		$this->db->from('container_groups_users');
		$this->db->where($conditions);
		$return = [];
		if($postPassData['utype'] == 'group_leader')
		{
			$this->db->where(['container_groups_users.is_leader' => 1]);
		}

		if($postPassData['utype'] == 'student')
		{
			//$this->db->where_(['container_groups_users.is_leader' => 0]);
			$this->db->where_in('container_groups_users.is_leader',[0,1]);

		}

		$query = $this->db->get();
		foreach ($query->result_array() as $key => $value) {
			$return[] = $value['user_id'];
		}
		return $return;
		
	}


	/*
     * Fetch containers data from the database
     * @param $_POST filter data based on the posted parameters
     */
    public function getContainerRowsForExport($postData){		
		$this->_getContainerRowsForExport_get_datatables_query($postData);		
        /*if($postData['length'] != -1){
            $this->db->limit($postData['length'], $postData['start']);
        }*/
		$query = $this->db->get();
		$data['result'] = $query->result();
		$data['total'] = $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total;
		return $data;
    }
	

	/*
     * Perform the SQL queries needed for an server-side processing requested
     * @param $_POST filter data based on the posted parameters
     */
	private function _getContainerRowsForExport_get_datatables_query($postData)
	{	
		
		$condition= [
			"containers.status_delete"=>1
		];
   	    $this->db->select('SQL_CALC_FOUND_ROWS containers.id,containers.name,containers.domain,containers.status,containers.status_delete,institution_contacts.name as primary_contact_name', false);   	    
   	    $this->db->select('COUNT(DISTINCT container_courses.course_id) as total_course');
   	    $this->db->select('COUNT(DISTINCT course_sections.id) as total_section');
   	    
   	    $this->db->select('COUNT(DISTINCT containers_users_roles.user_id) as total_user');
   	    /*$this->db->select('IF(containers_users_roles.role_id = STUDENT, COUNT(DISTINCT containers_users_roles.user_id),0) as total_user');*/

   	    
   	    $this->db->select('COUNT(DISTINCT course_assigned_to_users.user_id) as total_enroll_user');

		//$this->db->select('DATE_FORMAT('.$this->table.'.created_at, "%m/%d/%Y") as created_at');
        $this->db->from($this->table);
   	   
   	    
   	    $this->db->join('containers_users_roles', 'containers_users_roles.container_id = '.$this->table.'.id AND containers_users_roles.role_id ='.STUDENT, 'left');

   	    $this->db->join('container_courses', 'container_courses.container_id = '.$this->table.'.id', 'left');
   	    $this->db->join('course_sections', 'course_sections.institute_id = '.$this->table.'.id AND course_sections.course_id = container_courses.course_id', 'left');

   	    $this->db->join('course_assigned_to_users', 'course_assigned_to_users.container_id = '.$this->table.'.id', 'left');
   	    $this->db->join('institution_contacts', 'institution_contacts.institution_id = '.$this->table.'.id  AND institution_contacts.type = "primary"', 'left');

   	   	//pr($postData);exit;
		if(isset($postData['selected_site_ids']) && $postData['selected_site_ids'] != ''){
			$this->db->where_in('containers.id', explode(',',$postData['selected_site_ids']));
		}

		$this->db->where($condition);
		$this->db->group_by('containers.id');

	}


	public function get_number_current_courses($containerID){
        $sql = "SELECT COUNT(DISTINCT container_courses.course_id) as total_course  FROM container_courses
        WHERE container_courses.container_id = " . $containerID;
        $query =  $this->db->query($sql);
        $total_course = $query->row()->total_course;
        return $total_course;
    }

    public function get_number_current_section($containerID)
    {
    	$sql = "SELECT COUNT(DISTINCT course_sections.id) as total_section  FROM course_sections
        WHERE course_sections.institute_id = " . $containerID;
        $query =  $this->db->query($sql);
        $total_section = $query->row()->total_section;
        return $total_section;
    }

    public function get_number_current_student($containerID)
    {
    	$sql = "SELECT COUNT(DISTINCT containers_users_roles.user_id) as total_user  FROM containers_users_roles
        WHERE containers_users_roles.container_id = " . $containerID;
        $query =  $this->db->query($sql);
        $total_user = $query->row()->total_user;
        return $total_user;
    }

    public function get_number_current_enrollment($containerID)
    {
    	$sql = "SELECT COUNT(DISTINCT course_assigned_to_users.user_id) as total_enroll_user  FROM course_assigned_to_users
        WHERE course_assigned_to_users.container_id = " . $containerID;
        $query =  $this->db->query($sql);
        $total_enroll_user = $query->row()->total_enroll_user;
        return $total_enroll_user;
    }
   
}
