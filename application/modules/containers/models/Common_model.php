<?php
/**
*
*/
class Common_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();

		$this->column_order = array(null, 'users.first_name',  'users.email');
        // Set searchable column fields
		$this->column_search = array('users.first_name','users.last_name', 'users.email');
        // Set default order
		$this->order = array('course_assigned_to_users.id' => 'desc');
	}
	
	function InsertData($table,$Data)
	{
		$Insert = $this->db->insert($table,$Data);
		if ($Insert):
			return true;
		endif;
	}
	function getAllData($table,$specific='',$row='',$Where='',$order='',$limit='',$groupBy='',$like = '')
	{	

		// If Condition
		if (!empty($Where)):
			$this->db->where($Where);
		endif;
		// If Specific Columns are require
		if (!empty($specific)):
			$this->db->select($specific);
		else:
			$this->db->select('*');
		endif;

		if (!empty($groupBy)):
			$this->db->group_by($groupBy);
		endif;
		// if Order
		if (!empty($order)):
			$this->db->order_by($order);
		endif;
		// if limit
		if (!empty($limit)):
			$this->db->limit($limit);
		endif;

		//if like
		if(!empty($like)):
			$this->db->like($like);
		endif;	
		// get Data
		
		//if select row
		if(!empty($row)):
			$GetData = $this->db->get($table);
			return $GetData->row();
		else:
			$GetData = $this->db->get($table);
			return $GetData->result();
		endif;	
	}
	function UpdateDB($table,$Where,$Data)
	{
		$this->db->where($Where);
		$Update = $this->db->update($table,$Data);
		if ($Update):
			return true;
		else:
			return false;
		endif;
	}
	function Authentication($table,$data)
	{
		$this->db->where($data);
		$query = $this->db->get($table);
		if ($query) {
			return $query->row();
		}
		else
		{
			return false;
		}
	}
	function DJoin($field,$tbl,$jointbl1,$Joinone,$row='',$jointbl3='',$Where='',$order='',$groupy = '',$limit = '',$query = '')
    {
        $this->db->select($field);
        $this->db->from($tbl);
        $this->db->join($jointbl1,$Joinone);
        if (!empty($jointbl3)):
            foreach ($jointbl3 as $Table => $On):
                $this->db->join($Table,$On);
            endforeach;
        endif;
        // if Group
		if (!empty($groupy)):
			$this->db->group_by($groupy);
		endif;
        if(!empty($order)):
            $this->db->order_by($order);
        endif;
        if(!empty($Where)):
            $this->db->where($Where);
        endif;
        if(!empty($limit)):
            $this->db->limit($limit);
        endif;
        
        if(!empty($query)):
            $this->db->like($like, $query);
        endif;

        if(!empty($row)):
			$query = $this->db->get();
			return $query->row();
		else:
	        $query=$this->db->get();
	        return $query->result();
	    endif;	    

    }
    function DeleteDB($table,$where)
    {
    	$this->db->where($where);
    	$done = $this->db->delete($table);
    	if ($done) {
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    }

    public function MultiDeleteDB($table, $where = '', $users = [])
    {	

    	$this->db->where_in('user_id',$users);
    	$this->db->where($where);
    	$done = $this->db->delete($table);
    	
    	if ($done) {
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    }

	function Encode_html($str) {
    return trim(stripslashes(htmlentities($str)));
	}

	function Encode($str) {
	    return trim(  htmlentities( $str, ENT_QUOTES ) ) ;
	}

	function Decode($str) {
	    return html_entity_decode(stripslashes($str));
	}

	function Encrypt($password) {
	    return crypt(md5($password), md5($password));
	}

	
	

    /**
     * getCheckExist method
     * @description this method is use to find name from database
	 * @param string, numbers, array
     * @return number
     */	
   	public function getCheckExist($coloumnName,$table,$postData,$type,$containerID = '')
	{	
		if($containerID) {
			$condition = [
				$coloumnName => $postData,
				'type' => $type,
				'status_delete'=>1,
				'id !=' => $containerID  
			];
		}else{
			$condition = [
				$coloumnName => $postData,
				'type' => $type,
				'status_delete'=> 1
			];
		}
		$this->db->where($condition);
		if($query = $this->db->get($table))
		{	
			return $query->num_rows();
		}		
	}

	

	/**
     * getRecordByIdType method
     * @description this method is use to fetch sign record from db
	 * @param numeric
     * @return array
     */	
	public function getRecordByIdType($id, $type, $column = ''){
		if($column != '') {
			$this->db->select($column);
		}		
		$this->db->where(['containers.id' => $id, 'containers.type' => $type,'containers.status_delete' => 1]);
        $query = $this->db->get("containers");
        return $query->row();
	}

	
    /**
     * getCountRecord method
     * @description this method is use to get count record
	 * @param string, number
     * @return number
     */	
    public function getCountRecord($table,$condition) {
    	$this->db->where($condition);
		if($query = $this->db->get($table))
		{	
			return $query->num_rows();
		}
    }


    /**
     * getRecordByID method
     * @description this method is use to get  record
	 * @param string, number
     * @return array
     */	
    public function getRecordByID($table,$condition,$columns = '') {

    	if($columns)
    	{
    		$this->db->select($columns);
    	}	
    	$this->db->where($condition);
		if($query = $this->db->get($table))
		{	
			return $query->row();
		}
    }

    /**
     * getDatawithIncluse method
     * @description this method is use to get date with multiple conditions
	 * @param string, int
     * @return array
     */	
	public function getDatawithIncluse($tableName,$columns= '', $incluseArray='',$incluseColumn='', $where= '' ) {
		
		// If Specific Columns are require
		if (!empty($columns)):
			$this->db->select($columns);
		else:
			$this->db->select('*');
		endif;
		$this->db->from($tableName);

		// If Condition
		if (!empty($where)):
			$this->db->where($where);
		endif;
		if (!empty($incluseArray)):
			$this->db->where_in($incluseColumn,$incluseArray);
		endif;
		
		$query = $this->db->get();
		return $query->result_array();
    }

     /**
     * InsertBatchData method
     * @description this method is use to save mutiple record in signle query
	 * @param array
     * @return boolean
     */	

    public function InsertBatchData($table,$Data)
	{
		$Insert = $this->db->insert_batch($table,$Data);		
		if ($Insert):
			return true;
		endif;
	}

	
	/*
     * Fetch members data from the database
     * @param $_POST filter data based on the posted parameters
     */
    public function getRows($postData){
	
		$this->_get_datatables_query($postData);
	
        if($postData['length'] != -1){
            $this->db->limit($postData['length'], $postData['start']);
        }
		$query = $this->db->get();
		$data['result'] = $query->result();
		$data['total'] = $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total;
        return $data;
    }
     

	public function _get_datatables_query($postData)
	{
		$this->db->select("SQL_CALC_FOUND_ROWS users.id as user_id,course_assigned_to_users.id as enroll_id, CONCAT_WS(' ', users.first_name, users.middle_name, users.last_name) as user_name,  users.email,course_assigned_to_users.section_id , IF(course_assigned_to_users.is_instructor = 0, 'Student', 'Instructor') as role", false);

		/*$this->db->select("SQL_CALC_FOUND_ROWS users.id as user_id,course_assigned_to_users.id as enroll_id, CONCAT_WS(' ', users.first_name, users.middle_name, users.last_name) as user_name,  users.email,course_assigned_to_users.section_id , IF(course_assigned_to_users.is_instructor = 0, 'Student', 'Instructor') as role", false);*/

		$this->db->from('course_assigned_to_users');
		$this->db->join('users', 'users.id = course_assigned_to_users.user_id', 'inner');
		$this->db->where('course_assigned_to_users.section_id', $postData['section_selected']);
		$this->db->where('course_assigned_to_users.is_instructor', 0);
		
		$i = 0;
        // loop searchable columns 
        foreach($this->column_search as $item){
		
            // if datatable send POST for search
            if($postData['search']['value']){
                // first loop
                if($i === 0){
				
                    // open bracket
					$this->db->group_start();
					//pr( $item);	
                    $this->db->like($item, $postData['search']['value']);
                }else{
                    $this->db->or_like($item, $postData['search']['value']);
                }
                    // last loop
                if(count($this->column_search) - 1 == $i){
                    // close bracket
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if(isset($postData['order'])){
            $this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        }else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }	
	}

	public function multiUpdateRecords($table,$selectedRecordID = [], $column)
    {	
    	if ($this->db->update_batch($table,$selectedRecordID, $column)) {
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    }

    /**
     * getDatawithIncluse method
     * @description this method is use to get date with multiple conditions
	 * @param string, int
     * @return array
     */	
	public function getCountRecordUsingincluse($tableName,$columns= '', $incluseArray='',$incluseColumn='', $where= '' ) {
		
		// If Specific Columns are require
		if (!empty($columns)):
			$this->db->select($columns);
		else:
			$this->db->select('*');
		endif;
		$this->db->from($tableName);

		// If Condition
		if (!empty($where)):
			$this->db->where($where);
		endif;
		if (!empty($incluseArray)):
			$this->db->where_in($incluseColumn,$incluseArray);
		endif;
		
		$query = $this->db->get();
		return $query->num_rows();
    }

    
     
}
?>
