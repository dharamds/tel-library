<?php
/**
 *
 */
class Users_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
		$this->column_order = array(null, 'users.first_name',  'users.email');
        // Set searchable column fields
		$this->column_search = array('users.first_name','users.last_name', 'users.email');

		$this->col_order = array('container_groups_users.id' => 'desc');
	}


	/**
	 * getLeaderByCourseSection method
	 * @description this method is use to get all group leaders by course and section
	 * @param int
	 * @return array
	 */
	public function getLeaderByCourseSection($postData)
	{
		//pr($postData);
		$this->db->select('CONCAT_WS(" ", users.first_name, users.middle_name, users.last_name) as user_name,users.id');
		$this->db->from('container_groups_users');
		$this->db->join('users', 'users.id = container_groups_users.user_id', 'left');
		$this->db->where([
			'container_groups_users.container_id' => $postData['siteID'],
			'container_groups_users.course_id' => $postData['course_id'],
			'container_groups_users.section_id' => $postData['section_id'],
			'users.delete_status' => 1,
			'container_groups_users.is_leader' => 1
		]);
		$this->db->group_by('container_groups_users.user_id');
		$query = $this->db->get();
		return $query->result_array();
	}

	 /**
	 * getUsers method
	 * @description this method is use to get all users to matching search data
	 * @param string,int
	 * @return array
	 */
    public function getUsers($conditions,$keyword)
    {	

    	$this->db->select('CONCAT_WS(" ", users.first_name, users.middle_name, users.last_name) as user_name,users.id,users.email');
		$this->db->from('containers_users_roles');
		$this->db->join('users', 'containers_users_roles.user_id = users.id', 'left');
		$this->db->where($conditions);
    	$this->db->like('users.email', $keyword,'both');
    	$this->db->limit(15); 
    	return $this->db->get()->result();
    }

     /**
	 * getUsersName method
	 * @description this method is use to get all users to matching search data
	 * @param string,int
	 * @return array
	 */
    public function getUsersName($conditions,$keyword)
    {	

    	$this->db->select('CONCAT_WS(" ", users.first_name, users.middle_name, users.last_name) as user_name,users.id,users.email');
		$this->db->from('containers_users_roles');
		$this->db->join('users', 'containers_users_roles.user_id = users.id', 'left');
		$this->db->where($conditions);
    	$this->db->like('users.first_name', $keyword,'both');
    	$this->db->or_like('users.middle_name', $keyword,'both');
    	$this->db->or_like('users.last_name', $keyword,'both');
    	$this->db->group_by('users.id');
		$this->db->limit(15); 
    	return $this->db->get()->result();
    }


    /*
     * Fetch members data from the database
     * @param $_POST filter data based on the posted parameters
     */
    public function getGroupUsers($postData){
		$this->_get_datatables_query_for_group_users($postData);
	
        if($postData['length'] != -1){
            $this->db->limit($postData['length'], $postData['start']);
        }
		$query = $this->db->get();
		$data['result'] = $query->result();
		$data['total'] = $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total;
        return $data;
    }

    public function _get_datatables_query_for_group_users($postData)
	{
		//pr($postData);exit;
		$this->db->select("SQL_CALC_FOUND_ROWS users.id as user_id,container_groups_users.id as enroll_id, CONCAT_WS(' ', users.first_name, users.middle_name, users.last_name) as user_name,users.email,container_groups_users.section_id , IF(container_groups_users.is_leader = 0, 'Student', 'Leader') as role", false);
		$this->db->from('container_groups_users');
		$this->db->join('users', 'users.id = container_groups_users.user_id', 'inner');
		$this->db->where([
				'container_groups_users.container_group_id' => $postData['groupID'],
				'container_groups_users.container_id'=> $postData['siteID'],
				'container_groups_users.course_id'=> $postData['courseID'],
				'container_groups_users.section_id'=> $postData['sectionID']
		]);

		$i = 0;
        // loop searchable columns 
        foreach($this->column_search as $item){
		
            // if datatable send POST for search
            if($postData['search']['value']){
                // first loop
                if($i === 0){
				
                    // open bracket
					$this->db->group_start();
					//pr( $item);	
                    $this->db->like($item, $postData['search']['value']);
                }else{
                    $this->db->or_like($item, $postData['search']['value']);
                }
                    // last loop
                if(count($this->column_search) - 1 == $i){
                    // close bracket
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if(isset($postData['order'])){
            $this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        }else if(isset($this->col_order)){
            $order = $this->col_order;
            $this->db->order_by(key($order), $order[key($order)]);
        }	
	}

	/**
	 * getLeaderByCourseSection method
	 * @description this method is use to get all group leaders by course and section
	 * @param int
	 * @return array
	 */
	public function getUsersByGroupID($groupIDS,$siteID)
	{
		//pr($postData);
		$this->db->select('CONCAT_WS(" ", users.first_name, users.middle_name, users.last_name) as user_name,users.id,users.email');
		$this->db->from('container_groups_users');
		$this->db->join('users', 'users.id = container_groups_users.user_id', 'left');
		$this->db->where([
			'container_groups_users.container_id' => $siteID,
			'users.delete_status' => 1,
			'users.active' => 1
		]);
		$this->db->where_in('container_groups_users.container_group_id',$groupIDS);
		$this->db->group_by('container_groups_users.user_id');
		$query = $this->db->get();
		return $query->result_array();
	}

	 public function get_enroll_courses($userID){
        //
        $this->db->select('GROUP_CONCAT(DISTINCT COALESCE(courses.name, \'NA\') SEPARATOR "<br>")  as course_name', false);
        $this->db->from("users");
        $this->db->join('course_assigned_to_users', 'course_assigned_to_users.user_id = users.id', 'left');
		$this->db->join('courses', 'courses.id = course_assigned_to_users.course_id AND course_assigned_to_users.user_id = users.id', 'left');
		$this->db->where('users.id', $userID);
		$result = $this->db->get()->row();         
        return ($result->course_name) ? $result->course_name : 'NA';
    }

     public function get_enroll_section($userID){
        $this->db->select('GROUP_CONCAT(DISTINCT COALESCE(course_sections.name, \'NA\') SEPARATOR "<br>")  as course_section_name', false);
        $this->db->from("users");
        $this->db->join('course_assigned_to_users', 'course_assigned_to_users.user_id = users.id', 'left');

        $this->db->join('courses', 'courses.id = course_assigned_to_users.course_id AND course_assigned_to_users.user_id = users.id', 'left');
		$this->db->join('container_courses', 'container_courses.course_id = courses.id', 'inner');

		$this->db->join('course_sections', 'course_sections.id = course_assigned_to_users.section_id AND course_assigned_to_users.user_id = users.id', 'left');
		$this->db->where('users.id', $userID);
		$result = $this->db->get()->row();         
        return ($result->course_section_name) ? $result->course_section_name : 'NA';
    }

   
}
