<?php
/**
 *
 */
class Groups_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
		//$this->load->model('common_model');
		// Set table name
		// Set orderable column fields
		$this->column_order = array(null,null,'containers.status', 'containers.name', 'institution_contacts.name', 'total_course','total_section','total_user','total_enroll_user');
		// Set searchable column fields
		$this->column_search = array('containers.name');
		// Set default order
		$this->order = array('containers.created_at' => 'desc');
	}


	/*
     * Fetch containers data from the database
     * @param $_POST filter data based on the posted parameters
     */
    public function getGroupRows($postData){	
    	//pr($postData)	
		$this->_get_group_datatables_query($postData);		
        if($postData['length'] != -1){
            $this->db->limit($postData['length'], $postData['start']);
        }
		$query = $this->db->get();
		return $query->result();
    }

    /*
     * Perform the SQL queries needed for an server-side processing requested
     * @param $_POST filter data based on the posted parameters
     */
	private function _get_group_datatables_query($postData)
	{
		$condition= [
			"container_groups.delete_status"=>1,
			"container_groups.container_id"=> $postData['institution_id']
		];
		 $this->db->select('container_groups.id,container_groups.name as group_name,container_groups.status,container_groups.delete_status,courses.name as course_name,course_sections.name as section_name/*,container_groups_users.is_leader*/', false);
   	
   	    $this->db->select('IF(container_groups.display_avaibility = 1, \'Display for student\', \'Hide for Student\') as display_avaibility');

   	    $this->db->select('IF(container_groups_users.is_leader = 1, GROUP_CONCAT(CONCAT_WS(" ", users.first_name, users.middle_name, users.last_name) SEPARATOR "<br>"), \'NA\') as group_leaders');

   	    $this->db->select('COUNT(container_groups_users.user_id) as total_users');
        $this->db->from('container_groups');

   	    $this->db->join('container_groups_users', 'container_groups_users.container_group_id = container_groups.id', 'left');
   	    $this->db->join('courses', 'courses.id = container_groups.course_id', 'left');   	    
   	    $this->db->join('course_sections', 'course_sections.id = container_groups.section_id', 'left');

   	    $this->db->join('users', 'users.id = container_groups_users.user_id', 'left');

   	    if (isset($postData['selected_course']) && $postData['selected_course'] != '') 
		{
			$this->db->where(['container_groups.course_id' => $postData['selected_course']]);
		}

		if (isset($postData['section_id']) && $postData['section_id'] != '') 
		{
			$this->db->where(['container_groups.section_id' => $postData['section_id']]);
		}

		if (isset($postData['group_leader']) && $postData['group_leader'] != '') 
		{
			$this->db->where(['container_groups_users.user_id' => $postData['group_leader']]);
		}
		$this->db->where($condition);
		$i = 0;
		// loop searchable columns 
		$column_search = ['container_groups.name','courses.name','course_sections.name'];
		foreach ($column_search as $item) {
			// if datatable send POST for search
			if ($postData['search']['value']) {
				// first loop
				if ($i === 0) {
					// open bracket
					$this->db->group_start();
					$this->db->like($item, $postData['search']['value'], 'both');
				} else {
					$this->db->or_like($item, $postData['search']['value'], 'both');
				}

				if(count($column_search) - 1 == $i){
                    // close bracket
                    $this->db->group_end();
                }
			}
			$i++;
		}

		
		$this->db->group_by('container_groups.id');

		$groupOrder = array('container_groups.created' => 'desc');
		if (isset($postData['order'])) 
		{
			$column_order = array(null,null, 'container_groups.status', 'container_groups.name', 'course_name', 'section_name', 'display_avaibility', 'total_users', 'group_leaders');
			$this->db->order_by($column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
		} 

		else if (isset($groupOrder)) 
		{
			$order = $groupOrder;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	/**
	 * count_container method
	 * @description this method is use to get count of container
	 * @return int
	 */
	public function count_group_container()
	{
		$condition=array(
			"delete_status"=>1
		);
		$this->db->select('*'); 
        $this->db->from('container_groups');
		$this->db->where($condition);
		return $this->db->count_all_results();
	}

	/*
     * Count records based on the filter params
     * @param $_POST filter data based on the posted parameters
     */
	public function countGroupFiltered($postData)
	{
		$this->_get_group_datatables_query($postData);		
		$query = $this->db->get();		
		return $query->num_rows();
	}

	public function get_groups_by_course_section($container_id, $course_id, $section_id){
		$this->db->select('container_groups.name, container_groups.id'); 
        $this->db->from('container_groups');
		$this->db->where(['container_groups.container_id' => $container_id, 'container_groups.course_id' => $course_id, 'container_groups.section_id' => $section_id,'container_groups.status' => 1,'container_groups.delete_status' => 1]);
		return $this->db->get()->result();
	}

	public function get_users_from_site($siteID)
	{
		$this->db->select('CONCAT_WS(" ", users.first_name, users.middle_name, users.last_name) as user_name,users.id,users.email');
		$this->db->from('containers_users_roles');
		$this->db->join('users', 'containers_users_roles.user_id = users.id', 'left');
		$this->db->where(['containers_users_roles.container_id' => $siteID]);
		$query = $this->db->get();
		return $query->result_array();
	}


   
}
