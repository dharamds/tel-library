<div class="container-fluid">
    <div class="row">
        <div class="col">
            <div class="panel panel-default" id="tabslist">
                <div class="panel-heading"> <!-- disable-all -->
                    <ul class="nav nav-tabs <?php echo (isset($siteID)?'':'disable-all')?>" role="tablist">
                        <li role="presentation" class="active identity institute_tab_management" data-tabid="identity" data-siteid="<?php echo (isset($siteID)?$siteID:0); ?>">
                            <a href="#identity" role="tab" data-toggle="tab">Identity</a>
                        </li>
                        <li role="presentation" class="contactInfo institute_tab_management" data-tabid="contactInfo" data-siteid="<?php echo (isset($siteID)?$siteID:0); ?>">
                            <a href="#contactInfo" role="tab" data-toggle="tab">Contact Info</a>
                        </li>
                        <li role="presentation" class="visuals institute_tab_management" data-tabid="visuals" data-siteid="<?php echo (isset($siteID)?$siteID:0); ?>">
                            <a href="#visuals" role="tab" data-toggle="tab">Visuals</a>
                        </li>

                        <li role="presentation" class="courses institute_tab_management" data-tabid="courses" data-siteid="<?php echo (isset($siteID)?$siteID:0); ?>">
                            <a href="#courses" role="tab" data-toggle="tab">Courses</a>
                        </li>
                        <li role="presentation" class="metadata institute_tab_management" data-tabid="metadata" data-siteid="<?php echo (isset($siteID)?$siteID:0); ?>">
                            <a href="#metadata" role="tab" data-toggle="tab">Metadata</a>
                        </li>
                        <li role="presentation" class="users institute_tab_management" data-tabid="users" data-siteid="<?php echo (isset($siteID)?$siteID:0); ?>" >
                            <a href="#users" role="tab" data-toggle="tab">Users</a>
                        </li>
                        <li role="presentation" class="groups institute_tab_management" data-tabid="groups" data-siteid="<?php echo (isset($siteID)?$siteID:0); ?>" >
                            <a href="#groups" role="tab" data-toggle="tab">Groups</a>
                        </li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <!------------------------->
                        <div role="tabpanel" class="tab-pane active identity-skeleton" id="identity" >
                            <div class="col-sm-12">
                                <div class="page-loading-box">
                                    <span class="page-loader-quart"></span> Loading...
                                </div>
                            </div>
                        </div>

                        <!---------------------------------->
                        <div role="tabpanel" class="tab-pane contactinfo-skeleton" id="contactInfo">
                            <div class="col-sm-12">
                                <div class="page-loading-box">
                                    <span class="page-loader-quart"></span> Loading...
                                </div>
                            </div>
                        </div>
                        <!------------------------->
                        <!------------------------->
                        <div role="tabpanel" class="tab-pane visual-skeleton" id="visuals">
                            <div class="col-sm-12">
                                <div class="page-loading-box">
                                    <span class="page-loader-quart"></span> Loading...
                                </div>
                            </div>
                        </div>
                        <!------------------------->
                        <!------------------------->
                        <div role="tabpanel" class="tab-pane courses-skeleton" id="courses">
                            <div class="col-sm-12">
                                <div class="page-loading-box">
                                    <span class="page-loader-quart"></span> Loading...
                                </div>
                            </div>
                        </div>
                        <!------------------------->
                        <!------------------------->
                        <div role="tabpanel" class="tab-pane metadata-skeleton" id="metadata">
                            <div class="col-sm-12">
                                <div class="page-loading-box">
                                    <span class="page-loader-quart"></span> Loading...
                                </div>
                            </div>
                        </div>
                        <!------------------------->
                        <div role="tabpanel" class="tab-pane users-skeleton" id="users">
                            <div class="col-sm-12">
                                <div class="page-loading-box">
                                    <span class="page-loader-quart"></span> Loading...
                                </div>
                            </div>
                        </div>
                        <!------------------------->
                        <div role="tabpanel" class="tab-pane groups-skeleton" id="groups">
                            <div class="col-sm-12">
                                <div class="page-loading-box">
                                    <span class="page-loader-quart"></span> Loading...
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://unpkg.com/jquery-input-mask-phone-number@1.0.0/dist/jquery-input-mask-phone-number.js"></script>

<script type="text/javascript" src="<?php bs('public/assets/js/container/school-create.js') ?>"></script>
<?php
$jsVars = [
    'ajax_call_root' => base_url(),
    'siteID' =>(isset($siteID)?$siteID:0)
];
?>
<script>
jQuery(document).ready(function () {
    SchoolCreate.init(<?php echo json_encode($jsVars); ?>);
    $('.institute_tab_management').on('click', function(){
        getViewTemplate($(this).data('tabid'),$(this).data('siteid'));
    });
});

function getViewTemplate(selectorView, siteID) {
    $(".tab-pane").html('<div class="col-sm-12"><div class="page-loading-box"><span class="page-loader-quart"></span> Loading...</div></div>');
    $("#" + selectorView).load("<?php echo base_url() ?>containers/schools/load_view/" + selectorView + "/" + siteID, function () {});
}

function getViewInnerTemplate(selectorView, siteID) {
    $("#" + selectorView).load("<?php echo base_url() ?>containers/schools/load_view/" + selectorView + "/" + siteID, function () {});
}
$(".nav-tabs a[data-toggle=tab]").on("click", function(e) {
  if ($(this).parent().parent().hasClass("disable-all")) {
    e.preventDefault();
    return false;
  }
});
</script>