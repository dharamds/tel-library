<div class="row">
  <div class="col-sm-12 p-0 themePanel">
      <div class="col-sm-12 icon-left pt-3 pb-4">
          <h4 class="text-dark p-0 block-heading">
              <i class="icon flaticon-penitentiary mr-3"></i> <?php echo $containerDetails->name;?>
          </h4>
      </div>
      <div class="col-sm-12">
          <div class="hr-line mb-4"></div>
      </div>
  </div>  
</div>
<div class="row mt-3">  
    <div class="col-sm-6">
        <div class="panel panel-grey">
            <div class="panel-heading">
                <h2>
                    <label class="checkbox-inline icheck">
                      Institutional Categories
                    </label>
                </h2>

                <div class="panel-ctrls button-icon-bg">
                </div>
            </div>          
            <div class="col-md-12 tag-lib-container" id="institute_cat_section">
            </div>           
        </div>
    </div>
    <div class="col-sm-6">
        <div class="panel panel-grey">
            <div class="panel-heading">
                <h2>
                    <label class="checkbox-inline icheck">Institutional Tags</label>
                </h2>
                 <div class="panel-ctrls button-icon-bg">
                </div>
            </div>
            <div class="panel-body" id="institute_default_section">
                
            </div>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-sm-6">
        <div class="panel panel-grey">
            <div class="panel-heading">
                <h2>
                    <label class="checkbox-inline icheck">
                        <input type="checkbox" id="institutional_sys_cat" value="option1"> System Categories
                    </label>
                </h2>
                <div class="panel-ctrls button-icon-bg"></div>
        </div>
        <div class="col-md-12 tag-lib-container" id="system_cat_section">
        </div>
    </div>
</div>

    <div class="col-sm-6">
        <div class="panel panel-grey">
            <div class="panel-heading">
                    <h2>
                        <label class="checkbox-inline icheck">
                           System Tags
                        </label>
                    </h2>
                    <div class="panel-ctrls button-icon-bg">
                </div>
            </div>
            <div class="panel-body" id="institute_system_section">
                
            </div>
        </div>
    </div>
</div>
<div class="col-sm-12">
    <div class="hr-line mb-4"></div>
</div>
<div class="row">
    <div class="col-sm-12 text-right mt-3 mb-4">
        <button type="button" class="btn btn-danger back_to_course scroll-top">Back</button>
        <button type="button" class="btn btn-primary next_to_user scroll-top"><?php echo ($this->session->userdata('action') == 'add')?'Save & Next':'Save Changes' ?></button>
    </div>
</div>


<script type="text/javascript" src="<?php bs('public/assets/js/container/school-metadata.js') ?>"></script>

<?php

$jsVars = [
    'ajax_call_root' => base_url(),
    'containerID'    => $siteID
];    
?>
<script>
    jQuery(document).ready(function() { 
        SchoolMetadata.init(<?php echo json_encode($jsVars); ?>);
    });
    $(".scroll-top").on("click", function(){
      $('body,html').animate({
          scrollTop: 0
      }, 500);
    })     
</script>
