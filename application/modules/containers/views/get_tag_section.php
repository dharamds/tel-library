<div class="tag-lib-container">
  <div class="tag-lib-input">
    <input type="text" name="tag" value="" placeholder="Tag" id="<?php echo $this->input->post('inputEle') ?>" class=" form-control"/>
  </div>
  <div class="<?php echo $this->input->post('inputEle') ?> tag-lib-panel"></div>
</div>

<script type="text/javascript" src="<?php bs('public/assets/js/container/tag_section.js') ?>"></script>
<?php
$jsVars = [
    'ajax_call_root' => base_url(),
    'post_data'      => $this->input->post()
];    
?>

<script>
  jQuery(document).ready(function() { 
      TagSection.init(<?php echo json_encode($jsVars); ?>);
  });
</script>    