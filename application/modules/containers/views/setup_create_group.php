<div class="row">
  <div class="col-sm-12 p-0 themePanel">
      <div class="col-sm-12 icon-left pt-3 pb-4">
          <h4 class="text-dark p-0 block-heading">
              <i class="icon flaticon-penitentiary mr-3"></i> Harvard University
          </h4>
      </div>
      <div class="col-sm-12">
          <div class="hr-line mb-4"></div>
      </div>
  </div>  
</div>
<div class="col-sm-12 pl-2">
  <div class="themePanel icon-left mb-3 pl-5">
    <h4 class="block-heading">
      <i class="flaticon-network-1 ficon icon"></i> Create New Group
    </h4>
    <div class="row pb-3">
        <div class="ist-visual-info col-sm-12 pt-2">
          <div class="col-md-4">
            <select id="e1" class="populate">
              <option>Astronomy</option>
              <option>Biology</option>
              <option>Biomedical Sciences</option>
              <option>Chemistry</option>
              <option>Earth Sciences</option>
              <option>Environmental Sciences</option>
              <option>Food Science and Technology</option>
              <option>General Sciences</option>
              <option>Life Sciences</option>
              <option>Materials Sciences</option>
              <option>Mathematics</option>
              <option>Physical Geography</option>
              <option>Physics</option>
              <option>Sports Science</option>
            </select>
          </div>
          <div class="col-md-4">
            <select id="e2" class="populate">
              <option>Section 001</option>
              <option>Section 002</option>
              <option>Section 003</option>
              <option>Section 004</option>
              <option>Section 005</option>
              <option>Section 006</option>
              <option>Section 007</option>
              <option>Section 008</option>
              <option>Section 009</option>
              <option>Section 010</option>
              <option>Section 011</option>
              <option>Section 012</option>
              <option>Section 013</option>
              <option>Section 014</option>
              <option>Section 015</option>
            </select>
          </div>    
          <div class="col-md-7 pt-4 mt-2">
            <div class="form-group">
              <input type="text" placeholder="Enter Group Name" class="form-control">
            </div>
          </div>
          <div class="col-md-12 pt-4 mt-2">
            <div class="form-group">
              <label class="control-label">Group availability</label>
              <div class="col-xs-12 p-0">
                <label for="hidegroup" class="f14 f500 text-dark btn btn-link">
                  <input type="radio" name="hideshowgroup" id="hidegroup">
                  Hide Group and Group Tools from Students
                </label>
              </div>  
              <div class="col-xs-12 p-0">  
                <label for="showgroup" class="f14 f500 text-dark btn btn-link">
                  <input type="radio" name="hideshowgroup" id="showgroup">
                  Show Group and Group Tools to Students
                </label>
              </div>
            </div>
          </div>
        </div>

        <!-----//  Group Leader Section ------->
        <div class="col-sm-12 pt-2 mt-5">
          <div class="card">
            <div class="card-header">
              <h2>Group Leaders</h2>
            </div>
            <div class="card-body">
              <div class="col-sm-12 p-0 pt-5">
                  <div class="ms-container ms-multiple-select-list">
                      <div class="ms-selectable themePanel">
                          <h5 class="block-sub-heading">All Group Leaders and Instructors</h5>
                          <input type="text" class="form-control mb-4 search_course_section_std" autocomplete="off" placeholder="Search name"  >
                          <ul class="ms-list">
                            <li class="ms-elem-selectable">
                                <span>Sarah W. Spurlock</span>
                                <div>sarah@email.com</div>
                                <a href="javascript:void(0)" class="btn btn-primary btn-xs">
                                    <i class="flaticon-swap-horizontal-orientation-arrows"></i>    
                                </a>    
                            </li>
                            <li class="ms-elem-selectable">
                                <span>Allison N. Martin</span>
                                <div>martin@email.com</div>
                                <a href="javascript:void(0)" class="btn btn-primary btn-xs">
                                    <i class="flaticon-swap-horizontal-orientation-arrows"></i>    
                                </a>    
                            </li>
                        </ul>
                      </div>
                      <div class="ms-selection ms-arrow themePanel">
                          <h5 class="block-sub-heading">Group Leaders </h5>
                          <input type="text" class="form-control mb-4 search_enroll_user" autocomplete="off" placeholder="Search name">
                           <ul class="ms-list">
                            <li class="ms-elem-selection">
                                <span>Sarah W. Spurlock</span>
                                <div>sarah@email.com</div>
                                <a href="javascript:void(0)" class="btn btn-danger btn-xs">
                                    <i class="ti ti-trash"></i>    
                                </a>    
                            </li>
                            <li class="ms-elem-selection">
                                <span>Allison N. Martin</span>
                                <div>martin@email.com</div>
                                <a href="javascript:void(0)" class="btn btn-danger btn-xs">
                                    <i class="ti ti-trash"></i>    
                                </a>    
                            </li>
                        </ul>
                      </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
        <!-----//  Enroll New Group Members Section ------->
        <div class="col-sm-12 pt-2 mt-5 mb-5">
          <div class="card">
            <div class="card-header">
              <h2>Enroll New Group Members</h2>
            </div>
            <div class="card-body">
              <div class="col-sm-12 p-0 pt-5">
                  <div class="ms-container ms-multiple-select-list">
                      <div class="ms-selectable themePanel">
                          <h5 class="block-sub-heading">All Institutional Users</h5>
                          <input type="text" class="form-control mb-4 search_course_section_std" autocomplete="off" placeholder="Search name"  >
                          <ul class="ms-list">
                            <li class="ms-elem-selectable">
                                <span>Sarah W. Spurlock</span>
                                <div>sarah@email.com</div>
                                <a href="javascript:void(0)" class="btn btn-primary btn-xs">
                                    <i class="flaticon-swap-horizontal-orientation-arrows"></i>    
                                </a>    
                            </li>
                            <li class="ms-elem-selectable">
                                <span>Allison N. Martin</span>
                                <div>martin@email.com</div>
                                <a href="javascript:void(0)" class="btn btn-primary btn-xs">
                                    <i class="flaticon-swap-horizontal-orientation-arrows"></i>    
                                </a>    
                            </li>
                        </ul>
                      </div>
                      <div class="ms-selection ms-arrow themePanel">
                          <h5 class="block-sub-heading">Enrolled Users</h5>
                          <input type="text" class="form-control mb-4 search_enroll_user" autocomplete="off" placeholder="Search name">
                           <ul class="ms-list">
                            <li class="ms-elem-selection">
                                <span>Sarah W. Spurlock</span>
                                <div>sarah@email.com</div>
                                <a href="javascript:void(0)" class="btn btn-danger btn-xs">
                                    <i class="ti ti-trash"></i>    
                                </a>    
                            </li>
                            <li class="ms-elem-selection">
                                <span>Allison N. Martin</span>
                                <div>martin@email.com</div>
                                <a href="javascript:void(0)" class="btn btn-danger btn-xs">
                                    <i class="ti ti-trash"></i>    
                                </a>    
                            </li>
                        </ul>
                      </div>
                  </div>
              </div>
            </div>
          </div>
        </div>





      </div>
      <div class="col-sm-12 text-right p-0">
          <button type="submit" class="btn btn-danger">Cancel</button>
          <button type="submit" class="btn btn-primary">Add Users</button>
      </div>
  </div>
</div>
<script type="text/javascript" src="<?= bs('public/assets/plugins/form-select2/select2.min.js') ?>"></script>  
<script type="text/javascript">
  $("#e1, #e2").select2({width: '100%'});
  $('#groupListTable').DataTable();
  $('.dataTables_filter input').attr('placeholder','Search...');
    //DOM Manipulation to move datatable elements integrate to panel
  $('.panel-ctrls').append($('.dataTables_filter').addClass("pull-right")).find("label").addClass("panel-ctrls-center");
  $('.panel-ctrls').append("<i class='separator'></i>");
  $('.panel-ctrls').append($('.dataTables_length').addClass("pull-left")).find("label").addClass("panel-ctrls-center");

  $('.panel-footer').append($(".dataTable+.row"));
  $('.dataTables_paginate>ul.pagination').addClass("pull-right m-n");
</script>