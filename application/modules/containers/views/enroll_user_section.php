<div class="col-xs-12 p-0">
    <div class="hr-line mb-5"></div>
</div>                
<div class="col-sm-12 p-0">
    <h4>
        Enroll Users
    </h4>
    <div class="ms-container ms-multiple-select-list">
        <div class="ms-selectable themePanel">
            <h5 class="block-sub-heading">All Institutional Users</h5>
            <input type="text" class="form-control mb-4 search_course_section_std" autocomplete="off" placeholder="Search name"  >
            <div id="section_users_select">
            </div>
        </div>
        <div class="ms-selection ms-arrow themePanel">
            <h5 class="block-sub-heading">Students</h5>
            <input type="text" class="form-control mb-4 search_enroll_user" autocomplete="off" placeholder="Search name">
             <div id="section_enroll_users_select">
            </div>
        </div>
        
    </div>    
</div>
<div class="col-xs-12 p-0">
    <div class="hr-line mb-5 mt-5"></div>
</div> 

<div class="col-sm-12 p-0">
    <h4>
        Add Instructor
    </h4>
    <div class="ms-container ms-multiple-select-list">
        <div class="ms-selectable themePanel">
            <h5 class="block-sub-heading">Instructor</h5>
            <input type="text" class="form-control mb-4 search_course_section_std_for_instructor" autocomplete="off" placeholder="Search name"  >
            <div id="section_users_select_for_instructor">
            </div>
        </div>
        <div class="ms-selection ms-arrow themePanel">
            <h5 class="block-sub-heading">Course Instructors</h5>
            <input type="text" class="form-control mb-4 search_instructor_user" autocomplete="off" placeholder="Search name">
             <div id="section_instructor_select">
            </div>
        </div>
    </div>    
</div>
<div class="col-xs-12 p-0">
    <div class="hr-line mb-5 mt-5"></div>
</div> 
<div class="col-sm-12 p-0">
    <h4>Enrolled User list</h4> 
    <div class="col-xs-12 p-0 mt-3">
        <table id="memListTable" class="table table-custome ui-checkbox-container" cellspacing="0">
            <colgroup>
              <col width="8%" />
              <col width="35%" />
              <col width="29%" />
              <col  />
          </colgroup>
          <thead>
              <tr>
                <th>
                    <label for="sellectAll" class="checkbox icheck">
                        <input type="checkbox" name="sellectAll" id="sellectAll" class="mr-2 users_select_all"> All
                    </label>
                </th>
                <th>Student Name</th>
                <th>Institutional Email</th>
                <th>User Role</th>
            </tr>
       </thead>
   </table>

        <div class="col-xs-12 p-0">
            <button class="btn btn-dark ml-4" type="button" id="bulkunenroll">Un-enroll Users</button>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php bs('public/assets/js/container/section-details/section-users.js') ?>"></script>
<?php
    $jsVars = [
        'ajax_call_root' => base_url(),
        'siteID'         => $siteID 
    ];    
?>
<script>
    jQuery(document).ready(function() { 
        SectionDetailsUsers.init(<?php echo json_encode($jsVars); ?>);
    });
    $('input[type="radio"], input[type="checkbox"]').iCheck({
      labelHover: false,
      cursor: true
    }); 
</script>