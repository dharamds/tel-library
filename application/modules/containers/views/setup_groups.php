<div class="row">
  <div class="col-sm-12 p-0 themePanel">
      <div class="col-sm-12 icon-left pt-3 pb-4">
          <h4 class="text-dark p-0 block-heading">
              <i class="icon flaticon-penitentiary mr-3"></i> <?php echo $containerDetails->name;?>
          </h4>
      </div>
      <div class="col-sm-12">
          <div class="hr-line mb-4"></div>
      </div>
  </div>  
</div>
<div class="col-sm-12 p-0 pl-2" id="groups_set_view">
    <div class="col-md-12 p-0">
       <div class="flex-row panel-form-control">
          <div class="flex-col">
             <?php echo form_dropdown('course_id', $courses, '', 'class="form-control group_filter" id="selected_course" '); ?>
          </div>
          <div class="flex-col-sm pl-0">
             <select id="section_id" name="section_id" class="form-control group_filter">
                <option value=""> --Section-- </option>
            </select>
          </div>
          <div class="flex-col-sm pl-0">
             <select id="group_leader" name="group_leader" class="form-control group_filter">
                <option value=""> --Group Leader-- </option>
             </select>
          </div>
          <div class="flex-col-sm-1 pl-0">
             <button class="btn btn-danger btn-block reset_group_filter">
                <span class="flaticon-close-1 f21 fw100"></span>
             </button>
          </div>
       </div>
    </div>
    <div class="col-sm-12 p-0 mt-4">
      <div class="hr-line mb-4"></div>
    </div>
    <div class="col-sm-12 p-0">
      <div class="flex-row">
        <div class="flex-col-auto pt-3">
           <a class="btn btn-primary panel-btn" href="javascript:void(0)" onclick="SchoolGroups.getOtherViewTemplate('groups',0,'<?php echo $siteID ?>')">
              <i class="flaticon-plus-button"></i> Add New Group
           </a>
            <button type="button" class="btn btn-danger panel-btn" id="remove_multiple" ><i class="fa fa-trash-o"></i> Delete Selected</button>
             <button type="button" class="btn btn-warning panel-btn" id="notify_to_users" ><i class="flaticon-ring"></i> Notify</button>
          <a class="btn btn-primary panel-btn"  href="javascript:void(0)" onclick="SchoolGroups.getOtherViewTemplate('groups_set_view',0,'<?php echo $siteID ?>')">
            <i class="flaticon-network-1"></i> Set of Groups
          </a>
        </div>
        <div class="flex-col panel-form-control">
           <div class="panel-ctrls"></div>
        </div>
      </div>
    </div>
    <div class="col-sm-12 p-0 data_tables_custome mb-3">
        <table class="table table-bordered" id="group_list_table">
            <thead>
                <tr>
                    <th style="min-width: 50px;">
                        <label for="select_all" class="checkbox-tel">
                            <input type="checkbox" name="select_all_group" id="select_all_group" class="mr-2 group_select_all"> 
                        </label>
                    </th>
                    <th style="min-width: 95px;" class="text-center">Action</th>
                    <th style="min-width: 100px;">Status</th>
                    <th style="min-width: 180px;">Group Name</th>
                    <th style="min-width: 220px;">Associated Course</th>
                    <th style="min-width: 220px;">Associated Section</th>
                    <th style="min-width: 220px;">Visible to Students?</th>
                    <th style="min-width: 200px;">Number Enrolled</th>
                    <th style="min-width: 200px;">Group Leaders</th>
                    
                </tr>
            </thead>
        </table>
    </div>
    <div class="col-sm-12 p-0 mt-4">
      <div class="hr-line mb-4"></div>
    </div>
    <div class="col-sm-12 text-right p-0 mt-3 mb-4">
        <button type="button" class="btn btn-danger back_to_user scroll-top">Back</button>
        <?php if($this->container_id == 0){ 
          if($this->session->userdata('action') == 'add'){
          ?>
        <button type="button" class="btn btn-primary cancel_request">Save & Add</button>

      <?php }else{?>
        <button type="button" class="btn btn-primary save_change_finish">Save Changes</button>
        
      <?php } } ?>
    </div> 
</div>

<script type="text/javascript" src="<?php bs('public/assets/js/container/school_groups.js') ?>"></script>
<?php
$jsVars = [
   'ajax_call_root' => base_url(),
   'siteID' => $siteID
];
?>
<script>
  jQuery(document).ready(function() {
    SchoolGroups.init(<?php echo json_encode($jsVars); ?>);
  });
  $(".scroll-top").on("click", function(){
    $('body,html').animate({
        scrollTop: 0
    }, 500);
  })    
</script>

<!--//  Modal of Set of Groups  //-->
<div class="modal fade" id="group_notification_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <div class="modal-loading-overlay" id="modal-loading" style="display: none">
              <div class="flex-row center-content-panel">
                <div class="flex-col">
                  <div class="loading-section">
                      <div class="page-loading-box">
                          <span class="page-loader-quart"></span> Sending...
                      </div> 
                  </div>
                </div>
              </div>
            </div>

            <div class="modal-header">
                <button type="button" class="close close_modal" ><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-share-square" aria-hidden="true"></i> Send
                Email</h4>
            </div>
            <form method="post" action="#" id="group_notification" name="group_notification">
                <div class="modal-body">
                    <div class="form-role">
                        <label>Subject</label>
                        <input type="text" class="form-control"  name="subject" placeholder="Subject" required>
                    </div> 
                    <div class="form-role">
                        <label>Message</label>
                        <textarea name="msg" class="form-control" rows="10" id="msg" cols="10" placeholder="Write You Message" required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default close_modal" >Close</button>
                    <button type="submit" class="btn btn-primary" id="email_submit_button" ><i class="fa fa-paper-plane" aria-hidden="true"></i>
                    Send</button>
                </div>
            </form>
        </div>
    </div>
</div>