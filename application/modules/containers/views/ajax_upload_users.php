<div id="ajax_upload_message" style="display: none">
	<div class="alert alert-success mb-4" >
		<span>Your file has been successfully uploaded.</span>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		  <i aria-hidden="true">&times;</i>
		</button>
	</div>
</div>
<div style="position: relative;padding: 5px 0 0;" class="col-md-12 p-0">
	<div class="col-sm-9 progress-absolute" id="ajax_file_loader" style="display: none">
		<div class="progress1">
			<div class="loader-box">
				<span class="loader loader-quart"></span> Please do not refresh or reload page while file uploading.
			</div>
		</div>
	</div>
<?php echo form_open('containers/schools/add_new',['id'=>'userbulkupload','name'=>'userbulkupload','enctype' => 'multipart/form-data']); ?>
<input type="hidden" name="upload_loc" value="<?php echo ($uploadLocation)?$uploadLocation:0 ?>" id="upload_loc">
<?php if( (int)$siteID):?>
							<?php echo form_input([ 
									'name' => 'container_id',
									'id' => 'container_id',
									'type' => 'hidden',
									'class' => 'form-control',
									'value' => ($siteID) ? $siteID : 0
							]);?>
							<?php echo form_input([ 
									'name' => 'course_id',
									'id' => 'course_id',
										'type' => 'hidden',
									'class' => 'form-control',
									'value' => ($this->session->userdata('courseID')) ? $this->session->userdata('courseID') : 0
							]);?>


							<?php echo form_input([ 
									'name' => 'section_id',
									'id' => 'section_id',
										'type' => 'hidden',
									'class' => 'form-control',
									'value' => ($this->session->userdata('sectionID')) ? $this->session->userdata('sectionID') : 0
							]);
            ?>                
<?php else:?>
							<div class="flex-col-sm-12 p-0">
							<div class="form-group">
			        <label class="control-label col-md-3">Institutions:</label>
			        <div class="col-md-6">
									<?php echo form_dropdown('container_id', $institutions, '', 'class="selected_condition form-control" id="select_container" '); ?>
							</div>
							</div>
								<div style="clear:both;"></div>
							<div class="flex-col-sm-12 p-0">
							<div class="form-group">
			        <label class="control-label col-md-3">Courses:</label>
			        <div class="col-md-6">
									<select id="select_course" name="course_id" class="selected_condition form-control">
											<option value="0"> --Courses--</option>
									</select>
							</div>
							</div>
							<div style="clear:both;"></div>
							<div class="flex-col-sm-12 p-0">
							<div class="form-group">
			        <label class="control-label col-md-3">Section:</label>
			        <div class="col-md-6">
									<select id="select_section" name="section_id" class="selected_condition form-control">
											<option value="0"> --Section--</option>
									</select>
							</div>
							</div>
<?php endif;?>


<div class="istVisualContainer m-0 p-0">
		<div class="row m-0">
			
			<div style="clear:both;"></div>

			<div class="flex-col-sm-12 p-0">
			    <div class="form-group">
			        <label class="control-label col-md-3">User type:&nbsp;*</label>
			        <div class="col-md-6">
				        <?php
							echo form_dropdown('user_type', $roles, STUDENT, ['id' => 'user_type', 'class' => 'form-control']);
						?>
					</div>
			    </div>
				</div>
				<div style="clear:both;"></div>

				

			<div class="col-sm-12 p-0 mt-3 pt-2 mb-5" id="ajax_file_uploader">
				<div class="col-sm-3"></div>
				<div class="col-sm-9 p-0 mb-4">
					<div class="col-sm-5">
						<div class="fileinput fileinput-new" data-provides="fileinput">
							<span class="btn btn-default btn-file btn-md">
								<span class="fileinput-new">Choose File</span>
								<span class="fileinput-exists">Change</span>
								<input type="file" name="bulkupload" id="bulkupload">
							</span>
						</div>
					</div>
					<div class="col-sm-5">
						<input type="submit" name="upload" value="Upload" class="btn btn-primary btn-md btn-block">
					</div>
					<div class="col-md-12 mb-3 mt-3">
						<span class="alert alert-waring alert-light-warning">
							Note : This file must be a CSV or Excel formatted file.
						</span>
					</div>
				</div>
				<div class="col-xs-12 pt-2 pb-2 clearfix"></div>
				<div class="row clearfix m-0"></div>
				<div class="row m-0 bg-1 round-5">
					<div class="col-md-12 pt-4 pb-4 text-center">
						<div class="pb-4">
							<h5 class="m-0 f14 fw500">Sample CSV and Excel file example.</h5>
						</div>
						<a href="<?php bs('/uploads/samples/users_sample.csv'); ?>" target="_blank" class="btn btn-info btn-normal">
							Sample CSV file.
						</a>
						<a href="<?php bs('/uploads/samples/users_sample.xlsx'); ?>" target="_blank" class="btn btn-info btn-normal">
							Sample Excel file.
						</a>
					</div>
				</div>



		</div>
			</div>
		</div>
		
	</div>
</form>
</div>
<script type="text/javascript" src="<?php bs('public/assets/js/container/upload-users.js') ?>"></script>
<?php
    $jsVars = [
        'ajax_call_root' => base_url(),
        'siteID'         => $siteID,
        'callBackRequest' => $callBackRequest
    ];    
?>
<script>
    jQuery(document).ready(function() { 
        uploadUsers.init(<?php echo json_encode($jsVars); ?>);
    });

		$('#select_container').on('change', function () { 
	                $.ajax({
                    type: "POST",
                    url: "<?php bs('/users/get_db_cources'); ?>",
                    data: {"container_id": $(this).val()},
                    success: function (response) {
                        var options = '<option>--Course--</option>';
                        $.each(JSON.parse(response), function (key, value) {
                            options = options + "<option value=" + value.id + ">" + value.name + "</option>";
                        });
                        $("#select_course").html(options);
                    }
                });
            });
		    //load sections 
				$('#select_course').on('change', function () {
                $.ajax({
                    type: "POST",
                    url: "<?php bs('/users/get_db_sections'); ?>",
                    data: {"container_id": $('#select_container option:selected').val(), "course_id": $(this).val()},
                    success: function (response) {
                        var options = '<option>--Section--</option>';
                        $.each(JSON.parse(response), function (key, value) {
                            options = options + "<option value=" + value.id + ">" + value.name + "</option>";
                        });
                        $("#select_section").html(options);
                    }
                });
            });				

</script>

