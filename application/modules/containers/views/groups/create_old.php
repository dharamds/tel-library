<div class="row">
  <div class="col-sm-12 p-0 themePanel">
      <div class="col-sm-12 icon-left pt-3 pb-4">
          <h4 class="text-dark p-0 block-heading">
              <i class="icon flaticon-penitentiary mr-3"></i> Harvard University
          </h4>
      </div>
      <div class="col-sm-12">
          <div class="hr-line mb-4"></div>
      </div>
  </div>  
</div>
<div class="col-sm-12 p-0 pl-2">
    <h4 class="text-dark p-0 block-heading">
        <i class="icon flaticon-penitentiary mr-3"></i> Create new group
    </h4>
    <div class="col-sm-12 p-0 pl-2"> <br>
        <div class="col-sm-6">
            <select class="form-control">
                <option>Course</option>
            </select>
        </div>
       <div class="col-sm-6">
            <select class="form-control">
                <option>Course</option>
            </select>
        </div>
        <br /><br />
        <div class="col-sm-6">
            <input type="text" name="group_name" class="form-control">
        </div>
        <br/><br/>
        <div class="col-sm-6">
            <input type="radio" name="group_avaibility" value="1"> Hide Group and Group Tools from Students <br/>
            <input type="radio" name="group_avaibility" value="2"> Show Group and Group Tools from Students
        </div>
        <br /><br />   
        <br />      
        <div class="col-sm-12">
            Group Leaders
        </div>    
        <br /><br />   
        <br />      
        <div class="col-sm-12">
            Enroll new group members 
        </div>       

    </div>    
</div>  