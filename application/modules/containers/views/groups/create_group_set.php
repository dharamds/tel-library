<link type="text/css" href="<?= bs('public/assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css')?>" rel="stylesheet">
<div class="col-sm-12 p-0 pl-2">
    <h4 class="p-0 block-heading">
        <i class="flaticon-network-1 mr-3"></i> Add new group set
    </h4>
    <?php echo form_open('containers/schools/add_new',['id'=>'setupschoolgroup','name'=>'setupschoolgroup']); ?>
    <div class="col-sm-12 pt-5 pb-3 ">
        <div class="col-sm-4">
            <?php echo form_dropdown('course_id', $courses, (isset($groupData->course_id)?$groupData->course_id:''), 'class="selected_condition form-control" id="selected_course" '); ?>
        </div>
        <div class="col-sm-4">
            <select id="group_section_id" name="group_section_id" class="selected_condition form-control">
                <option value=""> --Section-- </option>
            </select>
        </div>
    </div> 
    <div class="col-sm-12 pt-3 ">   
        <div class="col-sm-6">
            <?php echo form_input([ 
                              'name' => 'group_name',
                              'id' => 'group_name',
                              'class' => 'form-control',
                              'placeholder' => 'Enter new group set name',
                              'value' => (isset($groupData->name) ? $groupData->name : '')
            ]);?>
        </div>

        <div class="col-sm-12 pt-4 pb-4">
           <h5 class="mb-2">Group Avaibility</h5>
            <div class="col-xs-12 p-0">
              <label class="radio-tel col-xs-12 p-0 pb-3">
                <input type="radio" name="group_avaibility" value="0" class="t2 mr-2"> Hide Group and Group Tools from Students
              </label>
              <label class="radio-tel col-xs-12 p-0">
                <input type="radio" name="group_avaibility" value="1" class="t2 mr-2"> 
                Show Group and Group Tools to Students
              </label>
            </div>
        </div>
        <div class="col-sm-12 pt-4 pb-4 row m-0">
            <h5 class="mb-2">Populate Group</h5>
            <div class="col-xs-12 p-0 pb-4">
              <div class="col-xs-12 p-0 pt-2 pb-2">
                Split students into <div class="d-inline-block pl-2 pr-2 mb--10"><input type="number" name="number_of_group" id="number_of_group" value="1" min="1" max="4" class="touchspin4"></div> number of groups
              </div>
            </div>
            <div class="col-xs-12 p-0 pb-4">
              <div class="col-xs-12 p-0 pt-2 pb-2">
                Split students into groups of
                <div class="d-inline-block pl-2 pr-2 mb--10"><input type="number" name="number_of_student" id="number_of_student" value="1"  min="1" max="10" class="touchspin4"></div> number of students
              </div>
            </div>
        </div>   
    </div>
    <div class="col-sm-12">
        <div class="hr-line mb-5"></div>
    </div>
    <div class="row">
        <div class="col-sm-12 text-right pb-4">
            <button type="button" class="btn btn-danger back_to_group_list scroll-top">Cancel & back</button>
            <button type="button" class="btn btn-primary create_group_set_popup" >Create</button>
        </div>
    </div>
    </form>
    <div class="col-sm-12"  id="enroll_users_section">
    </div>
    
</div> 
<script type="text/javascript" src="<?= bs('public/assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js') ?>"></script>
<script type="text/javascript" src="<?php bs('public/assets/js/container/groups/school_groups_set_create.js') ?>"></script>
<?php

$jsVars = [
   'ajax_call_root' => base_url(),
   'siteID'         => $siteID,
   'recordID'       => $recordID,
   'groupData'      => $groupData
];
?>
<script>
  $(".scroll-top").on("click", function(){
    $('body,html').animate({
        scrollTop: 0
    }, 500);
  })    
   jQuery(document).ready(function() {
      SchoolGroupsSetCreate.init(<?php echo json_encode($jsVars); ?>);
   });
 
  $("input.touchspin4").TouchSpin({
    verticalbuttons: true,
    step: 1,
    min : 0,
    max : 100
  }); 
   
</script> 



<div class="modal fade modals modals-tel-theme" id="setOfGrpupsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog w80p">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h2 class="modal-title">New Group Sets</h2>
        </div>
        <div class="modal-body" id="popup_model_body">
          
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->