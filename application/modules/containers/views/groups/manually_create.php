<div class="row">
    <div class="col-sm-12 p-0 themePanel">
        <div class="col-sm-12 icon-left pt-3 pb-4">
            <h4 class="text-dark p-0 block-heading">
                <i class="icon flaticon-penitentiary mr-3"></i> <?php echo $containerDetails->name;?>
            </h4>
        </div>
        <div class="col-sm-12">
            <div class="hr-line mb-4"></div>
        </div>
    </div>  
</div>

<div class="col-sm-12 p-0" >
    <h4 class="p-0 block-heading mb-5 mt-2">
        <i class="icon flaticon-penitentiary mr-3 "></i> Create new group
    </h4>
    <?php echo form_open('containers/schools/add_new',['id'=>'setupschoolgroup','name'=>'setupschoolgroup']); ?>
    <?php echo form_input([ 
                              'name' => 'submit_type',
                              'id' => 'submit_type',
                              'type' => 'hidden',
                              'value' => (isset($groupData->id) ? 'save_enroll' : '')
            ]);?>
     <?php echo form_input([ 
                              'name' => 'group_id',
                              'id' => 'group_id',
                              'type' => 'hidden',
                              'value' => (isset($groupData->id) ? $groupData->id : '')
            ]);?>        
    <div class="col-sm-12 p-0 pl-2">
        <div class="col-sm-4">
            <?php echo form_dropdown('course_id', $courses, (isset($groupData->course_id)?$groupData->course_id:''), 'class="selected_condition form-control" id="selected_course" '); ?>
        </div>
        <div class="col-sm-4">
            <select id="group_section_id" name="group_section_id" class="selected_condition form-control">
                <option value=""> --Section-- </option>
            </select>
        </div>
        <div class="col-xs-12 p-0 pt-4 mt-3 mb-5">
          <div class="col-sm-7">
              <?php echo form_input([ 
                                'name' => 'group_name',
                                'id' => 'group_name',
                                'class' => 'form-control',
                                'placeholder' => 'Enter group name',
                                'value' => (isset($groupData->name) ? $groupData->name : '')
              ]);?>
          </div>
        </div>
        <div class="col-sm-12 pb-5 radio-error">
            <div class="col-xs-12 p-0">
              <label class="radio-tel m-0 pb-3">
                <input type="radio" name="group_avaibility" value="0" <?php echo (isset($groupData->display_avaibility) && $groupData->display_avaibility == 0 ? 'checked': '')?>> Hide Group and Group Tools from Students 
              </label>
            </div>
            <div class="col-xs-12 p-0 m-0">
              <label class="radio-tel mb-0">
                <input type="radio" name="group_avaibility" value="1" <?php echo (isset($groupData->display_avaibility) && $groupData->display_avaibility == 1 ? 'checked': '')?>> Show Group and Group Tools to Students
              </label>
          </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-sm-12">
            <div class="col-md-6">
              <button type="button" class="btn btn-success save_enroll" ><?php echo (isset($groupData->id) ? 'Update & Enroll Users' : 'Save & Enroll Users')?></button>
            </div>
            <div class="col-md-6 text-right">
              
              <button type="button" class="btn btn-danger back_to_group_list scroll-top">Cancel & Back</button>
              <button type="button" class="btn btn-primary save_add_new" ><?php echo (isset($groupData->id) ? 'Update & Add New' : 'Save & Add New')?> </button>  
            </div>
            
            
        </div>
    </div>
    </form>
    <div class="col-sm-12 p-0 mt-4"  id="enroll_users_section">
    </div>
    <div class="col-sm-12 p-0"  id="enroll_users_listing">
    </div>
</div> 

<script type="text/javascript" src="<?php bs('public/assets/js/container/groups/school_groups_create.js') ?>"></script>
<?php

$jsVars = [
   'ajax_call_root' => base_url(),
   'siteID'         => $siteID,
   'recordID'       => $recordID,
   'groupData'      => $groupData
];
?>
<script>
   jQuery(document).ready(function() {
      SchoolGroupsCreate.init(<?php echo json_encode($jsVars); ?>);
   });
  $(".scroll-top").on("click", function(){
    $('body,html').animate({
        scrollTop: 0
    }, 500);
  }) 
</script> 