<input type="hidden" name="search_text_box" id="search_text_box">
<div class="col-sm-12 collapse-panel">
    <h4>Group Leaders</h4>
    <div class="ms-container ms-multiple-select-list pl-5 pt-3">
        <div class="ms-selectable themePanel">
            <h5 class="block-sub-heading">All Institutional Users</h5>
            <input type="text" class="form-control mb-4 search_site_users" autocomplete="off" placeholder="Search name"  >

            <div id="site_enroll_users" data-for="1">
            </div>
        </div>
        <div class="ms-selection ms-arrow themePanel">
            <h5 class="block-sub-heading">Group Leaders</h5>
            <input type="text" class="form-control mb-4 search_groupleader_users" autocomplete="off" placeholder="Search name">
            <div id="site_group_leaders" data-for="0">
            </div>
        </div>             
    </div>
</div> 

<div class="col-sm-12 mt-5 collapse-panel mb-5">
    <h4>Enroll New Group Members</h4>
    <div class="ms-container ms-multiple-select-list pl-5 pt-3">
        <div class="ms-selectable themePanel">
            <h5 class="block-sub-heading">All Institutional Users</h5>
            <input type="text" class="form-control mb-4 search_site_users_secondery" autocomplete="off" placeholder="Search name"  >
            <div id="site_enroll_users_secondary"  data-for="2">
            </div>
        </div>
        <div class="ms-selection ms-arrow themePanel">
            <h5 class="block-sub-heading">Enroll Users</h5>
            <input type="text" class="form-control mb-4 search_group_users" autocomplete="off" placeholder="Search name">
            <div id="site_group_users" data-for="3">
            </div>
        </div>             
    </div>
</div> 
<script type="text/javascript" src="<?php bs('public/assets/js/container/groups/school_groups_user_enroll_section.js') ?>"></script>
<?php

$jsVars = [
   'ajax_call_root' => base_url(),
   'siteID' => $siteID,
   'courseID' => $courseID,
   'sectionID' => $sectionID,
   'groupID'   => $groupID
];
?>
<script>
   jQuery(document).ready(function() {
      SchoolGroupsUserEnrollSection.init(<?php echo json_encode($jsVars); ?>);
   });
</script> 