<div class="col-sm-12 p-0">
    <div class="hr-line mb-4"></div>
</div>

<div class="col-sm-12 p-0">
      <h4 class="mb-5">Enrolled Group Members</h4> 
      <div class="col-xs-12 p-0">
          <table id="memListTable" class="table ui-checkbox-container" cellspacing="0">
              <colgroup>
                <col width="8%" />
                <col width="35%" />
                <col width="29%" />
                <col  />
            </colgroup>
            <thead>
                <tr>
                  <th>
                      <label for="sellectAll">
                          <input type="checkbox" name="sellectAll" id="sellectAll" class="mr-2 users_select_all"> All
                      </label>
                  </th>
                  <th>Student Name</th>
                  <th>Institutional Email</th>
                  <th>User Role</th>
              </tr>
            </thead>
        </table>
      <div class="col-sm-12 p-0 mt-3">
          <div class="hr-line mb-4"></div>
      </div>
      <div class="col-xs-12 p-0 mb-4">
          <button class="btn btn-dark" type="button">Move User To</button>
          <button class="btn btn-dark ml-4" type="button" id="bulkunenroll">Un-enroll Users</button>
          <button class="btn btn-dark ml-4" type="button" id="">Email or Message Users</button>
      </div>
    </div>
</div>

<script type="text/javascript" src="<?php bs('public/assets/js/container/groups/school_groups_user_enroll_listing.js') ?>"></script>
<?php

$jsVars = [
   'ajax_call_root' => base_url(),
   'siteID' => $siteID,
   'courseID' => $courseID,
   'sectionID' => $sectionID,
   'groupID'   => $groupID
];
?>
<script>
   jQuery(document).ready(function() {
      SchoolGroupsUserEnrollListing.init(<?php echo json_encode($jsVars); ?>);
   });
</script> 