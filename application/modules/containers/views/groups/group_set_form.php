<?php //pr($userList)?>
<?php echo form_open('containers/schools/add_new',['id'=>'setupofgroup','name'=>'setupofgroup','autocomplete'=>'off']); ?>
<input type="hidden" name="siteID" value="<?php echo $this->input->post('siteID')?>" class="form-control">
<input type="hidden" name="course_id" value="<?php echo $this->input->post('course_id')?>" class="form-control">
<input type="hidden" name="group_section_id" value="<?php echo $this->input->post('group_section_id')?>" class="form-control">
<input type="hidden" name="group_avaibility" value="<?php echo $this->input->post('group_avaibility')?>" class="form-control">
<div class="row pt-4 m-0 pl-4 pr-4">
    <?php 
    for ($i = 1; $i <= $this->input->post('number_of_group'); $i++) { //echo $i?>
    <div class="panel panel-default panel-set-groups mb-3">
        <div class="panel-heading cursor_p" data-toggle="collapse" data-target="#setOfGroup<?php echo $i?>">
            <h4>
                <i class="flaticon-network-1 mr-4 f22"></i> <?php echo $this->input->post('group_name').'-'.$i?>
                <input type="hidden" name="groups_name[<?=$i?>]" value="<?php echo $this->input->post('group_name').'-'.$i?>">
            </h4>
            <a class="panel-arrow" href="javascript:void(0)">
                <i class="flaticon-arrow-point-to-right"></i>
            </a>
        </div>
        <div class="panel-body collapse <?php echo ($i==1)? 'in':''?> pl-0 pr-0" id="setOfGroup<?php echo $i?>">
            <?php for ($j = 1; $j <= $this->input->post('number_of_student'); $j++) { /*echo $j;*/


                ?>
            <div class="row m-0">
                <div class="col-xs-12 col-md-1">
                    <i class="flaticon-contact-1 f32 mt-4 block pt-3"></i>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="form-group">
                        <label class="control-label">Student Name</label>
                        <input type="text" name="group[<?=$i?>][<?=$j?>][name]" value="<?=(isset($userList[0]['user_name'])?$userList[0]['user_name']:'')?>" id="std_name_<?=$i.$j?>" class="form-control student_name_search" data-email="std_email_<?=$i.$j?>" data-record-id="std_id_<?=$i.$j?>">
                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="form-group">
                        <label class="control-label">Student Email</label>
                        <input type="text" name="group[<?=$i?>][<?=$j?>][email]" value="<?=(isset($userList[0]['email'])?$userList[0]['email']:'')?>" id="std_email_<?=$i.$j?>" class="form-control student_email_search" data-name="std_name_<?=$i.$j?>" data-record-id="std_id_<?=$i.$j?>">
                    </div>
                </div>
                <input type="hidden" name="group[<?=$i?>][<?=$j?>][id]"  value="<?=(isset($userList[0]['id'])?$userList[0]['id']:'')?>" id="std_id_<?=$i.$j?>" class="form-control">
                <div class="col-xs-12 col-md-3 text-right">
                    <?php if($j > 1){ ?>
                    <a href="javascript:void(0)" class="f24 mt-4 block pt-3 c2">
                        <i class="flaticon-close-2 remove_row"></i>
                    </a>
                <?php } ?>
                </div>
                <div class="clearfix"></div>
                <div class="hr-line mb-3 mt-2"></div>
            </div>
            
        <?php 
        array_shift($userList);   

    } ?>
        </div>
    </div>
<?php  } ?>
</div>
<div class="row pt-4 m-0 pl-4 pr-4 mb-4">
    <button type="button" class="btn btn-danger btn-md" data-dismiss="modal">Cancel</button>
    <button type="submit" class="btn btn-primary btn-md submit_save_group_btn">Save Group Set</button>
</div>
</form>

<script type="text/javascript" src="<?php bs('public/assets/js/container/groups/group-set-create-form.js') ?>"></script>
<?php
$jsVars = [
    'ajax_call_root' => base_url(),
    'siteID'         => $this->input->post('siteID')
];    
?>
<script>
    jQuery(document).ready(function() { 
        GroupSetCreateForm.init(<?php echo json_encode($jsVars); ?>);
        /*$('[id^=std_name_]').each(function(e) {
            $(this).rules('add', {
                required: true,
                noSpace:true
            });
        });
        $('[id^=std_email_]').each(function(e) {
            $(this).rules('add', {
                required: true,
                emailExt:true,
                noSpace:true
            });
        });*/
    });
</script>
