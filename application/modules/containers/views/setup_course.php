<link type="text/css" href="<?php echo bs('public/assets/plugins/form-multiselect/css/multi-select.css')?>" rel="stylesheet">
<script type="text/javascript" src="<?php echo bs('public/assets/plugins/quicksearch/jquery.quicksearch.min.js') ?>"></script>  
<script type="text/javascript" src="<?php echo bs('public/assets/plugins/form-multiselect/js/jquery.multi-select.min.js') ?>"></script>
<div class="row">
  <div class="col-sm-12 p-0 themePanel">
      <div class="col-sm-12 icon-left pt-3 pb-4">
          <h4 class="text-dark p-0 block-heading">
              <i class="icon flaticon-penitentiary mr-3"></i> <?php echo $containerDetails->name;?>
          </h4>
      </div>
      <div class="col-sm-12">
          <div class="hr-line mb-4"></div>
      </div>
  </div>  
</div>
<div class="row">
  <?php  if($this->container_id == 0 ){?>
  <div class="col-sm-12 p-0 pt-3">
      <div class="themePanel col-sm-12 icon-left mb-0">
          <h4>
            <i class="flaticon-book ficon"></i> Select Course
          </h4>
          <div class="istVisualContainer col-md-12 p-0 pb-5">
            <div class="col-sm-12 row">
              <div class="form-group">                          
                <div class="col-sm-8 row">
                  <select multiple="multiple" id="multi-select">
                    <?php foreach ($courses as $key => $value) {?>
                        <option value="<?php echo $value->id ?>" <?php if(isset($courseAssigned) && $courseAssigned != ''): echo (in_array($value->id, $courseAssigned)? 'selected' : ''); endif; ?>><?php echo $value->name ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
            </div>
           </div>
        </div>
  </div>
 <?php } ?>
  <div class="themePanel col-sm-12 icon-left mb-0">
        <h4>
            <i class="ficon flaticon-web-design"></i> Add Section
        </h4>
        <div class="istVisualContainer col-md-12 p-0 pb-2 create_section_form">
        <?php echo form_open('containers/schools/add_new',['id'=>'setupcourse','name'=>'setupcourse']); ?>
         <?php echo form_input([ 
                        'name' => 'section_id',
                        'type' => 'hidden',
                        'id' => 'section_id',
                        'class' => 'form-control'
                      ]);?>
            <div class="col-sm-12 p-0 mb-0">
         <div class="themePanel p-0">
            <div class="flex-row m-0">
              <div class="flex-col-3 p-0">
                <div class="form-group colorPicker">
                   <label class="control-label">Section <span class="field-required">*</span></label>
                   <div class="input-group col-sm-12">
                    <?php echo form_input([ 
                        'name' => 'section',
                        'id' => 'section',
                        'class' => 'form-control'
                      ]);?>
                  </div>
                </div>
              </div>
              <div class="flex-col-3 p-0 pl-4">
                <div class="form-group colorPicker">
                   <label class="control-label">Courses <span class="field-required">*</span></label>
                   <div class="input-group col-sm-12">
                    <select class="form-control" name="course_section" id="course_section">
                      <option value="">Select course</option>
                      <?php foreach ($courseSelected as $key => $value) {
                          echo '<option value='.$value['id'].'>'.$value['name'].'</option>';
                      } ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="flex-col p-0 pl-4">
                <div class="form-group colorPicker111">
                  <label class="control-label">Start Date <span class="field-required">*</span></label>
                   <div class="input-group date11" >
                    <?php echo form_input([ 
                        'name' => 'section_start_date',
                        'id' => 'section_start_date',
                        'class' => 'form-control',
                        'autocomplete' => 'off'
                      ]);?>
                    <span class="input-group-addon" >
                      <span class="flaticon-calendar-1"></span>
                    </span>
                  </div>
                </div>
              </div>
              <div class="flex-col-2 p-0 pl-4">
                <div class="form-group colorPicker11">
                  <label class="control-label">End Date <span class="field-required">*</span></label>
                   <div class="input-group date1">
                    <?php echo form_input([ 
                        'name' => 'section_end_date',
                        'id' => 'section_end_date',
                        'class' => 'form-control',
                        'autocomplete' => 'off'
                      ]);?>
                    <span class="input-group-addon">
                      <span class="flaticon-calendar-1"></span>
                    </span>
                  </div>
                </div>
              </div>
              <div class="flex-col pt-4 p-0 pl-4">
                <button type="submit" class="btn btn-primary btn-block mt-3 section_submit_button" >Save</button>
              </div>
            </div>
            
          </div>
            </div>
        </form>
      </div>
  </div>
    <div class="themePanel col-sm-12 icon-left mb-0">
        <h4>
            <i class="ficon flaticon-web-design"></i> Section List
        </h4>
        <div id="section_list" class="table-skeleton-layout"></div>
    </div>    

</div>  

<div class="col-sm-12">
    <div class="hr-line mb-4"></div>
</div>
<div class="row">
    <div class="col-sm-12 text-right mt-3 mb-4">
        <button type="button" class="btn btn-danger back_to_visual scroll-top">Back</button>
        <button type="button" class="btn btn-primary course_next scroll-top" ><?php echo ($this->session->userdata('action') == 'add')?'Save & Next':'Save Changes' ?></button>
    </div>
</div>
<script type="text/javascript" src="<?php bs('public/assets/js/container/school-course.js') ?>"></script>
<?php
$jsVars = [
    'ajax_call_root' => base_url(),
    'siteID' => $siteID
];    
?>
<script>
    jQuery(document).ready(function() { 
        SchoolCourse.init(<?php echo json_encode($jsVars); ?>);
    });
    $(".scroll-top").on("click", function(){
      $('body,html').animate({
          scrollTop: 0
      }, 500);
    });  
</script>

