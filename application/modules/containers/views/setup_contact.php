<div class="row">
  <div class="col-sm-12 p-0 themePanel">
      <div class="col-sm-12 icon-left pt-3 pb-4">
          <h4 class="text-dark p-0 block-heading">
              <i class="icon flaticon-penitentiary mr-3"></i> <?php echo $containerDetails->name;?>
          </h4>
      </div>
      <div class="col-sm-12">
          <div class="hr-line mb-4"></div>
      </div>
  </div>  
</div>  
<!----// List Item ----->
<?php echo form_open('containers/schools/add_new',['id'=>'setupcontact','name'=>'setupcontact']); ?>
<div class="list-item icon-left mt-4">
    <div class="list-item-head">
        <span class="ficon flaticon-placeholder-1"></span>
        Institution Address
    </div>
    <div class="list-item-content">

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="control-label">Street 1 <span class="field-required">*</span></label>
                    <?php 
                    echo form_input([ 
                        'name' => "institution[institution_address][street_1]",
                        'class' => 'form-control',
                        'value' => (isset($siteContactData->street_1) ? $siteContactData->street_1 : '' )
                    ]);
                    ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="control-label">Street 2 </label>
                    <?php echo form_input([ 
                        'name' => "institution[institution_address][street_2]",
                        'class' => 'form-control',
                        'value' => (isset($siteContactData->street_2) ? $siteContactData->street_2 : '' )
                    ]);?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group">
                    <label class="control-label">State <span class="field-required">*</span></label>
                    <?php 
                    echo form_input([ 
                        'name' => "institution[institution_address][state]",
                        'class' => 'form-control states',
                        'value' => (isset($siteContactData->state) ? $siteContactData->state : '' )
                    ]);
                    ?>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label class="control-label">City <span class="field-required">*</span></label>
                    <?php 
                    echo form_input([ 
                        'name' => "institution[institution_address][city]",
                        'class' => 'form-control city',
                        'value' => (isset($siteContactData->city) ? $siteContactData->city : '' )
                    ]);
                    ?>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label class="control-label">Zip <span class="field-required">*</span></label>
                    <?php 
                    echo form_input([ 
                        'name' => "institution[institution_address][zip]",
                        'class' => 'form-control',
                        'value' => (isset($siteContactData->zip) ? $siteContactData->zip : '' )
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="hr-line mb-5"></div>
<!----// List Item ----->
<div class="list-item icon-left">
    <div class="list-item-head">
        <span class="ficon flaticon-contact"></span>
        Primary Contact
    </div>
    <div class="list-item-content">
        <div class="row">
            
            <div class="col-sm-3">
                <div class="form-group">
                    <label class="control-label">Name <span class="field-required">*</span></label>
                    <?php 
                    echo form_input([ 
                        'name' => "institution[primary][name]",
                        'class' => 'form-control',
                        'value' => (isset($contactData['primary']['name']) ? $contactData['primary']['name'] : '' )
                    ]);
                    ?>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label class="control-label">Title </label>

                    <?php 
                    echo form_input([ 
                        'name' => "institution[primary][title]",
                        'class' => 'form-control',
                        'value' => (isset($contactData['primary']['title']) ? $contactData['primary']['title'] : '' )
                    ]);
                    ?>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label class="control-label">Email <span class="field-required">*</span></label>
                    <?php 
                    echo form_input([ 
                        'name' => "institution[primary][email]",
                        'class' => 'form-control',
                        'value' => (isset($contactData['primary']['email']) ? $contactData['primary']['email'] : '' )
                    ]);
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group">
                    <label class="control-label">Primary Phone <span class="field-required">*</span></label>
                    <?php 
                    echo form_input([ 
                        'name' => "institution[primary][pri_phone]",
                        'class' => 'form-control usphone',
                        'value' => (isset($contactData['primary']['pri_phone']) ? $contactData['primary']['pri_phone'] : '' )
                    ]);
                    ?>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label class="control-label">Secondary Phone </label>
                    <?php 
                    echo form_input([ 
                        'name' => "institution[primary][sec_phone]",
                        'class' => 'form-control usphone',
                        'value' => (isset($contactData['primary']['sec_phone']) ? $contactData['primary']['sec_phone'] : '' )
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="hr-line mb-5"></div>
<!----// List Item ----->
<div class="list-item icon-left">
    <div class="list-item-head">
        <span class="ficon flaticon-contact"></span>
        Instructional Support Contact
        <div class="list-checkbox">
            <label>
                <?php 

                echo form_checkbox([ 
                    'name' => "institution[instructional_support][same_as_primary]",
                    'onclick' => "SchoolContact.getSelectSameAsPrimary('instructional_support', this)",
                    'value' => 1,
                    'checked' => (isset($contactData['instructional_support']['same_as_primary']) &&  $contactData['instructional_support']['same_as_primary'] == 1) ? 'checked' : '' 
                ]);
                ?>
                Same as Primary Contact
            </label>
        </div>
    </div>
    <div class="list-item-content" id="instructional_support" style="<?php echo (isset($contactData['instructional_support']['same_as_primary']) &&  $contactData['instructional_support']['same_as_primary'] == 1) ? 'display: none' : ''  ?>">
        <div class="row">
            
            <div class="col-sm-3">
                <div class="form-group">
                    <label class="control-label">Name </label>
                    <?php 
                    echo form_input([ 
                        'name' => "institution[instructional_support][name]",
                        'class' => 'form-control',
                        'value' => (isset($contactData['instructional_support']['name']) ? $contactData['instructional_support']['name'] : '' )
                    ]);
                    ?>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="form-group">
                    <label class="control-label">Title </label>
                   
                     <?php 
                    echo form_input([ 
                        'name' => "institution[instructional_support][title]",
                        'class' => 'form-control',
                        'value' => (isset($contactData['instructional_support']['title']) ? $contactData['instructional_support']['title'] : '' )
                    ]);
                    ?>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="form-group">
                    <label class="control-label">Email </label>
                    <?php 
                    echo form_input([ 
                        'name' => "institution[instructional_support][email]",
                        'class' => 'form-control',
                        'value' => (isset($contactData['instructional_support']['email']) ? $contactData['instructional_support']['email'] : '' )
                    ]);
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group">
                    <label class="control-label">Primary Phone </label>
                    <?php 
                    echo form_input([ 
                        'name' => "institution[instructional_support][pri_phone]",
                        'class' => 'form-control usphone',
                        'value' => (isset($contactData['instructional_support']['pri_phone']) ? $contactData['primary']['pri_phone'] : '' )
                    ]);
                    ?>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label class="control-label">Secondary Phone </label>
                    <?php 
                    echo form_input([ 
                        'name' => "institution[instructional_support][sec_phone]",
                        'class' => 'form-control usphone',
                        'value' => (isset($contactData['instructional_support']['sec_phone']) ? $contactData['instructional_support']['sec_phone'] : '' )
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="hr-line mb-5"></div>
<!----// List Item ----->
<div class="list-item icon-left">
    <div class="list-item-head">
        <span class="ficon flaticon-contact"></span>
        Technical Contact
        <div class="list-checkbox">
            <label>
                <?php 
                echo form_checkbox([ 
                    'name' => "institution[technical][same_as_primary]",
                    'onclick' => "SchoolContact.getSelectSameAsPrimary('technical', this)",
                    'value' => 1,
                    'checked' => (isset($contactData['technical']['same_as_primary']) &&  $contactData['technical']['same_as_primary'] == 1) ? 'checked' : '' 
                ]);
                ?>
                Same as Primary Contact
            </label>
        </div>
    </div>
    <div class="list-item-content" id="technical"  style="<?php echo (isset($contactData['technical']['same_as_primary']) &&  $contactData['technical']['same_as_primary'] == 1) ? 'display: none' : ''  ?>">
        <div class="row">
            
            <div class="col-sm-3">
                <div class="form-group">
                    <label class="control-label">Name </label>
                    <?php 
                    echo form_input([ 
                        'name' => "institution[technical][name]",
                        'class' => 'form-control',
                        'value' => (isset($contactData['technical']['name']) ? $contactData['technical']['name'] : '' )
                    ]);
                    ?>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label class="control-label">Title </label>
                    
                     <?php 
                    echo form_input([ 
                        'name' => "institution[technical][title]",
                        'class' => 'form-control',
                        'value' => (isset($contactData['technical']['title']) ? $contactData['technical']['title'] : '' )
                    ]);
                    ?>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label class="control-label">Email </label>
                    <?php 
                    echo form_input([ 
                        'name' => "institution[technical][email]",
                        'class' => 'form-control',
                        'value' => (isset($contactData['technical']['email']) ? $contactData['technical']['email'] : '' )
                    ]);
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group">
                    <label class="control-label">Primary Phone</label>
                    <?php 
                    echo form_input([ 
                        'name' => "institution[technical][pri_phone]",
                        'class' => 'form-control usphone',
                        'value' => (isset($contactData['technical']['pri_phone']) ? $contactData['technical']['pri_phone'] : '' )
                    ]);
                    ?>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label class="control-label">Secondary Phone</label>
                    <?php 
                    echo form_input([ 
                        'name' => "institution[technical][sec_phone]",
                        'class' => 'form-control usphone',
                        'value' => (isset($contactData['technical']['sec_phone']) ? $contactData['technical']['sec_phone'] : '' )
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="hr-line mb-5"></div>
<!----// List Item ----->
<div class="list-item icon-left">
    <div class="list-item-head">
        <span class="ficon flaticon-contact"></span>
        Billing Contact
        <div class="list-checkbox">
            <label>
                <?php 
                echo form_checkbox([ 
                    'name' => "institution[billing][same_as_primary]",
                    'onclick' => "SchoolContact.getSelectSameAsPrimary('billing', this)",
                    'value' => 1,
                    'checked' => (isset($contactData['billing']['same_as_primary']) && $contactData['billing']['same_as_primary'] == 1) ? 'checked' : ''
                ]);
                ?>
                Same as Primary Contact
            </label>
        </div>
    </div>
    <div class="list-item-content" id="billing" style="<?php echo (isset($contactData['billing']['same_as_primary']) &&  $contactData['billing']['same_as_primary'] == 1) ? 'display: none' : ''  ?>">
        <div class="row">
            
            <div class="col-sm-3">
                <div class="form-group">
                    <label class="control-label">Name </label>
                    <?php 
                    echo form_input([ 
                        'name' => "institution[billing][name]",
                        'class' => 'form-control',
                        'value' => (isset($contactData['billing']['name']) ? $contactData['billing']['name'] : '' )
                    ]);
                    ?>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label class="control-label">Title </label>

                     <?php 
                    echo form_input([ 
                        'name' => "institution[billing][title]",
                        'class' => 'form-control',
                        'value' => (isset($contactData['billing']['title']) ? $contactData['billing']['title'] : '' )
                    ]);
                    ?>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label class="control-label">Email </label>
                    <?php 
                    echo form_input([ 
                        'name' => "institution[billing][email]",
                        'class' => 'form-control',
                        'value' => (isset($contactData['billing']['email']) ? $contactData['billing']['email'] : '' )
                    ]);
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group">
                    <label class="control-label">Primary Phone </label>
                    <?php 
                    echo form_input([ 
                        'name' => "institution[billing][pri_phone]",
                        'class' => 'form-control usphone',
                        'value' => (isset($contactData['billing']['pri_phone']) ? $contactData['billing']['pri_phone'] : '' )
                    ]);
                    ?>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label class="control-label">Secondary Phone </label>
                    <?php 
                    echo form_input([ 
                        'name' => "institution[billing][sec_phone]",
                        'class' => 'form-control usphone',
                        'value' => (isset($contactData['billing']['sec_phone']) ? $contactData['billing']['sec_phone'] : '' )
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="hr-line mb-5"></div>
<!----// List Item ----->
<div class="list-item icon-left">
    <div class="list-item-head">
        <span class="ficon flaticon-contact"></span>
        Site Manager 1
        <div class="list-checkbox">
            <label>
                <?php 
                echo form_checkbox([ 
                    'name' => "institution[site_manager_1][same_as_primary]",
                    'onclick' => "SchoolContact.getSelectSameAsPrimary('site_manager_1', this)",
                     'value' => 1,
                     'checked' => (isset($contactData['site_manager_1']['same_as_primary']) &&  $contactData['site_manager_1']['same_as_primary'] == 1) ? 'checked' : ''
                ]);
                ?>
                Same as Primary Contact
            </label>
        </div>
    </div>
    <div class="list-item-content" id="site_manager_1" style="<?php echo (isset($contactData['site_manager_1']['same_as_primary']) &&  $contactData['site_manager_1']['same_as_primary'] == 1) ? 'display: none' : ''  ?>">
        <div class="row">
            
            <div class="col-sm-3">
                <div class="form-group">
                    <label class="control-label">Name </label>
                    <?php 
                    echo form_input([ 
                        'name' => "institution[site_manager_1][name]",
                        'class' => 'form-control',
                        'value' => (isset($contactData['site_manager_1']['name']) ? $contactData['site_manager_1']['name'] : '' )
                    ]);
                    ?>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label class="control-label">Title </label>
                   
                    <?php 
                    echo form_input([ 
                        'name' => "institution[site_manager_1][title]",
                        'class' => 'form-control',
                        'value' => (isset($contactData['site_manager_1']['title']) ? $contactData['site_manager_1']['title'] : '' )
                    ]);
                    ?>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label class="control-label">Email </label>
                    <?php 
                    echo form_input([ 
                        'name' => "institution[site_manager_1][email]",
                        'class' => 'form-control',
                        'value' => (isset($contactData['site_manager_1']['email']) ? $contactData['site_manager_1']['email'] : '' )
                    ]);
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group">
                    <label class="control-label">Primary Phone </label>
                    <?php 
                    echo form_input([ 
                        'name' => "institution[site_manager_1][pri_phone]",
                        'class' => 'form-control usphone',
                        'value' => (isset($contactData['site_manager_1']['pri_phone']) ? $contactData['site_manager_1']['pri_phone'] : '' )
                    ]);
                    ?>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label class="control-label">Secondary Phone </label>
                    <?php 
                    echo form_input([ 
                        'name' => "institution[site_manager_1][sec_phone]",
                        'class' => 'form-control usphone',
                        'value' => (isset($contactData['site_manager_1']['sec_phone']) ? $contactData['site_manager_1']['sec_phone'] : '' )
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="hr-line mb-5"></div>
<!----// List Item ----->
<div class="list-item icon-left">
    <div class="list-item-head">
        <span class="ficon flaticon-contact"></span>
        Site Manager 2 (Optional)
        <div class="list-checkbox">
            <label>
                <?php 
                echo form_checkbox([ 
                    'name' => "institution[site_manager_2][same_as_primary]",
                    'onclick' => "SchoolContact.getSelectSameAsPrimary('site_manager_2', this)",
                    'value' => 1,
                    'checked' => (isset($contactData['site_manager_2']['same_as_primary']) &&  $contactData['site_manager_2']['same_as_primary'] == 1) ? 'checked' : ''
                ]);
                ?>
                Same as Primary Contact
            </label>
        </div>
    </div>
    <div class="list-item-content" id="site_manager_2" style="<?php echo (isset($contactData['site_manager_2']['same_as_primary']) &&  $contactData['site_manager_2']['same_as_primary'] == 1) ? 'display: none' : ''  ?>">
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group">
                    <label class="control-label">Name </label>
                    <?php 
                    echo form_input([ 
                        'name' => "institution[site_manager_2][name]",
                        'class' => 'form-control',
                        'value' => (isset($contactData['site_manager_2']['name']) ? $contactData['site_manager_2']['name'] : '' )
                    ]);
                    ?>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="form-group">
                    <label class="control-label">Title </label>
                   <?php 
                    echo form_input([ 
                        'name' => "institution[site_manager_2][title]",
                        'class' => 'form-control',
                        'value' => (isset($contactData['site_manager_2']['title']) ? $contactData['site_manager_2']['title'] : '' )
                    ]);
                    ?>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="form-group">
                    <label class="control-label">Email </label>
                    <?php 
                    echo form_input([ 
                        'name' => "institution[site_manager_2][email]",
                        'class' => 'form-control',
                        'value' => (isset($contactData['site_manager_2']['email']) ? $contactData['site_manager_2']['email'] : '' )
                    ]);
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group">
                    <label class="control-label">Primary Phone </label>
                    <?php 
                    echo form_input([ 
                        'name' => "institution[site_manager_2][pri_phone]",
                        'class' => 'form-control usphone',
                        'value' => (isset($contactData['site_manager_2']['pri_phone']) ? $contactData['site_manager_2']['pri_phone'] : '' )
                    ]);
                    ?>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label class="control-label">Secondary Phone </label>
                    <?php 
                    echo form_input([ 
                        'name' => "institution[site_manager_2][sec_phone]",
                        'class' => 'form-control usphone',
                        'value' => (isset($contactData['site_manager_2']['sec_phone']) ? $contactData['site_manager_2']['sec_phone'] : '' )
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="hr-line mb-5"></div>
<!----// List Item ----->
<div class="list-item icon-left">
    <div class="list-item-head">
        <span class="ficon flaticon-comment"></span>
        Notes/Comments
    </div>
    <div class="list-item-content">
        <div class="row">
            <div class="col-sm-9">
                <div class="form-group">
                    <?php 
                    echo form_textarea([ 
                        'name' => "institution[comments]",
                        'class' => 'form-control',
                        'placeholder' => 'Write Here...',
                        'rows' => 3,
                        'value' => (isset($siteContactData->comments) ? $siteContactData->comments : '' )
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-sm-12">
    <div class="hr-line mb-4"></div>
</div>
<div class="row">
    <div class="col-sm-12 text-right mt-3 mb-4">
        <button type="button" class="btn btn-danger back_to_identity scroll-top" >Back</button>
        <button type="submit" class="btn btn-primary scroll-top"><?php echo ($this->session->userdata('action') == 'add')?'Save & Next':'Save Changes' ?></button>
    </div>
</div>

</form>

<script type="text/javascript" src="<?php bs('public/assets/js/container/school-contact.js') ?>"></script>
<?php

$jsVars = [
    'ajax_call_root' => base_url(),
    'states'         => $states,
    'siteID'         => $siteID
];    
?>
<script>

    jQuery(document).ready(function() { 
        SchoolContact.init(<?php echo json_encode($jsVars); ?>);
        $('.usphone').usPhoneFormat(true);
    });
    $(".scroll-top").on("click", function(){
      $('body,html').animate({
          scrollTop: 0
      }, 500);
    })      
</script>