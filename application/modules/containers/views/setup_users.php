
<div class="row">
  <div class="col-sm-12 p-0 themePanel">
      <div class="col-sm-12 icon-left pt-3 pb-4">
          <h4 class="text-dark p-0 block-heading">
              <i class="icon flaticon-penitentiary mr-3"></i> <?php echo $containerDetails->name;?>
          </h4>
      </div>
      <div class="col-sm-12">
          <div class="hr-line mb-4"></div>
      </div>
  </div>  
</div>
<div id="userAddFormSection" style="display: none">
<?php echo form_open('containers/schools/add_new',['id'=>'setupusers','name'=>'setupusers','autocomplete'=>'off']); ?>
<div class="row">
  <div class="col-sm-4 pt-3">
    <div class="form-group">
        <label class="control-label">First Name <span class="field-required">*</span></label>
        
        <?php echo form_input([ 
          'name' => 'first_name',
          'id' => 'first_name',
          'class' => 'form-control',
          'value' => (isset($schoolData->name) ? $schoolData->name : '')
        ]);?>
    </div>
  </div>
  <div class="col-sm-4 pt-3">
    <div class="form-group">
        <label class="control-label">Middle Name </label>
        <?php echo form_input([ 
          'name' => 'middle_name',
          'id' => 'middle_name',
          'class' => 'form-control',
          'value' => (isset($schoolData->name) ? $schoolData->name : '')
        ]);?>
    </div>
  </div>
  <div class="col-sm-4 pt-3">
    <div class="form-group">
        <label class="control-label">Last Name <span class="field-required">*</span></label>
        <?php echo form_input([ 
          'name' => 'last_name',
          'id' => 'last_name',
          'class' => 'form-control',
          'value' => (isset($schoolData->name) ? $schoolData->name : '')
        ]);?>
    </div>
  </div>
  <div class="col-sm-4 pt-3">
    <div class="form-group">
        <label class="control-label">Unique ID</label>
        <?php echo form_input([ 
          'name' => 'unique_id',
          'id' => 'unique_id',
          'class' => 'form-control',
          'readonly' => 'readonly',
          'value' => (isset($unique_id) ? $unique_id : '')
        ]);?>
    </div>
  </div>
  <div class="col-sm-4 pt-3">
    <div class="form-group">
        <label class="control-label">Primary Email <span class="field-required">*</span></label>
        <?php echo form_input([ 
          'name' => 'email',
          'id' => 'email',
          'class' => 'form-control',
          'value' => (isset($schoolData->name) ? $schoolData->name : '')
        ]);?>
    </div>
  </div>
  <div class="col-sm-4 pt-3">
    <div class="form-group">
        <label class="control-label">Phone No </label>
        <?php echo form_input([ 
          'name' => 'phone',
          'id' => 'phone',
          'class' => 'form-control',
          'value' => (isset($schoolData->name) ? $schoolData->name : '')
        ]);?>
    </div>
  </div>
  <div class="col-sm-4 pt-3">
    <div class="form-group">
        <label class="control-label">User Role <span class="field-required">*</span></label>
        <?php
        echo form_dropdown('roles', $roles,'', ['id'=>'role_id','class' => 'form-control']);
        ?>
    </div>
  </div>
  <div class="col-sm-4 pt-3"></div>
  <div class="col-sm-4 pt-3 text-right">
    <button type="submit" class="btn btn-primary mt-5">Add User</button>
  </div>
</div>
</form>        
<div class="col-sm-12 p-0">
  <div class="hr-line mb-5 mt-5"></div>
</div>
<div class="themePanel col-sm-12 icon-left mb-3">
  <h4>
    <i class="flaticon-book ficon"></i> Bulk Upload Users
  </h4>
  <div class="row">
    <div class="col-sm-12">
      <div class="ist-visual-info p-0">
        <div class="col-md-2 p-0 mr-4 pt-1">
         <button type="button" class="btn btn-success" onclick="SchoolUsers.getOpenUploadPopup('Upload users','containerUsersDataTables','',1);">Upload Users</button>
       </div>
       <div class="col-md-8 p-0">
        <small class="f13 text-dark">
          Note : This file must be a CSV formatted file.<br> 
          <span class="d-inline-block bg-1 round-5 pt-2 pb-2 pl-4 pr-4 mt-2 fw600 f12">
            Sample 
            <a class="text-link" href="<?php bs('/uploads/samples/users_sample.csv'); ?>" target="_blank">
              CSV <i class="flaticon-download mr-1"></i>
            </a> 
            and 
            <a class="text-link" href="<?php bs('/uploads/samples/users_sample.xlsx'); ?>" target="_blank">
              Excel <i class="flaticon-download mr-1"></i>
            </a> 
              file example.
          </span>
        </small>
      </div>
    </div>
  </div>
</div> 
</div> 
<div class="col-sm-12 p-0">
    <div class="hr-line mb-5 mt-5"></div>
  </div>
    <div class="col-sm-12 text-right p-0 mt-3 mb-4">
        <button type="button" class="btn btn-danger"  onclick="SchoolUsers.showHideUserSection('#userAddFormSection','#userListingSection')">Back</button>
    </div>
  </div>
</div>

<!--------------//  User Listing Section  //------------------------>
<div class="col-xs-12 p-0" id="userListingSection">
<div class="row m-0">

        <div class="col-md-12">
            <div class="panel-form-control flex-row">
                <div class="flex-col pl-0">
                    <?php 
                    echo form_dropdown('role_id', $roles_list, '', 'class="selected_condition form-control" id="selected_roles"'); ?>
                </div>
                <div class="flex-col pl-0">
                    <select id="selected_course" name="course_id" class="selected_condition form-control">
                        <option value="0"> --Course--</option>
                    </select>
                </div>
                <div class="flex-col pl-0">
                    <select id="selected_section" name="section_id" class="selected_condition form-control">
                        <option value="0"> --Section--</option>
                    </select>
                </div>
                <div class="flex-col-sm-1 pl-0 pr-0">
                    <a class="btn btn-danger btn-block pt-2 reset_filter" href="javascript:void(0)" ><i class="flaticon-close-1 f21 fw100"></i></a>
                </div>
            </div>
        </div>
        <div class="col-sm-12 p-0">
          <div class="hr-line mb-3 mt-3"></div>
        </div>        
    </div>
<div class="col-xs-12 p-0 data_tables_custome">
    <div class="col-xs-12 row m-0 p-0">
      <div class="col-xs-6 p-0 pt-3">
        <a href="javascript:void(0)" class="btn btn-primary btn-md-2" onclick="SchoolUsers.showHideUserSection('#userListingSection','#userAddFormSection')">Add User</a>
      </div>
      <div class="col-xs-6 p-0">
        <div class="panel-ctrls"></div>
      </div>
    </div>
    <table id="containerListTable" class="table table-custome1 ui-checkbox-container" cellspacing="0">
        <thead>
            <tr>
                <th>
                    <label for="sellectAll" class="checkbox-tel">
                        <input type="checkbox" name="sellectAll" id="sellectAlluser" class="mr-2 users_select_all">
                    </label>
                </th>
                 <th>Student Name</th>
                <th>Institutional Email</th>
                <th>Enrolled Courses</th>
                <th>Enrolled Sections</th>
                <th>Created Date</th>
                <th>Parent</th>
                <th>Switch To User</th>
            </tr>
        </thead>
    </table>

    <div class="col-xs-12 p-0">
        <button class="btn btn-dark" type="button" id="remove_users">Remove</button>
    </div>
</div>


  <div class="col-sm-12 p-0">
    <div class="hr-line mb-5 mt-5"></div>
  </div>
    <div class="col-sm-12 text-right p-0 mt-3 mb-4">
        <button type="button" class="btn btn-danger back_to_metadata scroll-top">Back</button>
        <button type="button" class="btn btn-primary next_to_group scroll-top"><?php echo ($this->session->userdata('action') == 'add')?'Save & Next':'Save Changes' ?></button>
    </div>
  </div>

</div>

<script type="text/javascript" src="<?php bs('public/assets/js/container/school-users.js') ?>"></script>
<?php
$jsVars = [
    'ajax_call_root' => base_url(),
    'siteID'         => $siteID
];    
?>
<script>
    jQuery(document).ready(function() { 
        SchoolUsers.init(<?php echo json_encode($jsVars); ?>);
    });
    $(".scroll-top").on("click", function(){
      $('body,html').animate({
          scrollTop: 0
      }, 500);
    });  
           
</script>
