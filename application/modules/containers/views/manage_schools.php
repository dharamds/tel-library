<link rel="stylesheet" href="<?php  bs('public/assets/plugins/multiselect/sumoselect.min.css') ?>" /> 
<div class="container-fluid">
   <div>
      <div class="row">
         <div class="col-md-12">
            <div class="filter-container flex-row" >
               <div class="flex-col" id="reflect_system_category">
               </div>
               <div class="flex-col-sm" id="reflect_system_tag">
               </div>
               <div class="flex-col-sm" id="reflect_institute_category">
               </div>
               <div class="flex-col-sm" id="reflect_institute_tag">
               </div>
               <div class="flex-col-sm-1">
                  <button type="button"  class="btn btn-danger btn-block reset_filter">
                     <span class="fa fa-times"></span>
                  </button>
               </div>
            </div>
         </div>
         <div class="col-md-12">
            <div class="panel panel-default panel-grid">
               <div class="panel-heading">

                  <div class="flex-row">
                     <div class="flex-col-md-8">
                        <a class="btn btn-primary " href="<?= base_url('institutions/add') ?>">Add New </a>
                        <button class="btn btn-danger" id="remove_multiple" ><i class="fa fa-trash-o"></i> Remove </button>
                        <button class="btn btn-warning" id="deactive_multiple" ><i class="fa fa-ban"></i> Deactivate </button>
                        <button type="button" class="btn btn-success" id="bulk_export" ><i class="flaticon-download-1 mr-2 f11"></i> Download selected</button>
                        <button type="button" class="btn btn-success" id="bulk_export_all" ><i class="flaticon-download-1 mr-2 f11"></i> Download all</button>
                     </div>
                     <div class="flex-col-md-4">
                        <div class="panel-ctrls"></div>
                     </div>
                  </div>
               </div>
               <div class="panel-body">
                  <div class="row p-0">
                     <div class="col-md-12 m-0">
                        <table class="table table-bordered" id="memListTable">
                          
                           <thead>
                              <tr>
                                 <th style="min-width: 50px;">
                                    <label for="select_all" class="checkbox-tel">
                                       <input type="checkbox" name="select_all" id="select_all" class="mr-2 site_select_all"> 
                                    </label>
                                 </th>
                                 <th style="min-width: 120px;">Actions</th>
                                 <th style="min-width: 100px;">Status</th>
                                 <th style="min-width: 220px;">Institute</th>
                                 <th style="min-width: 150px;">Primary Contact</th>
                                 <th style="min-width: 210px;">Number of Current Courses</th>
                                 <th style="min-width: 210px;">Number of Current Sections</th>
                                 <th style="min-width: 210px;">Number of Current Students</th>
                                 <th style="min-width: 230px;">Number of Current Enrollments</th>
                              </tr>
                           </thead>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>     
<script type="text/javascript" src="<?php  bs('public/assets/plugins/multiselect/sumoselect.min.js') ?>"></script>    
<script type="text/javascript" src="<?php bs('public/assets/js/container/manage_schools.js') ?>"></script>
<?php
$jsVars = [
   'ajax_call_root' => base_url()
];
?>
<script>
   jQuery(document).ready(function() {
      Manage_Schools.init(<?php echo json_encode($jsVars); ?>);
   });  
</script>