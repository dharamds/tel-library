<div class="container-fluid">
    <div>
        <div class="row">
            <div class="col-md-12">
                <div class="filter-container flex-row panel-form-control">
                    <div class="flex-col">
                        <?php echo form_dropdown('course_id', $courses, '', 'class="form-control group_filter" id="selected_course" '); ?>
                    </div>
                    <div class="flex-col-sm">
                        <select id="section_id" name="section_id" class="form-control group_filter">
                            <option value=""> --Section-- </option>
                        </select>
                    </div>
                    <div class="flex-col-sm">
                        <select id="group_leader" name="group_leader" class="form-control group_filter">
                            <option value=""> --Group Leader-- </option>
                        </select>
                    </div>
                    <div class="flex-col-sm-1">
                        <button class="btn btn-danger btn-block reset_group_filter">
                            <span class="fa fa-times"></span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default panel-grid">
                    <div class="panel-heading">
                         <div class="col-sm-12 p-0">
                              <div class="flex-row">
                                <div class="flex-col-auto pt-3">
                                   <a class="btn btn-primary panel-btn" href="javascript:void(0)">
                                      <i class="flaticon-plus-button"></i> Add New Group
                                   </a>
                                    <button class="btn btn-danger panel-btn" ><i class="fa fa-trash-o"></i> Delete Selected</button>
                                   <a class="btn btn-warning panel-btn" href="javascript:void(0)">
                                      <i class="flaticon-ring"></i> Notify To
                                   </a>
                                   <a class="btn btn-primary panel-btn"  href="javascript:void(0)" >
                                    <i class="flaticon-network-1"></i> Set of Groups
                                  </a>
                                  
                                </div>
                                <div class="flex-col panel-form-control">
                                   <div class="panel-ctrls"></div>
                                </div>
                              </div>
                            </div>
                    </div>
                    <div class="panel-body">
                        <div class="row p-0">
                            <div class="col-sm-12 data_tables_custome">
                                <table class="table table-bordered" id="group_list_table">
                                    <thead>
                                        <tr>
                                            <th>
                                                <label for="select_all">
                                                    <input type="checkbox" name="select_all_group" id="select_all_group" class="mr-2 group_select_all"> 
                                                </label>
                                            </th>
                                            <th>Group Name</th>
                                            <th>Associated Course</th>
                                            <th>Associated Section</th>
                                            <th>Visible to Students?</th>
                                            <th>Number Enrolled</th>
                                            <th>Group Leaders</th>
                                            <th>Status</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  

<script type="text/javascript" src="<?php bs('public/assets/js/container/site_group_list.js') ?>"></script>
<?php
$jsVars = [
   'ajax_call_root' => base_url(),
   'siteID' => $siteID
];
?>
<script>
   jQuery(document).ready(function() {
      SiteGroupsList.init(<?php echo json_encode($jsVars); ?>);
   });
</script>
