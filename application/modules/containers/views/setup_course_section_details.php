<div class="row">
    <div class="col-sm-12 p-0 themePanel">
        <div class="col-sm-12 icon-left pt-3 pb-4">
            <h4 class="text-dark p-0 block-heading">
                <i class="flaticon-penitentiary mr-3"></i> <?php echo $sectionData->container_name ?>
                <button type="button" class="btn btn-primary btn-sm pull-right scroll-top" onclick="SchoolCourseSectionDetails.getBackToList('courses');">
                  <i class="fa fa-angle-left mr-2"></i> Back to course
                </button>
            </h4>
        </div>
        <div class="col-sm-12">
            <div class="hr-line mb-5"></div>
        </div>
    </div>

    <div class="col-sm-12 p-0">
        <div class="themePanel col-sm-12 icon-left mb-3">
            <h4>
                <i class="flaticon-book ficon"></i> Bulk Enroll Users
            </h4>
            <div class="row istVisualContainer">
                <div class="col-sm-12">
                    <div class="ist-visual-info p-0">
                      <div class="col-md-2 p-0 mr-4 pt-1">
                       <button type="button" class="btn btn-success" onclick="SchoolCourseSectionDetails.getOpenUploadPopup('Upload users','getUserSection|getCountEnrollUsers','',2);">Upload Users</button>
                      </div>
                      <div class="col-md-8 p-0">
                        <small class="f13 text-dark">
                        Note : This file must be a CSV formatted file.<br> 
                        <span class="d-inline-block bg-1 round-5 pt-2 pb-2 pl-4 pr-4 mt-2 fw600 f12">
                          Sample 
                          <a class="text-link" href="<?php bs('/uploads/samples/users_sample.csv'); ?>" target="_blank">
                            CSV <i class="flaticon-download mr-1"></i>
                          </a> 
                          and 
                          <a class="text-link" href="<?php bs('/uploads/samples/users_sample.xlsx'); ?>" target="_blank">
                            Excel <i class="flaticon-download mr-1"></i>
                          </a> 
                            file example.
                        </span>
                        </small>
                      </div>
                   </div>
               </div>
           </div>            
        </div>
    </div>
 
    <div class="col-sm-12 p-0">
        <div class="themePanel col-sm-12 icon-left m-0">
            <h4>
                <i class="flaticon-book ficon"></i> <?php echo $sectionData->course_name ?>
            </h4>
            <div class="row istVisualContainer">
              <div class="col-md-12">
                <div class="flex-row flex-table-view m-0 align-items-center">
                  <div class="flex-col-sm-3 flex-col-head text-center">
                    <h5>
                      <i class="flaticon-calendar-2 mr-2"></i>
                      <?php echo $sectionData->sectionname ?>
                    </h5>
                  </div>
                  <div class="flex-col-sm-3">
                    <label>Start Date</label>
                    <p><?php echo date("M d, Y", strtotime($sectionData->start_date))  ?></p>
                  </div>
                  <div class="flex-col-sm-3">
                    <label>End Date</label>
                    <p><?php echo date("M d, Y", strtotime($sectionData->end_date))  ?></p>
                  </div>
                  <div class="flex-col-sm-3">
                    <label>Enrollment</label>
                    <p><span id="enroll_count">0</span></p>
                  </div>
                </div>
              </div>
              <div class="col-sm-12 mt-5" id="course_section_detail_user_part">
                    
              </div>
          </div>
      </div>
    </div>
    <!------------------------------------>

  <!------------------->


</div>  

<div class="row">
    <div class="col-sm-12 text-right">
        <button type="button" class="btn btn-danger scroll-top" onclick="SchoolCourseSectionDetails.getBackToList('courses');">Back</button>
        <button type="button" class="btn btn-primary course_next scroll-top" ><?php echo ($this->session->userdata('action') == 'add')?'Save & Next':'Save Changes' ?></button>
    </div>
</div>
<script type="text/javascript" src="<?php bs('public/assets/js/container/school-course-section-details.js') ?>"></script>
<?php
    $jsVars = [
        'ajax_call_root' => base_url(),
        'siteID'         => $siteID
    ];    
?>
<script>
    $(".scroll-top").on("click", function(){
      $('body,html').animate({
          scrollTop: 0
      }, 500);
    });  
    jQuery(document).ready(function() { 
        SchoolCourseSectionDetails.init(<?php echo json_encode($jsVars); ?>);
    });
</script>

