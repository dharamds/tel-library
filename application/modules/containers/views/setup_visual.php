<link type="text/css" href="<?= bs('public/assets/plugins/form-multiselect/css/multi-select.css') ?>" rel="stylesheet">
<link type="text/css" href="<?= bs('public/assets/css/croppie.css') ?>" rel="stylesheet">

<script type="text/javascript" src="<?php bs('public/assets/plugins/form-colorpicker/js/bootstrap-colorpicker.min.js') ?>"></script>
<?php
$header_back_color = (isset($themeData['header_back_color']) &&  $themeData['header_back_color'] != '' ? $themeData['header_back_color'] : '#1657d4');
$header_text_color = (isset($themeData['header_text_color']) &&  $themeData['header_text_color'] != '' ? $themeData['header_text_color'] : '#ffffff');

$footer_back_color = (isset($themeData['footer_back_color']) &&  $themeData['footer_back_color'] != '' ? $themeData['footer_back_color'] : '#1657d4');
$footer_text_color = (isset($themeData['footer_text_color']) &&  $themeData['footer_text_color'] != '' ? $themeData['footer_text_color'] : '#ffffff');

$button_back_color = (isset($themeData['button_back_color']) &&  $themeData['button_back_color'] != '' ? $themeData['button_back_color'] : '#1cbe9e');
$button_text_color = (isset($themeData['button_text_color']) &&  $themeData['button_text_color'] != '' ? $themeData['button_text_color'] : '#ffffff');

?>
<div class="row">
  <div class="col-sm-12 p-0 themePanel">
      <div class="col-sm-12 icon-left pt-3 pb-4">
          <h4 class="text-dark p-0 block-heading">
              <i class="icon flaticon-penitentiary mr-3"></i> <?php echo $containerDetails->name;?>
          </h4>
      </div>
      <div class="col-sm-12">
          <div class="hr-line mb-4"></div>
      </div>
  </div>  
</div>
<?php echo form_open('containers/schools/add_new', ['id' => 'setupvisual', 'name' => 'setupvisual', 'enctype' => 'multipart/form-data']); ?>
<div class="istVisualContainer">
    <div class="row">
        <div class="row col-sm-10">
            <div class="col-sm-3 mr-5">
                <div class="ist-logo">
                    <?php if (isset($themeData['logo']) &&  $themeData['logo'] != '') { ?>
                        <img src="<?php bs() ?>uploads/containers/logo/thumb/<?php echo $themeData['logo']  ?>" id="ist_logo_id">
                    <?php } else { ?>
                        <img src="<?php bs() ?>public/assets/img/logo-bg.png" id="ist_logo_id">
                    <?php } ?>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="ist-visual-info">
                    <h4>Institution Logo</h4>
                    <div class="row">
                        <div class="col-sm-6">
                            <input type="hidden" name="old_img" value="<?php echo (isset($themeData['logo']) && $themeData['logo'] != '' ? $themeData['logo'] : '') ?>">
                            <div class="fileinput fileinput-new col-md-12 p-0" data-provides="fileinput">
                                <span class="btn btn-danger btn-file col-md-12">
                                    <span class="fileinput-new">Choose File</span>
                                    <span class="fileinput-exists">Change</span>
                                    <input id="dis_img" name="logo" type="file" class="blog_img1 visible1">
                                    <br />
                                    <div id="uploaded_image"></div>
                                    
                                </span>
                                <div class="filename-with-btn col-md-12">
                                    <span class="fileinput-filename"></span>
                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" id="uploaded_img_name" name="uploaded_img_name">
                        <div class="col-sm-6">
                            <div class="small">
                                Size 200 x 200 (Max Size 1 MB)
                            </div>
                            <div class="small">
                                (File must be .png, .jpg, .jpeg)
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-8 p-0">
        <div class="col-sm-11 p-0">
            <!--//  Header  //-->
            <div class="themePanel icon-left">
                <h4>
                    <i class="ficon flaticon-web-design"></i> Header Theme 
                </h4>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group colorPicker">
                            <label class="control-label">Background color <small>(Ex. : #CCCCCC)</small></label>
                            <div class="input-group cpicker color" data-color="<?php echo $header_back_color ?>" data-color-format="hex">

                                <span class="input-group-addon">
                                    <span class="fa fa-paint-brush"></span>
                                </span>
                                <?php echo form_input([
                                    'name' => 'theme[header_back_color]',
                                    'id' => 'header_back_color',
                                    'class' => 'form-control',
                                    'value' => $header_back_color
                                ]); ?>

                                <span class="input-group-addon">
                                    <i style="background-color: <?php echo $header_back_color ?>;"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group colorPicker">
                            <label class="control-label">Text color <small>(Ex. : #CCCCCC)</small></label>
                            <div class="input-group cpicker color" data-color="<?php echo $header_text_color ?>" data-color-format="hex">
                                <span class="input-group-addon">
                                    <span class="fa fa-paint-brush"></span>
                                </span>
                                <?php echo form_input([
                                    'name' => 'theme[header_text_color]',
                                    'id' => 'header_text_color',
                                    'class' => 'form-control',
                                    'value' => $header_text_color
                                ]); ?>
                                <span class="input-group-addon">
                                    <i style="background-color:<?php echo $header_text_color ?>"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!---------------------------------->
            <!--//  Footer  //-->
            <div class="themePanel icon-left">
                <h4>
                    <i class="ficon flaticon-web-design"></i> Footer Theme
                </h4>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group colorPicker">
                            <label class="control-label">Background color <small>(Ex. : #CCCCCC)</small></label>
                            <div class="input-group cpicker color" data-color="<?php echo $footer_back_color ?>" data-color-format="hex">
                                <span class="input-group-addon">
                                    <span class="fa fa-paint-brush"></span>
                                </span>
                                <?php echo form_input([
                                    'name' => 'theme[footer_back_color]',
                                    'id' => 'footer_back_color',
                                    'class' => 'form-control',
                                    'value' => $footer_back_color
                                ]); ?>
                                <!-- <input type="text" readonly class="form-control" value="#1657d4"> -->
                                <span class="input-group-addon">
                                    <i style="background-color: <?php echo $footer_back_color ?>;"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group colorPicker">
                            <label class="control-label">Text color <small>(Ex. : #CCCCCC)</small></label>
                            <div class="input-group cpicker color" data-color="<?php echo $footer_text_color ?>" data-color-format="hex">
                                <span class="input-group-addon">
                                    <span class="fa fa-paint-brush"></span>
                                </span>
                                <?php echo form_input([
                                    'name' => 'theme[footer_text_color]',
                                    'id' => 'footer_text_color',
                                    'class' => 'form-control',
                                    'value' => $footer_text_color
                                ]); ?>
                                <span class="input-group-addon">
                                    <i style="background-color:<?php echo $footer_text_color ?>;"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="col-sm-4 themePreviewPanel">
        <div class="col-sm-offset-3">
            <h4>Theme View</h4>
            <div class="previewHeaderFooter">
                <div id="previewHeader" style="background-color: <?php echo $header_back_color ?>; color: <?php echo $header_text_color ?>">Header</div>
                <div class="previewContent"></div>
                <div id="previewFooter" style="background-color: <?php echo $footer_back_color ?>; color: <?php echo $footer_text_color ?>">Footer</div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-8 p-0">
        <div class="col-sm-11 p-0">
            <!---------------------------------->
            <!--//  Button  //-->
            <div class="themePanel icon-left">
                <h4>
                    <i class="ficon flaticon-web-design"></i> Button Theme
                </h4>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group colorPicker">
                            <label class="control-label">Background color <small>(Ex. : #CCCCCC)</small></label>
                            <div class="input-group cpicker color" data-color="<?php echo $button_back_color ?>" data-color-format="hex">
                                <span class="input-group-addon">
                                    <span class="fa fa-paint-brush"></span>
                                </span>
                                <?php echo form_input([
                                    'name' => 'theme[button_back_color]',
                                    'id' => 'button_back_color',
                                    'class' => 'form-control',
                                    'value' => $button_back_color
                                ]); ?>
                                <span class="input-group-addon">
                                    <i style="background-color: <?php echo $button_back_color ?>;"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group colorPicker">
                            <label class="control-label">Text color <small>(Ex. : #CCCCCC)</small></label>
                            <div class="input-group cpicker color" data-color="<?php echo $button_text_color ?>" data-color-format="hex">
                                <span class="input-group-addon">
                                    <span class="fa fa-paint-brush"></span>
                                </span>
                                <?php echo form_input([
                                    'name' => 'theme[button_text_color]',
                                    'id' => 'button_text_color',
                                    'class' => 'form-control',
                                    'value' => $button_text_color
                                ]); ?>
                                <span class="input-group-addon">
                                    <i style="background-color: <?php echo $button_text_color ?>;"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!---------------------------------->
        </div>
    </div>
    <div class="col-sm-4 themePreviewPanel">
        <div class="col-sm-offset-3">
            <div class="previewButton">
                <button type="button" style="background-color:<?php echo $button_back_color; ?>; color:<?php echo $button_text_color ?> ; cursor:default ">Button Preview</button>
            </div>
        </div>
    </div>
</div>
<div class="col-sm-12 mt-5">
    <div class="hr-line mb-5"></div>
</div>
<div class="row mt-5 mb-4">
    <div class="col-sm-12 text-right">
        <button type="button" class="btn btn-danger back_to_contact scroll-top">Back</button>
        <button type="submit" class="btn btn-primary scroll-top" onchange="SchoolVisual.getName();"><?php echo ($this->session->userdata('action') == 'add')?'Save & Next':'Save Changes'?></button>
    </div>
</div>
</form>
<script type="text/javascript" src="<?= bs('public/assets/js/croppie.js') ?>"></script>
<script type="text/javascript" src="<?= bs('public/assets/plugins/form-jasnyupload/fileinput.min.js') ?>"></script>
<script type="text/javascript" src="<?php bs('public/assets/js/container/school-visual.js') ?>"></script>
<?php
$jsVars = [
    'ajax_call_root' => base_url(),
    'siteID' => $siteID
];
?>
<div id="uploadimageModal" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Upload & Crop Image</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8 text-center">
                        <div id="image_demo" style="width:350px; margin-top:30px"></div>
                    </div>
                    <div class="col-md-4" style="padding-top:30px;">
                        <br />
                        <br />
                        <br />
                        <button class="btn btn-success crop_image">Crop & Upload Image</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {

        $image_crop = $('#image_demo').croppie({
            enableExif: true,
            viewport: {
                width: 200,
                height: 200,
                type: 'square' //circle
            },
            boundary: {
                width: 300,
                height: 300
            }
        });

        $('#dis_img').on('change', function() {
            var reader = new FileReader();
            reader.onload = function(event) {
                $image_crop.croppie('bind', {
                    url: event.target.result
                }).then(function() {
                    console.log('jQuery bind complete');
                });
            }
            reader.readAsDataURL(this.files[0]);
            $('#uploadimageModal').modal('show');
        });

        $('.crop_image').click(function(event) {
            $image_crop.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function(response) {
                $.ajax({
                    url: "<?php echo bs('containers/Schools/img_crop_upload'); ?>",
                    type: "POST",
                    data: {
                        "image": response
                    },
                    success: function(data) {
                        data = JSON.parse(data);
                        //alert(data['img_full_name']);
                        $('#uploaded_img_name').val(data['img_name']);
                        $('#uploadimageModal').modal('hide');
                        $('#ist_logo_id').attr("src", data['img_full_name']);
                    }
                });
            })
        });

    });
</script>
<script>
    jQuery(document).ready(function() {
        SchoolVisual.init(<?php echo json_encode($jsVars); ?>);
    });
    $(".scroll-top").on("click", function(){
      $('body,html').animate({
          scrollTop: 0
      }, 500);
    })      
</script>