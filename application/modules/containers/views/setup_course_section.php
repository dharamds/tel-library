<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th class="text-center">Action</th>
            <th>Section</th>
            <th>Courses</th>
            <th>Start Date</th>
            <th>End Date</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($course_section as $key => $value) { //pr($value);?>
        <tr>
            <td class="text-center">
                <button type="button" class="btn btn-primary btn-sm btn-grid scroll-top" onclick="SchoolCourseSection.getOtherViewTemplate('courses','<?php echo $value['id'] ?>','<?php echo $siteID ?>')" title="Click to view">
                    <i class="fa fa-eye"></i>
                </button>
                <button type="button" data-id="<?php echo $value['id'] ?>" class="btn btn-primary btn-sm btn-grid get_make_update_section" title="Click to edit">
                    <i class="fa fa-edit"></i>
                </button>
                <button type="button" class="btn btn-danger btn-sm btn-grid remove_section" value="<?php echo base64_encode($value['id']); ?>" title="Click to remove">
                    <i class="fa fa-trash"></i>
                </button>
            </td>
            <td><?php echo $value['section_name'] ?></td>
            <td><?php echo $value['name'] ?></td>
            <td><?php echo date("M d, Y", strtotime($value['start_date']))  ?></td>
            <td><?php echo date("M d, Y", strtotime($value['end_date']))  ?></td>
            
        </tr>
        <?php } if(empty($course_section)){?>
        <tr>
            <td colspan="5">Sorry, There is no section. </td>
        </tr>
        <?php } ?>        
    </tbody>
</table>

<script type="text/javascript" src="<?php bs('public/assets/js/container/school-course-section.js') ?>"></script>
<?php
$jsVars = [
    'ajax_call_root' => base_url(),
    'siteID'         => $siteID];    
?>
<script>
    jQuery(document).ready(function() { 
        SchoolCourseSection.init(<?php echo json_encode($jsVars); ?>);
    });
    $(".scroll-top").on("click", function(){
      $('body,html').animate({
          scrollTop: 0
      }, 500);
    });    
</script>

