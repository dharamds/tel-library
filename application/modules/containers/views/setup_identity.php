<?php echo form_open('containers/schools/add_new',['id'=>'setupschool','name'=>'setupschool']); ?>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label">Institution/Site Name <span class="field-required">*</span></label>
                <?php echo form_input([ 
                    'name' => 'name',
                    'id' => 'name',
                    'class' => 'form-control',
                    'value' => (isset($schoolData->name) ? $schoolData->name : '')
                ]);?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label">Institution/Site Secondary Title or Slogan <span class="field-required">*</span></label>
                <?php echo form_input([ 
                    'name' => 'slogan',
                    'id' => 'slogan',
                    'class' => 'form-control',
                    'value' => (isset($schoolData->slogan) ? $schoolData->slogan : '')
                ]);?>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label">
                    Institution/Site Domain <span class="field-required">*</span>
                    <a data-toggle="tooltip" data-placement="top" title="Enter only name. Ex: tel-library. " >
                        <i class="flaticon-question-mark"></i>
                    </a>
                </label>
                <div class="input-group">
                    <?php echo form_input([ 
                        'name' => 'domain',
                        'id' => 'domain',
                        'class' => 'form-control',
                        'value' =>  (isset($schoolData->domain) ? str_replace(MAIN_DOMAIN_SITE_CREATION,'',$schoolData->domain) : '')
                    ]);?>
                    <span class="input-group-btn">
                        <div class="btn btn-default btn-caption"><?php echo MAIN_DOMAIN_SITE_CREATION ?></div>
                    </span>
                </div>
                <div class="domainmsg"></div>
            </div>
        </div>
       
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label">Institution Homepage Welcome text <span class="field-required">*</span></label>
                <?php echo form_textarea([ 
                    'name' => 'home_page_text',
                    'id' => 'editor1',
                    'class' => 'form-control',
                    'value' => (isset($schoolData->home_page_text) ? $schoolData->home_page_text : '') 
                ]);?>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label">Institution Homepage Footer text <span class="field-required">*</span></label>
                <?php echo form_textarea([ 
                    'name' => 'home_footer_text',
                    'id' => 'editor2',
                    'class' => 'form-control',
                    'value' => (isset($schoolData->home_footer_text) ? $schoolData->home_footer_text : '') 
                ]);?>
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="hr-line mb-4"></div>
    </div>
    <div class="row">
        <div class="col-sm-12 text-right mb-4 mt-3">
          <?php if($this->container_id == 0){ ?>
            <button type="button" class="btn btn-danger cancel_request" >Cancel</button>
          <?php } ?>  
            <button type="submit" class="btn btn-primary scroll-top"><?php echo ($this->session->userdata('action') == 'add')?'Save & Next':'Save Changes' ?></button>
        </div>
    </div>
</form>
<script type="text/javascript" src="<?php bs('public/assets/js/container/school-identity.js') ?>"></script>
<?php

$jsVars = [
    'ajax_call_root' => base_url(),
    'siteID'         => $siteID
];    
?>
<script>
    jQuery(document).ready(function() { 
        SchoolIdentity.init(<?php echo json_encode($jsVars); ?>);
        $('[data-toggle="tooltip"]').tooltip()
    });
    $(".scroll-top").on("click", function(){
      $('body,html').animate({
          scrollTop: 0
      }, 500);
    })      
</script>
