<div class="static-content-wrapper">
<div class="static-content">
<div class="page-content">
<ol class="breadcrumb">
   <li><a href="">Home</a></li>
   <li><a href="#">Update Site</a></li>
</ol>
<div class="container-fluid">
<div data-widget-group="group1">
<div class="row">
<div class="col-md-12">
   <div class="panel panel-success">
      <div class="panel-heading">
         <h2><i class="fa fa-file-text-o" aria-hidden="true"></i>Update</h2>
         <div class="panel-ctrls">
            <a href="#" class="button-icon"><i class="ti ti-file"></i></a>
            <a href="#" class="button-icon"><i class="ti ti-mouse"></i></a>
            <a href="#" class="button-icon"><i class="ti ti-settings"></i></a>
         </div>
      </div>
      <div class="panel-body">
         <div class="row">
            <div class="col-md-12">
               <?php 

               if (!empty(validation_errors())): ?>
                  
                  <div class="alert alert-danger">
                     <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                     <strong><?php echo validation_errors(); ?></strong>
                  </div>
                  
               <?php endif ?>

               <form action="<?= base_url('containers/schools/setup_edit') ?>" enctype="multipart/form-data" method="post" class="form-horizontal row-border" id="setupschooledit" name="setupschooledit">
                  <div class="form-group">
                     <label class="col-sm-2 control-label text-success">
                       Name
                     </label>
                     <div class="col-sm-4 sun">
                        <input type="text" name="name" id="name" value="<?php echo $schoolData->name?>" class="form-control" required>
                     </div>
                     <label class="col-sm-2 control-label text-success">
                       Slowgan 
                     </label>
                     <div class="col-sm-4">
                        <input type="text" name="slogan"  value="<?php echo $schoolData->slogan?>" class="form-control" required>
                     </div>
                  </div>
                   <div class="form-group">
                     <label class="col-sm-2 control-label text-success">
                       Domain
                     </label>
                     <div class="col-sm-4 sun">
                        <input type="text" name="domain" id="domain" value="<?php echo $schoolData->domain?>" class="form-control" readonly>
                     </div>
                  </div>
                  <div class="form-group">
                      <label class="col-sm-2 control-label text-success">
                       Email
                     </label>
                     <div class="col-sm-4">
                        <input type="text" name="email"  value="<?php echo $schoolData->email?>" class="form-control" required>
                     </div>

                      <label class="col-sm-2 control-label text-success">
                       Phone
                     </label>
                     <div class="col-sm-4">
                        <input type="text" name="phone" class="form-control"  value="<?php echo $schoolData->phone?>" required>
                     </div>
                  </div>
                  <div class="form-group">
                      <label class="col-sm-2 control-label text-success">
                       Address
                     </label>
                     <div class="col-sm-4">
                        <input type="text" name="address" value="<?php echo $schoolData->address?>" class="form-control" required>
                     </div>

                     
                  </div>
                  <div class="form-group">
                     <label class="col-sm-2 control-label text-success">Logo</label>

                        <div class="col-sm-4">
                           <label for="blog_img">
                           <img src="<?php bs() ?>public/assets/img/add-image.png" id="img" width="100" height="100">
                           </label>
                           <input id="blog_img" name="logo" type="file" class="blog_img visible" style="visibility: hidden; height: 0" required>
                           <img src="<?php echo base_url().'uploads/containers/'.$schoolData->logo?>" width="100">
                           <input type="hidden" name="old_img" value="<?php echo $schoolData->logo ?>">
                        </div>
                    
                  </div>
                  

                  <div class="form-group">
                     <label class="col-sm-2 control-label text-success">Description </label>
                     <div class="col-sm-10">
                        <textarea name="description" id="summernote" class="form-control" rows="5" required><?php echo $schoolData->description?></textarea>
                     </div>
                  </div>
                  <?php
                    $themeStyle = unserialize($schoolData->theme_style);
                  ?>
                  <div class="form-group">
                     <label class="col-sm-2 control-label text-success">Brand Color </label>
                     <div class="col-sm-4">
                        <input name="brand_color" type="hidden" id="brand_color" value="<?php echo $themeStyle['brand_color'] ?>">
                        <button class="jscolor {valueElement: 'brand_color'}">Pick a color</button>
                     </div>
                     <label class="col-sm-2 control-label text-success">Text Color </label>
                     <div class="col-sm-4">
                        <input name="text_color" type="hidden" id="text_color" value="<?php echo $themeStyle['text_color'] ?>">
                        <button class="jscolor {valueElement: 'text_color'}">Pick a color</button>
                     </div>
                  </div>

                  

                  <div class="form-group">
                     <label class="col-sm-2 control-label text-success">Primary heading Color </label>
                     <div class="col-sm-4">
                        <input name="pri_heading_color" type="hidden" id="pri_heading_color" value="<?php echo $themeStyle['pri_heading_color'] ?>">
                        <button class="jscolor {valueElement: 'pri_heading_color'}">Pick a color</button>
                     </div>

                      <label class="col-sm-2 control-label text-success">Secondary heading Color </label>
                     <div class="col-sm-4">
                        <input name="sec_heading_color" type="hidden" id="sec_heading_color" value="<?php echo $themeStyle['sec_heading_color'] ?>">
                        <button class="jscolor {valueElement: 'sec_heading_color'}">Pick a color</button>
                     </div>
                  </div>
                

                  <div class="form-group">
                    <label class="col-sm-2 control-label text-success">Font family </label>
                    <div class="col-sm-4">
                        <select name="text_font" id="text_font" class="form-control">
                            <option value="">Select font</option>
                            <option value="Arial" <?php echo ($themeStyle['text_font'] == 'Arial') ? 'selected' : '';  ?>>Arial</option>
                            <option value="Comic Sans" <?php echo ($themeStyle['text_font'] == 'Comic Sans') ? 'selected' : '';  ?>>Comic Sans</option>
                            <option value="Courier New" <?php echo ($themeStyle['text_font'] == 'Courier New') ? 'selected' : '';  ?> >Courier New</option>
                            <option value="Georgia" <?php echo ($themeStyle['text_font'] == 'Georgia') ? 'selected' : '';  ?>>Georgia</option>
                            <option value="Times New Roman" <?php echo ($themeStyle['text_font'] == 'Times New Roman') ? 'selected' : '';  ?>>Times New Roman</option>
                            <option value="Trebuchet MS" <?php echo ($themeStyle['text_font'] == 'Trebuchet MS') ? 'selected' : '';  ?>>Trebuchet MS</option>
                            <option value="Verdana" <?php echo ($themeStyle['text_font'] == 'Verdana') ? 'selected' : '';  ?>>Verdana</option>
                            <option value="Calibri" <?php echo ($themeStyle['text_font'] == 'Calibri') ? 'selected' : '';  ?>>Calibri</option>
                        </select>
                        <br>
                     </div>
                     <div class="col-sm-4" id="demo-font-text">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                     </div>   
                    </div>

                    <div class="form-group">
                     <label class="col-sm-2 control-label text-success">Status</label>
                     <div class="col-sm-4">
                        <select name="status" class="form-control" required>
                           <option value="1" <?php echo ($schoolData->status == 1) ? 'selected' : '';  ?> >Publish</option>
                           <option value="0"  <?php echo ($schoolData->status == '0') ? 'selected' : '';  ?> >Unpublish</option>
                        </select>
                     </div>
                    
                  </div>                   
                    <label class="col-sm-2 control-label text-success"></label>
                    <div class="col-sm-8">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript" src="<?php bs('public/assets/plugins/color-picker/jscolor.js') ?>"></script>
<script type="text/javascript" src="<?php bs('public/assets/js/container/school-edit.js') ?>"></script>
<?php
    $jsVars = [
        'ajax_call_root' => base_url(),
        'record_id'      =>  $schoolData->id
    ];
    
?>
<script>
jQuery(document).ready(function() { 
    SchoolEdit.init(<?php echo json_encode($jsVars); ?>);
});
</script>