<style type="text/css">
    .li-hidden{ display: none }
</style>
<?php if( $categories ): ?>
<link rel="stylesheet" href="<?php  bs('public/assets/plugins/multiselect/sumoselect.min.css') ?>" />  
<div class="tag-lib-input brd-0 mb-0 pb-0 checkbox-list-search open">
  <div class="SumoSelect sumo_categories" style="width: 100%; ">
        <div>
            <input type="text" class="form-control search_categories" value="" placeholder="Search">
        </div>
        <div class="optWrapper selall multiple" style="display: block;position: relative;">
            <div class="select-all selectChildElement p-3 pl-3">
                <label class="mb-0 w100p checkbox-tel"> 
                    <input type="checkbox" class="mr-3" id="<?php echo $this->input->post('inputEle')?>">Select All
                </label>
            </div>
            <ul class="options p-0" style="padding:0">
                <?php foreach ($categories as $key => $value) { //pr($categories)?>
                    <li class="opt pl-3 m-0 li-parent">
                        <label class="checkbox-tel ">
                            <input type="checkbox" class="mr-3 <?php echo $this->input->post('inputEle')?>" name="categories" value="<?php echo $value['id'] ?>" <?php echo (in_array($value['id'],$selectedData))?'checked':'' ?> data-input-name="<?php echo $value['name'] ?>"><?php echo $value['name'] ?>
                        </label>
                    </li>    
                    <?php if(count($value['children']) > 0){?>
                        <?php foreach ($value['children'] as $key => $child) {?>
                        <li  class="opt pl-4 m-0 li-child">
                             <label class="checkbox-tel">
                                <input type="checkbox" class="mr-3 <?php echo $this->input->post('inputEle')?>" name="categories" value="<?php echo $child['id'] ?>" <?php echo (in_array($child['id'],$selectedData))?'checked':'' ?> data-input-name="<?php echo $child['name'] ?>"><?php echo $child['name'] ?>
                             </label>
                        </li>
                        <?php } ?>
                    <?php } ?>             
                <?php } ?>
            </ul>
            <p class="no-match" style="display:none">No matches for <span></span></p>
        </div>
    </div>   
</div>
<script type="text/javascript" src="<?php bs('public/assets/js/container/cat_section.js') ?>"></script>
<?php

$jsVars = [
    'ajax_call_root' => base_url(),
    'post_data'      => $this->input->post()
];    
?>

<script>
  jQuery(document).ready(function() { 
      CatSection.init(<?php echo json_encode($jsVars); ?>);
  });
</script>
<?php endif;?>