<div class="static-content-wrapper">
<div class="static-content">
<div class="page-content">
<ol class="breadcrumb">
   <li><a href="">Home</a></li>
   <li><a href="#">Update Site</a></li>
</ol>
<div class="container-fluid"><?php print_r($schoolData);?>
<div data-widget-group="group1">
<div class="row">
<div class="col-md-12">
   <div class="panel panel-success">
      <div class="panel-heading">
         <h2><i class="fa fa-file-text-o" aria-hidden="true"></i>Update</h2>
         <div class="panel-ctrls">
            <a href="#" class="button-icon"><i class="ti ti-file"></i></a>
            <a href="#" class="button-icon"><i class="ti ti-mouse"></i></a>
            <a href="#" class="button-icon"><i class="ti ti-settings"></i></a>
         </div>
      </div>
      <div class="panel-body">
         <div class="row">
            <div class="col-md-12">
               <?php if (!empty(validation_errors())): ?>
                  
                  <div class="alert alert-danger">
                     <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                     <strong><?php echo validation_errors(); ?></strong>
                  </div>
                  
               <?php endif ?>

               <form action="<?= base_url('containers/schools/add_new') ?>" enctype="multipart/form-data" method="post" class="form-horizontal row-border" id="setupschool" name="setupschool">
                  <div class="form-group">
                     <label class="col-sm-2 control-label text-success">
                       Name
                     </label>
                     <div class="col-sm-4 sun">
                        <input type="text" name="name" id="name" class="form-control" required>
                     </div>
                     <label class="col-sm-2 control-label text-success">
                       Slowgan 
                     </label>
                     <div class="col-sm-4">
                        <input type="text" name="slogan" class="form-control" required>
                     </div>
                  </div>
                  <div class="form-group">
                      <label class="col-sm-2 control-label text-success">
                       Email
                     </label>
                     <div class="col-sm-4">
                        <input type="text" name="email" class="form-control" required>
                     </div>

                      <label class="col-sm-2 control-label text-success">
                       Phone
                     </label>
                     <div class="col-sm-4">
                        <input type="text" name="phone" class="form-control" required>
                     </div>
                  </div>
                  <div class="form-group">
                      <label class="col-sm-2 control-label text-success">
                       Address
                     </label>
                     <div class="col-sm-4">
                        <input type="text" name="address" class="form-control" required>
                     </div>

                     
                  </div>
                  <div class="form-group">
                     <label class="col-sm-2 control-label text-success">Logo</label>

                        <div class="col-sm-4">
                           <label for="blog_img">
                           <img src="<?php bs() ?>public/assets/img/add-image.png" id="img" width="100" height="100">
                           </label>
                           <input id="blog_img" name="logo" type="file" class="blog_img visible" style="visibility: hidden; height: 0" required>
                        </div>
                    
                  </div>
                  

                  <div class="form-group">
                     <label class="col-sm-2 control-label text-success">Description </label>
                     <div class="col-sm-10">
                        <textarea name="description" id="summernote" class="form-control" rows="5" required></textarea>
                     </div>
                  </div>

                  <div class="form-group">
                     <label class="col-sm-2 control-label text-success">Brand Color </label>
                     <div class="col-sm-10">
                        <input name="brand_color" type="hidden" id="color_value" value="414141">
                        <button class="jscolor {valueElement: 'color_value'}">Pick a color</button>
                     </div>
                  </div>

                  <div class="form-group">
                     <label class="col-sm-2 control-label text-success">Text Color </label>
                     <div class="col-sm-10">
                        <input name="text_color" type="hidden" id="color_value" value="414141">
                        <button class="jscolor {valueElement: 'color_value'}">Pick a color</button>
                     </div>
                  </div>

                  <div class="form-group">
                     <label class="col-sm-2 control-label text-success">Primary heading Color </label>
                     <div class="col-sm-10">
                        <input name="pri_heading_color" type="hidden" id="color_value" value="414141">
                        <button class="jscolor {valueElement: 'color_value'}">Pick a color</button>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-2 control-label text-success">Secondary heading Color </label>
                     <div class="col-sm-10">
                        <input name="sec_heading_color" type="hidden" id="color_value" value="414141">
                        <button class="jscolor {valueElement: 'color_value'}">Pick a color</button>
                     </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label text-success">Font family </label>
                    <div class="col-sm-4">
                        <select name="text_font" id="text_font" class="form-control">
                            <option value="">Select font</option>
                            <option value="Arial">Arial</option>
                            <option value="Comic Sans">Comic Sans</option>
                            <option value="Courier New">Courier New</option>
                            <option value="Georgia">Georgia</option>
                            <option value="Times New Roman">Times New Roman</option>
                            <option value="Trebuchet MS">Trebuchet MS</option>
                            <option value="Verdana">Verdana</option>
                            <option value="Calibri">Calibri</option>
                        </select>
                        <br>
                     </div>
                     <div class="col-sm-4" id="demo-font-text">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                     </div>   
                    </div>

                    <div class="form-group">
                     <label class="col-sm-2 control-label text-success">Status </label>
                     <div class="col-sm-4">
                        <select name="status" class="form-control" required>
                           <option value="1">Publish</option>
                           <option value="0" selected="">Unpublish</option>
                        </select>
                     </div>
                    
                  </div>
                    <!---->                     
                    <label class="col-sm-2 control-label text-success"></label>
                    <div class="col-sm-8">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript" src="<?php bs('public/assets/plugins/color-picker/jscolor.js') ?>"></script>
<script>
function first_img(input) 
{

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#img').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$(".blog_img").change(function() 
{
  first_img(this);
});

function formValidator() {

   jQuery.validator.addMethod("noSpace", function(value, element) { 
      return value == '' || value.trim().length != 0;  
   }, "Space is not allow");

   jQuery.validator.addMethod("emailExt", function(value, element, param) {
      return value.match(/^[a-zA-Z0-9_\.%\+\-]+@[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,}$/);
   },"Please enter valide email address");

   $("#setupschool").validate({
         rules: {
            name: {
                required : true,
                noSpace  : true,
                remote:{
                    url:"<?php echo bs().'containers/schools/getCheckName'?>",
                    type: 'POST'
                }
            },
            slogan: {
               required : true,
               noSpace  : true
            },
            email: {
               required : true,
               noSpace  : true,
               emailExt : true
            },
            phone: {
               required : true,
               noSpace  : true,
               number : true
            },
            address: {
               required : true,
               noSpace  : true
            },
            logo: {
               required : true
            }
         },
         messages: {
            name: {
               required: "Please enter name",
               remote   : "Name is already exist, please try with new",              
            },
            slogan: {
               required: "Please enter slogan"               
            },
            email: {
               required: "Please enter email"               
            },
            phone: {
               required: "Please enter phone number",
               number  : "Please enter valid phone number"               
            },
            address: {
               required: "Please enter address"               
            },
            logo: {
               required: "Please select site logo"               
            }
         },   
         submitHandler: function (form) { // for demo 
            return true;
         }
      });
}

$(document).ready(function() {
    $('#summernote').summernote();
    formValidator();
    console.log(this.value);
    $("#text_font").on('click', function(){
        $("#demo-font-text").css('font-family',this.value);
    });
}); 

</script>