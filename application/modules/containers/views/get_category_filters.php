<?php 
if( $categories ): ?>
<link rel="stylesheet" href="<?php  bs('public/assets/plugins/multiselect/sumoselect.min.css') ?>" />  
<div class="tag-lib-input brd-0 mb-0 pb-0 SumoSelectModal">
  <div class="selectFilterMode form-control" id="<?php echo strtolower(str_replace(" ", "_", $this->input->post('selector'))) ?>"><?php echo $this->input->post('selector') ?> <span></span>
</div>
<div class="SumoSelect sumo_categories select-modal" style="width: 100%; ">
        <div class="optSearch">
            <input type="text" class="form-control search_categories" value="" placeholder="Search">
        </div>
        <div class="optWrapper selall multiple" style="display: block;position: relative;">
            <div class="select-all selectChildElement">
                <label class="checkbox-tel"> 
                    <input type="checkbox" class="mr-3 meta_data_filter" id="<?php echo strtolower(str_replace(" ","_",$this->input->post('selector'))) ?>_check_fil" >Select All
                </label>
            </div>
            <ul class="options p-0" style="padding:0" id="elm_<?php echo strtolower(str_replace(" ","_",$this->input->post('selector')))?>"></ul>
                <script>
                    var obj = <?php echo json_encode($categories); ?>;
                    //console.log(obj);
                    var newObjArr = [];
                    var newChildObjArr ="";
                    var newParentObj = "";
                    var ulElements = "elm_<?php echo strtolower(str_replace(" ","_",$this->input->post('selector')))?>";
                    var notParent = obj.filter( item => item.type >= 0);

                    if(obj[0]['parent']!='undefined'){
                        newParentObj = obj.filter( x => x.parent == 0 );
                        newChildObjArr = obj.filter( x => x.parent > 0 );
                        for( var i=0; i<newParentObj.length; i++){
                            var x = newParentObj[i]['id'];
                            newParentObj[i]['child'] = newChildObjArr.filter( newParentObj => newParentObj.parent == x )
                            
                        }
                         getListOfDropDown(newParentObj)
                    }

                    if(obj[0]['type']!='undefined'){
                        newParentObj = '';
                        var newParentObjtype = notParent;
                        getListOfDropDown(newParentObjtype)
                    }
                    
                    
                    function getListOfDropDown(containerObjId){
                        //console.log(containerObjId)
                        $("#"+ulElements).each(function(){
                        for(var j=0; j<containerObjId.length;j++){
                            var parentListOfLi = $('<li class="opt m-0 li-parent"><label class="checkbox-tel "><input type="checkbox" class="mr-3 meta_data_filter <?php echo strtolower(str_replace(" ","_",$this->input->post("selector")))?>_check_fil" name="categories" value="'+containerObjId[j]['id']+'" data-input-name="'+containerObjId[j]['id']+'" />'+containerObjId[j]['name']+'</label></li>');
                            if(typeof containerObjId[j]['child'] != "undefined"){
                                if(containerObjId[j]['child'].length > 0 ){
                                    var childListOfLi = [];
                                    var chilUl = $("<ul></ul>");
                                    for( var k=0; k<containerObjId[j]['child'].length; k++){
                                        childListOfLi.push( $('<li  class="opt m-0 li-child"><label class="checkbox-tel"><input type="checkbox" class="mr-3 meta_data_filter <?php echo strtolower(str_replace(" ","_",$this->input->post("selector")))?>_check_fil" name="categories" value="'+containerObjId[j]['child'][k]['id']+'" data-input-name="'+containerObjId[j]['child'][k]['name']+'" /> '+containerObjId[j]['child'][k]['name']+'</label></li>'));
                                    }
                                    chilUl.html(childListOfLi)[j];
                                    parentListOfLi.children("label").after(chilUl);
                                }
                            }
                            $(this).append(parentListOfLi)
                            
                        }
                        
                    })
                    }
                </script>            
            <p class="no-match" style="display:none">No matches for <span></span></p>
        </div>
    </div>   
</div>
<script type="text/javascript" src="<?php bs('public/assets/js/meta_filters/get_category_filters.js') ?>"></script>
<?php

$jsVars = [
    'ajax_call_root' => base_url(),
    'post_data'      => $this->input->post()
];    
?>

<script>
    jQuery(document).ready(function() { 
        CatFilters.init(<?php echo json_encode($jsVars); ?>);
    });

    $(".SumoSelect.select-modal").on('click',function(e){
        e.stopPropagation();
    });
    $(".selectFilterMode").off("click").on("click", function(e){
        e.preventDefault();
        e.stopPropagation();
        $(this).toggleClass('active');
        if($(this).hasClass('active')){
            $('.SumoSelect.select-modal').removeClass('open')
            $(this).siblings('.SumoSelect.select-modal').addClass('open');
            $(".selectFilterMode").removeClass('active')
        }else{
            $(this).siblings('.SumoSelect.select-modal').removeClass('open');
            $('.SumoSelect.select-modal').removeClass('open')
            $(".selectFilterMode").removeClass('active')
        }
    });
    
    $('body').click(function(){
        $('.SumoSelect.select-modal').removeClass('open')
        $(".selectFilterMode").removeClass('active')
    })
</script>
<?php endif;?>