<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
Author Akash Hedaoo
Date 16/04/2019
*/

class LearningStandard extends MY_Controller
{
    public function __construct()
    {
        //ini_set('display_errors', 1);
        parent::__construct();
        $this->load->module('template'); 
        $this->load->model(['Common_model', 'LearningStandard_model']);        
        $this->load->helper(array('html', 'form'));
        $this->load->library('form_validation');

        if (!$this->ion_auth->logged_in()):
            redirect('users/auth', 'refresh');
        endif;
    }

    /**
     * index method
     * @description this function use to display list of categories 
     * @return void
     */
    public function index()
    {       
        $data['page'] = "general_modules/LearningStandard/list";
        $this->template->template_view($data);
    }


    /**
     * getLists method 
     * @description this function called via ajax request, use to display list of lesson
     * @return void
     */         
    public function getLists() {

        $data = $row = [];
        $LearningStandardData = $this->LearningStandard_model->getRows($_POST); 
        
        $i = $_POST['start']; $cnt=1;
        foreach ($LearningStandardData as $key => $learningStds) {
            $data[] = [
                        'id' => $learningStds->id,
                        'cnt' => $cnt,
                        'name' => $learningStds->name,
                        'created_by' => ucfirst($learningStds->first_name).' '.ucfirst($learningStds->last_name),
                        'created' => date('m/d/Y', strtotime($learningStds->created)),
                        'status' => ($learningStds->status==1) ? 'Publish' : 'Unpublish',
                    ]; 
                    $cnt++;                 
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->LearningStandard_model->count_records(),
            "recordsFiltered" => $this->LearningStandard_model->countFiltered($_POST),
            "data" => $data,
        );
        // Output to JSON format
        echo json_encode($output);
    }


    /**
     * add method 
     * @description this method is use to create new category
     * @return void
     */
    public function add() {        

        if ($this->input->post()) 
        {            
            $this->form_validation->set_rules('name', '', 'trim|required');

            if ($this->form_validation->run() == FALSE)  {  
                set_flashdata('error','Learning standard has been added successfully');
                $data['page'] = "general_modules/LearningStandard/add";
                $this->template->template_view($data);
            }else{  
                $data = [
                        'created_by' => $this->session->userdata('user_id'),
                        'name'  => post('name'), 
                        'status'  => post('status')
                    ];
                $this->Common_model->InsertData('learning_standards',$data);                
                set_flashdata('success','Learning standard has been added successfully');
                redirect('general_modules/LearningStandard/','refresh');
            }
        }else {      

            $data['page'] = 'general_modules/LearningStandard/add';
            $this->template->template_view($data);
        }
    }


    /**
     * edit method 
     * @description this method is use to update category
     * @return void
     */
    public function edit($id) {

        if ($this->input->post()) 
        {                       
            $this->form_validation->set_rules('name', '', 'trim|required');

            if ($this->form_validation->run() == FALSE)  {               
                $data['page'] = "general_modules/LearningStandard/edit/";
                $this->template->template_view($data);
            }else if($id) {  
                $data = [
                        'created_by' => $this->session->userdata('user_id'),
                        'name'  => post('name'), 
                        'status'  => post('status') 
                    ];
                $this->Common_model->UpdateDB('learning_standards', [ 'id' => $id ], $data);            
                set_flashdata('success','LearningStandard has been updated successfully');
                redirect('general_modules/LearningStandard/','refresh');
            }else {
                set_flashdata('error','Unable to update category, try again');
                redirect('general_modules/LearningStandard/','refresh');
            }
        }else {      
            if($id) { 
                $data['learingStandardData'] = $this->Common_model->getDataById('learning_standards', '*', ['id' => $id] );
                $data['page'] = 'general_modules/LearningStandard/edit';
                $this->template->template_view($data);
            }else {
                set_flashdata('error','Unable to update category, try again');
                redirect('general_modules/LearningStandard/','refresh');
            }
        }        
    }


    /**
     * delete method
     * @description this function use to delete tempray record 
     * @return void
     */ 
    function delete() {
        $id = $this->uri->segment(4);      
        $data = ['delete_status' => 0];
        if($id) {
            $this->Common_model->UpdateDB('learning_standards', [ 'id' => $id ], $data);
            $this->session->set_flashdata('success', $this->ion_auth->messages());
            $msg = "deleted";    
            echo json_encode(array("msg"=>$msg)); exit;
        }else {
            set_flashdata('error','Unable to delete category, try again');
            redirect('general_modules/LearningStandard/','refresh');
        }
    }

     /**
     * detail method
     * @description this function use to view complete learning standard details
     * @return void
     */ 
    public function detail() {
        $learning_id = $this->uri->segment(4);
        if(isset($learning_id)) {
            $data['learningStandardData'] = $this->Common_model->getDataById('learning_standards','*', ['id' => $learning_id]);                       
            $data['page'] = "general_modules/LearningStandard/detail";
            $this->template->template_view($data);
        }
    }

    
}

