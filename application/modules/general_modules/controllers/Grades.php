<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Grades extends MY_Controller
{

    public function __construct()
    {

        parent::__construct();
        //Do your magic here

        $this->load->module('template');
        $this->load->model('common_model');
        $this->load->helper(['html', 'form']);
        $this->load->library('form_validation');

        if (!$this->ion_auth->logged_in()) :
            redirect('users/auth', 'refresh');
        endif;

        if (!$this->ion_auth->is_admin()) :
            return show_error("You Must Be An Administrator To View This Page");
        endif;
    }

    /**
     * setup method
     * @description this function use to create container
     * @return void
     */
    public function grades_list()
    {
        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Add', 'link' => '', 'class' => 'active'];
        $data['grades'] = $this->common_model->getAllData('grades', '*', '');
        $data['page'] = "general_modules/grades/grades_list";



        $this->template->template_view($data);
    }

    /**
     * add_new method
     * @description this method is use to insert site creatation data in databse
     * @param string, numbers
     * @return void
     */
    public function add_grades()
    {

        if ($this->input->post()) {

            $validation_rules = [
                [
                    'field' => 'name',
                    'label' => 'Name',
                    'rules' => 'required',
                    'errors' => [
                        'required' => 'Please enter grade name',
                    ]
                ]
            ];
            $this->form_validation->set_rules($validation_rules);
            if ($this->form_validation->run() == FALSE) {
                $data['page'] = "general_modules/grades/add_grades";
                $this->template->template_view($data);
            } else {
                $data = array('name' => trim(post('name')));
                $this->common_model->InsertData('grades', $data);
                set_flashdata('success', 'Grade has been added successfully');
                redirect('general_modules/grades/grades_list', 'refresh');
            }
        } else {
            $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
            $data['breadcrumb'][]           = ['title' => 'Grades', 'link' => base_url('general_modules/grades/grades_list'), 'class' => ''];
            $data['breadcrumb'][]           = ['title' => 'Add', 'link' => '', 'class' => 'active'];
            $data['page'] = "general_modules/grades/add_grades";
            $this->template->template_view($data);
        }
    }

    public function edit_grades($id = NULL)
    {
        if ($this->input->post()) {

            $validation_rules = [
                [
                    'field' => 'name',
                    'label' => 'Name',
                    'rules' => 'required',
                    'errors' => [
                        'required' => 'Please enter name',
                    ],
                ]
            ];
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run() == FALSE) {
                $data['page'] = "general_modules/grades/edit_grades";
                $this->template->template_view($data);
            } else {
                $data = array('name' => trim(post('name')));
                $this->common_model->UpdateDB('grades', ['id' => $id], $data);
                set_flashdata('success', 'Grades has been updated Successfully');
                redirect('general_modules/grades/grades_list/', 'refresh');
            }
        } else {
            $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
            $data['breadcrumb'][]           = ['title' => 'Grades', 'link' => base_url('general_modules/grades/grades_list'), 'class' => ''];
            $data['breadcrumb'][]           = ['title' => 'Edit', 'link' => '', 'class' => 'active'];
            $data['grades'] = $this->common_model->getDataById('grades', '*', ['id' => $id]);
            $data['page'] = "general_modules/grades/edit_grades";
            $this->template->template_view($data);
        }
    }

    function check_default($post_string)
    {
        if ($this->input->post('country') === 'selectcountry') {
            $this->form_validation->set_message('country_check', 'Please choose your country.');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function delete_grades()
    {
        $id = $this->uri->segment(4);
        $this->common_model->DeleteDB('grades',  ['id' => $id]);
        set_flashdata('success', 'Grades has been deleted Successfully');
        redirect('general_modules/grades/grades_list/', 'refresh');
    }
}
