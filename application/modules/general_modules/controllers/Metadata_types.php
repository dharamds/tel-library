<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
  Author Akash Hedaoo
  Date 16/04/2019
 */

class Metadata_types extends MY_Controller
{

    public function __construct()
    {
        //ini_set('display_errors', 1);
        parent::__construct();
        $this->load->module('template');
        $this->load->model(['Common_model', 'Metadata_types_model']);
        $this->load->helper(array('html', 'form'));
        $this->load->library('form_validation');

        if (!$this->ion_auth->logged_in()) :
            redirect('users/auth', 'refresh');
        endif;
    }

    /**
     * index method
     * @description this function use to display list of Metadata_types 
     * @return void
     */
    public function index()
    {
        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Metadata types', 'link' => '', 'class' => 'active'];

        $data['page'] = "general_modules/Metadata_types/list";
        $this->template->template_view($data);
    }

    /**
     * getLists method 
     * @description this function called via ajax request, use to display list of lesson
     * @return void
     */
    public function getLists()
    {
        $Metadata_types = $this->Metadata_types_model->getRows($_POST);
        # Fetch metdata types
        $metadata_types1 = $this->Common_model->fetch_metadata_types('metadata_types');
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $Metadata_types['total'],
            "recordsFiltered" => $Metadata_types['total'],
            "data" => $Metadata_types['result'],
        );
        // Output to JSON format
        echo json_encode($output);
    }

    /**
     * add method 
     * @description this method is use to create new Metadata_types
     * @return void
     */
    public function add()
    {
        if ($this->input->post()) {
            $this->form_validation->set_rules('description', 'Metadata type description', 'trim|required');
            $this->form_validation->set_rules('name', 'Metadata type name', 'trim|required', array('required' => 'Please enter Metadata type name'));

            if ($this->form_validation->run() == FALSE) {
                $data['page'] = "general_modules/Metadata_types/add";
                $this->template->template_view($data);
            } else {
                
                    $data = [
                        'description' => post('description'),
                        'name' => post('name'),
                    ];
                    $this->Common_model->InsertData('Metadata_types', $data);
                    set_flashdata('success', 'Metadata types has been added successfully');
                    redirect('general_modules/Metadata_types/', 'refresh');
                
            }
        }
        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Metadata_types', 'link' => base_url('general_modules/Metadata_types'), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Add', 'link' => '', 'class' => 'active'];

        $data['metadata_types'] = $this->Common_model->fetch_metadata_types('metadata_types');
        $data['page'] = 'general_modules/Metadata_types/add';
        $this->template->template_view($data);
    }

    /**
     * edit method 
     * @description this method is use to update Metadata_types
     * @return void
     */
    public function edit($id)
    {
        
        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Metadata_types', 'link' => base_url('general_modules/Metadata_types'), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Edit', 'link' => '', 'class' => 'active'];
        if ($this->input->post()) {
            $this->form_validation->set_rules('description', 'Metadata type type', 'trim|required');
            $this->form_validation->set_rules('name', 'Metadata type name', 'trim|required');

            
            if ($this->form_validation->run() == FALSE) {
                $data['page'] = "general_modules/Metadata_types/edit/";
                $this->template->template_view($data);
            } else if ($id) {
                
                    $data = [
                        'name' => post('name'),
                        'description' => post('description'),
                    ];
                    $this->Common_model->UpdateDB('metadata_types', ['id' => $id], $data);
                    
                    set_flashdata('success', 'Metadata types has been updated successfully');
                    redirect('general_modules/Metadata_types/', 'refresh');
                
            } else {
                set_flashdata('error', 'Unable to update Metadata Types, try again');
                redirect('general_modules/Metadata_types/', 'refresh');
            }
        } else {
            if ($id) {
                $data['Metadata_types'] = $this->Common_model->getDataById('metadata_types', '*', ['id' => $id]);
                //$data['metadata_types'] = $this->Common_model->fetch_metadata_types('metadata_types');
                $data['page'] = 'general_modules/Metadata_types/edit';
                $this->template->template_view($data);
            } else {
                set_flashdata('error', 'Unable to update Metadata Types, try again');
                redirect('general_modules/Metadata_types/', 'refresh');
            }
        }
    }

    /**
     * delete method
     * @description this function use to delete tempray record 
     * @return void
     */
    function delete()
    {
        $id = $this->uri->segment(4);
        $data = ['delete_status' => 0];
        if ($id) {
            $this->Common_model->UpdateDB('metadata_types', ['id' => $id], $data);
            $this->session->set_flashdata('success', $this->ion_auth->messages());
            $msg = "deleted";
            echo json_encode(array("msg" => $msg));
            exit;
        } else {
            set_flashdata('error', 'Unable to delete metadata types, try again');
            redirect('general_modules/Metadata_types/', 'refresh');
        }
    }

    /**
     * detail method
     * @description this function use to view complete question details
     * @return void
     */
    public function detail()
    {
        $Metadata_types_id = $this->uri->segment(4);
        if (isset($Metadata_types_id)) {
            $data['Metadata_types'] = $this->Common_model->getDataById('metadata_types', '*', ['id' => $Metadata_types_id]);
            $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
            $data['breadcrumb'][]           = ['title' => 'Metadata types', 'link' => base_url('general_modules/Metadata_types'), 'class' => ''];
            $data['breadcrumb'][]           = ['title' => 'View', 'link' => '', 'class' => 'active'];

            $data['page'] = "general_modules/Metadata_types/detail";
            $this->template->template_view($data);
        }
    }

    /**
     * detail method
     * @update_status this function use to update status
     * @return void
     */
    public function update_status($id, $action)
    {
        $status = ($action == 'activate') ? 1 : 0;
        $data = array('status' => $status);
        $this->Metadata_types_model->update_status($id, $data);
        $msg = "Updated";
        echo json_encode(['msg' => $msg]);
        exit;
    }
}
