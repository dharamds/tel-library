<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
  Author Akash Hedaoo
  Date 16/04/2019
 */

class Tags extends MY_Controller
{

    public function __construct()
    {
        ini_set('display_errors', 1);
        parent::__construct();
        $this->load->module('template');
        $this->load->model(['Common_model', 'Tags_model']);
        $this->load->helper(array('html', 'form'));
        $this->load->library('form_validation');
        // cache settings
        $this->config->set_item('cache_path', APPPATH. '/cache/metadata/');
        $this->load->driver('cache',
        array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'tel_')
        );

        if (!$this->ion_auth->logged_in()) :
            redirect('users/auth', 'refresh');
        endif;
    }

    /**
     * index method
     * @description this function use to display list of tags 
     * @return void
     */
    public function index()
    {
        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Tags', 'link' => '', 'class' => 'active'];
        $data['metadata_types']         = $this->Common_model->fetch_metadata_types('metadata_types');
        $data['page'] = "general_modules/Tags/list";
        $this->template->template_view($data);
    }

    /**
     * getLists method 
     * @description this function called via ajax request, use to display list of lesson
     * @return void
     */
    public function getLists()
    {
        $tagsData = $this->Tags_model->getRows($_POST);
        # Fetch metdata types
        $metadata_types = $this->Common_model->fetch_metadata_types('metadata_types');
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $tagsData['total'],
            "recordsFiltered" => $tagsData['total'],
            "data" => $tagsData['result'],
        );
        // Output to JSON format
        echo json_encode($output);
    }

    /**
     * add method 
     * @description this method is use to create new tags
     * @return void
     */
    public function add()
    {
        if ($this->input->post()) {
            $this->form_validation->set_rules('type', 'Tag type', 'trim|required');
            $this->form_validation->set_rules('name', 'Tag name', 'trim|required', array('required' => 'Please enter tags name'));

            if ($this->form_validation->run() == FALSE) {
                $data['page'] = "general_modules/Tags/add";
                $this->template->template_view($data);
            } else {
                $this->db->select('*');
                $this->db->from('tags');
                $this->db->where('type', $this->input->post('type'));
                $this->db->where('name', $this->input->post('name'));
                $query = $this->db->get();
                if ($query->num_rows() == 0) {
                    $data = [
                        'created_by' => $this->session->userdata('user_id'),
                        'type' => post('type'),
                        'name' => post('name'),
                    ];

                    $this->Common_model->InsertData('tags', $data);
                    
                    clean_cache('metadata', 'tel_meta_filter_cache_2');

                   
                    set_flashdata('success', 'Tag has been added successfully');
                    redirect('general_modules/Tags/', 'refresh');
                } else {
                    set_flashdata('error', 'Tag name with same metadata type is already exist !');
                    redirect('general_modules/Tags/add', 'refresh');
                }
            }
        }
        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Tags', 'link' => base_url('general_modules/Tags'), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Add', 'link' => '', 'class' => 'active'];

        $data['metadata_types'] = $this->Common_model->fetch_metadata_types('metadata_types');
        $data['page'] = 'general_modules/Tags/add';
        $this->template->template_view($data);
    }

    /**
     * edit method 
     * @description this method is use to update tags
     * @return void
     */
    public function edit($id)
    {
        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Tags', 'link' => base_url('general_modules/Tags'), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Edit', 'link' => '', 'class' => 'active'];
        if ($this->input->post()) {
            $this->form_validation->set_rules('type', 'Tag type', 'trim|required');
            $this->form_validation->set_rules('name', 'Tag name', 'trim|required');


            if ($this->form_validation->run() == FALSE) {
                $data['page'] = "general_modules/Tags/edit/";
                $this->template->template_view($data);
            } else if ($id) {

                clean_cache('metadata', 'tel_meta_filter_cache_2');

                $this->db->select('*');
                $this->db->from('tags');
                $this->db->where('type', $this->input->post('type'));
                $this->db->where('name', $this->input->post('name'));
                $query = $this->db->get();
                if ($query->num_rows() == 0) {
                    $data = [
                        'created_by' => $this->session->userdata('user_id'),
                        'type' => post('type'),
                        'name' => post('name'),
                    ];
                    $this->Common_model->UpdateDB('tags', ['id' => $id], $data);
                    
                    clean_cache('metadata', 'tel_meta_filter_cache_2');

                    set_flashdata('success', 'Tag has been updated successfully');
                    redirect('general_modules/Tags/', 'refresh');
                } else {
                    set_flashdata('error', 'Tag name with same metadata type is already exist !');
                    redirect('general_modules/Tags/edit/' . $id, 'refresh');
                }
            } else {
                set_flashdata('error', 'Unable to update tags, try again');
                redirect('general_modules/Tags/', 'refresh');
            }
        } else {
            if ($id) {
                $data['tagsData'] = $this->Common_model->getDataById('tags', '*', ['id' => $id]);
                $data['metadata_types'] = $this->Common_model->fetch_metadata_types('metadata_types');
                //echo "sss"; exit;
                $data['page'] = 'general_modules/Tags/edit';
                $this->template->template_view($data);
            } else {
                set_flashdata('error', 'Unable to update tags, try again');
                redirect('general_modules/Tags/', 'refresh');
            }
        }
    }

    /**
     * delete method
     * @description this function use to delete tempray record 
     * @return void
     */
    function delete()
    {
        $id = $this->uri->segment(4);
        $data = ['delete_status' => 0];
        if ($id) {
            $this->Common_model->UpdateDB('tags', ['id' => $id], $data);
            $this->session->set_flashdata('success', $this->ion_auth->messages());
            $msg = "deleted";
            clean_cache('metadata', 'tel_meta_filter_cache_2');
            echo json_encode(array("msg" => $msg));
            exit;
        } else {
            set_flashdata('error', 'Unable to delete tags, try again');
            redirect('general_modules/Tags/', 'refresh');
        }
    }

    /**
     * detail method
     * @description this function use to view complete question details
     * @return void
     */
    public function detail()
    {
        $tags_id = $this->uri->segment(4);
        if (isset($tags_id)) {
            $data['tagsData'] = $this->Common_model->getDataById('tags', '*', ['id' => $tags_id]);
            $data['metadata_types'] = $this->Common_model->fetch_metadata_types('metadata_types');
            $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
            $data['breadcrumb'][]           = ['title' => 'Tags', 'link' => base_url('general_modules/Tags'), 'class' => ''];
            $data['breadcrumb'][]           = ['title' => 'View', 'link' => '', 'class' => 'active'];

            $data['page'] = "general_modules/Tags/detail";
            $this->template->template_view($data);
        }
    }

    /**
     * detail method
     * @update_status this function use to update status
     * @return void
     */
    public function update_status($id, $action)
    {
        $status = ($action == 'activate') ? 1 : 0;
        $data = array('status' => $status);
        $this->Tags_model->update_status($id, $data);
        clean_cache('metadata', 'tel_meta_filter_cache_2');
        $msg = "Updated";
        echo json_encode(['msg' => $msg]);
        exit;
    }
}
