<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
  Author Akash Hedaoo
  Date 16/04/2019
 */

class Gallery_Images extends MY_Controller
{

    public function __construct()
    {
        //ini_set('display_errors', 1);
        parent::__construct();
        $this->load->module('template');
        $this->load->model(['Common_model','Gallery_Images_model']);
        $this->load->helper(array('html', 'form'));
        $this->load->library('form_validation');

        if (!$this->ion_auth->logged_in()) :
            redirect('users/auth', 'refresh');
        endif;
    }

    public function gallery_images_popup()
    {
        $data['id']             = $_POST['id'];
        $data['module_id']      = $_POST['module_id'];
        $data['call_back']      = $_POST['call_back'];
        $data['instanceName']   = $_POST['instanceName'];
        $this->load->view("general_modules/Gallery_Images/gallery_images_popup", $data);
        
    }
    
    public function filter_data($page = 0)
    {
        $data['data'] = $this->Gallery_Images_model->filter_data($_POST['media_cat'], $_POST['media_tag'], $_POST['search'], $page);
        $data['page'] = $page + 24; 
        $this->load->view("general_modules/Gallery_Images/gallery_images_search_data", $data);
    }

    
}
