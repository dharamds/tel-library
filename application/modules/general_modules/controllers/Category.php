<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
  Author Akash Hedaoo
  Date 16/04/2019
 */

class Category extends MY_Controller
{

    public function __construct()
    {
        //ini_set('display_errors', 1);
        parent::__construct();
        $this->load->module('template');
        $this->load->model(['Common_model', 'Category_model']);
        $this->load->helper(array('html', 'form'));
        $this->load->library('form_validation');
        $this->config->set_item('cache_path', APPPATH. '/cache/metadata/');
        $this->load->driver('cache',
        array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'tel_')
		);
        if (!$this->ion_auth->logged_in()) :
            redirect('users/auth', 'refresh');
        endif;
    }

    /**
     * index method
     * @description this function use to display list of categories 
     * @return void
     */
    public function index()
    {
        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Categories', 'link' => '', 'class' => 'active'];
        $data['metadata_types']         = $this->Common_model->fetch_metadata_types('metadata_types');
        $data['page']                   = "general_modules/Category/list";
        $this->template->template_view($data);
    }

    /**
     * getLists method 
     * @description this function called via ajax request, use to display list of lesson
     * @return void
     */
    public function getLists()
    {
        $categoryData = $this->Category_model->getRows($_POST);
        # Fetch metdata types
        $metadata_types = $this->Common_model->fetch_metadata_types('metadata_types');

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $categoryData['total'],
            "recordsFiltered" => $categoryData['total'],
            "data" => $categoryData['result'],
        );
        // Output to JSON format
        echo json_encode($output);
    }

    /**
     * add method 
     * @description this method is use to create new category
     * @return void
     */
    public function add()
    {
        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Categories', 'link' => base_url('general_modules/Category'), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Add', 'link' => '', 'class' => 'active'];
        if ($this->input->post()) {
            $this->form_validation->set_rules('type', 'Category type', 'trim|required');
            $this->form_validation->set_rules('name', 'Category name', 'trim|required', array('required' => 'Please enter category name'));

            if ($this->form_validation->run() == FALSE) {
                $data['page'] = "general_modules/Category/add";

                $this->template->template_view($data);
            } else {
                $this->db->select('*');
                $this->db->from('categories');
                $where = [
                    "type" => $this->input->post('type'),
                    "name" => $this->input->post('name'),
                    "delete_status" => 1,
                    "status" => 1
                ];
                $this->db->where($where);
                
                $query = $this->db->get();
                
                if ($query->num_rows() == 0) {
                    $data = [
                        'created_by' => $this->session->userdata('user_id'),
                        'type' => post('type'),
                        'name' => post('name'),
                        'parent' => post('parent_category'),
                        'description' => post('description'),
                    ];
                    $this->Common_model->InsertData('categories', $data);
                    clean_cache('metadata', 'tel_meta_filter_cache_1');
                   // clean_cache('metadata', 'tel_meta_filter_cache_1');
                    set_flashdata('success', 'Category has been added successfully');
                    redirect('general_modules/Category/', 'refresh');
                } else {
                    set_flashdata('error', 'Category name with same metadata type is already exist !');
                    redirect('general_modules/Category/add', 'refresh');
                }
            }
        }
        $data['metadata_types'] = $this->Common_model->fetch_metadata_types('metadata_types');
        $data['parent_metadata_types_categories'] = $this->Common_model->fetch_parent_metadata_types_categories('categories');
        $data['page'] = 'general_modules/Category/add';
        $this->template->template_view($data);
    }
         
    /**
     * edit method 
     * @description this method is use to update category
     * @return void
     */
    public function edit($id)
    {
        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Categories', 'link' => base_url('general_modules/category'), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Edit', 'link' => '', 'class' => 'active'];
        if ($this->input->post()) {
            $this->form_validation->set_rules('type', 'Category type', 'trim|required');
            $this->form_validation->set_rules('name', 'Category name', 'trim|required', array('required' => 'Please enter category name'));

            if ($this->form_validation->run() == FALSE) {
                $data['page'] = "general_modules/Category/edit";
                $this->template->template_view($data);
            } else if ($id) {
                $this->db->select('*');
                $this->db->from('categories');
                $where = [
                    "type" => $this->input->post('type'),
                    "name" => $this->input->post('name'),
                    "delete_status" => 1,
                    "status" => 1,
                    "id !=" => $id
                ];
                $this->db->where($where);
                $query = $this->db->get();
                if ($query->num_rows() == 0) {
                    $data = [
                        'created_by' => $this->session->userdata('user_id'),
                        'type' => post('type'),
                        'name' => post('name'),
                        'parent' => post('parent_category'),
                        'description' => post('description'),

                    ];
                    $this->Common_model->UpdateDB('categories', ['id' => $id], $data);
                    // clean metacache
                    clean_cache('metadata', 'tel_meta_filter_cache_1');
                    set_flashdata('success', 'Category has been updated successfully');
                    redirect('general_modules/Category/', 'refresh');
                } else {
                    set_flashdata('error', 'Category name with same metadata type is already exist !');
                    redirect('general_modules/Category/edit/' . $id, 'refresh');
                }
            } else {
                set_flashdata('error', 'Unable to update category, try again');
                redirect('general_modules/Category/', 'refresh');
            }
        } else {
            if ($id) {
                $data['categoryData'] = $this->Common_model->getDataById('categories', '*', ['id' => $id]);
                $data['metadata_types'] = $this->Common_model->fetch_metadata_types('metadata_types');
                $data['parent_metadata_types_categories'] = $this->Common_model->fetch_parent_metadata_types_categories('categories');
                $data['menus'] = $this->Category_model->fetch_menus();
                $data['page'] = 'general_modules/Category/edit';
                $this->template->template_view($data);
            } else {
                set_flashdata('error', 'Unable to update category, try again');
                redirect('general_modules/Category/', 'refresh');
            }
        }
    }

    /**
     * delete method
     * @description this function use to delete tempray record 
     * @return void
     */
    function delete()
    {
        $id = $this->uri->segment(4);
        $data = ['delete_status' => 0];
        if ($id) {
            $this->Common_model->UpdateDB('categories', ['id' => $id], $data);
            $this->session->set_flashdata('success', $this->ion_auth->messages());
            $msg = "deleted";
            clean_cache('metadata', 'tel_meta_filter_cache_1');
            //$this->cache->clean();
            echo json_encode(array("msg" => $msg));
            exit;
        } else {
            set_flashdata('error', 'Unable to delete category, try again');
            redirect('general_modules/Category/', 'refresh');
        }
    }

    /**
     * detail method
     * @description this function use to view complete question details
     * @return void
     */
    public function detail()
    {
        $category_id = $this->uri->segment(4);
        if (isset($category_id)) {
            $data['categoryData'] = $this->Common_model->getDataById('categories', '*', ['id' => $category_id]);
            $data['metadata_types'] = $this->Common_model->fetch_metadata_types('metadata_types');
            $data['page'] = "general_modules/Category/detail";
            $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
            $data['breadcrumb'][]           = ['title' => 'Categories', 'link' => base_url('general_modules/Category'), 'class' => ''];
            $data['breadcrumb'][]           = ['title' => 'View', 'link' => '', 'class' => 'active'];
            $this->template->template_view($data);
        }
    }

    /**
     * detail method
     * @update_status this function use to update status
     * @return void
     */
    public function update_status($id, $action)
    {
        $status = ($action == 'activate') ? 1 : 0;
        $data = array('status' => $status);
        //$this->cache->clean();
        $this->Category_model->update_status($id, $data);
        clean_cache('metadata', 'tel_meta_filter_cache_1');
        $msg = "Updated";
        echo json_encode(['msg' => $msg]);
        exit;
    }

    /**
     * getLists method 
     * @description this function called via ajax request, use to display list of lesson
     * @return void
     */
    public function menu_filter_data()
    {
        $menu_filter_data = $this->Category_model->menu_filter_data($_POST);
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $menu_filter_data['total'],
            "recordsFiltered" => $menu_filter_data['total'],
            "data" => $menu_filter_data['result'],
        );
        // Output to JSON format
        echo json_encode($output);
    }
}
