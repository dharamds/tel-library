<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
  Author Akash Hedaoo
  Date 16/04/2019
 */

class Glossary extends MY_Controller {

    public function __construct() {
        //ini_set('display_errors', 1);
        parent::__construct();
        $this->load->module('template');
        $this->load->model(['Common_model', 'Glossary_model']);
        $this->load->helper(array('html', 'form'));
        $this->load->library('form_validation');

        if (!$this->ion_auth->logged_in()) :
            redirect('users/auth', 'refresh');
        endif;
        // get controller permissions
        if ($this->current_user_permissions = $this->get_permissions()) {
            $this->add_permission = $this->current_user_permissions->add_per;
            $this->edit_permission = $this->current_user_permissions->edit_per;
            $this->delete_permission = $this->current_user_permissions->delete_per;
            $this->view_permission = $this->current_user_permissions->view_per;
            $this->list_permission = $this->current_user_permissions->list_per;
        }
        // get container_id
        $this->container_id = get_container_id();
    }

    /**
     * index method
     * @description this function use to display list of categories 
     * @return void
     */
    public function index() {
        // check permissions
        if (!$this->list_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect('users/auth', 'refresh');
        endif;
        $data['permissions'] = $this->current_user_permissions;
        $data['breadcrumb'][] = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][] = ['title' => 'Glossary', 'link' => '', 'class' => 'active'];

        $data['page'] = "general_modules/Glossary/list";
        $this->template->template_view($data);
    }

    /**
     * getLists method 
     * @description this function called via ajax request, use to display list of lesson
     * @return void
     */
    public function getLists() {
        
        $categoryData = $this->Glossary_model->getRows($_POST);
        # Fetch metdata types
        $metadata_types = $this->Common_model->fetch_metadata_types('metadata_types');

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $categoryData['total'],
            "recordsFiltered" => $categoryData['total'],
            "data" => $categoryData['result'],
        );
        // Output to JSON format
        echo json_encode($output);
    }

    /**
     * add method 
     * @description this method is use to create new category
     * @return void
     */
    public function add($id = NULL) {
           // check permissions
        if ($id > 0):
            if ($id && !$this->edit_permission && !$this->ion_auth->is_admin()) :
                $this->session->set_flashdata('error', $this->lang->line('access_denied'));
                redirect($_SERVER['HTTP_REFERER'], 'refresh');
            endif;
        else:
            if (!$this->add_permission && !$this->ion_auth->is_admin()) :
                $this->session->set_flashdata('error', $this->lang->line('access_denied'));
                redirect($_SERVER['HTTP_REFERER'], 'refresh');
            endif;
        endif;


        $data['breadcrumb'][] = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][] = ['title' => 'Glossary', 'link' => base_url('/glossary'), 'class' => ''];
        //$data['breadcrumb'][] = ['title' => 'Add', 'link' => '', 'class' => 'active'];



        if ($id != null) {
            $data['glossaryData'] = $this->common_model->getDataById('glossaries', '*', ['id' => $id]);
            $data['breadcrumb'][] = ['title' => 'Edit', 'link' => '', 'class' => 'active'];
            $data['categories'] = $this->common_model->selectCategoryMetadata('categories', 1); // 1=system
        } else {
            $data['breadcrumb'][] = ['title' => 'Add', 'link' => '', 'class' => 'active'];
        }

        $data['metadata_types'] = $this->Common_model->fetch_metadata_types('metadata_types');
        $data['page'] = 'general_modules/Glossary/add';
        $this->template->template_view($data);
    }

    public function upload_docs_image() {
        $data = [];
        $html = '';
        $file_element_name = 'file';
        $config['upload_path'] = './uploads/glossary_audio/';
        $config['allowed_types'] = 'mp3|wav|ogg';
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload($file_element_name)) {
            $status = 'error';
            $data['error'] = $this->upload->display_errors('', '');
        } else {
            $fileData = $this->upload->data();
            $html .= ' <audio id="" controls>';
            $html .= ' <source id="audioSource" src="' . base_url('/uploads/glossary_audio/') . '' . $fileData['file_name'] . '" type="audio/mp3"> ';
            $html .= ' </audio>';
            $data['fileName'] = $fileData['file_name'];
            $data['html'] = $html;
        }
        
        echo json_encode($data);
        exit;
    }

    public function addGlossary() {

        $postData = [];
        $postData['term'] = trim($this->input->post('term'));
        $postData['media_file'] = trim($this->input->post('glossaryFileName'));
        $postData['created_by'] = $this->session->userdata('user_id');
        $postData['long_title'] = trim($this->input->post('long_title'));
        $postData['definitions'] = $this->input->post('definitions');
        $postData['variations'] = $this->input->post('variations');
        $postData['term'] = $this->input->post('term');
        $postData['status'] = $this->input->post('status');
        $postData['image_file'] = $this->input->post('image_file_name');
        
        if ($this->input->post('ccId') > 0) {
            $cid = $this->input->post('ccId');
            $this->common_model->UpdateDB('glossaries', ['id' => $cid], $postData);
            echo $cid;
        } else {
            $inserted = $this->common_model->InsertData('glossaries', $postData);
            echo $lastInsertId = $this->db->insert_id();
        }
        exit;
    }

    public function addGlossaryContentOLD() {
        
        if ((int) $this->input->post('cmId')) {
            
            if ($this->input->post('course_expiry') != "")
                $course_expiry = $this->common_model->getDateMdYToYmd($this->input->post('course_expiry'));
            else
                $course_expiry = NULL;

            $cid = $this->input->post('cmId');
            $data = [
                'certificate_id' => $this->input->post('certificates'),
                'certificate_requirement' => $this->input->post('certificate_requirement'),
                'course_img' => $this->input->post('uploadedCourseImg'),
                'course_availability' => $this->input->post('course_availability'),
                'show_glossary' => $this->input->post('show_glossary'),
                'exp_status' => $this->input->post('exp_status'),
                'course_expiry' => $course_expiry,
            ];
            
            $this->common_model->UpdateDB('courses', ['id' => $cid], $data);
            echo $cid;
        } else {

            $maxCourseId = $this->Courses_modal->getMaxId();
            $course_code = "CRS" . date('ym') . $maxCourseId[0]['maxid'];
            $data = [
                'slug' => slugify($this->input->post('title')),
                'created_by' => $this->session->userdata('user_id'),
                'name' => $this->input->post('title'),
                'long_title' => $this->input->post('long_title'),
                'code' => $course_code,
                'description' => $this->input->post('description'),
                'introduction' => $this->input->post('introduction'),
                'outcomes' => $this->input->post('outcomes'),
                'getting_started' => $this->input->post('getting_started'),
            ];

            if ($this->input->post('ccId') > 0) {
                $cid = $this->input->post('ccId');
                $this->common_model->UpdateDB('courses', ['id' => $cid], $data);

                $doc_titles = explode('|', $this->input->post('dtitles'));
                $doc_files = explode('|', $this->input->post('dfiles'));
                $doc_ids = explode('|', $this->input->post('documentsId'));
                $doc_data = [];
                for ($i = 0; $i < count($doc_files); $i++) {
                    $doc_data['reference_id'] = $cid;
                    $doc_data['reference_type'] = 1; // 1=Course                
                    $doc_data['title'] = $doc_titles[$i];
                    $doc_data['filename'] = $doc_files[$i];
                    if ($doc_ids[$i] == '') {
                        $this->common_model->InsertData('documents', $doc_data);
                    } else {
                        $did = $doc_ids[$i];
                        $this->common_model->UpdateDB('documents', ['id' => $did], $doc_data);
                    }
                }
                echo $cid;
            } else {
                $this->common_model->InsertData('courses', $data);
                $lastInsertId = $this->db->insert_id();
                $doc_titles = explode('|', $this->input->post('dtitles'));
                $doc_files = explode('|', $this->input->post('dfiles'));
                $doc_data = [];
                for ($i = 0; $i < count($doc_files); $i++) {
                    $doc_data[$i]['reference_id'] = $lastInsertId;
                    $doc_data[$i]['reference_type'] = 1; // 1=Course                
                    $doc_data[$i]['title'] = $doc_titles[$i];
                    $doc_data[$i]['filename'] = $doc_files[$i];
                }
                
                $this->common_model->InsertBatchData('documents', $doc_data);
                echo $lastInsertId;
            }
        }
        exit;
    }

    public function upload_docs_audio($files = array()) {
        $data = [];
        $file_element_name = 'file';
        $config['upload_path'] = './uploads/glossary_audio/';
        $config['allowed_types'] = 'mp3';
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload($file_element_name)) {
            $status = 'error';
            $data['error'] = $this->upload->display_errors('', '');
            $data['status'] = 0;
        } else {
            $fileData = $this->upload->data();
            $data['fileName'] = $fileData['file_name'];
            $data['status'] = 1;
        }
        return $data;
    }

    /**
     * delete method
     * @description this function use to delete tempray record 
     * @return void
     */
    function delete() {
        $id = $this->uri->segment(4);
        $data = ['delete_status' => 0];
        if ($id) {
            $this->Common_model->UpdateDB('glossaries', ['id' => $id], $data);
            $this->session->set_flashdata('success', $this->ion_auth->messages());
            $msg = "deleted";
            echo json_encode(array("msg" => $msg));
            exit;
        } else {
            set_flashdata('error', 'Unable to delete glossary, try again');
            redirect('general_modules/glossary/list', 'refresh');
        }
    }

    /**
     * detail method
     * @description this function use to view complete question details
     * @return void
     */
    public function detail() {
        $category_id = $this->uri->segment(4);
        if (isset($category_id)) {
            $data['categoryData'] = $this->Common_model->getDataById('categories', '*', ['id' => $category_id]);
            $data['metadata_types'] = $this->Common_model->fetch_metadata_types('metadata_types');
            $data['page'] = "general_modules/glossary/detail";
            $data['breadcrumb'][] = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
            $data['breadcrumb'][] = ['title' => 'Categories', 'link' => base_url('general_modules/category'), 'class' => ''];
            $data['breadcrumb'][] = ['title' => 'View', 'link' => '', 'class' => 'active'];
            $this->template->template_view($data);
        }
    }

    /**
     * detail method
     * @update_status this function use to update status
     * @return void
     */
    public function update_status($id, $action) {
        $status = ($action == 'activate') ? 1 : 0;
        $data = array('status' => $status);
        $this->Glossary_model->update_status($id, $data);
        $msg = "Updated";
        echo json_encode(['msg' => $msg]);
        exit;
    }

    public function setAllSelected()
    {

        $glossaryIds = array_filter(explode("|", $this->input->post('selectedGlossary')));
      
        $data = [];
        $status = $this->input->post('status');
        foreach ($glossaryIds as $key => $val) {
            $data['status'] = $status;
            $this->Glossary_model->update_status($val, $data);
        }
        $msg = 'Updated';
        echo json_encode(['msg' => $msg]);
        exit;
    }
    

}
