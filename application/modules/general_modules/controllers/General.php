
<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
Author Akash Hedaoo
Date 16/04/2019
*/

class General extends MY_Controller
{
    public function __construct()
    {
        //ini_set('display_errors', 1);
        parent::__construct();
        $this->load->module('template'); 
        $this->load->model(['Common_model']);        
        $this->load->helper(array('html', 'form'));
        $this->load->library('form_validation');

        if (!$this->ion_auth->logged_in()):
            redirect('users/auth', 'refresh');
        endif;
    }

    /**
     * index method
     * @description this function use to display list 
     * @return void
     */
    public function index()
    {   
        pr('ok'); exit;
         //$data['page'] = "general_modules/General/list";
        //$this->template->template_view($data);
    }
        
    // select category and tags as per param
    // sub_type ===> 1=>System, 2=> Default, 3=>Institute, 4=>Course, 5=>Lesson, 6=> Module, 7=>Poll, 8=>Quiz, 9=>Assignment, 10=>Media, 11=>Question, 12=>Gradebook, 13=> Glossary

    /*public function get_list()
    {

        if(post('type')):    
        switch(post('type'))
            {
                case '1':
                $data['list_data'] = $this->Common_model->getAllData('categories', 'id, name', '', ['type' => post('sub_type'), 'delete_status' => 1]);
                $data['selector'] = ucfirst(post('selector'));
                break;

                case '2':
                $data['list_data'] = $this->Common_model->getAllData('tags', 'id, name', '', ['type' => post('sub_type'), 'delete_status' => 1]);
                $data['selector'] = ucfirst(post('selector'));
                break;

                default:
                $data['list_data'] = $this->Common_model->getAllData('categories', 'id, name', '', ['type' => post('sub_type'), 'delete_status' => 1]);
                $data['selector'] = ucfirst(post('selector'));
            }
            $data['sub_type'] = post('sub_type');
            $data['type_class'] = (post('type')==1) ? 'category_class' : 'tag_class';
            echo json_encode($data); exit;
            //$data['page'] = "general_modules/General/get_list";
            //$this->template->ajax_view($data);
        else:
            exit('Error');
        endif;    
    }    */
}

