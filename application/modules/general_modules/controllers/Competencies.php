<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
Author Akash Hedaoo
Date 16/04/2019
*/

class Competencies extends MY_Controller
{
    public function __construct()
    {
        //ini_set('display_errors', 1);
        parent::__construct();
        $this->load->module('template'); 
        $this->load->model(['Common_model', 'Competencies_model']);        
        $this->load->helper(array('html', 'form'));
        $this->load->library('form_validation');

        if (!$this->ion_auth->logged_in()):
            redirect('users/auth', 'refresh');
        endif;
    }

    /**
     * index method
     * @description this function use to display list of categories 
     * @return void
     */
    public function index()
    {       
        $data['page'] = "general_modules/Competencies/list";
        $this->template->template_view($data);
    }


    /**
     * getLists method 
     * @description this function called via ajax request, use to display list of lesson
     * @return void
     */         
    public function getLists() {

        $data = $row = [];
        $CompetenciesData = $this->Competencies_model->getRows($_POST); 
        
        $i = $_POST['start']; $cnt=1;
        foreach ($CompetenciesData as $key => $competencies) {
            $data[] = [
                        'id' => $competencies->id,
                        'cnt' => $cnt,
                        'name' => $competencies->name,
                        'created_by' => ucfirst($competencies->first_name).' '.ucfirst($competencies->last_name),
                        'created' => date('m/d/Y', strtotime($competencies->created)),
                        'status' => ($competencies->status==1) ? 'Publish' : 'Unpublish',
                    ]; 
                    $cnt++;                 
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Competencies_model->count_records(),
            "recordsFiltered" => $this->Competencies_model->countFiltered($_POST),
            "data" => $data,
        );
        // Output to JSON format
        echo json_encode($output);
    }


    /**
     * add method 
     * @description this method is use to create new category
     * @return void
     */
    public function add() {        

        if ($this->input->post()) 
        {            
            $this->form_validation->set_rules('name', '', 'trim|required');

            if ($this->form_validation->run() == FALSE)  {  
                set_flashdata('error','Learning standard has been added successfully');
                $data['page'] = "general_modules/Competencies/add";
                $this->template->template_view($data);
            }else{  
                $data = [
                        'created_by' => $this->session->userdata('user_id'),
                        'name'  => post('name'), 
                        'status'  => post('status')
                    ];
                $this->Common_model->InsertData('competencies',$data);                
                set_flashdata('success','Learning standard has been added successfully');
                redirect('general_modules/Competencies/','refresh');
            }
        }else {      

            $data['page'] = 'general_modules/Competencies/add';
            $this->template->template_view($data);
        }
    }


    /**
     * edit method 
     * @description this method is use to update category
     * @return void
     */
    public function edit($id) {

        if ($this->input->post()) 
        {                       
            $this->form_validation->set_rules('name', '', 'trim|required');

            if ($this->form_validation->run() == FALSE)  {               
                $data['page'] = "general_modules/Competencies/edit/";
                $this->template->template_view($data);
            }else if($id) {  
                $data = [
                        'created_by' => $this->session->userdata('user_id'),
                        'name'  => post('name'), 
                        'status'  => post('status') 
                    ];
                $this->Common_model->UpdateDB('competencies', [ 'id' => $id ], $data);            
                set_flashdata('success','Competencies has been updated successfully');
                redirect('general_modules/Competencies/','refresh');
            }else {
                set_flashdata('error','Unable to update category, try again');
                redirect('general_modules/Competencies/','refresh');
            }
        }else {      
            if($id) { 
                $data['competenciesData'] = $this->Common_model->getDataById('competencies', '*', ['id' => $id] );
                $data['page'] = 'general_modules/Competencies/edit';
                $this->template->template_view($data);
            }else {
                set_flashdata('error','Unable to update category, try again');
                redirect('general_modules/Competencies/','refresh');
            }
        }        
    }


    /**
     * delete method
     * @description this function use to delete tempray record 
     * @return void
     */ 
    function delete() {
        $id = $this->uri->segment(4);      
        $data = ['delete_status' => 0];
        if($id) {
            $this->Common_model->UpdateDB('competencies', [ 'id' => $id ], $data);
            $this->session->set_flashdata('success', $this->ion_auth->messages());
            $msg = "deleted";    
            echo json_encode(array("msg"=>$msg)); exit;
        }else {
            set_flashdata('error','Unable to delete Competencies, try again');
            redirect('general_modules/Competencies/','refresh');
        }
    }

     /**
     * detail method
     * @description this function use to view complete learning standard details
     * @return void
     */ 
    public function detail() {
        $id = $this->uri->segment(4);
        if(isset($id)) {
            $data['competenciesData'] = $this->Common_model->getDataById('competencies','*', ['id' => $id]);                       
            $data['page'] = "general_modules/Competencies/detail";
            $this->template->template_view($data);
        }
    }

    
}

