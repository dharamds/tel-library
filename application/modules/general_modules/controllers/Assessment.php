<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Assessment extends MY_Controller
{

    public function __construct()
    {

        parent::__construct();
        //Do your magic here

        $this->load->module('template');
        $this->load->model('common_model');
        $this->load->helper(['html', 'form']);
        $this->load->library('form_validation');

        if (!$this->ion_auth->logged_in()) :
            redirect('users/auth', 'refresh');
        endif;

        
    }

        public function index(){
        $data['page']          = "general_modules/assessment/index";
        $data['breadcrumb'][]  = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]  = ['title' => 'Assessment', 'link' => '', 'class' => 'active'];
        $this->template->template_view($data);
        }

        function load_iframe(){ 
        # ------------------------------
        # START CONFIGURATION SECTION
        # 
        $launch_data = array(
        "user_id" => $this->ion_auth->user()->row()->id,
        "roles" => "Instructor",
        "resource_link_id" => $this->ion_auth->user()->row()->id,
        "resource_link_title" => "Assessments",
        "resource_link_description" => "Course Assessments",
        "lis_person_name_full" => $this->ion_auth->user()->row()->first_name." ".$this->ion_auth->user()->row()->middle_name." ".$this->ion_auth->user()->row()->last_name,
        "lis_person_name_family" => $this->ion_auth->user()->row()->last_name,
        "lis_person_name_given" => $this->ion_auth->user()->row()->first_name,
        "lis_person_contact_email_primary" => $this->ion_auth->user()->row()->email,
        "lis_person_sourcedid" => "school.edu:user",
        "context_id" => "456434513".$this->ion_auth->user()->row()->id,
        );
        #
        # END OF CONFIGURATION SECTION
        # ------------------------------
        $now = new DateTime();
        $launch_data["lti_version"] = "LTI-1p0";
        $launch_data["lti_message_type"] = "basic-lti-launch-request";
        # Basic LTI uses OAuth to sign requests
        # OAuth Core 1.0 spec: http://oauth.net/core/1.0/
        $launch_data["oauth_callback"] = "about:blank";
        $launch_data["oauth_consumer_key"] = LTI_KEY;
        $launch_data["oauth_version"] = "1.0";
        $launch_data["oauth_nonce"] = uniqid('', true);
        $launch_data["oauth_timestamp"] = $now->getTimestamp();
        $launch_data["oauth_signature_method"] = "HMAC-SHA1";
        # In OAuth, request parameters must be sorted by name
        $launch_data_keys = array_keys($launch_data);
        sort($launch_data_keys);
        $launch_params = array();
        foreach ($launch_data_keys as $key) {
        array_push($launch_params, $key . "=" . rawurlencode($launch_data[$key]));
        }
        $base_string = "POST&" . urlencode(LTI_LAUNCH_URL) . "&" . rawurlencode(implode("&", $launch_params));
        $data['secret']         = urlencode(LTI_SECRET) . "&";
        $data['signature']      = base64_encode(hash_hmac("sha1", $base_string, LTI_SECRET, true));
        $data['launch_data']    = $launch_data;
        $data['launch_url']     = LTI_LAUNCH_URL; 
        $data['page']           = "general_modules/assessment/iframe";
        $this->template->iframe_view($data);
        }

}
