<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
Author Akash Hedaoo
Date 16/04/2019
*/

class LearningOutcome extends MY_Controller
{
    public function __construct()
    {
        //ini_set('display_errors', 1);
        parent::__construct();
        $this->load->module('template'); 
        $this->load->model(['Common_model', 'LearningOutcome_model']);        
        $this->load->helper(array('html', 'form'));
        $this->load->library('form_validation');

        if (!$this->ion_auth->logged_in()):
            redirect('users/auth', 'refresh');
        endif;
    }

    /**
     * index method
     * @description this function use to display list of categories 
     * @return void
     */
    public function index()
    {       
        $data['page'] = "general_modules/LearningOutcome/list";
        $this->template->template_view($data);
    }


    /**
     * getLists method 
     * @description this function called via ajax request, use to display list of lesson
     * @return void
     */         
    public function getLists() {

        $data = $row = [];
        $categoryData = $this->LearningOutcome_model->getRows($_POST); 
        # Fetch metdata types
        $metadata_types = $this->Common_model->fetch_metadata_types('metadata_types');

        $i = $_POST['start']; $cnt=1;
        foreach ($categoryData as $key => $category) {
            $data[] = [
                        'id' => $category->id,
                        'cnt' => $cnt,
                        'name' => $category->name,
                        'created_by' => ucfirst($category->first_name).' '.ucfirst($category->last_name),
                        'created' => date('m/d/Y', strtotime($category->created)),
                        'status' => ($category->status==1) ? 'Publish' : 'Unpublish',
                    ]; 
                    $cnt++;                 
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->LearningOutcome_model->count_records(),
            "recordsFiltered" => $this->LearningOutcome_model->countFiltered($_POST),
            "data" => $data,
        );
        // Output to JSON format
        echo json_encode($output);
    }


    /**
     * add method 
     * @description this method is use to create new category
     * @return void
     */
    public function add() {

        if ($this->input->post()) 
        {            
            $this->form_validation->set_rules('name', '', 'trim|required');

            if ($this->form_validation->run() == FALSE)  {  
                set_flashdata('error','Learning outcome has been added successfully');
                $data['page'] = "general_modules/LearningOutcome/add";
                $this->template->template_view($data);
            }else{  
                $data = [
                        'created_by' => $this->session->userdata('user_id'),
                        'name'  => post('name'), 
                        'status'  => post('status')
                    ];
                $this->Common_model->InsertData('learning_outcomes',$data);                
                set_flashdata('success','Learning outcome has been added successfully');
                redirect('general_modules/LearningOutcome/','refresh');
            }
        }else {      

            $data['page'] = 'general_modules/LearningOutcome/add';
            $this->template->template_view($data);
        }
    }


    /**
     * edit method 
     * @description this method is use to update category
     * @return void
     */
    public function edit($id) {

        if ($this->input->post()) 
        {                       
            $this->form_validation->set_rules('name', '', 'trim|required');

            if ($this->form_validation->run() == FALSE)  {               
                $data['page'] = "general_modules/LearningOutcome/edit/";
                $this->template->template_view($data);
            }else if($id) {  
                $data = [
                        'created_by' => $this->session->userdata('user_id'),
                        'name'  => post('name'), 
                        'status'  => post('status') 
                    ];
                $this->Common_model->UpdateDB('learning_outcomes', [ 'id' => $id ], $data);            
                set_flashdata('success','LearningOutcome has been updated successfully');
                redirect('general_modules/LearningOutcome/','refresh');
            }else {
                set_flashdata('error','Unable to update category, try again');
                redirect('general_modules/LearningOutcome/','refresh');
            }
        }else {      
            if($id) { 
                $data['learingOutcomeData'] = $this->Common_model->getDataById('learning_outcomes', '*', ['id' => $id] );
                $data['page'] = 'general_modules/LearningOutcome/edit';
                $this->template->template_view($data);
            }else {
                set_flashdata('error','Unable to update category, try again');
                redirect('general_modules/LearningOutcome/','refresh');
            }
        }        
    }


    /**
     * delete method
     * @description this function use to delete tempray record 
     * @return void
     */ 
    function delete() {
        $id = $this->uri->segment(4);      
        $data = ['delete_status' => 0];
        if($id) {
            $this->Common_model->UpdateDB('learning_outcomes', [ 'id' => $id ], $data);
            $this->session->set_flashdata('success', $this->ion_auth->messages());
            $msg = "deleted";    
            echo json_encode(array("msg"=>$msg)); exit;
        }else {
            set_flashdata('error','Unable to delete category, try again');
            redirect('general_modules/LearningOutcome/','refresh');
        }
    }

     /**
     * detail method
     * @description this function use to view complete learning outcome details
     * @return void
     */ 
    public function detail() {
        $learning_id = $this->uri->segment(4);
        if(isset($learning_id)) {
            $data['LearningOutcomeData'] = $this->Common_model->getDataById('learning_outcomes','*', ['id' => $learning_id]);                       
            $data['page'] = "general_modules/LearningOutcome/detail";
            $this->template->template_view($data);
        }
    }

    
}

