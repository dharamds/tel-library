
            <div class="container-fluid">
                <div data-widget-group="group1">
                    <div class="row">
                        <div class="col-md-12">
                            <a class="btn btn-success " href="<?= base_url('general_modules/grades/add_grades') ?>">Add New Grade</a>
                        </div>
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h2><i class="fa fa-wrench"></i>Manage Grades</h2>

                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">

                                            <table class="table table-hover" id="example">
                                                <thead>
                                                    <tr>
                                                        <th>Sr.</th>
                                                        <th>Grade</th>
                                                        <th>Status</th>
                                                        <th style="width:8%">Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <?php 
                                                   
                                                    if (!empty($grades)) : ?>
                                                        <?php
                                                         $i=1;
                                                        foreach ($grades as $key => $value) :
                                                            ?>
                                                            <tr>
                                                                <td><?php echo $i; ?></td>
                                                                <td><?php echo $value->name ?></td>
                                                                <td>
                                                                    <?php if ($value->status == 1) : ?>
                                                                        <a href="<?php bs() ?>general_modules/grades/change_status/<?php echo $value->id ?>/0/grades" data-toggle="tooltip" data-placement="top" title="Click to Change Status"><button type="button" class="btn btn-midnightblue-alt btn-sm">Active</button></a>
                                                                    <?php else : ?>
                                                                        <a href="<?php bs() ?>general_modules/grades/change_status/<?php echo $value->id ?>/1/grades" data-toggle="tooltip" data-placement="top" title="Click to Change Status"><button type="button" class="btn btn-midnightblue-alt btn-sm">Inactive</button></a>
                                                                    <?php endif ?>
                                                                </td>
                                                                <td style="width:12%">
                                                                    <a style="margin-left:7px;" class="btn btn-midnightblue-alt btn-sm " href="<?php bs(); ?>general_modules/grades/edit_grades/<?php echo $value->id ?>"><i class="ti ti-pencil"></i></a>&nbsp;
                                                                    <a href="<?php bs(); ?>general_modules/grades/delete_grades/<?php echo $value->id ?>" class="btn btn-midnightblue-alt btn-sm delete_item"><i class="ti ti-close"></i></a>
                                                                        
                                                                </td>
                                                            </tr>
                                                            
                                                        <?php 
                                                        $i++;
                                                        endforeach ?>

                                                    <?php else : ?>

                                                        <tr>
                                                            <td>
                                                                <p>
                                                                    <font color="red" size="4"><b>Posts Not Available</b></font>
                                                                </p>
                                                            </td>
                                                        </tr>

                                                    <?php endif ?>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
