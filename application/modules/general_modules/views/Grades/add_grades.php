
            <div class="container-fluid">
                <div class="panel panel-info" data-widget='{"draggable": "false"}'>
                    <div class="panel-heading">
                        <h2><i class="fa fa-user"></i>Add Grade</h2>
                        <div class="panel-ctrls" data-actions-container="" data-action-collapse='{"target": ".panel-body"}'></div>
                    </div>
                    <div class="panel-body">
                        <?php echo form_open('general_modules/grades/add_grades', array('id' => 'add_grades_form_validation', 'class' => 'form-horizontal')); ?>

                        <div class="form-group">
                            <label for="name" class="col-md-3 control-label">Name</label>
                            <div class="col-md-6">                        
                                <?php
                                echo form_input([
                                    'name' => 'name',
                                    'id' => 'name',
                                    'class' => 'form-control',
                                    'placeholder' => 'Please enter grade name ']);
                                ?>
                                <?php echo form_error('name', '<div class="error">', '</div>'); ?>
                            </div>
                        </div> 

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <input type="submit" class="finish btn-success btn" value="Save">
                            </div>
                        </div>
                    </div>
                </div>
            </div>



<script type='text/javascript' src="<?php echo base_url(); ?>public\assets\js\General_modules\grades.js"></script>
<?php
$jsVars = [
    'ajax_call_root' => base_url()
];    
?>
<script>
    jQuery(document).ready(function() { 
        Grades.init(<?php echo json_encode($jsVars); ?>);
    });
</script>
