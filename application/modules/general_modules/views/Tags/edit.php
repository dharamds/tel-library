        
            <div class="container-fluid">
            <div class="panel panel-default panel-grid">
                <div class="panel-heading brd-0 pb-2"></div>
                <div class="panel-body">
                    <!-- Form starts -->  
                    <?php isset($tagsData->id) ? $id = $tagsData->id  : $id = '' ?>
                    <?php 
                     echo form_open('general_modules/tags/edit/'.$id, [
                        'name'=>'frmAddTag',
                        'id'=>'frmAddTag',
                        'class'=>'form-horizontal']); 
                    ?> 
                    <div class="row m-0">
                      <div class="form-group col-xs-12 mb-5">
                          <label for="type" class="col-md-12 control-label">Metadata type</label>
                          <div class="col-md-6">
                          <?php isset($tagsData->type) ? $type = $tagsData->type  : $type = '' ?>                        
                          <?php                            
                            $c = ['class' => 'form-control'];
                            echo form_dropdown('type', $metadata_types, $type, $c);?>
                            <?php echo form_error('type', '<div class="error">', '</div>');?>
                          </div>
                      </div> 

                      <div class="form-group col-xs-12 mb-5">
                          <label for="name" class="col-md-12 control-label">Tag name</label>
                          <div class="col-md-6">
                            <?php isset($tagsData->name) ? $name = $tagsData->name  : $name = '' ?>   
                            <?php echo form_input ([
                                'name'=>'name',
                                'id'=>'name',
                                'value' => $name,
                                'class'=>'form-control editor'
                             ]);?>
                            <?php echo form_error('name', '<div class="error">', '</div>');?>
                          </div>
                      </div>
                    </div>

                    <div class="col-md-12">
                      <div class="hr-line mt-4 mb-5"></div>
                    </div>
                    <div class="form-group col-md-12 text-right p-0">
                      <input type="submit" class="finish btn-success btn" value="Update">
                    </div> 
                    <?php echo form_close();?>      
                  <!-- Form ends -->        
               </div>
            </div>
         </div>
        </div>
<script type='text/javascript' src="<?php echo base_url(); ?>public\assets\js\General_modules\Tags_create_edit.js"></script>
<?php
$jsVars = [
    'ajax_call_root' => base_url()
];    
?>
<script>
    jQuery(document).ready(function() { 
        Tags_create_edit.init(<?php echo json_encode($jsVars); ?>);
    });
</script>