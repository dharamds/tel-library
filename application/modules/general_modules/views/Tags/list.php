
            <div class="container-fluid">
            <div class="row">
        <div class="col-md-12">
            <div class="filter-container flex-row">
                <div class="flex-col-sm-3">
                <?php echo form_dropdown('type', $metadata_types, '', ['class' => 'form-control selected_condition', 'id' => 'category_type']); ?>
                </div>
      
                <div class="flex-col-12 flex-col-md-auto ml-auto">
                    <a class="btn btn-danger pt-2" id="reset_form" href="javascript:void(0)"><i class="flaticon-close-1 f21 fw100"></i></a>
                </div>
            </div>
        </div>
    </div>
                <div data-widget-group="group1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default panel-grid">
                                <div class="panel-heading">
                                    <div class="flex-row">
                                        <div class="flex-col">
                                           <a class="btn btn-success" href="<?=base_url('general_modules/tags/add') ?>">New</a>
                                        </div>
                                        <div class="flex-col">
                                            <div class="panel-ctrls"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table id="tagListTable" class="table table-bordered table-striped table-hover" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>Sr.</th>
                                                        <th>Actions</th>
                                                        <th>Status</th>
                                                        <th>Type</th>
                                                        <th>Name</th>
                                                        <th>Created by</th>
                                                        <th>Created</th>                                                       
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.selected_condition').on('change', function() {
            table.search('').draw();
        });
        $('#reset_form').on('click', function(){
            $('#category_type').val('');
            table.search('').draw();

        });
        var table = table = $('#tagListTable').DataTable({
            // Processing indicator
            "processing": true,
            // DataTables server-side processing mode
            "serverSide": true,
            // Initial no order.
            "iDisplayLength": 25,
            "bPaginate": true,
            "order": [],
            // Load data from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('general_modules/tags/getLists/'); ?>",
                "type": "POST",
                "data": function (data) {
                    data.system_type =  $('#category_type option:selected').val();
                    data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                },
            },

            //Set column definition initialisation properties
            "columnDefs": [{
                    "targets": 2,
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                            if (full['status_button'] == '1') {
                                data = '<a href="<?php bs('general_modules/tags/update_status/') ?>' + full['id'] + '/deactivate" data-toggle="tooltip" data-placement="top" title="Click to Change Status" class="btn btn-success btn-status btn-sm change_status">Active</a>';
                            } else {
                                data = '<a href="<?php bs('general_modules/tags/update_status/') ?>' + full['id'] + '/activate" data-toggle="tooltip" data-placement="top" title="Click to Change Status" class="btn btn-danger btn-status btn-sm change_status">Inactive</a>';
                            }
                        }
                        return data;

                    }

                }, {
                    "targets": 1,
                    "data": null,
                    "orderable":false,
                    "width": "4%",
                    "render": function (data, type, full, meta) {
                        data = '<a class="btn btn-primary btn-sm " href="<?= base_url('general_modules/tags/edit/') ?>' + full['id'] + '"><i class="ti ti-pencil"></i></a>&nbsp;<a href="<?= base_url('general_modules/tags/delete/') ?>' + full['id'] + '" class="btn btn-danger btn-sm delete_item"><i class="ti ti-trash"></i></a>&nbsp;';
                        return data;
                    }
                }],
            "columns": [{
                    "data": "sr_no",
                    "orderable":false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },

                    "autoWidth": true
                },
                {
                    "data": "id",
                    "autoWidth": true
                },
                {
                    "data": "id",
                    "autoWidth": true
                },
                {
                    "data": "type_name",
                    "autoWidth": true
                },
                {
                    "data": "name",
                    "autoWidth": true
                },
                {
                    "data": "user_name",
                    "autoWidth": true
                },
                {
                    "data": "created",
                    "autoWidth": true
                }
            ]
        });

        $(document).on('click', '.change_status', function (e) {
            e.preventDefault();
            $.get($(this).attr("href"), // url
                    function (data, textStatus, jqXHR) { // success callback
                        var obj = JSON.parse(data);
                        if (obj.msg === 'Updated') {
                            table.ajax.reload();  //just reload table
                        }
                    });
        });

        // To delete record
        $(document).on('click', '.delete_item', function (e) {
            e.preventDefault();
            var scope = $(this);
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure?',
                buttons: {
                    confirm: function () {
                        $.get(scope.attr("href"), // url
                                function (data, textStatus, jqXHR) { // success callback
                                    var obj = JSON.parse(data);
                                    if (obj.msg === 'deleted') {
                                        table.ajax.reload(); //just reload table
                                    }
                                });
                        return true;
                    },
                    cancel: function () {
                        return true;
                    }
                }
            });
        });

    });
</script>