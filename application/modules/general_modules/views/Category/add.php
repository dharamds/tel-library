<div class="container-fluid">
  <div class="panel panel-default panel-grid">
    <div class="panel-heading brd-0 pb-2"></div>
    <div class="panel-body">
      <!-- Form starts -->
      <?php
      echo form_open('general_modules/category/add/', [
        'name' => 'frmAddCategory',
        'id' => 'frmAddCategory',
        'class' => 'form-horizontal'
      ]);
      ?>
      
      <div class="row m-0">
      <div class="form-group col-xs-12 mb-5">
          <label for="name" class="col-md-12 control-label">Category name <span class="field-required">*</span></label>
          <div class="col-md-6">
            <?php echo form_input([
              'name' => 'name',
              'id' => 'name',
              'value' => '',
              'class' => 'form-control'
            ]); ?>
            <?php echo form_error('name', '<div class="error">', '</div>'); ?>
          </div>
        </div>
        <div class="form-group col-xs-12 mb-5">
          <label for="type" class="col-md-12 control-label">Metadata type <span class="field-required">*</span></label>
          <div class="col-md-6">
            <?php
            $c = ['class' => 'form-control'];
            echo form_dropdown('type', $metadata_types, '', $c); ?>
            <?php echo form_error('type', '<div class="error">', '</div>'); ?>
          </div>
        </div>

        <div class="form-group col-xs-12 mb-5">
          <label for="type" class="col-md-12 control-label">Parent Category</label>
          <div class="col-md-6">
            <?php
            $c = ['class' => 'form-control'];
            echo form_dropdown('parent_category', $parent_metadata_types_categories, '', $c); ?>
            <?php echo form_error('type', '<div class="error">', '</div>'); ?>
          </div>
        </div>

        
        <div class="form-group col-xs-12 mb-5">
          <label for="name" class="col-md-12 control-label">Description</label>
          <div class="col-md-6">
            <?php
            echo form_textarea([
              'name'  => 'description',
              'id'    => 'description',
              'class' => 'form-control editor',
              'rows'  => '5'
            ]);
            ?>
            <?php echo form_error('name', '<div class="error">', '</div>'); ?>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="hr-line mt-4 mb-5"></div>
      </div>
      <div class="form-group col-md-12 text-right p-0">
        <input type="submit" class="finish btn-success btn" value="Save">
      </div>
      <?php echo form_close(); ?>
      <!-- Form ends -->
    </div>
  </div>
</div>
</div>
<script type='text/javascript' src="<?php echo base_url(); ?>public\assets\js\General_modules\Category_create_edit.js"></script>
<?php
$jsVars = [
  'ajax_call_root' => base_url()
];
?>
<script>
  jQuery(document).ready(function() {
    Category_create_edit.init( <?php echo json_encode($jsVars); ?> );
  });
</script>