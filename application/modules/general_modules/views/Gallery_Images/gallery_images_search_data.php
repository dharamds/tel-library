<?php 
if (!empty($data)) { ?>
	<?php foreach ($data as $items) {
		$path = base_url('uploads/media_images/' . $items->file_name);
		$fileExist = file_exists($_SERVER['DOCUMENT_ROOT'] . $items->file_name);
		if (!$fileExist) { ?>
			  <div class="col-sm-2 loaded_div">
				<div class="col-md-12">
				<div class="form-group media-thumb" onclick="place_img('<?php echo 'uploads/media_images/' . $items->file_name; ?>','<?php bs('uploads/media_images/' . $items->file_name) ?>')" data-toggle="tooltip" data-placement="bottom" title="<?php echo $items->title;?>">
				<?php 
				
				$ext = pathinfo(base_url('uploads/media_images/thumbnails/' . $items->file_name)  , PATHINFO_EXTENSION);
				
				$ext_data='';
				if($ext == 'png' || $ext == 'jpg' || $ext == 'jpeg') {
					$ext_data .= "<img class='img-rounded' src=".base_url("uploads/media_images/thumbnails/" . $items->file_name)." value=".base_url('uploads/media_images/' . $items->file_name) ." alt=".$items->title."><span>".$items->title."</span>";
				} else if($ext=='mp3' || $ext=='mb') {
					$ext_data .= "<div class='flaticon-mp3 f45 text-center pt-4' ></div>";
				} else if($ext=='txt') {
					$ext_data .= "<div class='flaticon-txt f45 text-center pt-4' ></div>";
				} else if($ext=='pdf') {
					$ext_data .= "<div class='flaticon-pdf f45 text-center pt-4' ></div>";
				} else if($ext=='csv') {
					$ext_data .= "<div class='flaticon-csv f45 text-center pt-4' ></div>";
				} else if($ext=='doc' || $ext=='docx') {
					$ext_data .= "<div class='flaticon-doc f45 text-center pt-4' ></div>";
				} else if($ext=='xls' || $ext=='xlsx') {
					$ext_data .= "<div class='flaticon-xls f45 text-center pt-4' ></div>";
				} else if($ext=='mp4') {
					$ext_data .= "<div class='f45 text-center pt-4 media-thumb-img' ><img src=".base_url('/public/assets/img/mp4.png') ."></img></div>";
				}
				?>
					<?php echo $ext_data; ?>
					

				</div>
			  </div>
			</div>
		<?php }
	} ?>
	
	<div class="call_back_call">
			<script type="text/javascript">
			$(document).ready(function() {
				var run_code = false;
				document.getElementById('search_div').addEventListener('scroll', function(event) { 

						if ((parseInt($('#search_div')[0].scrollHeight) - parseInt(this.clientHeight)) == parseInt($('#search_div').scrollTop())){
							if(!run_code){
								load_images(<?php echo $page;?>, 'append'); 
								run_code = true; 
								$('.call_back_call').remove();
							}   	        	
						}
				});
				$('[data-toggle="tooltip"]').tooltip();
			}); 
			</script>
	</div>

<?php } else { ?>
	
<?php } ?>