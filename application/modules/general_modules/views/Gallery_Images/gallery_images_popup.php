<style>
.istVisualContainer .popup_image_add_new_section{
    width: 210px;
    height: 150px;
    border-radius: 5px;
    border: 1px solid #f3f3f3;
}
</style>
<script type="text/javascript" src="<?= base_url('public/assets/js/jquery.visible.min.js') ?>"></script> <!-- Load jQueryUI -->

<div class="row m-0" style="min-height:500px;">
    <div class="progress1  skylo progress_bar_dark"  id="loaderImg">
        <div class="loader-box" style="opacity: 0.964308;">
            Loading...<span class="loader loader-quart"></span>
        </div>
    </div>
    <div class="col">
        <ul class="nav nav-tabs modal-nav-tab" role="tablist">
            <li role="presentation" class="active">
                <a href="#allMedia" role="tab" data-toggle="tab">Media</a>
            </li>
            <li role="presentation">
                <a href="#addNewMedia" role="tab" data-toggle="tab">Add New</a>
            </li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="allMedia">

                <div class="row m-0">
                    <div class="col-md-12 pt-4 mt-2">
                        <div class="filter-container1 flex-row m-0 mb-4">
                            <div class="flex-col-sm-3 selected_condition" id="media_cat"></div>
                            <div class="flex-col-sm-3 selected_condition" id="media_tag"></div>
                            <div class="flex-col-sm-3 selected_condition">
                                <input type="text" id="image_search" class="form-control" placeholder="Search...">
                            </div>
                            <div class="flex-col-12 flex-col-md-auto ml-auto pl-0">
                                <a class="btn btn-danger btn-md pt-2" id="media_search_button" href="javascript:void(0)"><i class="flaticon-search f16 fw100 d-inline-block mt-1"></i></a>
                            </div>
                            <div class="flex-col-12 flex-col-md-auto pl-0  pr-0">
                                <a class="btn btn-danger btn-md pt-2" id="reset_form" href="javascript:void(0)"><i class="flaticon-close-1 f16 fw100 d-inline-block mt-1"></i></a>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div id="search_div" class="" style="overflow: auto; max-height:348px;padding-top:42px;padding-bottom: 75px;">

                </div>



            </div>
            <div role="tabpanel" class="tab-pane" id="addNewMedia">
                <div class="istVisualContainer mb-0">
                    <?php
                    $id = post('id');
                    echo form_open('lessons/add' . $id, [
                        'name' => 'lesson_img_up',
                        'id' => 'lesson_img_up',
                        'enctype' => 'multipart/form-data',
                    ]);
                    ?>
                    <div class="row m-0">
                        <div class="row col-sm-12 m-0">
                            <div class="col-sm-12 pl-0">
                                <div class="ist-visual-info pt-0">

                                    <div class="row m-0">

                                        <div class="col-md-6 row m-0 p-0">
                                            <div class="col-md-12 p-0 pt-4">
                                                <label class="col-md-12">
                                                    Title<span class="field-required">*</span>
                                                </label>
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" name="title" id="title" required="required">
                                                </div>
                                            </div>
                                            <div class="col-md-12 p-0 pt-4">
                                                <label class="col-md-12">
                                                    Media categories
                                                </label>
                                                <div class="col-md-12 col-xs-12" id="img_media_cat"></div>
                                            </div>
                                            <div class="col-md-12 p-0 pt-4">
                                                <label class="col-md-12">
                                                    Media tags
                                                </label>
                                                <div class="col-md-12 col-xs-12" id="img_media_tag"></div>
                                            </div>
                                        </div>
                                        <!-------------------------------------->

                                        <input type="hidden" name="module_id" id="module_id" value=<?php echo post('module_id') ?>>
                                        <input type="hidden" name="lesson_id" id="lesson_id" value=<?php echo $id ?>>
                                        <input type="hidden" name="old_img" value="">
                                        <div class="fileinput fileinput-new col-md-6 pl-5 pt-5 mt-3" data-provides="fileinput">
                                            <!-- <div class="col-md-3 ist-logo fileinput-preview thumbnail p-0" data-trigger="fileinput"> -->
                                            <div class="col-md-3 popup_image_add_new_section fileinput-preview thumbnail p-0" data-trigger="fileinput">
                                                <img class="uploadImgPreview">
                                            </div>
                                            <div class="col-md-6 pt-4">
                                                <h4 class="mt-2">Select Image</h4>
                                                <span class="btn btn-primary btn-primary-default btn-file" style="min-width: 120px;">
                                                    <span class="fileinput-exists">Change</span>
                                                    <span class="fileinput-new">Choose File</span>
                                                    <input id="uploadImage" name="file" type="file" class="blog_img1 visible1">
                                                </span>
                                                <a href="#" class="btn btn-danger fileinput-exists delete_img" style="min-width: 50px;">
                                                    <i class="ti ti-trash"></i>
                                                </a>
                                                <div class="small mt-3">(File must be .png, .jpg, .jpeg, .mp3, .xlsx, .xls, .doc , .docx, .pdf, .mp4)</div>

                                                <div>
                                                    <div class='progress' id="progressDivId" style="display:none">
                                                        <div class='progress-bar' id='progressBar'></div>
                                                        <div class='percent' id='percent'>0%</div>
                                                    </div>
                                                    <div style="height: 10px;"></div>
                                                    <div id='outputImage'></div>
                                                </div>

                                            </div>
                                        </div>
                                        <div style="color:red;" id="upload_error"></div>
                                        <!-------------------------------------->
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mt-5 mb-2 p-0">
                                <div class="hr-line"></div>
                            </div>
                            <div class="col-md-12 pt-4 mt-3 text-right p-0">
                                <button type="button" class="btn btn-primary" id="up_img">Upload</button>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>




<script type="text/javascript" src="<?php echo base_url('public/assets/js/jquery.form.js') ?>"></script>
<script>
    $('#gallery_images_popup').on('shown.bs.modal', function(e) {
    })

    $('#media_search_button').on('click', function() {
        load_images(0, 'html');
    });

    function load_images(pageLimit = 0, loadtype = 'append') {
        
        var loadtype = loadtype;
        var media_cat = CommanJS.getMetaCallBack('media_category_filter');
        var media_tag = CommanJS.getMetaCallBack('media_tag_filter');
        var search = $('#image_search').val();
        $.ajax({
            type: "POST",
            url: "<?php bs('general_modules/Gallery_Images/filter_data/') ?>" + pageLimit,
            data: {
                "media_cat": media_cat,
                "media_tag": media_tag,
                "search": search
            },
            dataType: 'html',
            beforeSend: function() {
                // setting a timeout
                $('#loaderImg').show();
                
            },
            success: function(data) {

                if (loadtype == 'append') {
                    $('#search_div').append(data);
                } else {
                    $('#search_div').html(data);
                }
                $('[data-toggle="tooltip"]').tooltip();
                $('#loaderImg').hide();
            }
        });
    }

    $(document).ready(function() {
        load_images(0, 'html');
        CommanJS.get_metadata_options("Media category Filter", 1, <?= META_MEDIA; ?>, "media_cat");
        CommanJS.get_metadata_options("Media category", 1, <?= META_MEDIA; ?>, "img_media_cat");
        CommanJS.get_metadata_options("Media tag Filter", 2, <?= META_MEDIA; ?>, "media_tag");
        CommanJS.get_metadata_options("Media tag", 2, <?= META_MEDIA; ?>, "img_media_tag");
        $("#lesson_img_up").validate({
            ignore: [],
            rules: {
                title: {
                    required: true
                }
            },
            messages: {
                title: "Please enter title"
            },
            errorPlacement: function(error, $elem) {
                if ($elem.is('textarea')) {
                    $elem.insertAfter($elem.next('div'));
                }
                error.insertAfter($elem);
            },
            submitHandler: function(form) {
                // D fixesss   

                return true;
            }
        });

        $('#reset_form').click(function() {
            $(".meta_data_filter").prop("checked",false);
            $(".selectFilterMode span").text('');  
            $('#image_search').val('');   
            load_images(0, 'html');       
        });




    });

    // Delete module image
    $('.delete_img').off('click').on('click', function() {

        var module_id = '<?php echo $module_id; ?>';
        var filename = $("#img_name").val();
        //var scope = $(this);

        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure?',
            buttons: {
                confirm: function() {
                    $("#img_name").val('');
                    $(".ist-logo").html('');
                    $(".ist-logo").append('<img src="<?php echo bs("public/assets/img/logo-bg.png"); ?>" id="image_write_path" class="img-rounded">');

                },
                cancel: function() {
                    return true;
                }
            }
        });
    });

    $('#up_img').on('click', function() {
        
        $('#lesson_img_up').ajaxForm({
            
            url: '<?php echo bs(); ?>lessons/upload_docs',
            data: {
                'media_category': CommanJS.getMetaCallBack('media_category'),
                'media_tag': CommanJS.getMetaCallBack('media_tag'),
            },
            beforeSubmit: function() {
                $("#progressDivId").css("display", "block");
                var percentValue = '0%';
                $('#progressBar').width(percentValue);
                $('#percent').html(percentValue);
            },
            uploadProgress: function(event, position, total, percentComplete) {

                var percentValue = percentComplete + '%';
                $("#progressBar").animate({
                    width: percentValue
                }, {
                    duration: 50,
                    easing: "linear",
                    step: function(x) {
                        percentText = Math.round(x * 100 / percentComplete);
                        $("#percent").text(percentText + "%");
                        if (percentText == "100") {
                            $('#progressDivId').hide();
                            $('#uploadImage').val('');
                        }
                    }
                });
            },
            error: function(response, status, e) {
                alert('Oops something went.');
            },
            complete: function(xhr) {
                if (xhr.responseText && xhr.responseText != "error") {} else {
                    $("#outputImage").show();
                    $("#outputImage").html("<div class='error'>Problem in uploading file.</div>");
                    $("#progressBar").stop();
                }
            },
            success: function(data) {
                var jsn = JSON.parse(data);
                if (jsn.error) {
                    $('#upload_error').html(jsn.error);
                } else {
                    var url = jsn.fullfilePath + jsn.fileName;
                    $("#gallery_images_popup").modal('hide');
                    var percentValue = '0%';
                    $("#progressBar").animate({
                        'width': percentValue
                    }, {
                        step: function(x) {
                            $("#percent").text("0%");
                        }
                    });

                    <?= $call_back; ?>(jsn.filePath + jsn.fileName, url, <?= ($instanceName) ? $instanceName : "''"; ?>);
                }
                $('a.delete_img').css({
                    "display": "inline-block"
                });
            }

        }).submit();


    });

    function place_img(image_name, img_full_url) {
        <?= $call_back; ?>(image_name, img_full_url, <?= ($instanceName) ? $instanceName : "''"; ?>);
        $("#gallery_images_popup").modal('hide');
        return true;
    }

    $(document).on('change', '#uploadImage', function() {

        readURL(this);
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var extension = input.files[0].name.split('.').pop().toLowerCase(); //file extension from input file
            
            if (extension == 'png' || extension == 'jpg' || extension == 'jpeg') {
                reader.onload = function(e) {
                    $(".popup_image_add_new_section").addClass("fileinput-preview");
                    $(".popup_image_add_new_section").children().attr('src', e.target.result);
                }
            } else if (extension == 'mp3' || extension == 'mb') {
                reader.onload = function(e) {
                    $(".popup_image_add_new_section").removeClass("fileinput-preview");
                    $('.popup_image_add_new_section').html("<div class='flaticon-mp3 f45 text-center pt-5' ></div>");
                }
            } else if (extension == 'txt') {
                reader.onload = function(e) {
                    $(".popup_image_add_new_section").removeClass("fileinput-preview");
                    $('.popup_image_add_new_section').html("<div class='flaticon-txt f45 text-center pt-5' ></div>");
                }
            } else if (extension == 'pdf') {
                reader.onload = function(e) {
                    $(".popup_image_add_new_section").removeClass("fileinput-preview");
                    $('.popup_image_add_new_section').html("<div class='flaticon-pdf f45 text-center pt-5' ></div>");
                }
            } else if (extension == 'csv') {
                reader.onload = function(e) {
                    $(".popup_image_add_new_section").removeClass("fileinput-preview");
                    $('.popup_image_add_new_section').html("<div class='flaticon-csv f45 text-center pt-5' ></div>");
                }
            } else if (extension == 'docx' || extension == 'doc') {
                reader.onload = function(e) {
                    $(".popup_image_add_new_section").removeClass("fileinput-preview");
                    $('.popup_image_add_new_section').html("<div class='flaticon-doc f45 text-center pt-5' ></div>");
                }
            } else if (extension == 'xls' || extension == 'xlsx') {
                reader.onload = function(e) {
                    $(".popup_image_add_new_section").removeClass("fileinput-preview");
                    $('.popup_image_add_new_section').html("<div class='flaticon-xls f45 text-center pt-5' ></div>");
                }
            } else if (extension == 'mp4') {
                reader.onload = function(e) {
                    $(".popup_image_add_new_section").removeClass("fileinput-preview");
                    $('.popup_image_add_new_section').children().attr('src','<?php bs('/public/assets/img/mp4.png') ?>');
                }
            }


            reader.readAsDataURL(input.files[0]);
        }
    }
</script>