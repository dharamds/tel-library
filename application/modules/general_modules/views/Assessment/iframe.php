<form target="_self" id="ltiLaunchForm" name="ltiLaunchForm" method="POST" action="<?php printf($launch_url); ?>">
<?php foreach ($launch_data as $k => $v ) { ?>
  <input type="hidden" name="<?php echo $k ?>" value="<?php echo $v ?>">
<?php } ?>
  <input type="hidden" name="oauth_signature" value="<?php echo $signature ?>">
</form>
<script>
$( document ).ready(function() {
    $('#ltiLaunchForm').submit();
});
</script>