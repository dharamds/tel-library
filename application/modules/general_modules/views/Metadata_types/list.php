
            <div class="container-fluid">
                <div data-widget-group="group1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default panel-grid">
                                <div class="panel-heading">
                                    <div class="flex-row">
                                        <div class="flex-col">
                                           <a class="btn btn-success" href="<?= base_url('general_modules/Metadata_types/add') ?>">New</a>
                                        </div>
                                        <div class="flex-col">
                                            <div class="panel-ctrls"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table id="Metadata_typesListTable" class="table table-bordered table-striped table-hover" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>Sr.</th>
                                                        <th>Name</th>
                                                        <th>Description</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<script type="text/javascript">
    $(document).ready(function () {
        var table = table = $('#Metadata_typesListTable').DataTable({
            // Processing indicator
            "processing": true,
            // DataTables server-side processing mode
            "serverSide": true,
            // Initial no order.
            "iDisplayLength": 10,
            "bPaginate": true,
            "order": [],
            // Load data from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('general_modules/Metadata_types/getLists/'); ?>",
                "type": "POST",
                "data": function (data) {
                    data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                },
            },

            //Set column definition initialisation properties
            "columnDefs": [{
                    "targets": [3],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        data = '<a class="btn btn-primary btn-sm " href="<?= base_url('general_modules/Metadata_types/edit/') ?>' + full['id'] + '"><i class="ti ti-pencil"></i></a>&nbsp;';
                        return data;

                    }

                }],
            "columns": [{
                    "data": "sr_no",
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },

                    "autoWidth": true
                },
                {
                    "data": "name",
                    "autoWidth": true
                },
                {
                    "data": "description",
                    "autoWidth": true
                },
                {
                    "data": "created",
                    "autoWidth": true
                }
            ]
        });

        

        $(document).on('click', '.change_status', function (e) {
            e.preventDefault();
            $.get($(this).attr("href"), // url
                    function (data, textStatus, jqXHR) { // success callback
                        var obj = JSON.parse(data);
                        if (obj.msg === 'Updated') {
                            table.ajax.reload();  //just reload table
                        }
                    });
        });

        // To delete record
        $(document).on('click', '.delete_item', function (e) {
            e.preventDefault();
            var scope = $(this);
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure?',
                buttons: {
                    confirm: function () {
                        $.get(scope.attr("href"), // url
                                function (data, textStatus, jqXHR) { // success callback
                                    var obj = JSON.parse(data);
                                    if (obj.msg === 'deleted') {
                                        table.ajax.reload(); //just reload table
                                    }
                                });
                        return true;
                    },
                    cancel: function () {
                        return true;
                    }
                }
            });
        });

    });
</script>