<div class="container-fluid">
	<div class="panel panel-default panel-grid">
		<div class="panel-heading brd-0 pb-2"></div>
		<div class="panel-body">
			<!-- Form starts -->
			<?php
			echo form_open('general_modules/Metadata_types/add/', [
				'name' => 'frmAddTag',
				'id' => 'frmAddTag',
				'class' => 'form-horizontal'
			]);
			?>
			<div class="row m-0">
				<div class="form-group col-xs-12 mb-5">
					<label for="name" class="col-md-12 control-label">Name</label>
					<div class="col-md-6">
						<?php echo form_input([
							'name' => 'name',
							'id' => 'name',
							'value' => '',
							'class' => 'form-control editor'
						]); ?>
						<?php echo form_error('name', '<div class="error">', '</div>'); ?>
					</div>
				</div>
			</div>
			<div class="form-group col-xs-12 mb-5">
				<label for="name" class="col-md-12 control-label">Description</label>
				<div class="col-md-6">
					<?php
					echo form_textarea([
						'name'  => 'description',
						'id'    => 'description',
						'class' => 'form-control editor',
						'rows'  => '5'
					]);
					?>
					<?php echo form_error('description', '<div class="error">', '</div>'); ?>
				</div>
			</div>
			<div class="col-md-12">
				<div class="hr-line mt-4 mb-5"></div>
			</div>
			<div class="form-group col-md-12 text-right p-0">
				<input type="submit" class="finish btn-success btn" value="Save">
			</div>
			<?php echo form_close(); ?>
			<!-- Form ends -->
		</div>
	</div>
</div>
</div>
<script type='text/javascript' src="<?php echo base_url(); ?>public\assets\js\General_modules\Tags_create_edit.js"></script>
<script>

$(document).ready(function() {
	
    var config = {
     toolbarGroups: [{
        "name": "basicstyles",
        "groups": ["basicstyles"]
     },
     {
        "name": "links",
        "groups": ["links"]
     },
     {
        "name": "paragraph",
        "groups": ["list", "blocks"]
     },
     {
        "name": "document",
        "groups": ["mode"]
     },
     {
        "name": "insert",
        "groups": ["insert"]
     },
     {
        "name": "styles",
        "groups": ["styles"]
     },
     {
        "name": "about",
        "groups": ["about"]
     }
     ],
     removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar'
  };
  // to assign ckeditor
  CKEDITOR.replace('description',config);
});
</script>
<?php
$jsVars = [
	'ajax_call_root' => base_url()
];
?>
<script>
	jQuery(document).ready(function() {
		Tags_create_edit.init( <?php echo json_encode($jsVars); ?> );
	});
</script>