<div class="container-fluid">
<div class="row">
        <div class="col-md-12">
            <div class="filter-container flex-row">
                <div class="flex-col-sm-3 selected_condition" id="system_cat">

                </div>
                <div class="flex-col-sm-2 selected_condition" id="system_tag">

                </div>
                <div class="flex-col-sm-3 selected_condition" id="glossary_cat">

                </div>
                <div class="flex-col-sm-2 selected_condition" id="glossary_tag">

                </div>
                <div class="flex-col-12 flex-col-md-auto ml-auto">
                    <a class="btn btn-danger pt-2" id="reset_form" href="javascript:void(0)"><i class="flaticon-close-1 f21 fw100"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div data-widget-group="group1">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default panel-grid">
          
                    <div class="panel-heading">
                        <?php if ($this->ion_auth->is_admin() || $permissions->add_per): ?>
                        <a class="btn btn-primary" href="<?= base_url('glossary/add') ?>">Add New</a>
                        <?php endif; ?>

                        <?php if ($this->ion_auth->is_admin() || $permissions->delete_per): ?>
                            <a href="<?php echo base_url('Glossary/setAllSelected') ?>" class="btn btn-primary" id="bulk_glossary">
                                Set all selected as available
                            </a>
                        <?php endif; ?>
                        <?php if ($this->ion_auth->is_admin() || $permissions->delete_per): ?>
                            <a href="<?php echo base_url('Glossary/setAllSelected') ?>" class="btn btn-primary" id="bulk_glossary2">
                                Set all selected as unavailable
                            </a>
                        <?php endif; ?>

                        <div class="panel-ctrls"></div>
                    </div>
                    
                    <div class="panel-body no-padding">
                        <table id="memListTable" class="table table-bordered table-striped table-hover" cellspacing="0">
                            <thead>
                                <tr>
                                    <th style="min-width: 50px;">
                                        <label class="checkbox-tel">
                                            <input type="checkbox" class="checkAll" id="checkAll">
                                        </label>
                                    </th>
                                    <th style="min-width: 100px;">Action</th>
                                    <th style="min-width: 100px;">Availability</th>
                                    <th style="min-width: 150px;">Glossary Term</th>
                                    <th style="min-width: 180px;">Glossary Categories</th>
                                    <th style="min-width: 140px;">Glossary Tags</th>
                                    <th style="min-width: 150px;">System Categories</th>
                                    <th style="min-width: 140px;">System Tags</th>
                                    <th style="min-width: 100px;"># Uses</th>
                                    <th style="min-width: 150px;">Used on the pages</th>
                                 </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- .container-fluid -->

<script>
    $(document).ready(function () {
        $('.checkAll').click(function () {
            $(':checkbox.checkLesson').prop('checked', this.checked);
        });

        CommanJS.get_metadata_options("System category", 1, <?=META_SYSTEM;?>, "system_cat");
        CommanJS.get_metadata_options("System tag", 2, <?=META_SYSTEM;?>, "system_tag");
        CommanJS.get_metadata_options("Glossary category", 1, <?=META_GLOSSARY;?>, "glossary_cat");
        CommanJS.get_metadata_options("Glossary tag", 2, <?=META_GLOSSARY;?>, "glossary_tag");

        $('#reset_form').click(function() {
            $(".meta_data_filter").prop("checked",false);
            $(".selectFilterMode span").text('');            
            table.search('').draw();
        });
        $('.selected_condition').on('change', function() {
            table.search('').draw();
        });
    var table = table  = $('#memListTable').DataTable({
    // Processing indicator
    "processing": true,
            // DataTables server-side processing mode
            "serverSide": true,
            // Initial no order.
            "iDisplayLength": 10,
            "bPaginate": true,
            "order": [],
            "scrollX": true,
            "autoWidth": false,
            "drawCallback": function(settings) {
                $('.load_meta_data').each(function(key, item) {
                    $.getJSON("<?php echo bs('questions/get_meta_tags/'); ?>" + $(this).attr('id'), function(data) {
                           if(data) $("#"+data.typeid).html(data.value);
                    });
				});
				$('.load_meta_category').each(function(key, item) {
                    $.getJSON("<?php echo bs('questions/get_meta_categories/'); ?>" + $(this).attr('id'), function(data) {
                           if(data) $("#"+data.typeid).html(data.value);
                    });
				});

                },
            // Load data from an Ajax source
            "ajax": {
            "url": "<?php echo base_url('general_modules/glossary/getLists'); ?>",
                    "type": "POST",
                    "data": function (data) {
                    data.system_cat = CommanJS.getMetaCallBack('system_category'); 
                    data.system_tag = CommanJS.getMetaCallBack('system_tag');
                    data.glossary_cat = CommanJS.getMetaCallBack('glossary_category');
                    data.glossary_tag = CommanJS.getMetaCallBack('glossary_tag');
                    data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                    $(window).scrollTop(0);
                    },
            },
            //Set column definition initialisation properties
            "columnDefs": [{
                    "targets": 0,
                    // "searchable": false,
                    "orderable": false,
                    "data": null,
                    // "width": "4%",
                    "render": function (data, type, full, meta) {
                        var data = '';
                        if (type == 'display') {
                            data = '<label class="checkbox-tel"><input type="checkbox" class="checkLesson" name="item[]" value="' + full['id'] + '"></label>';
                        }
                        return data;
                    }
                },{
            "targets": 2,
                    "data": null,
                    "render": function (data, type, full, meta) { 
                    if (type == 'display') {
                        
                        if (full['status'] == 1) {
                                data ='<a href="<?php bs('general_modules/glossary/update_status/') ?>' + full['id'] + '/deactivate" data-toggle="tooltip" data-placement="top" title="Click to Change Status" class="text-success f18 change_status"><i class="flaticon-checked"></i></a>';
                            } else {
                                
                                data ='<a href="<?php bs('general_modules/glossary/update_status/') ?>' + full['id'] + '/activate" data-toggle="tooltip" data-placement="top" title="Click to Change Status" class="text-danger f18 change_status"><i class="flaticon-close"></i></a>';
                            }
                    }
                    return data;
                    }
            },
            {
            "targets": 1,
            "orderable": false,
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type == 'display') {
                        data  = '<a class="btn btn-primary btn-sm" href="<?php echo bs('glossary/add/') ?>' + full['id'] + '"><i class="ti ti-pencil"></i></a>';
                        url   = '<?php echo bs('general_modules/glossary/delete/') ?>' + full['id'];                 
                        data  += '<a href="' + url + '" class="btn btn-danger btn-sm delete_item" ><i class="ti ti-trash"></i></a>';
                        }
                    return data;
                    }
            },
            {
                    "targets": [4],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                         data = '<span id="' + full['id'] +'_<?php echo META_GLOSSARY; ?>_<?php echo META_GLOSSARY; ?>_cat" data-type="course_category" class="load_meta_category"> Loading...</span>';
						  //data = 1;
						}
                        return data;
                    }
                },
				{
                    "targets": [5],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                           data = '<span id="' + full['id'] +'_<?php echo META_GLOSSARY; ?>_<?php echo META_GLOSSARY; ?>" data-type="course_tags" class="load_meta_data"> Loading...</span>';
                        }
                        return data;
                    }
                },
                {
                    "targets": [6],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                           data = '<span id="' + full['id'] +'_<?php echo META_GLOSSARY ; ?>_<?php echo META_SYSTEM; ?>_syscat" data-type="system_category" class="load_meta_category"> Loading...</span>';
						 //data = 2;
						}
                        return data;
                    }
                },
                {
                    "targets": [7],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                           data = '<span id="' + full['id'] +'_<?php echo META_GLOSSARY ; ?>_<?php echo META_SYSTEM; ?>_systag" data-type="system_tags" class="load_meta_data"> Loading...</span>';
                        }
                        return data;
                    }
                }],
                "columns": [
                    {
                    "data": "id",
                },
                {
                    "data": "id",
                },
                {
                    "data": "id",
                },
                {"data": "term"},
                {"data": "glossary_categories"},
                {"data": "glossary_tags"},
                {"data": "system_category"},
                {"data": "system_tags"},
                {"data": "id", "orderable": false},
                {"data": "id", "orderable": false}
                ]
        });

$("#bulk_glossary").click(function (e) {
            e.preventDefault();
            var selected_glossary_id = "";
            $('input[name="item[]"]:checked').each(function () {
                //  console.log(this.value);
                selected_glossary_id = this.value + '|' + selected_glossary_id;
            });
            if (selected_glossary_id != "") {
                $('#check').val(selected_glossary_id);
            } else {
                CommanJS.getDisplayMessgae(400, 'Please select atleast one glossary.')
                return false;
            }
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('general_modules/glossary/setAllSelected'); ?>",
                data: {
                    selectedGlossary: selected_glossary_id,
                    status: '1'
                },
                dataType: 'json',
                success: function (data) {
                    //console.log(data);
                    if (data.msg === 'Updated') {
                        
                        $('.checkAll').prop('checked', false);
                        table.ajax.reload(); //just reload table
                    }
                }
            });
        });
        $("#bulk_glossary2").click(function (e) {
            e.preventDefault();
            var selected_glossary_id = "";
            $('input[name="item[]"]:checked').each(function () {
                // console.log(this.value);
                selected_glossary_id = this.value + '|' + selected_glossary_id;
            });
            if (selected_glossary_id != "") {
                $('#check2').val(selected_glossary_id);
            } else {
                CommanJS.getDisplayMessgae(400, 'Please select atleast one glossary.')
                return false;
            }

            $.ajax({
                type: "POST",
                url: "<?php echo base_url('general_modules/glossary/setAllSelected'); ?>",
                data: {
                    selectedGlossary: selected_glossary_id,
                    status: '0'
                },
                dataType: 'json',
                success: function (data) {
                    // console.log(data);
                    if (data.msg === 'Updated') {
                        $('.checkAll').prop('checked', false);
                        table.ajax.reload(); //just reload table
                    }
                }
            });
        });

        $(document).on('click', '.change_status', function (e) {
        e.preventDefault();
        var scope = $(this);
        $.confirm({
        title: 'Confirm!',
                content: 'Are you sure?',
                buttons: {
                confirm: function () {
                    $.get(scope.attr("href"), // url
                    function (data, textStatus, jqXHR) { // success callback
                    var obj = JSON.parse(data);               
                        if (obj.msg === 'Updated') {

                        table.ajax.reload(); //just reload table
                        }                                
                    });
                return true;
                },
                        cancel: function () {
                        return true;
                        }
                }
            });      
        });
            $(document).on('click', '.delete_item', function (e) {
            e.preventDefault();
            var scope = $(this);
                    $.confirm({
                    title: 'Confirm!',
                            content: 'Are you sure?',
                            buttons: {
                            confirm: function () {
                            $.get(scope.attr("href"), // url
                                    function (data, textStatus, jqXHR) { // success callback
                                    var obj = JSON.parse(data);
                                    if (obj.msg === 'deleted') {
                                    table.ajax.reload(); //just reload table
                                    }
                                    });
                            return true;
                            },
                                    cancel: function () {
                                    return true;
                                    }
                            }
                    });
                });
        });


</script>