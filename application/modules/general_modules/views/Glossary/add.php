<?php
$glossary_id = isset($glossaryData->id) ? $glossaryData->id : NULL;
?>

<div class="container-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <ul class="nav nav-tabs" role="tablist" id="addLessonTab">
                <li role="presentation" class="active">
                    <a href="#glossary_content" role="tab" data-toggle="tab"> Content </a>
                </li>
                <li role="presentation">
                    <a href="#glossary_metadata" id="metadata_id" role="tab" data-id="<?php echo $glossary_id; ?>" data-toggle="tab"> Metadata </a>
                </li>
            </ul>
        </div>

        <div class="panel-body">
            <div class="tab-content">
                <!--// Content Tab Start  //-->
                <div role="tabpanel" class="tab-pane active" data-id="<?php echo $glossary_id; ?>" id="glossary_content">
                    <form name="addGlossary" id="addGlossary" method="post" enctype="multipart/form-data">


                        <?php
                        $hccid_data = [
                            'type' => 'hidden',
                            'id' => 'ccId',
                            'name' => 'ccId',
                            'value' => $glossary_id
                        ];
                        echo form_input($hccid_data);
                        ?>


                        <div class="row mt-4 mb-3">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <?php $term = isset($glossaryData->term) ? $glossaryData->term : ''; ?>
                                    <label for="title" class="control-label"> Term <span class="field-required">*</span> </label>
                                    <input type="text" name="term" value="<?php echo $term; ?>" id="term" class="form-control">
                                </div>
                            </div>

                        </div>

                        <div class="row mt-4 mb-3">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <?php $long_title = isset($glossaryData->long_title) ? $glossaryData->long_title : ''; ?>
                                    <label for="long_title" class="control-label"> Long Title </label>
                                    <input type="text" name="long_title" value="<?php echo $long_title; ?>" id="long_title" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-10">
                                <div class="form-group">
                                    <?php $definitions = isset($glossaryData->definitions) ? $glossaryData->definitions : ''; ?>
                                    <label for="definitions" class="control-label"> Definition <span class="field-required">*</span> </label>
                                    <textarea class="editor" id="definitions" name="definitions" rows="10"><?php echo $definitions; ?></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="hr-line mt-4 mb-4 col-md-12"></div>

                        <div class="istVisualContainer mb-0 pb-0">
                            <div class="row">
                                <div class="row col-sm-10">
                                    <div class="form-group col-xs-12 mt-4 mb-0">
                                        <label class="control-label block">Attached media file </label>
                                    </div>
                                    <div class="form-group col-xs-12">
                                        <!--<div class="col-md-4 p-0">
                                            
                                        </div>-->
                                        <div class="col-md-8">
                                            <div class="fileinput fileinput-new col-md-12 p-0" data-provides="fileinput">
                                                <input type="hidden" id="mediaFileName" class="form-control" placeholder="Select audio file">
                                                <span class="btn btn-primary btn-primary-default btn-file">
                                                    <span class="fileinput-new">Choose File</span>
                                                    <span class="fileinput-exists">Change</span>
                                                    <!--                                                    <input id="uploadImage" name="file" type="file" class="blog_img1 visible1" accept=".mp3,.wav,.ogg">-->
                                                    <input id="choose_media_image_file_audio" name="file" class="blog_img1 visible1">
                                                </span>
                                                <?php $show1 = !empty($glossaryData->media_file) ? "display:inline-block" : "display:none"; ?>
                                                <a class="btn btn-danger fileinput-exists" style="<?php echo $show1 ?>" id="mediaAudio">Remove</a>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <span id="glossaryAudio">
                                                <?php $media = isset($glossaryData->media_file) ? $glossaryData->media_file : '';
                                                if (!empty($media)) {
                                                    ?>
                                                    <audio  controls>
                                                        <source id="" src="<?php echo bs() . '' . $media; ?>" type="audio/mp3">
                                                    </audio>
                                                <?php
                                                }
                                                ?>
                                            </span>
                                        </div>
                                        <div class="col-md-12 p-0">
                                            <div class="small">Upload files (e.g. audio files, .mp3, .wav, .ogg) </div>
                                        </div>
                                        <div>
                                            <div class='progress' id="progressDivId" style="display:none">
                                                <div class='progress-bar' id='progressBar'></div>
                                                <div class='percent' id='percent'>0%</div>
                                            </div>
                                            <div style="height: 10px;"></div>
                                            <div id='outputImage'></div>
                                        </div>
                                    </div>





                                </div>
                            </div>
                        </div>
                        <div class="istVisualContainer mb-0 pb-0">
                            <div class="row">
                                <div class="row col-sm-10">
                                    <div class="form-group col-xs-12 mt-4 mb-0">
                                        <label class="control-label block">Attached image file </label>
                                    </div>
                                    <div class="form-group col-xs-12">
                                        <!-- <div class="col-md-4 p-0">
                                        
                                        </div>-->
                                        <div class="col-md-8">
                                            <div class="fileinput fileinput-new col-md-12 p-0" data-provides="fileinput">
                                                <input type="hidden" name="image_file_name" id="image_file_name" value="<?php echo $glossaryData->image_file; ?>" class="form-control">
                                                <span class="btn btn-primary btn-primary-default btn-file" id="image_file">
                                                    <span class="fileinput-new">Choose File</span>
                                                    <span class="fileinput-exists">Change</span>
                                                    <!--                                                    <input id="uploadImage" name="file" type="file" class="blog_img1 visible1" accept=".mp3,.wav,.ogg">-->
                                                    <input name="file" class="blog_img1 visible1">
                                                </span>
                                                <?php $show = !empty($glossaryData->image_file) ? "display:inline-block" : "display:none"; ?>
                                                <a class="btn btn-danger fileinput-exists" style="<?php echo $show ?>" id="image_remove">Remove</a>
                                                
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <?php if (!empty($glossaryData->image_file)) { ?>
                                                <img id="image_write_path" src="<?php echo bs('') . '' . $glossaryData->image_file; ?>" style=" width:100px;height:100px">
                                            <?php } else { ?>
                                                <img id="image_write_path" src="<?php echo bs("/public/assets/img/logo-bg.png") ?>" style=" width:100px;height:100px">
                                            <?php } ?>
                                        </div>
                                    </div>





                                </div>
                            </div>
                        </div>
                        <!-- <div class="row mt-4 mb-3">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <?php $image_file = isset($glossaryData->image_file) ? $glossaryData->image_file : ''; ?>
                                    <label for="long_title" class="control-label"> Image </label>
                                    <input type="text" name="image_file" value="<?php echo $image_file; ?>" id="image_file" class="form-control">
                                </div>
                            </div>
                        </div> -->

                        <div class="row mt-4 mb-3">
                            <?php $variations = isset($glossaryData->variations) ? $glossaryData->variations : ''; ?>
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <label for="long_title" class="control-label"> Variations </label>
                                    <input type="text" name="variations" value="<?php echo $variations; ?>" id="variations" class="form-control" placeholder="List term variations (plural, spellings) by a comma">
                                </div>
                            </div>
                        </div>
                        <div class="hr-line mt-4 mb-4 col-md-12"></div>

                        <div class="row mt-4 mb-3">
                            <div class="col-md-12 p-0">
                                <div class="form-group col-xs-12 pt-3">
                                    <label class="control-label block">
                                        Glossary Availability
                                    </label>
                                    <div class="col-md-12 p-0 pt-3">
                                        <?php
                                        $status = isset($glossaryData->status) ? $glossaryData->status : '';
                                        $avail = ($status == 1) ? 'checked' : '';
                                        $hidden = ($status == 0) ? 'checked' : '';
                                        ?>
                                        <label class="radio-tel m-0">
                                            <input type="radio" id="abc1" name="status" value="1" <?php echo $avail; ?> class="mr-2 t2"> Available
                                        </label>
                                        <label class="radio-tel m-0 ml-4">
                                            <input type="radio" id="abc2" name="status" value="0" <?php echo $hidden; ?> class="mr-2 t2"> Hidden
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        $fileName = isset($glossaryData->media_file) ? $glossaryData->media_file : '';
                        ?>
                        <input type="hidden" name="glossaryFileName" value="<?php echo $fileName; ?>" id="glossaryFileName">
                        <div class="hr-line mt-4 mb-4 col-md-12"></div>
                        <div class="col-xs-12 text-right">
                            <button class="btn btn-primary" type="submit" id="nextMetadata">Next</button>
                        </div>
                    </form>
                </div>
                <!--// Metadata Tab Start  //-->
                <div role="tabpanel" class="tab-pane" id="glossary_metadata">


                    <div class="pl-4 col-md-12">
                        <div class="col-md-6 pl-0 pt-5">
                            <div class="panel panel-grey">
                                <div class="panel-heading mb-3">
                                    <h2><span>Glossary Categories</span></h2>
                                </div>
                                <div class="panel-body panel-collapse-body" id="glossary_categories_section">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 pl-0 pt-5">
                            <div class="panel panel-grey">
                                <div class="panel-heading">
                                    <h2><span>Glossary Tags</span></h2>
                                    <div class="panel-ctrls button-icon-bg">
                                    </div>
                                </div>
                                <div class="panel-body" id="glossary_tags_section">

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="hr-line col-md-12"></div>

                    <div class="pl-4 col-md-12">
                        <div class="col-md-6 pl-0 pt-5">
                            <div class="panel panel-grey">
                                <div class="panel-heading mb-3">
                                    <h2><span>System Categories</span></h2>
                                </div>
                                <div class="panel-body panel-collapse-body" id="system_categories_section">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 pl-0 pt-5">
                            <div class="panel panel-grey">
                                <div class="panel-heading">
                                    <h2><span>System Tags</span></h2>
                                    <div class="panel-ctrls button-icon-bg">
                                    </div>
                                </div>
                                <div class="panel-body" id="system_tags_section">

                                </div>
                            </div>
                        </div>

                    </div>

                    <!-- <div class="col-xs-12 text-right">
                        <button class="btn btn-primary" type="button" id="nextMetadata">Save</button>
                    </div>  -->

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function media_image_callback(image_name, img_full_url) {
        output =  '<audio controls>';
        output +=  '<source src='+img_full_url+' type="audio/mp3" />';
        output +=  '</audio>';
        $('#glossaryAudio').html(output);
        $('#mediaFileName').val(image_name);
        $('#glossaryFileName').val(image_name);
        $('#mediaAudio').show();
    }

    $("#choose_media_image_file_audio").click(function() {
        CommanJS.choose_media_image_file('media_image_callback');
    });

    function image_file(image_name, img_full_url) {
        $('#image_write_path').attr('src', img_full_url);
        $('#image_file_name').val(image_name);
        $('#image_remove').show();

    }
    $(document).on('click', '#image_remove', function() {

        var module_id = '<?php echo $glossary_id; ?>';
        var filename = $("#image_file_name").val();
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure?',
            buttons: {
                confirm: function() {
                    //alert("true");
                    $("#image_file_name").val('');
                    $("#image_write_path").html('');
                    $("#image_write_path").attr('src', '<?php echo bs("/public/assets/img/logo-bg.png"); ?>');
                    //$("#image_write_path").attr('src', '<?php echo bs("/public/assets/img/logo-bg.png"); ?>');
                    $('#image_remove').hide();
                    //return true;
                },
                cancel: function() {
                    return true;
                }
            }
        });
    });
    $(document).on('click', '#mediaAudio', function() {

        var module_id = '<?php echo $glossary_id; ?>';
        var filename = $("#mediaFileName").val();

        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure?',
            buttons: {
                confirm: function() {
                    $("#glossaryFileName").val('');
                    $("#glossaryAudio").html('');
                    $("#glossaryAudio").attr('src', '<?php echo bs("/public/assets/img/logo-bg.png"); ?>');
                    $('#mediaAudio').hide();
                },
                cancel: function() {
                    return true;
                }
            }
        });
    });
    $("#image_file").click(function() {
        CommanJS.choose_media_image_file('image_file');
    });
    $(document).ready(function() {
        var config = {
            height: 200,
            toolbar: 'short'
        };
        CKEDITOR.replace('definitions', config);
        $('#metadata_id').click(function() {
            var glossary_id = $('#ccId').val();
            if (glossary_id != '') {
                getMetadata(glossary_id);
            } else {
                return false;
            }
        });

        $("#addGlossary").validate({

            ignore: [],
            rules: {
                term: {
                    required: true,
                },
                availability: {
                    required: true,
                },
                definitions: {
                    required: function(textarea) {
                        CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                        return editorcontent.length === 0;
                    },
                },
            },
            messages: {
                term: {
                    required: "Please enter term"
                },
                definitions: {
                    required: "Please enter definitions"
                },
                availability: {
                    required: "Please select availability"
                },
                getting_started: {
                    required: "Please enter course getting started content"
                },
            },
            submitHandler: function(form) {
                $.ajax({
                    url: "<?php echo bs(); ?>general_modules/glossary/addGlossary",
                    type: "POST",
                    data: $("#addGlossary").serialize(),
                    success: function(data) {
                        if (data > 0) {
                            $('.nav-tabs a[href="#glossary_metadata"]').tab('show');
                            $('.nav-tabs a[href="#glossary_content"]').attr('data-id', data);
                            $('.nav-tabs a[href="#glossary_metadata"]').attr('data-id', data);
                            $('#ccId').val(data);
                            $('#cmId').val(data);
                            getMetadata(data);
                            CommanJS.getDisplayMessgae(200, 'Glossary saved successfully.');
                            $(window).scrollTop(0);
                        }
                    }
                });
            },
            errorPlacement: function(error, $elem) {
                if ($elem.is('textarea')) {
                    $elem.insertAfter($elem.next('div'));
                }
                error.insertAfter($elem);
            },
        });

        function getMetadata(glossaryId) {
            // Glossary Category  13
            CommanJS.getCatSection(<?=META_GLOSSARY;?>, 'glossary_categories_section', 'glossary_categories', glossaryId, <?=META_GLOSSARY;?>);
            // Glossary tags 
            CommanJS.getTagSection(<?=META_GLOSSARY;?>, 'glossary_tags_section', 'glossary_tags', glossaryId, <?=META_GLOSSARY;?>);
            // System Category  1
            CommanJS.getCatSection(<?=META_SYSTEM;?>, 'system_categories_section', 'system_categories', glossaryId, <?=META_GLOSSARY;?>);
            // System tags  1
            CommanJS.getTagSection(<?=META_SYSTEM;?>, 'system_tags_section', 'system_tags', glossaryId, <?=META_GLOSSARY;?>);
        }
    });
</script>