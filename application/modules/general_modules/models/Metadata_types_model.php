<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
  Author Akash Hedaoo
  Date 16/04/2019
 */

class Metadata_types_model extends CI_Model
{

    function __construct()
    {
        // Set table name
        $this->table = 'metadata_types';
        // Set orderable column fields
        $this->column_order = array('id', 'metadata_types.name', 'created_by', 'created', 'status');
        // Set searchable column fields
        $this->column_search = array('metadata_types.name');
        // Set default order
        $this->order = array('created' => 'desc');
    }

    /*
     * Fetch members data from the database
     * @param $_POST filter data based on the posted parameters
     */

    public function getRows($postData)
    {
        //pr($postData,"k");
        $this->_get_datatables_query($postData);
        if ($postData['length'] != -1) {
            $this->db->limit($postData['length'], $postData['start']);
        }
        $query = $this->db->get();

        $data['result'] = $query->result();
        $data['total'] = $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total;
        return $data;
    }

    /*
     * Count records based on the filter params
     * @param $_POST filter data based on the posted parameters
     */

    public function countFiltered($postData)
    {
        $this->_get_datatables_query($postData);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function update_status($id = null, $data)
    {
        $this->db->where('id', $id);
        $delete = $this->db->update('metadata_types', $data);
        if ($delete) :
            return true;
        endif;
    }

    //Count modules
    public function count_records()
    {
        $this->db->select('*');
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    /*
     * Perform the SQL queries needed for an server-side processing requested
     * @param $_POST filter data based on the posted parameters
     */

    private function _get_datatables_query($postData)
    {
        $this->db->select('SQL_CALC_FOUND_ROWS ' . $this->table . '.id, ' . $this->table . '.name, ' . $this->table . '.created,' . $this->table . '.description, ' . $this->table . '.status', false);
        $this->db->select('DATE_FORMAT(' . $this->table . '.created, "%m/%d/%Y") as created');
        $this->db->select("IF(" . $this->table . ".status = 0, 'Publish', 'Unpublish') as status");
        $this->db->from($this->table);
        $this->db->where($this->table . '.status_delete = 0');

        $i = 0;
        // loop searchable columns 
        foreach ($this->column_search as $item) {
            // if datatable send POST for search
            if ($postData['search']['value']) {
                // first loop
                if ($i === 0) {
                    // open bracket
                    $this->db->group_start();
                    $this->db->like($item, $postData['search']['value'], 'both');
                } else {
                    $this->db->or_like($item, $postData['search']['value'], 'both');
                }

                // last loop
                if (count($this->column_search) - 1 == $i) {
                    // close bracket
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($postData['status']) && $postData['status'] < 2) {
            $this->db->where($this->table . '.status', $postData['status']);
        }

        if (isset($postData['order'])) {
            $this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
}

/* End of file Users_modal.php */
