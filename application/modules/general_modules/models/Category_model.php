<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
  Author Akash Hedaoo
  Date 16/04/2019
 */

class Category_model extends CI_Model
{

    function __construct()
    {
        // Set table name
        $this->table = 'categories';
        // Set orderable column fields
        $this->column_order = array(null, null, 'status', 'metadata_types.name', 'categories.name', 'created_by', 'created');
        // Set searchable column fields
        $this->column_search = array('metadata_types.name', 'categories.name');
        // Set default order
        $this->order = array('created' => 'desc');
    }

    /*
     * Fetch members data from the database
     * @param $_POST filter data based on the posted parameters
     */

    public function getRows($postData)
    {
        $this->_get_datatables_query($postData);
        if ($postData['length'] != -1) {
            $this->db->limit($postData['length'], $postData['start']);
        }
        $query = $this->db->get();
        $data['result'] = $query->result();
        $data['total'] = $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total;
        return $data;
    }

    public function update_status($id = null, $data)
    {
        $this->db->where('id', $id);
        $delete = $this->db->update('categories', $data);
        if ($delete) :
            return true;
        endif;
    }

    /*
     * Count records based on the filter params
     * @param $_POST filter data based on the posted parameters
     */

    public function countFiltered($postData)
    {
        $this->_get_datatables_query($postData);
        $query = $this->db->get();
        $data['result'] = $query->result();
        $data['total'] = $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total;
        return $data;
    }

    //Count modules
    public function count_records()
    {
        $this->db->select('*');
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    /*
     * Perform the SQL queries needed for an server-side processing requested
     * @param $_POST filter data based on the posted parameters
     */

    private function _get_datatables_query($postData)
    {
        $this->db->select('SQL_CALC_FOUND_ROWS ' . $this->table . '.id, ' . $this->table . '.type, ' . $this->table . '.name, ' . $this->table . '.created_by, ' . $this->table . '.created, ' . $this->table . '.status as status_button, users.first_name, users.last_name, metadata_types.name AS type_name', false);
        $this->db->select('DATE_FORMAT(' . $this->table . '.created, "%m/%d/%Y") as created');
        $this->db->select("IF(" . $this->table . ".status = 1, 'Publish', 'Unpublish') as status");
        $this->db->select("IF(" . $this->table . ".type = 1, 'System', 'Default') as type");
        $this->db->select("CONCAT_WS(' ', users.first_name, users.middle_name, users.last_name) as user_name");
        $this->db->from($this->table);
        $this->db->join('metadata_types', $this->table . '.type = metadata_types.id');
        $this->db->join('users', $this->table . '.created_by = users.id');
        $this->db->where($this->table . '.delete_status = 1');

        $i = 0;
        // loop searchable columns 
        foreach ($this->column_search as $item) {
            // if datatable send POST for search
            if ($postData['search']['value']) {
                // first loop
                if ($i === 0) {
                    // open bracket
                    $this->db->group_start();
                    $this->db->like($item, $postData['search']['value'], 'both');
                } else {
                    $this->db->or_like($item, $postData['search']['value'], 'both');
                }

                // last loop
                if (count($this->column_search) - 1 == $i) {
                    // close bracket
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($postData['system_type']) && $postData['system_type'] > 0) {
            $this->db->where($this->table . '.type', $postData['system_type']);
        }

        if (isset($postData['order'])) {
            $this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    //fetch menus
    public function fetch_menus()
    {
        $this->db->select('*');
        $this->db->from('metadata_types');
        $this->db->where(["status" => 1, "status_delete" => 1]);
        return $this->db->get()->result_array();
    }

    //get data acc to metadata
    public function menu_filter_data($postData)
    {
        if(!empty($postData['user_metadata_selected'])) {
            $this->_get_datatables_query_menu_filter_data($postData);
            if ($postData['length'] != -1) {
                $this->db->limit($postData['length'], $postData['start']);
            }
            $query = $this->db->get();
            $data['result'] = $query->result();
            $data['total'] = $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total;
            return $data;
        } else {
            $data=[];
            return $data;
        }
        
        
        
        
    }

    /*
     * Perform the SQL queries needed for an server-side processing requested
     * @param $_POST filter data based on the posted parameters
     */

    private function _get_datatables_query_menu_filter_data($postData)
    {
        //query start
        $query = '';

        //select clause
        $query .='SELECT SQL_CALC_FOUND_ROWS ';

        //conditional select clause
        if (in_array(4, $postData['user_metadata_selected'])) {
            $query .='courses.name ';
        }

        //main query table
        $this->db->from('category_assigned');

        //conditional tables
        if (in_array(4, $postData['user_metadata_selected'])) {
            $query .=' JOIN courses as courses ON courses.id = category_assigned.category_id ';
        }

        //where clause
        $query .='WHERE ';
        
        //where conditions
        if (in_array(4, $postData['user_metadata_selected'])) {
            $query .='courses.status = 1 ';
        }
        
        
    }
}
