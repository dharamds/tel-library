<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  Author Akash Hedaoo
  Date 16/04/2019
 */

class Glossary_model extends CI_Model {

    function __construct() {
        // Set table name
        $this->table = 'glossaries';
        // Set orderable column fields
            $this->column_order = array(null,null,'status', 'term','glossary_categories', 'glossary_tags', 'system_category', 'system_tags', null, null);
        // Set searchable column fields
        $this->column_search = array('glossaries.term', 'glossaries.long_title');
        // Set default order
        $this->order = array('glossaries.id' => 'desc');
    }

    /*
     * Fetch members data from the database
     * @param $_POST filter data based on the posted parameters
     */

    public function getRows($postData) {

        $this->_get_datatables_query($postData);
        if ($postData['length'] != -1) {
            $this->db->limit($postData['length'], $postData['start']);
        }
        $query = $this->db->get();
        $data['result'] = $query->result();
        $data['total'] = $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total;
        return $data;
    }

    public function update_status($id = null, $data) {

        $this->db->where('id', $id);
            $delete = $this->db->update('glossaries', $data);
            
        if ($delete):
            return true;
        endif;
    }

    /*
     * Count records based on the filter params
     * @param $_POST filter data based on the posted parameters
     */

    public function countFiltered($postData) {
        $this->_get_datatables_query($postData);
        $query = $this->db->get();
        $data['result'] = $query->result();
        $data['total'] = $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total;
        return $data;
    }

    //Count modules
    public function count_records() {
        $this->db->select('*');
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    /*
     * Perform the SQL queries needed for an server-side processing requested
     * @param $_POST filter data based on the posted parameters
     */

    private function _get_datatables_query($postData) {
      //  $this->db->cache_on();
        $this->db->select('SQL_CALC_FOUND_ROWS ' . $this->table . '.id, ' . $this->table . '.term, ' . $this->table . '.long_title, '. $this->table . '.status', false);
        $this->db->select('DATE_FORMAT(' . $this->table . '.created, "%m/%d/%Y") as created');
       // $this->db->select("IF(" . $this->table . ".status = 1, 'Publish', 'Unpublish') as status");
        $this->db->select("CONCAT_WS(' ', users.first_name, users.middle_name, users.last_name) as user_name");
        
        // system metadata
        if(isset($postData['system_cat']) && $postData['system_cat'] > 0 || $postData['order']['0']['column'] == 6) {
        $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT system_category.name ORDER BY system_category.name ASC  SEPARATOR "<br>"), \'NA\')  as system_category');
        }else{
            $this->db->select('"..." as system_category');
        }
        if(isset($postData['system_tag']) && $postData['system_tag'] > 0 || $postData['order']['0']['column'] == 7) {
        $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT system_tags.name ORDER BY system_tags.name ASC  SEPARATOR "<br>"), \'NA\')  as system_tags');
        }else{
            $this->db->select('"..." as system_tags');
        }   
        // glossary metadata

        if(isset($postData['glossary_cat']) && $postData['glossary_cat'] > 0 || $postData['order']['0']['column'] == 4) {
            $this->db->select('GROUP_CONCAT( DISTINCT glossary_categories.name SEPARATOR \'<br> \')  as glossary_categories');
        }else{
            $this->db->select('"..." as glossary_categories');
        }

        if(isset($postData['glossary_tag']) && $postData['glossary_tag'] > 0 || $postData['order']['0']['column'] == 5) {
            $this->db->select('GROUP_CONCAT( DISTINCT glossary_tags.name SEPARATOR \'<br> \')  as glossary_tags');
        }else{
            $this->db->select('"..." as glossary_tags');
        } 


        $this->db->from($this->table);
        $this->db->join('users', $this->table . '.created_by = users.id', 'left');
        if(isset($postData['system_cat']) && $postData['system_cat'] > 0 || $postData['order']['0']['column'] == 6) {
            $this->db->join('category_assigned as assigned_system_category', 'assigned_system_category.reference_id = glossaries.id AND assigned_system_category.reference_type = '.META_SYSTEM.' AND  assigned_system_category.reference_sub_type ='.META_GLOSSARY, 'left');
            $this->db->join('categories as system_category', 'system_category.id = assigned_system_category.category_id AND system_category.delete_status = 1 AND system_category.status = 1', 'left');
        }
        if(isset($postData['system_tag']) && $postData['system_tag'] > 0 || $postData['order']['0']['column'] == 7) {
            $this->db->join('tag_assigned as assigned_system_tags', 'assigned_system_tags.reference_id = glossaries.id AND assigned_system_tags.reference_type = '.META_SYSTEM.' AND  assigned_system_tags.reference_sub_type ='.META_GLOSSARY, 'left');
            $this->db->join('tags as system_tags', 'system_tags.id = assigned_system_tags.tag_id AND system_tags.delete_status = 1 AND system_tags.status = 1', 'left');
        }
        if(isset($postData['glossary_cat']) && $postData['glossary_cat'] > 0 || $postData['order']['0']['column'] == 4) {
            $this->db->join('category_assigned as assigned_glossary_category', 'assigned_glossary_category.reference_id = glossaries.id AND assigned_glossary_category.reference_type = '.META_GLOSSARY.' AND  assigned_glossary_category.reference_sub_type ='.META_GLOSSARY, 'left');
            $this->db->join('categories as glossary_categories', 'glossary_categories.id = assigned_glossary_category.category_id AND glossary_categories.delete_status = 1 AND glossary_categories.status = 1', 'left');
        }
        if(isset($postData['glossary_tag']) && $postData['glossary_tag'] > 0 || $postData['order']['0']['column'] == 5) {
            $this->db->join('tag_assigned as assigned_glossary_tags', 'assigned_glossary_tags.reference_id = glossaries.id AND assigned_glossary_tags.reference_type = '.META_GLOSSARY.' AND  assigned_glossary_tags.reference_sub_type ='.META_GLOSSARY, 'left');
            $this->db->join('tags as glossary_tags', 'glossary_tags.id = assigned_glossary_tags.tag_id AND glossary_tags.delete_status = 1 AND glossary_tags.status = 1', 'left');  
        }
        $this->db->where($this->table . '.delete_status = 1');

        $i = 0;
        // loop searchable columns 
          // loop searchable columns 
          foreach($this->column_search as $item){
            // if datatable send POST for search
            if($postData['search']['value']){
                // first loop
                if($i === 0){
				    // open bracket 
					$this->db->group_start();
					//pr( $item);	
                    $this->db->like($item, $postData['search']['value'], 'both');
                }else{
                    $this->db->or_like($item, $postData['search']['value'], 'both');
                }
                    // last loop
                if(count($this->column_search) - 1 == $i){
                    // close bracket
                    $this->db->group_end();
                }
            }
            $i++;
        }

         // set system_cat 
         if(isset($postData['system_cat']) && $postData['system_cat'] > 0) {
            $this->db->where_in('assigned_system_category.category_id', $postData['system_cat']);
        }

        // set system_tag 
        if(isset($postData['system_tag']) && $postData['system_tag'] > 0) {
            $this->db->where_in('assigned_system_tags.tag_id', $postData['system_tag']);
        }

        if(isset($postData['glossary_cat']) && $postData['glossary_cat'] > 0) {
            $this->db->where_in('assigned_glossary_category.category_id', $postData['glossary_cat']);
        }
    
        if(isset($postData['glossary_tag']) && $postData['glossary_tag'] > 0) {
            $this->db->where_in('assigned_glossary_tags.tag_id', $postData['glossary_tag']);
        }     

        if (isset($postData['status']) && $postData['status'] < 2) {
            $this->db->where($this->table . '.status', $postData['status']);
        }

        $this->db->group_by($this->table.'.id');

        if (isset($postData['order'])) {
            $this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

}
