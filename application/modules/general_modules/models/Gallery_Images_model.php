<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  Author Akash Hedaoo
  Date 16/04/2019
 */

class Gallery_Images_model extends CI_Model {

    function __construct() {
        
    }

    public function img_div() {
        $this->db->select('*');
        return $this->db->from('media_sources')->where(["status"=>1,"delete_status"=>1])->limit(30)->get()->result();
    }

    public function filter_data($media_cat=0,$media_tag=0,$search=null, $page= 0) {
        $this->db->select('*');
        $this->db->from('media_sources');
        if(count($media_cat) >0) {
            $this->db->join('category_assigned','category_assigned.reference_id=media_sources.id AND category_assigned.reference_type='.META_MEDIA);
        }
        if(count($media_tag) > 0) {
            $this->db->join('tag_assigned','tag_assigned.reference_id=media_sources.id AND tag_assigned.reference_type='.META_MEDIA);
        }
        
        if(count($media_cat) > 0) {
            $this->db->where_in('category_assigned.category_id',$media_cat);
        }
        if(count($media_tag) > 0) {
            $this->db->where_in('tag_assigned.tag_id',$media_tag);
        }

        if($search != '') {
            $this->db->like('media_sources.title',$search, 'both');
        }

        $this->db->where(["status"=>1,"delete_status"=>1]);
        $this->db->order_by('media_sources.id', 'desc');
        $this->db->limit(24, $page);
        $data = $this->db->get()->result();
        
        return $data;
    }


}
