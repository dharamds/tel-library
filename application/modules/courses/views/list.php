       <div class="container-fluid">
            <div class="row">
        <div class="col-md-12">
            <div class="filter-container flex-row">
                <div class="flex-col-sm-3 selected_condition" id="system_cat">

                </div>
                <div class="flex-col-sm-2 selected_condition" id="system_tag">

                </div>
                <div class="flex-col-sm-3 selected_condition" id="course_cat">

                </div>
                <div class="flex-col-sm-2 selected_condition" id="course_tag">

                </div>
                <div class="flex-col-12 flex-col-md-auto ml-auto">
                    <a class="btn btn-danger pt-2" id="reset_form" href="javascript:void(0)"><i class="flaticon-close-1 f21 fw100"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div data-widget-group="group1">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default panel-grid">
                    <div class="panel-heading">

                    <?php if ($this->ion_auth->is_admin() || $permissions->add_per): ?>
                        <a class="btn btn-primary" href="<?= base_url('courses/add') ?>">Add New</a>
                    <?php endif; ?>    
                    <?php if ($this->ion_auth->is_admin() || $permissions->delete_per): ?>
                <a href="<?php echo base_url('modules/setAllSelected') ?>" class="btn btn-primary" id="bulk_courses_1">
                    Set all selected as available
                </a>
            <?php endif;?>
            <?php if ($this->ion_auth->is_admin() || $permissions->delete_per): ?>
                <a href="<?php echo base_url('modules/setAllSelected') ?>" class="btn btn-primary" id="bulk_courses_2">
                    Set all selected as unavailable
                </a>
            <?php endif;?>    
                        <div class="panel-ctrls"></div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="coursesListTable" class="table table-bordered table-striped table-hover" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th style="min-width: 50px;">
                                                <label class="checkbox-tel">
                                                    <input type="checkbox" class="checkAll">
                                                </label>
                                            </th>
                                            <th style="min-width: 100px;">Action</th>
                                            <th style="min-width: 120px;">Available</th>
                                            <th style="min-width: 120px;">Title</th>
                                            <th style="min-width: 250px;">Long Title</th>
                                            <th style="min-width: 200px;">Course Categories</th>
                                            <th style="min-width: 150px;">Course Tags</th>
                                            <th style="min-width: 200px;">System Categories</th>
                                            <th style="min-width: 170px;">System Tags</th>
                                            <th style="min-width: 250px;">No. of Current Enrollments</th>
                                            <th style="min-width: 150px;">Current Institutions</th>
                                            <th style="min-width: 150px;">Created</th>
                                            
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#reset_form').click(function() {
            $(".meta_data_filter").prop("checked",false);
            $(".selectFilterMode span").text('');            
            table.search('').draw();
        });
    $("#bulk_courses_1").click(function(e) {
            e.preventDefault();
            var selected_user_id = "";
            $('input[name="item[]"]:checked').each(function() {
                //  console.log(this.value);
                selected_user_id = this.value + '|' + selected_user_id;
            });
            if (selected_user_id != "") {
                $('#check').val(selected_user_id);
            } else {
                CommanJS.getDisplayMessgae(400, 'Please select atleast one lesson.')
                return false;
            }
            $.ajax({
                type: "POST",
                url: "courses/setAllSelected",
                data: {
                    selectedLesson: selected_user_id,
                    status: '1'
                },
                dataType: 'json',
                success: function(data) {
                    //console.log(data);
                    if (data.msg === 'Updated') {
                        $('.checkAll').prop('checked', false);
                        table.ajax.reload(); //just reload table
                    }
                }
            });
        });
        $("#bulk_courses_2").click(function(e) {
            e.preventDefault();
            var selected_user_id = "";
            $('input[name="item[]"]:checked').each(function() {
                // console.log(this.value);
                selected_user_id = this.value + '|' + selected_user_id;
            });
            if (selected_user_id != "") {
                $('#check2').val(selected_user_id);
            } else {
                CommanJS.getDisplayMessgae(400, 'Please select atleast one lesson.')
                return false;
            }

            $.ajax({
                type: "POST",
                url: "courses/setAllSelected",
                data: {
                    selectedLesson: selected_user_id,
                    status: '0'
                },
                dataType: 'json',
                success: function(data) {
                    // console.log(data);
                    if (data.msg === 'Updated') {
                        $('.checkAll').prop('checked', false);
                        table.ajax.reload(); //just reload table
                    }
                }
            });
        });
    $('.checkAll').click(function() {
        $(':checkbox.checkLesson').prop('checked', this.checked);
    });
        CommanJS.get_metadata_options("System category", 1, <?=META_SYSTEM;?>, "system_cat");
        CommanJS.get_metadata_options("System tag", 2, <?=META_SYSTEM;?>, "system_tag");
        CommanJS.get_metadata_options("Course category", 1, <?=META_COURSE;?>, "course_cat");
        CommanJS.get_metadata_options("Course tag", 2, <?=META_COURSE;?>, "course_tag");
        
        $('.selected_condition').on('change', function() {
            table.search('').draw();
        });
        var table = table = $('#coursesListTable').DataTable({
            // Processing indicator
            "processing": true,
            // DataTables server-side processing mode
            "serverSide": true,
            // Initial no order.
            "iDisplayLength": 10,
            "bPaginate": true,
            // "order": [],
            "scrollX": true,
            "autoWidth": false,
            "drawCallback": function(settings) {
                $('.load_meta_data').each(function(key, item) {
                    $.getJSON("<?php echo bs('questions/get_meta_tags/'); ?>" + $(this).attr('id'), function(data) {
                           if(data) $("#"+data.typeid).html(data.value);
                    });
				});
				$('.load_meta_category').each(function(key, item) {
                    $.getJSON("<?php echo bs('questions/get_meta_categories/'); ?>" + $(this).attr('id'), function(data) {
                           if(data) $("#"+data.typeid).html(data.value);
                    });
				});

                $('.load_current_enrollments_count').each(function(key, item) {
                    //alert()
                    $.getJSON("<?php echo bs('courses/get_current_enrollments_count/'); ?>" + $(this).attr('id'), function(data) {
                        if (data) $("#" + data.typeid).html(data.value);
                    });
                });

                $('.load_current_enrollments').each(function(key, item) {
                    $.getJSON("<?php echo bs('courses/get_current_enrollments/'); ?>" + $(this).attr('id'), function(data) {
                        if (data) $("#" + data.typeid).html(data.value);
                    });
                });

                },
            // Load data from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('courses/getLists'); ?>",
                "type": "POST",
                "data": function(data) {
                   /// data.active = $('#select_status option:selected').val();
                    data.system_cat = CommanJS.getMetaCallBack('system_category'); 
                    data.system_tag = CommanJS.getMetaCallBack('system_tag');
                    data.course_cat = CommanJS.getMetaCallBack('course_category');
                    data.course_tag = CommanJS.getMetaCallBack('course_tag');
                    //data.active = $('#select_status option:selected').val();
                    data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                },
            },
            //Set column definition initialisation properties
            "columnDefs": [{
                    "targets": [0],
                    // "searchable": false,
                    "orderable": false,
                    "data": null,
                    // "width": "4%",
                    "render": function(data, type, full, meta) {
                        var data = '';
                        if (type == 'display') {
                            data = '<label class="checkbox-tel"><input type="checkbox" class="checkLesson" name="item[]" value="' + full['id'] + '"></label>';
                        }
                        return data;
                    }
                },
                {
                    "targets": [2],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                            if (full['status'] == '1') {
                                data = '<a href="<?php bs('courses/Courses/update_status/') ?>' + full['id'] + '/deactivate" data-toggle="tooltip" data-placement="top" title="Click to Change Status" class="text-success f18 change_status"><i class="flaticon-checked"></i></a>';
                            } else {
                                data = '<a href="<?php bs('courses/Courses/update_status/') ?>' + full['id'] + '/activate" data-toggle="tooltip" data-placement="top" title="Click to Change Status" class="text-danger f18 change_status"><i class="flaticon-close"></i></a>';
                            }
                        }
                        return data;
                    }
                },
                {
                    "targets": [1],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                            data = '';
                        <?php if ($this->ion_auth->is_admin() || $permissions->edit_per) : ?>
                            data = '<a class="btn btn-primary btn-sm" href="<?php echo base_url('courses/edit/') ?>' + full['id'] + '" > <i class="ti ti-pencil"></i> </a>';
                            <?php
                        endif;
                        if ($this->ion_auth->is_admin() || $permissions->view_per) :
                            ?>
                            /*data += '<a class="btn btn-midnightblue-alt btn-sm" href="< ?php echo base_url('courses/detail/') ?>' + full['id'] + '"> <i class="ti ti-eye"></i> </a>';*/
                        //    data += '<a class="btn btn-info btn-sm" href="#"> <i class="ti ti-eye"></i> </a>';
                            <?php
                        endif;
                        if ($this->ion_auth->is_admin() || $permissions->delete_per) :
                            ?>
                            data += '<a class="btn btn-danger btn-sm delete_item" href="<?php echo base_url('courses/delete/') ?>' + full['id'] + '"> <i class="ti ti-trash"></i> </a>';
                        <?php endif; ?>
                        }
                        return data;
                    }
                },
				{
                    "targets": [5],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                         data = '<span id="' + full['id'] +'_<?php echo META_COURSE; ?>_<?php echo META_COURSE; ?>_cat" data-type="course_category" class="load_meta_category"> Loading...</span>';
						  //data = 1;
						}
                        return data;
                    }
                },
				{
                    "targets": [6],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                           data = '<span id="' + full['id'] +'_<?php echo META_COURSE; ?>_<?php echo META_COURSE; ?>" data-type="course_tags" class="load_meta_data"> Loading...</span>';
                        }
                        return data;
                    }
                },
                {
                    "targets": [7],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                           data = '<span id="' + full['id'] +'_<?php echo META_COURSE ; ?>_<?php echo META_SYSTEM; ?>_syscat" data-type="system_category" class="load_meta_category"> Loading...</span>';
						 //data = 2;
						}
                        return data;
                    }
                },
                {
                    "targets": [8],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                           data = '<span id="' + full['id'] +'_<?php echo META_COURSE ; ?>_<?php echo META_SYSTEM; ?>_systag" data-type="system_tags" class="load_meta_data"> Loading...</span>';
                        }
                        return data;
                    }
                },
                {
                    "targets": [9],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                            data = '<span id="' + full['id'] + '_current_enrollments_count"  class="load_current_enrollments_count"> Loading...</span>';
                        }
                        return data;
                    }
                },
                {
                    "targets": [10],
                    "data": null,
                    "orderable": true,
                    "render": function(data, type, full, meta) {
                        if (type === 'display') {
                            data = '<span id="' + full['id'] + '_current_enrollments"  class="load_current_enrollments"> Loading...</span>';
                        }
                        return data;
                    }
                },
            ],
            "columns": [
                {
                    "data": "checkbox",
                    "autoWidth": true
                },
                {
                    "data": null,
                    "autoWidth": true
                },
                {
                    "data": null,
                    "autoWidth": true
                },
                {
                    "data": "name",
                    "autoWidth": true
                },
                {
                    "data": "long_title",
                    "autoWidth": true
                },
                {
                    "data": "course_categories",
                    "autoWidth": true
                },
                {
                    "data": "course_tags",
                    "autoWidth": true
                },
                {
                    "data": "system_category",
                    "autoWidth": true
                },
                {
                    "data": "system_tags",
                    "autoWidth": true
                },
                {
                    "data": "enrolled_users",
                    "autoWidth": true
                },
                {
                    "data": "institutions",
                    "autoWidth": true
                },
                {
                    "data": "created",
                    "autoWidth": true
                }
            ]
        });

        $(document).on('click', '.change_status', function (e) {
            e.preventDefault();
            $.get($(this).attr("href"), // url
                    function (data, textStatus, jqXHR) { // success callback
                        var obj = JSON.parse(data);
                        if (obj.msg === 'Updated') {
                            table.ajax.reload();  //just reload table
                        }
                    });
        });

        // To delete record
        $(document).on('click', '.delete_item', function (e) {
            e.preventDefault();
            var scope = $(this);
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure?',
                buttons: {
                    confirm: function () {
                        $.get(scope.attr("href"), // url
                                function (data, textStatus, jqXHR) { // success callback
                                    var obj = JSON.parse(data);
                                    if (obj.msg === 'deleted') {
                                        table.ajax.reload(); //just reload table
                                    }
                                });
                        return true;
                    },
                    cancel: function () {
                        return true;
                    }
                }
            });
        });
    });
       
    $(document).ready(function() {
        $("#coursesListTable_paginate .pagination").click(function() {
            $("html, body").animate({
                scrollTop: 0
            }, "slow");
        });
    });
</script>