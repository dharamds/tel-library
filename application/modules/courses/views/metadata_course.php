<!--// Start Metadate Tab  //-->
<div role="tabpanel" class="tab-pane" id="metadata">
    <?php
    echo form_open('', [
        'name' => 'course_img_up',
        'id' => 'course_img_up',
        'enctype' => 'multipart/form-data'
    ]);
    ?>
    <?php
    $hide_meta_id_data = [
        'type' => 'hidden',
        'id' => 'crs_up_img_meta_id',
        'name' => 'crs_up_img_meta_id',
        'value' => isset($courseID) ? $courseID : ''
    ];
    echo form_input($hide_meta_id_data);
    //echo "ssfdsa".$courseData->course_img;
    ?>
    <div class="istVisualContainer">
        <div class="row">
            <div class="row col-sm-10">
                <div class="form-group col-xs-12 mt-4">
                    <label class="control-label block"> Course Image </label>
                </div>

                <div class="col-sm-3 mr-5">
                    <div class="ist-logo">
                        <?php
                        if (isset($courseData->course_img) && (!empty($courseData->course_img))) {
                            ?>
                            <img src="<?php echo base_url($courseData->course_img); ?>" id="image_write_path" class="img-rounded">
                        <?php } else { ?>
                            <img src="<?php echo base_url(); ?>/public/assets/img/logo-bg.png" id="image_write_path" class="img-rounded">
                        <?php } ?>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="ist-visual-info">
                        <h4>Select Course Image</h4>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="old_img" value="">
                                <div class="fileinput fileinput-new col-md-12 p-0" data-provides="fileinput">
                                    <span class="btn btn-primary btn-primary-default btn-file" id="choose_media_image_file1">
                                        <span class="fileinput-new">Choose File</span>
                                        <span class="fileinput-exists">Choose File</span>
                                    </span>
                                    <a class="btn btn-danger fileinput-exists delete_img_main_page">Remove</a>
                                </div>

                                <div class="small"> (File must be .png, .jpg, .jpeg) </div>
                            </div>
                        </div>
                        <div style="color:red;" id="upload_error"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>

    <?php
    echo form_open('', [
        'name'      => 'courseMetadata',
        'id'        => 'courseMetadata',
        'enctype'   => 'multipart/form-data'
    ]);
    ?>
    <?php
    $hcmid_data = [
        'type'  => 'hidden',
        'name'  => 'course_metadata_id',
        'id'    => 'course_metadata_id',
        'value' => isset($courseID) ? $courseID : ''
    ];
    echo form_input($hcmid_data);
    ?>

    <?php
    $hide_course_img = [
        'type'  => 'hidden',
        'id'    => 'uploadedCourseImg',
        'name'  => 'uploadedCourseImg',
        'value' => isset($courseData->course_img) ? $courseData->course_img : ''
    ];
    echo form_input($hide_course_img);
    ?>
    <input type="hidden" name="img_name" id="img_name" value="<?php echo $courseData->course_img ?>">
    <div class="certificate-list">
        <div class="row">
            <div class="col-xs-6">
                <div class="form-group col-xs-12">
                    <label class="control-label block"> Course Certificates</label>
                    <div class="col-md-12 col-xs-12">
                        <?php $certificate_id = isset($courseData->certificate_id) ? $courseData->certificate_id : 0; ?>
                        <?php echo form_dropdown('certificates', $certificateData, $certificate_id, ['class' => 'form-control']); ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group col-xs-6">
                    <label class="control-label block">Certificates requirement </label>
                    <div class="input-group">
                        <?php
                        $certificate_val = isset($courseData->certificate_requirement) ? $courseData->certificate_requirement : '';
                        $cert_req_data = [
                            'name' => 'certificate_requirement',
                            'id' => 'certificate_requirement',
                            'class' => 'form-control',
                            'value' => $certificate_val
                        ];
                        ?>
                        <?php echo form_input($cert_req_data); ?>
                        <span class="input-group-addon">%</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="hr-line mt-5 mb-5"></div>

    <div class="row">
        <div class="col-md-6 p-0">
            <div class="form-group col-xs-12">
                <label class="control-label block">
                    Course Availability
                </label>
                <div class="row col-xs-12 m-0 p-0 mt-3">
                    <div class="col-md-6 p-0">
                        <label class="radio-tel m-0">
                            <?php  $avail_status = '';
                            if (isset($courseID) && $courseData->status == 1) {
                                $avail_status = 'checked';
                            } 
                            $data_avai = [
                                'name'      => 'course_availability',
                                'id'        => 'course_avai',
                                'value'     => 1,
                                'checked'   => $avail_status,
                                'class'     => 'mr-3 t2'
                            ];
                            echo form_radio($data_avai);
                            ?> Available
                        </label>
                    </div>
                    <div class="col-md-6 p-0">
                        <label class="radio-tel m-0">
                            <?php
                            $hide_status = '';
                            if (isset($courseID) && $courseData->status == 0) {
                                $hide_status = 'checked';
                            } else {
                                $hide_status = '';
                            }
                            ?>
                            <?php
                            $data_avai = [
                                'name'      => 'course_availability',
                                'id'        => 'course_hid',
                                'value'     => 0,
                                'checked'   => $hide_status,
                                'class'     => 'mr-3 t2'
                            ];
                            echo form_radio($data_avai);
                            ?> Hidden
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6 p-0 ">
            <div class="form-group col-xs-12 mb-3">
                <div>
                    <label class="checkbox-tel m-0">
                        <?php
                        if ($courseData->exp_status == 1)
                            $st_exp = 'checked';
                        else  $st_exp = ''; ?>
                        <input type="checkbox" name="exp_status" class="t2 mr-2" id="exp_status" value="1" <?php echo $st_exp; ?>> Set Course Expiry
                    </label>
                </div>
                <?php
                $display_block = "none";
                if ($courseData->exp_status == 1 && $courseID > 0) {
                    $display_block = "block";
                }
                ?>
                <div class="form-group colorPicker pl-5 pt-4 course_expiry_div" style="display: <?php echo $display_block; ?>">
                    <label class="col-md-5 p-0">Course Expiry Date</label>
                    <div class="input-group col-md-8">
                        <?php
                        $course_exp_date = "";
                        if (isset($courseData->course_expiry)) {
                            $exp_dt = explode('-', $courseData->course_expiry);
                            $course_exp_date = $exp_dt[1] . "/" . $exp_dt[2] . "/" . $exp_dt[0];
                        }
                        $data_exp_dt = [
                            'name'  => 'course_expiry',
                            'id'    => 'course_expiry',
                            'class' => 'form-control',
                            'value' => $course_exp_date
                        ];
                        echo form_input($data_exp_dt);
                        ?>
                        <span class="input-group-addon">
                            <span class="flaticon-calendar-1"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="hr-line mt-4 mb-4"></div>

    <div class="col-md-12 p-0">
        <div class="control-label block mb-4 f16"> Glossary Display on this Page</div>
        <div class="pl-4 col-md-12">
            <div class="form-group">
                <label class="control-label block"> Inline Pop-ups on this Page </label>
                <div class="col-md-3 p-0">
                    <label class="radio-tel m-0">
                        <?php
                        if (isset($courseID) && $courseData->show_glossary == 1) {
                            $gloss_avail_status = 'checked';
                        } elseif (isset($courseID) && $courseData->show_glossary != 1) {
                            $gloss_avail_status = '';
                        } else {
                            $gloss_avail_status = 'checked';
                        }

                        $data_avai = [
                            'name'    => 'show_glossary',
                            'id'     => 'available',
                            'value'  => 1,
                            'checked' => $gloss_avail_status,
                            'class'  => 'mr-3 t2'
                        ];
                        echo form_radio($data_avai);
                        ?> Available
                    </label>
                </div>
                <div class="col-md-3 p-0">
                    <label class="radio-tel m-0">
                        <?php
                        if (isset($courseID) && $courseData->show_glossary == 0) {
                            $gloss_hide_status = 'checked';
                        } else {
                            $gloss_hide_status = '';
                        }
                        $data_hidden = [
                            'name' => 'show_glossary',
                            'id' => 'hidden',
                            'value' => 0,
                            'checked' => $gloss_hide_status,
                            'class' => 'mr-3 t2'
                        ];
                        echo form_radio($data_hidden);
                        ?> Hidden
                    </label>
                </div>
                <div class="col-md-3 p-0">
                    <label class="radio-tel m-0">
                        <?php
                        if (isset($courseID) && $courseData->show_glossary == 2) {
                            $gloss_once_status = 'checked';
                        } else {
                            $gloss_once_status = '';
                        }
                        $data_instance = [
                            'name' => 'show_glossary',
                            'id' => 'instance',
                            'value' => 2,
                            'checked' => $gloss_once_status,
                            'class' => 'mr-3 t2'
                        ];
                        echo form_radio($data_instance);
                        ?> Only for first instance
                    </label>
                </div>
            </div>
        </div>
        <div class="hr-line col-md-12 mt-4 mb-5"></div>
        <?php echo form_close(); ?>
        <div class="pl-4 col-md-12">
            <div class="col-md-6 pl-0 pt-5">
                <div class="panel panel-grey">
                    <div class="panel-heading mb-3">
                        <h2><span>Glossary Categories</span></h2>
                    </div>
                    <div class="panel-body panel-collapse-body" id="glossary_categories_section">
                    </div>
                </div>
            </div>
            <div class="col-md-6 pl-0 pt-5">
                <div class="panel panel-grey">
                    <div class="panel-heading">
                        <h2><span>Glossary Tags</span></h2>
                        <div class="panel-ctrls button-icon-bg">
                        </div>
                    </div>
                    <div class="panel-body" id="glossary_tags_section">

                    </div>
                </div>
            </div>
        </div>

        <div class="hr-line col-md-12"></div>

        <div class="pl-4 col-md-12">
            <div class="col-md-6 pl-0 pt-5">
                <div class="panel panel-grey">
                    <div class="panel-heading mb-3">
                        <h2><span>Course Categories</span></h2>
                    </div>
                    <div class="panel-body panel-collapse-body" id="course_categories_section">
                    </div>
                </div>
            </div>
            <div class="col-md-6 pl-0 pt-5">
                <div class="panel panel-grey">
                    <div class="panel-heading">
                        <h2><span>Course Tags</span></h2>
                        <div class="panel-ctrls button-icon-bg">
                        </div>
                    </div>
                    <div class="panel-body" id="course_tags_section">

                    </div>
                </div>
            </div>
        </div>

        <div class="hr-line col-md-12"></div>

        <div class="pl-4 col-md-12">
            <div class="col-md-6 pl-0 pt-5">
                <div class="panel panel-grey">
                    <div class="panel-heading mb-3">
                        <h2><span>System Categories</span></h2>
                    </div>
                    <div class="panel-body panel-collapse-body" id="system_categories_section">

                    </div>
                </div>
            </div>
            <div class="col-md-6 pl-0 pt-5">
                <div class="panel panel-grey">
                    <div class="panel-heading">
                        <h2><span>System Tags</span></h2>
                        <div class="panel-ctrls button-icon-bg">
                        </div>
                    </div>
                    <div class="panel-body" id="system_tags_section">

                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 mt-3 mb-3 p-0 pb-2"> <div class="hr-line mb-4"></div> </div>
        <div class="col-xs-12 text-right">
            <button class="btn btn-primary" type="button" id="nextMetadata"><?php echo isset($courseID) ? 'Save changes' : 'Save and next' ?></button>
        </div>

    </div>
    <!----------->
</div>
<!--//  End Metadate Tab  //-->

<script type="text/javascript" src="<?= bs('public/assets/plugins/form-jasnyupload/fileinput.min.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $('.delete_img_main_page').off('click').on('click', function() {

            var module_id = '<?php echo $module_id; ?>';
            var filename = $("#img_name").val();
            //var scope = $(this);

            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure?',
                buttons: {
                    confirm: function() {
                        //alert("true");
                        $("#img_name").val('');
                        $(".ist-logo").html('');
                        $(".ist-logo").append('<img src="<?php echo bs("public/assets/img/logo-bg.png"); ?>" id="image_write_path" class="img-rounded">');
                        //$("#image_write_path").attr('src', '<?php echo bs("/public/assets/img/logo-bg.png"); ?>');

                        //return true;
                    },
                    cancel: function() {
                        return true;
                    }
                }
            });
        });
        $('.ist-logo').html(CommanJS.getImagebox($('#image_write_path').attr('src'), 'thumbnail', 'f45 text-center mt-3 pt-5'));
        // Course expiry datepicker
        $("#course_expiry").datepicker({
            changeYear: true,
            changeMonth: true,
            numberOfMonths: 1,
            minDate: 0
        });

        // Set expiry date
        $('#exp_status').click(function() {
            if ($('#exp_status').prop("checked") == true) {
                $('.course_expiry_div').show();
            } else {
                $("#course_expiry").val('');
                $('.course_expiry_div').hide();
            }
        });

        // Submit metadata    
        $('#nextMetadata').click(function() {
            $("#courseMetadata").submit();
        });

        $("#courseMetadata").validate({
            ignore: [],
            rules: {
                course_expiry: {
                    required: function() {
                        if ($('#exp_status').is(':checked'))
                            return true;
                        else
                            return false;
                    },
                }
            },
            messages: {
                course_expiry: {
                    required: "Please set date for expiry of course"
                },
            },
            submitHandler: function(form) {
                $.ajax({
                    url: "<?php echo bs(); ?>courses/addCourseMetadata",
                    type: "POST",
                    dataType: "json",
                    data: $("#courseMetadata").serialize(),
                    success: function(data) {
                        if (data.code == 200) {
                            $("#coursePanelTab li").removeClass("active");
                            $("#tab-assessment").addClass('active');
                            $("#tab-assessment").attr('data-id', data.id);
                            getViewTemplate('assessment', data.id);
                            //getMetadata(data.id);                            
                            CommanJS.getDisplayMessgae(data.code, data.success);
                        } else if (data.code == 400) {
                            CommanJS.getDisplayMessgae(data.code, data.error);
                        }
                    }
                });
            }
        });
        return false;
    }); // document ready ends

    // function media_image_callback(image_name, img_full_url) {
    //     $('#image_write_path').attr('src', img_full_url);
    //     $('#img_name').val(image_name);
    // }

    // $("#choose_media_image_file").click(function() {
    //     CommanJS.choose_media_image_file('media_image_callback');
    // });

    function media_image_callback_metadata(image_name, img_full_url) {

        $('.ist-logo').html(CommanJS.getImagebox(img_full_url, 'thumbnail', 'f45 text-center mt-3 pt-5'));
        $('#img_name').val(image_name);
        $('.delete_img_main_page').css('display', 'inline-block');
        $('.delete_img_main_page').show();
        //alert(img_full_url)
    }

    $("#choose_media_image_file1").click(function() {
        CommanJS.choose_media_image_file('media_image_callback_metadata');
    });
</script>