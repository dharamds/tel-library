<link rel="stylesheet" href="<?php bs('public/assets/plugins/multiselect/sumoselect.min.css') ?>" />

<div class="container-fluid pr-0">
    <div class="panel panel-default">
        <div class="panel-heading">
            <ul class="nav nav-tabs" id="coursePanelTab">
                <li role="presentation" class="active change_tab" id="tab-content" data-tab-name="content">
                    <a href="javascript:void(0)">Content</a>
                </li>
                <li role="presentation" id="tab-metadata" class="change_tab" data-tab-name="metadata">
                   <a href="javascript:void(0)">Metadata</a>
                </li>
                <li role="presentation" id="tab-assessment" class="change_tab" data-tab-name="assessment">
                   <a href="javascript:void(0)">Assessment</a>
                </li>
                <li role="presentation" id="tab-builder" class="change_tab" data-tab-name="builder">
                   <a href="javascript:void(0)">Builder</a>
                </li>
                <li role="presentation" id="tab-gradebook" class="change_tab" data-tab-name="gradebook">
                   <a href="javascript:void(0)">Grade-book</a>
                </li>
            </ul>
        </div>
      <div class="panel-body" style="padding: 16px 20px;">
         <div class="tab-content">
            <!--// Start Content Tab  -->
            <div role="tabpanel" class="tab-pane active" id="course-tab-view">
               <div class="col-sm-12"><div class="page-loading-box"><span class="page-loader-quart"></span> Loading...</div></div>
            </div>
         </div>
      </div>
   </div>
</div>

<script src="http://malsup.github.com/jquery.form.js"></script>            
<script type="text/javascript">

    $(document).ready(function() { 
        var courseId = "<?php echo $courseID; ?>";
        //alert(courseId);
        getViewTemplate('content', courseId);

        $(".change_tab").on('click', function(){
            $("#coursePanelTab li").removeClass("active");
            $("#"+this.id).addClass('active');                             
            var tab = $("#"+this.id).attr('data-tab-name');
            //alert(tab);
            getViewTemplate(tab,courseId); 
            $("#tab-"+tab).attr('data-id', courseId);
            if(tab == 'metadata') 
                getMetadata(courseId); 
        });        
    }); //end $(document).ready


    
    // Load template
    function getViewTemplate(viewType, courseId) {
        $(".tab-pane").html('<div class="col-sm-12"><div class="page-loading-box"><span class="page-loader-quart"></span> Loading...</div></div>');
           $("#course-tab-view").load("<?php echo base_url() ?>courses/load_view/" + viewType + "/" + courseId, function () {});
      }

    // to get all system and default categories and save into table
    function getMetadata(courseID) {
        // Glossary Category  13
        CommanJS.getCatSection(<?=META_GLOSSARY;?>, 'glossary_categories_section', 'glossary_categories', courseID, <?=META_COURSE;?>);
        // Glossary tags 
        CommanJS.getTagSection(<?=META_GLOSSARY;?>, 'glossary_tags_section', 'glossary_tags', courseID, <?=META_COURSE;?>);
        // Course Category  4
        CommanJS.getCatSection(<?=META_COURSE;?>, 'course_categories_section', 'course_categories', courseID, <?=META_COURSE;?>);
        // Course tags  4
        CommanJS.getTagSection(<?=META_COURSE;?>, 'course_tags_section', 'course_tags', courseID, <?=META_COURSE;?>);
        // System Category  1
        CommanJS.getCatSection(<?=META_SYSTEM;?>, 'system_categories_section', 'system_categories', courseID, <?=META_COURSE;?>);
        // System tags  4
        CommanJS.getTagSection(<?=META_SYSTEM;?>, 'system_tags_section', 'system_tags', courseID, <?=META_COURSE;?>);
        // Learning Outcomes  1
        //CommanJS.getCatSection(14, 'learningoutcome_categories_section', 'learningoutcome_categories', courseID, 4);
        // Learning standards  15
        //CommanJS.getCatSection(15, 'learninstandards_categories_section', 'learninstandards_categories', courseID, 4);
        // Competencies  16
        //CommanJS.getCatSection(16, 'competencies_categories_section', 'competencies_categories', courseID, 4);
        // Skills  17
        //CommanJS.getCatSection(17, 'skills_categories_section', 'skills_categories', courseID, 4);
    }

   
    function checkCourseNumber(val) {
        var course_id = $('#course_content_id').val();
        //alert(val); 
        $.ajax({
            url: "<?php echo bs('courses/checkCourseNumber'); ?>",
            type: "POST",
            data: { 'id' : course_id, 'course_number' : val},
            success: function (data) {   
                if(data==1){
                    $("#course_number").select();
                    $("#duplicateCode").html("<div class='error'>"+val+" course number already exists.</div>");
                }else {                    
                    $("#duplicateCode").empty();
                }
            }
        });             
    }
       

    function checkContextId(val) {
        var course_id = $('#course_content_id').val();
        //alert(val); 
        $.ajax({
            url: "<?php bs('courses/checkContextId'); ?>",
            type: "POST",
            data: { 'id' : course_id, 'context_id' : val},
            success: function (data) {   
                if(data){
                    //$("#context_id").val('');
                    $("#context_id").select();
                    $("#duplicateContextId").html("<div class='error'>"+val+" course context id already associated with other courses.</div>");
                }else if(data==0) {
                    $("#duplicateContextId").empty();
                } 
            }
        });             
    }

    function checkResourceLinkId(val) {

        var course_id = $('#ass_course_id').val();
        var assessment_id = $('#assessment_id').val();      
        $.ajax({
            url: "<?php bs('courses/checkResourceLinkId'); ?>",
            type: "POST",
            data: { 'id' : assessment_id, 'course_id' : course_id, 'resource_link_id' : val},
            success: function (data) {   
                if(data==1){
                    //$("#context_id").val('');
                    $("#resource_link_id").focus();
                    $("#duplicateResourceLinkId").html("<div class='error'>"+val+" course Resource Link Id already associated with other courses.</div>");
                }else if(data==0) {
                    $("#duplicateResourceLinkId").empty();
                } 
            }
        });   

    }

    // Delete course document
    $(document).on('click', '.delete_doc', function() {

var filename = $(this).data('docname');
var filenameID = $(this).data('did');

var scope = $(this);
$.confirm({
    title: 'Confirm!',
    content: 'Are you sure?',
    buttons: {
        confirm: function() {
            $.ajax({
                url: "<?php echo bs(); ?>courses/remove_course_doc",
                type: "POST",
                data: {
                    'file': filename,
                    'fileID': filenameID
                },
                beforeSend: function() {
                    $("#docLoader").show();
                },
                success: function(data) {
                    scope.parent('div').remove()
                    CommanJS.getDisplayMessgae(200, 'Deleted successfully.');
                    $('#docLoader').hide();
                }
            });
            return true;
        },
        cancel: function() {
            return true;
        }
    }
});
});

    // Upload course documents
    $(document).on('change', '#courseDocs', function () {
        // check file extension
        var ext = $('#courseDocs').val().split('.').pop().toLowerCase();
        var updoctitle = $('#docTitle').val();
        updoctitle = $.trim(updoctitle);

        if (updoctitle == "" || $.inArray(ext, ['doc', 'docx', 'pdf', 'odt', 'xls', 'xlsx', 'ppt', 'pptx']) == -1) {
            $("#docValidFormat").show();
            $("#docValidFormat").html("<div class='error'>Please enter document title.</div>");
            return false;
        } else {
            $("#docValidFormat").hide();
            var file_data = $(this).prop("files")[0];
            var form_data = new FormData();
            form_data.append("file", file_data)
            form_data.append("elementId", $(this).attr('id'))
            form_data.append("elementType", $(this).data('type'))
            form_data.append("fileTitle", $('#docTitle').val())
            $.ajax({
                url: "<?php echo bs(); ?>courses/upload_docs_image",
                cache: false,
                contentType: false,
                processData: false,
                data: form_data, // Setting the data attribute of ajax with file_data
                type: 'post',
                beforeSend: function () {
                    $("#docLoader").show();
                },
                success: function (data) {

                    $("#docLoader").hide();
                    var obj = JSON.parse(data);
                    $("#docTitle").val('');
                    $("#courseDocs").val('');

                    var listItemCnt = '<span class="list-group-item"><h4 class="list-group-item-heading added_doc_cnt">' + obj.fileTitle + '</h4><a class="btn btn-danger delete_doc" data-docname ="' + obj.fileName + '"><i class="flaticon-trash"></i></a></span>';
                    $(".list-file-row").append(listItemCnt);

                    var temptitles = $("#dtitles").val();
                    if (temptitles)
                        temptitles = temptitles + '|' + obj.fileTitle;
                    else
                        temptitles = obj.fileTitle;

                    $("#dtitles").val(temptitles);

                    var tempfiles = $("#dfiles").val();
                    if (tempfiles)
                        tempfiles = tempfiles + '|' + obj.fileName;
                    else
                        tempfiles = obj.fileName;

                    $("#dfiles").val(tempfiles);
                    var up_doc_cnt = parseInt($("#docCnt").text()) + 1;
                    $("#docCnt").text(up_doc_cnt);
                }
            });
        }
    });
    
    // Upload course image
    $('#uploadImage').on('change', function () {
        var ext = $('#uploadImage').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['jpg', 'png', 'jpeg']) == -1) {
            $("#outputImage").show();
            $("#outputImage").html("<div class='error'>Please select valid image.</div>");
            return false;
        } else {
            $('#course_img_up').ajaxForm({
                url: "<?php echo bs(); ?>courses/upload_docs_image",
                beforeSubmit: function (data) {
                    $("#outputImage").hide();
                    if ($("#uploadImage").val() == "") {
                        $("#outputImage").show();
                        $("#outputImage").html("<div class='error'>Choose a file to upload.</div>");
                        return false;
                    }
                    $("#progressDivId").css("display", "block");
                    var percentValue = '0%';
                    $('#progressBar').width(percentValue);
                    $('#percent').html(percentValue);
                },
                uploadProgress: function (event, position, total, percentComplete)
                {
                    var percentValue = percentComplete + '%';
                    $("#progressBar").animate({
                        width: '' + percentValue + ''
                    },
                    {
                        duration: 5,
                        easing: "linear",
                        step: function (x) {
                            percentText = Math.round(x * 100 / percentComplete);
                            $("#percent").text(percentText + "%");
                            if (percentText == "100") {
                                $('#progressDivId').hide();
                                $('#uploadImage').val('');
                            }
                        }
                    });
                },
                error: function (response, status, e) {
                    alert('Oops something went.');
                },
                complete: function (xhr) {
                    if (xhr.responseText && xhr.responseText != "error") {
                        $("#outputImage").html(xhr.responseText);
                    } else {
                        $("#outputImage").show();
                        $("#outputImage").html("<div class='error'>Problem in uploading file.</div>");
                        $("#progressBar").stop();
                    }
                },
                success: function (data) {
                    var jsn = JSON.parse(data);
                    if (jsn.error) {
                        $('#upload_error').html(jsn.error);
                    } else {
                        var url = jsn.filePath + jsn.fileName;
                        $('#up_course_img').attr('src', url);
                        $('#uploadedCourseImg').val(jsn.fileName);
                        var percentValue = '0%';
                        $("#progressBar").animate({
                            'width': percentValue
                        }, {
                            step: function (x) {
                                $("#percent").text("0%");
                            }
                        });
                        CommanJS.getDisplayMessgae(200, 'Course image uploaded successfully.');
                    }
                }
            }).submit();
        }
    });
</script>
