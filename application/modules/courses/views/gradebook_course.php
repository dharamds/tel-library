<link type="text/css" href="<?= bs('public/assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css')?>" rel="stylesheet"> 
<!-- tabpanel -->
<div role="tabpanel" class="tab-pane" id="gradebook">
	
	<div class="row">
		<div class="col-sm-12 p-0 themePanel">
			<div class="col-sm-12 icon-left pt-3 pb-4">
				<h4 class="text-dark p-0 block-heading">
				<i class="icon flaticon-book mr-3"></i> Created gradebook for course : <?php echo $courseData->name; ?></h4>
			</div>
			<div class="col-sm-12"> <div class="hr-line mb-4"></div> </div>
		</div>  
	</div>

	
	<div class="row">
		<h4  class="text-dark p-0 block-heading pl-4 mt-2">Gradebook sections</h4>
		<div class="col-md-12 pt-4">
			<div class="panel-group" id="accordion">
				<?php 
					$i=1; $c=0;
					foreach ($sectionType as $key => $value) {  ?>

						<?php 
					        echo form_open('', [ 
					            'name'      => 'courseGradebook'.$i,
					            'id'        => 'courseGradebook'.$i,
					            'enctype'   => 'multipart/form-data']);
					    ?>

					    <?php
					        $hgbcid_data = [ 'type' => 'hidden',
					                        'id' 	=> 'course_gb_id',
					                        'name' 	=> 'course_gb_id',
					                        'value' => $courseID ];
					        echo form_input($hgbcid_data);
					    ?>
					    <?php
					        $hsec_data = [ 'type' 	=> 'hidden',
					                        'id' 	=> 'section_cnt',
					                        'name' 	=> 'section_cnt',
					                        'value' => $i ];
					        echo form_input($hsec_data);
					    ?>
					    <?php
					        $hsec_type_data = [ 'type' 	=> 'hidden',
						                        'id' 	=> 'section_type'.$i,
						                        'name' 	=> 'section_type'.$i,
						                        'value' => $key ];
					        echo form_input($hsec_type_data);
					    ?>
					    <?php
					    	$sectionId = isset($gradebookData[$c]->id) ? $gradebookData[$c]->id : '';
					        $h_gb_sec_id_data = [ 'type' 	=> 'hidden',
						                        'id' 	=> 'gb_section_id'.$i,
						                        'name' 	=> 'gb_section_id'.$i,
						                        'value' =>  $sectionId ];
					        echo form_input($h_gb_sec_id_data);
					    ?>
						<div class="panel panel-default panel-collapse">
							<div class="panel-heading">
								<h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$i;?>">Section : <?=$value;?> </a></h4>
							</div>

							<div id="collapse<?=$i;?>" class="panel-collapse collapse in">
								
								<div class="row mt-4 mb-3">
							        <div class="col-sm-6">
							            <div class="form-group">
							                <label for="title" class="control-label">Section Title <span class="field-required">*</span> </label>
							                <?php	
							                	$sectionTitle = isset($gradebookData[$c]->section_title) ? $gradebookData[$c]->section_title : '';	
								                echo form_input([
								                    'id'    => 'title'.$i,
								                    'name'  => 'title'.$i,     
								                    'class' => 'form-control',
								                    'value' => $sectionTitle
								                ]);
							                ?>
							            </div>
							        </div>
							        <div class="col-sm-6"> 
							        	<div class="form-group">
							                <label for="Weight" class="control-label">Weight <span class="field-required">*</span> </label>
							                <div class="flex-col-5 p-0 m-0 ml-4">
							                <?php 
							                	$sectionWeight = isset($gradebookData[$c]->weight) ? $gradebookData[$c]->weight : 0;
						                        $txt_weight = [ 'name'  => 'weight'.$i,
						                                        'id'    => 'weight'.$i,
						                                        'value' => $sectionWeight,
						                                        'class' => 'touchspin4'
						                                  ];
						                        echo form_input($txt_weight); 
						                    ?>
						                    </div>
							            </div> 
							        </div>
							    </div>

							    <div class="row mb-3">
							        <div class="col-sm-10">
							            <div class="form-group">
							                <label for="description" class="control-label">Section description <span class="field-required">*</span> </label>
							                <div>
						                    <?php
						                    	$sectionDescription = isset($gradebookData[$c]->section_description) ? $gradebookData[$c]->section_description : '';
							                    echo form_textarea([
							                        'name'  => 'description'.$i,
							                        'id'    => 'description'.$i,		
							                        'class' => 'form-control editor',
							                        'rows'  => '5',
							                        'value' => $sectionDescription
							                    ]);
						                    ?>
							                </div>
							            </div>
							        </div>
							    </div>

							    <div class="col-xs-12 text-right p-0 mt-4">
						        	<button type="button" id="saveGradebook<?=$i;?>" data-form-id="<?=$i;?>" class="btn btn-primary btnSubmit">Save</button>
						        </div>

						        <?php if(count($gradebooks[$i]) > 0 ) { ?>

							        <div class="col-xs-12 mt-3 mb-3 p-0 pb-2"> <div class="mb-2"></div> </div>
								    
							        <div class="row m-0">
							            <div class="col-md-12">
							                <table id="courseAssessmentList" class="table table-bordered table-striped table-hover" cellspacing="0">
						                    <thead>
						                        <tr>                            
						                            <th style="min-width: 20px;">#</th>
						                            <th style="min-width: 100px;">Title</th>
						                            <th style="min-width: 100px;">Long title</th>
						                        </tr>
						                    </thead>
						                    <tbody>
						                    <?php $srno=1;
						                    	foreach($gradebooks[$i] as $key => $gradebook) { ?>
							                    	<tr>
							                    		<td><?=$srno;?></td>
							                    		<td><?php echo $gradebook->title;?></td>
							                    		<td><?php echo $gradebook->long_title;?></td>
							                    	</tr>
						                    	<?php $srno++;
						                		} //end foreach ?>
						                    </tbody>
							                </table>
							            </div>
							        </div>								    
								<?php } //end if(count($gradebooks[$i]) > 0 )?>
						        <?php echo form_close(); ?>
							</div>		
						</div>
					<?php 
					$i++; $c++;
				} ?>
			</div> 
		</div>		
	</div>
		

</div>
<!-- tabpanel ends -->
<script type="text/javascript" src="<?= bs('public/assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js') ?>"></script>   
<script type="text/javascript">
	$(document).ready(function() {

		var title = 'title'; var description = 'description'; var frmName = 'courseGradebook';
        // Ck editor
        var config = { height: 120, allowedContent :true, toolbar: 'short' };
        $('.editor').each(function(e) {
            CKEDITOR.replace(this.id, config);
        });

        //Touchspin for weight
	    $("input.touchspin4").TouchSpin({
	      verticalbuttons: true,
	      step: 5,
	      min : 0,
	      max : 100
	    });

	    $(".btnSubmit").click( function () {
	    	var id = $(this).attr('data-form-id');
	    	var title = title+id;
    		var description = description+id;
	    	var frmName = frmName+id;  	    		
		    $("#courseGradebook"+id).submit();
	    });

	    // CYK gradebook
	    $("#courseGradebook1").validate({
        	ignore: [],
            rules: { 
            	title1: { required: true },
                weight1: { required: true, min: 1 },
                description1: {
                    required: function(textarea) {
                        CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                        return editorcontent.length === 0;
                    },
                }
            },
            messages: {
                title1: { required: "Please enter section title" },  
                weight1: { required: "Please select section weight" },              
                description1: { required: "Please enter section description" }               
            },
            submitHandler: function(form) {
                $.ajax({
                    url: "<?php echo bs();?>courses/addCourseGradebook",
                    type: "POST",
                    dataType: 'json',
                    data: $("#courseGradebook1").serialize(),
                    success: function(data) {
                        console.log(data);
                        if(data.code==200) {
                        	var scnt = data.sec_cnt;
                        	$("#gb_section_id"+scnt).val(data.id);               	
                        	CommanJS.getDisplayMessgae(data.code, data.success);
			            }else if(data.code == 400) {
			              	CommanJS.getDisplayMessgae(data.code, data.error);
			          	}
                    }
                });
            },
            errorPlacement: function(error, $elem) {
                console.log(error);
                if ($elem.is('textarea')) {
                    $elem.insertAfter($elem.next('div'));
                }
                error.insertAfter($elem);
            },
        });

	    // Moduel gradebook
        $("#courseGradebook2").validate({
        	ignore: [],
            rules: {
            	title2: { required: true },
                weight2: { required: true, min: 1 },
                description2: {
                    required: function(textarea) {
                        CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                        return editorcontent.length === 0;
                    },
                }
            },
            messages: {
                title2: { required: "Please enter section title" },  
                weight2: { required: "Please select section weight" },              
                description2: { required: "Please enter section description" }               
            },
            submitHandler: function(form) {
                $.ajax({
                    url: "<?php echo bs();?>courses/addCourseGradebook",
                    type: "POST",
                    dataType: 'json',
                    data: $("#courseGradebook2").serialize(),
                    success: function(data) {
                        console.log(data);
                        if(data.code==200) {
                        	var scnt = data.sec_cnt;
                        	$("#gb_section_id"+scnt).val(data.id);               	
                        	CommanJS.getDisplayMessgae(data.code, data.success);
			            }else if(data.code == 400) {
			              	CommanJS.getDisplayMessgae(data.code, data.error);
			          	}
                    }
                });
            },
            errorPlacement: function(error, $elem) {
                console.log(error);
                if ($elem.is('textarea')) {
                    $elem.insertAfter($elem.next('div'));
                }
                error.insertAfter($elem);
            },
        });

        // Evidence gradebook
        $("#courseGradebook3").validate({
        	ignore: [],
            rules: {
            	title3: { required: true },
                weight3: { required: true, min: 1 },
                description3: {
                    required: function(textarea) {
                        CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                        return editorcontent.length === 0;
                    },
                }
            },
            messages: {
                title3: { required: "Please enter section title" },  
                weight3: { required: "Please select section weight" },              
                description3: { required: "Please enter section description" }               
            },
            submitHandler: function(form) {
                $.ajax({
                    url: "<?php echo bs();?>courses/addCourseGradebook",
                    type: "POST",
                    dataType: 'json',
                    data: $("#courseGradebook3").serialize(),
                    success: function(data) {
                        console.log(data);
                        if(data.code==200) {
                        	var scnt = data.sec_cnt;
                        	$("#gb_section_id"+scnt).val(data.id);               	
                        	CommanJS.getDisplayMessgae(data.code, data.success);
			            }else if(data.code == 400) {
			              	CommanJS.getDisplayMessgae(data.code, data.error);
			          	}
                    }
                });
            },
            errorPlacement: function(error, $elem) {
                console.log(error);
                if ($elem.is('textarea')) {
                    $elem.insertAfter($elem.next('div'));
                }
                error.insertAfter($elem);
            },
        });

        // Mid-term gradebook
        $("#courseGradebook4").validate({
        	ignore: [],
            rules: {
            	title4: { required: true },
                weight4: { required: true, min: 1 },
                description4: {
                    required: function(textarea) {
                        CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                        return editorcontent.length === 0;
                    },
                }
            },
            messages: {
                title4: { required: "Please enter section title" },  
                weight4: { required: "Please select section weight" },              
                description4: { required: "Please enter section description" }               
            },
            submitHandler: function(form) {
                $.ajax({
                    url: "<?php echo bs();?>courses/addCourseGradebook",
                    type: "POST",
                    dataType: 'json',
                    data: $("#courseGradebook4").serialize(),
                    success: function(data) {
                        console.log(data);
                        if(data.code==200) {
                        	var scnt = data.sec_cnt;
                        	$("#gb_section_id"+scnt).val(data.id);               	
                        	CommanJS.getDisplayMessgae(data.code, data.success);
			            }else if(data.code == 400) {
			              	CommanJS.getDisplayMessgae(data.code, data.error);
			          	}
                    }
                });
            },
            errorPlacement: function(error, $elem) {
                console.log(error);
                if ($elem.is('textarea')) {
                    $elem.insertAfter($elem.next('div'));
                }
                error.insertAfter($elem);
            },
        });

        // final gradebook
        $("#courseGradebook5").validate({
        	ignore: [],
            rules: {
            	title5: { required: true },
                weight5: { required: true, min: 1 },
                description5: {
                    required: function(textarea) {
                        CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                        return editorcontent.length === 0;
                    },
                }
            },
            messages: {
                title5: { required: "Please enter section title" },  
                weight5: { required: "Please select section weight" },              
                description5: { required: "Please enter section description" }               
            },
            submitHandler: function(form) {
                $.ajax({
                    url: "<?php echo bs();?>courses/addCourseGradebook",
                    type: "POST",
                    dataType: 'json',
                    data: $("#courseGradebook5").serialize(),
                    success: function(data) {
                        console.log(data);
                        if(data.code==200) {
                        	var scnt = data.sec_cnt;
                        	$("#gb_section_id"+scnt).val(data.id);               	
                        	CommanJS.getDisplayMessgae(data.code, data.success);
			            }else if(data.code == 400) {
			              	CommanJS.getDisplayMessgae(data.code, data.error);
			          	}
                    }
                });
            },
            errorPlacement: function(error, $elem) {
                console.log(error);
                if ($elem.is('textarea')) {
                    $elem.insertAfter($elem.next('div'));
                }
                error.insertAfter($elem);
            },
        });      
	    
    });// document ready ends
</script>
