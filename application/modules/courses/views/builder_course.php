<link rel="stylesheet" href="<?php bs('public/assets/css/drag.drop.theme.css') ?>" />  
<script type="text/javascript" src="<?php bs('public/assets/js/stickybox.js') ?>"></script>
<!--// Start Builder Tab  //-->                     
<div role="tabpanel" class="tab-pane" id="builder">
    <?php //pr($modulesData);
        echo form_open('courses/add/', [ 
            'name'      => 'courseBuilder',
            'id'        => 'courseBuilder',
            'enctype'   => 'multipart/form-data']);
    ?>                       
    <?php
        $hbcid_data = [ 'type' => 'hidden',
                        'id' => 'course_builder_id',
                        'name' => 'course_builder_id',
                        'value' => $courseID ];
        echo form_input($hbcid_data);
    ?>
    <div class="pl-4 col-md-12 pb-4">
        <div class="col-md-3 pl-0 pt-5">
            <div class="" id="system_categories_bulider"></div>
        </div>
        <div class="col-md-3 pl-0 pt-5">         
            <div id="system_tags_bulider"></div>
        </div>

        <!-- <div class="col-md-3 pl-0 pt-5">         
            <div class="flex-col p-0" id="selected_assessment_cat"></div>
        </div>

        <div class="col-md-3 pl-0 pt-5">         
            <div class="flex-col p-0" id="selected_assessment_tag"></div>
        </div>  -->       
    </div>
    <div class="pl-0 col-md-12 pb-5">
        <div class="col-md-12 pl-0 text-right">
            <div id="builderOptionLoading">
                <div class="page-loading-box">
                    <span class="page-loader-quart"></span> Loading...
                </div>
            </div>
            <button class="btn btn-md btn-primary" type="button" id="searchBtnBuilder">Search</button>

            <button class="btn btn-md btn-danger" type="reset" id="reset_form">Reset</button>
        </div>
    </div>

    <div class="hr-line col-md-12"></div>

    <div class="row">
        <div class="col-md-6 pt-5 draggable-area-section" style="min-height: 450px;">
            <div class="panel-links">
                <a href="javascript:void(0)">Expand all</a>
                <a href="javascript:void(0)">Collapse all</a> 
            </div>
            <!--//  Module Listing Panel  //-->
            <div class="panel panel-collapse">
                <div class="panel-header">
                    <h4>Modules</h4>
                    <div class="right">
                      
                        <button class="btn-icons active" data-collapse-trigger="#moduleContainer" type="button">
                            <span><i class="ti ti-plus"></i></span>
                        </button>
                    </div>
                </div>
                <div class="panel-body panel-collapse-container" id="moduleContainer">
                    <div class="panel-search-area">
                        <input type="text" class="form-control" id="txtSearchModule" placeholder="Search" onkeyup="searchModules()">
                    </div>
                    <div class="panel-list-area panel-module-theme">
                        <ul class="connectedSortable" id="modulesList"  >
                            <img id="docLoader" src="<?php echo base_url(); ?>/public/assets/img/spinner.gif" style="display:none;"/>
                        </ul>
                    </div>
                </div>
            </div>

            <!--//  Lessons Listing Panel  //-->
            <div class="panel panel-collapse">
                <div class="panel-header">
                    <h4>Lessons</h4>
                    <div class="right">
                       
                        <button class="btn-icons active" data-collapse-trigger="#lessonContainer" type="button">
                            <span><i class="ti ti-plus"></i></span>
                        </button>
                    </div>
                </div>
                <div class="panel-body panel-collapse-container" id="lessonContainer">
                    <div class="panel-search-area">
                        <input type="text" class="form-control" id="txtSearchLesson" placeholder="Search" onkeyup="searchLessons()">
                    </div>
                    <div class="panel-list-area panel-lessons-theme">
                        <ul class="connectedSortable"  id="lessonsList">
                        </ul>
                    </div>
                </div>
            </div>

            <!--//  Quizzes Listing Panel  //-->
            <div class="panel panel-collapse">
                <div class="panel-header">
                    <h4>Quizzes</h4>
                    <div class="right">                       
                        <button class="btn-icons active" data-collapse-trigger="#quizzesContainer" type="button">
                            <span><i class="ti ti-plus"></i></span>
                        </button>
                    </div>
                </div>
                <div class="panel-body panel-collapse-container" id="quizzesContainer">
                    <div class="panel-search-area">
                        <input type="text" class="form-control" id="txtSearchQuiz" placeholder="Search" onkeyup="searchQuizzes()">
                    </div>
                    <div class="panel-list-area panel-quiz-theme">
                        <ul class="connectedSortable" id="quizzesList">
                            <!-- <li class="ui-state-default li-term" data-list-id="t_1">Mid-Term</li>
                            <li class="ui-state-default li-term" data-list-id="t_2">Final</li> -->
                        </ul>
                    </div>
                </div>
            </div>

            <!-- Assessments Listing Panel  -->
            <div class="panel panel-collapse">
                <div class="panel-header">
                    <h4>Assessments</h4>
                    <div class="right">
                        <button class="btn-icons active" data-collapse-trigger="#assessmentsContainer" type="button">
                            <span><i class="ti ti-plus"></i></span>
                        </button>
                    </div>
                </div>
                <div class="panel-body panel-collapse-container" id="assessmentsContainer">
                    <div class="panel-search-area">
                        <input type="text" class="form-control" id="txtSearchAssessment" placeholder="Search" onkeyup="searchAssessments()">
                    </div>
                    <div class="panel-list-area panel-assessment-theme">
                        <ul class="connectedSortable" id="assessmentList">
                            <?php if(isset($courseAssessments)) { ?>
                                <?php foreach($courseAssessments as $caKey => $assessmentVal) { ?>
                                    <?php $strike = ($assessmentVal['strike']==1) ? 'ui-throw-out' : ''; ?>
                                    <li class="li-assessment ui-state-default <?php echo $strike; ?>" data-list-id="a_<?php echo $assessmentVal['id']?>"><?php echo $assessmentVal['title'];?></li>   
                                <?php } ?>
                            <?php } ?>
                            <!-- <li class="li-assessment ui-state-default" data-list-id="a_1">Assessment 1</li>
                            <li class="li-assessment ui-state-default" data-list-id="a_2">Assessment 2</li> -->
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6 pt-5" id="startScrollSection">
            <div class="parentRelativeBlock">
                <div id="insetAbsolutSection">
                    <div class="panel-links">
                        <span>Items added</span>
                        <a href="javascript:void(0)">Expand all</a>
                        <a href="javascript:void(0)">Collapse all</a>
                    </div>

                    <!--//  Module Listing Panel  //-->
                    <div class="panel panel-collapse">
                        <div class="panel-body panel-collapse-container panel-draggable" id="moduleContainerDataList">
                            <div class="panel-list-area panel-module-theme">
                                <ul class="connectedSortable" id="listOfAllSection">
                                <!-- Module -->  <!------------------------>
                                <?php if(isset($selectedModules)) { ?>
                                    <?php foreach($selectedModules as $modKey => $selModule) { ?>
                                        <li id="ui-list-modules-<?php echo $selModule->id;?>" class="ui-custom-collapse module-li" data-container-id="m_<?php echo $selModule->id;?>">
                                            <h3>
                                                <span class="move"><?php echo $selModule->name;?></span>
                                                <span class="collapse-trigger"></span>
                                                <a class="m-dismiss ui-button" data-enable-id="m_<?php echo $selModule->id;?>"></a>
                                            </h3>   
                                            <!-- Lesson -->
                                            <?php if(isset($selectedLesson)) { ?>
                                            
                                            <div class="panel-list-area panel-lessons-theme collapse-panel">
                                                <ul class="inset-container lessonsInsetContainer ui-sortable" id="listOfModulesLessonm_<?php echo $selModule->id;?>">
                                                <?php foreach($selectedLesson as $lessonKey => $selLesson) { ?>
                                                    <?php if($selModule->id==$selLesson->module_id) { ?>
                                                    <li id="ui-list-lessons-<?php echo $selLesson->id;?>" class="ui-custom-collapse li-ml" data-lesson-id="l_<?php echo $selLesson->id;?>">
                                                        <h3>
                                                            <span class="move"><?php echo $selLesson->name;?></span>
                                                            <span class="collapse-trigger"></span>
                                                            <a class="l-dismiss ui-button" data-enable-id="l_<?php echo $selLesson->id;?>" ></a>
                                                        </h3>  
                                                        <!-- Quiz -->
                                                        <?php if(isset($selectedQuizzes)) { ?>
                                                            <div class="panel-list-area panel-quiz-theme collapse-panel">
                                                                <ul class="inset-container-quiz quizInsetContainer ui-sortable" id="listOfLessonsQuizl_<?php echo $selLesson->id;?>" data-qzid="l_<?php echo $selLesson->id;?>">

                                                            <?php foreach($selectedQuizzes as $quizKey => $selQuiz ) { ?>
                                                                <?php if($selQuiz->lesson_id==$selLesson->id) { ?>  
                                                                    <li id="ui-list-quiz-<?php echo $selQuiz->id;?>" class="ui-custom-collapse  lesson-li" data-quiz-id="q_<?php echo $selQuiz->id;?>">
                                                                    <h3>
                                                                        <span class="move"><?php echo $selQuiz->title;?></span>
                                                                        <a class="q-dismiss ui-button" data-enable-id="q_<?php echo $selQuiz->id;?>" data-quizid="<?php echo $selQuiz->id;?>"></a>
                                                                    </h3>
                                                                    </li>    
                                                                <?php } //end if($selQuiz->lesson_id==$selLesson->id) ?>
                                                            <?php } ?>
                                                            <!-- Asseesments -->
                                                            <?php foreach($selectedLessonAssessments as $assKey => $selAss) { ?>
                                                                <?php if($selAss->lesson_id==$selLesson->id) { ?>
                                                                    <li id="ui-list-quiz-a_<?php echo $selAss->id;?>" class="ui-custom-collapse assessment-li" data-assessment-id="a_<?php echo $selAss->id;?>">
                                                                        <h3>
                                                                            <span class="move"><?php echo $selAss->title;?></span>
                                                                            <a class="a-dismiss ui-button" data-enable-id="a_<?php echo $selAss->id;?>"></a>
                                                                        </h3>
                                                                    </li>
                                                                <?php } ?>
                                                            <?php } ?>
                                                            </ul>
                                                            <div class="panel-draggable-area for-quiz dragAreaForQuiz ui-droppable">Drop Lesson Quizzes/Assessments Here</div>
                                                            </div>
                                                        <?php } // end if(isset($selectedQuizzes)) ?>
                                                        </li>                                   
                                                    <?php } // end if($selModule->id==$selLesson->module_id) ?>
                                                <?php } // end selectedLesson ?>
                                                </ul>
                                                
                                                <ul class="inset-container moduleQuizInsetContainer ui-sortable" id="listOfModulesQuizm_<?php echo $selModule->id;?>">
                                                    <?php if(isset($selectedModuleQuizzes) || isset($selectedModuleAssessments) ) { ?>
                                                        <!-- Module quizzes -->
                                                        <?php foreach($selectedModuleQuizzes as $mquizkey => $selMQuiz) {?>
                                                            <?php if($selMQuiz->module_id == $selModule->id) {?>
                                                                <li id="ui-list-mquiz-<?php echo $selMQuiz->id;?>" class="ui-custom-collapse moduleQuiz-li li-mq" data-mquiz-id="q_<?php echo $selMQuiz->id;?>">
                                                                    <h3>
                                                                        <span class="move"><?php echo $selMQuiz->title;?></span>
                                                                        <a class="mq-dismiss ui-button" data-enable-id="q_<?php echo $selMQuiz->id;?>"></a>
                                                                    </h3>
                                                                </li>
                                                            <?php } ?>
                                                        <?php } //end selectedModuleQuizzes ?>

                                                        <!-- Moduel assessments -->
                                                        <?php foreach($selectedModuleAssessments as $massesskey => $selMAsses) {?>
                                                            <?php if($selMAsses->module_id == $selModule->id) {?>
                                                                <li id="ui-list-mquiz-<?php echo $selMAsses->id;?>" class="ui-custom-collapse moduleQuiz-li li-ma" data-mquiz-id="a_<?php echo $selMQuiz->id;?>">
                                                                    <h3>
                                                                        <span class="move"><?php echo $selMAsses->title;?></span>
                                                                        <a class="ma-dismiss ui-button" data-enable-id="a_<?php echo $selMAsses->id;?>"></a>
                                                                    </h3>
                                                                </li>
                                                            <?php } ?>
                                                        <?php } //end selectedModuleQuizzes ?>
                                                    <?php } //end if(isset($selectedModuleQuizzes) || isset($selectedModuleAssessments) ) ?>
                                                </ul>
                                                <div class="panel-draggable-area for-lessons dragAreaForLessons ui-droppable">
                                                    Drop Lessons Here
                                                </div>
                                                <div class="panel-draggable-area for-module-quiz dragAreaForModuleQuiz">
                                                    Drop Module Quizzes/Assessments Here
                                                </div>
                                            </div>                                    
                                         <?php } //end if(isset($selectedLesson))?>
                                        </li>
                                    <?php } // end selectedModules ?>
                                <?php } //end if(isset($selectedModules))?>
                                <!------------------------->
                                </ul>
                                <div class="panel-draggable-area for-module dragAreaForModule">Drag Module Here</div>
                                <ul id="listOfGlobalQuizze" class="ui-global-quiz-panel">
                                    <?php if(isset($selectedCourseQuizes)) { ?>
                                        <?php foreach($selectedCourseQuizes as $selCrsQuiz) { ?>
                                            <li id="ui-list-global-quize-q_<?php echo $selCrsQuiz->id;?>" class="ui-custom-collapse g-quiz-li" data-container-id="q_<?php echo $selCrsQuiz->id;?>" style=""><h3><span class="move"><?php echo $selCrsQuiz->title;?></span><a class="gq-dismiss ui-button" data-enable-id="q_<?php echo $selCrsQuiz->id;?>"></a></h3></li>
                                        <?php } ?>
                                    <?php } ?>

                                    <?php if(isset($selectedCourseAssessments)) { ?>
                                        <?php foreach($selectedCourseAssessments as $selCrsAssessment) { ?>
                                            <li id="ui-list-global-quize-a_<?php echo $selCrsAssessment->id;?>" class="ui-custom-collapse g-assessment-li" data-container-id="a_<?php echo $selCrsAssessment->id;?>" style=""><h3><span class="move"><?php echo $selCrsAssessment->title;?></span><a class="ga-dismiss ui-button" data-enable-id="a_<?php echo $selCrsAssessment->id;?>"></a></h3></li>
                                        <?php } ?>
                                    <?php } ?>
                                </ul>
                                <div class="panel-draggable-area for-global-quizee dragAreaForGlobalQuizze">Drag Course Quizzes/Assessments Here</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row mt-5" style="margin-top: 300px !important;">
        <div class="col-xs-12 text-right">
            <button type="button" id="nextBuilder" class="btn btn-primary">Next</button>
        </div>
    </div>
    <div id="stopScrollSection"></div>
<?php echo form_close(); ?>                
</div>
<!--//  End Builder Tab  //-->

<script type="text/javascript">
    
    $(document).ready(function () {
        $('#reset_form').click(function() {
            $(".meta_data_filter").prop("checked",false);
            $(".selectFilterMode span").text('');    
            $(".li-modules").remove();
            $(".li-lessons").remove();        
            $(".li-quizzes").remove();
            table.search('').draw();
        });
    });

    $('#nextBuilder').on('click', function () { 
        var course_id = $("#course_builder_id").val();
        $("#coursePanelTab li").removeClass("active");
        $("#tab-gradebook").addClass('active');
        $("#tab-gradebook").attr('data-id', course_id);
        getViewTemplate('gradebook', course_id);
    });

    // get system category and tags for course builder tab
    CommanJS.get_metadata_options("System category", 1, <?=META_SYSTEM;?>, "system_categories_bulider");
    CommanJS.get_metadata_options("System tag", 2, <?=META_SYSTEM;?>, "system_tags_bulider");
    CommanJS.get_metadata_options("Assessment category", 1, 9, "selected_assessment_cat");
    CommanJS.get_metadata_options("Assessment tag", 2, 9, "selected_assessment_tag");

    // Search modules in course builder
    function searchModules() {
        var txtSearchModule, filter, ul, li, a, i, txtValue;
        txtSearchModule = $("#txtSearchModule").val();
        //console.log(txtSearchModule);
        filter = txtSearchModule.toUpperCase();
        //console.log(filter);
        ul = document.getElementById("modulesList");
        li = ul.getElementsByTagName("li");
        for (i = 0; i < li.length; i++) {            
            txtValue = li[i].textContent || li[i].innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
            } else {
                li[i].style.display = "none";
            }
        }
    }

    // Search lesson in course builder
    function searchLessons() {
        var txtSearchLesson, filter, ul, li, a, i, txtValue;
        txtSearchLesson = $("#txtSearchLesson").val();
        //console.log(txtSearchModule);
        filter = txtSearchLesson.toUpperCase();
        //console.log(filter);
        ul = document.getElementById("lessonsList");
        li = ul.getElementsByTagName("li");
        for (i = 0; i < li.length; i++) {            
            txtValue = li[i].textContent || li[i].innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
            } else {
                li[i].style.display = "none";
            }
        }
    }

    // Search quizzes in course builder
    function searchQuizzes() {
        var txtSearchQuiz, filter, ul, li, a, i, txtValue;
        txtSearchQuiz = $("#txtSearchQuiz").val();
        filter = txtSearchQuiz.toUpperCase();
        //console.log(filter);
        ul = document.getElementById("quizzesList");
        li = ul.getElementsByTagName("li");
        for (i = 0; i < li.length; i++) {            
            txtValue = li[i].textContent || li[i].innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
            } else {
                li[i].style.display = "none";
            }
        }
    }  

    // Search assessments in course builder
    function searchAssessments() {
         var txtSearchAssessment, filter, ul, li, a, i, txtValue;
        txtSearchQuiz = $("#txtSearchAssessment").val();
        filter = txtSearchQuiz.toUpperCase();
        //console.log(filter);
        ul = document.getElementById("assessmentList");
        li = ul.getElementsByTagName("li");
        for (i = 0; i < li.length; i++) {            
            txtValue = li[i].textContent || li[i].innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
            } else {
                li[i].style.display = "none";
            }
        }
    } 

    // to search the module lesson and quizzes
    $('#searchBtnBuilder').click(function () {
        $("#courseBuilder").submit();
        $("#builderOptionLoading").show()
    });

    // Course bulder validation
    $("#courseBuilder").validate({
        ignore: [],
        rules: {
           /* System_category: {
                selectCatcheck: function(element) {
                    return ($('#selected_system_tag').val() == 0)
                }
            },
            System_tag: {
                selectTagcheck: function(element) {
                    return ($('#selected_system_category').val() == 0)
                }  
            }*/               
        },
        messages: {
            System_category: {required: "Please select system category"},
            System_tag: {required: "Please select system tag"},
        },
        submitHandler: function (form) {
            var selected_system_categories  = CommanJS.getMetaCallBack('system_category');
            var selected_system_tag         = CommanJS.getMetaCallBack('system_tag');
            var course_builder_id = $('#course_builder_id').val();            
            $.ajax({
                url: "<?php echo bs('courses/getModulesLessonsQuiz'); ?>",
                type: "POST",
                data:{ "course_builder_id" : course_builder_id, "selected_system_categories" : selected_system_categories, "selected_system_tag" : selected_system_tag},
                success: function (data) 
                {
                    $("#builderOptionLoading").hide()
                    //console.log(data);     
                    var obj = JSON.parse(data);
                    $(".li-modules").remove();
                    $.each(obj.modules, function (key, data) {
                        if(data.strike=='1') { 
                            $("#modulesList").append('<li class=" li-modules ui-throw-out" id="" data-mid="'+data.id+'" data-list-id="m_'+data.id+'" >'+data.name+'</li>').ready(function(){
                            });
                        }else {
                            $("#modulesList").append('<li class="ui-state-default li-modules" id="" data-mid="'+data.id+'" data-list-id="m_'+data.id+'" >'+data.name+'</li>').ready(function(){
                                $("#moduleContainerDataList ul").sortable();
                                $('.dragAreaForModule, .dragAreaForLessons, .dragAreaForModuleQuiz, .dragAreaForQuiz').droppable();
                            });
                        }

                        $("#moduleContainer li, #lessonContainer li, #quizzesContainer li, #assessmentsContainer li").draggable({
                            appendTo:   "body",
                            helper:     'clone',
                            connectWith:".connectedSortable",
                            revert:     "invalid",
                            appendTo:   "body",
                        });
                        $("#moduleContainer li.ui-throw-out").draggable({disabled: true})
                    });

                    $(".li-lessons").remove();
                    $.each(obj.lessons, function (key, data) {
                        //console.log(obj.lessons)  
                        if(data.strike=='1') { 
                            $("#lessonsList").append('<li class="li-lessons ui-throw-out" id="" data-list-id="l_'+data.id+'">'+data.name+'</li>').ready(function(){
                                $("#moduleContainerDataList ul").sortable();
                                $('.dragAreaForModule, .dragAreaForLessons, .dragAreaForModuleQuiz, .dragAreaForQuiz').droppable();
                            });
                        }else {
                            $("#lessonsList").append('<li class="ui-state-default li-lessons" id="" data-list-id="l_'+data.id+'">'+data.name+'</li>').ready(function(){
                                $("#moduleContainerDataList ul").sortable();
                                $('.dragAreaForModule, .dragAreaForLessons, .dragAreaForModuleQuiz, .dragAreaForQuiz').droppable();
                            });
                        }
                        $("#moduleContainer li, #lessonContainer li, #quizzesContainer li, #assessmentsContainer li").draggable({
                            appendTo: "body",
                            helper: 'clone',
                            connectWith: ".connectedSortable",
                            revert: "invalid",
                            appendTo: "body",
                        }); 
                        $("#moduleContainer li.ui-throw-out").draggable({disabled: true})
                    });

                    $(".li-quizzes").remove();
                    $.each(obj.quizzes, function (key, data) {
                        if(data.strike=='1') { 
                            $("#quizzesList").append('<li class=" li-quizzes ui-throw-out" id="" data-list-id="q_'+data.id+'">'+data.name+'</li>').ready(function(){
                                $("#moduleContainerDataList ul").sortable();
                                $('.dragAreaForModule, .dragAreaForLessons, .dragAreaForModuleQuiz, .dragAreaForQuiz').droppable();
                            });
                        }else{
                            $("#quizzesList").append('<li class="ui-state-default li-quizzes" id="" data-list-id="q_'+data.id+'">'+data.name+'</li>').ready(function(){
                                $("#moduleContainerDataList ul").sortable();
                                $('.dragAreaForModule, .dragAreaForLessons, .dragAreaForModuleQuiz, .dragAreaForQuiz').droppable();
                            });
                        }
                        $("#moduleContainer li, #lessonContainer li, #quizzesContainer li, #assessmentsContainer li").draggable({
                            appendTo: "body",
                            helper: 'clone',
                            connectWith: ".connectedSortable",
                            revert: "invalid",
                            appendTo: "body",
                        });
                        $("#moduleContainer li.ui-throw-out").draggable({disabled: true})
                    });

                    $.each(obj.assessments, function (key, data) {

                        if(data.strike==1) {

                            $("#assessmentList").append('<li class="li-assessment  ui-throw-out ui-state-default" data-list-id="a_'+data.id+'">'+data.title+'</li>').ready(function(){
                                $("#moduleContainerDataList ul").sortable();
                                $('.dragAreaForModule, .dragAreaForLessons, .dragAreaForModuleQuiz, .dragAreaForQuiz').droppable();
                            });

                        }else{
                            $("#assessmentList").append('<li class="li-assessment ui-state-default" data-list-id="a_'+data.id+'">'+data.title+'</li>').ready(function(){
                                $("#moduleContainerDataList ul").sortable();
                                $('.dragAreaForModule, .dragAreaForLessons, .dragAreaForModuleQuiz, .dragAreaForQuiz').droppable();
                            });
                        }                        
                        $("#moduleContainer li, #lessonContainer li, #quizzesContainer li, #assessmentsContainer li").draggable({
                            appendTo: "body",
                            helper: 'clone',
                            connectWith: ".connectedSortable",
                            revert: "invalid",
                            appendTo: "body",
                        });
                        $("#moduleContainer li.ui-throw-out").draggable({disabled: true})
                    });                     
                }                
            });
        },
        errorPlacement: function (error, $elem) {
            error.insertAfter($elem);
        },
    });

    $.validator.addMethod('selectCatcheck', function (value) {
        return (value != '0');
    }, "Category required");

    $.validator.addMethod('selectTagcheck', function (value) {
        return (value != '0');
    }, "Tag required");

    $(function(){
        var i = 0, j = 0, k = 0, l = 0;
        $("[data-collapse-trigger]").on('click', function(){
            $($(this).attr('data-collapse-trigger')).slideToggle();
            $(this).toggleClass('active');
        });

        $("#moduleContainer li, #lessonContainer li, #quizzesContainer li, #assessmentsContainer li").draggable({ 
            appendTo: "body",
            helper: 'clone',
            connectWith: ".connectedSortable",
            revert: "invalid",
            appendTo: "body",
        });

        //enableSortable();
        $('.dragAreaForModule')
        .droppable({
            accept  : "#moduleContainer li.ui-state-default",
            drop    : function(event, ui) {
                $(this).find(".ui-state-highlight").remove();
                var listLi = $("<li>",{
                    "id":"ui-list-modules-"+ui.draggable.attr('data-list-id'),
                    "class":"ui-custom-collapse module-li",
                    "data-container-id": ui.draggable.attr('data-list-id'),
                    "html" : "<h3><span class=\"move\">"+ui.draggable.text()+"</span><span class=\"collapse-trigger\"></span><a class=\"m-dismiss ui-button\" data-enable-id=\""+$(ui.draggable).attr('data-list-id')+"\"></a></h3><div class=\"panel-list-area panel-lessons-theme collapse-panel\"><ul class=\"inset-container lessonsInsetContainer\" id=\"listOfModulesLesson"+$(ui.draggable).attr('data-list-id')+"\"></ul><ul class=\"inset-container moduleQuizInsetContainer ui-sortable\" id=\"listOfModulesQuiz"+$(ui.draggable).attr('data-list-id')+"\"></ul><div class=\"panel-draggable-area for-lessons dragAreaForLessons\">Drop Lessons Here</div><div class=\"panel-draggable-area for-module-quiz dragAreaForModuleQuiz\">Drop Module Quizzes/Assessments Here</div></div></div>",
                });
                // Saving data module in course
                var mid = ui.draggable.attr('data-list-id'); // module id            
                var cid = $("#course_builder_id").val();
               
                $.ajax({               
                    url: '<?php echo bs('courses/builder'); ?>',
                    type: "POST",
                    data: {'module_id':mid, 'course_id': cid, 'type' : 'M'},
                    success: function (data) {
                        if(data) {
                            setOrders('M', mid, '', '');
                        }
                        
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        var cid = $("#course_builder_id").val(); // course id
                        var json_order_data_all = {}; 
                        $("#listOfAllSection li.module-li").each( function (key, data) {
                            var json_order_data = {};
                            json_order_data['course_id'] = cid;
                            json_order_data['module_id'] = $(this).attr('data-container-id');
                            json_order_data['order_id'] = key;     
                            json_order_data['type'] = 'M';                           
                            json_order_data_all[key] =json_order_data;               
                        });
                        $.ajax({               
                            url: '<?php echo bs('courses/set_orders'); ?>',
                            type: "POST",
                            data: json_order_data_all,
                            success: function (data) {
                                if(data) {
                                // console.log(data);
                                }
                            } 
                        });
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    }               
                });
                $(listLi).appendTo($("#listOfAllSection"));            
                lessonSection();
                moduleQuizSection();
                $(ui.draggable).addClass('ui-throw-out');
                $(ui.draggable).draggable( "disable" );
                CommanJS.getDisplayMessgae(200, ui.draggable.text()+' module added');
            },
            over : function(event, ui) {                      
            }
        });
    
    $("#listOfAllSection")
    .sortable({
        placeholder: "ui-state-highlight",
        //items: "> li",
        tolerance:'intersect',
        cursor: 'move',
        handle: '.move',
        receive: function (event, ui) {
            ui.item.remove();
        },
        sort: function( event, ui ) {
            var getSortableItem = $(ui.item).outerHeight();
            $(ui.placeholder).css({"height":getSortableItem})
        },
        update: function () {     
            // Sort Module
            // after sort arrange order in the db table order id           
            var cid = $("#course_builder_id").val(); // course id
            var json_order_data_all = {}; 
            $("#listOfAllSection li.module-li").each( function (key, data) {
                var json_order_data = {};
                json_order_data['course_id'] = cid;
                json_order_data['module_id'] = $(this).attr('data-container-id');
                json_order_data['order_id'] = key;     
                json_order_data['type'] = 'M';                           
                json_order_data_all[key] =json_order_data;               
            });
            $.ajax({               
                url: '<?php echo bs('courses/set_orders'); ?>',
                type: "POST",
                data: json_order_data_all,
                success: function (data) {
                    if(data) {
                      // console.log(data);
                    }
                } 
            });
        }
    }).disableSelection()
    .on("click", ".m-dismiss", function(event) {
        event.preventDefault();
        var thisId = $(this).parents('li').attr("id");
        var mid = $(this).parents('li[data-container-id]').attr("data-container-id"); // module id
        var cid = $("#course_builder_id").val(); // course id        
        var allLessonChild = $(this).parent().siblings('.panel-list-area').children('ul').children('li[data-lesson-id]');
        var allAssesmentChild = $(this).parent().siblings('.panel-list-area').find('li[data-assessment-id]');
        var allModuleQuizChild = $(this).parent().siblings('.panel-list-area').children('ul').find('li[data-mquiz-id]');
        
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure?',
            buttons: {
                confirm: function () {
                $.ajax({               
                    url: '<?php echo bs('courses/remove_course_data'); ?>',
                    type: "POST",
                    data: {'course_id' : cid, 'module_id' : mid, 'type': 'M'},
                    success: function (data) { 
                        //console.log(data); return false;
                        if(data) {

                            $("#"+thisId).remove();
                            // Removing lesson under modules

                            allLessonChild.each(function() {
                                var allChildrenElement = $(this).attr('data-lesson-id');
                                $("li[data-list-id=\""+allChildrenElement+"\"]").removeClass('ui-throw-out');
                                $("li[data-list-id=\""+allChildrenElement+"\"]").draggable( "enable" );
                                $("li[data-list-id=\""+allChildrenElement+"\"]").addClass("ui-state-default");

                                var allQuizChild = $(this).children('.panel-list-area').children('ul').children('li[data-quiz-id]');
                                console.log($(this));
                                // Removing quizzes under module's lesson
                                allQuizChild.each(function(){
                                    var allChildrenElement = $(this).attr('data-quiz-id');
                                    $("li[data-list-id=\""+allChildrenElement+"\"]").removeClass('ui-throw-out');
                                    $("li[data-list-id=\""+allChildrenElement+"\"]").draggable( "enable" );
                                    $("li[data-list-id=\""+allChildrenElement+"\"]").addClass("ui-state-default");
                                })
                            })

                            // Removing quizzes under modules
                            allModuleQuizChild.each(function () {
                                var allMQChildrenElement = $(this).attr('data-mquiz-id');
                                $("li[data-list-id=\""+allMQChildrenElement+"\"]").removeClass('ui-throw-out');
                                $("li[data-list-id=\""+allMQChildrenElement+"\"]").draggable( "enable" );
                                $("li[data-list-id=\""+allMQChildrenElement+"\"]").addClass("ui-state-default");
                            })

                             // Removing assesment under modules
                            allAssesmentChild.each(function () {
                                var allMAChildrenElement = $(this).attr('data-assessment-id');
                                $("li[data-list-id=\""+allMAChildrenElement+"\"]").removeClass('ui-throw-out');
                                $("li[data-list-id=\""+allMAChildrenElement+"\"]").draggable( "enable" );
                                $("li[data-list-id=\""+allMAChildrenElement+"\"]").addClass("ui-state-default");
                            })
                            
                            CommanJS.getDisplayMessgae(200, 'Module removed');
                            $("li[data-list-id=\""+mid+"\"]").removeClass('ui-throw-out');
                            $("li[data-list-id=\""+mid+"\"]").draggable( "enable" );
                            $("li[data-list-id=\""+mid+"\"]").addClass("ui-state-default");


                            var json_order_data_all = {}; 
                            $("#listOfAllSection li.module-li").each( function (key, data) {
                                var json_order_data = {};
                                json_order_data['course_id'] = cid;
                                json_order_data['module_id'] = $(this).attr('data-container-id');
                                json_order_data['order_id'] = key;     
                                json_order_data['type'] = 'M';                           
                                json_order_data_all[key] =json_order_data;               
                            });
                            console.log(json_order_data_all);

                            $.ajax({               
                                url: '<?php echo bs('courses/set_orders'); ?>',
                                type: "POST",
                                data: json_order_data_all,
                                success: function (data) {
                                    if(data) {
                                        //console.log(data);                                
                                    }
                                } 
                            });
                        }
                    }
                });    
            },
            cancel: function () {
                    return true;
                }
            }
        });    
    })
    .on("click",".collapse-trigger",function(){
        $(this).toggleClass('active');
        $(this).parent().siblings('.collapse-panel').slideToggle(function(){
            if($(this).is(':hidden')){
                $(this).removeClass('open');
                //enableSortable();
            }else{
                $(this).addClass('open');
                //disableSortable()
            }
        });
    });
    $(".collapse-trigger").on("click",function(){
        $(this).parent().siblings('.collapse-panel').slideToggle(function(){
            if($(this).is(':hidden')){
                $(this).removeClass('open');
                //enableSortable();
            }else{
                $(this).addClass('open');
                //disableSortable()
            }
        });
    });

    function lessonSection () {
        $(".lessonsInsetContainer")
        .sortable({
            placeholder: "ui-state-highlight",
            cursor: 'move',
            sort: function( event, ui ) {
                var getSortableItem = $(ui.item).outerHeight();
                $(ui.placeholder).css({"height":getSortableItem})
            },
            update: function () {                
                // Sort lesson
                var cid = $("#course_builder_id").val(); // course id
                // Saving data lesson in course
                var mid = $(this).parent().parent().attr('data-container-id'); // module id
                var json_order_data_all = {}; 
                $("#listOfModulesLesson"+mid+" li.li-ml").each( function (key, data) {
                    var json_order_data = {};
                    json_order_data['course_id'] = cid;
                    json_order_data['module_id'] = mid;
                    json_order_data['lesson_id'] = $(this).attr('data-lesson-id'); // lesson id
                    json_order_data['order_id'] = key;     
                    json_order_data['type'] = 'L';                           
                    json_order_data_all[key] =json_order_data;
                });
                console.log(json_order_data_all);
                $.ajax({               
                    url: '<?php echo bs('courses/set_orders'); ?>',
                    type: "POST",
                    data: json_order_data_all,
                    success: function (data) {
                        if(data) {
                           //CommanJS.getDisplayMessgae(200, 'Lesson removed');
                        }
                    } 
                });
            }
        })
        .disableSelection()
        .off('click')
        .on("click", ".l-dismiss", function(event) {
            
            event.preventDefault();
            var thisId = $(this).parents('li').attr("id");
            var mid = $(this).parents('li[data-container-id]').attr("data-container-id"); // module id
            var lid = $(this).attr("data-enable-id"); // lesson id
            var cid = $("#course_builder_id").val(); // course id
            var allChild = $(this).parent().siblings('.panel-list-area').children('ul').children('li[data-quiz-id]');
            
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure?',
                buttons: {
                    confirm: function () {
                        $.ajax({               
                            url: '<?php echo bs('courses/remove_course_data'); ?>',
                            type: "POST",
                            data: {'course_id' : cid, 'module_id' : mid, 'lesson_id' : lid, 'type': 'L'},
                            success: function (data) { 
                                if(data) {
                                    $("#"+thisId).remove();            
                                    //removeAllChildrenElement($(this));                        
                                    console.log($("li[data-list-id=\""+lid+"\"]"));
                                    CommanJS.getDisplayMessgae(200, 'Lesson removed');

                                    $("li[data-list-id=\""+lid+"\"]").removeClass('ui-throw-out');
                                    $("li[data-list-id=\""+lid+"\"]").draggable( "enable" );
                                    $("li[data-list-id=\""+lid+"\"]").addClass("ui-state-default");
                                    allChild.each(function(){
                                        var qz_id = $(this).attr('data-quiz-id');
                                        console.log(qz_id);
                                        $("li[data-list-id=\""+qz_id+"\"]").removeClass('ui-throw-out');
                                        $("li[data-list-id=\""+qz_id+"\"]").draggable( "enable" );
                                        $("li[data-list-id=\""+qz_id+"\"]").addClass("ui-state-default");
                                    })                                    

                                    var json_order_data_all = {}; 
                                    $("#listOfModulesLesson"+mid+" li").each( function (key, data) {
                                        var json_order_data = {};
                                        json_order_data['course_id'] = cid;
                                        json_order_data['module_id'] = mid;
                                        json_order_data['lesson_id'] = $(this).attr('data-lesson-id'); // lesson id
                                        json_order_data['order_id'] = key;     
                                        json_order_data['type'] = 'L';                           
                                        json_order_data_all[key] =json_order_data;
                                        //console.log($(this).attr('data-lesson-id'));         
                                    });
                                    console.log(json_order_data_all);
                             
                                    $.ajax({               
                                        url: '<?php echo bs('courses/set_orders'); ?>',
                                        type: "POST",
                                        data: json_order_data_all,
                                        success: function (data) {
                                            if(data) {                                                
                                                console.log(data);                                    
                                            }
                                        } 
                                    });                       
                                }
                            }
                        });
                    },
                    cancel: function () {
                        return true;
                    }
                }
            });
            
        });                    
        $('.dragAreaForLessons')
        .droppable({
            accept: "#lessonContainer li",
            drop : function(event, ui) {
                $(this).find(".ui-state-highlight").remove();
                var listLi = $("<li>",{
                    "id":"ui-list-lessons-"+$(ui.draggable).attr('data-list-id'),
                    "class":"ui-custom-collapse li-ml",
                    "data-lesson-id": $(ui.draggable).attr('data-list-id'),
                    "html" : "<h3><span class=\"move\">"+ui.draggable.text()+"</span><span class=\"collapse-trigger\"></span><a class=\"l-dismiss ui-button\" data-enable-id=\""+$(ui.draggable).attr('data-list-id')+"\" ></a></h3><div class=\"panel-list-area panel-quiz-theme collapse-panel\"><ul class=\"inset-container-quiz quizInsetContainer\" id=\"listOfLessonsQuiz"+$(ui.draggable).attr('data-list-id')+"\" data-qzid=\""+$(ui.draggable).attr('data-list-id')+"\"></ul><div class=\"panel-draggable-area for-quiz dragAreaForQuiz\">Drop Lesson Quizzes/Assessments Here</div></div>",
                });

                // Saving data lesson in course
                var mid = $(this).parent().parent().attr('data-container-id');
                console.log(mid);
                var lid = ui.draggable.attr('data-list-id'); // lesson id
                var cid = $("#course_builder_id").val(); // course id
                console.log(lid);
                
                $.ajax({               
                    url: '<?php echo bs('courses/builder'); ?>',
                    type: "POST",
                    data: {'lesson_id':lid, 'course_id':cid, 'module_id':mid, 'type': 'L'},
                    success: function (data) {
                        if(data) {
                            setOrders('L', mid, lid, '');
                        }
                    }               
                });

                $(listLi).appendTo($(this).siblings('.lessonsInsetContainer'));
                $(ui.draggable).addClass('ui-throw-out');
                $(ui.draggable).draggable( "disable" );
                CommanJS.getDisplayMessgae(200, ui.draggable.text()+' lesson added');
                quizSection();
            },
            over : function(event, ui){                      
            }
        });
    }
/*****************************************************************************/
/*  Global Quiz Section  */
    $(".dragAreaForGlobalQuizze").droppable({
        accept: "#quizzesContainer li, #assessmentsContainer li",
        drop : function(event, ui) {
            $(this).find(".ui-state-highlight").remove();
            if($(ui.draggable[0]).hasClass('li-assessment')) {                
                var listLi = $("<li>",{
                    "id":"ui-list-global-quize-"+$(ui.draggable).attr('data-list-id'),
                    "class":"ui-custom-collapse g-assessment-li",
                    "data-container-id": ui.draggable.attr('data-list-id'),
                    "html" : "<h3><span class=\"move\">"+ui.draggable.text()+"</span><a class=\"ga-dismiss ui-button\" data-enable-id=\""+$(ui.draggable).attr('data-list-id')+"\"></a></h3>",
                });
                
                var cid = $("#course_builder_id").val(); // course id
                var qid = ui.draggable.attr('data-list-id');
                $.ajax({               
                    url: '<?php echo bs('courses/builder'); ?>',
                    type: "POST",
                    data: {'course_id':cid, 'quiz_id':qid, 'type': 'CA'},
                    success: function (data) {
                        if(data) {
                            setOrders('CA', '','', '');
                        }
                    }               
                });
                CommanJS.getDisplayMessgae(200, ui.draggable.text()+' Assessments added');

            }else if($(ui.draggable[0]).hasClass('li-quizzes') || $(ui.draggable[0]).hasClass('li-term')){
                var listLi = $("<li>",{
                    "id":"ui-list-global-quize-"+$(ui.draggable).attr('data-list-id'),
                    "class":"ui-custom-collapse g-quiz-li",
                    "data-container-id": ui.draggable.attr('data-list-id'),
                    "html" : "<h3><span class=\"move\">"+ui.draggable.text()+"</span><a class=\"gq-dismiss ui-button\" data-enable-id=\""+$(ui.draggable).attr('data-list-id')+"\"></a></h3>",
                });

                var cid = $("#course_builder_id").val(); // course id
                var qid = ui.draggable.attr('data-list-id');
                $.ajax({               
                    url: '<?php echo bs('courses/builder'); ?>',
                    type: "POST",
                    data: {'course_id':cid, 'quiz_id':qid, 'type': 'CQ'},
                    success: function (data) {
                        if(data) {
                            setOrders('CQ', '','', '');
                        }
                    }               
                });
                CommanJS.getDisplayMessgae(200, 'Quiz added');
            }else{
            }
            $(listLi).appendTo($("#listOfGlobalQuizze"));
            $(ui.draggable).addClass('ui-throw-out');
            $(ui.draggable).draggable( "disable" );           
        }
    });
    
    $(".ui-global-quiz-panel").sortable({
        placeholder: "ui-state-highlight",
        sort: function( event, ui ) {
            var getSortableItem = $(ui.item).outerHeight();
            $(ui.placeholder).css({"height":getSortableItem})
        },
        update: function (event, ui ) {
            // Sorting assessments
            console.log($(ui.item[0]).attr("data-container-id"));
            var qid = $(ui.item[0]).attr("data-container-id");
            var res_ass_quiz = qid.split('_');
            var cid = $("#course_builder_id").val(); // course id     
            if(res_ass_quiz[0]=='a') {
                var json_order_data_all = {}; 
                $("#listOfGlobalQuizze li.g-assessment-li").each( function (key, data) {
                    var json_order_data = {};
                    json_order_data['course_id'] = cid;
                    json_order_data['module_id'] = null;
                    json_order_data['quiz_id'] = $(this).attr('data-container-id');
                    json_order_data['quiz_type'] = 4;
                    json_order_data['contain_type'] = 1;
                    json_order_data['order_id'] = key;     
                    json_order_data['type'] = 'CA';                                   
                    json_order_data_all[key] =json_order_data;              
                });
                console.log(json_order_data_all);
                $.ajax({               
                    url: '<?php echo bs('courses/set_orders'); ?>',
                    type: "POST",
                    data: json_order_data_all,
                    success: function (data) {
                        if(data) {
                           //console.log(data);
                        }
                    } 
                });
            }else  if(res_ass_quiz[0]=='q') { 
                var json_order_data_all = {}; 
                $("#listOfGlobalQuizze li.g-quiz-li").each( function (key, data) {
                    var json_order_data = {};
                    json_order_data['course_id'] = cid;
                    json_order_data['module_id'] = null;
                    json_order_data['quiz_id'] = $(this).attr('data-container-id');
                    json_order_data['quiz_type'] = 4;
                    json_order_data['contain_type'] = 0;
                    json_order_data['order_id'] = key;     
                    json_order_data['type'] = 'CQ';                                   
                    json_order_data_all[key] =json_order_data;              
                });
                console.log(json_order_data_all);
                $.ajax({               
                    url: '<?php echo bs('courses/set_orders'); ?>',
                    type: "POST",
                    data: json_order_data_all,
                    success: function (data) {
                        if(data) {
                           //console.log(data);
                        }
                    } 
                });
            }
        }

    }).disableSelection()
    .off('click')
    .on("click",".gq-dismiss, .ga-dismiss", function(event) {
        var gqid = event.target.dataset.enableId;

        var res_ass_quiz = gqid.split("_");
        var cid = $("#course_builder_id").val(); // course id
        if(res_ass_quiz[0]=='a') {
            $.confirm({ 
                title: 'Confirm!',
                content: 'Are you sure?',
                buttons: {
                    confirm: function () {
                        $.ajax({               
                            url: '<?php echo bs('courses/remove_course_data'); ?>',
                            type: "POST",
                            data: {'course_id' : cid, 'quiz_id' : gqid, 'type': 'CA' },
                            success: function (data) {                                
                                if(data==1) { 
                                    var listLi = document.querySelectorAll('[data-list-id="'+gqid+'"]')[0];
                                    listLi.classList.remove('ui-throw-out');
                                    $(listLi).draggable("enable");
                                    event.target.parentElement.parentNode.remove()
                                    var json_order_data_all = {}; 
                                    $("#listOfGlobalQuizze li.g-assessment-li").each( function (key, data) {
                                        var json_order_data = {};
                                        json_order_data['course_id'] = cid;
                                        json_order_data['module_id'] = null;
                                        json_order_data['quiz_id'] = $(this).attr('data-container-id');
                                        json_order_data['quiz_type'] = 4;
                                        json_order_data['contain_type'] = 1;
                                        json_order_data['order_id'] = key;     
                                        json_order_data['type'] = 'CA';               
                                        //json_order_data_all.push(json_order_data);              
                                        json_order_data_all[key] =json_order_data;              
                                    });
                                    console.log(json_order_data_all);
                                    $.ajax({               
                                        url: '<?php echo bs('courses/set_orders'); ?>',
                                        type: "POST",
                                        data: json_order_data_all,
                                        success: function (data) {
                                            if(data) {
                                               //console.log(data);
                                            }
                                        } 
                                    });
                                }
                            }
                        });
                    },
                    cancel: function () {
                        return true;
                    }
                }
            });
        }else if(res_ass_quiz[0]=='q') {
            // Course quizzess
            $.confirm({ 
                title: 'Confirm!',
                content: 'Are you sure?',
                buttons: {
                    confirm: function () {
                        $.ajax({               
                            url: '<?php echo bs('courses/remove_course_data'); ?>',
                            type: "POST",
                            data: {'course_id' : cid, 'quiz_id' : gqid, 'type': 'CQ' },
                            success: function (data) {
                                if(data==1) { 
                                    var listLi = document.querySelectorAll('[data-list-id="'+gqid+'"]')[0];
                                    listLi.classList.remove('ui-throw-out');
                                    $(listLi).draggable("enable");
                                    event.target.parentElement.parentNode.remove()
                                    var json_order_data_all = {}; 
                                    $("#listOfGlobalQuizze li.g-quiz-li").each( function (key, data) {
                                        var json_order_data = {};
                                        json_order_data['course_id'] = cid;
                                        json_order_data['module_id'] = null;
                                        json_order_data['quiz_id'] = $(this).attr('data-container-id');
                                        json_order_data['quiz_type'] = 4;
                                        json_order_data['contain_type'] = 0;
                                        json_order_data['order_id'] = key;     
                                        json_order_data['type'] = 'CQ';                        
                                        json_order_data_all[key] =json_order_data;              
                                    });
                                    console.log(json_order_data_all);
                                    $.ajax({               
                                        url: '<?php echo bs('courses/set_orders'); ?>',
                                        type: "POST",
                                        data: json_order_data_all,
                                        success: function (data) {
                                            if(data) {
                                               //console.log(data);
                                            }
                                        } 
                                    });
                                }
                            }
                        });
                    },
                    cancel: function () {                       
                        return true;
                    }
                }
            });
        }
       
    });

/*****************************************************************************/    

/*********************************** Module Quiz Section  ****************************************/
/*  Module Quiz Section  */
function moduleQuizSection() {

    $(".moduleQuizInsetContainer")
    .sortable({
        placeholder: "ui-state-highlight",
        sort: function( event, ui ) {
            var getSortableItem = $(ui.item).outerHeight();
            $(ui.placeholder).css({"height":getSortableItem})
        },
        update: function (event, ui ) { 
            // Sort module quiz and assessments
            var cid = $("#course_builder_id").val(); // course id
            // Saving data lesson in course
            var mid = $(this).parent().parent().attr('data-container-id'); // module id
            var json_order_data_all = {}; 
            $("#listOfModulesQuiz"+mid+" li.li-mq").each( function (key, data) {                
                var json_order_data = {};
                json_order_data['course_id'] = cid;
                json_order_data['module_id'] = mid;
                json_order_data['quiz_id'] = $(this).attr('data-mquiz-id');
                json_order_data['order_id'] = key;     
                json_order_data['type'] = 'MQ';                           
                json_order_data_all[key] =json_order_data;
                //console.log($(this).attr('data-lesson-id'));         
            });
            console.log(json_order_data_all); 
            $.ajax({               
                url: '<?php echo bs('courses/set_orders'); ?>',
                type: "POST",
                data: json_order_data_all,
                success: function (data) {
                    if(data) {
                       CommanJS.getDisplayMessgae(200, 'Quiz sorted');
                    }
                } 
            });

            // Assessments
            var json_order_data_all = {}; 
            $("#listOfModulesQuiz"+mid+" li.li-ma").each( function (key, data) {
                var json_order_data = {};
                json_order_data['course_id'] = cid;
                json_order_data['module_id'] = mid;
                json_order_data['quiz_id'] = $(this).attr('data-mquiz-id'); // Assessment id
                json_order_data['order_id'] = key;     
                json_order_data['type'] = 'MA';                           
                json_order_data_all[key] =json_order_data;
                //console.log($(this).attr('data-lesson-id'));         
            });
            console.log(json_order_data_all);
            $.ajax({               
                url: '<?php echo bs('courses/set_orders'); ?>',
                type: "POST",
                data: json_order_data_all,
                success: function (data) {
                    if(data) {
                       CommanJS.getDisplayMessgae(200, 'Assessments sorted');
                    }
                } 
            });

        }
    })
    .disableSelection()
    .off('click')
    .on("click", ".mq-dismiss, .ma-dismiss", function(event) {
        var mid = $(this).parents('li[data-container-id]').attr("data-container-id"); // module id
        var mqid = $(this).attr('data-enable-id'); // module quiz id       
        var cid = $("#course_builder_id").val(); // course id
        var res_mass_mquiz = mqid.split("_");
        
        if(res_mass_mquiz[0]=='q') {
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure?',
                buttons: {
                    confirm: function () {
                        $.ajax({               
                            url: '<?php echo bs('courses/remove_course_data'); ?>',
                            type: "POST",
                            data: {'course_id' : cid, 'module_id' : mid, 'quiz_id' : mqid, 'type': 'MQ' },
                            success: function (data) {
                                if(data) {                                          
                                    
                                    var listLi =  document.querySelectorAll('[data-list-id="'+mqid+'"]')[0];
                                    if(listLi){
                                        //$('[data-list-id="'+mqid+'"]');
                                        listLi.classList.remove('ui-throw-out');
                                        $(listLi).draggable( "enable" );     
                                        $(listLi).addClass("ui-state-default");                                  
                                    }
                                    event.target.parentElement.parentNode.remove();

                                    var json_order_data_all = {}; 
                                    $("#listOfModulesQuiz"+mid+" li.li-mq").each( function (key, data) {
                                        var json_order_data = {};
                                        json_order_data['course_id'] = cid;
                                        json_order_data['module_id'] = mid;
                                        json_order_data['quiz_id'] = $(this).attr('data-mquiz-id');
                                        json_order_data['order_id'] = key;     
                                        json_order_data['type'] = 'MQ';                           
                                        json_order_data_all[key] =json_order_data;
                                        //console.log($(this).attr('data-lesson-id'));         
                                    });
                                    console.log(json_order_data_all);
                                    $.ajax({               
                                        url: '<?php echo bs('courses/set_orders'); ?>',
                                        type: "POST",
                                        data: json_order_data_all,
                                        success: function (data) {
                                            if(data) {
                                              
                                            }
                                        } 
                                    });  
                                    CommanJS.getDisplayMessgae(200, 'Quiz removed');           
                                }
                            } 
                        });

                    },
                    cancel: function () {
                        return true;
                    }
                },            
            });
        } else if(res_mass_mquiz[0]=='a') {
            // Remove module assessments
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure?',
                buttons: {
                    confirm: function () {
                        $.ajax({               
                            url: '<?php echo bs('courses/remove_course_data'); ?>',
                            type: "POST",
                            data: {'course_id' : cid, 'module_id' : mid, 'quiz_id' : mqid, 'type': 'MA' },
                            success: function (data) {
                                if(data) {                                          
                                    
                                    var listLi =  document.querySelectorAll('[data-list-id="'+mqid+'"]')[0];
                                    if(listLi){
                                        //$('[data-list-id="'+mqid+'"]');
                                        listLi.classList.remove('ui-throw-out');
                                        $(listLi).draggable( "enable" );     
                                        $(listLi).addClass("ui-state-default");                                  
                                    }
                                    event.target.parentElement.parentNode.remove();

                                    var json_order_data_all = {}; 
                                    $("#listOfModulesQuiz"+mid+" li.li-ma").each( function (key, data) {
                                        var json_order_data = {};
                                        json_order_data['course_id'] = cid;
                                        json_order_data['module_id'] = mid;
                                        json_order_data['quiz_id'] = $(this).attr('data-mquiz-id');
                                        json_order_data['order_id'] = key;     
                                        json_order_data['type'] = 'MA';                           
                                        json_order_data_all[key] =json_order_data;
                                        //console.log($(this).attr('data-lesson-id'));         
                                    });
                                    console.log(json_order_data_all);
                                    $.ajax({               
                                        url: '<?php echo bs('courses/set_orders'); ?>',
                                        type: "POST",
                                        data: json_order_data_all,
                                        success: function (data) {
                                            if(data) {
                                              
                                            }
                                        } 
                                    });  
                                    CommanJS.getDisplayMessgae(200, 'Assessment removed');           
                                }
                            } 
                        });

                    },
                    cancel: function () {
                        return true;
                    }
                },            
            });
        }
    });


    $('.dragAreaForModuleQuiz')
    .droppable({
        accept: "#quizzesList li.li-quizzes, #assessmentsContainer li",
        drop : function(event, ui) {
            $(this).find(".ui-state-highlight").remove();
            console.log($(ui.draggable[0]))
            if($(ui.draggable[0]).hasClass('li-assessment')){
                var listLi = $("<li>",{
                    "id":"ui-list-mquiz-"+$(ui.draggable).attr('data-list-id'),
                    "class":"ui-custom-collapse li-ma",
                    "data-mquiz-id": $(ui.draggable).attr('data-list-id'),
                    "html" : "<h3><span class=\"move\">"+ui.draggable.text()+"</span><a class=\"mq-dismiss ui-button\" data-enable-id=\""+$(ui.draggable).attr('data-list-id')+"\" ></a></h3>",
                });
            }else if($(ui.draggable[0]).hasClass('li-quizzes')){
                var listLi = $("<li>",{
                    "id":"ui-list-mquiz-"+$(ui.draggable).attr('data-list-id'),
                    "class":"ui-custom-collapse li-mq",
                    "data-mquiz-id": $(ui.draggable).attr('data-list-id'),
                    "html" : "<h3><span class=\"move\">"+ui.draggable.text()+"</span><a class=\"mq-dismiss ui-button\" data-enable-id=\""+$(ui.draggable).attr('data-list-id')+"\" ></a></h3>",
                });
            }else{

            }
            $(listLi).appendTo($(this).siblings('ul.moduleQuizInsetContainer'));
            $(ui.draggable).addClass('ui-throw-out');
            $(ui.draggable).draggable( "disable" );

            // Saving data modules in courses
            var mid = $(this).parent().parent().attr('data-container-id');            
            var cid = $("#course_builder_id").val(); // course id
            var qid = ui.draggable.attr('data-list-id'); // quiz id      
            var res_module_ass_quiz = qid.split("_"); 

            var d_type = ''; 
            if(res_module_ass_quiz[0]=='a') d_type="MA"; else if(res_module_ass_quiz[0]=='q') d_type="MQ";

            $.ajax({               
                url: '<?php echo bs('courses/builder'); ?>',
                type: "POST",
                data: {'course_id':cid, 'module_id':mid, 'quiz_id':qid, 'type': d_type },
                success: function (data) {
                    if(data) {
                        setOrders(d_type, mid, '', qid);
                    }
                }               
            });
        },
        over : function(event, ui){                      
        }
    });
}
/* ========================================== Module Quiz Section ends ======================================== */

/* ========================================== Lesson Quiz Section ============================================= */

    function quizSection () {
        $(".quizInsetContainer")
        .sortable({
            placeholder: "ui-state-highlight",
            sort: function( event, ui ) {
                var getSortableItem = $(ui.item).outerHeight();
                $(ui.placeholder).css({"height":getSortableItem})
            },
            update: function (event, ui ) {
                //sort quiz and Assessments
                var cid = $("#course_builder_id").val(); // course id
                var mid = $(this).parents('li.module-li').attr('data-container-id');
                var lid = $(this).attr('data-qzid');

                // Quiz only
                var json_order_data_all = {}; 
                $("#listOfLessonsQuiz"+lid+" li.lesson-li").each( function (key, data) {
                    var json_order_data = {};
                    json_order_data['course_id'] = cid;
                    json_order_data['module_id'] = mid;
                    json_order_data['lesson_id'] = lid;
                    json_order_data['quiz_id'] = $(this).attr('data-quiz-id');
                    json_order_data['order_id'] = key;     
                    json_order_data['type'] = 'LQ';                           
                    json_order_data_all[key] =json_order_data;
                    //console.log($(this).attr('data-lesson-id'));         
                });
                console.log(json_order_data_all);
                $.ajax({               
                    url: '<?php echo bs('courses/set_orders'); ?>',
                    type: "POST",
                    data: json_order_data_all,
                    success: function (data) {
                        if(data) {
                           //console.log(data);
                        }
                    } 
                });

                //ASSESSMENT
                var json_ass_order_data_all = {}; 
                $("#listOfLessonsQuiz"+lid+" li.assessment-li").each( function (key, data) {
                    var json_order_data = {};
                    json_order_data['course_id'] = cid;
                    json_order_data['module_id'] = mid;
                    json_order_data['lesson_id'] = lid;
                    json_order_data['quiz_id'] = $(this).attr('data-assessment-id');
                    json_order_data['order_id'] = key;     
                    json_order_data['type'] = 'LA';                           
                    json_ass_order_data_all[key] =json_order_data;
                    //console.log($(this).attr('data-lesson-id'));         
                });
                console.log(json_ass_order_data_all);
                $.ajax({               
                    url: '<?php echo bs('courses/set_orders'); ?>',
                    type: "POST",
                    data: json_ass_order_data_all,
                    success: function (data) {
                        if(data) {
                           //console.log(data);
                        }
                    } 
                });
            }
        })
        .disableSelection()
        .off('click')
        .on("click", ".q-dismiss, .a-dismiss", function(event) {
            event.preventDefault();
            var thisId = $(this).parent().parent('li').attr("id"); 
            var mid = $(this).parents('li[data-container-id]').attr("data-container-id"); // module id
            var lid = $(this).parents('li[data-lesson-id]').attr("data-lesson-id"); // lesson id
            var qid = $(this).attr('data-enable-id');
            var cid = $("#course_builder_id").val(); // course id
            var res_ass_quiz = qid.split("_");

            if(res_ass_quiz[0] =='a') { 
                $.confirm({
                    title: 'Confirm!',
                    content: 'Are you sure?',
                    buttons: {
                        confirm: function () {
                            $.ajax({               
                                url: '<?php echo bs('courses/remove_course_data'); ?>',
                                type: "POST",
                                data: {'course_id' : cid, 'module_id' : mid, 'lesson_id' : lid, 'quiz_id' : qid, 'type': 'LA' },
                                success: function (data) {
                                    if(data) {
                                        $("#"+thisId).remove();
                                        console.log(data);                        
                                        CommanJS.getDisplayMessgae(200, 'Assessment removed');
                                        $("li[data-list-id=\""+qid+"\"]").removeClass('ui-throw-out');
                                        $("li[data-list-id=\""+qid+"\"]").draggable( "enable" );
                                        $("li[data-list-id=\""+qid+"\"]").addClass("ui-state-default");

                                        var json_order_data_all = {}; 
                                        $("#listOfLessonsQuiz"+lid+" li.assessment-li").each( function (key, data) {
                                            var json_order_data = {};
                                            json_order_data['course_id'] = cid;
                                            json_order_data['module_id'] = mid;
                                            json_order_data['lesson_id'] = lid;
                                            json_order_data['quiz_id'] = $(this).attr('data-assessment-id');
                                            json_order_data['order_id'] = key;     
                                            json_order_data['type'] = 'LA';                           
                                            json_order_data_all[key] =json_order_data;
                                            //console.log($(this).attr('data-lesson-id'));         
                                        });
                                        console.log(json_order_data_all);
                                        $.ajax({               
                                            url: '<?php echo bs('courses/set_orders'); ?>',
                                            type: "POST",
                                            data: json_order_data_all,
                                            success: function (data) {
                                                if(data) {
                                                  
                                                }
                                            } 
                                        });             
                                    }
                                } 
                            });
                        },
                        cancel: function () {
                            return true;
                        }
                    }
                });
            }else if(res_ass_quiz[0] =='q') {

                $.confirm({
                    title: 'Confirm!',
                    content: 'Are you sure?',
                    buttons: {
                        confirm: function () {
                            $.ajax({               
                                url: '<?php echo bs('courses/remove_course_data'); ?>',
                                type: "POST",
                                data: {'course_id' : cid, 'module_id' : mid, 'lesson_id' : lid, 'quiz_id' : qid, 'type': 'LQ' },
                                success: function (data) {
                                    if(data) {
                                        $("#"+thisId).remove();
                                        console.log(data);                        
                                        CommanJS.getDisplayMessgae(200, 'Quiz removed');
                                        $("li[data-list-id=\""+qid+"\"]").removeClass('ui-throw-out');
                                        $("li[data-list-id=\""+qid+"\"]").draggable( "enable" );
                                        $("li[data-list-id=\""+qid+"\"]").addClass("ui-state-default");

                                        var json_order_data_all = {}; 
                                        $("#listOfLessonsQuiz"+lid+" li.lesson-li").each( function (key, data) {
                                            var json_order_data = {};
                                            json_order_data['course_id'] = cid;
                                            json_order_data['module_id'] = mid;
                                            json_order_data['lesson_id'] = lid;
                                            json_order_data['quiz_id'] = $(this).attr('data-quiz-id');
                                            json_order_data['order_id'] = key;     
                                            json_order_data['type'] = 'LQ';                           
                                            json_order_data_all[key] =json_order_data;
                                            //console.log($(this).attr('data-lesson-id'));         
                                        });
                                        //console.log(json_order_data_all);
                                        $.ajax({               
                                            url: '<?php echo bs('courses/set_orders'); ?>',
                                            type: "POST",
                                            data: json_order_data_all,
                                            success: function (data) {
                                                if(data) {
                                                  
                                                }
                                            } 
                                        });             
                                    }
                                } 
                            });
                        },
                        cancel: function () {
                            return true;
                        }
                    }
                });                
            }
        });                    
        $('.dragAreaForQuiz')
        .droppable({
            accept: "#quizzesContainer li.li-quizzes, #assessmentsContainer li",
            drop : function(event, ui) {
                $(this).find(".ui-state-highlight").remove();
                var listLi;
                if($(ui.draggable[0]).hasClass('li-assessment')){
                    listLi = $("<li>",{
                        "id":"ui-list-quiz-"+$(ui.draggable).attr('data-list-id'),
                        "class":"ui-custom-collapse assessment-li",
                        "data-assessment-id": $(ui.draggable).attr('data-list-id'),
                        "html" : "<h3><span class=\"move\">"+ui.draggable.text()+"</span><a class=\"a-dismiss ui-button\"  data-enable-id=\""+$(ui.draggable).attr('data-list-id')+"\"></a></h3>",
                    });

                }else if($(ui.draggable[0]).hasClass('li-quizzes')){
                    listLi = $("<li>",{
                        "id":"ui-list-quiz-"+$(ui.draggable).attr('data-list-id'),
                        "class":"ui-custom-collapse lesson-li",
                        "data-quiz-id": $(ui.draggable).attr('data-list-id'),
                        "html" : "<h3><span class=\"move\">"+ui.draggable.text()+"</span><a class=\"q-dismiss ui-button\"  data-enable-id=\""+$(ui.draggable).attr('data-list-id')+"\"></a></h3>",
                    });
                }else{

                }               

                // Saving data quiz/polls in lesson
                var cid = $("#course_builder_id").val(); // course id
                var mID = $(this).parents('li');     
                var mid = $(mID[1]).attr("data-container-id");      // Module id      
                var lid = $(this).siblings('ul').attr("data-qzid") // lesson id
                var qid = ui.draggable.attr('data-list-id'); // quiz id 

                var res_qid = qid.split("_");           
                var d_type='';
                if(res_qid[0]=='a') d_type='LA'; else{ d_type='LQ'; }

                $.ajax({               
                    url: '<?php echo bs('courses/builder'); ?>',
                    type: "POST",
                    data: {'lesson_id':lid, 'course_id':cid, 'module_id':mid, 'quiz_id':qid, 'type': d_type},
                    success: function (data) {
                        if(data) {
                            setOrders(d_type, mid, lid, qid);
                        }
                    }               
                });
                $(listLi).appendTo($(this).siblings('.quizInsetContainer'));
                CommanJS.getDisplayMessgae(200, ui.draggable.text()+' quiz added test-ass');
                $(ui.draggable).addClass('ui-throw-out');
                $(ui.draggable).draggable( "disable" );
            },
            over : function(event, ui){                      
            }
        });
        /******************************************************/
    }
/* ======================================== Lesson Quiz Section =========================================== */

    function removeAllChildrenElement($this){
        var childeElement = $this.parent().siblings('.collapse-panel').children('ul').children('li');
        childeElement.each(function(){
            var chLiElement = $(this).find('a').attr('data-enable-id');
            $("li[data-list-id=\""+chLiElement+"\"]").removeClass('ui-throw-out');
            $("li[data-list-id=\""+chLiElement+"\"]").draggable( "enable" );   
            $("li[data-list-id=\""+chLiElement+"\"]").addClass("ui-state-default");
        });
    }
    function disableSortable(){
        $("#listOfAllSection").sortable({
           disabled: true 
        }).enableSelection();
    }
    function enableSortable(){
        $("#listOfAllSection").sortable({
            disabled: false,
            placeholder: "ui-state-highlight",
            tolerance:'intersect',
            receive: function (event, ui) {
                ui.item.remove();
            },
            sort: function( event, ui ) {
                var getSortableItem = $(ui.item).outerHeight();
                $(ui.placeholder).css({"height":getSortableItem})
            }
        }).disableSelection();
    }

    // set the order
    function setOrders(type, moduleId, lessonId, quizId) {
        var cid = $("#course_builder_id").val(); // course id

        if(type=='M') {
            var json_order_data_all = {}; 
            $("#listOfAllSection li.module-li").each( function (key, data) {
                var json_order_data = {};
                json_order_data['course_id'] = cid;
                json_order_data['module_id'] = moduleId;
                json_order_data['order_id'] = key;     
                json_order_data['type'] = 'M';               
                //json_order_data_all.push(json_order_data);              
                json_order_data_all[key] =json_order_data;              
            });
            //console.log(json_order_data_all);
            $.ajax({               
                url: '<?php echo bs('courses/set_orders'); ?>',
                type: "POST",
                data: json_order_data_all,
                success: function (data) {
                    if(data) {
                       //console.log(data);
                    }
                } 
            });
        }else if(type=='CA') {
            var json_order_data_all = {}; 
            $("#listOfGlobalQuizze li.g-assessment-li").each( function (key, data) {
                var json_order_data = {};
                json_order_data['course_id'] = cid;
                json_order_data['module_id'] = null;
                json_order_data['quiz_id'] = $(this).attr('data-container-id');
                json_order_data['quiz_type'] = 4;
                json_order_data['contain_type'] = 1;
                json_order_data['order_id'] = key;     
                json_order_data['type'] = 'CA';               
                //json_order_data_all.push(json_order_data);              
                json_order_data_all[key] =json_order_data;              
            });
            console.log(json_order_data_all);
            $.ajax({               
                url: '<?php echo bs('courses/set_orders'); ?>',
                type: "POST",
                data: json_order_data_all,
                success: function (data) {
                    if(data) {
                       //console.log(data);
                    }
                } 
            });
        }else if(type=='CQ') {
            var json_order_data_all = {}; 
            $("#listOfGlobalQuizze li.g-quiz-li").each( function (key, data) {
                var json_order_data = {};
                json_order_data['course_id'] = cid;
                json_order_data['module_id'] = null;
                json_order_data['quiz_id'] = $(this).attr('data-container-id');
                json_order_data['quiz_type'] = 4;
                json_order_data['contain_type'] = 0;
                json_order_data['order_id'] = key;     
                json_order_data['type'] = 'CQ';               
                //json_order_data_all.push(json_order_data);              
                json_order_data_all[key] =json_order_data;              
            });
            console.log(json_order_data_all);
            $.ajax({               
                url: '<?php echo bs('courses/set_orders'); ?>',
                type: "POST",
                data: json_order_data_all,
                success: function (data) {
                    if(data) {
                       //console.log(data);
                    }
                } 
            });
        }else if(type=='L') {
            var json_order_data_all = {}; 
            $("#listOfModulesLesson"+moduleId+" li").each( function (key, data) {
                var json_order_data = {};
                json_order_data['course_id'] = cid;
                json_order_data['module_id'] = moduleId;
                json_order_data['lesson_id'] = $(this).attr('data-lesson-id');
                json_order_data['order_id'] = key;     
                json_order_data['type'] = 'L';  
                json_order_data_all[key] =json_order_data;              
            });
            $.ajax({               
                url: '<?php echo bs('courses/set_orders'); ?>',
                type: "POST",
                data: json_order_data_all,
                success: function (data) {
                    if(data) {
                      // console.log(data);
                    }
                } 
            });
        }else if(type=="LQ") {
            var json_order_data_all = {}; 
            $("#listOfLessonsQuiz"+lessonId+" li.lesson-li").each( function (key, data) {
                var json_order_data = {};
                json_order_data['course_id'] = cid;
                json_order_data['module_id'] = moduleId;
                json_order_data['lesson_id'] = lessonId;
                json_order_data['quiz_id'] = $(this).attr('data-quiz-id');
                json_order_data['order_id'] = key;     
                json_order_data['type'] = 'LQ';  
                json_order_data_all[key] =json_order_data;              
            });
            console.log(json_order_data_all);
            $.ajax({               
                url: '<?php echo bs('courses/set_orders'); ?>',
                type: "POST",
                data: json_order_data_all,
                success: function (data) {
                    if(data) {
                       //console.log(data);
                    }
                } 
            });
        }else if(type=="LA") {
            var json_order_data_all = {}; 
            $("#listOfLessonsQuiz"+lessonId+" li.assessment-li").each( function (key, data) {
                var json_order_data = {};
                json_order_data['course_id'] = cid;
                json_order_data['module_id'] = moduleId;
                json_order_data['lesson_id'] = lessonId;
                json_order_data['quiz_id'] = $(this).attr('data-assessment-id');
                json_order_data['order_id'] = key;     
                json_order_data['type'] = 'LA';  
                json_order_data_all[key] =json_order_data;              
            });
            console.log(json_order_data_all);
            $.ajax({               
                url: '<?php echo bs('courses/set_orders'); ?>',
                type: "POST",
                data: json_order_data_all,
                success: function (data) {
                    if(data) {
                       //console.log(data);
                    }
                } 
            });
        }else if(type=="MQ") {
            var json_order_data_all = {}; 
            $("#listOfModulesQuiz"+moduleId+" li").each( function (key, data) {
                var json_order_data = {};
                json_order_data['course_id'] = cid;
                json_order_data['module_id'] = moduleId;                
                json_order_data['quiz_id'] = $(this).attr('data-mquiz-id');
                json_order_data['order_id'] = key;     
                json_order_data['type'] = 'MQ';  
                json_order_data_all[key] =json_order_data;              
            });
            console.log(json_order_data_all);
            $.ajax({               
                url: '<?php echo bs('courses/set_orders'); ?>',
                type: "POST",
                data: json_order_data_all,
                success: function (data) {
                    if(data) {
                       //console.log(data);
                    }
                } 
            });
        }else if(type=="MA") {
            var json_order_data_all = {}; 
            $("#listOfModulesQuiz"+moduleId+" li.li-ma").each( function (key, data) {
                var json_order_data = {};
                json_order_data['course_id'] = cid;
                json_order_data['module_id'] = moduleId;                
                json_order_data['quiz_id'] = $(this).attr('data-mquiz-id');
                json_order_data['order_id'] = key;     
                json_order_data['type'] = 'MA';  
                json_order_data_all[key] =json_order_data;              
            });
            console.log(json_order_data_all);
            $.ajax({               
                url: '<?php echo bs('courses/set_orders'); ?>',
                type: "POST",
                data: json_order_data_all,
                success: function (data) {
                    if(data) {
                       //console.log(data);
                    }
                } 
            });
        }
    }

    lessonSection();
    quizSection();
    moduleQuizSection();
    $(".lessonsInsetContainer, .quizInsetContainer").sortable({
        placeholder: "ui-state-highlight",
        //items: "> li",
        tolerance:'intersect',
        cursor: 'move',
        handle: '.move',
        receive: function (event, ui) {
            ui.item.remove();
        },
        sort: function( event, ui ) {
            var getSortableItem = $(ui.item).outerHeight();
            $(ui.placeholder).css({"height":getSortableItem})
        }
    }).disableSelection();
    $(".collapse-trigger").unbind();
    $(this).parent().siblings('.collapse-panel').slideToggle(function(){
        if($(this).is(':hidden')){
            $(this).removeClass('open');
            //enableSortable();
        }else{
            $(this).addClass('open');
            //disableSortable()
        }
    });
});


/*********************************************/
//On Scroll Fix Estimate breakdown box

// var fixmeTop = $('#startScrollSection').offset().top - 40;
// var ContactTop = $('#stopScrollSection').offset().top - 380;
// var breakdowncolumn = $('#insetAbsolutSection').offset().top;
// $(window).scroll(function () {
//     var currentScroll = $(window).scrollTop();
//     var ContactTop = $('#stopScrollSection').offset().top - 380;
//     if (currentScroll >= fixmeTop && currentScroll <= ContactTop) {
//         $('#insetAbsolutSection').css({
//             position: 'fixed',
//             top: '70px',
//             width: $("#startScrollSection").width(),
//             zIndex: 1
//         });  
//         $("#insetAbsolutSection .panel.panel-collapse").css({
//             maxHeight: '80vh',
//             overflow: 'auto',
//         });
//     } else {
//         $('#insetAbsolutSection').css({
//             position: 'relative',
//             top: 0
//         });
//         $("#insetAbsolutSection .panel.panel-collapse").css({
//             maxHeight: 'unset',
//             overflow: 'visible',
//         });
//     }
// });

$('#insetAbsolutSection').stickyBox({
    notStickyBreakpoint: 767,
    spaceTop: 70,
    stopper: "#stopScrollSection",
    stopperSpace: 20
})

</script>