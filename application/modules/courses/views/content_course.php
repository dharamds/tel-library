<!--// Start Content Tab  //-->
<div role="tabpanel" class="tab-pane active" id="content">
    <?php
    echo form_open('', [
        'name'    => 'courseContent',
        'id'      => 'courseContent',
        'enctype' => 'multipart/form-data'
    ]);
    ?>
    <?php
    $hccid_data = [
        'type' => 'hidden',
        'id'    => 'course_content_id',
        'name'  => 'course_content_id',
        'value' => isset($courseID) ? $courseID : ''
    ];
    echo form_input($hccid_data);
    ?>

    <div class="row mt-4 mb-3 <?php echo ($courseID > 0) ? '' : 'hide'; ?>">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="course_number" class="control-label"> Course Number * </label>
                <?php
                $course_code = isset($courseData->code) ? $courseData->code : '';
                echo form_input([
                    'id'        => 'course_number',
                    'name'      => 'course_number',
                    'class'     => 'form-control',
                    //'disabled'  => 'disabled',
                    'value'     => $course_code,
                    'onBlur'    => 'checkCourseNumber(this.value)'
                ]);
                ?>
            </div>
            <span id="duplicateCode"></span>
        </div>
    </div>
    <div class="row mt-4 mb-3">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="title" class="control-label"> Title <span class="field-required">*</span> </label>
                <?php
                $course_name = isset($courseData->name) ? $courseData->name : '';
                echo form_input([
                    'id'    => 'title',
                    'name'  => 'title',
                    'value' => $course_name,
                    'class' => 'form-control'
                ]);
                ?>
            </div>
        </div>
    </div>

    <div class="row mt-4 mb-3">
        <div class="col-sm-8">
            <div class="form-group">
                <label for="long_title" class="control-label"> Long Title
                </label>
                <?php
                $course_long_title = isset($courseData->long_title) ? $courseData->long_title : '';
                echo form_input([
                    'id'    => 'long_title',
                    'name'  => 'long_title',
                    'value' => $course_long_title,
                    'class' => 'form-control',
                ]);
                ?>
            </div>
        </div>
    </div>

    <div class="row mb-3">
        <div class="col-sm-8">
            <div class="form-group">
                <label for="description" class="control-label"> Description <span class="field-required">*</span> </label>
                <div>
                    <?php $course_description = isset($courseData->description) ? $courseData->description : ''; ?>
                    <?php
                    echo form_textarea([
                        'name'  => 'description',
                        'id'    => 'description',
                        'value' => $course_description,
                        'class' => 'form-control editor',
                        'rows'  => '5'
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row mb-3">
        <div class="col-sm-8">
            <div class="form-group">
                <label for="introduction" class="control-label">Introduction <span class="field-required">*</span> </label>
                <div>
                    <?php $course_introduction = isset($courseData->introduction) ? $courseData->introduction : ''; ?>
                    <?php
                    echo form_textarea([
                        'name'  => 'introduction',
                        'id'    => 'introduction',
                        'value' => $course_introduction,
                        'class' => 'form-control editor',
                        'rows'  => '5'
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row mb-3">
        <div class="col-sm-8">
            <div class="form-group">
                <label for="outcomes" class="control-label"> Outcomes <span class="field-required">*</span> </label>
                <div>
                    <?php $course_outcomes = isset($courseData->outcomes) ? $courseData->outcomes : ''; ?>
                    <?php
                    echo form_textarea([
                        'name'  => 'outcomes',
                        'id'    => 'outcomes',
                        'value' => $course_outcomes,
                        'class' => 'form-control editor',
                        'rows'  => '5'
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row mb-3">
        <div class="col-sm-8">
            <div class="form-group">
                <label for="getting_started" class="control-label"> Getting Started</label>
                <div>
                    <?php $course_getting_started = isset($courseData->getting_started) ? $courseData->getting_started : ''; ?>
                    <?php
                    echo form_textarea([
                        'name'  => 'getting_started',
                        'id'    => 'getting_started',
                        'value' => $course_getting_started,
                        'class' => 'form-control editor',
                        'rows'  => '5'
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>

    <div class="hr-line mb-5"></div>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-xs-12 p-0 mb-2">
                <label class="control-label">Documents </label>
            </div>
            <div class="form-group col-sm-6 p-0">
                <div class="col-md-6 p-0">
                    <?php
                    echo form_input([
                        'id'          => 'docTitle',
                        'name'        => 'docTitle',
                        'class'       => 'form-control',
                        'placeholder' => 'Enter document title'
                    ]);
                    ?>
                    <label class="error" id="document_error">Please enter title</label>
                </div>
                <!-- <div class="col-md-2">
                    <div class="fileinput fileinput-new col-xs-12 p-0" data-provides="fileinput">
                        <span class="btn btn-primary btn-file btn-primary-default">
                            <span class="fileinput-new pl-5 pr-5">Choose file</span>
                            <span class="fileinput-exists">Choose file</span>
                            <?php
                            echo form_upload([
                                'name'      => 'courseDocs',
                                'id'        => 'courseDocs',
                                'data-type' => 'course_doc',
                            ]);
                            ?>
                        </span>
                        <div id='docValidFormat'></div>
                    </div>
                </div> -->
                <div class="col-md-2">
                    <div class="fileinput fileinput-new col-xs-12 p-0" data-provides="fileinput">
                        <span class="btn btn-primary btn-primary-default btn-file">
                            <span class="fileinput-new">Choose File</span>
                            <span class="fileinput-exists">Change</span>
                            <!--                                                    <input id="uploadImage" name="file" type="file" class="blog_img1 visible1" accept=".mp3,.wav,.ogg">-->
                            <input id="choose_document" name="file" class="blog_img1 visible1">
                        </span>

                        <?php $show1 = !empty($glossaryData->media_file) ? "display:inline-block" : "display:none"; ?>
                        <a class="btn btn-danger fileinput-exists" style="<?php echo $show1 ?>" id="mediaAudio">Remove</a>
                    </div>

                </div>
            </div>

            <div class="form-group col-xs-12 p-0 mt-3">
                <?php if (isset($courseID)) { ?>
                    <label class="control-label">Added Documents</label>
                    <div class="col-12 mr-5 p-0">
                        <div class="list-group list-file-row document_show">
                            <?php foreach ($documentData as $key => $document) : ?>
                                <?php
                                $ext = pathinfo($document->filename, PATHINFO_EXTENSION);
                                $data1 = '';
                                switch ($ext) {
                                    case 'jpg':
                                    case 'jpeg':
                                    case 'png':
                                    case 'gif':
                                        $data1 .= "<div class='col-md-3 ml-4 p-0 mb-3' style='width: 190px;' data-toggle='tooltip' data-placement='top' title='" . $document->title . "'><input type='hidden' class='documents_name' name='documents[]' value='" . $document->filename . "|" . $document->title . "'> <img src='" . base_url($document->filename) . "' style='object-fit: cover;height: 120px;min-width: 170px;width: 100%;' class='img-rounded img-responsive'> <a class='btn btn-delete delete_doc'  data-docname='" . $document->filename . "' data-did='" . $document->id . "' ><i class='flaticon-rubbish-bin-delete-button'></i></a></div>";
                                        break;
                                    case "pdf":
                                        $data1 .= "<div class='col-md-3 ml-4 p-0 mb-3 doc-panel-pdf' style='width: 190px;' data-toggle='tooltip' data-placement='top' title='" . $document->title . "'><input type='hidden' class='documents_name' name='documents[]' value='" . $document->filename . "|" . $document->title . "'><div class='flaticon-pdf f45 text-center pt-4 mt-3' ></div> <a class='btn btn-delete delete_doc'  data-docname='" . $document->filename . "' data-did='" . $document->id . "'><i class='flaticon-rubbish-bin-delete-button'></i></a></div>";
                                        break;
                                    case "csv":
                                        $data1 .= "<div class='col-md-3 ml-4 p-0 mb-3 doc-panel-csv' style='width: 190px;' data-toggle='tooltip' data-placement='top' title='" . $document->title . "'><input type='hidden' class='documents_name' name='documents[]' value='" . $document->filename . "|" . $document->title . "'><div class='flaticon-csv f45 text-center pt-4 mt-3' ></div> <a class='btn btn-delete delete_doc' data-docname='" . $document->filename . "' data-did='" . $document->id . "'><i class='flaticon-rubbish-bin-delete-button'></i></a></div>";
                                        break;
                                    case 'mb':
                                    case 'mp3':
                                        $data1 .= "<div class='col-md-3 ml-4 p-0 mb-3 doc-panel-mp3' style='width: 190px;' data-toggle='tooltip' data-placement='top' title='" . $document->title . "'><input type='hidden' class='documents_name' name='documents[]' value='" . $document->filename . "|" . $document->title . "'><div class='flaticon-mp3 f45 text-center pt-4 mt-3' ></div> <a class='btn btn-delete delete_doc' data-docname='" . $document->filename . "' data-did='" . $document->id . "'><i class='flaticon-rubbish-bin-delete-button'></i></a></div>";
                                        break;
                                    case 'doc':
                                    case 'docx':
                                        $data1 .= "<div class='col-md-3 ml-4 p-0 mb-3 doc-panel-doc' style='width: 190px;' data-toggle='tooltip' data-placement='top' title='" . $document->title . "'><input type='hidden' class='documents_name' name='documents[]' value='" . $document->filename . "|" . $document->title . "'><div class='flaticon-doc f45 text-center pt-4 mt-3' ></div> <a class='btn btn-delete delete_doc' data-docname='" . $document->filename . "' data-did='" . $document->id . "'><i class='flaticon-rubbish-bin-delete-button'></i></a></div>";
                                        break;
                                    case 'xls':
                                    case 'xlsx':
                                        $data1 .= "<div class='col-md-3 ml-4 p-0 mb-3 doc-panel-xls' style='width: 190px;' data-toggle='tooltip' data-placement='top' title='" . $document->title . "'><input type='hidden' class='documents_name' name='documents[]' value='" . $document->filename . "|" . $document->title . "'><div class='flaticon-xls f45 text-center pt-4 mt-3' ></div> <a class='btn btn-delete delete_doc' data-docname='" . $document->filename . "' data-did='" . $document->id . "'><i class='flaticon-rubbish-bin-delete-button'></i></a></div>";
                                        break;
                                    case 'mp4':
                                        $data1 .= "<div class='col-md-3 ml-4 p-0 mb-3 doc-panel-xls' style='width: 190px;' data-toggle='tooltip' data-placement='top' title='" . $document->title . "'><input type='hidden' class='documents_name' name='documents[]' value='" . $document->filename . "|" . $document->title . "'><div class='f45 text-center pt-4 mt-3 media-thumb-img' ><img src='". base_url('/public/assets/img/mp4.png') ."'></img></div> <a class='btn btn-delete delete_doc' data-docname='" . $document->filename . "' data-did='" . $document->id . "'><i class='flaticon-rubbish-bin-delete-button'></i></a></div>";
                                        break;
                                    default:
                                        $data1 .= '<div class="col-md-3 ml-4 p-0 mb-3" style="width: 190px;" data-toggle="tooltip" data-placement="top" title="' . $document->title . '"> <input type="hidden" class="documents_name" name="documents[]" value="' . $document->filename . '|' . $document->title . '"><img src="' . base_url($document->filename) . '" style="object-fit: cover;height: 120px;min-width: 170px;width: 100%;" class="img-rounded img-responsive"> <a class="btn btn-delete delete_doc" data-docname="' . $document->filename . '" data-did="' . $document->id . '"  ><i class="flaticon-rubbish-bin-delete-button"></i></a></div>';
                                }
                                ?>
                                <?php echo $data1 ?>
                                <label class="control-label"><?php  ?></label>


                            <?php endforeach ?>
                            <div id="dFiles"></div>
                        </div>

                        <img id="docLoader" src="<?php echo base_url(); ?>/public/assets/img/spinner.gif" style="display:none;" />

                    </div>
                <?php } else { ?>

                    <label class="control-label"> Added Documents </label>
                    <div class="list-group list-file-row mb-3 document_show">
                        <div id="dFiles"></div>
                        <img id="docLoader" src="<?php echo base_url(); ?>/public/assets/img/spinner.gif" style="display:none;" />
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-xs-12 text-right">
            <button type="submit" id="nextContent" class="btn btn-primary"><?php echo isset($courseID) ? 'Save changes' : 'Save and next' ?></button>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<!--//  End Content Tab  //-->

<script type="text/javascript">
    function media_image_callback(image_name, img_full_url) {

        $('#document_error').hide();
        //$('#dFiles').append('<input type="hidden" class="documents_name" name="documents[]" value=' + image_name + '|' + $('#docTitle').val() + '>');

        var extension = img_full_url.substr((img_full_url.lastIndexOf('.') + 1));
        //alert($('#docTitle').val())
        switch (extension) {
            case 'jpg':
            case 'jpeg':
            case 'png':
            case 'gif':
                $('.document_show').append('<div class="col-md-3 ml-4 p-0 mb-3" style="width: 190px;" data-toggle="tooltip" data-placement="top" title="' + $('#docTitle').val() + '"><input type="hidden" class="documents_name" name="documents[]" value="' + image_name + '|' + $('#docTitle').val() + '"> <img src=' + img_full_url + ' style="object-fit: cover;height: 120px;min-width: 170px;width: 100%;" class="img-rounded img-responsive"> <a class="btn btn-delete delete_data_img"><i class="flaticon-rubbish-bin-delete-button"></i></a></div>').ready(function() {
                    $('[data-toggle="tooltip"]').tooltip();
                    $('.delete_data_img').click(function() {
                        $(this).parent().remove();
                        $(document).find('.tooltip').remove();
                    });
                });
                break; // the alert ended with pdf instead of gif.
            case 'pdf':
                $('.document_show').append("<div class='col-md-3 ml-4 p-0 mb-3 doc-panel-pdf' style='width: 190px;' data-toggle='tooltip' data-placement='top' title='" + $('#docTitle').val() + "'><input type='hidden' class='documents_name' name='documents[]' value='" + image_name + "|" + $('#docTitle').val() + "'><div class='flaticon-pdf f45 text-center pt-4 mt-3' ></div> <a class='btn btn-delete delete_data_img'><i class='flaticon-rubbish-bin-delete-button'></i></a></div>").ready(function() {
                    $('[data-toggle="tooltip"]').tooltip();
                    $('.delete_data_img').click(function() {
                        $(this).parent().remove()
                        $(document).find('.tooltip').remove();
                    });
                });
                break;
            case 'csv':
                $('.document_show').append("<div class='col-md-3 ml-4 p-0 mb-3 doc-panel-csv' style='width: 190px;' data-toggle='tooltip' data-placement='top' title='" + $('#docTitle').val() + "'><input type='hidden' class='documents_name' name='documents[]' value='" + image_name + "|" + $('#docTitle').val() + "'><div class='flaticon-csv f45 text-center pt-4 mt-3' ></div> <a class='btn btn-delete delete_data_img'><i class='flaticon-rubbish-bin-delete-button'></i></a></div>").ready(function() {
                    $('[data-toggle="tooltip"]').tooltip();
                    $('.delete_data_img').click(function() {
                        $(this).parent().remove()
                        $(document).find('.tooltip').remove();
                    });
                });
                break;
            case 'mb':
            case 'mp3':
                $('.document_show').append("<div class='col-md-3 ml-4 p-0 mb-3 doc-panel-mp3' style='width: 190px;' data-toggle='tooltip' data-placement='top' title='" + $('#docTitle').val() + "'><input type='hidden' class='documents_name' name='documents[]' value='" + image_name + "|" + $('#docTitle').val() + "'><div class='flaticon-mp3 f45 text-center pt-4 mt-3' ></div> <a class='btn btn-delete delete_data_img'><i class='flaticon-rubbish-bin-delete-button'></i></a></div>").ready(function() {
                    $('[data-toggle="tooltip"]').tooltip();
                    $('.delete_data_img').click(function() {
                        $(this).parent().remove()
                        $(document).find('.tooltip').remove();
                    });
                });
                break;
            case 'doc':
            case 'docx':
                $('.document_show').append("<div class='col-md-3 ml-4 p-0 mb-3 doc-panel-doc' style='width: 190px;' data-toggle='tooltip' data-placement='top' title='" + $('#docTitle').val() + "'><input type='hidden' class='documents_name' name='documents[]' value='" + image_name + "|" + $('#docTitle').val() + "'><div class='flaticon-doc f45 text-center pt-4 mt-3' ></div> <a class='btn btn-delete delete_data_img'><i class='flaticon-rubbish-bin-delete-button'></i></a></div>").ready(function() {
                    $('[data-toggle="tooltip"]').tooltip();
                    $('.delete_data_img').click(function() {
                        $(this).parent().remove()
                        $(document).find('.tooltip').remove();
                    });
                });
                break;
            case 'xls':
            case 'xlsx':
                $('.document_show').append("<div class='col-md-3 ml-4 p-0 mb-3 doc-panel-xls' style='width: 190px;' data-toggle='tooltip' data-placement='top' title='" + $('#docTitle').val() + "'><input type='hidden' class='documents_name' name='documents[]' value='" + image_name + "|" + $('#docTitle').val() + "'><div class='flaticon-xls f45 text-center pt-4 mt-3' ></div> <a class='btn btn-delete delete_data_img'><i class='flaticon-rubbish-bin-delete-button'></i></a></div>").ready(function() {
                    $('[data-toggle="tooltip"]').tooltip();
                    $('.delete_data_img').click(function() {
                        $(this).parent().remove()
                        $(document).find('.tooltip').remove();
                    });
                });
                break;
            case 'mp4':
                $('.document_show').append("<div class='col-md-3 ml-4 p-0 mb-3 doc-panel-xls' style='width: 190px;' data-toggle='tooltip' data-placement='top' title='" + $('#docTitle').val() + "'><input type='hidden' class='documents_name' name='documents[]' value='" + image_name + "|" + $('#docTitle').val() + "'><div class='f45 text-center mt-3 media-thumb-img' ><img src='<?php bs('/public/assets/img/mp4.png') ?>'></img></div> <a class='btn btn-delete delete_data_img'><i class='flaticon-rubbish-bin-delete-button'></i></a></div>").ready(function() {
                    $('[data-toggle="tooltip"]').tooltip();
                    $('.delete_data_img').click(function() {
                        $(this).parent().remove()
                        $(document).find('.tooltip').remove();
                    });
                });
                break;
            default:
                //console.log('who knows');
        }
        $('#docTitle').val('');
    }

    $("#choose_document").click(function() {
        if ($('#docTitle').val() == '') {
            $('#document_error').show();
        } else {
            CommanJS.choose_media_image_file('media_image_callback');
        }
    });
    $(document).ready(function() {
        
        $('[data-toggle="tooltip"]').tooltip();
        $('#document_error').hide();
        $('.delete_data_img').click(function() {
            $(this).parent().remove()
            CommanJS.getDisplayMessgae(200, 'Deleted successfully.');
        });
        // Ck editor
        var config = { height: 120, allowedContent :true, toolbar: 'short' };
        $('.editor').each(function(e) {
            CKEDITOR.replace(this.id, config);
        });

        // Course content validation
        $("#courseContent").validate({
            
            ignore: [],
            rules: {
                course_number: {
                    required: function(textarea) {
                        if ($('#course_content_id').val() > 0)
                            return true;
                    }
                },
                title: {
                    required: true,
                    remote:{
                        url: "<?php echo bs(); ?>courses/getCheckExist",
                        type: 'POST',
                        data: {'recordID':$('#course_content_id').val()}
                    }
                },
                description: {
                    required: function(textarea) {
                        CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                        return editorcontent.length === 0;
                    },
                    //noSpace: true
                },
                introduction: {
                    required: function(textarea) {
                        CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                        return editorcontent.length === 0;
                    },
                    //noSpace: true
                },
                outcomes: {
                    required: function(textarea) {
                        CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                        return editorcontent.length === 0;
                    },
                    //noSpace: true
                },
                /*getting_started: {
                    required: function (textarea) {
                        CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                        return editorcontent.length === 0;
                    },
                    //noSpace: true
                },*/
            },
            messages: {
                course_number: {
                    required: "Please enter unique course code"
                },
                title: {
                    required: "Please enter course title",
                    remote: "Course name is already exist"
                },
                description: {
                    required: "Please enter course description"
                },
                introduction: {
                    required: "Please enter course introduction"
                },
                outcomes: {
                    required: "Please enter course outcomes"
                },
                //getting_started: {required: "Please enter course getting started content"},
            },
            submitHandler: function(form) {
                $('.editor').each(function(e) {
                    CKEDITOR.instances[this.id].updateElement(); // update textarea
                });

                $.ajax({
                    url: "<?php echo bs(); ?>courses/addCourseContent",
                    type: "POST",
                    dataType: 'json',
                    data: $("#courseContent").serialize(),
                    success: function(data) {
                        console.log(data);
                        if (data.code == 200) {
                            $("#coursePanelTab li").removeClass("active");
                            $("#tab-metadata").addClass('active');
                            $("#tab-metadata").attr('data-id', data.id);
                            getViewTemplate('metadata', data.id);
                            getMetadata(data.id);
                            CommanJS.getDisplayMessgae(data.code, data.success);
                        } else if (data.code == 400) {
                            CommanJS.getDisplayMessgae(data.code, data.error);
                        }
                    }
                });
            },
            errorPlacement: function(error, $elem) {
                console.log(error);
                if ($elem.is('textarea')) {
                    $elem.insertAfter($elem.next('div'));
                }
                error.insertAfter($elem);
            },
        });

        $.validator.addMethod("noSpace", function(value, element, param) {
            return value.match(/^(?=.*\S).+$/);
        }, "No space please and don't leave it empty");
    }); // document read end
</script>