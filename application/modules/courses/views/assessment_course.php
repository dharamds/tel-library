<div role="tabpanel" class="tab-pane" id="assessment">
	<?php
	    echo form_open('', [
	        'name'    => 'courseAssessment',
	        'id'      => 'courseAssessment',
	        'enctype' => 'multipart/form-data'
	    ]);
    ?>
 
 	<?php
	    $hidden_courseid = [
					        'type' => 'hidden',
					        'id'    => 'ass_course_id',
					        'name'  => 'ass_course_id',
					        'value' => isset($courseID) ? $courseID : ''
					    ];
	    echo form_input($hidden_courseid);
    ?>

    <?php
        $hidden_assmtid = [
                            'type' => 'hidden',
                            'id'    => 'assessment_id',
                            'name'  => 'assessment_id',
                            //'value' => 
                        ];
        echo form_input($hidden_assmtid);
    ?>
	<div class="row mt-4 mb-3">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="title" class="control-label">Assessment Title <span class="field-required">*</span> </label>
                <?php
	                //$course_name = isset($courseData->name) ? $courseData->name : '';
	                echo form_input([
	                    'id'    => 'title',
	                    'name'  => 'title',
	                    //'value' => $course_name,
	                    'class' => 'form-control'
	                ]);
                ?>
            </div>
        </div>
    </div>

    <div class="row mt-4 mb-3">
        <div class="col-sm-8">
            <div class="form-group">
                <label for="long_title" class="control-label">Assessment Long Title
                </label>
                <?php
                	//$course_long_title = isset($courseData->long_title) ? $courseData->long_title : '';
                	echo form_input([
		                    'id'    => 'long_title',
		                    'name'  => 'long_title',
		                    //'value' => $course_long_title,
		                    'class' => 'form-control',
		                ]);
                ?>
            </div>
        </div>
    </div>
    
    <div class="row mb-3">
        <div class="col-sm-8">
            <div class="form-group">
                <label for="instruction" class="control-label">Assessment Instructions <span class="field-required">*</span> </label>
                <div>
                    <?php //$course_introduction = isset($courseData->introduction) ? $courseData->introduction : ''; ?>
                    <?php
	                    echo form_textarea([
	                        'name'  => 'instruction',
	                        'id'    => 'instruction',
	                        //'value' => $course_introduction,
	                        'class' => 'form-control editor',
	                        'rows'  => '5'
	                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row mb-3">
        <div class="col-sm-8">
            <div class="form-group">
                <label for="feedback" class="control-label">Assessment feedback <span class="field-required">*</span> </label>
                <div>
                    <?php //$course_introduction = isset($courseData->introduction) ? $courseData->introduction : ''; ?>
                    <?php
	                    echo form_textarea([
	                        'name'  => 'feedback',
	                        'id'    => 'feedback',
	                        //'value' => $course_introduction,
	                        'class' => 'form-control editor',
	                        'rows'  => '5'
	                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
	<div class="col-xs-12 mt-3 mb-3 p-0 pb-2"> <div class="mb-4"></div> </div>
    <div class="col-xs-12 text-right">
        <button class="btn btn-primary" type="submit">Save</button> <button class="btn btn-primary" type="button" id="nextAsseesment">Next</button>
    </div>

    <?php echo form_close(); ?>

    <?php  if(isset($courseAssessments) && count($courseAssessments) > 0) { ?>
    <div class="col-xs-12 mt-3 mb-3 p-0 pb-2"> <div class="mb-4"></div> </div>
    
    <div class="col-xs-12 mt-3 mb-3 p-0 pb-2"> <div class="mb-2"></div> </div>
    <div class="panel-body pl-0 pr-0">
        <div class="row m-0">
            <div class="col-md-12">
                <table id="courseAssessmentList" class="table table-bordered table-striped table-hover" cellspacing="0">
                    <thead>
                        <tr>                            
                            <th style="min-width: 20px;">#</th>
                            <th style="min-width: 100px;">Assessment title</th>                            
                        </tr>
                    </thead>
                    <tbody>
                    	<?php $srno=1;
                    	foreach($courseAssessments as $key => $assessment) { ?>
	                    	<tr>
	                    		<td><?=$srno;?></td>
	                    		<td><a href="<?php echo base_url('assessments/edit/')?><?=base64_encode($assessment->id);?>"><?php echo $assessment->title;?></a></td>
	                    	</tr>
                    	<?php $srno++;
                		} //end foreach ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- <div class="col-xs-12 mt-3 mb-3 p-0 pb-2"> <div class="hr-line mb-4"></div> </div>
    <div class="col-xs-12 text-right">
        <button class="btn btn-primary" type="button" id="nextAsseesment">Next</button>
    </div> -->
	<?php } //end if($courseAssessments)?>

</div>

<script type="text/javascript">
	$(document).ready(function() {
        // Ck editor
        var config = { height: 120, allowedContent :true, toolbar: 'short' };
        $('.editor').each(function(e) {
            CKEDITOR.replace(this.id, config);
        });

        $("#courseAssessment").validate({
        	ignore: [],
            rules: {
            	title: {
                    required: true
                },
                instruction: {
                    required: function(textarea) {
                        CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                        return editorcontent.length === 0;
                    },
                }
            },
            messages: {
                title: {
                    required: "Please enter assessment title"
                },                
                instruction: {
                    required: "Please enter assessment instruction"
                }
               
            },
            submitHandler: function(form) {
                $.ajax({
                    url: "<?php echo bs(); ?>courses/addCourseAssessment",
                    type: "POST",
                    dataType: 'json',
                    data: $("#courseAssessment").serialize(),
                    success: function(data) {
                        console.log(data);
                        if (data.code == 200) {                      
                        	$("#coursePanelTab li").removeClass("active");
                            $("#tab-assessment").addClass('active');
                            $("#tab-assessment").attr('data-id', data.id);      
                            getViewTemplate('assessment', data.id);                            
                            CommanJS.getDisplayMessgae(data.code, data.success);
                        } else if (data.code == 400) {
                            CommanJS.getDisplayMessgae(data.code, data.error);
                        }
                    }
                });
            },
            errorPlacement: function(error, $elem) {
                console.log(error);
                if ($elem.is('textarea')) {
                    $elem.insertAfter($elem.next('div'));
                }
                error.insertAfter($elem);
            },
        });



    }); // document ready ends

	$('#nextAsseesment').on('click', function () { 
		var course_id = $("#ass_course_id").val();
		$("#coursePanelTab li").removeClass("active");
        $("#tab-builder").addClass('active');
        $("#tab-builder").attr('data-id', course_id);
        getViewTemplate('builder', course_id);
	});
</script>