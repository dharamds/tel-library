
         <div class="container-fluid">
            <div class="tab-pane active" id="tab-about">
               <div class="panel panel-default panel-grid">
                  <div class="panel-heading brd-0 p-0 pt-2"></div>
                  <div class="panel-body">
                     <div class="about-area">
                        <div class="table-responsive">
                           <table class="table about-table">
                              <tbody>
                                 <tr>
                                    <th>Code</th>
                                    <td width="10%"></td>
                                    <td><?php echo $coursesData->code;?></td>
                                 </tr>
                                 <tr>
                                    <th>Title</th>
                                    <td width="10%"></td>
                                    <td><?php echo $coursesData->name;?></td>
                                 </tr>
                                 <tr>
                                    <th>Image</th>
                                    <td width="10%"></td>
                                    <td> <img src="<?php bs('uploads/course_images/')?><?php echo $coursesData->course_img;?>" alt="" width="120" /> </td>
                                 </tr>
                                 <tr>
                                    <th>Description</th>
                                    <td width="10%"></td>
                                    <td><?php echo $coursesData->description;?></td>
                                 </tr>
                                 <tr>
                                    <th>Introduction</th>
                                    <td width="10%"></td>
                                    <td><?php echo $coursesData->introduction;?></td>
                                 </tr>                                 
                                 <tr>
                                    <th>Learning outcome</th>
                                    <td width="10%"></td>
                                    <td><?php echo $coursesData->outcomes;?></td>
                                 </tr>
                                 <tr>
                                    <th>Getting started</th>
                                    <td width="10%"></td>
                                    <td><?php echo $coursesData->getting_started;?></td>
                                 </tr>                                 
                                 <tr>
                                    <th>Course availability</th>
                                    <td width="10%"></td>
                                    <td><?php echo ($coursesData->course_availability==1) ? 'Available' : 'Hidden'; ?></td>
                                 </tr>
                                 <!-- <tr>
                                    <th>Expiry status</th>
                                    <td width="10%"></td>
                                    <td><?php echo $coursesData->exp_status;?></td>
                                 </tr> -->
                                 <?php if($coursesData->exp_status=='Y') {?>
                                    <tr>
                                       <th>Course expiry</th>
                                       <td width="10%"></td>
                                       <td><?php echo date('m/d/Y', strtotime($coursesData->course_expiry));?></td>
                                    </tr>
                                 <?php } ?>
                                 <tr>
                                    <th>Glossary Display</th>
                                    <td width="10%"></td>
                                    <td><?php echo ($coursesData->show_glossary == 1) ? "Available" : (($coursesData->show_glossary == 2)  ? "Only for first instance" : "Hidden");?></td>
                                 </tr>
                                 <tr>
                                    <th>Documents</th>
                                    <td width="10%"></td>
                                    <td> 
                                       <?php  //pr($documents_data);
                                       $doc_type = ['1' => 'Syllabus', '2' => 'Assignments'];
                                       foreach ($documentsData as $key => $doc) { ?>
                                          <a href="<?php bs('uploads/modules_docs/')?><?php echo $doc->filename;?>" target="_new" title='Download'><span><?php echo $doc->title;?></span>
                                          </a><br>
                                       <?php                                         
                                       }
                                       ?>
                                    </td>
                                 </tr>
                                 <tr>
                                    <th>Created by</th>
                                    <td width="10%"></td>
                                    <td><?php 
                                       $authorData = $this->ion_auth->user($coursesData->created_by)->row(); 
                                       echo ucfirst($authorData->first_name); echo " ";  
                                       echo ucfirst($authorData->last_name);
                                       ?></td>
                                 </tr>
                                 <tr>
                                    <th>Created on</th>
                                    <td width="10%"></td>
                                    <td><?php echo date('m/d/Y',strtotime($coursesData->created)); ?></td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
