<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Courses extends MY_Controller
{

    public function __construct()
    {

        parent::__construct();
        //ini_set('display_errors', '2');
        //Do your magic here
        $this->load->module('template');
        $this->load->model(['common_model', 'Courses_modal']);
        $this->load->helper(['html', 'form', 'language']);
        $this->load->library('form_validation');

        if (!$this->ion_auth->logged_in()) :
            redirect('users/auth', 'refresh');
        endif;

        // get controller permissions		
        if ($this->current_user_permissions = $this->get_permissions()) {
            $this->add_permission = $this->current_user_permissions->add_per;
            $this->edit_permission = $this->current_user_permissions->edit_per;
            $this->delete_permission = $this->current_user_permissions->delete_per;
            $this->view_permission = $this->current_user_permissions->view_per;
            $this->list_permission = $this->current_user_permissions->list_per;
        }
        $this->container_id = get_container_id();
    }

    /**
     * setup method
     * @description this function use to create container
     * @return void
     */
    public function index()
    {
        $data['page'] = "courses/list";
        if (!$this->list_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect('users/auth', 'refresh');
        endif;
        $data['permissions'] = $this->current_user_permissions;

        $data['breadcrumb'][] = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][] = ['title' => 'Courses', 'link' => '', 'class' => 'active'];
        $this->template->template_view($data);
    }

    /**
     * getLists method 
     * @description this function called via ajax request, use to display list of course
     * @return void
     */
    public function getLists()
    {
        $coursesData = $this->Courses_modal->getRows($_POST);
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $coursesData['total'],
            "recordsFiltered" => $coursesData['total'],
            "data" => $coursesData['result'],
        );
        // Output to JSON format
        echo json_encode($output);
    }


    /* Load view*/
    public function load_view($viewType = '', $courseID = '')
    {
        //echo $courseID; exit;
        switch ($viewType) {
            case 'content':
                //$template_path = "quiz/add-questions";
                $template_path = "courses/content_course";
                if ($courseID > 0) {
                    $data['courseData'] = $this->common_model->getDataById('courses', '*', ['id' => $courseID]);
                    $data['documentData'] = $this->common_model->getAllData('documents', '*', '', ['reference_id' => $courseID, 'reference_type' => 1, 'delete_status' => 1]);
                    //echo $this->db->last_query();
                    $data['breadcrumb'][] = ['title' => 'Edit', 'link' => '', 'class' => 'active'];
                    $data['categories'] = $this->common_model->selectCategoryMetadata('categories', 1); // 1=system

                } else {
                    $data = [];
                }
                break;
            case 'metadata':
                $template_path = "courses/metadata_course";
                $certificates = $this->common_model->getAllData('certificates', ['id', 'name'], '', ['status' => 1]);
                $data['certificateData'] = ['' => 'Select certificate'];
                foreach ($certificates as $key => $certificate) {
                    $data['certificateData'][$certificate->id] = $certificate->name;
                }

                if ($courseID > 0) {
                    $data['courseData'] = $this->common_model->getDataById('courses', '*', ['id' => $courseID]);
                } else {
                    $data['details'] = [];
                }
                break;
            case 'assessment':
                $template_path = "courses/assessment_course";

                if ($courseID > 0) {
                    $data['courseAssessments'] = $this->common_model->getAllData('assessments', ['id', 'title'], '', ['course_id' => $courseID, 'delete_status' => 1]);
                } else {
                    $data['details'] = [];
                }
                break;

            case 'builder':
                $template_path = "courses/builder_course";
                if ($courseID > 0) {
                    $data['selectedModules'] = $this->common_model->Djoin('modules.id, modules.name, course_contain.order_id', 'modules', 'course_contain', 'modules.id = course_contain.module_id', '', '', 'course_contain.course_id=' . $courseID . ' AND course_contain.quiz_type=0 AND modules.delete_status=1 AND modules.status=1', 'course_contain.order_id ASC');
                    //echo $this->db->last_query();

                    $data['selectedLesson'] = $this->common_model->Djoin('lessons.id, lessons.name, module_contain.order_id, module_contain.module_id', 'lessons', 'module_contain', 'lessons.id = module_contain.lesson_id', '', '', 'module_contain.course_id=' . $courseID . ' AND lessons.delete_status=1 AND lessons.status=1  AND module_contain.quiz_type=0 AND module_contain.quiz_id=' . NULL . ' ', 'module_contain.module_id, module_contain.order_id ASC');

                    $data['selectedQuizzes'] = $this->common_model->Djoin('quizzes.id, quizzes.title, quizzes.type, lesson_contain.order_id, lesson_contain.module_id, lesson_contain.lesson_id', 'quizzes', 'lesson_contain', 'quizzes.id = lesson_contain.quiz_id', '', '', 'lesson_contain.course_id=' . $courseID . ' AND quizzes.delete_status=1 AND quizzes.status=1 AND lesson_contain.quiz_type=0 AND lesson_contain.contain_type=0', 'lesson_contain.module_id, lesson_contain.lesson_id, lesson_contain.order_id ASC');
                    //echo $this->db->last_query();


                    $data['selectedLessonAssessments'] = $this->common_model->Djoin('assessments.id, assessments.title, lesson_contain.order_id, lesson_contain.module_id, lesson_contain.lesson_id', 'assessments', 'lesson_contain', 'assessments.id = lesson_contain .quiz_id', '', '', 'lesson_contain.course_id=' . $courseID . ' AND assessments.delete_status=1 AND assessments.status=1 AND lesson_contain.quiz_type=2 AND lesson_contain.contain_type=1', 'lesson_contain.module_id, lesson_contain.lesson_id, lesson_contain.order_id ASC');

                    # Selected moduel quizzes
                    $data['selectedModuleQuizzes'] = $this->common_model->Djoin('quizzes.id, quizzes.title, module_contain.order_id, module_contain.module_id', 'quizzes', 'module_contain', 'quizzes.id = module_contain.quiz_id', '', '', 'module_contain.course_id=' . $courseID . ' AND module_contain.quiz_type=3  AND module_contain.contain_type=0 AND quizzes.delete_status=1 AND quizzes.status=1', 'module_contain.module_id, module_contain.order_id ASC');

                    # Selected moduel assessments
                    $data['selectedModuleAssessments'] = $this->common_model->Djoin('assessments.id, assessments.title, module_contain.order_id, module_contain.module_id', 'assessments', 'module_contain', 'assessments.id = module_contain.quiz_id', '', '', 'module_contain.course_id=' . $courseID . ' AND module_contain.quiz_type=3 AND module_contain.contain_type=1 AND assessments.delete_status=1 AND assessments.status=1', 'module_contain.module_id, module_contain.order_id ASC');


                    $data['selectedCourseAssessments'] = $this->common_model->Djoin('assessments.id, assessments.title, course_contain.order_id', 'assessments', 'course_contain', 'assessments.id = course_contain .quiz_id', '', '', 'course_contain.course_id=' . $courseID . ' AND course_contain.quiz_type=4 AND course_contain.contain_type=1 AND assessments.delete_status=1 AND assessments.status=1', 'course_contain.course_id,course_contain.order_id ASC');

                    # Fetch all the quiz from selected category and tags
                    $query_assessments = $this->db->query('SELECT `assessments`.`id`, `assessments`.`title`  FROM `assessments` WHERE `course_id` = ' . $courseID . ' AND `delete_status` = 1 AND `status` = 1 ORDER BY `assessments`.`title` ASC');
                    $assessments = $query_assessments->result_array();
                    # checking assemments in course contain
                    $this->db->select('quiz_id');
                    $this->db->from('course_contain');
                    $this->db->where(['course_id' => $courseID, 'quiz_type' => 4, 'contain_type' => 1]);
                    $sel_cassess = array_column($this->db->get()->result_array(), 'quiz_id');

                    foreach ($assessments as $key => $value) {
                        if (in_array($value['id'], $sel_cassess) && $assessments[$key]['strike'] == 0) {
                            $assessments[$key]['strike'] = 1;
                        }
                    }
                    # checking assemments in course lesson
                    $this->db->select('quiz_id');
                    $this->db->from('lesson_contain');
                    $this->db->where(['course_id' => $courseID, 'quiz_type' => 2, 'contain_type' => 1]);
                    $sel_cassess = array_column($this->db->get()->result_array(), 'quiz_id');
                    foreach ($assessments as $key => $value) {
                        if (in_array($value['id'], $sel_cassess) && $assessments[$key]['strike'] == 0) {
                            $assessments[$key]['strike'] = 1;
                        }
                    }

                    # checking assemments in module lesson
                    $this->db->select('quiz_id');
                    $this->db->from('module_contain');
                    $this->db->where(['course_id' => $courseID, 'quiz_type' => 3, 'contain_type' => 1]);
                    $sel_cassess = array_column($this->db->get()->result_array(), 'quiz_id');
                    foreach ($assessments as $key => $value) {
                        if (in_array($value['id'], $sel_cassess) && $assessments[$key]['strike'] == 0) {
                            $assessments[$key]['strike'] = 1;
                        }
                    }

                    $data['courseAssessments'] = $assessments;

                    $data['selectedCourseQuizes'] = $this->common_model->Djoin('quizzes.id, quizzes.title, course_contain.order_id', 'quizzes', 'course_contain', 'quizzes.id = course_contain .quiz_id', '', '', 'course_contain.course_id=' . $courseID . ' AND quizzes.delete_status = 1 AND quizzes.status=1 AND course_contain.quiz_type=4 AND course_contain.contain_type=0', 'course_contain.course_id,course_contain.order_id ASC');

                    //echo $this->db->last_query(); 
                } else {
                    $data['sections'] = [];
                    // $data['sections_count'] = 1;
                }
                break;
            case 'gradebook':
                $template_path = "courses/gradebook_course";
                if ($courseID > 0) {
                    $data['courseData'] = $this->common_model->getDataById('courses', ['id', 'name'], ['id' => $courseID]);
                    $data['gradebookData'] = $this->common_model->getAllData('courses_gradebook', ['id', 'section_title', 'section_type', 'section_description', 'weight'], '', ['course_id' => $courseID], 'section_type ASC');
                    # CYK quizzes = 1;

                    $cyk_gradebook = $this->db->query('SELECT `quizzes`.`id` , `quizzes`.`title`, `quizzes`.`long_title` , `quizzes`.`type` FROM `module_contain` JOIN `lessons` ON `module_contain`.`lesson_id` = `lessons`.`id` JOIN `quizzes` ON `quizzes`.`id` = `lessons`.`quiz_id` WHERE `module_contain`.`course_id` = ' . $courseID);

                    $data['gradebooks'][1] = $cyk_gradebook->result();

                    //pr($data['gradebooks'][1]);
                    $gradebook_data = $this->db->query('SELECT `quizzes`.`id`, `quizzes`.`title`, `quizzes`.`long_title`, `quizzes`.`type` FROM `quizzes` JOIN `lesson_contain` ON `quizzes`.`id` = `lesson_contain`.`quiz_id` WHERE `lesson_contain`.`course_id` = ' . $courseID . ' UNION SELECT `quizzes`.`id`, `quizzes`.`title`, `quizzes`.`long_title`, `quizzes`.`type` FROM `quizzes` JOIN `module_contain` ON `quizzes`.`id` = `module_contain`.`quiz_id` WHERE `module_contain`.`course_id` = ' . $courseID . ' UNION SELECT `quizzes`.`id`, `quizzes`.`title`, `quizzes`.`long_title`, `quizzes`.`type` FROM `quizzes` JOIN `course_contain` ON `quizzes`.`id` = `course_contain`.`quiz_id` WHERE `course_contain`.`course_id` = ' . $courseID);
                    $gradebook_quiz_data = $gradebook_data->result();

                    foreach ($gradebook_quiz_data as $gb_qz_key => $gb_qz_data) {
                        if ($gb_qz_data->type == 2)        // Module quizzes
                            $data['gradebooks'][2][] = $gb_qz_data;
                        if ($gb_qz_data->type == 4)        // Mid term
                            $data['gradebooks'][4][] = $gb_qz_data;
                        if ($gb_qz_data->type == 5)        // Final term
                            $data['gradebooks'][5][] = $gb_qz_data;
                    }

                    $gradebook_ass_data =  $gradebook_data = $this->db->query('SELECT `assessments`.`id`, `assessments`.`title`, `assessments`.`long_title` FROM `assessments` JOIN `lesson_contain` ON `assessments`.`id` = `lesson_contain`.`quiz_id` WHERE `lesson_contain`.`course_id` = ' . $courseID . ' AND `lesson_contain`.`contain_type` = 1 UNION SELECT `assessments`.`id`, `assessments`.`title`, `assessments`.`long_title` FROM `assessments` JOIN `module_contain` ON `assessments`.`id` = `module_contain`.`quiz_id` WHERE `module_contain`.`course_id` = ' . $courseID . ' AND `module_contain`.`contain_type` = 1 UNION SELECT `assessments`.`id`, `assessments`.`title`, `assessments`.`long_title` FROM `assessments` JOIN `course_contain` ON `assessments`.`id` = `course_contain`.`quiz_id` WHERE `course_contain`.`course_id` = ' . $courseID . '  AND `course_contain`.`contain_type` = 1 ');

                    $gradebook_assmnts_data = $gradebook_ass_data->result();
                    $data['gradebooks'][3] = $gradebook_assmnts_data;
                } else {
                    $data['courseData'] = [];
                }
                $data['sectionType'] = [1 => 'CYK', 2 => 'Module', 3 => 'Evidence', 4 => 'Mid Term', 5 => 'Final'];
                break;
        }
        $data['courseID'] = $courseID;
        echo $this->load->view($template_path, $data, true);
        exit;
    }

    /**
     * add_new method
     * @description this method is use to insert site creatation data in databse
     * @param string, numbers
     * @return void
     */
    public function add($id = NULL)
    {
        //Permission to access
        if ($id && !$this->edit_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect($_SERVER['HTTP_REFERER'], 'refresh');
        elseif (!$this->add_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect($_SERVER['HTTP_REFERER'], 'refresh');
        endif;

        $data['breadcrumb'][] = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][] = ['title' => 'Courses', 'link' => base_url('courses'), 'class' => ''];

        if ($id != null) {
            $data['courseData']     = $this->common_model->getDataById('courses', '*', ['id' => $id]);
            $data['documentData']   = $this->common_model->getAllData('documents', '*', '', ['reference_id' => $id, 'reference_type' => 1, 'delete_status' => 1]);
            $data['breadcrumb'][]   = ['title' => 'Edit', 'link' => '', 'class' => 'active'];
            $data['categories']     = $this->common_model->selectCategoryMetadata('categories', 1); // 1=system

            $data['selectedModules'] = $this->common_model->Djoin('modules.id, modules.name, course_contain.order_id', 'modules', 'course_contain', 'modules.id = course_contain.module_id', '', '', 'course_contain.course_id=' . $id . ' ', 'course_contain.order_id ASC');
            $data['selectedLesson'] = $this->common_model->Djoin('lessons.id, lessons.name, module_contain.order_id, module_contain.module_id', 'lessons', 'module_contain', 'lessons.id = module_contain.lesson_id', '', '', 'module_contain.course_id=' . $id . ' ', 'module_contain.module_id, module_contain.order_id ASC');
            $data['selectedQuizzes'] = $this->common_model->Djoin('quizzes.id, quizzes.title, lesson_contain .order_id, lesson_contain.module_id, lesson_contain.lesson_id', 'quizzes', 'lesson_contain', 'quizzes.id = lesson_contain.quiz_id', '', '', 'lesson_contain.course_id=' . $id . ' ', 'lesson_contain.module_id, lesson_contain.lesson_id, lesson_contain.order_id ASC');
        } else {
            $data['breadcrumb'][] = ['title' => 'Add', 'link' => '', 'class' => 'active'];
        }

        $data['page'] = "courses/add";
        $this->template->template_view($data);
    }


    public function edit($id = NULL)
    {
        //echo $id; exit;
        //Permission to access
        if ($id && !$this->edit_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect($_SERVER['HTTP_REFERER'], 'refresh');
        elseif (!$this->add_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect($_SERVER['HTTP_REFERER'], 'refresh');
        endif;

        $data['breadcrumb'][] = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][] = ['title' => 'Courses', 'link' => base_url('courses'), 'class' => ''];

        $data['breadcrumb'][] = ['title' => 'Edit', 'link' => '', 'class' => 'active'];
        $data['courseID'] = $id;

        $data['page'] = "courses/edit";
        $this->template->template_view($data);
    }

    /**
     * view_coursemodule method
     * @description this function use to view complete course module details
     * @return void
     */
    public function detail()
    {
        ini_set('display_errors', '0');
        // check method permission
        if (!$this->view_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect($_SERVER['HTTP_REFERER'], 'refresh');
        endif;

        $course_id = $this->uri->segment(3);
        if (isset($course_id)) {
            $data['coursesData'] = $this->common_model->getDataById('courses', '*', ['id' => $course_id]);

            $data['documentsData'] = $this->common_model->getAllData('documents', '*', '', ['reference_id' => $course_id, 'reference_type' => 1]);

            $data['page'] = "courses/detail";
            $data['breadcrumb'][] = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
            $data['breadcrumb'][] = ['title' => 'Courses', 'link' => base_url('courses'), 'class' => ''];
            $data['breadcrumb'][] = ['title' => 'View', 'link' => '', 'class' => 'active'];
            $this->template->template_view($data);
        }
    }

    /**
     * delete method
     * @description this function use to delete complete course
     * @return void
     */
    function delete()
    {
        // check permissions
        if (!$this->delete_permission && !$this->ion_auth->is_admin()) :
            echo json_encode(array("msg" => $this->lang->line('access_denied')));
            exit;
        endif;

        $id = $this->uri->segment(3);
        $data = ['delete_status' => 0];
        if ($id) {

            clean_cache('api', 'courses_');
            $this->common_model->UpdateDB('courses', ['id' => $id], $data);
            $this->common_model->UpdateDB('documents', ['reference_id' => $id, 'reference_type' => 1,], $data);
            clean_cache_by_key('api', 'tel_getAllCourses'); // for delete all courses list cache
            $this->session->set_flashdata('success', $this->ion_auth->messages());
            echo json_encode(array("msg" => "deleted"));
            exit;
        } else {
            set_flashdata('error', 'Unable to delete courses, try again');
            redirect('courses/', 'refresh');
        }
    }

    /* Ajax functions */
    /**
     * Manage upload image and docs
     *
     * @return Response
     */

    public function update_status($id, $action)
    {

        $status = ($action == 'activate') ? 1 : 0;
        $data = array('status' => $status);

        if ($this->container_id > 0) {
            $this->common_model->UpdateDB('container_courses', ['course_id' => $id, 'container_id' => $this->container_id], ['status' => $action]);
        } else {
            $this->Courses_modal->update_status($id, $data);
        }
        # clear cache when course are updated by status
        clean_cache_by_key('api', 'tel_getAllCourses'); // for delete all courses list cache
        clean_cache('api', 'courses_');
        echo json_encode(['msg' => "Updated"]);
        exit;
    }

    public function upload_docs_image()
    {
        $data = [];
        $file_element_name = 'file';
        if ($_REQUEST['elementType'] == 'course_doc') {
            $config['upload_path'] = './uploads/course_docs/';
            $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|ppt|pptx|odt';

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload($file_element_name)) {
                $status = 'error';
                $data['error'] = $this->upload->display_errors('', '');
            } else {
                $fileData = $this->upload->data();
                $data['fileName'] = $fileData['file_name'];
                $data['element'] = $_REQUEST['elementId'];
                $data['fileTitle'] = $_REQUEST['fileTitle'];
            }
        } else {

            $config['upload_path'] = './uploads/course_images/';
            $config['allowed_types'] = 'gif|jpg|png';

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload($file_element_name)) {
                $status = 'error';
                $data['error'] = $this->upload->display_errors('', '');
            } else {
                $fileData = $this->upload->data();
                $this->resizeImage($fileData['file_name']);
                $data['filePath'] = base_url() . 'uploads/course_images/thumbnails/';
                $data['fileName'] = $fileData['file_name'];
                $img_data = ['course_img' => $fileData['file_name']];
                $cid = $this->input->post('crs_up_img_meta_id');
                $this->common_model->UpdateDB('courses', ['id' => $cid], $img_data);
            }
        }
        echo json_encode($data);
        exit;
    }

    /**
     * Manage upload Image resize
     *
     * @return Response
     */
    public function resizeImage($filename)
    {
        $source_path = './uploads/course_images/' . $filename;
        $target_path = './uploads/course_images/thumbnails/';
        $config_manip = [
            'image_library' => 'gd2',
            'source_image' => $source_path,
            'new_image' => $target_path,
            'maintain_ratio' => TRUE,
            'create_thumb' => TRUE,
            //'thumb_marker' => '_thumb',
            'width' => 150,
            'height' => 150
        ];
        $this->load->library('image_lib', $config_manip);
        if (!$this->image_lib->resize()) {
            echo $this->image_lib->display_errors();
        }
        $this->image_lib->clear();
    }

    # Remove course docuemnt from server
    public function remove_course_doc()
    {

        if ((int) $this->input->post('fileID')) {
            $data = ['delete_status' => 0];
            $id = $this->input->post('fileID');
            $this->common_model->UpdateDB('documents', ['id' => $id, 'reference_type' => 1], $data);
        }
        $fileName = $this->input->post('file');
        if (unlink(APPPATH . '../uploads/course_docs/' . $fileName))
            echo 1;
        else
            echo 0;
    }

    # Remove course image from server
    public function remove_course_img()
    {
        $course_id = $this->input->post('course_id');
        $postData = array('course_img ' => '');
        $this->common_model->UpdateDB('courses', array("id" => $course_id), $postData);

        $fileName = $_POST['file'];
        if (unlink(APPPATH . '../uploads/course_images/' . $fileName)) {
            unlink(APPPATH . '../uploads/course_images/thumbnails/' . $fileName);
            echo 1;
        } else
            echo 0;
    }

    # Add/Edit course content
    public function addCourseContent()
    {
        //pr($_POST); exit;
        # Course content
        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        $this->form_validation->set_rules('description', 'Description', 'trim|required');
        $this->form_validation->set_rules('introduction', 'Introduction', 'trim|required');
        $this->form_validation->set_rules('outcomes', 'Outcomes', 'trim|required');
        //$this->form_validation->set_rules('dtitles', 'Docuement title', 'trim|required');
        //$this->form_validation->set_rules('dfiles', 'Docuement files', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $msg['error'] = validation_errors();
            $msg['code'] = 400;
        } else {

            $data = [
                'slug' => slugify($this->input->post('title')),
                'created_by' => $this->session->userdata('user_id'),
                'name' => $this->input->post('title'),
                'long_title' => $this->input->post('long_title'),
                //'code' => $course_code,
                'description' => $this->input->post('description'),
                'introduction' => $this->input->post('introduction'),
                'outcomes' => $this->input->post('outcomes'),
                'getting_started' => $this->input->post('getting_started'),
            ];

            # clear cache when course created or updated
            clean_cache_by_key('api', 'getAllCourses');
            clean_cache('api', 'courses_' . md5(slugify($this->input->post('title'))));

            if ($this->input->post('course_content_id') > 0) {

                # Update course content
                $cid = $this->input->post('course_content_id');

                # checking on for unique context_id
                /*if( !empty($this->input->post('context_id')))  {
                    $data['context_id'] = $this->input->post('context_id');
                    $check_data = ['context_id' => $data['context_id'], 'id !=' => $cid ];
                    $check_context_id = $this->common_model->getAllData('courses', 'id', true, $check_data);
                }else  $check_context_id = [];*/

                $check_ccode_data = ['code' => $this->input->post('course_number'), 'id !=' => $cid];
                $check_ccode = $this->common_model->getAllData('courses', 'id', true, $check_ccode_data);

                // 
                if (count($check_ccode) == 0) {

                    $data['code'] = $this->input->post('course_number');
                    $this->common_model->UpdateDB('courses', ['id' => $cid], $data);

                    clean_cache_by_key('api', 'tel_getAllCourses'); // for delete all courses list cache
                    clean_cache_by_key_from_start('api', 'tel_courseAssignListbyUserID');

                    # clear cache when course created or updated
                    $cache_key = 'tel_courseDetailsBySlugs_' . $data['slug'];
                    clean_cache('api', $cache_key);

                    $doc_titles = explode('|', $this->input->post('dtitles'));
                    $doc_files = explode('|', $this->input->post('dfiles'));
                    $doc_ids = explode('|', $this->input->post('documentsId'));
                    $doc_data = [];
                    if ($this->input->post('documents')) {
                        if ($cid > 0) {
                            $this->common_model->DeleteDB('documents', array("reference_id" => $cid, "reference_type" => 1));
                        }
                        foreach ($this->input->post('documents') as $docData) {
                            $exp = explode('|', $docData);
                            $docDataarr['reference_id'] = $cid;
                            $docDataarr['reference_type'] = 1; // 2=Modules                
                            $docDataarr['title'] = $exp[1];
                            $docDataarr['filename'] = $exp[0];
                            $this->common_model->InsertData('documents', $docDataarr);
                        }
                    }
                    //echo $cid;
                    $msg['success'] = "Course has been updated successfully.";
                    $msg['code'] = 200;
                    $msg['id'] = $cid;
                }
            } else {
                # Insert course content
                $maxCourseId = $this->Courses_modal->getMaxId();
                $course_code = "CRS" . date('ym') . $maxCourseId[0]['maxid'];
                $data['code'] = $course_code;

                # checking on for unique context_id
                if (!empty($this->input->post('context_id'))) {
                    $data['context_id'] = $this->input->post('context_id');
                    $check_data = ['context_id' => $data['context_id']];
                    $check_context_id = $this->common_model->getAllData('courses', 'id', true, $check_data);
                } else $check_context_id = [];

                if (count($check_context_id) == 0) {

                    $this->common_model->InsertData('courses', $data);
                    $lastInsertId = $this->db->insert_id();
                    if ($this->input->post('documents')) {
                        if ($lastInsertId > 0) {
                            $this->common_model->DeleteDB('documents', array("reference_id" => $lastInsertId, "reference_type" => 1));
                        }
                        foreach ($this->input->post('documents') as $docData) {
                            $exp = explode('|', $docData);
                            $docDataarr['reference_id'] = $lastInsertId;
                            $docDataarr['reference_type'] = 1; // 2=Modules                
                            $docDataarr['title'] = $exp[1];
                            $docDataarr['filename'] = $exp[0];
                            //pr(post('mod_cont_id'),"k");
                            $this->common_model->InsertData('documents', $docDataarr);
                        }
                    }
                    //echo $lastInsertId;
                    $msg['success'] = "Course has been save successfully.";
                    $msg['code'] = 200;
                    $msg['id'] = $lastInsertId;
                } else {
                    $msg['error'] = "Duplicate context id, unable to create course.";
                    $msg['code'] = 400;
                }
            }
        }
        echo json_encode($msg);
        exit;
    }

    # Ajax function to save metadata
    public function addCourseMetadata()
    {

        if ($this->input->post('course_metadata_id') > 0) {
            $cid = $this->input->post('course_metadata_id');
            if ($this->input->post('course_expiry') != "")
                $course_expiry = $this->common_model->getDateMdYToYmd($this->input->post('course_expiry'));
            else
                $course_expiry = NULL;
            $data = [
                'certificate_id' => $this->input->post('certificates'),
                'certificate_requirement' => $this->input->post('certificate_requirement'),
                'course_img' => $this->input->post('uploadedCourseImg'),
                'course_availability' => $this->input->post('course_availability'),
                'status' => $this->input->post('course_availability'),
                'show_glossary' => $this->input->post('show_glossary'),
                'exp_status' => $this->input->post('exp_status'),
                'course_expiry' => $course_expiry,
                'course_img' => post('img_name'),
            ];

            $this->common_model->UpdateDB('courses', ['id' => $cid], $data);
            //echo $cid;
            $msg['success'] = "Course has been updated successfully.";
            $msg['code'] = 200;
            $msg['id'] = $cid;
        } else {
            $msg['error'] = "Unable to update course details.";
            $msg['code'] = 400;
        }
        echo json_encode($msg);
        exit;
    }


    # Ajax function for course assessment allotment
    public function addCourseAssessment()
    {
        //pr($_POST); exit;

        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        $this->form_validation->set_rules('instruction', 'Instruction', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $msg['error'] = validation_errors();
            $msg['code'] = 400;
        } else {
            if ($this->input->post('ass_course_id') > 0) {
                $course_id = $this->input->post('ass_course_id');
                $assessment_id = $this->input->post('assessment');
                $assmt_data = [
                    'title'             => $this->input->post('title'),
                    'long_title'        => $this->input->post('long_title'),
                    'created_by'            => $this->session->userdata('user_id'),
                    'course_id'         => $course_id,
                    'instruction'       => $this->input->post('instruction'),
                    'feedback'          => $this->input->post('feedback'),
                ];
                $this->common_model->InsertData('assessments', $assmt_data);
                $msg['success'] = "Assessments has been alloted to course.";
                $msg['code'] = 200;
                $msg['id'] = $course_id;
            } else {
                $msg['error'] = "Unable to allot the assessment.";
                $msg['code'] = 400;
            }
        }
        echo json_encode($msg);
        exit;
    }

    # Ajax function for course gradebook

    public function addCourseGradebook()
    {
        //pr($_POST); exit;
        $sec_cnt = $this->input->post('section_cnt');
        $gb_section_id = $this->input->post('gb_section_id' . $sec_cnt);
        $this->form_validation->set_rules('title' . $sec_cnt, 'Title', 'trim|required');
        $this->form_validation->set_rules('description' . $sec_cnt, 'Instruction', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $msg['error'] = validation_errors();
            $msg['code'] = 400;
        } else {

            $data_gradebook = [
                'created_by'            => $this->session->userdata('user_id'),
                'course_id'             => $this->input->post('course_gb_id'),
                'section_type'          => $this->input->post('section_type' . $sec_cnt),
                'section_title'         => $this->input->post('title' . $sec_cnt),
                'section_description'   => $this->input->post('description' . $sec_cnt),
                'weight'                => $this->input->post('weight' . $sec_cnt),
            ];

            if ($gb_section_id) {
                $this->common_model->UpdateDB('courses_gradebook', ['id' => $gb_section_id], $data_gradebook);
                $msg['success'] = "Gradebook section updated.";
                $msg['code'] = 200;
                $msg['id'] = $gb_section_id;
                $msg['sec_cnt'] = $sec_cnt;
            } else {
                $this->common_model->InsertData('courses_gradebook', $data_gradebook);
                $msg['success'] = "Gradebook section created.";
                $msg['code'] = 200;
                $msg['id'] = $this->db->insert_id();
                $msg['sec_cnt'] = $sec_cnt;
            }
        }
        echo json_encode($msg);
        exit;
    }

    # Ajax function for course building
    public function builder()
    {
        //pr($_POST); exit;

        $course_id = $this->input->post('course_id');
        $course_slug = $this->common_model->getAllData('courses', 'slug', true, ['id' => $course_id]);
        # clear cache when course created or updated
        $cache_key = 'tel_courseDetailsBySlugs_' . $course_slug->slug;
        clean_cache('api', $cache_key);
        clean_cache('api', 'tel_courseDetailsBySlugs_' . $course_slug->slug);


        if ($this->input->post('type') == 'M') {
            $modules_str = $this->input->post('module_id');
            $modules = explode("_", $modules_str);
            $module_id = $modules[1];
            $course_id = $this->input->post('course_id');
            $course_module_data = ['course_id' => $course_id, 'module_id' => $module_id, 'quiz_type' => 0, 'quiz_id' => NULL];

            $check_duplicate = $this->common_model->getAllData('course_contain', '', '', $course_module_data, '', 1);
            //pr($check_duplicate);
            if (count($check_duplicate) == 0) {
                $this->common_model->InsertData('course_contain', $course_module_data);
                $lastInsertId = $this->db->insert_id();
            }
            ///cahce
            //echo $lastInsertId;
        } else if ($this->input->post('type') == 'CA') {

            $module_id = NULL;
            $course_id = $this->input->post('course_id');

            # Quiz id
            $quiz_str = $this->input->post('quiz_id');
            $quizs = explode("_", $quiz_str);
            $quiz_id = $quizs[1];

            $course_module_data = ['course_id' => $course_id, 'module_id' => $module_id, 'quiz_type' => 4, 'quiz_id' => $quiz_id, 'contain_type' => 1];
            $check_duplicate = $this->common_model->getAllData('course_contain', '', '', $course_module_data, '', 1);
            //pr($check_duplicate);
            if (count($check_duplicate) == 0) {
                $this->common_model->InsertData('course_contain', $course_module_data);
                $lastInsertId = $this->db->insert_id();
            }
            echo $lastInsertId;
        } else if ($this->input->post('type') == 'CQ') {

            $module_id = NULL;
            $course_id = $this->input->post('course_id');

            # Quiz id
            $quiz_str = $this->input->post('quiz_id');
            $quizs = explode("_", $quiz_str);
            $quiz_id = $quizs[1];

            $course_module_data = ['course_id' => $course_id, 'module_id' => $module_id, 'quiz_type' => 4, 'quiz_id' => $quiz_id, 'contain_type' => 0];
            $check_duplicate = $this->common_model->getAllData('course_contain', '', '', $course_module_data, '', 1);
            //pr($check_duplicate);
            if (count($check_duplicate) == 0) {
                $this->common_model->InsertData('course_contain', $course_module_data);
                $lastInsertId = $this->db->insert_id();
            }
            echo $lastInsertId;
        } else if ($this->input->post('type') == 'L') {
            # Lessons
            //pr($_POST); exit;
            $course_id = $this->input->post('course_id');
            $modules_str = $this->input->post('module_id');
            $modules = explode("_", $modules_str);
            $module_id = $modules[1];

            $lessons_str = $this->input->post('lesson_id');
            $lessons = explode("_", $lessons_str);
            $lesson_id = $lessons[1];

            $course_module_lesson_data = ['course_id' => $course_id, 'module_id' => $module_id, 'lesson_id' => $lesson_id, 'quiz_type' => 0, 'quiz_id' => NULL];
            $check_duplicate = $this->common_model->getAllData('module_contain', '', '', $course_module_lesson_data, '', 1);

            if (count($check_duplicate) == 0) {
                $this->common_model->InsertData('module_contain', $course_module_lesson_data);
                $lastInsertId = $this->db->insert_id();
            }
            echo $lastInsertId;
        } else if ($this->input->post('type') == 'LQ') {
            # Lesson Quizes            
            $course_id = $this->input->post('course_id');
            # Module id
            $modules_str = $this->input->post('module_id');
            $modules = explode("_", $modules_str);
            $module_id = $modules[1];
            # Lesson id
            $lessons_str = $this->input->post('lesson_id');
            $lessons = explode("_", $lessons_str);
            $lesson_id = $lessons[1];
            # Quiz id
            $quiz_str = $this->input->post('quiz_id');
            $quizs = explode("_", $quiz_str);
            $quiz_id = $quizs[1];

            $course_module_lesson_quiz_data = ['course_id' => $course_id, 'module_id' => $module_id, 'lesson_id' => $lesson_id, 'quiz_id' => $quiz_id, 'quiz_type' => 0, 'contain_type' => 0];
            $check_duplicate = $this->common_model->getAllData('lesson_contain', '', '', $course_module_lesson_quiz_data, '', 1);

            if (count($check_duplicate) == 0) {
                $this->common_model->InsertData('lesson_contain', $course_module_lesson_quiz_data);
                $lastInsertId = $this->db->insert_id();
            }
            echo $lastInsertId;
        } else if ($this->input->post('type') == 'LA') {
            # Lesson Assessments            
            $course_id = $this->input->post('course_id');
            # Module id

            $modules_str = $this->input->post('module_id');
            $modules = explode("_", $modules_str);
            $module_id = $modules[1];
            # Lesson id
            $lessons_str = $this->input->post('lesson_id');
            $lessons = explode("_", $lessons_str);
            $lesson_id = $lessons[1];
            # Quiz id
            $quiz_str = $this->input->post('quiz_id');
            $quizs = explode("_", $quiz_str);
            $quiz_id = $quizs[1];

            $course_module_lesson_quiz_data = ['course_id' => $course_id, 'module_id' => $module_id, 'lesson_id' => $lesson_id, 'quiz_id' => $quiz_id, 'quiz_type' => 2, 'contain_type' => 1];
            $check_duplicate = $this->common_model->getAllData('lesson_contain', '', '', $course_module_lesson_quiz_data, '', 1);

            if (count($check_duplicate) == 0) {
                $this->common_model->InsertData('lesson_contain', $course_module_lesson_quiz_data);
                $lastInsertId = $this->db->insert_id();
            }
            echo $lastInsertId;
        } else if ($this->input->post('type') == 'MQ') { // Insert module quiz
            # Course id
            $course_id = $this->input->post('course_id');
            # Module id
            $modules_str = $this->input->post('module_id');
            $modules = explode("_", $modules_str);
            $module_id = $modules[1];

            # Quiz id
            $quiz_str = $this->input->post('quiz_id');
            $quizs = explode("_", $quiz_str);
            $quiz_id = $quizs[1];

            $module_contain_data = ['course_id' => $course_id, 'module_id' => $module_id, 'quiz_id' => $quiz_id, 'quiz_type' => 3, 'contain_type' => 0];
            $check_duplicate = $this->common_model->getAllData('module_contain', '', '', $module_contain_data, '', 1);
            if (count($check_duplicate) == 0) {
                $this->common_model->InsertData('module_contain', $module_contain_data);
                $lastInsertId = $this->db->insert_id();
            }
            $module_slug = $this->common_model->getAllData('modules', 'slug', true, ['id' => $module_id])->slug;
            # clear cache when course created or updated
            $cache_key = 'tel_get_module_detail_by_slug_' . $course_slug->slug . '_' . $module_slug;
            clean_cache('api', $cache_key);

            echo $lastInsertId;
        } else if ($this->input->post('type') == 'MA') { // Insert module assessments
            # Course id
            $course_id = $this->input->post('course_id');
            # Module id
            $modules_str = $this->input->post('module_id');
            $modules = explode("_", $modules_str);
            $module_id = $modules[1];

            # Quiz id
            $quiz_str = $this->input->post('quiz_id');
            $quizs = explode("_", $quiz_str);
            $quiz_id = $quizs[1];

            $module_contain_data = ['course_id' => $course_id, 'module_id' => $module_id, 'quiz_id' => $quiz_id, 'quiz_type' => 3, 'contain_type' => 1];
            $check_duplicate = $this->common_model->getAllData('module_contain', '', '', $module_contain_data, '', 1);
            if (count($check_duplicate) == 0) {
                $this->common_model->InsertData('module_contain', $module_contain_data);
                $lastInsertId = $this->db->insert_id();
            }
            $module_slug = $this->common_model->getAllData('modules', 'slug', true, ['id' => $module_id])->slug;
            # clear cache when course created or updated
            $cache_key = 'tel_get_module_detail_by_slug_' . $course_slug->slug . '_' . $module_slug;
            clean_cache('api', $cache_key);
            echo $lastInsertId;
        }
        exit;
    }

    # Ajax function to set order of module, lesson and quiz
    public function set_orders()
    {
        //pr($_POST); exit;
        if ($this->input->post()) {
            $requestData = $this->input->post();

            foreach ($requestData as $key => $data) {

                $course_id = $data['course_id'];
                $course_slug = $this->common_model->getAllData('courses', 'slug', true, ['id' => $course_id]);
                # clear cache when course created or updated
                $cache_key = 'tel_courseDetailsBySlugs_' . $course_slug->slug;
                clean_cache('api', $cache_key);

                clean_cache('api', 'tel_courseDetailsBySlugs_' . $course_slug->slug);

                if ($data['type'] == 'M') {  // Module

                    $modules_str = $data['module_id'];
                    $modules = explode("_", $modules_str);
                    $module_id = $modules[1];
                    $course_id = $data['course_id'];
                    $order_id = $data['order_id'];
                    $course_module_data = ['course_id' => $course_id, 'module_id' => $module_id, 'quiz_type' => 0, 'quiz_id' => NULL];
                    $check = $this->common_model->getAllData('course_contain', '', true, $course_module_data, '', 1);
                    if ($check->id > 0) {
                        $data = ['course_id' => $course_id, 'module_id' => $module_id, 'order_id' => $order_id];
                        $this->common_model->UpdateDB('course_contain', ['id' => $check->id], $data);
                    }
                } else if ($data['type'] == 'CA') { // course assessments
                    //pr($_POST); exit;
                    $module_id = NULL;
                    $course_id = $data['course_id'];
                    $order_id = $data['order_id'];

                    $quiz_str = $data['quiz_id'];
                    $quizs = explode("_", $quiz_str);
                    $quiz_id = $quizs[1];

                    $course_module_data = ['course_id' => $course_id, 'module_id' => $module_id, 'quiz_type' => 4, 'quiz_id' => $quiz_id, 'contain_type' => 1];
                    $check = $this->common_model->getAllData('course_contain', '', true, $course_module_data, '', 1);
                    if ($check->id > 0) {
                        $data = ['course_id' => $course_id, 'module_id' => $module_id, 'order_id' => $order_id, 'quiz_type' => 4, 'quiz_id' => $quiz_id, 'contain_type' => 1];
                        $this->common_model->UpdateDB('course_contain', ['id' => $check->id], $data);
                    }
                } else if ($data['type'] == 'CQ') { // course quizes

                    $module_id = NULL;
                    $course_id = $data['course_id'];
                    $order_id = $data['order_id'];

                    $quiz_str = $data['quiz_id'];
                    $quizs = explode("_", $quiz_str);
                    $quiz_id = $quizs[1];

                    $course_module_data = ['course_id' => $course_id, 'module_id' => $module_id, 'quiz_type' => 4, 'quiz_id' => $quiz_id, 'contain_type' => 0];
                    $check = $this->common_model->getAllData('course_contain', '', true, $course_module_data, '', 1);
                    if ($check->id > 0) {
                        $data = ['course_id' => $course_id, 'module_id' => $module_id, 'order_id' => $order_id, 'quiz_type' => 4, 'quiz_id' => $quiz_id, 'contain_type' => 0];
                        $this->common_model->UpdateDB('course_contain', ['id' => $check->id], $data);
                    }
                } else if ($data['type'] == 'L') {    // Lesson

                    $modules_str = $data['module_id'];
                    $modules = explode("_", $modules_str);
                    $module_id = $modules[1];

                    $lessons_str = $data['lesson_id'];
                    $lessons = explode("_", $lessons_str);
                    $lesson_id = $lessons[1];

                    $course_id = $data['course_id'];
                    $order_id = $data['order_id'];
                    $course_module_lesson_data = ['course_id' => $course_id, 'lesson_id' => $lesson_id, 'module_id' => $module_id, 'quiz_type' => 0, 'quiz_id' => NULL];
                    $check = $this->common_model->getAllData('module_contain', '', true, $course_module_lesson_data, '', 1);
                    if ($check->id > 0) {
                        $data = ['course_id' => $course_id, 'lesson_id' => $lesson_id, 'module_id' => $module_id, 'order_id' => $order_id, 'quiz_type' => 0];
                        $this->common_model->UpdateDB('module_contain', ['id' => $check->id], $data);
                    }
                    echo $check->id;
                } else if ($data['type'] == 'Q') { // Quiz

                    $modules_str = $data['module_id'];
                    $modules = explode("_", $modules_str);
                    $module_id = $modules[1];

                    $lessons_str = $data['lesson_id'];
                    $lessons = explode("_", $lessons_str);
                    $lesson_id = $lessons[1];

                    $quiz_str = $data['quiz_id'];
                    $quizs = explode("_", $quiz_str);
                    $quiz_id = $quizs[1];

                    $course_id = $data['course_id'];
                    $order_id = $data['order_id'];
                    $course_module_lesson_quiz_data = ['course_id' => $course_id, 'lesson_id' => $lesson_id, 'module_id' => $module_id, 'quiz_id' => $quiz_id, 'quiz_type' => 0, 'contain_type' => 0];
                    $check = $this->common_model->getAllData('lesson_contain', '', true, $course_module_lesson_quiz_data);
                    if ($check->id > 0) {
                        $data = ['course_id' => $course_id, 'lesson_id' => $lesson_id, 'module_id' => $module_id, 'order_id' => $order_id, 'quiz_id' => $quiz_id, 'quiz_type' => 0, 'contain_type' => 0];
                        $this->common_model->UpdateDB('lesson_contain', ['id' => $check->id], $data);
                    }
                } else if ($data['type'] == 'LQ') {        // Lesson quizzes

                    $modules_str = $data['module_id'];
                    $modules = explode("_", $modules_str);
                    $module_id = $modules[1];

                    $lessons_str = $data['lesson_id'];
                    $lessons = explode("_", $lessons_str);
                    $lesson_id = $lessons[1];

                    $quiz_str = $data['quiz_id'];
                    $quizs = explode("_", $quiz_str);
                    $quiz_id = $quizs[1];

                    $course_id = $data['course_id'];
                    $order_id = $data['order_id'];
                    $course_module_lesson_quiz_data = ['course_id' => $course_id, 'lesson_id' => $lesson_id, 'module_id' => $module_id, 'quiz_id' => $quiz_id, 'quiz_type' => 0, 'contain_type' => 0];
                    $check = $this->common_model->getAllData('lesson_contain', '', true, $course_module_lesson_quiz_data);
                    if ($check->id > 0) {
                        $data = ['course_id' => $course_id, 'lesson_id' => $lesson_id, 'module_id' => $module_id, 'order_id' => $order_id, 'quiz_id' => $quiz_id, 'quiz_type' => 0, 'contain_type' => 0];
                        $this->common_model->UpdateDB('lesson_contain', ['id' => $check->id], $data);
                    }
                } else if ($data['type'] == 'LA') {   // Lesson assessment

                    $modules_str = $data['module_id'];
                    $modules = explode("_", $modules_str);
                    $module_id = $modules[1];

                    $lessons_str = $data['lesson_id'];
                    $lessons = explode("_", $lessons_str);
                    $lesson_id = $lessons[1];

                    $quiz_str = $data['quiz_id'];
                    $quizs = explode("_", $quiz_str);
                    $quiz_id = $quizs[1];

                    $course_id = $data['course_id'];
                    $order_id = $data['order_id'];
                    $course_module_lesson_quiz_data = ['course_id' => $course_id, 'lesson_id' => $lesson_id, 'module_id' => $module_id, 'quiz_id' => $quiz_id, 'quiz_type' => 2, 'contain_type' => 1];
                    $check = $this->common_model->getAllData('lesson_contain', '', true, $course_module_lesson_quiz_data);
                    if ($check->id > 0) {
                        $data = ['course_id' => $course_id, 'lesson_id' => $lesson_id, 'module_id' => $module_id, 'order_id' => $order_id, 'quiz_id' => $quiz_id, 'quiz_type' => 2, 'contain_type' => 1];
                        $this->common_model->UpdateDB('lesson_contain', ['id' => $check->id], $data);
                    }
                } else if ($data['type'] == 'A') {  // course assessment

                    $modules_str = $data['module_id'];
                    $modules = explode("_", $modules_str);
                    $module_id = $modules[1];

                    $lessons_str = $data['lesson_id'];
                    $lessons = explode("_", $lessons_str);
                    $lesson_id = $lessons[1];

                    $quiz_str = $data['quiz_id'];
                    $quizs = explode("_", $quiz_str);
                    $quiz_id = $quizs[1];

                    $course_id = $data['course_id'];
                    $order_id = $data['order_id'];
                    $course_module_lesson_quiz_data = ['course_id' => $course_id, 'lesson_id' => $lesson_id, 'module_id' => $module_id, 'quiz_id' => $quiz_id, 'quiz_type' => 4, 'contain_type' => 1];
                    $check = $this->common_model->getAllData('lesson_contain', '', true, $course_module_lesson_quiz_data);

                    if ($check->id > 0) {
                        $data = ['course_id' => $course_id, 'lesson_id' => $lesson_id, 'module_id' => $module_id, 'order_id' => $order_id, 'quiz_id' => $quiz_id, 'quiz_type' => 4, 'contain_type' => 1];
                        $this->common_model->UpdateDB('lesson_contain', ['id' => $check->id], $data);
                    }
                } else if ($data['type'] == 'MQ') { // Module quiz

                    $course_id = $data['course_id'];
                    $modules_str = $data['module_id'];
                    $modules = explode("_", $modules_str);
                    $module_id = $modules[1];

                    $quiz_str = $data['quiz_id'];
                    $quizs = explode("_", $quiz_str);
                    $quiz_id = $quizs[1];

                    $order_id = $data['order_id'];

                    # For module quiz type = 3 
                    $module_contain_data = ['course_id' => $course_id, 'module_id' => $module_id, 'quiz_id' => $quiz_id, 'quiz_type' => 3, 'contain_type' => 0];
                    $check = $this->common_model->getAllData('module_contain', '', true, $module_contain_data);
                    if ($check->id > 0) {
                        $data = ['course_id' => $course_id, 'module_id' => $module_id, 'order_id' => $order_id, 'quiz_id' => $quiz_id, 'quiz_type' => 3, 'contain_type' => 0];
                        $this->common_model->UpdateDB('module_contain', ['id' => $check->id], $data);
                    }
                    $module_slug = $this->common_model->getAllData('modules', 'slug', true, ['id' => $module_id])->slug;
                    # clear cache when course created or updated
                    $cache_key = 'tel_get_module_detail_by_slug_' . $course_slug->slug . '_' . $module_slug;
                    clean_cache('api', $cache_key);
                } else if ($data['type'] == 'MA') {   // Module assessments

                    $course_id = $data['course_id'];
                    $modules_str = $data['module_id'];
                    $modules = explode("_", $modules_str);
                    $module_id = $modules[1];

                    $quiz_str = $data['quiz_id'];
                    $quizs = explode("_", $quiz_str);
                    $quiz_id = $quizs[1];

                    $order_id = $data['order_id'];

                    # For module assessment type = 3 
                    $module_contain_data = ['course_id' => $course_id, 'module_id' => $module_id, 'quiz_id' => $quiz_id, 'quiz_type' => 3, 'contain_type' => 1];
                    $check = $this->common_model->getAllData('module_contain', '', true, $module_contain_data);
                    if ($check->id > 0) {
                        $data = ['course_id' => $course_id, 'module_id' => $module_id, 'order_id' => $order_id, 'quiz_id' => $quiz_id, 'quiz_type' => 3, 'contain_type' => 1];
                        $this->common_model->UpdateDB('module_contain', ['id' => $check->id], $data);
                    }
                    $module_slug = $this->common_model->getAllData('modules', 'slug', true, ['id' => $module_id])->slug;
                    # clear cache when course created or updated
                    $cache_key = 'tel_get_module_detail_by_slug_' . $course_slug->slug . '_' . $module_slug;
                    clean_cache('api', $cache_key);
                }
            } //end foreach
            echo 1;
            exit;
        }
    }

    # Get all the Module lessons quiz according to sytem category and tabs
    public function getModulesLessonsQuiz()
    {
        //pr($_POST); exit;
        $course_id = $this->input->post('course_builder_id');
        $category_id = ($this->input->post('selected_system_categories')) ? $this->input->post('selected_system_categories') : [];
        $tag_id = ($this->input->post('selected_system_tag')) ? $this->input->post('selected_system_tag') : [];

        if ($category_id) {
            $category_assigned_condition = ' `category_assigned`.`category_id` IN (' . implode(", ", $category_id) . ') ';
        }

        if ($tag_id) {
            $tag_assigned_condition = ' `tag_assigned`.`tag_id` IN (' . implode(", ", $tag_id) . ') ';
        }

        if ($category_assigned_condition && $tag_assigned_condition) {
            $conditions = ' ( ' . $category_assigned_condition . ' OR ' . $tag_assigned_condition . ' ) ';
            $joins_module = 'JOIN `category_assigned` ON `modules`.`id` = `category_assigned`.`reference_id` AND `category_assigned`.`reference_sub_type`= ' . META_MODULE . ' JOIN `tag_assigned` ON `modules`.`id` = `tag_assigned`.`reference_id` AND `tag_assigned`.`reference_sub_type`= ' . META_MODULE;

            $joins_lesson = 'JOIN `category_assigned` ON `lessons`.`id` = `category_assigned`.`reference_id` AND `category_assigned`.`reference_sub_type`= ' . META_LESSON . ' JOIN `tag_assigned` ON `lessons`.`id` = `tag_assigned`.`reference_id` AND `tag_assigned`.`reference_sub_type`=' . META_LESSON;

            $joins_quiz = 'JOIN `category_assigned` ON `quizzes`.`id` = `category_assigned`.`reference_id` AND `category_assigned`.`reference_sub_type`= ' . META_QUIZ . ' JOIN `categories` ON `category_assigned`.`category_id` = `categories`.`id` JOIN `tag_assigned` ON `quizzes`.`id` = `tag_assigned`.`reference_id` AND `tag_assigned`.`reference_sub_type`= ' . META_QUIZ . ' JOIN `tags` ON `tag_assigned`.`tag_id` = `tags`.`id`';

            //$joins_assessment = 'JOIN `category_assigned` ON `assessments`.`id` = `category_assigned`.`reference_id` AND `category_assigned`.`reference_sub_type`= '.META_ASSESSMENT.' JOIN `categories` ON `category_assigned`.`category_id` = `categories`.`id` JOIN `tag_assigned` ON `assessments`.`id` = `tag_assigned`.`reference_id` AND `tag_assigned`.`reference_sub_type`= '.META_ASSESSMENT.' JOIN `tags` ON `tag_assigned`.`tag_id` = `tags`.`id`';

        } else if ($category_assigned_condition && !$tag_assigned_condition) {
            $conditions = $category_assigned_condition;
            $joins_module = 'JOIN `category_assigned` ON `modules`.`id` = `category_assigned`.`reference_id` AND `category_assigned`.`reference_sub_type`= ' . META_MODULE;

            $joins_lesson = 'JOIN `category_assigned` ON `lessons`.`id` = `category_assigned`.`reference_id` AND `category_assigned`.`reference_sub_type`= ' . META_LESSON;

            $joins_quiz = 'JOIN `category_assigned` ON `quizzes`.`id` = `category_assigned`.`reference_id` AND `category_assigned`.`reference_sub_type`= ' . META_QUIZ . ' JOIN `categories` ON `category_assigned`.`category_id` = `categories`.`id` ';

            //$joins_assessment = 'JOIN `category_assigned` ON `assessments`.`id` = `category_assigned`.`reference_id` AND `category_assigned`.`reference_sub_type`= '.META_ASSESSMENT.' JOIN `categories` ON `category_assigned`.`category_id` = `categories`.`id` ';

        } else if (!$category_assigned_condition && $tag_assigned_condition) {
            $conditions = $tag_assigned_condition;
            $joins_module = 'JOIN `tag_assigned` ON `modules`.`id` = `tag_assigned`.`reference_id` AND `tag_assigned`.`reference_sub_type`= ' . META_MODULE;

            $joins_lesson = 'JOIN `tag_assigned` ON `lessons`.`id` = `tag_assigned`.`reference_id` AND `tag_assigned`.`reference_sub_type`=' . META_LESSON;

            $joins_quiz = 'JOIN `tag_assigned` ON `quizzes`.`id` = `tag_assigned`.`reference_id` AND `tag_assigned`.`reference_sub_type`= ' . META_QUIZ . ' JOIN `tags` ON `tag_assigned`.`tag_id` = `tags`.`id`';

            //$joins_assessment = 'JOIN `tag_assigned` ON `assessments`.`id` = `tag_assigned`.`reference_id` AND `tag_assigned`.`reference_sub_type`= '.META_ASSESSMENT.' JOIN `tags` ON `tag_assigned`.`tag_id` = `tags`.`id`';
        }
        $module_conditions = " AND modules.delete_status='1' AND modules.status='1' ";
        $lesson_conditions = " AND lessons.delete_status='1' AND lessons.status='1' ";
        $quizzes_conditions = " AND quizzes.delete_status='1' AND quizzes.status='1' ";
        # Fetch all the modules from selected category and tags
        $query_module = $this->db->query('SELECT DISTINCT `modules`.`id`, `modules`.`name` FROM `modules` ' . $joins_module . ' WHERE ' . $conditions . $module_conditions . ' ORDER BY `modules`.`name` ASC');
        $modules = $query_module->result_array();

        $this->db->select('module_id');
        $this->db->from('course_contain');
        $this->db->where_in('course_id', $course_id);
        $sel_modules = array_column($this->db->get()->result_array(), 'module_id');

        foreach ($modules as $key => $value) {
            if (in_array($value['id'], $sel_modules)) {
                $modules[$key]['strike'] = 1;
            } else $modules[$key]['strike'] = 0;
        }
        $data['modules'] = $modules;

        # Fetch all the lesson from selected category and tags
        $query_lesson = $this->db->query(' SELECT DISTINCT `lessons`.`id`, `lessons`.`name` FROM `lessons` ' . $joins_lesson . ' WHERE ' . $conditions . $lesson_conditions . ' ORDER BY `lessons`.`name` ASC ');
        $lessons = $query_lesson->result_array();

        $this->db->select('lesson_id');
        $this->db->from('module_contain');
        $this->db->where('course_id', $course_id);
        $sel_lessons = array_column($this->db->get()->result_array(), 'lesson_id');

        foreach ($lessons as $key => $value) {
            if (in_array($value['id'], $sel_lessons)) {
                $lessons[$key]['strike'] = 1;
            } else $lessons[$key]['strike'] = 0;
        }
        $data['lessons'] = $lessons;

        # Fetch all the quiz from selected category and tags
        $query_quiz = $this->db->query('SELECT DISTINCT `quizzes`.`id`, `quizzes`.`title` AS `name` , `quizzes`.`type` FROM `quizzes` ' . $joins_quiz . $quizzes_conditions . ' WHERE ' . $conditions . ' AND `quizzes`.`type` != 1  ORDER BY `quizzes`.`title` ASC');
        $quizzes = $query_quiz->result_array();
        //  pr($this->db->last_query());exit;
        # checking quizzes from lesson
        $this->db->select('quiz_id');
        $this->db->from('lesson_contain');
        $this->db->where(['course_id' => $course_id, 'quiz_type' => 0, 'contain_type' => 0]);
        $sel_quizzes = array_column($this->db->get()->result_array(), 'quiz_id');

        foreach ($quizzes as $key => $value) {
            if (in_array($value['id'], $sel_quizzes)) {
                $quizzes[$key]['strike'] = 1;
            } else $quizzes[$key]['strike'] = 0;
        }
        $data['quizzes'] = $quizzes;
        # checking quizzes from course
        $this->db->select('quiz_id');
        $this->db->from('course_contain');
        $this->db->where(['course_id' => $course_id, 'quiz_type' => 4, 'contain_type' => 0]);
        $sel_mquizzes = array_column($this->db->get()->result_array(), 'quiz_id');

        foreach ($quizzes as $key => $value) {
            if (in_array($value['id'], $sel_mquizzes) && $quizzes[$key]['strike'] == 0) {
                $quizzes[$key]['strike'] = 1;
            }
        }
        $data['quizzes'] = $quizzes;

        # checking quizzes from module
        $this->db->select('quiz_id');
        $this->db->from('module_contain');
        $this->db->where(['course_id' => $course_id, 'quiz_type' => 3, 'contain_type' => 0, 'module_id !=' => NULL]);
        $sel_mquizzes = array_column($this->db->get()->result_array(), 'quiz_id');

        foreach ($quizzes as $key => $value) {
            if (in_array($value['id'], $sel_mquizzes) && $quizzes[$key]['strike'] == 0) {
                $quizzes[$key]['strike'] = 1;
            }
        }
        $data['quizzes'] = $quizzes;
        echo json_encode($data);
        exit;
    }


    # Ajax request to check course number
    public function checkCourseNumber()
    {
        //pr($_POST); 
        if ($this->input->post()) {
            $course_id = $this->input->post('id');
            $course_number = $this->input->post('course_number');
            $data = ['code' => $course_number, 'id !=' => $course_id];
            $check = $this->common_model->getAllData('courses', 'id', true, $data);
            if (count($check) > 0) {
                echo 1;
            }
        }
        exit;
    }

    # Ajax request to check course number
    public function checkContextId()
    {
        //pr($_POST); 
        if ($this->input->post()) {
            $course_id = $this->input->post('id');
            if ($course_id == "") {
                $context_id = $this->input->post('context_id');
                $data = ['context_id' => $context_id];
                $check = $this->common_model->getAllData('courses', 'id', true, $data);
                if (count($check) > 0) {
                    echo 1;
                }
            } else {
                $context_id = $this->input->post('context_id');
                $data = ['context_id' => $context_id, 'id !=' => $course_id];
                $check = $this->common_model->getAllData('courses', 'id', true, $data);
                if (count($check) > 0) {
                    echo 1;
                }
            }
        } else echo 0;
        exit;
    }


    function checkResourceLinkId()
    {
        if ($this->input->post('resource_link_id') != '') {
            $assessment_id = $this->input->post('id');
            $course_id = $this->input->post('course_id');
            if ($assessment_id == "") {
                //echo "tt"; 
                $resource_link_id = $this->input->post('resource_link_id');
                $data = ['resource_link_id' => $resource_link_id];
                $check = $this->common_model->getAllData('assessments', 'id', true, $data);
                if (count($check) > 0) {
                    echo 1;
                } else echo 0;
            } else {
                //echo "zz"; exit;
                $resource_link_id = $this->input->post('resource_link_id');
                $data = ['resource_link_id' => $resource_link_id, 'id !=' => $assessment_id, 'course_id !=' => $course_id];
                $check = $this->common_model->getAllData('assessments', 'id', true, $data);
                if (count($check) > 0) {
                    echo 1;
                } else echo 0;
            }
        } else echo 0;
        exit;
    }

    /**
     * getLists method 
     * @description this function called via ajax request, use to display list of lesson
     * @return void
     */
    public function setAllSelected()
    {
        $lessonIds = array_filter(explode("|", $this->input->post('selectedLesson')));
        $data = [];
        $status = $this->input->post('status');
        foreach ($lessonIds as $key => $val) {
            $data['status'] = $status;
            $this->Courses_modal->update_status($val, $data);
        }
        $msg = 'Updated';
        clean_cache_by_key('api', 'tel_getAllCourses'); // for delete all courses list cache
        echo json_encode(['msg' => $msg]);
        exit;
    }

    public function remove_course_data()
    {
        if ($this->input->post()) {

            $type = $this->input->post('type');
            $course_id = $this->input->post('course_id');
            $course_slug = $this->common_model->getAllData('courses', 'slug', true, ['id' => $course_id]);

            # clear cache when course created or updated
            clean_cache('api', md5(slugify($course_slug->slug)));
            clean_cache('api', 'tel_courseDetailsBySlugs_' . $course_slug->slug);

            if ($type == 'M') {
                // module
                $modules_str = $this->input->post('module_id');
                $modules = explode("_", $modules_str);
                $module_id = $modules[1];

                $data = ['course_id' => $course_id, 'module_id' => $module_id];

                # Remove quiz and assessment of lesson
                $check_lesson_quiz_assmt_exist = $this->common_model->getAllData('lesson_contain',  '', '', $data);
                if ($check_lesson_quiz_assmt_exist) {
                    $remove_lesson_quiz_assmt = $this->db->query('DELETE FROM `lesson_contain` WHERE `course_id` = ' . $course_id . ' AND `module_id` = ' . $module_id);
                }

                # Remove quiz and assessment of modules
                $check_module_quiz_assmt_exist = $this->common_model->getAllData('module_contain',  '', '', $data);
                if ($check_module_quiz_assmt_exist) {
                    $remove_module_quiz_assmt = $this->db->query('DELETE FROM `module_contain` WHERE `course_id` = ' . $course_id . ' AND `module_id` = ' . $module_id);
                }

                # Remove module from course
                $check_module_exist = $this->common_model->getAllData('course_contain',  '', '', $data);
                if ($check_module_exist) {
                    $remove_module = $this->db->query('DELETE FROM `course_contain` WHERE `course_id` = ' . $course_id . ' AND `module_id` = ' . $module_id);
                    if ($remove_module)
                        echo 1;
                }
            } else if ($type == 'L') {   // lesson

                $modules_str = $this->input->post('module_id');
                $modules = explode("_", $modules_str);
                $module_id = $modules[1];

                $lessons_str = $this->input->post('lesson_id');
                $lessons = explode("_", $lessons_str);
                $lesson_id = $lessons[1];

                $data_lesson = ['course_id' => $course_id, 'module_id' => $module_id, 'lesson_id' => $lesson_id];
                $check_lesson_exits = $this->common_model->getAllData('lesson_contain', '', '', $data_lesson);

                if (count($check_lesson_exits) > 0) {
                    $remove_quiz = $this->db->query('DELETE FROM `lesson_contain` WHERE `course_id` = ' . $course_id . ' AND `module_id` = ' . $module_id . ' AND `lesson_id` = ' . $lesson_id);
                }
                $remove_lesson = $this->db->query('DELETE FROM `module_contain` WHERE `course_id` = ' . $course_id . ' AND `module_id` = ' . $module_id . ' AND `lesson_id` = ' . $lesson_id);
                if ($remove_lesson)
                    echo 1;
            } else if ($type == 'Q') {   // quiz

                $data = explode('_', $this->input->post('quiz_id'));

                $modules_str = $this->input->post('module_id');
                $modules = explode("_", $modules_str);
                $module_id = $modules[1];

                $lessons_str = $this->input->post('lesson_id');
                $lessons = explode("_", $lessons_str);
                $lesson_id = $lessons[1];

                $quizs_str = $this->input->post('quiz_id');
                $quizs = explode("_", $quizs_str);
                $quiz_id = $quizs[1];

                $remove_quiz = $this->db->query('DELETE FROM `lesson_contain` WHERE `course_id` = ' . $course_id . ' AND `module_id` = ' . $module_id . ' AND `lesson_id` = ' . $lesson_id . ' AND `quiz_id` = ' . $quiz_id);
                if ($remove_quiz)
                    echo 1;
            } else if ($type == 'LQ') {   // Lesson quiz

                $modules_str = $this->input->post('module_id');
                $modules = explode("_", $modules_str);
                $module_id = $modules[1];

                $lessons_str = $this->input->post('lesson_id');
                $lessons = explode("_", $lessons_str);
                $lesson_id = $lessons[1];

                $quizs_str = $this->input->post('quiz_id');
                $quizs = explode("_", $quizs_str);
                $quiz_id = $quizs[1];

                $remove_quiz = $this->db->query('DELETE FROM `lesson_contain` WHERE `course_id` = ' . $course_id . ' AND `module_id` = ' . $module_id . ' AND `lesson_id` = ' . $lesson_id . ' AND `quiz_id` = ' . $quiz_id . ' AND `contain_type`=0  AND `quiz_type`=0');
                //echo $this->db->last_query();
                if ($remove_quiz)
                    echo 1;
            } else if ($type == 'LA') {   // Lesson assessment

                $modules_str = $this->input->post('module_id');
                $modules = explode("_", $modules_str);
                $module_id = $modules[1];

                $lessons_str = $this->input->post('lesson_id');
                $lessons = explode("_", $lessons_str);
                $lesson_id = $lessons[1];

                $quizs_str = $this->input->post('quiz_id');
                $quizs = explode("_", $quizs_str);
                $quiz_id = $quizs[1];

                $remove_quiz = $this->db->query('DELETE FROM `lesson_contain` WHERE `course_id` = ' . $course_id . ' AND `module_id` = ' . $module_id . ' AND `lesson_id` = ' . $lesson_id . ' AND `quiz_id` = ' . $quiz_id . ' AND `contain_type` =1 AND `quiz_type` = 2');
                echo $this->db->last_query();
                if ($remove_quiz)
                    echo 1;
            } else if ($type == 'MQ') {   // module quiz

                $modules_str = $this->input->post('module_id');
                $modules = explode("_", $modules_str);
                $module_id = $modules[1];

                $quizs_str = $this->input->post('quiz_id');
                $quizs = explode("_", $quizs_str);
                $quiz_id = $quizs[1];

                $remove_quiz = $this->db->query('DELETE FROM `module_contain` WHERE `course_id` = ' . $course_id . ' AND `module_id` = ' . $module_id . ' AND `quiz_id` = ' . $quiz_id . ' AND `contain_type` =0 AND `quiz_type` = 3');
                // echo $this->db->last_query();

                $module_slug = $this->common_model->getAllData('modules', 'slug', true, ['id' => $module_id])->slug;
                # clear cache when course created or updated
                $cache_key = 'tel_get_module_detail_by_slug_' . $course_slug->slug . '_' . $module_slug;
                clean_cache('api', $cache_key);

                if ($remove_quiz)
                    echo 1;
            } else if ($type == 'MA') {   // module assessment

                $modules_str = $this->input->post('module_id');
                $modules = explode("_", $modules_str);
                $module_id = $modules[1];

                $quizs_str = $this->input->post('quiz_id');
                $quizs = explode("_", $quizs_str);
                $quiz_id = $quizs[1];

                $remove_quiz = $this->db->query('DELETE FROM `module_contain` WHERE `course_id` = ' . $course_id . ' AND `module_id` = ' . $module_id . ' AND `quiz_id` = ' . $quiz_id . ' AND `contain_type` =1 AND `quiz_type` = 3');

                $module_slug = $this->common_model->getAllData('modules', 'slug', true, ['id' => $module_id])->slug;
                # clear cache when course created or updated
                $cache_key = 'tel_get_module_detail_by_slug_' . $course_slug->slug . '_' . $module_slug;
                clean_cache('api', $cache_key);

                // echo $this->db->last_query();
                if ($remove_quiz)
                    echo 1;
            } else if ($type == 'CA') {   // Coruse quiz        

                $quizs_str = $this->input->post('quiz_id');
                $quizs = explode("_", $quizs_str);
                $quiz_id = $quizs[1];

                $remove_quiz = $this->db->query('DELETE FROM `course_contain` WHERE `course_id` = ' . $course_id . ' AND `quiz_type` = 4 AND `contain_type` = 1 AND `quiz_id` = ' . $quiz_id);
                if ($remove_quiz)
                    echo 1;
            } else if ($type == 'CQ') {   // Coruse quiz        

                $quizs_str = $this->input->post('quiz_id');
                $quizs = explode("_", $quizs_str);
                $quiz_id = $quizs[1];

                $remove_quiz = $this->db->query('DELETE FROM `course_contain` WHERE `course_id` = ' . $course_id . ' AND `quiz_type` = 4 AND `contain_type` = 0 AND `quiz_id` = ' . $quiz_id);
                if ($remove_quiz)
                    echo 1;
            }
            exit;
        }
    }

    /**
     * getCheckExist method
     * @description this method is use to check name is already exist or not
     * @param string
     * @return json array
     */
    public function getCheckExist()
    {
        //pr($_POST,"k");
        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->setup();
        } else {
            $this->load->model('Courses_modal');
            $conditions = [
                'name' => $this->input->post('title'),
                'delete_status' => 1
            ];
            if (post('recordID') != 0 && post('recordID') != '') {
                $conditions['id !='] = post('recordID');
            }

            if ($this->Courses_modal->getCount($conditions, 'courses') == 0) {
                echo 'true';
                exit;
            } else {
                echo 'false';
                exit;
            }
        }
        exit;
    }
    
    function get_current_enrollments_count($container_id)
    {
        
        $data['typeid'] = $container_id;
        list($container_id, $type) = explode("_", $container_id);
        $data['value'] = $this->Courses_modal->get_current_enrollments_count($container_id);
        echo json_encode($data);
        exit;
    }

    function get_current_enrollments($container_id)
    {
        
        $data['typeid'] = $container_id;
        list($container_id, $type) = explode("_", $container_id);
        $data['value'] = $this->Courses_modal->get_current_enrollments($container_id);
        echo json_encode($data);
        exit;
    }
    
    
}
