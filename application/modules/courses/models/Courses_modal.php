<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
Author Salman Iqbal
Company Parexons
Date 26/1/2017
*/

class Courses_modal extends CI_Model
{


    function __construct()
    {
        // Set table name
        $this->table = 'courses';
        // Set orderable column fields
        $this->column_order = array(null, null, $this->table . '.status',  $this->table . '.name',  $this->table . '.long_title', 'course_categories', 'course_tags', 'system_category', 'system_tags', 'enrolled_users', 'institutions', 'created');
        // Set searchable column fields
        $this->column_search = array($this->table . '.name', $this->table . '.long_title');
        // Set default order
        $this->order = array('id' => 'desc');
        //$this->user_container_id = get_container_id();       
    }

    /*
     * Fetch members data from the database
     * @param $_POST filter data based on the posted parameters
     */
    public function getRows($postData)
    {
        $this->_get_datatables_query($postData);

        if ($postData['length'] != -1) {
            $this->db->limit($postData['length'], $postData['start']);
        }
        $query = $this->db->get();

        $data['result'] = $query->result_array();

        // pr($this->db->last_query(), 'd');

        $data['total'] = $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total;
        //pr($data);
        return $data;
    }

    //update status
    public function update_status($id = null, $data)
    {
        $this->db->where('id', $id);
        $delete = $this->db->update($this->table, $data);
        if ($delete) :
            return true;
        endif;
    }


    // Function to get max id
    public function getMaxId()
    {
        /*$this->db->select('MAX(id) + 1 AS maxid');
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->result_array(); */
        $query = $this->db->query('SELECT AUTO_INCREMENT AS maxid FROM information_schema.TABLES WHERE TABLE_SCHEMA = "tel-library" AND TABLE_NAME = "courses"');
        return $query->result_array();
    }

    /*
     * Perform the SQL queries needed for an server-side processing requested
     * @param $_POST filter data based on the posted parameters
     */
    private function _get_datatables_query($postData)
    {
        $current_container_id = get_contener_id();
        $this->db->select('SQL_CALC_FOUND_ROWS ' . $this->table . '.id, ' . $this->table . '.code, ' . $this->table . '.name, ' . $this->table . '.long_title,' . $this->table . '.status', false);
        $this->db->select('DATE_FORMAT(' . $this->table . '.created, "%m/%d/%Y") as created');

        if ((isset($postData['course_cat']) && count($postData['course_cat']) > 0) || $postData['order']['0']['column'] == 5) {
            $this->db->select('GROUP_CONCAT( DISTINCT course_categories.name SEPARATOR \'<br> \')  as course_categories');
        } else {
            $this->db->select('"..." as course_categories');
        }
        if ((isset($postData['course_tag']) && count($postData['course_tag']) > 0) || $postData['order']['0']['column'] == 6) {
            $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT course_tags.name ORDER BY course_tags.name ASC  SEPARATOR "<br>") , \'NA\') as course_tags');
        } else {
            $this->db->select('"..." as course_tags');
        }


        // system metadata
        if ((isset($postData['system_cat']) && count($postData['system_cat']) > 0) || $postData['order']['0']['column'] == 7) {
            $this->db->select('GROUP_CONCAT( DISTINCT system_category.name SEPARATOR \'<br> \')  as system_category');
            /*Tags*/
        } else {
            $this->db->select('"..." as system_category');
        }
        //System Tags   
        if ((isset($postData['system_tag']) && count($postData['system_tag']) > 0) || $postData['order']['0']['column'] == 8) {
            $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT system_tags.name ORDER BY system_tags.name ASC  SEPARATOR "<br>"), \'NA\')  as system_tags');
        } else {
            $this->db->select('"..." as system_tags');
        }
        if ($postData['order']['0']['column'] == 9) {
            $this->db->select('COUNT(DISTINCT(container_courses.course_id)) as enrolled_users');
        } else {
            $this->db->select('"..." as enrolled_users');
        }
        if ($postData['order']['0']['column'] == 10) {
            $this->db->select('GROUP_CONCAT( DISTINCT containers.name SEPARATOR \'<br> \')  as institutions');
        } else {
            $this->db->select('"..." as institutions');
        }
        
        // current enrollment


        $this->db->from($this->table);

        if ((isset($postData['system_cat']) && count($postData['system_cat']) > 0) || $postData['order']['0']['column'] == 7) {
            $this->db->join('category_assigned as assigned_system_category', 'assigned_system_category.reference_id = courses.id AND assigned_system_category.reference_type = ' . META_SYSTEM . ' AND  assigned_system_category.reference_sub_type =' . META_COURSE, 'left');
            $this->db->join('categories as system_category', 'system_category.id = assigned_system_category.category_id AND system_category.delete_status = 1 AND system_category.status = 1', 'left');
        }
        if ((isset($postData['system_tag']) && count($postData['system_tag']) > 0) || $postData['order']['0']['column'] == 8) {
            $this->db->join('tag_assigned as assigned_system_tags', 'assigned_system_tags.reference_id = courses.id AND assigned_system_tags.reference_type = ' . META_SYSTEM . ' AND  assigned_system_tags.reference_sub_type =' . META_COURSE, 'left');
            $this->db->join('tags as system_tags', 'system_tags.id = assigned_system_tags.tag_id AND system_tags.delete_status = 1 AND system_tags.status = 1', 'left');
        }
        if ((isset($postData['course_cat']) && count($postData['course_cat']) > 0) || $postData['order']['0']['column'] == 5) {
            $this->db->join('category_assigned as assigned_course_category', 'assigned_course_category.reference_id = courses.id AND assigned_course_category.reference_type = ' . META_COURSE . ' AND  assigned_course_category.reference_sub_type =' . META_COURSE, 'left');
            $this->db->join('categories as course_categories', 'course_categories.id = assigned_course_category.category_id AND course_categories.delete_status = 1 AND course_categories.status = 1', 'left');
        }
        if ((isset($postData['course_tag']) && count($postData['course_tag']) > 0) || $postData['order']['0']['column'] == 6) {
            $this->db->join('tag_assigned as assigned_course_tags', 'assigned_course_tags.reference_id = courses.id AND assigned_course_tags.reference_type = ' . META_COURSE . ' AND  assigned_course_tags.reference_sub_type =' . META_COURSE, 'left');
            $this->db->join('tags as course_tags', 'course_tags.id = assigned_course_tags.tag_id AND course_tags.delete_status = 1 AND course_tags.status = 1', 'left');
        }
        if ($postData['order']['0']['column'] == 9) {
            $this->db->join('container_courses', 'container_courses.course_id = ' . $this->table . '.id AND container_courses.status = 1', 'left');
            $this->db->join('course_assigned_to_users', 'course_assigned_to_users.course_id = ' . $this->table . '.id', 'left');
        }
        
        if ($postData['order']['0']['column'] == 10) {
            $this->db->join('container_courses', 'container_courses.course_id = ' . $this->table . '.id AND container_courses.status = 1', 'left');
            $this->db->join('containers', 'containers.id = container_courses.container_id', 'left');    
        }
        
        
        
        

        $this->db->where($this->table . '.delete_status = 1');

        $i = 0;
        // loop searchable columns 
        foreach ($this->column_search as $item) {

            // if datatable send POST for search
            if ($postData['search']['value']) {
                // first loop
                if ($i === 0) {
                    // open bracket 
                    $this->db->group_start();
                    //pr( $item);	
                    $this->db->like($item, $postData['search']['value'], 'both');
                } else {
                    $this->db->or_like($item, $postData['search']['value'], 'both');
                }
                // last loop
                if (count($this->column_search) - 1 == $i) {
                    // close bracket
                    $this->db->group_end();
                }
            }
            $i++;
        }

        // set users 
        if ($this->user_container_id > 0) {
            $this->db->where('course_assigned_to_users.container_id', $this->user_container_id);
        }

        // set system_cat 
        if (isset($postData['system_cat']) && $postData['system_cat'] > 0) {
            $this->db->where_in('assigned_system_category.category_id', $postData['system_cat']);
        }

        // set system_tag 
        if (isset($postData['system_tag']) && $postData['system_tag'] > 0) {
            $this->db->where_in('assigned_system_tags.tag_id', $postData['system_tag']);
        }

        if (isset($postData['course_cat']) && $postData['course_cat'] > 0) {
            $this->db->where_in('assigned_course_category.category_id', $postData['course_cat']);
        }

        if (isset($postData['course_tag']) && $postData['course_tag'] > 0) {
            $this->db->where_in('assigned_course_tags.tag_id', $postData['course_tag']);
        }

        if (isset($postData['status']) && $postData['status'] < 2) {
            $this->db->where($this->table . '.status', $postData['status']);
        }

        $this->db->group_by($this->table . '.id');

        if (isset($postData['order'])) {
            $this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_current_enrollments_count($container_id = null)
    {
        $sql = "SELECT COUNT(DISTINCT c.id) as enrolled_courses from container_courses 
        JOIN containers AS c ON (c.id = container_courses.container_id)
        WHERE container_courses.container_id = " . $container_id . ' group by container_courses.container_id';
        $query =  $this->db->query($sql);
        return ($query->row()->enrolled_courses) ? $query->row()->enrolled_courses : '0';
    }

    public function get_current_enrollments($container_id = null)
    {
        $sql = "SELECT COALESCE(GROUP_CONCAT(DISTINCT c.name ORDER BY c.name ASC SEPARATOR '<br>'), 'NA')  as name from container_courses 
        JOIN containers AS c ON (c.id = container_courses.container_id)
        WHERE container_courses.container_id = " . $container_id . ' group by container_courses.container_id';
        $query =  $this->db->query($sql);
        return ($query->row()->name) ? $query->row()->name : 'NA';
    }

    

    /*
    *getCheckExist this method is used to check data is already exist or not
    * @param string
    * @return string 
    */
    public function getCount($condition = [], $table)
    {
        $this->db->where($condition);
        if ($query = $this->db->get($table)) {
            return $query->num_rows();
        }
    }
}
