<?php

defined('BASEPATH') or exit('No direct script access allowed');



class Certificate extends MY_Controller {

    
    public function __construct() {

        parent::__construct();
        //Do your magic here
        $this->load->module('template');
        $this->load->model('common_model');
        $this->load->model('Certificates_model');
        $this->load->library('form_validation');
        $this->load->helper(array('html', 'language', 'form', 'country_helper'));
        if (!$this->ion_auth->logged_in()) :
            redirect('users/auth', 'refresh');
        endif;

        if (!$this->ion_auth->is_admin()) :
            return show_error("You Must Be An Administrator To View This Page");
        endif;
    }

    /**

     * @return [void]
     */
    public function index() {


        if (!$this->ion_auth->is_admin()) {
            return show_error('You Must Be an Administrator To view This Page');
        }

        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Certificates', 'link' => '', 'class' => 'active'];
        $data['certificates'] = $this->Certificates_model->certificates_list();
        //  exit('dfd00'); exit;
        $data['page'] = "certificates/Certificates/view";
        $this->template->template_view($data);
    }

    //save certificates page
    public function save() {
        if (!$this->ion_auth->is_admin()) {
            return show_error('You Must Be an Administrator To view This Page');
        }

        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Certificates', 'link' => base_url('/certificates/Certificate'), 'class' => 'active'];
        if ($this->uri->segment(4) == '') {
            $data['page'] = 'certificates/Certificates/save';
                    $data['breadcrumb'][]           = ['title' => 'Add', 'link' => '', 'class' => 'active'];

            $this->template->template_view($data);
        } else {
            $data['data'] = $this->Certificates_model->edit($this->uri->segment(4));
            $data['id'] = $this->uri->segment(4);
            $data['certificates_options'] = $this->Certificates_model->get_category_list($this->uri->segment(4));
            $data['certificates_options'] = $cat;
            $data['certificates_data'] = $this->Certificates_model->edit($this->uri->segment(4));
            $data['page'] = 'certificates/Certificates/save';
            $data['breadcrumb'][]           = ['title' => 'Edit', 'link' => '', 'class' => 'active'];

            $this->template->template_view($data);
        }
    }

  public function detail() {
        $this->benchmark->mark('code_start');
        if (!$this->ion_auth->is_admin()) {
            return show_error('You Must Be an Administrator To view This Page');
        }
        if ($this->uri->segment(4) == '') {
            $data['page'] = 'certificates/Certificates/detail';
            $this->template->template_view($data);
        } else {
            $data['data'] = $this->Certificates_model->edit($this->uri->segment(4));
            $data['id'] = $this->uri->segment(4);
            $data['certificate_data'] = $this->Certificates_model->edit($this->uri->segment(4));
            $data['page'] = 'certificates/Certificates/detail';
            $this->template->template_view($data);
        }  
    }

    /**
     * [Add New Poll]
     */
    public function add() {
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        // $this->form_validation->set_rules('type', 'Type', 'trim|required');
        //   $this->form_validation->set_rules('description', 'Description', 'trim|required');
        //pr(); exit;
        if ($this->form_validation->run() == false) {
            $msg = 'Fill form correctly.';
            $this->session->set_flashdata('error', $msg);
        } else {

            $data = array(
                'created_by' => $this->session->userdata('user_id'),
                'name' => post('name'),
                'body' => post('body'),
                'status' => 1,
                'delete_status' => 1,
                'created' => date("Y-m-d H:i:s"),
            );
            if (post(id) != '') {
                $where = array(
                    "id" => post(id)
                );
                $this->common_model->UpdateDB('certificates', $where, $data); //update data
                //pr(vd(),"h");
                set_flashdata('success', 'Certificate Updated Successfully');
            } else {
                $ins = $this->common_model->InsertData('certificates', $data); //insert data
                set_flashdata('success', 'Certificate Added Successfully');
            }
        }
        redirect('certificates/Certificate/index', 'refresh');
    }

    //delete certificates
    public function delete() {
        $this->session->set_flashdata('success', $this->ion_auth->messages());
        if ($this->input->post('status') == '') {
            $status = 0;
        } else {
            $status = 1;
        }
        $additional_data = array(
            'delete_status' => $status,
        );
        $this->Certificates_model->update($this->uri->segment(4), $additional_data);

        $msg = "deleted";
        echo json_encode(array("msg" => $msg));
        exit;
    }

    //update certificates status
    public function update_status($id, $action) {
        $status = ($action == 'activate') ? 1 : 0;
        $data = array('status' => $status);
        $this->Certificates_model->update_status($id, $data);
        $msg = "Updated";
        echo json_encode(['msg' => $msg]);
        exit;
    }

    function getLists() {

        $Data = $this->Certificates_model->getRows($_POST);
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $Data['total'],
            "recordsFiltered" => $Data['total'],
            "data" => $Data['result'],
        );
        echo json_encode($output);
    }

    function generatePdf($cerId){
        ini_set('display_errors', 1);
        if (!$this->ion_auth->is_admin()) {
            return show_error('You Must Be an Administrator To view This Page');
        }


        $data = $this->Certificates_model->edit($cerId);
        
        // Load all views as normal
        $this->load->view('Certificates/certificatepdf');

        // Get output html 
        $html = $this->output->get_output();
        
        // Load library
        $this->load->library('dompdf_gen');
        
        // Convert to PDF
        $this->dompdf->load_html($html); 
        $this->dompdf->render();
        $this->dompdf->stream('welcome.pdf', array('Attachment' => false));
    }

}

/* End of file Posts.php */
/* Location: ./application/modules/blog/controllers/Posts.php */
