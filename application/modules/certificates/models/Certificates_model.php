<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
  Author Akash Hedaoo
  Date 29/04/2019
 */

class Certificates_model extends CI_Model {

    function __construct() {
        // Set table name
        $this->table = 'certificates';
        // Set orderable column fields
        $this->column_order = array('id', 'name', 'created', 'created_by', 'status', 'delete_status');
        // Set searchable column fields
        $this->column_search = array('certificates.name', 'certificates.created');
        // Set default order
        $this->order = array('certificates.created' => 'desc');
    }

    /*
     * Fetch data from the database
     * @param $_POST filter data based on the posted parameters
     */

    public function certificates_list() {
        $this->db->where('delete_status', 1);
        $query = $this->db->get('certificates');
        return $query->result();
    }

    /*
     * Fetch category data from the database
     * @param $_POST filter data based on the posted parameters
     */

    public function get_category_list() {
        $array = array('type' => 3, 'status' => 1);
        $this->db->select('id,name');
        $this->db->where($array);
        $query = $this->db->get('categories');
        return $query->result_array();
    }

    /*
     * Fetch data from the database
     * @param $id filter data based on the particular id from url
     */

    public function edit($id = null) {
        $this->db->where('id', $id);
        $query = $this->db->get('certificates');
        return $query->row();
    }

    /*
     * Update data into the database
     * @param $id filter data based on the particular id from url
     */

    public function update($id = null, $data) {
        $this->db->where('id', $id);
        $update = $this->db->update('certificates', $data);
        if ($update):
            return true;
        endif;
    }

    /*
     * update delete_status for active inactive skills from the database
     * @param $id filter data based on the particular id from url
     */

    public function update_status($id = null, $data) {
        $this->db->where('id', $id);
        $delete = $this->db->update('certificates', $data);
        if ($delete):
            return true;
        endif;
    }

    /*
     * Fetch members data from the database
     * @param $_POST filter data based on the posted parameters
     */

    public function getRows($postData) {
        //pr($postData); exit;
        $this->_get_datatables_query($postData);

        if ($postData['length'] != -1) {
            $this->db->limit($postData['length'], $postData['start']);
        }
        $query = $this->db->get();
        $data['result'] = $query->result();
        $data['total'] = $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total;
        return $data;
    }

    /*
     * Perform the SQL queries needed for an server-side processing requested
     * @param $_POST filter data based on the posted parameters
     */

    private function _get_datatables_query($postData) {
        $this->db->select('SQL_CALC_FOUND_ROWS ' . 'certificates.id,name,status,created, CONCAT_WS(" ", users.first_name, users.middle_name, users.last_name) as created_by', false);
        //  $this->db->select("IF(".$this->table.".status = 1, 'Publish', 'Unpublish') as status");
        $this->db->join('users', $this->table . '.created_by = users.id');
        $this->db->select('DATE_FORMAT(' . $this->table . '.created, "%m/%d/%Y") as created');
        $this->db->where('certificates.delete_status', 1);
        $this->db->from('certificates');


        $i = 0;
        // loop searchable columns 
        foreach ($this->column_search as $item) {
            // if datatable send POST for search
            if ($postData['search']['value']) {
                // first loop
                if ($i === 0) {
                    // open bracket
                    $this->db->like($item, $postData['search']['value']);
                } else {
                    $this->db->or_like($item, $postData['search']['value']);
                }
            }
            $i++;
        }
        if (isset($postData['order'])) {
            $this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    //Count certificates
    public function count_certificates() {
        $this->db->select('*');
        $this->db->from('certificates');
        return $this->db->count_all_results();
    }

    /*
     * Count records based on the filter params
     * @param $_POST filter data based on the posted parameters
     */

    public function countFiltered($postData) {
        $this->_get_datatables_query($postData);
        $query = $this->db->get();
        return $query->num_rows();
    }

}

/* End of file Users_modal.php */
