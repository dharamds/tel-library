

            <div class="container-fluid">
                <div class="panel panel-info" data-widget='{"draggable": "false"}'>
                    <div class="panel-heading">
                        <h2><i class="fa fa-user"></i><?php echo (!empty($id) ? 'Edit Certificate' : 'Create Certificate')  ?></h2>
                        <div class="panel-ctrls" data-actions-container="" data-action-collapse='{"target": ".panel-body"}'></div>
                    </div>
                    <div class="panel-body">
                        <?= form_open('certificates/Certificate/add', array('id' => 'certificate_form_validation', 'class' => 'form-horizontal')); ?>
                        <input type="hidden" name="id" value="<?php echo $id ?>" >
                        <div class="form-group col-md-12">
                            <div class="col-md-6">
                                <label for="fieldname" class="col-md-12 control-label">Name</label>
                                <?php echo form_input([
                                    'name' => 'name',
                                    'id' => 'name',
                                    'class' => 'form-control editor',
                                    'value' => $data->name
                                ]); ?>
                                <?php echo form_error('template', '<div class="error">', '</div>'); ?>
                            </div>                              
                        </div>
                        <div class="form-group col-md-12">

                            <div class="col-md-6">
                                <label for="fieldname" class="col-md-12 control-label">Body</label>
                                <?php echo form_textarea([
                                    'name' => 'body',
                                    'id' => 'body',
                                    'class' => 'form-control editor',
                                    'value' => $data->body
                                ]); ?>
                                <?php echo form_error('learning_outcomes', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-md-6">
                                <input style="margin-left:15px;" type="submit" class="finish btn-success btn" value="Save">
                            </div>
                            </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        
<!-- #page-content -->
<script>
    $(document).ready(function () {
        
        jQuery.validator.addMethod("noSpace", function (value, element, param) {
            return value.match(/^(?=.*\S).+$/);
        }, "No space please and don't leave it empty");

        CKEDITOR.replace('body', {
            toolbar: 'short',
        });

        $("#certificate_form_validation").validate({
            
             ignore: [],
            
            rules: {
                name: {
                    required: true,
                    noSpace: true
                },
                body: {
                    required: function (textarea) {
                        CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                        return editorcontent.length === 0;
                    }
                }
            },
            messages: {
                name: {
                    required: "Please enter name"
                },
                body: {
                    required: "Please enter certificate body"
                }
            },
                errorPlacement: function(error, $elem) {
            if ($elem.is('textarea')) {
                $elem.insertAfter($elem.next('div'));
            }
            error.insertAfter($elem);
        },
        });
    });
</script>