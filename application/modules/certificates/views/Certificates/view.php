
            <div class="container-fluid">

                <br>
                <div data-widget-group="group1">
                    <div class="row">
                        <div class="col-md-12">
                            <a class="btn btn-success" href="<?= base_url('certificates/certificate/save') ?>">New</a>
                        </div>
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h2>Certificates Records</h2>
                                    <div class="panel-ctrls"></div>
                                    <a href="<?= bs('users/print_with_dompdf') ?>">
                                        <i class="fa fa-print" style="padding-left: 1%;color: black"></i>
                                    </a>
                                </div>
                                <div class="panel-body no-padding">


                                    <table id="memListTable" class="table table-bordered table-striped table-hover" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>Sr.</th>
                                                <th style="min-width: 200px;">Name</th>
                                                <th style="min-width: 100px;">Created By</th>
                                                <th style="min-width: 100px;">Created At</th>
                                                <th style="min-width: 100px;">Status</th>
                                                <th style="min-width: 100px;">Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- .container-fluid -->
        
<script>
    $(document).ready(function () {
        var table = table = table = $('#memListTable').DataTable({
            // Processing indicator
            "processing": true,
            // DataTables server-side processing mode
            "serverSide": true,
            // Initial no order.
            "iDisplayLength": 10,
            "bPaginate": true,
            "order": [],
            "scrollX": true,
            "autoWidth": false,
            // Load data from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('certificates/Certificate/getLists'); ?>",
                "type": "POST",
                "data": function (data) {
                },
            },
            //Set column definition initialisation properties
            "columnDefs": [{
                    "targets": 4,
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                            if (full['status'] == '1') {
                                data = '<a href="<?php bs('certificates/Certificate/update_status/') ?>' + full['id'] + '/deactivate" data-toggle="tooltip" data-placement="top" title="Click to Change Status" class="btn btn-success btn-status btn-sm change_status">Active</a>';
                            } else {
                                data = '<a href="<?php bs('certificates/Certificate/update_status/') ?>' + full['id'] + '/activate" data-toggle="tooltip" data-placement="top" title="Click to Change Status" class="btn btn-danger btn-status btn-sm change_status">Inactive</a>';
                            }
                        }
                        return data;

                    }

                },
                {
                    "targets": 5,
                    "data": null,
                    "render": function (data, type, full, meta) {
                        
                        if (type === 'display') {
                            data = '<a class="btn btn-primary btn-sm" href="<?php echo bs('certificates/Certificate/save/') ?>' + full['id'] + '"><i class="ti ti-pencil"></i></a>';
                            url = '<?php echo bs('certificates/Certificate/delete/') ?>' + full['id'];
                            
                            data += '<a href="<?php echo bs('certificates/Certificate/detail/') ?>' + full['id'] + '" class="btn btn-warning btn-sm " ><i class="ti ti-eye"></i></a>';
                            data += '<a title="Download Pdf" target="_blank" href="<?php echo bs('certificates/Certificate/generatePdf/') ?>' + full['id'] + '" class="btn btn-info btn-sm " ><i class="ti ti-eye"></i></a>';
                            data += '<a href="' + url + '" class="btn btn-danger btn-sm delete_item" ><i class="ti ti-trash"></i></a>';
                            
                            
                        }
                        return data;

                    }

                }
            ],
            "columns": [
                {
                    "data": "sr_no", render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                },
                {"data": "name"},
               {"data": "created_by"},
               {"data": "created"},
            ]
        });
        $(document).on('click', '.change_status', function (e) {
                    e.preventDefault();
                          $.get($(this).attr("href"), // url
                                function (data, textStatus, jqXHR) { // success callback
                                    var obj = JSON.parse(data);
                                    if (obj.msg === 'Updated') {
                                        table.ajax.reload();  //just reload table
                                    }                                });
        });

        $(document).on('click', '.delete_item', function (e) {
            e.preventDefault();
            var scope = $(this);
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure?',
                buttons: {
                    confirm: function () {
                        $.get(scope.attr("href"), // url
                                function (data, textStatus, jqXHR) { // success callback
                                    var obj = JSON.parse(data);
                                    if (obj.msg === 'deleted') {
                                        table.ajax.reload();  //just reload table
                                    }
                                });
                        return true;
                    },
                    cancel: function () {
                        return true;
                    }
                }
            });
        });
    });
</script>