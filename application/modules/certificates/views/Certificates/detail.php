

<div class="static-content-wrapper">
   <div class="static-content">
      <div class="page-content">
         <ol class="breadcrumb">
             <li class=""><a href="<?php echo base_url() ?>">Home</a></li>
                <li class=""><a href="<?php echo base_url('certificates/Certificate/') ?>">Certificate</a></li>
                <li class="active"><a href=""> Certificate Details </a></li>
         </ol>
         <div class="container-fluid">
            <div class="tab-pane active" id="tab-about">
               <div class="panel panel-default">
                  <div class="panel-heading">
                     <h2>Certificate Details </h2>
                  </div>
                  <div class="panel-body">
                     <div class="about-area">
                        <div class="table-responsive">
                           <table class="table about-table">
                              <tbody>
                                  
                                 <tr>
                                    <th>Name</th>
                                    <td width="5%"></td>
                                    <td><?php 
                                          echo $data->name;?>
                                    </td>
                                 </tr>
                                 
                                 <tr>
                                    <th>Description</th>
                                    <td width="5%"></td>
                                    <td><?php echo $data->body;?></td>
                                 </tr> 
                                 
                                 <tr>
                                    <th>Created by</th>
                                    <td width="5%"></td>
                                    <td><?php 
                                       $authorData = $this->ion_auth->user($certificate_data->created_by)->row(); 
                                       echo ucfirst($authorData->first_name); echo " ";  
                                       echo ucfirst($authorData->last_name);
                                       ?></td>
                                 </tr>  
                                 
                                 <tr>
                                    <th>Created on</th>
                                    <td width="5%"></td>
                                    <td><?php echo date('m-d-Y',strtotime($certificate_data->created)); ?></td>
                                 </tr>
                                 
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- .container-fluid -->
</div>
<!-- #page-content -->
