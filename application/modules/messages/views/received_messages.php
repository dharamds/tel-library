<div class="container-fluid">
    <div data-widget-group="group1">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default panel-grid">
                    <div class="panel-body pl-0 pr-0">
                        <div class="row m-0">
                            <div class="col-md-12">
                                <table id="messagesListTable" class="table table-bordered table-striped table-hover" cellspacing="0" width="100%">
                                    <colgroup>
                                        <col width="5%">
                                        <col width="15%">
                                        <col width="15%">
                                        <col width="45%">
                                        <col width="10%">
                                        <col width="10%">
                                    </colgroup>
                                    <thead>
                                        <tr>
                                            <th>Sr.</th>
                                            <th>Sender</th>
                                            <th>Receiver</th>
                                            <th>Message</th>
                                            <th>Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!----Message Modal----->
<div class="modal fade" id="reply_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-share-square" aria-hidden="true"></i> Send Reply</h4>
            </div>
            <div class="modal-body">
                <div class="form-role">
                        <h4>Message : <label id="popup_message"> </label></h4>
                    </div>
                <form method="post" action="<?php bs('messages/messages/send_reply') ?>" id="send_reply">
                    <div class="form-role">
                        <label>Reply</label>
                        <textarea name="msg" class="form-control" rows="10" cols="10" placeholder="Write You Reply" required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="reply_submit_button" ><i class="fa fa-paper-plane" aria-hidden="true"></i>
                    Send</button>
                </div>
                <input type="hidden" name="reply_user_id" id="reply_user_id">
            </form>
        </div>
    </div>
</div>
<!----Message Modal----->

<script type="text/javascript">
    $(document).ready(function() {
        
        var table = table = $('#messagesListTable').DataTable({
            // Processing indicator
            "processing": true,
            // DataTables server-side processing mode
            "serverSide": true,
            // Initial no order.
            "iDisplayLength": 10,
            "bPaginate": true,
            "order": [],
            // Load data from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('messages/getlistings_received'); ?>",
                "type": "POST",
                "data": function(data) {
                    data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                },
            },
            "columnDefs": [{
                "targets": 5,
                "data": null,
                "orderable": false,
                "render": function(data, type, full, meta) {
                    if (type === 'display') {
                          data = '<a data-toggle="tooltip" title="view user" href="<?= base_url('messages/view_messages/') ?>' +
                                    full['id'] +
                                    '" class="btn btn-warning btn-sm"><i class="ti ti-eye"></i></a>';
                        data += '<a style="margin-left:8px;" id="' + full['id'] + '_' + full['sender_id'] + '" title="Reply" href="#" onclick="set_user_id(this.id,\''+full['body']+'\')" class="btn btn-info btn-sm" data-toggle="modal" data-target="#reply_modal"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>';
                        
                    }
                    return data;
                }
            }],
            "columns": [{
                    "data": "sr_no",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                    "autoWidth": true
                },
                {
                    "data": "sender",
                    "autoWidth": true
                },
                {
                    "data": "receiver",
                    "autoWidth": true
                },
                {
                    "data": "body",
                    "autoWidth": true
                },

                {
                    "data": "created",
                    "autoWidth": true
                }
            ]
        });


        $("#send_reply").validate({
				ignore: [],
				rules: {
					msg: {
						required: true
					},
				},
				messages: {
					msg: "Please enter message."
				},
				errorPlacement: function(error, $elem) {
					if ($elem.is('textarea')) {
						$elem.insertAfter($elem.next('div'));
					}
					error.insertAfter($elem);
				},
				submitHandler: function(form) {
					$('#reply_submit_button').hide();
					$.ajax({
						url: "<?php echo bs("messages/messages/send_reply") ?>",
						method: "POST",
						data: $('form#send_reply').serialize(),
						dataType: "json",
		
						complete: function(xhr, status) {
							$('#reply_modal').modal('hide');
							getDisplayMessgae(200, 'Reply sent successfully.');
							$('#reply_submit_button').show();
						},
						success: function(data) {
							console.log(data);
							
						}
					});
					return false;
				}
		
			});

    });
    function set_user_id(user_id,message){
        $('#reply_user_id').val(user_id);
        $('#popup_message').html('');
        $('#popup_message').append(message);
        $.ajax({
						url: "<?php echo bs("messages/messages/read_reply_status") ?>",
						method: "POST",
						data: 'reply_user_id='+$('#reply_user_id').val(),
						dataType: "json",
		
						complete: function(xhr, status) {
							
						},
						success: function(data) {
							console.log(data);
							
						}
					});
    }
</script>
