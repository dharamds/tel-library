            <div class="container-fluid">
                <div data-widget-group="group1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default panel-grid">
                                <div class="panel-body pl-0 pr-0">
                                    <div class="row m-0">
                                        <div class="col-md-12">
                                            <table id="messagesListTable" class="table table-bordered table-striped table-hover" cellspacing="0" width="100%">
                                                <colgroup>
                                                    <col width="5%">
                                                    <col width="20%">
                                                    <col width="20%">
                                                    <col width="45%">
                                                    <col width="10%">
                                                </colgroup>
                                                <thead>
                                                    <tr>
                                                        <th>Sr.</th>
                                                        <th>Sender</th>
                                                        <th>Receiver</th>
                                                        <th>Message</th>
                                                        <th>Date</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>


            <script type="text/javascript">
                $(document).ready(function() {

                    var table = table = $('#messagesListTable').DataTable({
                        // Processing indicator
                        "processing": true,
                        // DataTables server-side processing mode
                        "serverSide": true,
                        // Initial no order.
                        "iDisplayLength": 10,
                        "bPaginate": true,
                        "order": [],
                        // Load data from an Ajax source
                        "ajax": {
                            "url": "<?php echo base_url('messages/getlistings_sent'); ?>",
                            "type": "POST",
                            "data": function(data) {
                                data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                            },
                        },
                        "columns": [{
                                "data": "sr_no",
                                render: function(data, type, row, meta) {
                                    return meta.row + meta.settings._iDisplayStart + 1;
                                },
                                "autoWidth": true
                            },
                            {
                                "data": "sender",
                                "autoWidth": true
                            },
                            {
                                "data": "receiver",
                                "autoWidth": true
                            },
                            {
                                "data": "body",
                                "autoWidth": true
                            },
                            
                            {
                                "data": "created",
                                "autoWidth": true
                            }
                        ]
                    });




                });
            </script>