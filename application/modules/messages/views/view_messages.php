      <div class="container-fluid">
         <div data-widget-group="group1">
            <div class="row">
              
               <!-- col-sm-3 -->
               <div class="col-sm-12">
                  <div class="tab-content">
                  
                     <!-- #tab-projects -->
                     <div class="tab-pane active" id="tab-about">
                        <div class="panel panel-default panel-grid">
                           <div class="panel-heading brd-0 m-0 pt-1"></div>
                           <div class="panel-body">
                              <div class="about-area">
                                 <h4>Message Detail</h4>
                                 <div class="table-responsive">
                                    <table class="table about-table">
                                       <tbody>
                                          <tr>
                                             <th>Subject</th>
                                             <td><?php echo $message->subject; ?></td>
                                          </tr>
                                          <tr>
                                             <th>Message</th>
                                              <td><?php echo $message->body; ?></td>
                                          </tr>

                                          <tr>
                                             <th>Created Date</th>
                                             <td><?php echo date('d-m-Y', strtotime($message->created)) ?></td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>                     
                     
                  </div>
                  <!-- .tab-content -->
               </div>
               <!-- col-sm-8 -->
            </div>
         </div>
