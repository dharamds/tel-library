<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Messages extends MY_Controller {

    public function __construct() {

        parent::__construct();
        //Do your magic here

        $this->load->module('template');
        $this->load->model('Message_modal');
        $this->load->model('common_model');
        $this->load->helper(['html', 'form']);
        $this->load->library('form_validation');


        if (!$this->ion_auth->logged_in()) :
            redirect('users/auth', 'refresh');
        endif;
    }

    /**
     * setup method
     * @description this function use to create container
     * @return void
     */
    public function sent_messages() {
        $data['breadcrumb'][] = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][] = ['title' => 'Messages', 'link' => '', 'class' => ''];
        $data['page'] = "Messages/sent_messages";

        $this->template->template_view($data);
    }

    public function view_messages($id = null) {
        $data['breadcrumb'][] = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][] = ['title' => 'Messages', 'link' => '', 'class' => ''];
        $select = $this->common_model->getDataById('messages', '*', array("id" => $id));
        $data['message'] = $select;
        $data['page'] = "Messages/view_messages";
        $upData = ['is_read' => 1];
        $this->common_model->UpdateDB('messages', array('id' => $id), $upData);
        $this->template->template_view($data);
    }

    public function getUserMessages() {
        $userId = trim($this->session->userdata('user_id'));
        $select = $this->common_model->getAllData('messages', '*', '', array("receiver_id" => $userId), '', 10);
        $countSelect = $this->common_model->getAllData('messages', '*', '', array("receiver_id" => $userId, "is_read" => 0), 'id desc', 10);
        $count = count($countSelect);
        $arr['count'] = $count;
        $url = 'http://192.168.1.74:7777/tel_library/messages/view_messages/';
        $arrIds = array();
        $html = '';
        $html .= ' <ul class="media-list scroll-content">';
        if (!empty($select)) {
            foreach ($select as $key => $val) {
                array_push($arrIds, $val->id);
                if ($val->is_read == 0) {
                    $html .= '<li class="media notification-message notification-unread">';
                } else {
                    $html .= '<li class="media notification-message notification-read">';
                }
              
                $html .= '<a href=' . $url . '' . $val->id . '><div class="media-left"><i class="flaticon-ring"></i></div><div class="media-body">' . $val->subject . '</div></a>';
                $html .= '</li>';
            }
        } else {
            $html .= '<div class="topnav-dropdown-footer"><a href="#" style="color:red;">No Messages</a></div>';
        }
        $html .= '</ul>';
        $arr['html'] = $html;
        $arr['messageArr'] = $arrIds;
        $arr['list'] = $select;
        echo json_encode($arr);
        exit;
    }

    public function readMessages() {
        $arr = explode(',', $this->input->post('ids'));
        $this->db->where_in("id", $arr);
        $this->db->update('messages', array('is_read' => 1));
        echo json_encode(['msg' => 'success']);
        exit;
    }

    // list users as per filter
    public function getlistings_sent() {
        // Fetch member's records
        $userData = $this->Message_modal->getRows_sent($_POST);
        //gkc on dated 28th May , 2019
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $userData['total'],
            "recordsFiltered" => $userData['total'],
            "data" => $userData['result'],
        );
        // Output to JSON format
        echo json_encode($output);
        exit;
    }

    /**
     * setup method
     * @description this function use to create container
     * @return void
     */
    public function received_messages() {
        $data['breadcrumb'][] = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][] = ['title' => 'Messages', 'link' => '', 'class' => ''];
        $data['page'] = "Messages/received_messages";

        $this->template->template_view($data);
    }

    // list users as per filter
    public function getlistings_received() {
        // Fetch member's records
        $userData = $this->Message_modal->getRows_received($_POST);
        //gkc on dated 28th May , 2019
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $userData['total'],
            "recordsFiltered" => $userData['total'],
            "data" => $userData['result'],
        );
        // Output to JSON format
        echo json_encode($output);
        exit;
    }

    // send reply
    public function send_reply() {
        $exp = explode("_", $this->input->post('reply_user_id'));

        $data = array(
            'parent_id' => $exp[0],
            'sender_id' => $_SESSION['user_id'],
            'receiver_id' => $exp[1],
            'subject' => '',
            'body' => $this->input->post('msg'),
            'is_read' => '0',
            'created' => date('Y-m-d H:i:s'),
        );
        if ($this->common_model->InsertData('messages', $data)) {
            return true;
        } else {
            return false;
        }
    }

    // send reply
    public function read_reply_status() {
        $exp = explode("_", $this->input->post('reply_user_id'));
        $data = array(
            'is_read' => '1',
        );
        if ($this->common_model->UpdateDB('messages', array("id" => $exp[0]), $data)) {
            return true;
        } else {
            return false;
        }
    }

}
