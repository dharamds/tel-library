<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
Author Salman Iqbal
Company Parexons
Date 26/1/2017
*/

class Message_modal extends CI_Model
{

    function __construct()
    {
        // Set table name
        $this->table = 'messages';
        // Set orderable column fields
        $this->column_order = array('id', 'created');
        // Set searchable column fields
        $this->column_search = array('body');
        // Set default order
        $this->order = array('created' => 'desc');
        //$this->user_container_id = get_container_id();       
    }


    /*
     * Fetch members data from the database
     * @param $_POST filter data based on the posted parameters
     */
    public function getRows_sent($postData)
    {

        $user_id = $this->ion_auth->user()->row()->id;
        $this->_get_datatables_query_sent($postData, $user_id);

        if ($postData['length'] != -1) {
            $this->db->limit($postData['length'], $postData['start']);
        }
        $query = $this->db->get();
        $data['result'] = $query->result_array();
        $data['total'] = $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total;
        //pr($data);
        return $data;
    }

    /*
     * Perform the SQL queries needed for an server-side processing requested
     * @param $_POST filter data based on the posted parameters
     */
    private function _get_datatables_query_sent($postData, $user_id)
    {
        $this->db->select('SQL_CALC_FOUND_ROWS ' . $this->table . '.id, SUBSTRING(' . $this->table . '.body,1,150) AS body, ' . $this->table . '.created, CONCAT_WS(" ", users.first_name, users.middle_name, users.last_name) as sender,CONCAT_WS(" ", receiver.first_name, receiver.middle_name, receiver.last_name) as receiver', false);
        $this->db->select('DATE_FORMAT(' . $this->table . '.created, "%m/%d/%Y") as created');
        //$this->db->select("IF(".$this->table.".priority = 1, 'High', 'Low') as priority");
        $this->db->from($this->table);
        //$this->db->join('container_courses', $this->table.'.id = container_courses.course_id');
        $this->db->join('users', $this->table . '.sender_id = users.id');
        $this->db->join('users as receiver', $this->table . '.receiver_id = receiver.id');
        //$this->db->where($this->table.'.status = 1');

        $i = 0;
        // loop searchable columns 
        foreach ($this->column_search as $item) {
            // if datatable send POST for search
            if ($postData['search']['value']) {
                // first loop
                if ($i === 0) {
                    // open bracket
                    $this->db->group_start();
                    $this->db->like($item, $postData['search']['value']);
                } else {
                    $this->db->or_like($item, $postData['search']['value']);
                }

                // last loop
                if (count($this->column_search) - 1 == $i) {
                    // close bracket
                    $this->db->group_end();
                }
            }
            $i++;
        }

        // set user 
        //$this->db->where($this->table.'.receiver_id', $user_id);


        
        $this->db->where($this->table . '.sender_id', $_SESSION['user_id']);

        if (isset($postData['order'])) {
            $this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    /*
     * Fetch members data from the database
     * @param $_POST filter data based on the posted parameters
     */
    public function getRows_received($postData)
    {

        $user_id = $this->ion_auth->user()->row()->id;
        $this->_get_datatables_query_received($postData, $user_id);

        if ($postData['length'] != -1) {
            $this->db->limit($postData['length'], $postData['start']);
        }
        $query = $this->db->get();
        //pr(vd(),"j");
        $data['result'] = $query->result_array();
        $data['total'] = $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total;
        //pr($data);
        return $data;
    }

    /*
     * Perform the SQL queries needed for an server-side processing requested
     * @param $_POST filter data based on the posted parameters
     */
    private function _get_datatables_query_received($postData, $user_id)
    {
        $this->db->select('SQL_CALC_FOUND_ROWS ' . $this->table . '.id, ' . $this->table . '.body, ' . $this->table . '.created, CONCAT_WS(" ", users.first_name, users.middle_name, users.last_name) as sender,sender_id,CONCAT_WS(" ", receiver.first_name, receiver.middle_name, receiver.last_name) as receiver', false);
        $this->db->select('DATE_FORMAT(' . $this->table . '.created, "%m/%d/%Y") as created');
        //$this->db->select("IF(".$this->table.".priority = 1, 'High', 'Low') as priority");
        $this->db->from($this->table);
        //$this->db->join('container_courses', $this->table.'.id = container_courses.course_id');
        $this->db->join('users', $this->table . '.sender_id = users.id','left');
        $this->db->join('users as receiver', $this->table . '.receiver_id = receiver.id','left');
        //$this->db->where($this->table.'.status = 1');

        $i = 0;
        // loop searchable columns 
        foreach ($this->column_search as $item) {
            // if datatable send POST for search
            if ($postData['search']['value']) {
                // first loop
                if ($i === 0) {
                    // open bracket
                    $this->db->group_start();
                    $this->db->like($item, $postData['search']['value']);
                } else {
                    $this->db->or_like($item, $postData['search']['value']);
                }

                // last loop
                if (count($this->column_search) - 1 == $i) {
                    // close bracket
                    $this->db->group_end();
                }
            }
            $i++;
        }

        // set user 
        //$this->db->where($this->table.'.receiver_id', $user_id);


        
        $this->db->where($this->table . '.receiver_id', $_SESSION['user_id']);

        if (isset($postData['order'])) {
            $this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
}
