<form id="ltiLaunchForm" name="ltiLaunchForm" method="POST" action="<?php printf($launch_url); ?>">
<?php foreach ($launch_data as $k => $v ) { ?>
  <input type="hidden" name="<?php echo $k ?>" value="<?php echo $v ?>">
<?php } ?>
  <input type="hidden" name="oauth_signature" value="<?php echo $signature ?>">
</form>
<iframe id="upload_target" name="upload_target" src="" style="width:100%; border:1px solid #ccc; min-height:800px;"></iframe>
<div class="col-sm-12 p-0 mt-5 mb-5">
    <div class="hr-line"></div>
</div>
<div class="row mb-3">
<div class="col-xs-12 p-0 text-right">
    <button type="button" class="btn btn-danger scroll-top click_to_back">Back</button>
    <button class="btn btn-primary scroll-top click_to_finish">Back to list</button>
</div>
</div>
<script type="text/javascript" src="<?php bs('public/assets/js/assessment/add-setting.js') ?>"></script>
<?php

$jsVars = [
    'ajax_call_root' => base_url(),
    'recordID' => (isset($recordID)?$recordID:0),
];    
?>
<script>
$( document ).ready(function() {
      document.getElementById('ltiLaunchForm').onsubmit=function() {
      document.getElementById('ltiLaunchForm').target = 'upload_target'; //'upload_target' is the name of the iframe    
}
$('#ltiLaunchForm').submit();
});
</script>