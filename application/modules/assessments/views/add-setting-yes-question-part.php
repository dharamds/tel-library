<link type="text/css" href="<?= bs('public/assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css')?>" rel="stylesheet"> 
<div class="col-xs-12 fib-response-panel">
    <h4>
        <i class="flaticon-checklist"></i> 
        Create assessment sections
    </h4>
</div>
<div class="col-xs-12 p-0 fib-left" id="count_section_start">
    <?php 
    for($i = 1; $i<=$sections_count; $i++) { ?>
        <div class="panel panel-response mb-4">
            <div class="panel-body">
                <div class="form-group col-xs-12 p-0 pt-3">
                    <label class="control-label col-xs-12 p-0"><!-- Section --> <?//=$i?> Title <span class="field-required">*</span></label>
                    <div class="col-xs-6 p-0">
                        <?php 
                        echo form_input([ 
                            'name' => 'section['.$i.'][title]',
                            'class' => 'form-control',
                            'id'  => 'title_'.$i,
                            'value' => (isset($sections[$i-1]->title))?$sections[$i-1]->title:''
                        ]);
                        ?>
                    </div>

                    <div class="col-xs-3 p-0 pt-3">
                        <div class="pull-right mt--10">
                            <div class="form-group flex-row flex-col-7 p-0 m-0 ml-auto justify-content-end">
                                <label class="control-label flex-col-5 p-0 pt-2">Weight %</label>
                                <div class="flex-col-5 p-0 m-0">
                                    <?php echo form_input([ 
                                      'name' => 'section['.$i.'][weight]',
                                      'class' => 'form-control touchspin4',
                                      'value' => (isset($sections[$i-1]->weight))?$sections[$i-1]->weight:0
                                  ]);?>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-xs-3 p-0 mt--25 pl-5">
                      <div class="mt--10 f12">
                          Total Number of section question to display to student
                      </div>
                      <div class="d-inline-block w100">
                          <?php echo form_input([ 
                            'name' => 'section['.$i.'][student_question_display]',
                            'class' => 'form-control touchspin4',
                            'data-question-display' => 'student_question_display',
                            'id'  => 'student_question_display_'.$i,
                            'value' => (isset($sections[$i-1]->student_question_display))?$sections[$i-1]->student_question_display:1
                          ]);?>
                      </div>
                  </div>


              </div>
              <div class="col-xs-12 p-0 pb-4">
                <?php echo form_textarea([ 
                  'name' => 'section['.$i.'][description]',
                  'id' => 'editor_'.$i,
                  'class' => 'form-control ckeditor_form',
                  'value' => (isset($sections[$i-1]->description))?$sections[$i-1]->description:''
              ]);?>
          </div>
           <?php  if($sections_count > 1){ ?>
            <div class="col-xs-12 p-0 text-right last_remove">
                <button type="button" class="btn btn-danger btn-md remove_section_exist" data-remove-id="count_element_<?php echo $i ?>" data-record-id="<?php echo $sections[$i-1]->id?>">
                    <i class="flaticon-waste-bin"></i> Remove 
                </button>
            </div>
            <?php } ?>
      </div>
  </div>
<?php } ?>
<div id="additional_section">

</div>
<div class="col-xs-12 p-0 mt-3 mb-4">
    <button type="button" class="btn btn-primary add_new_section">Add another section</button>
</div>
</div>
<script type="text/javascript" src="<?=bs('public/assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js') ?>"></script>
<script type="text/javascript" src="<?=bs('public/assets/js/assessment/add-setting-yes-question-part.js') ?>"></script>
<?php

$jsVars = [
    'ajax_call_root' => base_url(),
    'recordID' => $recordID,
    'sectionCount' => $sections_count
];    
?>
<script type="text/javascript">   
   $(document).ready(function() {
      AddSettingYesQuestionPart.init(<?php echo json_encode($jsVars); ?>);
   });  
</script>