<?php  if($details->question_section == 1){ ?>
<div class="row m-0">
	<div class="col-xs-12 p-0 pt-4 pb-4">
		<div class="form-group col-xs-12 p-0">
	        <label class="control-label col-sm-12 col-sm-2 pt-2">Select Section *</label>
	        <div class="col-sm-12 col-md-5">
	        	<select class="form-control" id="selected_que_section_id">
	        		<?php foreach ($sections as $key => $value) {?>
	        		<option  value="<?php echo $value->id ?>" <?php echo ($key===0)?'selected':'' ?>><?php echo $value->title ?></option>
	        	<?php } ?>
				</select>
	        </div>
	    </div>
	    <div class="col-xs-12 p-0">
	    	<div class="hr-line"></div>
	    </div>
	</div>
</div>
<?php } ?>
<div class="row m-0" id="selected_section_part">
	
</div>

<div class="col-xs-12 p-0">
    <div class="hr-line mt-5 mb-5"></div>
</div>
<div class="col-xs-12 p-0 text-right">
    <button type="button" class="btn btn-danger scroll-top click_to_back">Back</button>
    <button class="btn btn-primary scroll-top click_to_finish">Back to list</button>
</div>

<script type="text/javascript" src="<?php bs('public/assets/js/assessment/add-questions.js') ?>"></script>
<?php
$jsVars = [
    'ajax_call_root' => base_url(),
    'recordID' => $recordID,
    'question_section'=>$details->question_section
];    
?>
<script type="text/javascript">   
   $(document).ready(function() {
      AddQuizQuestion.init(<?php echo json_encode($jsVars); ?>);
   });  
</script>

<!------------------------------------------------------------------>

<div class="modal fade modals modals-tel-theme" id="sectionQuestionModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog w80p">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h2 class="modal-title">Question Section</h2>
			</div>
			<div class="modal-body" id="popup-questions">				

			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->