<link type="text/css" href="<?= bs('public/assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css')?>" rel="stylesheet">
<div class="row mt-3">  
    <div class="col-sm-5">
        <div class="panel panel-grey mb-0">
            <div class="panel-heading">
                <h2>
                    <label class="checkbox-inline icheck">
                        Assessment Categories
                    </label>
                </h2>
            </div>          
            <div class="col-md-12 tag-lib-container" id="assessment_categories"></div>           
        </div>
    </div>
    <div class="col-sm-5">
        <div class="panel panel-grey mb-0">
            <div class="panel-heading">
                <h2>
                    <label class="checkbox-inline icheck">Assessment Tags</label>
                </h2>
            </div>
            <div class="panel-body" id="assessment_tag"></div>
        </div>
    </div>
</div>
<div class="col-xs-12 p-0">
    <div class="hr-line mb-5"></div>
</div>
<div class="row">  
    <div class="col-sm-5">
        <div class="panel panel-grey mb-0">
            <div class="panel-heading">
                <h2>
                    <label class="checkbox-inline icheck">
                        System Categories
                    </label>
                </h2>
            </div>          
            <div class="col-md-12 tag-lib-container" id="system_categories"></div>           
        </div>
    </div>
    <div class="col-sm-5">
        <div class="panel panel-grey mb-0">
            <div class="panel-heading">
                <h2>
                    <label class="checkbox-inline icheck">System Tags</label>
                </h2>
            </div>
            <div class="panel-body" id="system_tag"></div>
        </div>
    </div>
</div>

<div class="col-xs-12 p-0">
    <div class="hr-line mb-5"></div>
</div>
<div class="row">  
    <div class="col-sm-5">
        <div class="panel panel-grey mb-0">
            <div class="panel-heading">
                <h2>
                    <label class="checkbox-inline icheck">
                        Learning Outcomes
                    </label>
                </h2>
            </div>          
            <div class="col-md-12 tag-lib-container" id="learning_outcomes"></div>           
        </div>
    </div>
    <div class="col-sm-5">
        <div class="panel panel-grey mb-0">
            <div class="panel-heading">
                <h2>
                    <label class="checkbox-inline icheck">TEL Mastery Standards</label>
                </h2>
            </div>
            <div class="col-md-12 tag-lib-container" id="learning_standards"></div>
        </div>
    </div>
</div>

<div class="col-xs-12 p-0">
    <div class="hr-line"></div>
</div>
<?php
echo form_open('quiz/add',['id'=>'createquiz','name'=>'createquiz']); ?>
<div class="col-xs-12 p-0 pt-5 pb-5">
    <div class="form-group flex-row m-0 pb-4">
        <label for="question_type" class="control-label pt-4">Skill Mastery Points</label>
        <div class="p-0 m-0 ml-4 w80 flex-auto pt-3">
            <input type="text" name="skill_mastery_point" value="<?php echo $details->skill_mastery_point ?>" class="touchspin4 form-control">
        </div>
    </div>    
</div>
<div class="col-xs-12 p-0">
    <div class="hr-line mt-5 mb-5"></div>
</div>
<div class="col-xs-12 p-0 text-right">
    <button type="button" class="btn btn-danger scroll-top click_to_back">Back</button>
    <button class="btn btn-primary scroll-top click_to_next"><?php echo ($this->session->userdata('action') == 'add')?'Save & Next':'Save Changes' ?></button>
</div>
</form>
<script type="text/javascript" src="<?= bs('public/assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js') ?>"></script>
<script type="text/javascript" src="<?php bs('public/assets/js/assessment/add-metadata.js') ?>"></script>
<?php

$jsVars = [
    'ajax_call_root' => base_url(),
    'recordID' => $recordID,
];    
?>
<script type="text/javascript">   
   $(document).ready(function() {
      AddQuizMetadata.init(<?php echo json_encode($jsVars); ?>);
   });  
</script>

<script>
    $(".scroll-top").on("click", function(){
      $('body,html').animate({
          scrollTop: 0
      }, 500);
    });        
</script>