<link type="text/css" href="<?= bs('public/assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css')?>" rel="stylesheet"> 
<div class="col-xs-12 p-0">
    <div class="form-group flex-row m-0 pb-4">
        <label for="question_type" class="control-label pt-4"># of questions per quiz</label>
        <div class="p-0 m-0 ml-4 w80 flex-auto pt-3">
            <?php echo form_input([ 
                  'name' => 'question_per_quiz',
                  'id' => 'question_per_quiz',
                  'class' => 'form-control touchspin4',
                  'value' => (isset($details->question_per_quiz)?$details->question_per_quiz:1)
              ]);?>
        </div>
        <span class="flex-auto pt-4 pl-3 f14">
            Number of questions from the Question Pool to include in each instance of the quiz
        </span>
    </div> 
</div> 

<script type="text/javascript" src="<?=bs('public/assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js') ?>"></script>

<script type="text/javascript" src="<?=bs('public/assets/js/assessment/add-setting-no-question-part.js') ?>"></script>
<?php

$jsVars = [
    'ajax_call_root' => base_url(),
    'recordID' => $recordID
];    
?>
<script type="text/javascript">   
   $(document).ready(function() {
      AddSettingNoQuestionPart.init(<?php echo json_encode($jsVars); ?>);
   });  
</script>  