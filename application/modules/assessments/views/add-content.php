<?php echo form_open('assessments/add',['id'=>'createassessments','name'=>'createassessments']); ?>
<div class="row mt-4 mb-3">
    <div class="col-sm-6">
        <div class="form-group">
            <label for="question_type" class="control-label"> Select Course <span class="field-required">*</span> </label>
            <?php echo form_dropdown('course_id', $courses, (isset($details->course_id) ? $details->course_id : 0), [ 
                              'id' => 'course_id',
                              'class' => 'form-control']);?>
        </div>
    </div>
</div>
<div class="row mt-4 mb-3">
    <div class="col-sm-6">
        <div class="form-group">
            <label for="question_type" class="control-label"> Assessment Title <span class="field-required">*</span> </label>
            <?php echo form_input([ 
                              'name' => 'title',
                              'id' => 'title',
                              'class' => 'form-control',
                              'value' => (isset($details->title)?$details->title:'')
            ]);?>
        </div>
    </div>
</div>
<div class="row mt-4 mb-3">
    <div class="col-sm-10">
        <div class="form-group">
            <label for="question_type" class="control-label"> Assessment Long Title </label>
            <?php echo form_input([ 
                              'name' => 'long_title',
                              'id' => 'long_title',
                              'class' => 'form-control',
                              'value' => (isset($details->long_title)?$details->long_title:'')
            ]);?>
        </div>
    </div>
</div>
<div class="row mt-4 mb-3">
    <div class="col-sm-12">
        <div class="form-group">
            <label for="question_type" class="control-label"> Assessment Instructions <span class="field-required">*</span> </label>
            <div class="col-sm-12 p-0">
                <?php echo form_textarea([ 
                              'name' => 'quizInstructions',
                              'id' => 'quizInstructions',
                              'class' => 'form-control',
                              'value' => (isset($details->instruction)?$details->instruction:'')
                ]);?>
            </div>
        </div>
    </div>
</div>
<div class="row mt-5 mb-3">
    <div class="col-sm-12">
        <div class="form-group">
            <label for="question_type" class="control-label"> Assessment Feedback </label>
            <div class="col-sm-12 p-0">
                <?php echo form_textarea([ 
                              'name' => 'quizFeedback',
                              'id' => 'quizFeedback',
                              'class' => 'form-control',
                              'value' => (isset($details->feedback)?$details->feedback:'')
                ]);?>
            </div>
        </div>
    </div>
</div>
<div class="col-sm-12 p-0 mt-5 mb-5">
    <div class="hr-line"></div>
</div>
<div class="row mb-3">
    <div class="col-sm-12 text-right">
        <button class="btn btn-danger click_to_finish">Cancel</button>
        <button type="submit" class="btn btn-primary scroll-top"><?php echo ($this->session->userdata('action') == 'add')?'Save & Next':'Save Changes' ?></button>
    </div>
</div>
</form>
<script type="text/javascript" src="<?php bs('public/assets/js/assessment/add-content.js') ?>"></script>
<?php

$jsVars = [
    'ajax_call_root' => base_url(),
    'recordID' => (isset($recordID)?$recordID:0),
];    
?>
<script type="text/javascript">   
   $(document).ready(function() {
      AddContent.init(<?php echo json_encode($jsVars); ?>);
   }); 
  $(".scroll-top").on("click", function(){
    $('body,html').animate({
        scrollTop: 0
    }, 500);
  });     
</script>