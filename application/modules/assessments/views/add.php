<div class="container-fluid pr-0">
   <div class="panel panel-default">
      <div class="panel-heading">
         <ul class="nav nav-tabs" id="quizPanelTab">
            <li role="presentation" class="active" id="tab-content">
               <a href="javascript:void(0)">Content</a>
            </li>
            <li role="presentation" id="tab-metadata">
               <a href="javascript:void(0)">Metadata</a>
            </li>
            <li role="presentation" id="tab-setting">
               <a href="javascript:void(0)">Peerceptiv LTI</a>
            </li>
           
            
         </ul>
      </div>
      <div class="panel-body" style="padding: 16px 20px;">
         <div class="tab-content">
            <!--// Start Content Tab  -->
            <div role="tabpanel" class="tab-pane active" id="quiz-tab-view">
               <div class="col-sm-12"><div class="page-loading-box"><span class="page-loader-quart"></span> Loading...</div></div>
            </div>
         </div>
      </div>
   </div>
</div>
<!------------------------------------------>
<!-- #page-content -->
<script type="text/javascript" src="<?php bs('public/assets/js/assessment/add.js') ?>"></script>
<?php
  $jsVars = [
      'ajax_call_root' => base_url()
  ];    
?>
<script type="text/javascript">   
  $(document).ready(function() {
      Add.init(<?php echo json_encode($jsVars); ?>);
  });  

  function getViewTemplate(viewType,recordID = 0) {
    $(".tab-pane").html('<div class="col-sm-12"><div class="page-loading-box"><span class="page-loader-quart"></span> Loading...</div></div>');
       $("#quiz-tab-view").load("<?php echo base_url() ?>assessments/load_view/" + viewType + "/" + recordID, function () {});
  }

</script>