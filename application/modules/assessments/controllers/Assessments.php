<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Assessments extends MY_Controller
{

    public function __construct()
    {
        ini_set('display_errors', 1);
        parent::__construct();
        //Do your magic here
        ini_set('display_errors', '0');
        $this->load->module('template');
        $this->load->model('common_model');
        $this->load->helper(['html', 'form']);
        $this->load->library('form_validation');
       
        // get controller permissions
        if ($this->current_user_permissions = $this->get_permissions()) {
            $this->add_permission = $this->current_user_permissions->add_per;
            $this->edit_permission = $this->current_user_permissions->edit_per;
            $this->delete_permission = $this->current_user_permissions->delete_per;
            $this->view_permission = $this->current_user_permissions->view_per;
            $this->list_permission = $this->current_user_permissions->list_per;
        }

        // get container_id
        $this->container_id = get_container_id();
    }

    /**
     * setup method
     * @description this function use to create container
     * @return void
     */
    public function index()
    {
        if (!$this->ion_auth->logged_in()) :
            redirect('users/auth', 'refresh');
        endif;

        if (!$this->list_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect('users/auth', 'refresh');
        endif;
        
        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Assessments', 'link' => '', 'class' => 'active'];
        $data['page']                   = "assessments/list";
        $this->template->template_view($data);
    }


    /**
     * getLists method 
     * @description this function called via ajax request, use to display list of questions
     * @return void
     */
    public function getLists() 
    {
        $this->load->model('Assessment_modal');
        $data = $this->Assessment_modal->getRows($_POST);
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $data['total'],
            "recordsFiltered" => $data['total'],
            "data" => $data['result']
        );
        echo json_encode($output);
    }



    /**
     * add method
     * @description this method is use to insert site creatation data in databse
     * @param string, numbers
     * @return void
     */
    public function add($id = null)
    {
        if (!$this->ion_auth->logged_in()) :
            redirect('users/auth', 'refresh');
        endif;    
        // check permissions
        if (!$this->add_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect($_SERVER['HTTP_REFERER'], 'refresh');
        endif;
        $this->session->set_userdata('action', 'add');
        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Assessments', 'link' => base_url('assessments'), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Add', 'link' => '', 'class' => 'active'];  
        $data['page'] = "assessments/add";
        $this->template->template_view($data);
    }


    /**
     * edit method
     * @description this method is use to insert site creatation data in databse
     * @param string, numbers
     * @return void
     */
    public function edit($id = null)
    {   
        if (!$this->ion_auth->logged_in()) :
            redirect('users/auth', 'refresh');
        endif;
         // check permissions
        if (!$this->edit_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect($_SERVER['HTTP_REFERER'], 'refresh');
        endif;
        if(is_numeric(base64_decode($id))){
            $condition = [
                'id' =>base64_decode($id),
                'delete_status' => 1
            ];
            if ($this->common_model->getCountRecord('assessments', $condition) == 0) {
                $this->session->set_flashdata('error', "Something went wrong");
                redirect('assessments', 'refresh');
            }
        }else{
            $this->session->set_flashdata('error', "Something went wrong");
            redirect('assessments', 'refresh');
        }
        $this->session->set_userdata('action', 'edit');
        
        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Assessments', 'link' => base_url('assessments'), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Edit', 'link' => '', 'class' => 'active']; 

        $data['recordID'] = base64_decode($id);
        $data['page'] = "assessments/edit";
        $this->template->template_view($data);
    }


     /**
     * small_load_view method
     * @description this function use to load view in tab
     * @return html
     */
     public function small_load_view($viewType = '', $recordID = '')
    {   
        switch ($viewType) {
            case 1:
                $data['sections'] = $this->common_model->getAllData('assessment_sections', ['title', 'description','id','weight','student_question_display'], '', ['delete_status' => 1, 'status' => 1,'assessment_id'=>$recordID]);
                $data['sections_count'] = (count($data['sections']) == 0)?1:count($data['sections']);
                $template_path = "assessments/add-setting-yes-question-part";
            break;
            case 2:
                $columns = 'assessments.question_per_quiz';
                $condition = [
                    'assessments.id' => $recordID,
                    'delete_status' => 1, 
                    'status' => 1
                ];
                $data['details'] = $this->common_model->getRecordByID('assessments', $condition, $columns);
                $template_path = "assessments/add-setting-no-question-part";     
            break;
           
        }
        $data['recordID'] = $recordID;
        echo $this->load->view($template_path, $data, true);
        exit;
    }

    /**
     * lti_grading method
     * @description this function use to cach grades from peerceptiv
     * @return html
     */

    public function lti_grading(){
            $response = file_get_contents('php://input');
            logit($response, 'peerceptiv_response');
            
         /*$response = '<?xml version = "1.0" encoding = "UTF-8"?>
         <imsx_POXEnvelopeRequest xmlns = "http://www.imsglobal.org/services/ltiv1p1/xsd/imsoms_v1p0">
           <imsx_POXHeader>
             <imsx_POXRequestHeaderInfo>
               <imsx_version>V1.0</imsx_version>
               <imsx_messageIdentifier>629f4da7-f37f-459c-93ea-e46f53263319</imsx_messageIdentifier>
             </imsx_POXRequestHeaderInfo>
           </imsx_POXHeader>
           <imsx_POXBody>
             <replaceResultRequest>
               <resultRecord>
                 <sourcedGUID>
                   <sourcedId>2:72:12:21:27:</sourcedId>
                 </sourcedGUID>
                 <result>
                   <resultScore>
                     <language>en</language>
                     <textString>0.796</textString>
                   </resultScore>
                 </result>
               </resultRecord>
             </replaceResultRequest>
           </imsx_POXBody>
         </imsx_POXEnvelopeRequest>'; */
                  
         $xml=simplexml_load_string($response);
         $sourseId = (string)$xml->imsx_POXBody->replaceResultRequest->resultRecord->sourcedGUID->sourcedId;
         $grading = (string)$xml->imsx_POXBody->replaceResultRequest->resultRecord->result->resultScore->textString;
    if($sourseId){
        list($asseementId, $userId, $courseId, $sectionId, $moduleId, $lessonId) = explode(":", $sourseId);
        ////////////////////////////////////////////////////////////////
        $cadata['user_id']          = $userId;
        $cadata['course_id']        = $courseId;
        $cadata['section_id']       = $sectionId;
        $cadata['module_id']        = $moduleId;
        $cadata['lesson_id']        = $lessonId;
        $cadata['contain_id']       = $asseementId;
        $cadata['contain_type']     = 2;
        $cadata['contain_answer']   = 1;
        $cadata['contain_attempt']  = 1;
        $cadata['status']           = 1;
        $cadata['created']          = date('Y-m-d h:i:s');
        $this->common_model->InsertData('course_activities', $cadata);
//////////////////////////////////////////////////////////////////
        $gqdata['user_id']          = $userId;
        $gqdata['course_id']        = $courseId;
        $gqdata['section_id']       = $sectionId;
        $gqdata['quiz_assess_id']   = $asseementId;
        $gqdata['quiz_assess_type'] = 3;
        $gqdata['grade']            = $grading;
        $gqdata['created']          = date('Y-m-d h:i:s');
        $this->common_model->InsertData('gradebook_quiz_assess_grade', $gqdata);
    }  
        echo 'Grading save successfully.';
            exit;
        
    }

    /*small_load_view*/
    /**
     * load_view method
     * @description this function use to load view in tab
     * @return html
     */
    public function load_view($viewType = '', $recordID = '')
    {   
        switch ($viewType) {
            case 'content':
                //$template_path = "assessments/add-questions";
                $template_path = "assessments/add-content";
                if($recordID > 0)
                {   
                    $columns = 'assessments.course_id,assessments.title,assessments.long_title,assessments.instruction,assessments.feedback,assessments.id';
                    $condition = [
                        'assessments.id' => $recordID
                    ];
                    $data['details'] = $this->common_model->getRecordByID('assessments', $condition, $columns);
                }else{
                    $data['details'] = [];
                }    
                $courses = $this->common_model->getAllData('courses', 'id, name', '', ['courses.delete_status' => 1], 'courses.name asc');
                foreach($courses as $item){
                    $data['courses'][$item->id] = $item->name;
                }

                break;
            case 'setting':
                $template_path = "assessments/add-setting";
                
                if($recordID > 0)
                {   
                   $assessmentDetails = $this->common_model->DJoin('assessments.id, assessments.title, assessments.long_title, courses.id as course_id, courses.name, courses.long_title', 'assessments', 'courses', 'assessments.course_id = courses.id', true, '', ['assessments.id' => $recordID]);

                /****************** Peerceptiv code***********************/
                        # ------------------------------
                        # START CONFIGURATION SECTION
                        # 
                        $launch_data = array(
                            "user_id"                               => $this->ion_auth->user()->row()->id,                           
                           // "roles"                               => "Learner",
                            "roles"                                 => "Instructor",
                            "context_id"                            => CONTEXT_START + (int)$assessmentDetails->course_id,
                            "context_title"                         => $assessmentDetails->name,
                            "context_label"                         => $assessmentDetails->name,
                            "resource_link_id"                      => (RESOURCE_LINK_START + (int)$assessmentDetails->id).'-e1919-bb3456',
                            "resource_link_title"                   => $assessmentDetails->title,
                            "resource_link_description"             => $assessmentDetails->long_title,
                            "lis_person_name_full"                  => $this->ion_auth->user()->row()->first_name." ".$this->ion_auth->user()->row()->middle_name." ".$this->ion_auth->user()->row()->last_name,
                            "lis_person_name_family"                => $this->ion_auth->user()->row()->last_name,
                            "lis_person_name_given"                 => $this->ion_auth->user()->row()->first_name,
                            "lis_person_contact_email_primary"      => $this->ion_auth->user()->row()->email,
                            "lis_outcome_service_url"               => base_url('/assessments/lti_grading'),
                            //"lis_outcome_service_url"             => 'https://www.google.com',
                            //"lis_person_contact_email_primary"    => 'dhawal@tellibrary.org',
                            "lis_person_sourcedid"                  => "school.edu:user",
                            //"lis_result_sourcedid"                  => $this->ion_auth->user()->row()->id.'_'.$assessmentDetails->id
                            );
                        
                            #
                            # END OF CONFIGURATION SECTION
                            # ------------------------------
                            $now = new DateTime();
                            $launch_data["lti_version"] = "LTI-1p0";
                            $launch_data["lti_message_type"] = "basic-lti-launch-request";
                            # Basic LTI uses OAuth to sign requests
                            # OAuth Core 1.0 spec: http://oauth.net/core/1.0/
                            $launch_data["oauth_callback"]          = "about:blank";
                            $launch_data["oauth_consumer_key"]      = LTI_KEY;
                            $launch_data["oauth_version"]           = "1.0";
                            $launch_data["oauth_nonce"]             = uniqid('', true);
                            $launch_data["oauth_timestamp"]         = $now->getTimestamp();
                            $launch_data["oauth_signature_method"]  = "HMAC-SHA1";
                            # In OAuth, request parameters must be sorted by name
                            $launch_data_keys                       = array_keys($launch_data);
                            sort($launch_data_keys);
                            $launch_params                          = array();
                            foreach ($launch_data_keys as $key) {
                            array_push($launch_params, $key . "=" . rawurlencode($launch_data[$key]));
                            }
                            $base_string = "POST&" . urlencode(LTI_LAUNCH_URL) . "&" . rawurlencode(implode("&", $launch_params));
                            $data['secret']         = urlencode(LTI_SECRET) . "&";
                            $data['signature']      = base64_encode(hash_hmac("sha1", $base_string, LTI_SECRET, true));
                            $data['launch_data']    = $launch_data;
                            $data['launch_url']     = LTI_LAUNCH_URL;    
                /****************** Peerceptiv code***********************/
                          
                }else{
                    $data['details'] = [];
                } 
            break;
            case 'metadata':

                $template_path = "assessments/add-metadata";
                if($recordID > 0)
                {   
                    $columns = 'assessments.skill_mastery_point';
                    $condition = [
                        'assessments.id' => $recordID,
                        'delete_status' => 1, 
                        'status' => 1
                    ];
                    $data['details'] = $this->common_model->getRecordByID('assessments', $condition, $columns);
                }else{
                    $data['details'] = [];
                }
            break;
            case 'questions':
                $template_path = "assessments/add-questions";
                 if($recordID > 0)
                {   

                    $columns = 'assessments.question_section,assessments.question_per_quiz';
                    $condition = [
                        'assessments.id' => $recordID,
                        'delete_status' => 1, 
                        'status' => 1
                    ];
                    $data['details'] = $this->common_model->getRecordByID('assessments', $condition, $columns);
                    $data['sections'] = $this->common_model->getAllData('assessment_sections', ['title','id'], '', ['delete_status' => 1, 'status' => 1,'assessment_id'=>$recordID]);
                }else{
                    $data['sections'] = [];
                  //  $data['sections_count'] = 1;
                } 
            break;
        }
        $data['recordID'] = $recordID;
        echo $this->load->view($template_path, $data, true);
        exit;
    }

    /**
     * getCheckExist method
     * @description this method is use to check name is already exist or not
     * @param string
     * @return json array
     */
    public function getCheckExist()
    {
        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        if ($this->form_validation->run() == FALSE) 
        {
            $this->setup();
        } else {
            $this->load->model('Assessment_modal');
            $conditions = [
                'title' => $this->input->post('title'),
                'delete_status' => 1
            ];
            if(post('recordID') != 0 && post('recordID') != '')
            {
                $conditions['id !='] = post('recordID');
            }

            if ($this->Assessment_modal->getCount($conditions,'assessments') == 0) 
            {
                echo 'true';
                exit;
            } else {
                echo 'false';
                exit;
            }
        }
        exit;
    }


    public function saveContents()
    {
        if (!$this->ion_auth->logged_in()) :
            redirect('users/auth', 'refresh');
        endif;
        if($this->input->post())
        {   
            $saveData = [
                'created_by' => $this->session->userdata('user_id'),
                'title' => post('title'),
                'course_id' => post('course_id'),
                'long_title' => post('long_title'),
                'instruction' => post('quizInstructions'),
                'feedback' => post('quizFeedback'),
                'created' => date("Y-m-d H:i:s"),
            ];
            if(post('recordID') > 0){
                if ($this->common_model->UpdateDB('assessments', ['id' => post('recordID')], $saveData)) {
                    echo json_encode(['code' => 200, 'message' => "Content has been successfully saved",'id'=>post('recordID')]);
                    exit;
                }
                    echo json_encode(['code' => 400, 'message' => "Something went wrong, please try again!"]);
                    exit;

            }else{
                if($this->common_model->InsertData('assessments', $saveData)){
                    echo json_encode(['code' => 200, 'message' => "Content has been successfully saved",'id'=>$this->db->insert_id()]);
                    exit;   
                }
                    echo json_encode(['code' => 400, 'message' => "Something went wrong, please try again!"]);
                    exit; 
            }
              
        }
        exit;    
    }

    public function saveSetting()
    {   
        if($this->input->post())
        {
            if(post('question_per_quiz'))
            {
                $quizData = [
                    'number_attempt' => post('number_attempt'),
                    'gradebook_record' => post('gradebook_record'),
                    'passing_requirement' => post('passing_requirement'),
                    'randomize_que_order' => (post('randomize_que_order'))?post('randomize_que_order'):0,
                    'restart_quize_btn' => (post('restart_quize_btn'))?post('restart_quize_btn'):0,
                    'view_result_btn' => (post('view_result_btn'))?post('view_result_btn'):0,
                    'question_section' => post('question_section'),
                    'question_per_quiz' => post('question_per_quiz')
                ];
            }else{
                $quizData = [
                    'number_attempt' => post('number_attempt'),
                    'gradebook_record' => post('gradebook_record'),
                    'passing_requirement' => post('passing_requirement'),
                    'randomize_que_order' => (post('randomize_que_order'))?post('randomize_que_order'):0,
                    'restart_quize_btn' => (post('restart_quize_btn'))?post('restart_quize_btn'):0,
                    'view_result_btn' => (post('view_result_btn'))?post('view_result_btn'):0,
                    'question_section' => post('question_section')
                ];
            }

            if ($this->common_model->UpdateDB('assessments', ['id' => post('recordID')], $quizData)) {
                
                if(post('section')){
                    $this->common_model->DeleteDB('assessment_sections',['assessment_id'  => post('recordID')]);
                    foreach (post('section') as $key => $value) {
                        $sectionData[] = [
                            'assessment_id' => post('recordID'),
                            'title' => $value['title'],
                            'description' => $value['description'],
                            'weight' => $value['weight'],
                            'student_question_display' => $value['student_question_display']
                        ];  
                    }
                    if($this->db->insert_batch('assessment_sections',$sectionData)){
                        echo json_encode(['code' => 200, 'message' => "Setting has been saved successfully"]);
                        exit;  
                    }
                }else{
                        echo json_encode(['code' => 200, 'message' => "Setting has been saved successfully"]);
                        exit;
                }
            }
            echo json_encode(['code' => 400, 'message' => "Something went wrong, please try again!"]);
            exit; 
        }
        exit;
    }

    public function save_skill_mastery_points()
    {
        if($this->input->post())
        {
            $quizData = [
                'skill_mastery_point' => post('skill_mastery_point')
            ];

            if ($this->common_model->UpdateDB('assessments', ['id' => post('recordID')], $quizData)) {
                echo json_encode(['code' => 200, 'message' => "Metadata has been saved successfully"]);
                exit;
            }
            echo json_encode(['code' => 400, 'message' => "Something went wrong, please try again!"]);
            exit; 
        }
        exit;  
    }


    public function get_active_questions()
    {
        $data = [];
        if($this->input->post())
        {   
            //pr($this->input->post());
            $data['quizID'] = $this->input->post('quizID'); 
            $data['sectionID'] = $this->input->post('sectionID'); 
        }    
        $template_path = "assessments/popup-questions";
        echo $this->load->view($template_path, $data, true);
        exit;
    }

    public function get_active_questions_no()
    {
        $data = [];
        if($this->input->post())
        {   
            $data['quizID'] = $this->input->post('quizID');
        }    
        $template_path = "assessments/popup-questions-no";
        echo $this->load->view($template_path, $data, true);
        exit;
    }

    public function get_selected_section_part_yes(){
        $data = [];
        if($this->input->post())
        {   
            $data['quizID'] = $this->input->post('quizID'); 
            $data['sectionID'] = $this->input->post('sectionID');
            $data['sectionDetails'] = $this->common_model->getRecordByID('assessment_sections',['assessment_sections.id'=>$this->input->post('sectionID')],'assessment_sections.title'); 
        }    
        $template_path = "assessments/get_selected_section_part";
        echo $this->load->view($template_path, $data, true);
        exit;
    }

    public function get_selected_section_part_no(){
        $data = [];
        if($this->input->post())
        {   
            $data['quizID'] = $this->input->post('quizID');
            /*$this->load->model('Assessment_modal');
            $data['details'] = $this->Assessment_modal->get_count_questions_and_limit($this->input->post('quizID'));*/
        }    
        $template_path = "assessments/get_selected_section_part_no";
        echo $this->load->view($template_path, $data, true);
        exit;
    }

    
    

     /**
     * get_make_action_request method
     * @description this method is use to make action as per request(like status change)
     * @param int
     * @return json array
     */
    public function get_make_action_request()
    {
        if ($this->input->post()) {
            $updateData = [];
            switch ($this->input->post('actionType')) {
                case 'status':
                    $updateData = [
                        'status' => post('status')
                    ];
                    $message = "You has been changed status successfully";
                    break;

                case 'delete':
                    $updateData = [
                        'delete_status' => post('status')
                    ];
                    $message = "You has been removed record successfully";
                    break;
            }
            if ($this->common_model->UpdateDB('assessments', ['id' => post('id')], $updateData)) {
                echo json_encode(['code' => 200, 'message' => $message]);
                exit;
            }
            echo json_encode(['code' => 400, 'message' => "Something went wrong, please try again!"]);
            exit;
        }
        exit;
    }

    public function get_remove_quiz()
    {
        if ($this->input->post()) {
            $postArray = [];
            foreach ($this->input->post('selected_record') as $key => $value) {
                $postArray[] = [
                    'id'            => $value,
                    'delete_status' => 0
                ];
            }
            if ($this->common_model->multiUpdateRecords('assessments', $postArray, 'id')) {
                echo json_encode(['code' => 200, 'message' => "You have been removed record successfully"]);
                exit;
            }
            echo json_encode(['code' => 400, 'message' => "Something went wrong, please try again!"]);
            exit;
        }
        exit;
    }


    public function get_export_quiz()
    {
        if($this->input->post())
        {   
            $file_name = 'assessments_list'.strtotime("now"). '.csv';
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$file_name");
            header("Content-Type: application/csv;");
            // get data
            $_POST['export_csv'] = true;
            $_POST['selected_site_ids'] = $this->input->post('selected_record');

            $this->load->model('Assessment_modal');
            $data = $this->Assessment_modal->getQuizRowsForExport($_POST);
          
            // file creation
            $file = fopen('php://output', 'w');

            $header = array("Sr. No", "Title","Long title", "Assessment Category", "System Category", "Learning Outcome", /*"Quiz Type","Blloms Level",*/"TEL Mastery Standards",/*"Skill Category","Competencies Category",*/"Assessment Tag","System Tag",/*"Course Tag","Module Tag","Lesson Tag", */"Status");
            fputcsv($file, $header);
            $i = 1;
            foreach ($data['result'] as $key => $value) {
                $nstatus = ($value->status == 1) ? 'Active' : 'Inactive';
                $narray = [
                            $i, 
                            $value->title, 
                            $value->long_title,
                            $value->assesment_category, 
                            $value->system_category, 
                            $value->outcomes_category,
                            /*$value->quiz_type_category,
                            $value->blooms_level_category, */
                            $value->standards_category,
                          /*  $value->skills_category,
                            $value->competencies_category,*/
                            $value->assessment_tags,
                            $value->system_tags,
                           /* $value->courses_tags,
                            $value->modules_tags,
                            $value->lessons_tags,*/
                            $nstatus
                        ];
                fputcsv($file, $narray);
                $i++;
            }
            fclose($file);
            exit;
        }   
       exit; 
    }

    public function get_count_questions_and_limit()
    {
        $this->load->model('Assessment_modal');
        $details = $this->Assessment_modal->get_count_questions_and_limit($this->input->post('quizID'));
        echo json_encode($details);
    }

    public function get_check_setting()
    {
        if($this->input->post())
        {
            $columns = 'assessments.question_section';
            $condition = [
                'assessments.id' => post('quizID'),
                'assessments.delete_status' => 1, 
                'assessments.status' => 1
            ];
            $details = $this->common_model->getRecordByID('assessments', $condition, $columns);
            echo json_encode($details->question_section);
        }
        exit;
    }

    public function removeSection()
    {
        if($this->input->post())
        {   
            $updateData = [
                'delete_status' => 0
            ];
            if(post('record_ID') > 0){
                if ($this->common_model->UpdateDB('assessment_sections', ['id' => post('record_ID')], $updateData)) {
                    echo json_encode(['code' => 200, 'message' => "Section has been successfully removed",'id'=>post('record_ID')]);
                    exit;
                }
                    echo json_encode(['code' => 400, 'message' => "Something went wrong, please try again!"]);
                    exit;

            }
             echo json_encode(['code' => 400, 'message' => "Something went wrong, please try again!"]);
                    exit; 
              
        }
        exit;    
    }

}
