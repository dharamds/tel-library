<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Questions extends MY_Controller {

    public function __construct() {
        //ini_set('display_errors', 2);
        parent::__construct();
        $this->load->module('template');
        $this->load->helper(array('html', 'form'));
        $this->load->library('form_validation');

        if (!$this->ion_auth->logged_in()):
            redirect('users/auth', 'refresh');
        endif;
    }
    

    /**
     * getSelectedLists method 
     * @description this function called via ajax request, use to display list of selected questions
     * @return void
     */
    public function getSelectedLists() 
    {
       
        $this->load->model('Questions_modal');
        if($_POST['sectionID']){
            $data = $this->Questions_modal->getSelectedRows($_POST);
        }else{
            $data = $this->Questions_modal->getSelectedRowsNoSection($_POST);
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $data['total'],
            "recordsFiltered" => $data['total'],
            "data" => $data['result']
        );
        echo json_encode($output);
    }
   
     /**
     * getLists method 
     * @description this function called via ajax request, use to display list of questions
     * @return void
     */
    public function getLists() 
    {
        $this->load->model('Questions_modal');
        $data = $this->Questions_modal->getRows($_POST);
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $data['total'],
            "recordsFiltered" => $data['total'],
            "data" => $data['result']
        );
        echo json_encode($output);
    }


     /**
     * selectedAssigned method 
     * @description this function called via ajax request, use to add selected record to sections
     * @param int
     * @return json array
     */
    public function selectedAssigned()
    {
        if($this->input->post())
        {   
            $selectedQue = $this->input->post('selected_record');
            $quizID = $this->input->post('quizID');
            $sectionID = $this->input->post('sectionID');
            foreach ($selectedQue as $key => $value) {
                $saveData[] = [
                    'assessment_id' => $quizID,
                    'section_id' => $sectionID,
                    'question_id' => $value,
                ];
            }
            if($this->db->insert_batch('assessment_section_questions', $saveData))
            {
                echo json_encode(['code'=>200,'message'=>"Questions has been added successfully"]);
                exit;
            }
                echo json_encode(['code' => 400, 'message' => "Something went wrong, please try again!"]);
                exit;
        }
        exit;
    }

    public function selectedRemove(){
        if($this->input->post())
        {
            $selectedQue = $this->input->post('selected_record');
            $where = [
                    'assessment_id'    => $this->input->post('quizID')
            ];
            
            $this->load->model('common_model');
            if($this->common_model->MultiDeleteDB('assessment_section_questions',$where,$selectedQue,'question_id'))
            {
                echo json_encode(['code'=>200,'message'=>"Questions has been removed successfully"]);
                exit;
            }
                echo json_encode(['code' => 400, 'message' => "Something went wrong, please try again!"]);
                exit;
        }
        exit;
    }

    



}
