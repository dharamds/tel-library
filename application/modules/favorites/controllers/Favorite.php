<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Favorite extends MY_Controller {

    public function __construct() {

        parent::__construct();
        //Do your magic here
        $this->load->module('template');
        $this->load->model('common_model');
        $this->load->model('Favorites_model');
        $this->load->library('form_validation');
        $this->load->helper(array('html', 'language', 'form', 'country_helper'));
        if (!$this->ion_auth->logged_in()) :
            redirect('users/auth', 'refresh');
        endif;
        // get controller permissions
        if ($this->current_user_permissions = $this->get_permissions()) {
            $this->add_permission = $this->current_user_permissions->add_per;
            $this->edit_permission = $this->current_user_permissions->edit_per;
            $this->delete_permission = $this->current_user_permissions->delete_per;
            $this->view_permission = $this->current_user_permissions->view_per;
            $this->list_permission = $this->current_user_permissions->list_per;
        }
        // get container_id
        $this->container_id = get_container_id();
        
    }

    /**

     * @return [void]
     */
    public function index() {
        if (!$this->list_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect('users/auth', 'refresh');
        endif;
        $data['permissions'] = $this->current_user_permissions;
        $data['breadcrumb'][] = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][] = ['title' => 'Favorites', 'link' => '', 'class' => 'active'];
        $data['favorites'] = $this->Favorites_model->favorites_list();
        //  exit('dfd00'); exit;
        $data['page'] = "favorites/Favorites/view";
        $this->template->template_view($data);
    }

    public function make_fav() {
        $userId = trim($this->session->userdata('user_id'));
        $title = trim($this->input->post('title'));
        $slug = trim($this->input->post('param'));

        $data = [
            'user_id' => $userId,
            'title' => $title,
            'slug' => $slug,
        ];


        $arr = [];
        $select = $this->common_model->getDataById('favorites', '*', array("title" => $title, "slug" => $slug, "user_id" => $userId));
        if (!empty($select)) {
            $delete = $this->common_model->DeleteDB('favorites', array("title" => $title, "slug" => $slug, "user_id" => $userId));
            if ($delete) {
                $arr['status'] = true;
                $arr['message'] = 'deleted';
            } else {
                $arr['status'] = false;
                $arr['message'] = 'Something went wrong!';
            }
        } else {
            if ($this->common_model->InsertData('favorites', $data)) {
                $arr['status'] = true;
                $arr['message'] = 'inserted';
            } else {
                $arr['status'] = false;
                $arr['message'] = 'Something went wrong!';
            }
        }


        echo json_encode($arr);
        exit;
    }

    public function checkFavStatus() {
        $userId = trim($this->session->userdata('user_id'));
        $title = trim($this->input->post('title'));
        $slug = trim($this->input->post('param'));
        $active = false;
        $msg = 'inactive';
        $select = $this->common_model->getDataById('favorites', '*', array("title" => $title, "slug" => $slug, "user_id" => $userId));
        if($select){
            $active = true;
            $msg = 'active';
        }
        $arr['msg'] = $msg;
        $arr['active'] = $active;
        echo json_encode($arr);exit;
    }

    public function delete($id) {
        $this->session->set_flashdata('success', $this->ion_auth->messages());

        $delete = $this->common_model->DeleteDB('favorites', array("id" => $id));

        $msg = "deleted";
        //$this->session->set_flashdata('success', $msg);
        echo json_encode(array("msg" => $msg));
        exit;
        //redirect('certificates/Certificates/', 'refresh');
    }

    //update certificates status
    public function update_status($id, $action) {
        $status = ($action == 'activate') ? 1 : 0;
        $data = array('status' => $status);
        $this->Certificates_model->update_status($id, $data);
        $msg = "Updated";
        echo json_encode(['msg' => $msg]);
        exit;
    }

    function getLists() {
        $Data = $this->Favorites_model->getRows($_POST);
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $Data['total'],
            "recordsFiltered" => $Data['total'],
            "data" => $Data['result'],
        );
        echo json_encode($output);
    }

}

/* End of file Posts.php */
/* Location: ./application/modules/blog/controllers/Posts.php */
