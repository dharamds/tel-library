
<div class="container-fluid">

    <br>
    <div data-widget-group="group1">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default panel-grid">
                   <div class="panel-heading">
                        <div class="panel-ctrls"></div>
                    </div>
                    <div class="panel-body no-padding">
                        <table id="memListTable" class="table table-bordered table-striped table-hover" cellspacing="0">
                            <thead>
                                <tr>
                                    <th style="min-width: 5%;">Sr.</th>
                                    <th style="min-width: 60%;">Title</th>
                                    <th style="min-width: 20%;">Created At</th>
                                    <th style="min-width: 15%">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- .container-fluid -->

<script>
    $(document).ready(function () {
        var table = table = table = $('#memListTable').DataTable({
            // Processing indicator
            "processing": true,
            // DataTables server-side processing mode
            "serverSide": true,
            // Initial no order.
            "iDisplayLength": 10,
            "bPaginate": true,
            "order": [],
            "scrollX": true,
            "autoWidth": false,
            // Load data from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('favorites/Favorite/getLists'); ?>",
                "type": "POST",
                "data": function (data) {
                    //  alert("h");
                },
            },
            "columnDefs": [
                {
                    "targets": 3,
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                             url = '<?php echo bs('favorites/Favorite/delete/') ?>' + full['id'];
                             data = '<a href="' + url + '" class="btn btn-danger btn-sm delete_item" ><i class="ti ti-trash"></i></a>';
                                                        
                        }
                        return data;
                    }
                },
                  {
                    "targets": 1,
                    "data": null,
                    "render": function (data, type, full, meta) {
                   // console.log(full);
                        if (type === 'display') {
                             url = '<?php echo bs() ?>' + full['slug'];
                             data = '<a href="' + url + '">' + full['title'] + '</a>';                          
                        }
                        return data;
                    }
                }
            ],
            "columns": [
                {
                    "data": "sr_no", render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                },
                {"data": "title"},
                {"data": "created"},
            ]
        });
        $(document).on('click', '.change_status', function (e) {
            e.preventDefault();
            $.get($(this).attr("href"), // url
                    function (data, textStatus, jqXHR) { // success callback
                        var obj = JSON.parse(data);
                        if (obj.msg === 'Updated') {
                            table.ajax.reload();  //just reload table
                        }
                    });
        });

        $(document).on('click', '.delete_item', function (e) {
            e.preventDefault();
            var scope = $(this);
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure?',
                buttons: {
                    confirm: function () {
                        $.get(scope.attr("href"), // url
                                function (data, textStatus, jqXHR) { // success callback
                                    var obj = JSON.parse(data);
                                    if (obj.msg === 'deleted') {
                                        table.ajax.reload();  //just reload table
                                    }
                                });
                        return true;
                    },
                    cancel: function () {
                        return true;
                    }
                }
            });
        });
    });
</script>