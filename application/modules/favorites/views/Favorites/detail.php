<ol class="breadcrumb">
    <li class=""><a href="<?php echo base_url() ?>">Home</a></li>
       <li class=""><a href="<?php echo base_url('certificates/Certificate/') ?>">Certificate</a></li>
       <li class="active"><a href=""> Certificate Details </a></li>
</ol>
<div class="container-fluid">
   <div class="tab-pane active" id="tab-about">
      <div class="panel panel-default panel-grid">
         <div class="panel-heading brd-0 pt-1"></div>
         <div class="panel-body">
            <div class="row">
               <div class="col-md-12">
                  <div class="form-group">
                     <label class="control-label fw600">Name</label>
                     <div><?php  echo $data->name;?></div>
                     <div class="hr-line mt-3 mb-3"></div>
                  </div>
               </div>
               <div class="col-md-12">
                  <div class="form-group">
                     <label class="control-label fw600">Description</label>
                     <div><?php echo $data->body;?></div>
                     <div class="hr-line mt-3 mb-3"></div>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="control-label fw600">Created by</label>
                     <div>
                        <?php 
                        $authorData = $this->ion_auth->user($certificate_data->created_by)->row(); 
                        echo ucfirst($authorData->first_name); echo " ";  
                        echo ucfirst($authorData->last_name);
                        ?>
                     </div>
                     <div class="hr-line mt-3 mb-3"></div>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="control-label fw600">Created by</label>
                     <div>
                        <?php echo date('m-d-Y',strtotime($certificate_data->created)); ?>
                     </div>
                     <div class="hr-line mt-3 mb-3"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

