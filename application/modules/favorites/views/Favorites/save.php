

            <div class="container-fluid">
                <div class="panel panel-default panel-grid" data-widget='{"draggable": "false"}'>
                    <div class="panel-heading brd-0 pt-1"></div>
                    <div class="panel-body ">
                        <?= form_open('certificates/Certificate/add', array('id' => 'certificate_form_validation', 'class' => 'form-horizontal')); ?>
                        <input type="hidden" name="id" value="<?php echo $id ?>" >
                        <div class="row m-0">
                            <div class="form-group col-md-12 p-0">
                                <div class="col-md-6">
                                    <label for="fieldname" class="control-label">Name</label>
                                    <?php echo form_input([
                                        'name' => 'name',
                                        'id' => 'name',
                                        'class' => 'form-control editor',
                                       // 'rows' => '5',
                                        'value' => $data->name
                                    ]); ?>
                                    <?php echo form_error('template', '<div class="error">', '</div>'); ?>
                                </div>                              
                            </div>
                            <div class="form-group col-md-12 p-0">
                                <div class="col-md-12">
                                    <label for="fieldname" class="control-label">Body</label>
                                    <div>
                                        <?php echo form_textarea([
                                            'name' => 'body',
                                            'id' => 'body',
                                            'class' => 'form-control editor',
                                            //'rows' => '5',
                                            'value' => $data->body
                                        ]); ?>
                                        <?php echo form_error('learning_outcomes', '<div class="error">', '</div>'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 p-0">
                                <div class="hr-line mt-4 mb-5"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 text-right">
                                    <input style="margin-left:15px;" type="submit" class="finish btn-success btn" value="Save">
                                </div>
                                </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
<!-- #page-content -->
<script>
    $(document).ready(function () {
        
        jQuery.validator.addMethod("noSpace", function (value, element, param) {
            return value.match(/^(?=.*\S).+$/);
        }, "No space please and don't leave it empty");

        CKEDITOR.replace('body', {
            toolbar: 'short',
        });

        $("#certificate_form_validation").validate({
            
             ignore: [],
            
            rules: {
                name: {
                    required: true,
                    noSpace: true
                },
                body: {
                    required: function (textarea) {
                        CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                        return editorcontent.length === 0;
                    }
                }
            },
            messages: {
                name: {
                    required: "Please enter name"
                },
                body: {
                    required: "Please enter certificate body"
                }
            },
//            submitHandler: function (form) {
//                $("#Discussion_create_form_validation").submit();
//               return true;
//            },
                errorPlacement: function(error, $elem) {
            if ($elem.is('textarea')) {
                $elem.insertAfter($elem.next('div'));
            }
            error.insertAfter($elem);
        },
        });
    });
</script>