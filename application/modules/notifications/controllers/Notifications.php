<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Notifications extends MY_Controller {

    public function __construct() {

        parent::__construct();
        //Do your magic here

        $this->load->module('template');
        $this->load->model('Notification_modal');
        $this->load->model('common_model');
        $this->load->helper(['html', 'form']);
        $this->load->library('form_validation');


        if (!$this->ion_auth->logged_in()) :
            redirect('users/auth', 'refresh');
        endif;

         // get controller permissions
         if ($this->current_user_permissions = $this->get_permissions()) {
            $this->add_permission = $this->current_user_permissions->add_per;
            $this->edit_permission = $this->current_user_permissions->edit_per;
            $this->delete_permission = $this->current_user_permissions->delete_per;
            $this->view_permission = $this->current_user_permissions->view_per;
            $this->list_permission = $this->current_user_permissions->list_per;
        }
        // get container_id
        $this->container_id = get_container_id();
    }

    /**
     * setup method
     * @description this function use to create container
     * @return void
     */
    public function index() {
         // check permissions
         if (!$this->list_permission && !$this->ion_auth->is_admin()) :
            redirect('users/auth', 'refresh');
        endif;
        $data['permissions'] = $this->current_user_permissions;
        $data['breadcrumb'][] = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][] = ['title' => 'Notifications', 'link' => '', 'class' => ''];
        $data['page'] = "notifications/listings";

        $this->template->template_view($data);
    }

    public function getUserNotification() {
        $userId = trim($this->session->userdata('user_id'));
        $select = $this->common_model->getAllData('notifications', '*', '', array("receiver_id" => $userId), 'id desc', 10);
        $countSelect = $this->common_model->getAllData('notifications', '*', '', array("receiver_id" => $userId, "is_read" => 0), 'id desc', 10);
        $count = count($countSelect);
        $arr['count'] = $count;
        $arrIds = array();
        $html = '';
        $html .= ' <ul class="media-list scroll-content">';
        if (!empty($select)) {
            foreach ($select as $key => $val) {
                array_push($arrIds, $val->id);
                if($val->is_read == 0){
                    $html .= '<li class="media notification-message notification-unread">'; 
                }else{
                    $html .= '<li class="media notification-message notification-read">'; 
                }
                $html .= '<a href="#"><div class="media-left"><i class="flaticon-envelope"></i></div><div class="media-body">'.$val->body.'</div></a>';
                $html .= '</li>';
            }
        } else {
            $html .= '  <div class="topnav-dropdown-footer"><a href="#" style="color:red;">No Notifications</a></div>';
        }
        $html .= '</ul>';
        $arr['html'] = $html;
        $arr['notificationArr'] = $arrIds;
        $arr['list'] = $select;
        echo json_encode($arr);
        exit;
    }

    public function readNotifications() {
        $arr = explode(',', $this->input->post('ids'));
        $this->db->where_in("id", $arr);
        $this->db->update('notifications', array('is_read' => 1));
        //$done = $this->db->delete('notifications');
        echo json_encode(['msg' => 'success']);
        exit;
    }

    // list users as per filter
    public function getlistings() {
        // Fetch member's records
        $userData = $this->Notification_modal->getRows($_POST);

        //gkc on dated 28th May , 2019
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $userData['total'],
            "recordsFiltered" => $userData['total'],
            "data" => $userData['result'],
        );
        // Output to JSON format
        echo json_encode($output);
        exit;
    }
    // list users as per filter
    public function delete() {
        // Fetch member's records
        if(!empty($_POST['selected_notification_id'])) {
            $exp=explode("|",$_POST['selected_notification_id']);
            foreach($exp as $delete_data) {
                $this->db->delete('notifications', array('id' => $delete_data));
            }
            return true;
        } else {
            $this->db->delete('notifications', array('id' => $this->uri->segment('3')));
            return true;
        }
        
        
    }

}
