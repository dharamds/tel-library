<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Manual_Notifications extends MY_Controller
{

    public function __construct()
    {

        parent::__construct();
        //Do your magic here

        $this->load->module('template');
        $this->load->model('Manual_Notifications_modal');
        $this->load->model('common_model');
        $this->load->helper(['html', 'form']);
        $this->load->library('form_validation');


        if (!$this->ion_auth->logged_in()) :
            redirect('users/auth', 'refresh');
        endif;

        // get controller permissions
        if ($this->current_user_permissions = $this->get_permissions()) {
            $this->add_permission = $this->current_user_permissions->add_per;
            $this->edit_permission = $this->current_user_permissions->edit_per;
            $this->delete_permission = $this->current_user_permissions->delete_per;
            $this->view_permission = $this->current_user_permissions->view_per;
            $this->list_permission = $this->current_user_permissions->list_per;
        }
        // get container_id
        $this->container_id = get_container_id();
    }

    /**
     * setup method
     * @description this function use to create container
     * @return void
     */
    public function index()
    {

        $data['breadcrumb'][] = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][] = ['title' => 'Manual Notifications', 'link' => '', 'class' => ''];
        $data['page'] = "notifications/Manual_Notifications/listings";
             // check permissions
             if (!$this->list_permission && !$this->ion_auth->is_admin()) :
                redirect('users/auth', 'refresh');
            endif;
            $data['permissions'] = $this->current_user_permissions;
        $this->template->template_view($data);
    }

    public function add()
    {
        if (!$this->add_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect($_SERVER['HTTP_REFERER'], 'refresh');
        endif;
        $data['permissions'] = $this->current_user_permissions;
        $data['breadcrumb'][] = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][] = ['title' => 'Manual Notifications', 'link' => base_url('manual_notification'), 'class' => ''];

        if ($this->input->post()) {

            $this->form_validation->set_rules('subject', 'Subject', 'trim|required');
            $this->form_validation->set_rules('message', 'Message', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $data['page'] = "Notifications/Manual_Notifications/listings";
                set_flashdata('error', 'Unable to add Manual Notifications, try again');
                $this->template->template_view($data);
            } else {

                foreach ($_POST['user_ids'] as $user_data) {
                    $insert_data[] = array(
                        'sender_id' => $this->session->userdata('user_id'),
                        'receiver_id' => $user_data,
                        'subject' => post('subject'),
                        'body' => post('message'),
                        'priority' => 1,
                        'is_read' => 0,
                        'status' => 1
                    );
                    $stu_data = $this->common_model->getDataById('users', 'email,CONCAT_WS(" ", users.first_name) as user_name' , ["id" => $user_data]);
                    
                    $email_data['email'] = $stu_data->email;
                    $email_data['variables']['user_name'] = $stu_data->user_name;
                    $email_data['variables']['body'] = post('message');
                    $email_data['template_code'] = 'NOTIFICATION';
                    sendgrid_email($email_data);
                }

                $this->common_model->InsertBatchData('notifications', $insert_data);
                set_flashdata('success', 'Manual Notifications Sent');
                redirect('manual_notification/', 'refresh');
            }
        } else {
            $data['breadcrumb'][]           = ['title' => 'Add', 'link' => '', 'class' => 'active'];
            $data['page'] = "notifications/Manual_Notifications/save";
            $this->template->template_view($data);
        }
    }

    public function create()
    {

        $data['breadcrumb'][] = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][] = ['title' => 'Manual Notifications', 'link' => '', 'class' => ''];
        $data['page'] = "manual_notification/save";

        $this->template->template_view($data);
    }

    public function save($user_id = null)
    {
        echo $this->Manual_Notifications_modal->set_users($user_id);
        exit;
    }

    // list users as per filter
    public function getlistings()
    {
        // Fetch member's records
        $userData = $this->Manual_Notifications_modal->getRows($_POST);

        //gkc on dated 28th May , 2019
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $userData['total'],
            "recordsFiltered" => $userData['total'],
            "data" => $userData['result'],
        );
        // Output to JSON format
        echo json_encode($output);
        exit;
    }

    /**
     * delete method
     * @description this function use to delete tempray record 
     * @return void
     */
    function delete()
    {
        if (!$this->delete_permission && !$this->ion_auth->is_admin()) :
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect($_SERVER['HTTP_REFERER'], 'refresh');
        endif;


        $id = $this->uri->segment(4);

        $data = ['status' => 0];
        if ($id) {
            $this->common_model->UpdateDB('notifications', ['id' => $id], $data);
            $this->session->set_flashdata('success', $this->ion_auth->messages());
            $msg = "deleted";
            echo json_encode(array("msg" => $msg));
            exit;
        } else {
            set_flashdata('error', 'Unable to delete poll, try again');
            redirect('manual_notification/', 'refresh');
        }
    }
}
