<style>
    .dropdown-menu>.active>a,
    .dropdown-menu>.active>a:hover,
    .dropdown-menu>.active>a:focus,
    .tt-dropdown-menu>.active>a,
    .tt-dropdown-menu>.active>a:hover,
    .tt-dropdown-menu>.active>a:focus,
    .tt-suggestion>p:hover,
    .tt-suggestion>p:focus,
    .tt-suggestion.tt-cursor>p {
        color: #000000
    }
</style>
<div class="container-fluid">
    <div class="panel panel-info panel-grid">
        <div class="panel-heading brd-0 pt-0"></div>
        <div class="panel-body">
            <?php isset($detail->id) ? $detailId = $detail->id  : $detailId = '' ?>
            <?php echo form_open('manual_notification/add/' . $details->id, array('id' => 'form_validation')); ?>

            <fieldset>
                <legend>Manual Notification</legend>
                <div class="form-group col-xs-12 p-0">
                    <label for="subject" class="col-md-12 control-label">Search User</label>
                    <div class="col-md-9">
                        <input class="combobox form-control" id="search_users" name="search_users">
                    </div>
                </div>

                <div class="form-group col-md-8" id="menu_permission_users"></div>

            </fieldset>
            <div class="form-group col-xs-12 p-0">
                <label for="subject" class="col-md-12 control-label">Subject</label>
                <div class="col-md-9">
                    <?php
                    echo form_input([
                        'name' => 'subject',
                        'id' => 'subject',
                        'class' => 'form-control',
                        'value' => $details->subject,
                        'placeholder' => 'Enter Subject'
                    ]);
                    ?>
                    <?php echo form_error('subject', '<div class="error">', '</div>'); ?>
                </div>
            </div>

            <div class="form-group col-xs-12 p-0">
                <label for="message" class="col-md-12 control-label">Message</label>
                <div class="col-md-9">
                    <?php
                    echo form_textarea([
                        'name' => 'message',
                        'id' => 'message',
                        'class' => 'form-control editor',
                        'value' => $details->message,
                        'rows' => '5'
                    ]);
                    ?>
                    <?php echo form_error('message', '<div class="error">', '</div>'); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6 text-left pb-4 pt-4">
                    <a class="finish btn-primary btn" href="<?php bs('/manual_notification'); ?>">Back</a>
                </div>
                <div class="col-md-6 text-right pb-4 pt-4">
                    <input type="submit" class="finish btn-primary btn" value="Save">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #page-content -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>
<script>
    $(document).ready(function() {
        //$('#message').summernote();
        var config = { height: 120, toolbar: 'short', allowedContent :true};
        // to assign ckeditor
        CKEDITOR.replace('message', config);
        $("#form_validation").validate({
            ignore: [],
            rules: {

                subject: {
                    required: true
                },
                message: {
                    required: function(textarea) {
                        CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                        return editorcontent.length === 0;
                    }
                }
            },
            messages: {
                subject: {
                    required: 'Please enter subject'
                },
                message: {
                    required: 'Please enter message'
                }
            },
            errorPlacement: function(error, $elem) {
                if ($elem.is('textarea')) {
                    $elem.insertAfter($elem.next('div'));
                }
                error.insertAfter($elem);
            },
            submitHandler: function(form) { // for demo
                return true; // for demo
            }
        });

        $('#search_users').typeahead({
            source: function(query, result) {
                $.ajax({
                    url: "<?php echo base_url('/users/getUsers'); ?>",
                    method: "POST",
                    data: {
                        query: query
                    },
                    dataType: "json",
                    success: function(data) {
                        result($.map(data, function(item) {
                            return item;
                        }));
                    }
                })
            },
            updater: function(item) {
                if ($('#' + item.id).length) {
                    alert("Already exists");
                } else {
                    data = "";
                    data += "<div id=" + item.id + " ><label>" + item.name + "</label> &nbsp; <i class='fa fa-trash' onclick='delete_name(" + item.id + ")' ></i><input type='hidden' name='user_ids[]' value=" + item.id + "></div>";
                    $("#menu_permission_users").append(data);
                }

            }
        });

    });

    function delete_name(id) {
        $("#" + id + "").remove();
    }
</script>