            <div class="container-fluid">
                <div data-widget-group="group1">
                    <div class="row">
                    <div class="col-md-12 ">
                            <a class="btn btn-success" href="<?= base_url('manual_notification/add') ?>">New</a>
                        </div>
                        <div style="clear:both;"></div><br>
                        <div class="col-md-12">
                            <div class="panel panel-default panel-grid">
                                <div class="panel-header brd-0 pt-2"></div>
                                <div class="panel-body pl-0 pr-0">
                                    <div class="row m-0">
                                        <div class="col-md-12">
                                            <table id="notoficationListTable" class="table table-bordered table-striped table-hover" cellspacing="0" width="100%">
                                                <colgroup>
                                                    <col width="10%">  
                                                    <col width="10%">  
                                                    <col width="10%">  
                                                    <col width="55%">
                                                    <col width="10%">  
                                                    <col width="10%">
                                                </colgroup>
                                                <thead>
                                                    <tr>
                                                        <!-- <th style="min-width: 50px"><label class="checkbox-tel"><input type="checkbox" class="select_all"></th> -->
                                                        <th style="min-width: 50px">Action</th>
                                                        <th style="min-width: 220px">Sender</th>
                                                        <th style="min-width: 220px">Receiver</th>
                                                        <th style="min-width: 150px">Notification</th>
                                                        <th style="min-width: 150px">Priority</th>
                                                        <th style="min-width: 100px">Date</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <script type="text/javascript">
    $(document).ready(function () {
        $(".select_all").click(function() {
            $(".selct_class").prop('checked', $(this).prop('checked'));
        });
        var table = table = $('#notoficationListTable').DataTable({
            // Processing indicator
            "processing": true,
            // DataTables server-side processing mode
            "serverSide": true,
            // Initial no order.
            "iDisplayLength": 10,
            "bPaginate": true,
            "order": [],
            // Load data from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('notifications/Manual_Notifications/getlistings'); ?>",
                "type": "POST",
                "data": function (data) {
                    data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                     $(window).scrollTop(0);
                },
              },
              "columnDefs": [
                {
                    "targets": 0,
                    "orderable": false,
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                            data = '';
                            <?php if ($this->ion_auth->is_admin() || $permissions->delete_per): ?>
                                data += '<a href="<?=base_url('Notifications/Manual_Notifications/delete/')?>' + full['id'] + '" class="btn btn-danger btn-sm delete_item"><i class="ti ti-trash"></i></a>';
                            <?php endif;?>
                        }
                        return data;
                    }
                }
               
            ],
            "columns": [      
                {
                     "data":null ,
                     "autoWidth":true
                },
                {
                     "data":"sender" ,
                     "autoWidth":true
                },
                {
                    "data": "receiver",
                    "autoWidth": true
                },
                {
                    "data": "body",
                    "autoWidth": true
                },
                {
                    "data": "priority",
                    "autoWidth": true
                },
                {
                    "data": "created",
                    "autoWidth": true
                }
            ]
        });

        $(document).on('click', '.delete_item', function (e) {
            e.preventDefault();
            var scope = $(this);
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure?',
                buttons: {
                    confirm: function () {
                        $.get(scope.attr("href"), // url
                                function (data, textStatus, jqXHR) { // success callback
                                    var obj = JSON.parse(data);
                                    if (obj.msg === 'deleted') {
                                        table.ajax.reload(); //just reload table
                                    }
                                });
                        return true;
                    },
                    cancel: function () {
                        return true;
                    }
                }
            });
        });


    });
</script>