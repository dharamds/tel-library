            <div class="container-fluid">
                <div data-widget-group="group1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default panel-grid">
                                <div class="panel-heading">

                                    <div class="flex-col-auto">
                                        <?php if ($this->ion_auth->is_admin() || $permissions->delete_per) : ?>
                                            <button type="button" class="btn btn-danger delete_selected">
                                                Delete Selected
                                            </button>
                                        <?php endif; ?>
                                        <div class="panel-ctrls" id="manage_user_control"></div>
                                    </div>
                                </div>
                                <div class="panel-header brd-0 pt-2"></div>
                                <div class="panel-body pl-0 pr-0">
                                    <div class="row m-0">
                                        <div class="col-md-12">
                                            <table id="notoficationListTable" class="table table-bordered table-striped table-hover" cellspacing="0" width="100%">
                                                <colgroup>
                                                    <col width="5%">
                                                    <col width="5%">
                                                    <col width="15%">
                                                    <col width="15%">
                                                    <col width="45%">
                                                    <col width="5%">
                                                    <col width="10%">
                                                </colgroup>
                                                <thead>
                                                    <tr>
                                                        <th><label class="checkbox-tel"><input type="checkbox" class="select_all"></label></th>
                                                        <th>Action</th>
                                                        <th>Sender</th>
                                                        <th>Receiver</th>
                                                        <th>Subject</th>
                                                        <th>Priority</th>
                                                        <th>Date</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <script type="text/javascript">
                $(document).ready(function() {
                    
                    $(".delete_selected").click(function() {

                        var selected_notification_id = "";

                        $('input[name="notification_id[]"]:checked').each(function() {
                            selected_notification_id = this.value + '|' + selected_notification_id;
                        });
                        
                        
                        if (selected_notification_id != "") {
                            $.confirm({
                                title: 'Confirm!',
                                content: 'Are you sure want to delete?',
                                buttons: {
                                    confirm: function() {
                                        $.ajax({
                                            url: "<?php echo base_url('notifications/delete') ?>",
                                            method: "POST",
                                            data: {
                                                selected_notification_id: selected_notification_id
                                            },
                                            success: function(data) {
                                                CommanJS.getDisplayMessgae(400,'Record has been deleted.');
                                                table.search('').draw();
                                                return true;
                                            }
                                        });
                                    },
                                    cancel: function() {
                                        return true;
                                    }
                                }
                            })
                        } else {

                            CommanJS.getDisplayMessgae(400, 'Please select atleast one Record.')
                            return false;

                        }

                    });

                    $(".select_all").click(function() {
                        $(".selct_class").prop('checked', $(this).prop('checked'));
                    });


                    var table = table = $('#notoficationListTable').DataTable({
                        // Processing indicator
                        "processing": true,
                        // DataTables server-side processing mode
                        "serverSide": true,
                        // Initial no order.
                        "iDisplayLength": 10,
                        "bPaginate": true,
                        "order": [],
                        // Load data from an Ajax source
                        "ajax": {
                            "url": "<?php echo base_url('notifications/getlistings'); ?>",
                            "type": "POST",
                            "data": function(data) {
                                data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                                $(window).scrollTop(0);
                            },
                        },
                        "columns": [{
                                "targets": 0,
                                "data": null,
                                "orderable": false,
                                "render": function(data, type, full, meta) {
                                    var data = '';
                                    if (type === 'display') {
                                        data = '<label class="checkbox-tel"><input type="checkbox" name="notification_id[]" class="selct_class" value="' + full['id'] + '" data-userdata="' + full['user_name'] + '~' + full['email'] + '~' + full['id'] + '"></label>';
                                    }
                                    return data;
                                }
                            },
                            {
                                "targets": 1,
                                "orderable": false,
                                "data": null,
                                "render": function(data, type, full, meta) {
                                    if (type === 'display') {
                                        <?php
                                        if ($this->ion_auth->is_admin() || $permissions->delete_per) : ?>
                                            data = '<a data-toggle="tooltip" title="delete Notification" href="<?= base_url('notifications/delete/') ?>' +
                                                full['id'] + '" class="btn btn-danger btn-sm delete_item"><i class="ti ti-trash"></i></a>';
                                        <?php endif; ?>

                                    }
                                    return data;

                                }
                            },
                            {
                                "data": "sender",
                                "autoWidth": true
                            },
                            {
                                "data": "receiver",
                                "autoWidth": true
                            },
                            {
                                "data": "subject",
                                "autoWidth": true
                            },
                            {
                                "data": "priority",
                                "autoWidth": true
                            },
                            {
                                "data": "created",
                                "autoWidth": true
                            }
                        ]
                    });



                    $('[data-toggle="tooltip"]').tooltip();
                    $(document).on('click', '.delete_item', function(e) {
                        e.preventDefault();
                        var scope = $(this);
                        $.confirm({
                            title: 'Confirm!',
                            content: 'Are you sure want to delete?',
                            buttons: {
                                confirm: function() {
                                    $.get(scope.attr("href"), // url
                                        function(data, textStatus, jqXHR) { // success callback
                                            var obj = JSON.parse(data);
                                            if (obj.msg === 'deleted') {
                                                table.search('').draw();
                                            }
                                        });
                                    return true;
                                },
                                cancel: function() {
                                    return true;
                                }
                            }
                        });
                    });

                });
            </script>