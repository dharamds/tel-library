
<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Auto_Notifications extends MY_Controller
{

    public function __construct()
    {

        parent::__construct();
        //Do your magic here

        $this->load->module('template');
        $this->load->model('common_model');
        $this->load->helper(['html', 'form']);
        $this->load->library('form_validation');

        if (!$this->ion_auth->logged_in()) :
            redirect('users/auth', 'refresh');
        endif;
    }

    /**
     * setup method
     * @description this function use to create container
     * @return void
     */
    public function index()
    {
        $data['breadcrumb'][]   = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]   = ['title' => 'Notification Templates', 'link' => '', 'class' => ''];
        $data['listings']  = $this->common_model->DJoin('notification_templates.*,containers.name as container_name', 'notification_templates', 'containers', 'notification_templates.container_id=containers.id', '', '', 'notification_templates.status=1');
        $data['page']           = "Auto_Notifications/listings";

        $this->template->template_view($data);
    }

    /**
     * add_new method
     * @description this method is use to insert site creatation data in databse
     * @param string, numbers
     * @return void
     */
    public function save($id = NULL)
    {
        if ($this->input->post()) {
            $validation_rules = [
                [
                    'field' => 'container_id',
                    'label' => 'container_id',
                    'rules' => 'required',
                    'errors' => [
                        'required' => 'Please select Container',
                    ],
                ],
                [
                    'field' => 'temp_code',
                    'label' => 'temp_code',
                    'rules' => 'required',
                    'errors' => [
                        'required' => 'Please enter Temporary Code',
                    ],
                ],
                [
                    'field' => 'type',
                    'label' => 'type',
                    'rules' => 'required',
                    'errors' => [
                        'required' => 'Please select Type',
                    ],
                ],
                [
                    'field' => 'message',
                    'label' => 'message',
                    'rules' => 'required',
                    'errors' => [
                        'required' => 'Please select message',
                    ],
                ],
                [
                    'field' => 'from_email',
                    'label' => 'from_email',
                    'rules' => 'required',
                    'errors' => [
                        'required' => 'Please select From Email Detail',
                    ],
                ],
                [
                    'field' => 'from_name',
                    'label' => 'from_name',
                    'rules' => 'required',
                    'errors' => [
                        'required' => 'Please select From Name',
                    ],
                ],
            ];
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run() == FALSE) {
                set_flashdata('error', 'Please fill all fields !');
                if ($id) :
                    $data['details'] =  $this->common_model->getAllData('notification_templates', '*', true,  ['id' => $id]);
                endif;
                $condition = '';
                if ($this->container_id > 0) {
                    $condition = ['id' => $this->container_id];
                }
                $containers = $this->common_model->getAllData('containers', 'id, name', '', $condition);

                $data['containers'][] = '--Select Container--';
                foreach ($containers as $container) {
                    $data['containers'][$container->id] = $container->name;
                }
                $data['page'] = "Auto_Notifications/save";
                $this->template->template_view($data);
            } else {

                $data = array(
                    'container_id' =>  post('container_id'),
                    'temp_code' =>  strtoupper(post('temp_code')),
                    'type' =>  post('type'),
                    'subject' => post('subject'),
                    'message' => post('message'),
                    'from_email' => post('from_email'),
                    'from_name' => ucwords(strtolower(post('temp_code')))
                );



                if ($id) {
                    $check_temp_code = $this->common_model->getAllData('notification_templates', 'id,temp_code,container_id', '', ['id' => $id]);
                    if ($check_temp_code[0]->id == $id && $check_temp_code[0]->temp_code == post('temp_code') && $check_temp_code[0]->container_id == post('container_id')) {
                        $this->common_model->UpdateDB('notification_templates', ['id' => $id], $data);
                    } else if ($check_temp_code[0]->id == $id  && $check_temp_code[0]->temp_code != post('temp_code')) {
                        $this->common_model->UpdateDB('notification_templates', ['id' => $id], $data);
                    } else {
                        set_flashdata('error', 'Template code already exists');
                        redirect('Auto_Notifications', 'refresh');
                    }
                } else {
                    $check_temp_code = $this->common_model->getAllData('notification_templates', 'id', '', ['container_id' => post('container_id'), "temp_code" => post("temp_code")]);
                    if (count($check_temp_code) >= 1) {
                        set_flashdata('error', 'Template code already exists');
                        redirect('Auto_Notifications', 'refresh');
                    } else {
                        $data['status'] = 1;
                        $this->common_model->InsertData('notification_templates', $data);
                    }
                }



                set_flashdata('success', 'Template has been save successfully');
                redirect('Auto_Notifications', 'refresh');
            }
        } else {
            if ($id) :
                $data['details'] =  $this->common_model->getAllData('notification_templates', '*', true,  ['id' => $id]);
            endif;

            $condition = '';
            if ($this->container_id > 0) {
                $condition = ['id' => $this->container_id];
            }
            $containers = $this->common_model->getAllData('containers', 'id, name', '', $condition);

            $data['containers'][] = '--Select Container--';
            foreach ($containers as $container) {
                $data['containers'][$container->id] = $container->name;
            }
            $data['currentcontainers'][] = $this->container_id;
            $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
            $data['breadcrumb'][]           = ['title' => 'Notification Templates', 'link' => base_url('Auto_Notifications'), 'class' => ''];
            $data['breadcrumb'][]           = ['title' => ($id) ? 'Edit' : 'Add', 'link' => '', 'class' => 'active'];
            $data['page']                   = "Auto_Notifications/save";
            $this->template->template_view($data);
        }
    }

    public function change_status($id = NULL, $status = 0)
    {
        $this->common_model->UpdateDB('notification_templates', ['id' => $id], ['status' => $status]);
        set_flashdata('success', 'Template has been save successfully');
        redirect('Auto_Notifications', 'refresh');
    }

    public function get_html_type()
    {
        if ($_POST['type'] == 1) {
            $html = '<div class="col-md-12">
            <div class="form-group col-xs-12 p-0">
            <label for="message" class="col-md-12 control-label">Select Roles</label>
            <div class="col-md-9">';
            $roles = $this->common_model->getAllData('roles', 'id, name', '');
            foreach ($roles as $roles_data) {
                $html .= ' <input type="checkbox" name="roles[]" value="' . $roles_data->id . '">&nbsp;' . ucfirst($roles_data->name) . '<br>';
            }
            //$trigger;
            $html .= '
            <div class="form-group col-xs-12 p-0">
            <label for="temp_code" class="col-md-12 control-label">Trigger</label>
            <div class="col-md-6">
                <select name="notification_triggers" id="notification_triggers" class="form-control">
                    <option value="">Select a Trigger</option>';
            $notification_triggers = $this->common_model->getAllData('notification_triggers', 'id, title', '', ["type_id" => $_POST['type']]);
            foreach ($notification_triggers as $notification_triggers_data) {
                $html .= ' <option value="' . $notification_triggers_data->id . '">' . $notification_triggers_data->title . '</option>';
            }
            $html .= ' </select>
            </div>';

            $html .= '
            <div class="form-group col-xs-12 p-0">
            <label for="temp_code" class="col-md-12 control-label">Courses</label>
            <div class="col-md-6">
                <select name="courses" id="courses" class="form-control">
                    <option value="">Select Course</option>';
            $courses = $this->common_model->getAllData('courses', 'id, name', '');
            foreach ($courses as $courses_data) {
                $html .= ' <option value="' . $courses_data->id . '">' . $courses_data->name . '</option>';
            }
            $html .= ' </select>
            </div>';

            $html .= '
            <div class="form-group col-xs-12 p-0">
            <label for="temp_code" class="col-md-12 control-label">Courses</label>
            <div class="col-md-6">
                <select name="institution" id="institution" class="form-control">
                    <option value="">Select Institution</option>';
            $containers = $this->common_model->getAllData('containers', 'id, name', '');
            foreach ($containers as $containers_data) {
                $html .= ' <option value="' . $containers_data->id . '">' . $containers_data->name . '</option>';
            }
            $html .= ' </select>
            </div>';

            $html .= '</div>
            </div>
        </div>
            </div>';
        }


        echo $html;
        exit;
    }
}
