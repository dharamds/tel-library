            <div class="container-fluid">
                <div data-widget-group="group1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default panel-grid">
                                <div class="panel-heading">
                                    <a class="btn btn-primary
                               " href="<?= base_url('Auto_Notifications/save') ?>">Add New</a>
                                    <div class="panel-ctrls"></div>
                                </div>
                                <div class="panel-body pl-0 pr-0">
                                    <div class="row m-0">
                                        <div class="col-md-12">

                                            <table class="table table-hover" id="example">
                                                <thead>
                                                    <tr>
                                                        <th>Sr.</th>
                                                        <th>Container</th>
                                                        <th>Temporary Code</th>
                                                        <th>Title</th>
                                                        <th>Status</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                        <?php
                                                        $i = 1;
                                                        foreach ($listings as $key => $value) : ?>
                                                            <tr>
                                                                <td><?php echo $i; ?></td>
                                                                <td><?php echo $value->container_name; ?></td>
                                                                <td><?php echo $value->temp_code; ?></td>
                                                                <td><?php echo $value->subject; ?></td>
                                                                <td>
                                                                    <?php if ($value->status == 1) : ?>
                                                                        <a href="<?php bs('notifications_templates/change_status') ?>/<?php echo $value->id ?>/0" data-toggle="tooltip" data-placement="top" title="Click to Change Status"><button type="button" class="btn btn-success btn-status btn-sm">Active</button></a>
                                                                    <?php else : ?>
                                                                        <a href="<?php bs('notifications_templates/change_status') ?>/<?php echo $value->id ?>/1" data-toggle="tooltip" data-placement="top" title="Click to Change Status"><button type="button" class="btn btn-danger btn-status btn-sm">Inactive</button></a>
                                                                    <?php endif ?>
                                                                </td>
                                                                <td>
                                                                    <a class="btn btn-primary btn-sm " href="<?php bs(); ?>notifications_templates/save/<?php echo $value->id ?>"><i class="ti ti-pencil"></i></a>&nbsp;

                                                                </td>
                                                            </tr>
                                                            <?php
                                                            $i++;
                                                        endforeach;
                                                        ?>
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>