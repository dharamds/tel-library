<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
  Author Akash Hedaoo
  Date 29/04/2019
 */

class MediaSources_model extends CI_Model {

    function __construct() {
        // Set table name
        $this->table = 'media_sources';
        // Set orderable column fields
        $this->column_order = array(null,null,'status', null, 'title','caption','categories', 'tags' , 'system_categories' , 'system_tags', 'created', null,null,'license_url','license_and_citation');
        // Set searchable column fields
        $this->column_search = array('media_sources.title', 'media_sources.caption');
        // Set default order
        $this->order = array('media_sources.id' => 'desc');
    }

    /*
     * Fetch data from the database
     * @param $_POST filter data based on the posted parameters
     */

    public function media_sources_list() {
        $this->db->where('delete_status', 1);
        $query = $this->db->get('media_sources');
        return $query->result();
    }

    /*
     * Fetch category data from the database
     * @param $_POST filter data based on the posted parameters
     */

    public function get_category_list() {
        $array = array('type' => 3, 'status' => 1);
        $this->db->select('id,name');
        $this->db->where($array);
        $query = $this->db->get('categories');
        return $query->result_array();
    }

    /*
     * Fetch data from the database
     * @param $id filter data based on the particular id from url
     */

    public function edit($id = null) {
        $this->db->where('id', $id);
        $query = $this->db->get('media_sources');
        return $query->row();
    }

    /*
     * Update data into the database
     * @param $id filter data based on the particular id from url
     */

    public function update($id = null, $data) {
        $this->db->where('id', $id);
        $update = $this->db->update('media_sources', $data);
        if ($update):
            return true;
        endif;
    }

    /*
     * update delete_status for active inactive skills from the database
     * @param $id filter data based on the particular id from url
     */

    public function update_status($id = null, $data) {
        $this->db->where('id', $id);
        $delete = $this->db->update('media_sources', $data);
        if ($delete):
            return true;
        endif;
    }

    /*
     * Fetch members data from the database
     * @param $_POST filter data based on the posted parameters
     */

    public function getRows($postData) {
        //pr($postData); exit;
        $this->_get_datatables_query($postData);

        if ($postData['length'] != -1) {
            $this->db->limit($postData['length'], $postData['start']);
        }
        $query = $this->db->get();
        $data['result'] = $query->result();
        $data['total'] = $this->db->select('FOUND_ROWS() as total', false)->get()->row()->total;
        return $data;
    }

    /*
     * Perform the SQL queries needed for an server-side processing requested
     * @param $_POST filter data based on the posted parameters
     */

    private function _get_datatables_query($postData) {
        $this->db->select('SQL_CALC_FOUND_ROWS media_sources.*', false);
        //  $this->db->select("IF(".$this->table.".status = 1, 'Publish', 'Unpublish') as status");
        $this->db->select('DATE_FORMAT(' . $this->table . '.created, "%m/%d/%Y") as created');
         // courses metadata
         if ((isset($postData['media_category']) && count($postData['media_category']) > 0) || $postData['order']['0']['column'] == 5) 
         {
         $this->db->select('GROUP_CONCAT( DISTINCT categories.name SEPARATOR \'<br> \')  as categories');
         }else{
            $this->db->select('"..." as categories');
        }    
         if ((isset($postData['media_tag']) && count($postData['media_tag']) > 0) || $postData['order']['0']['column'] == 6) 
         {
         $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT tags.name ORDER BY tags.name ASC  SEPARATOR "<br>") , \'NA\') as tags');
         }else{
             $this->db->select('"..." as tags');
         } 


         // system metadata
         if ((isset($postData['system_cat']) && count($postData['system_cat']) > 0) || $postData['order']['0']['column'] == 7) 
         {
         $this->db->select('GROUP_CONCAT( DISTINCT system_category.name SEPARATOR \'<br> \')  as system_categories');
             /*Tags*/
         }else{
            $this->db->select('"..." as system_categories');
        }    
        //System Tags   
        if ((isset($postData['system_tag']) && count($postData['system_tag']) > 0) || $postData['order']['0']['column'] == 8) 
        {
            $this->db->select('COALESCE(GROUP_CONCAT(DISTINCT system_tags.name ORDER BY system_tags.name ASC  SEPARATOR "<br>"), \'NA\')  as system_tags');
        }else{
            $this->db->select('"..." as system_tags');
        }
         

        $this->db->from('media_sources');
        //$this->db->join('users', $this->table . '.created_by = users.id', 'left');

        $this->db->join('category_assigned', 'category_assigned.reference_id = '.$this->table.'.id AND category_assigned.reference_type = '.META_MEDIA, 'left');
        $this->db->join('categories', 'categories.id = category_assigned.category_id AND categories.delete_status = 1 AND categories.status = 1', 'left');
        
        if ((isset($postData['media_tag']) && count($postData['media_tag']) > 0) || $postData['order']['0']['column'] == 6) 
        {
        $this->db->join('tag_assigned', 'tag_assigned.reference_id = '.$this->table.'.id AND tag_assigned.reference_type = '.META_MEDIA, 'left');
        $this->db->join('tags', 'tags.id = tag_assigned.tag_id  AND tags.delete_status = 1 AND tags.status = 1', 'left');
        }
        
        $this->db->join('category_assigned as system_category_assigned', 'system_category_assigned.reference_id = '.$this->table.'.id AND system_category_assigned.reference_type = '.META_SYSTEM, 'left');
        $this->db->join('categories as system_category', 'system_category.id = system_category_assigned.category_id AND system_category.delete_status = 1 AND system_category.status = 1', 'left');

        if ((isset($postData['system_tag']) && count($postData['system_tag']) > 0) || $postData['order']['0']['column'] == 8) 
        {
        $this->db->join('tag_assigned as system_tag_assigned', 'system_tag_assigned.reference_id = '.$this->table.'.id AND system_tag_assigned.reference_type = '.META_SYSTEM, 'left');
        $this->db->join('tags as system_tags', 'system_tags.id = system_tag_assigned.tag_id  AND system_tags.delete_status = 1 AND system_tags.status = 1', 'left');
        }
   
       

        $i = 0;
        // loop searchable columns 
          // loop searchable columns 
          foreach($this->column_search as $item){
            // if datatable send POST for search
            if($postData['search']['value']){
                // first loop
                if($i === 0){
				    // open bracket 
					$this->db->group_start();
					//pr( $item);	
                    $this->db->like($item, $postData['search']['value'], 'both');
                }else{
                    $this->db->or_like($item, $postData['search']['value'], 'both');
                }
                    // last loop
                if(count($this->column_search) - 1 == $i){
                    // close bracket
                    $this->db->group_end();
                }
            }
            $i++;
        }
             

        if($postData['system_cat']) {
            $this->db->where_in('system_category_assigned.category_id', $postData['system_cat']);
        }
        if($postData['system_tag']) {
            $this->db->where_in('system_tag_assigned.tag_id', $postData['system_tag']);
        }
        if($postData['media_category']) {
            $this->db->where_in('category_assigned.category_id', $postData['media_category']);
        }
        if($postData['media_tag']) {
            $this->db->where_in('tag_assigned.tag_id', $postData['media_tag']);
        }
       
        if($postData['license_and_citation']) {
            $this->db->where('license_and_citation', $postData['license_and_citation']);
        }
        $this->db->where('media_sources.delete_status', 1);

        $this->db->group_by($this->table.'.id');

        if (isset($postData['order'])) {
            $this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    //Count media_sources
    public function count_media_sources() {
        $this->db->select('*');
        $this->db->from('media_sources');
        return $this->db->count_all_results();
    }

    /*
     * Count records based on the filter params
     * @param $_POST filter data based on the posted parameters
     */

    public function countFiltered($postData) {
        $this->_get_datatables_query($postData);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function InsertData($table, $Data) {
        $Insert = $this->db->insert($table, $Data);
        if ($Insert):
            return $this->db->insert_id();
        endif;
    }
}

/* End of file Users_modal.php */
