<div class="static-content-wrapper">
   <div class="static-content">
      <div class="page-content">
         <ol class="breadcrumb">
             <li class=""><a href="<?php echo base_url() ?>">Home</a></li>
                <li class=""><a href="<?php echo base_url('media_sources/MediaSources/') ?>">Media Sources</a></li>
                <li class="active"><a href=""> Media Source Detail </a></li>
         </ol>
         <div class="container-fluid">
            <div class="tab-pane active" id="tab-about">
               <div class="panel panel-default">
                  <div class="panel-heading">
                     <h2>Certificate Details </h2>
                  </div>
                  <div class="panel-body">
                     <div class="about-area">
                        <div class="table-responsive">
                           <table class="table about-table">
                              <tbody>
                                  
                                 <tr>
                                    <th>Title</th>
                                    <td width="5%"></td>
                                    <td><a href="<?php echo $data->file_link ?>"></a><?php 
                                          echo $data->title;?>
                                    </td>
                                 </tr>
                                 
                                 <tr>
                                    <th>Author</th>
                                    <td width="5%"></td>
                                    <td><?php echo $data->author;?></td>
                                 </tr> 
                                 
                                  <tr>
                                    <th>Publisher</th>
                                    <td width="5%"></td>
                                    <td><?php echo $data->publisher;?></td>
                                 </tr>
                                 
                                  <tr>
                                    <th>License</th>
                                    <td width="5%"></td>
                                    <td><a href="<?php echo $data->publisher;?>">Public domain</a></td>
                                 </tr>
                                 
                                 <tr>
                                    <th>Created by</th>
                                    <td width="5%"></td>
                                    <td><?php 
                                       $authorData = $this->ion_auth->user($certificate_data->created_by)->row(); 
                                       echo ucfirst($authorData->first_name); echo " ";  
                                       echo ucfirst($authorData->last_name);
                                       ?></td>
                                 </tr>  
                                 
                                 <tr>
                                    <th>Created on</th>
                                    <td width="5%"></td>
                                    <td><?php echo date('m-d-Y',strtotime($certificate_data->created)); ?></td>
                                 </tr>
                                 
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- .container-fluid -->
</div>
<!-- #page-content -->
