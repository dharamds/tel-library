<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="filter-container flex-row">
				<div class="flex-col-sm-2">
					<select id="license_and_citation" class="selected_condition form-control">
						<option value="0"> --Media Type--</option>
						<option value="1">License & Citation</option>
					</select>
				</div>
				<div class="flex-col-sm-3 selected_condition" id="system_cat">

				</div>
				<div class="flex-col-sm-2 selected_condition" id="system_tag">

				</div>
				<div class="flex-col-sm-2 selected_condition" id="media_cat">

				</div>
				<div class="flex-col-sm-2 selected_condition" id="media_tag">

				</div>

				<div class="flex-col-12 flex-col-md-auto ml-auto">
					<a class="btn btn-danger pt-2" id="reset_form" href="javascript:void(0)"><i class="flaticon-close-1 f21 fw100"></i></a>
				</div>
			</div>
		</div>
	</div>

	<div data-widget-group="group1">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default panel-grid">
					<div class="panel-heading">
						<div class="flex-row">
							<div class="flex-col">
								<a class="btn btn-primary" href="<?= base_url('mediasources/save') ?>">Add New</a>
							</div>
							<div class="flex-col">
								<div class="panel-ctrls"></div>
							</div>
						</div>
					</div>
					<div class="panel-body no-padding">
						<div class="row m-0">
							<div class="col-md-12 p-0">
								<table id="memListTable" class="table table-bordered table-striped table-hover" cellspacing="0">
									<thead>
										<tr>
											<th>
												<label class="checkbox-tel"><input type="checkbox" class="checkAll"></label>
											</th>
											<th style="min-width: 100px;">Action</th>
											<th style="min-width: 80px;">Status</th>
											<th style="min-width: 120px;">Thumbnail</th>
											<th style="min-width: 180px;">Media Title</th>
											<th style="min-width: 180px;">Caption</th>
											<th style="min-width: 180px;">Media Category</th>
											<th style="min-width: 180px;">Media Tags</th>
											<th style="min-width: 180px;">System Category</th>
											<th style="min-width: 180px;">System Tags</th>
											<th style="min-width: 180px;">Created Date</th>
											<th style="min-width: 180px;">Creater</th>
											<th style="min-width: 280px;"># of times used</th>
											<th style="min-width: 280px;">Used of these pages</th>
											<th style="min-width: 150px;">Media Type</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- .container-fluid -->
<script>
	$(document).ready(function() {
		$('.selected_condition').on('change', function() {
			table.search('').draw();
		});

		$('#reset_form').click(function() {
			$(".meta_data_filter").prop("checked", false);
			$(".selectFilterMode span").text('');
			table.search('').draw();
		});

		$('.checkAll').click(function() {
			$(':checkbox.checkLesson').prop('checked', this.checked);
		});


		CommanJS.get_metadata_options("System category", 1, <?= META_SYSTEM; ?>, "system_cat");
		CommanJS.get_metadata_options("System tag", 2, <?= META_SYSTEM; ?>, "system_tag");
		CommanJS.get_metadata_options("Media category", 1, <?= META_MEDIA; ?>, "media_cat");
		CommanJS.get_metadata_options("Media tag", 2, <?= META_MEDIA; ?>, "media_tag");


		var table = $('#memListTable').DataTable({
			// Processing indicator
			"processing": true,
			// DataTables server-side processing mode
			"serverSide": true,
			// Initial no order.
			"iDisplayLength": 10,
			"bPaginate": true,
			"order": [],
			"scrollX": true,
			"autoWidth": false,
			"drawCallback": function(settings) {
                $('.load_meta_data').each(function(key, item) {
                    $.getJSON("<?php echo bs('questions/get_meta_tags/'); ?>" + $(this).attr('id'), function(data) {
                           if(data) $("#"+data.typeid).html(data.value);
                    });
				});
				$('.load_meta_category').each(function(key, item) {
                    $.getJSON("<?php echo bs('questions/get_meta_categories/'); ?>" + $(this).attr('id'), function(data) {
                           if(data) $("#"+data.typeid).html(data.value);
                    });
				});

                },
			// Load data from an Ajax source
			"ajax": {
				"url": "<?php echo base_url('media_sources/mediaSources/getLists'); ?>",
				"type": "POST",
				"data": function(data) {
					data.system_cat = CommanJS.getMetaCallBack('system_category');
					data.system_tag = CommanJS.getMetaCallBack('system_tag');
					data.media_category = CommanJS.getMetaCallBack('media_category');
					data.media_tag = CommanJS.getMetaCallBack('media_tag');
					data.license_and_citation = $('#license_and_citation option:selected').val();
					data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
					$(window).scrollTop(0);
				}
			},
			//Set column definition initialisation properties
			"columnDefs": [{
					"targets": 3,
					"data": null,
					"render": function(data, type, full, meta) {
						if (type === 'display') {
							var thumbnail = (full['file_name']) ? full['file_name'] : 'nophoto.gif';
							data = CommanJS.getImagebox(full['file_name'],'datatable');
						}
						return data;

					}
				},
				{
					"targets": 2,
					"data": null,
					"orderable": false,
					"render": function(data, type, full, meta) {
						if (type === 'display') {
							if (full['status'] == '1') {
								data = '<a href="<?php bs('media_sources/MediaSources/update_status/') ?>' + full['id'] + '/deactivate" data-toggle="tooltip" data-placement="top" title="Click to Change Status" class="btn btn-success btn-status btn-sm change_status">Active</a>';
							} else {
								data = '<a href="<?php bs('media_sources/MediaSources/update_status/') ?>' + full['id'] + '/activate" data-toggle="tooltip" data-placement="top" title="Click to Change Status" class="btn btn-status btn-danger btn-sm change_status">Inactive</a>';
							}
						}
						return data;
					}
				},
				{
					"targets": 1,
					"data": null,
					"orderable": false,
					"render": function(data, type, full, meta) {
						if (type === 'display') {
							data = '<a class="btn btn-primary btn-sm" href="<?php echo bs('mediasources/save/') ?>' + full['id'] + '"><i class="ti ti-pencil"></i></a>';
							url = '<?php echo bs('media_sources/MediaSources/delete/') ?>' + full['id'];
							//data += '<a href="<?php echo bs('media_sources/MediaSources/detail/') ?>' + full['id'] + '" class="btn btn-warning btn-sm " ><i class="ti ti-eye"></i></a>';
							data += '<a href="' + url + '" class="btn btn-danger btn-sm delete_item" ><i class="ti ti-trash"></i></a>';
						}
						return data;
					}
				},
				{
                    "targets": [6],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                         data = '<span id="' + full['id'] +'_<?php echo META_MEDIA; ?>_<?php echo META_MEDIA; ?>_cat" data-type="media_category" class="load_meta_category"> Loading...</span>';
						  //data = 1;
						}
                        return data;
                    }
                },
				{
                    "targets": [7],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                           data = '<span id="' + full['id'] +'_<?php echo META_MEDIA; ?>_<?php echo META_MEDIA; ?>" data-type="media_tags" class="load_meta_data"> Loading...</span>';
                        }
                        return data;
                    }
                },
                {
                    "targets": [8],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                           data = '<span id="' + full['id'] +'_<?php echo META_MEDIA ; ?>_<?php echo META_SYSTEM; ?>_cat" data-type="system_category" class="load_meta_category"> Loading...</span>';
						 //data = 2;
						}
                        return data;
                    }
                },
                {
                    "targets": [9],
                    "data": null,
                    "render": function (data, type, full, meta) {
                        if (type === 'display') {
                           data = '<span id="' + full['id'] +'_<?php echo META_MEDIA ; ?>_<?php echo META_SYSTEM; ?>" data-type="system_tags" class="load_meta_data"> Loading...</span>';
                        }
                        return data;
                    }
                },
				{
					"targets": 14,
					"data": null,
					"render": function(data, type, full, meta) {
						// console.log(full);
						if (full['license_and_citation'] == 1) {
							data = 'License & Citation';
						} else {
							data = '';
						}
						return data;
					}
				},
				{
					"targets": 0,
					"orderable": false,
					"data": null,
					"render": function(data, type, full, meta) {
						var data = '';
						if (type == 'display') {
							data = '<label class="checkbox-tel"><input type="checkbox" class="checkLesson" name="item[]" value="' + full['id'] + '"></label>';
						}
						return data;
					}
				}
			],
			"columns": [{
					"data": "id"
				},
				{
					"data": "id"
				},
				{
					"data": "id"
				},
				{
					"data": "id"
				},
				{
					"data": "title"
				},
				{
					"data": "caption"
				},
				{
					"data": "categories"
				},
				{
					"data": "tags"
				},
				{
					"data": "system_categories"
				},
				{
					"data": "system_tags"
				},
				{
					"data": "created"
				},
				{
					"data": "author"
				},
				{
					"data": "author"
				},
				{
					"data": "author"
				},
				{
					"data": "id"
				}
			]
		});
		$(document).on('click', '.change_status', function(e) {
			e.preventDefault();
			$.get($(this).attr("href"), // url
				function(data, textStatus, jqXHR) { // success callback
					var obj = JSON.parse(data);
					if (obj.msg === 'Updated') {
						table.ajax.reload(); //just reload table
					}
				});
		});

		$(document).on('click', '.delete_item', function(e) {
			e.preventDefault();
			var scope = $(this);
			$.confirm({
				title: 'Confirm!',
				content: 'Are you sure?',
				buttons: {
					confirm: function() {
						$.get(scope.attr("href"), // url
							function(data, textStatus, jqXHR) { // success callback
								var obj = JSON.parse(data);
								if (obj.msg === 'deleted') {
									table.ajax.reload(); //just reload table
								}
							});
						return true;
					},
					cancel: function() {
						return true;
					}
				}
			});
		});
	});
</script>