<?php
//pr($mediaData);
$media_id = isset($mediaData->id) ? $mediaData->id : null;
?>

<div class="container-fluid pr-0">
    <div class="panel panel-default">
        <div class="panel-heading">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active" data-tabid="content">
                    <a href="#content" class="set_tab" role="tab" data-toggle="tab" data-id="<?php echo $qid; ?>" data-queid="<?php echo isset($qid) ? $qid : 0; ?>"> Content</a>
                </li>
                <li role="presentation" data-tabid="metadata">
                    <a href="#metadata" class="set_tab" id="metadata_id" role="tab" data-toggle="tab" data-id="<?php echo $qid; ?>" data-queid="<?php echo isset($qid) ? $qid : 0; ?>">Metadata</a>
                </li>
            </ul>
        </div>
        <div class="panel-body">
            <div class="tab-content">
                <!-- Content -->
                <div role="tabpanel" class="tab-pane active" id="content">

                    <?php echo form_open('media_sources/MediaSources/upload_docs', array('id' => 'upload_docs', 'enctype' => 'multipart/form-data', 'style' => 'position:absolute')); ?>

                    <?php
                    echo form_input([
                        'name' => 'file_name',
                        'id' => 'file_name',
                        'type' => 'file',
                        'class' => 'form-control removerules',
                        'value' => $data->file_name,
                        'style' => 'visibility:hidden;'
                    ]);
                    echo form_close();
                    ?>
                    <?php echo form_open('media_sources/MediaSources/add', array('id' => 'media_form_validation', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')); ?>
                    <?php
                    $media_hid = [
                        'type'  => 'hidden',
                        'id'    => 'media_id',
                        'name'  => 'id',
                        'value' => $media_id
                    ];
                    echo form_input($media_hid);
                    ?>
                    <div class="row">
                        <div class="row m-0 mb-4">
                            <div class="col-md-12">
                                <div class=" col-md-3 ist-logo p-0">
                                
                                </div>

                                <div class="col-md-9 pl-5">
                                    <div class="ist-visual-info pt-4">
                                        <h4 class="m-0 pb-3">Select Media File</h4>
                                        <div class="row m-0">
                                            <div class="col-sm-12 p-0">
                                                <div class="fileinput fileinput-new mb-0" data-provides="fileinput">
                                                    <div>
                                                        <a class="btn btn-danger fileinput-exists delete_img" data-dismiss="fileinput">Remove</a>
                                                        <span class="btn btn-primary btn-primary-default btn-file m-0">
                                                            <span class="fileinput-new">Choose File</span>
                                                            <span class="fileinput-exists">Change</span>
                                                            <input type="button" id="upload_image" value="Choose File" class="blog_img1 visible1">
                                                            <?php if (isset($media_id)) { ?>
                                                                <input type="hidden" id="file_name" name="file_name" value="<?php echo $mediaData->file_name; ?>">
                                                            <?php } else { ?>
                                                                <input type="hidden" id="file_name" name="file_name" value="">
                                                            <?php } ?>
                                                        </span>
                                                        <input type="hidden" name="media_img" id="media_img" value="">

                                                    </div>
                                                </div>

                                                <div style="color:red;" id="upload_error"></div>

                                                <div>
                                                    <div class='progress' id="progressDivId" style="display:none">
                                                        <div class='progress-bar' id='progressBar'></div>
                                                        <div class='percent' id='percent'>0%</div>
                                                    </div>
                                                    <div style="height: 10px;"></div>
                                                    <div id='outputImage'></div>
                                                </div>
                                            </div>
                                            <div class="small">(File must be .png, .jpg, .jpeg, .mp3,<br> .mp4, .ogg, .aac , .xlsx, .xls, .doc , .docx, .pdf)</div>
                                            <img id="imgLoader" src="<?php echo base_url(); ?>/public/assets/img/spinner.gif" style="display:none;" />
                                            <!---->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row m-0 mb-4">
                            <div class="col-md-8">
                                <div class="form-group m-0">
                                    <label for="fieldname" class="control-label">Title <span class="field-required">*</span></label>
                                    <?php
                                    echo form_input([
                                        'name' => 'title',
                                        'id' => 'title',
                                        'class' => 'form-control removerules',
                                        // 'rows' => '5',
                                        'value' => $data->title,
                                    ]);
                                    ?>
                                    <?php echo form_error('title', '<div class="error">', '</div>'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row m-0 mb-4">
                            <div class="col-md-8">
                                <div class="form-group m-0">
                                    <label for="fieldname" class="control-label">Caption</label>
                                    <?php
                                    echo form_input([
                                        'name' => 'caption',
                                        'id' => 'caption',
                                        'class' => 'form-control removerules',
                                        // 'rows' => '5',
                                        'value' => $data->caption,
                                    ]);
                                    ?>
                                    <?php echo form_error('title', '<div class="error">', '</div>'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row m-0 mb-4">
                            <div class="col-md-8">
                                <div class="form-group m-0">
                                    <label for="fieldname" class="control-label">Alternative Text</label>
                                    <?php
                                    echo form_input([
                                        'name' => 'alternative_text',
                                        'id' => 'alternative_text',
                                        'class' => 'form-control removerules',
                                        // 'rows' => '5',
                                        'value' => $data->alternative_text,
                                    ]);
                                    ?>
                                    <?php echo form_error('title', '<div class="error">', '</div>'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row m-0 mb-4">
                            <div class="col-md-8">
                                <div class="form-group m-0">
                                    <label for="description" class="control-label"> Description </label>
                                    <textarea class="editor" id="description" name="description" rows="10"><?php echo isset($data->description) ? $data->description : ''; ?></textarea>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row m-0 mb-4">
                            <div class="col-md-8">
                                <div class="form-group m-0">
                                    <label for="fieldname" class="control-label">License & Citation</label>
                                    
                                     <select onchange="valueChanged()"  name="license_and_citation" id="license_and_citation" class="form-control" >
                                         <option value="">--Select--</option>
                                         <option value="1" <?php echo !empty($data->license_and_citation)? 'selected':''; ?> >License & Citation</option>
                                     </select>
                                   
                                </div>
                            </div>
                        </div>
                        <div class="row m-0 mb-4 license_div">
                            <div class="col-md-4">
                                <div class="form-group m-0">
                                    <label for="fieldname" class="col-md-12 control-label pl-0"> Item Title</label>
                                    <?php
                                    echo form_input([
                                        'name' => 'item_title',
                                        'id' => 'item_title',
                                        'class' => 'form-control removerules',
                                        'value' => $data->item_title,
                                    ]);
                                    ?>
                                    <?php echo form_error('author', '<div class="error">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group m-0">
                                    <label for="fieldname" class="control-label"> Title URL</label>
                                    <?php
                                    echo form_input([
                                        'name' => 'title_url',
                                        'id' => 'title_url',
                                        'class' => 'form-control removerules',
                                        'value' => $data->title_url,
                                    ]);
                                    ?>
                                    <?php echo form_error('file_link', '<div class="error">', '</div>'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row m-0 mb-4 license_div">
                            <div class="col-md-4">
                                <div class="form-group m-0">
                                    <label for="fieldname" class="col-md-12 control-label  pl-0">Creator</label>
                                    <?php
                                    echo form_input([
                                        'name' => 'author',
                                        'id' => 'author',
                                        'class' => 'form-control removerules',
                                        'value' => $data->author,
                                    ]);
                                    ?>
                                    <?php echo form_error('author', '<div class="error">', '</div>'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row m-0 mb-4 license_div">
                            <div class="col-md-4">
                                <div class="form-group m-0">
                                    <label for="fieldname" class="col-md-12 control-label  pl-0">Publisher - Source</label>
                                    <?php
                                    echo form_input([
                                        'name' => 'publisher',
                                        'id' => 'publisher',
                                        'class' => 'form-control removerules',
                                        'value' => $data->publisher,
                                    ]);
                                    ?>
                                    <?php echo form_error('author', '<div class="error">', '</div>'); ?>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row m-0 mb-4 license_div">
                            <div class="col-md-4">
                                <div class="form-group m-0">
                                    <label for="fieldname" class="control-label">License</label>
                                    <?php
                                    echo form_input([
                                        'name' => 'license',
                                        'id' => 'license',
                                        'class' => 'form-control removerules',
                                        'value' => $data->license,
                                    ]);
                                    ?>
                                    <?php echo form_error('publisher', '<div class="error">', '</div>'); ?>
                                </div>
                            </div>

                            <div class="col-md-8 ">
                                <div class="form-group m-0">
                                    <label for="fieldname" class="control-label">License URL</label>
                                    <?php
                                    echo form_input([
                                        'name' => 'license_url',
                                        'id' => 'license_url',
                                        'class' => 'form-control removerules',
                                        'value' => $data->license_url,
                                    ]);
                                    ?>
                                    <?php echo form_error('license_url', '<div class="error">', '</div>'); ?>
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="colxs-12 p-0">
                        <div class="hr-line mt-5 mb-5"></div>
                    </div>
                    <div class="form-group mb-4">
                        <div class="col-md-12 text-right">
                            <input id="nextContent" type="submit" class="finish btn-primary btn" value="Save">
                        </div>
                        </form>
                    </div>
                </div>
                <!-- Metadata -->
                <div role="tabpanel" class="tab-pane " id="metadata">
                    <div class="pl-4 col-md-12">
                        <div class="col-md-6 pl-0 pt-5">
                            <div class="panel panel-grey">
                                <div class="panel-heading mb-3">
                                    <h2><span>Media Categories</span></h2>
                                </div>
                                <div class="panel-body panel-collapse-body" id="media_categories_section">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 pl-0 pt-5">
                            <div class="panel panel-grey">
                                <div class="panel-heading">
                                    <h2><span>Media Tags</span></h2>
                                    <div class="panel-ctrls button-icon-bg">
                                    </div>
                                </div>
                                <div class="panel-body" id="media_tags_section"> </div>
                            </div>
                        </div>
                    </div>

                    <div class="hr-line col-md-12"></div>

                    <div class="pl-4 col-md-12">
                        <div class="col-md-6 pl-0 pt-5">
                            <div class="panel panel-grey">
                                <div class="panel-heading mb-3">
                                    <h2><span>System Categories</span></h2>
                                </div>
                                <div class="panel-body panel-collapse-body" id="system_categories_section">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 pl-0 pt-5">
                            <div class="panel panel-grey">
                                <div class="panel-heading">
                                    <h2><span>System Tags</span></h2>
                                    <div class="panel-ctrls button-icon-bg">
                                    </div>
                                </div>
                                <div class="panel-body" id="system_tags_section">

                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Metadata ends -->
                </div>
            </div>
            <script type="text/javascript">
                $(document).ready(function() {
                    $('.ist-logo').html(CommanJS.getImagebox('<?php echo bs('uploads/media_images/thumbnails/'.$mediaData->file_name) ?>','thumbnail'));
                    valueChanged();
                });

                function valueChanged() {

                    if ($('#license_and_citation').val()!='')

                        $(".license_div").show();
                    else
                        $(".license_div").hide();
                }
            </script>
            <!-- #page-content -->
            <script type="text/javascript" src="<?php echo base_url('public/assets/js/jquery.form.js') ?>"></script>
            <script type="text/javascript" src="<?= bs('public/assets/plugins/form-jasnyupload/fileinput.min.js') ?>"></script>
            <script>
                $('#metadata_id').click(function() {
                    var mediaId = $('#media_id').val();
                    if (mediaId != '') {
                        $('.nav-tabs a[href="#metadata"]').tab('show');
                        getMetadata();
                    } else {
                        return false;
                    }
                });


                function getMetadata() {
                    var mediaId = $('#media_id').val();
                    CommanJS.getCatSection(<?= META_MEDIA; ?>, 'media_categories_section', 'media_categories', mediaId, <?= META_MEDIA; ?>);
                    CommanJS.getTagSection(<?= META_MEDIA; ?>, 'media_tags_section', 'media_tags', mediaId, <?= META_MEDIA; ?>);
                    // System Category  1
                    CommanJS.getCatSection(<?= META_SYSTEM; ?>, 'system_categories_section', 'system_categories', mediaId, <?= META_MEDIA; ?>);
                    CommanJS.getTagSection(<?= META_SYSTEM; ?>, 'system_tags_section', 'system_tags', mediaId, <?= META_MEDIA; ?>);
                }

                $(document).ready(function() {
                    var config = {
                        height: 200,
                        toolbar: 'short'
                    };
                    $('.editor').each(function(e) {
                        CKEDITOR.replace(this.id, config);
                        CKEDITOR.instances[this.id].updateElement();
                    });

                    var rules = [];
                    $('#upload_image').click(function() {
                        $('#file_name').click();
                    });
                    // Upload course image
                    $(document).on('change', '#file_name', function() {

                        

                            $('#upload_docs').ajaxForm({
                                url: '<?php echo bs('media_sources/MediaSources/upload_docs'); ?>',
                                beforeSubmit: function() {

                                    $("#outputImage").hide();
                                    if ($("#file_name").val() == "") {
                                        $("#outputImage").show();
                                        $("#outputImage").html("<div class='error'>Choose a file to upload.</div>");
                                        return false;
                                    }
                                    $("#progressDivId").css("display", "block");
                                    var percentValue = '0%';

                                    $('#progressBar').width(percentValue);
                                    $('#percent').html(percentValue);
                                },
                                uploadProgress: function(event, position, total, percentComplete) {
                                    var percentValue = percentComplete + '%';
                                    $("#progressBar").animate({
                                        width: '' + percentValue + ''
                                    }, {
                                        duration: 500,
                                        easing: "linear",
                                        step: function(x) {
                                            percentText = Math.round(x * 100 / percentComplete);
                                            $("#percent").text(percentText + "%");
                                            if (percentText == "100") {
                                                $('#progressDivId').hide();
                                            }
                                        }
                                    });
                                },
                                error: function(response, status, e) {
                                    alert('Oops something went.');
                                },
                                complete: function(xhr) {
                                    
                                    if (xhr.responseText && xhr.responseText != "error") {
                                        $("#outputImage").html(xhr.responseText);
                                    } else {
                                        $("#outputImage").show();
                                        $("#outputImage").html("<div class='error'>Problem in uploading file.</div>");
                                        $("#progressBar").stop();
                                    }
                                },
                                success: function(data) {
                                    
                                    
                                    var jsn = JSON.parse(data);
                                    if (jsn.error) {
                                        $('#upload_error').html(jsn.error);
                                    } else {
                                        
                                        var url = jsn.filePath + jsn.fileName;
                                        
                                        $('#up_course_img').hide();
                                        $('#up_course_img').parent().append(CommanJS.getImagebox(url,'thumbnail'));
                                        $('.ist-logo').html(CommanJS.getImagebox(url,'thumbnail'));
                                        $('#media_img').val(jsn.fileName);
                                        $('#ist_logo_id').attr('src', url);
                                        var percentValue = '0%';
                                        $("#progressBar").animate({
                                            'width': percentValue
                                        }, {
                                            step: function(x) {
                                                $("#percent").text("0%");
                                            }
                                        });
                                        $("#media_form_validation").validate().settings.ignore = ":hidden";
                                        CommanJS.getDisplayMessgae(200, 'File updated successfully.');
                                    }
                                }
                            }).submit();
                       
                    });


                    $(document).on('click', 'a.delete_img', function() {
                        var filename = $("#up_course_img").data('imgname');
                        var scope = $(this);
                        $.confirm({
                            title: 'Confirm!',
                            content: 'Are you sure?',
                            buttons: {
                                confirm: function() {
                                    $.ajax({
                                        url: "<?php echo bs(); ?>courses/remove_course_img",
                                        type: "POST",
                                        data: {
                                            'file': filename
                                        },
                                        beforeSend: function() {
                                            $("#imgLoader").show();
                                        },
                                        success: function(data) {
                                            $("#imgLoader").hide();
                                            if (data) {
                                                $("#up_course_img").attr('src', '<?php echo bs(); ?>/public/assets/img/logo-bg.png');
                                            }

                                        }
                                    });
                                    return true;
                                },
                                cancel: function() {
                                    return true;
                                }
                            }
                        });
                    });

                    $.validator.addMethod("noSpace", function(value, element, param) {
                        return value.match(/^(?=.*\S).+$/);
                    }, "No space please and don't leave it empty");

                    $("#media_form_validation").validate({
                        ignore: [],
                        rules: {
                            title: {
                                required: true,
                                noSpace: true
                            }
                        },
                        messages: {
                            title: {
                                required: "Please enter title"
                            }
                        },
                        submitHandler: function(form) {
                            for (instance in CKEDITOR.instances) {
                                CKEDITOR.instances[instance].updateElement();
                            }
                            $.ajax({
                                url: "<?php echo bs('media_sources/MediaSources/add'); ?>",
                                method: "POST",
                                data: $('form#media_form_validation').serialize(),
                                dataType: "json",

                                complete: function(xhr, status) {

                                },
                                success: function(data) {
                                    // console.log(data);
                                    $('#media_id').val(data.id);
                                    CommanJS.getDisplayMessgae(200, data.msg);

                                    $('.nav-tabs a[href="#metadata"]').tab('show');
                                    getMetadata();
                                    $(window).scrollTop(0);

                                }
                            });
                            return false;
                        }
                    });
                    <?php if (!$media_id) : ?>
                        $('#media_img').rules('add', {
                            required: true,
                            min: 1,
                            messages: {
                                required: "Please select File.",
                            }
                        });
                    <?php endif; ?>
                });
            </script>