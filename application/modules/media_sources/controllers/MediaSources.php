<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MediaSources extends MY_Controller
{
    public function __construct()
    {

        parent::__construct();
        //Do your magic here

        $this->load->module('template');
        $this->load->model('common_model');
        $this->load->model('MediaSources_model');
        $this->load->library(array('form_validation'));
        $this->load->helper(array('html', 'language', 'form', 'country_helper'));
        if (!$this->ion_auth->logged_in()) :
            redirect('users/auth', 'refresh');
        endif;
        // get controller permissions
        if ($this->current_user_permissions = $this->get_permissions()) {
            $this->add_permission = $this->current_user_permissions->add_per;
            $this->edit_permission = $this->current_user_permissions->edit_per;
            $this->delete_permission = $this->current_user_permissions->delete_per;
            $this->view_permission = $this->current_user_permissions->view_per;
            $this->list_permission = $this->current_user_permissions->list_per;
        }
        // get container_id
        $this->container_id = get_container_id();
    }

    /**

     * @return [void]
     */
    public function index()
    {

        $data['certificates']           = $this->MediaSources_model->media_sources_list();
        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Media', 'link' => '', 'class' => 'active'];
        $data['page']                   = "media_sources/MediaSources/view";
        $this->template->template_view($data);
    }

    public function remove_course_img()
    {
        $fileName = $_POST['file'];
        if (unlink(APPPATH . '../uploads/media_images/' . $fileName)) {
            unlink(APPPATH . '../uploads/media_images/thumbnails/' . $fileName);
            echo 1;
        } else
            echo 0;
    }

    public function resizeImage($filename)
    {
        $source_path = './uploads/media_images/' . $filename;
        $target_path = './uploads/media_images/thumbnails/';
        $config_manip = [
            'image_library' => 'gd2',
            'source_image' => $source_path,
            'new_image' => $target_path,
            'maintain_ratio' => TRUE,
            'create_thumb' => TRUE,
            'width' => 150,
            'height' => 150
        ];
        $this->load->library('image_lib', $config_manip);
        if (!$this->image_lib->resize()) {
            echo $this->image_lib->display_errors();
        }
        $this->image_lib->clear();
    }

    public function upload_docs_image()
    {
        $data = [];
        $file_element_name = 'file';
        if ($_REQUEST['elementType'] == 'course_img') {

            $config['upload_path'] = './uploads/media_images/';
            $config['allowed_types'] = 'gif|jpg|png';

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload($file_element_name)) {

                $status = 'error';
                $data['error'] = $this->upload->display_errors('', '');
            } else {
                $fileData = $this->upload->data();
                $this->resizeImage($fileData['file_name']);
                $data['filePath'] = base_url() . 'uploads/media_images/thumbnails/';
                $data['fileName'] = $fileData['file_name'];
                $data['elementType'] = $_REQUEST['elementType'];
            }
        } else if ($_REQUEST['elementType'] == 'course_doc') {
            $config['upload_path'] = './uploads/course_docs/';
            $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|ppt|pptx';
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload($file_element_name)) {
                $status = 'error';
                $data['error'] = $this->upload->display_errors('', '');
            } else {
                $fileData = $this->upload->data();
                $data['fileName'] = $fileData['file_name'];
                $data['element'] = $_REQUEST['elementId'];
                $data['fileTitle'] = $_REQUEST['fileTitle'];
            }
        }
        echo json_encode($data);
        exit;
    }

    //save media_sources page
    public function save($id = 0)
    {
        if ($id > 0) :
            if ($id && !$this->edit_permission && !$this->ion_auth->is_admin()) :
                $this->session->set_flashdata('error', $this->lang->line('access_denied'));
                redirect($_SERVER['HTTP_REFERER'], 'refresh');
            endif;
        else :
            if (!$this->add_permission && !$this->ion_auth->is_admin()) :
                $this->session->set_flashdata('error', $this->lang->line('access_denied'));
                redirect($_SERVER['HTTP_REFERER'], 'refresh');
            endif;
        endif;

        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Media', 'link' => base_url('mediasources'), 'class' => ''];

        if ($id > 0) {
            $data['data'] = $this->MediaSources_model->edit($id);
            $data['id'] = $id;
            $data['mediaData'] = $this->common_model->getDataById('media_sources', '*', ['id' => $id]);
            $data['breadcrumb'][]           = ['title' => 'Edit', 'link' => '', 'class' => 'active'];
        } else {
            $data['breadcrumb'][]           = ['title' => 'Add', 'link' => '', 'class' => 'active'];
        }
        $data['page'] = 'media_sources/MediaSources/save';
        $this->template->template_view($data);
    }

    public function detail($id = 0)
    {
        $data['breadcrumb'][]           = ['title' => ucfirst(currentGroup()->name), 'link' => base_url(), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'Media', 'link' => base_url('media_sources/MediaSources'), 'class' => ''];
        $data['breadcrumb'][]           = ['title' => 'View', 'link' => '', 'class' => 'active'];


        if ($id > 0) {
            $data['data'] = $this->MediaSources_model->edit($id);
            $data['certificate_data'] = $this->MediaSources_model->edit($id);
            $data['id'] = $id;
        } else {
            redirect('users/auth', 'refresh');
        }
        $data['page'] = 'media_sources/MediaSources/detail';
        $this->template->template_view($data);
    }

    /**
     * [Add New Poll]
     */
    public function add()
    {


        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        if ($this->form_validation->run() == false) {
            $msg = 'Fill form correctly.';
            $this->session->set_flashdata('error', $msg);
        } else {
            $data = array(
                'created_by'    => $this->session->userdata('user_id'),
                'title'         => post('title'),
                'caption'         => post('caption'),
                'alternative_text'         => post('alternative_text'),
                'description'         => post('description'),
                'file_link'     => post('file_link'),
                'author'        => post('author'),
                'publisher'     => post('publisher'),
                'license_url'   => post('license_url'),
                'license_and_citation'   => post('license_and_citation'),
                'status'        => 1,
                'delete_status' => 1,
                'created'       => date("Y-m-d H:i:s"),
                'item_title'       => post('item_title'),
                'title_url'       => post('title_url'),
                'author'       => post('author'),
                'publisher'       => post('publisher'),
                'license'       => post('license'),
            );
            if (post('media_img')) {
                $data['file_name'] = post('media_img');
            }

            if (post(id) != '') {
                $id = post(id);
                $where = array(
                    "id" => $id
                );
                $this->common_model->UpdateDB('media_sources', $where, $data); //update data
            } else {
                $id = $this->MediaSources_model->InsertData('media_sources', $data); //insert data
            }
        }
        echo json_encode(array("msg" => "Media saved successfully", "id" => $id));
        exit;
    }

    //delete MediaSources
    public function delete()
    {
        $this->session->set_flashdata('success', $this->ion_auth->messages());
        if ($this->input->post('status') == '') {
            $status = 0;
        } else {
            $status = 1;
        }
        $additional_data = array(
            'delete_status' => $status,
        );
        $this->MediaSources_model->update($this->uri->segment(4), $additional_data);

        $msg = "deleted";
        echo json_encode(array("msg" => $msg)); exit;
    }

    //update media sources status
    public function update_status($id, $action)
    {
        $status = ($action == 'activate') ? 1 : 0;
        $data = array('status' => $status);
        $this->MediaSources_model->update_status($id, $data);
        $msg = "Updated";
        echo json_encode(['msg' => $msg]);
        exit;
    }

    function getLists()
    {
        $result = $this->MediaSources_model->getRows($_POST);
        $output = array(
            "draw"              => $_POST['draw'],
            "recordsTotal"      => $result['total'],
            "recordsFiltered"   => $result['total'],
            "data"              => $result['result'],
        );

        echo json_encode($output);
        exit;
    }

    public function upload_docs()
    {
        $data = [];
        $config['upload_path'] = './uploads/media_images/';
        $config['allowed_types'] = ALLOWED_FILES_EXT;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file_name')) {
            $data['error'] = $this->upload->display_errors('', '');
        } else {
            $fileData = $this->upload->data();

            $fileTypeArr = explode('/', $fileData['file_type']);

            $type = $fileTypeArr[0];
            if ($type == 'image') {
                $this->resizeImage($fileData['file_name']);
                $data['filePath'] = base_url() . 'uploads/media_images/thumbnails/';
                $data['fileName'] = $fileData['file_name'];
            } else {
                $data['filePath'] = base_url() . 'uploads/media_images/';
                $data['fileName'] = $fileData['file_name'];
            }
        }
        echo json_encode($data);
        exit;
    }


    public function upload_image()
    {
        $data = [];
        $file_element_name = 'upload';
        $config['upload_path'] = './uploads/media_images/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);

        try {
            if (!$this->upload->do_upload($file_element_name)) {
                $status = 'error';
                $result['error'] = $this->upload->display_errors('', '');
                echo json_encode($result);
            } else {
                $fileData = $this->upload->data();

                $this->resizeImage($fileData['file_name']);
                $data['filePath'] = base_url() . 'uploads/media_images/thumbnails/';
                $data['fileName'] = $fileData['file_name'];
                $img_data = ['file_name' => $fileData['file_name'], 'title' => $fileData['file_name'],  'created_by' => $this->session->userdata('user_id')];

                $this->common_model->InsertData('media_sources', $img_data);

                $result['url']      = base_url() . 'uploads/media_images/' . $data['fileName'];
                $result['fileName'] = $data['fileName'];
                $result['uploaded'] = 1;
                echo json_encode($result);
            }
        } catch (Exception $e) {
            echo json_encode(['error' => 'Unable to upoad image!']);
        }
        exit;
    }
}
