<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
Author Akash Hedaoo
Date 16/04/2019
 */

class Course_assign_model extends CI_Model
{

    function __construct()
    {
        // Set table name
        $this->table = 'container_courses';
        // Set orderable column fields
        $this->column_order = array(null, 'cat_name', 'template', 'status');
        // Set searchable column fields
        $this->column_search = array('template');
        // Set default order
        $this->order = array('container_courses.created' => 'desc');
    }
    /*
     * Fetch data from the database
     * @param $_POST filter data based on the posted parameters
     */
    public function Course_assign_list()
    {
        $this->db->where('delete_status', 1);
        $query = $this->db->get('gradebooks');
        return $query->result();
    }

    /*
     * Fetch container data from the database
     * @param $_POST filter data based on the posted parameters
     */
    public function get_container_list()
    {
        $array = array('status' => 0);
        $this->db->select('id,name,type');
        $this->db->where($array);
        $query = $this->db->get('containers');
        return $query->result_array();
    }

    /*
     * Fetch course data from the database
     * @param $_POST filter data based on the posted parameters
     */
    public function get_course_list()
    {
        $array = array('status' => 1);
        $this->db->select('id,name');
        $this->db->where($array);
        $query = $this->db->get('courses');
        return $query->result_array();
    }

    /*
     * Fetch data from the database
     * @param $id filter data based on the particular id from url
     */
    public function edit($id = null)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('gradebooks');
        return $query->row();
    }



    /*
     * Update data into the database
     * @param $id filter data based on the particular id from url
     */
    public function update($id = null, $data)
    {
        $this->db->where('id', $id);
        $update = $this->db->update('gradebooks', $data);
        if ($update) :
            return true;
        endif;
    }

    /*
     * update delete_status for active inactive skills from the database
     * @param $id filter data based on the particular id from url
     */
    public function update_status($id = null, $data)
    {
        $this->db->where('id', $id);

        $delete = $this->db->update('gradebooks', $data);
        //pr($this->db->last_query()); exit;
        if ($delete) :
            return true;
        endif;
    }

    /*
     * delete option for active inactive skills from the database
     * @param $id filter data based on the particular id from url
     */
    public function delete_option($id = null)
    {
        $this->db->where('id', $id);
        $delete = $this->db->delete('polls_options');
        //pr($this->db->last_query()); exit;
        if ($delete) :
            return true;
        endif;
    }

    /*
     * Fetch data from the database
     * @param $_POST filter data based on the posted parameters
     */
    public function getRows($postData)
    {
        $this->_get_datatables_query($postData);
        if ($postData['length'] != -1) {
            $this->db->limit($postData['length'], $postData['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }

    /*
     * Perform the SQL queries needed for an server-side processing requested
     * @param $_POST filter data based on the posted parameters
     */
    private function _get_datatables_query($postData)
    {
        $fields = 'container_courses.*,containers.name as containers_name';
        $tbl = 'container_courses';
        $jointbl1 = 'containers';
        $Joinone = 'container_courses.container_id = containers.id';
        $Where = array('containers.status' => 0);
        $jointbl3=array(
            "courses" => "container_courses.course_id = courses.id"
        );
        $this->common_model->DJoin($fields, $tbl, $jointbl1, $Joinone, "", "", $Where);
        $i = 0;
        // loop searchable columns 
        foreach ($this->column_search as $item) {
            // if datatable send POST for search
            if ($postData['search']['value']) {
                // first loop
                if ($i === 0) {
                    // open bracket
                    $this->db->like($item, $postData['search']['value']);
                } else {
                    $this->db->or_like($item, $postData['search']['value']);
                }
            }
            $i++;
        }
        if (isset($postData['order'])) {
            $this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    //Count gradebook
    public function count_gradebook()
    {
        $this->db->select('*');
        $this->db->from('gradebooks');
        return $this->db->count_all_results();
    }

    /*
     * Count records based on the filter params
     * @param $_POST filter data based on the posted parameters
     */
    public function countFiltered($postData)
    {
        $this->_get_datatables_query($postData);
        $query = $this->db->get();
        return $query->num_rows();
    }
}

/* End of file Users_modal.php */
