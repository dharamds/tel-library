
<div class="container-fluid pr-0">
   <div class="panel panel-default panel-grid">
	   <div class="panel-heading">
      		<div class="col-md-12 row">
			  <?php echo form_open('gradings', [ 
					'name'      => 'gradingForm',
					'id'        => 'gradingForm']);
				?> 
				<input type="hidden" id="search_export_hidden" name="search_export_hidden" value="0">

				<div class="col-md-3 pl-0">					
					<select class="form-control" required name="containerId" id="containerId">
						<?php 
						//pr($containers,"kl");
						if(isset($containers) ) { ?> 
						<option value="">-- Select Institute --</option>
						<?php foreach($containers as $container) { ?>
							<option value="<?php echo $container->id;?>" <?=$containerSelected?>><?php echo $container->name;?></option>
						<?php } ?>
					<?php } else { echo '<option value="">-- Select Institute --</option>';} ?>
					</select>
				</div>
				<div class="col-md-3 pl-0">					
					<select class="form-control" required name="courseId" id="courseId">
						<?php if(isset($courses)) { ?> 
						<option value="">-- Select course --</option>
						<?php foreach($courses as $course) { ?>
														
							<option value="<?php echo $course['id'];?>" <?=$courseSelected;?> ><?php echo $course['name'];?></option>
						<?php } ?>
					<?php } else{ echo '<option value="">-- Select course --</option>';} ?>
					</select>
				</div>
				<div class="col-md-3 pl-0">
					<select class="form-control"  name="sectionId" id="sectionId" required>	
					<?php if(isset($section_options)) { echo $section_options; } ?>					
					</select>
				</div>
				
				<div class="col-md-1 p-0">
					<button class="col-md-1 btn btn-primary btn-block" id="search" type="submit">Search</button>
				</div>
				<div class="col-md-1">  </div>
				<div class="col-md-1 p-0">
					<button class="col-md-1 btn btn-primary btn-block" id="export_data" name="export_data" value="export" type="submit">Export</button>
				</div>
				<?php echo form_close(); ?>
			</div>
      	</div>
      	<div class="panel-body" id="ajax_loads">
		  
  		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function () {
	$('#containerId').change(function(){
		$.ajax({               
		    url: '<?php echo bs('gradebook/getCourseList'); ?>',
		    type: "POST",
		    data: {'container_id' : $('#containerId :selected').val()},
		    success: function (html) {	
				$("#courseId").html(html);
		    } 
		});
	});	

	$('#courseId').change(function(){
		$.ajax({               
		    url: '<?php echo bs('gradebook/getSectionList'); ?>',
		    type: "POST",
		    data: {'course_id' : $('#courseId :selected').val(),"container_id" :$('#containerId :selected').val()},
		    success: function (html) {		    	
		        $("#sectionId").html(html);
		    } 
		});
	});	

		$('#export_data').on('click',function(){
			$('#search_export_hidden').val(1);
		});
		$('#search').on('click',function(){
			$('#search_export_hidden').val(0);
		});

	     $("#gradingForm").validate({
	     	ignore: [],
	         rules: {
              	courseId: { 
             		required: true
                 },
              	sectionId: { 
              		required: true
              	},
	         },
	         messages: {
	            courseId: {required: "Please select course"},
	            sectionId: {required: "Please select section"},
	         },
	         submitHandler: function (form) {
				if($('#search_export_hidden').val() == 1){
					return true;	
				}else{
					$("#ajax_loads").html('<div class="page-loading-box"><span class="page-loader-quart"></span> Loading...</div>');
					$.ajax({               
						url: '<?php echo bs('gradings'); ?>',
						type: "POST",
						data: $('form#gradingForm').serialize(),
						success: function (html) {		    	
							$("#ajax_loads").html(html);
						} 
				});
				return false;	
				}				 
             },
             errorPlacement: function(error, $elem) {
                 console.log(error);
                 if ($elem.is('textarea')) {
                     $elem.insertAfter($elem.next('div'));
                 }
                 error.insertAfter($elem);
             },

	     });

	});

</script>
