

            <div class="container-fluid">
                <div class="panel panel-info" data-widget='{"draggable": "false"}'>
                    <div class="panel-heading">
                        <h2><i class="fa fa-user"></i>Create Gradebook</h2>
                        <div class="panel-ctrls" data-actions-container="" data-action-collapse='{"target": ".panel-body"}'></div>
                    </div>
                    <div class="panel-body">
                        <?= form_open('Gradebook/Gradebook/add', array('id' => 'gradebook_create_form_validation', 'class' => 'form-horizontal')); ?>
                        <div class="form-group col-md-12">
                            
                            <div class="col-md-6">
                                <label for="fieldname" class="col-md-12 control-label">Grading's Category</label>
                                <?php
                                $extra = array(
                                    'name' => 'category_id',
                                    'id' => 'category_id',
                                    'value' => set_value('category_id'),
                                    'class' => 'form-control'
                                );
                                echo form_dropdown('category_id', $category_list, 0, $extra);
                                ?>
<?php echo form_error('category_id', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="col-md-6">
                                <label for="fieldname" class="col-md-12 control-label">Grading's Name</label>
                                <?php
                                echo form_input([
                                    'name' => 'name',
                                    'id' => 'name',
                                    'class' => 'form-control',
                                ]);
                                ?>
<?php echo form_error('template', '<div class="error">', '</div>'); ?>
                            </div>

                        </div>
                        <div class="form-group col-md-12">
                            <div class="col-md-6">
                                <label for="fieldname" class="col-md-12 control-label">Grading's Template</label>
                                <?php
                                echo form_textarea([
                                    'name' => 'template',
                                    'id' => 'template',
                                    'class' => 'form-control editor',
                                    'rows' => '5'
                                ]);
                                ?>
                                <?php echo form_error('template', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="col-md-6">
                                <label for="fieldname" class="col-md-12 control-label">Grading's Description</label>
                                <?php
                                echo form_textarea([
                                    'name' => 'description',
                                    'id' => 'description',
                                    'class' => 'form-control editor',
                                    'rows' => '5'
                                ]);
                                ?>
<?php echo form_error('description', '<div class="error">', '</div>'); ?>
                            </div>


                        </div>


                        <div class="form-group">

                            <div class="col-md-6">
                                <input type="submit" class="finish btn-success btn" value="Save">
                            </div>
                            </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
<script>
    $(document).ready(function () {
        
         jQuery.validator.addMethod("noSpace", function (value, element, param) {
            return value.match(/^(?=.*\S).+$/);
        }, "No space please and don't leave it empty");

        CKEDITOR.replace('description', {
            toolbar: 'short',
        });

        CKEDITOR.replace('template', {
            toolbar: 'short',
        });

        $('#gradebook_create_form_validation').validate({// initialize the plugin
            ignore: [],
            rules: {
                category_id: {
                    required: true,
                },
                name: {
                    required: true,
                    noSpace:true
                },
                template: {
                    required: function (textarea) {
                        CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                        return editorcontent.length === 0;
                    }
                },
                description: {
                    required: function (textarea) {
                        CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                        return editorcontent.length === 0;
                    }
                }
            },
            messages: {
                category_id: {
                    required: "Please select category"
                },
                name: {
                    required: "Please enter name"
                },
                template: {
                    required: "Please enter template"
                },
                description: {
                    required: "Please enter description"
                }
            },
            submitHandler: function (form) { // for demo
                return true; // for demo
            }
        });
    });
</script>