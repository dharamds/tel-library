<!-- DataTables CSS library -->
<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            <ol class="breadcrumb">
                <li class=""><a href="">Home</a></li>
                <li class=""><a href="">Gradebooks</a></li>
                <li class="active"><a href="">View Gradebooks</a></li>
            </ol>
            <div class="container-fluid">
                
                <br>
                <div data-widget-group="group1">
                    <div class="row">
                    <div class="col-md-12">
                            <a class="btn btn-primary" href="<?php echo base_url('gradebook/gradebook/save') ?>">Add New Gradebook</a> 
                        </div>  
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h2>Gradebook's Records</h2>
                                    <div class="panel-ctrls"></div>
                                    <a href="<?php echo bs('users/print_with_dompdf') ?>">
                                        <i class="fa fa-print" style="padding-left: 1%;color: black"></i>
                                    </a>	
                                </div>
                                <div class="panel-body no-padding">


                                    <table id="memListTable" class="table table-bordered table-striped table-hover" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Sr. No</th>
                                                <th>Grader's Category</th>
                                                <th>Grader's Template</th>
                                                <?php  if ($this->session->userdata("role_id") == 1): ?>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                <?php endif ?>
                                            </tr>
                                        </thead>
                                        
                                    </table>
                                </div>
                                <div class="panel-footer"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- .container-fluid -->
        </div>
        <!-- #page-content -->
    </div>
    <script type='text/javascript' src="<?php echo base_url(); ?>public\assets\js\Gradebook\Course_assign_view.js"></script>
<?php
$jsVars = [
    'ajax_call_root' => base_url()
];    
?>
<script>
    jQuery(document).ready(function() { 
        Course_assign_view.init(<?php echo json_encode($jsVars); ?>);
    });
</script>