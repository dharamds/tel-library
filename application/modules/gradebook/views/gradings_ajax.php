<?php if($msg): ?>
<div><?php echo $msg; ?></div>
<?php else: ?>
<table id="gradingListTable" class="table table-bordered table-striped" cellspacing="0"  cellpadding="0">
		<thead>
			<tr>
				<th style="min-width: 100px;"></th>
				<th style="min-width: 100px;"></th>
				<th style="min-width: 100px;"></th>
				<th style="min-width: 100px;"></th>
				<!-- Displaying dyanamic columns of gradebook -->
				<?php if(isset($gradebookTableHeaderData)) { ?>
					<?php foreach($gradebookTableHeaderData as $headerData) { ?>
						<th class="text-center"> 
							<?=$headerData->section_title;?> (<?=$headerData->weight;?>%)
						</th>
					<?php }?>
				<?php } ?>                           
			</tr>
		</thead>
		<tbody>  
			<tr>
				<th style="min-width: 100px;">Student name</th>
				<th style="min-width: 100px;">ID Number</th>                    
				<th style="min-width: 100px;">Final grade</th>
				<th style="min-width: 100px;">Current grade</th>
				<?php if(isset($gradebookTableHeaderData)) { ?>
					<?php foreach($gradebookTableHeaderData as $headerData) { ?>
						<td class="p-0"> 
							<!-- Internal table header column -->                    			
							<?php 
								$type = $headerData->section_type; 
								# CYK
								if(isset($gradebooks[$type]) && $type==1) {
									$int_table = '<table width="100%" cellpadding="4" cellspacing="2" class="table table-bordered table-striped f12 m-0"><tr>';
									$i=1;
									foreach ($gradebooks[$type] as $key => $cykQuizData) {
										$int_table .= '<td align="center" style="min-width:60px;">CYK '.$i. $cykQuizData['id'].'</td>';
										$i++;
									}
									$int_table .= '<td><b>AVG</b></td></tr></table>';
									echo $int_table;
								}else if(isset($gradebooks[$type]) && $type==2) {
									$int_table = '<table width="100%" cellpadding="4" cellspacing="2"  class="table table-bordered table-striped f12 m-0"><tr>';
									$i=1;
									foreach ($gradebooks[$type] as $key => $moduleQuizData) {
										$int_table .= '<td align="center" style="min-width:60px;">M '.$i.'</td>';
										$i++;
									}
									$int_table .= '<td><b>AVG</b></td></tr></table>';
									echo $int_table;
								}else if(isset($gradebooks[$type]) && $type==3) {
									$int_table = '<table width="100%" cellpadding="4" cellspacing="2"  class="table table-bordered table-striped f12 m-0"><tr>';
									$i=1;
									foreach ($gradebooks[$type] as $key => $evidenceQuizData) {
										$int_table .= '<td align="center" style="min-width:60px;">E '.$i.'</td>';
										$i++;
									}
									$int_table .= '<td><b>AVG</b></td></tr></table>';
									echo $int_table;
								}else if(isset($gradebooks[$type]) && $type==4) {
									$int_table = '<table width="100%" cellpadding="4" cellspacing="2"  class="table table-bordered table-striped f12 m-0"><tr>';
									$i=1;
									foreach ($gradebooks[$type] as $key => $midQuizData) {
										$int_table .= '<td align="center" style="min-width:60px;">Mid-term '.$i.'</td>';
										$i++;
									}
									$int_table .= '<td><b>AVG</b></td></tr></table>';
									echo $int_table;
								}else if(isset($gradebooks[$type]) && $type==5) {
									$int_table = '<table width="100%" cellpadding="4" cellspacing="2"  class="table table-bordered table-striped f12 m-0"><tr>';
									$i=1;
									foreach ($gradebooks[$type] as $key => $finalQuizData) {
										$int_table .= '<td align="center" style="min-width:60px;">Final '.$i.'</td>';
										$i++;
									}
									$int_table .= '<td><b>AVG</b></td></tr></table>';
									echo $int_table;
								}
							?>
						</td>
					<?php }?>
				<?php } ?>
			</tr>
		
			
		<?php if(isset($studentList)) {  ?>       	
			<?php foreach($studentList as $student) { 
				$sid = $student['id'];
				?>
				<tr>
					<td><?php echo $student['student']?></td>
					<td><?php echo $student['unique_id']?></td>
					<td>
						<?php 
							$final_grade = array_sum($studentSectionAvgs[$sid]['final_grade']);
							echo round($final_grade);
						?>	            			
					</td>
					<td><?php 
							$current_grade = array_sum($studentSectionAvgs[$sid]['current_grade']);
							echo round($current_grade);
						?>	</td>
					<td class="p-0">
						<!-- Internal table for values CYK -->          
						<?php
						if(isset($student[1]) ) {
							$int_table = '<table width="100%" cellpadding="4" cellspacing="2" class="table table-bordered table-striped f12 m-0"><tr>';
							$i=1;
							foreach ($student[1] as $key => $cykQuizData) {									
								$cyk_value = isset($cykQuizData['grade']) ? '<i class="flaticon-checked"></i>' : '--';
								$int_table .= '<td align="center" style="min-width:60px;"> '.$cyk_value.' </td>';
								$i++;									
							}
							$section_weight = $gradebookTableHeaderData[0];
							if(count($studentSectionAvgs[$sid][1]['current']) > 0) {
								$cyk_section_avg = array_sum($studentSectionAvgs[$sid][1]['current']) / count($studentSectionAvgs[$sid][1]['current']);
								$cyk_section_avg = ($cyk_section_avg * $section_weight->weight) / 100;
							}
							else $cyk_section_avg = "0.0"; 
							$int_table .= '<td>'.number_format($cyk_section_avg, 1, '.', '').'</td></tr></table>';
							echo $int_table; 
						}
						?>
					</td>
					<td class="p-0">
						<!-- Internal table for values MODULE -->  
						<?php
						if(isset($student[2]) ) {
							$int_table = '<table width="100%" cellpadding="4" cellspacing="2" class="table table-bordered table-striped f12 m-0"><tr>';
							$i=1;
							foreach ($student[2] as $key => $moduleQuizData) {
								$module_value = isset($moduleQuizData['grade']) ? $moduleQuizData['grade'] : '--';
								$int_table .= '<td align="center" style="min-width:60px;"> '.$module_value.' </td>';
								$i++;
							}

							$section_weight = $gradebookTableHeaderData[1];
							if(count($studentSectionAvgs[$sid][2]['current']) > 0) {
								$module_section_avg = array_sum($studentSectionAvgs[$sid][2]['current']) / count($studentSectionAvgs[$sid][2]['current']);
								$module_section_avg = ($module_section_avg * $section_weight->weight) / 100;
							}
							else $module_section_avg = "0.0"; 
							$int_table .= '<td>'.number_format($module_section_avg,1, '.', '').'</td></tr></table>';
							echo $int_table;
						}
						?>
					</td>
					<td class="p-0">
						<!-- Internal table for values EVIDENCE -->  
						<?php
						if(isset($student[3]) ) {
							$int_table = '<table width="100%" cellpadding="4" cellspacing="2" class="table table-bordered table-striped f12 m-0"><tr>';
							$i=1;
							foreach ($student[3] as $key => $evidenceQuizData) {
								$evidence_value = isset($evidenceQuizData['grade']) ? $evidenceQuizData['grade'] : '--';
								$int_table .= '<td align="center" style="min-width:60px;"> '.$evidence_value.' </td>';
								$i++;
							}

							$section_weight = $gradebookTableHeaderData[2];
							if(count($studentSectionAvgs[$sid][3]['current']) > 0) {
								$evidence_section_avg = array_sum($studentSectionAvgs[$sid][3]['current']) / count($studentSectionAvgs[$sid][3]['current']);
								$evidence_section_avg = ($evidence_section_avg * $section_weight->weight) / 100;
							}
							else $evidence_section_avg = "0.0"; 
							$int_table .= '<td>'.number_format($evidence_section_avg,1, '.', '').'</td></tr></table>';
							echo $int_table;
						}
						?>
					</td>
					<td class="p-0">
						<!-- Internal table for values MID TERM -->  
						<?php
						if(isset($student[4]) ) {
							$int_table = '<table width="100%" cellpadding="4" cellspacing="2" class="table table-bordered table-striped f12 m-0"><tr>';
							$i=1;
							foreach ($student[4] as $key => $midQuizData) {
								$mid_term_value = isset($midQuizData['grade']) ? $midQuizData['grade'] : '--';
								$int_table .= '<td align="center" style="min-width:60px;"> '.$mid_term_value.' </td>';
								$i++;
							}
							$section_weight = $gradebookTableHeaderData[3];
							if(count($studentSectionAvgs[$sid][4]['current']) > 0) {
								$midterm_section_avg = array_sum($studentSectionAvgs[$sid][4]['current']) / count($studentSectionAvgs[$sid][4]['current']);
								$midterm_section_avg = ($midterm_section_avg * $section_weight->weight) / 100;
							}
							else $midterm_section_avg = "0.0"; 
							$int_table .= '<td>'.number_format($midterm_section_avg,1, '.', '').'</td></tr></table>';
							echo $int_table;
						}
						?>
					</td>
					<td class="p-0">
						<!-- Internal table for values FINAL -->  
						<?php
						if(isset($student[5]) ) {
							$int_table = '<table width="100%" cellpadding="4" cellspacing="2" class="table table-bordered table-striped f12 m-0"><tr>';
							$i=1;
							foreach ($student[5] as $key => $finalQuizData) {
								$final_value = isset($finalQuizData['grade']) ? $finalQuizData['grade'] : '--';
								$int_table .= '<td align="center" style="min-width:60px;"> '.$final_value.' </td>';
								$i++;
							}
							$section_weight = $gradebookTableHeaderData[4];
							if(count($studentSectionAvgs[$sid][5]['current']) > 0) {
								$final_section_avg = array_sum($studentSectionAvgs[$sid][5]['current']) / count($studentSectionAvgs[$sid][5]['current']);
								$final_section_avg = ($final_section_avg * $section_weight->weight) / 100;
							}
							else $final_section_avg = "0.0"; 
							$int_table .= '<td>'.number_format($final_section_avg,1, '.', '').'</td></tr></table>';
							echo $int_table;
						}
						?>
					</td>
				</tr>
			<?php } ?>
		<?php } ?>
	</tbody>
</table>
<?php endif; ?>