<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Name:  DOMPDF
* 
* Author: Jd Fiscus
* 	 	  jdfiscus@gmail.com
*         @iamfiscus
*          
*
* Origin API Class: http://code.google.com/p/dompdf/
* 
* Location: http://github.com/iamfiscus/Codeigniter-DOMPDF/
*          
* Created:  06.22.2010 
* 
* Description:  This is a Codeigniter library which allows you to convert HTML to PDF with the DOMPDF library
* 
*/

class Dompdf_gen {
		
	public function __construct() {
		
		/*
		require_once APPPATH.'third_party/dompdf/dompdf_config.inc.php';
		
		$pdf = new DOMPDF();
		
		$CI =& get_instance();
		$CI->dompdf = $pdf;
		*/

		require_once  APPPATH.'third_party/dompdf/lib/html5lib/Parser.php';
		require_once  APPPATH.'third_party/dompdf/lib/php-font-lib/src/FontLib/Autoloader.php';
		require_once  APPPATH.'third_party/dompdf/lib/php-svg-lib/src/autoload.php';
		require_once  APPPATH.'third_party/dompdf/src/Autoloader.php';
		Dompdf\Autoloader::register();
		
		use Dompdf\Dompdf;
		
		class Dom_pdf {
				
			public function __construct() {
				$pdf = new Dompdf();
				
				$CI =& get_instance();
				$CI->dompdf = $pdf;
				
			}
			
		}
		
	}
	
}