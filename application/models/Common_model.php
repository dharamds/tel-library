<?php

/**
 *
 */
class Common_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function InsertData($table, $Data) {
        $Insert = $this->db->insert($table, $Data);
        if ($Insert):
            return true;
        endif;
    }

    function InsertBatchData($table, $Data) {
        $Insert = $this->db->insert_batch($table, $Data);
        if ($Insert):
            return true;
        endif;
    }

    function getAllData($table, $specific = '', $row = '', $Where = '', $order = '', $limit = '', $groupBy = '', $like = '') {
        // If Condition
        if (!empty($Where)):
            $this->db->where($Where);
        endif;
        // If Specific Columns are require
        if (!empty($specific)):
            $this->db->select($specific);
        else:
            $this->db->select('*');
        endif;

        if (!empty($groupBy)):
            $this->db->group_by($groupBy);
        endif;
        // if Order
        if (!empty($order)):
            $this->db->order_by($order);
        endif;
        // if limit
        if (!empty($limit)):
            $this->db->limit($limit);
        endif;

        //if like
        if (!empty($like)):
            $this->db->like($like);
        endif;
        // get Data
        //if select row
        if (!empty($row)):
            $GetData = $this->db->get($table);
            return $GetData->row();
        else:
            $GetData = $this->db->get($table);
            return $GetData->result();
        endif;
    }

    function getDataById($table, $fields, $conditons) { 
        $this->db->select($fields);
        $this->db->from($table);
        $this->db->where($conditons);
        $query = $this->db->get();
        return $query->row();
    }

    function UpdateDB($table, $Where, $Data) {
        $this->db->where($Where);
        $Update = $this->db->update($table, $Data);
        if ($Update):
            return true;
        else:
            return false;
        endif;
    }

    function Authentication($table, $data) {
        $this->db->where($data);
        $query = $this->db->get($table);
        if ($query) {
            return $query->row();
        } else {
            return false;
        }
    }

    function DJoin($field, $tbl, $jointbl1, $Joinone, $row = '', $jointbl3 = '', $Where = '', $order = '', $groupy = '', $limit = '', $query = '') {
        $this->db->select($field);
        $this->db->from($tbl);
        $this->db->join($jointbl1, $Joinone);
        
        if (!empty($jointbl3)):
            foreach ($jointbl3 as $Table => $On):
                $this->db->join($Table, $On);
            endforeach;
        endif;
       
        // if Group
        if (!empty($groupy)):
            $this->db->group_by($groupy);
        endif;
        if (!empty($order)):
            $this->db->order_by($order);
        endif;
        if (!empty($Where)):
            $this->db->where($Where);
        endif;
        if (!empty($limit)):
            $this->db->limit($limit);
        endif;    

        if (!empty($row)):
            $query = $this->db->get();
            return $query->row();
        else:
            $query = $this->db->get();
            return $query->result();
        endif;
    }

    function DeleteDB($table, $where) {
        $this->db->where($where);
        $done = $this->db->delete($table);
        if ($done) {
            return true;
        } else {
            return false;
        }
    }

    function getDateMdYToYmd($date_mdy) {
        $mdy_date = explode("/", $date_mdy);
        $ymd_date = $mdy_date['2'] . "-" . $mdy_date['0'] . "-" . $mdy_date['1'];
        return $ymd_date;
    }

    function Encode_html($str) {
        return trim(stripslashes(htmlentities($str)));
    }

    function Encode($str) {
        return trim(htmlentities($str, ENT_QUOTES));
    }

    function Decode($str) {
        return html_entity_decode(stripslashes($str));
    }

    function Encrypt($password) {
        return crypt(md5($password), md5($password));
    }

    public function fetch_posts($limit, $start) {
        $this->db->limit($limit, $start);
        $this->db->order_by("id", "desc");
        $this->db->where('deleted_at ', "Null");
        $this->db->where('status ', 1);
        $query = $this->db->get("blog_post");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    /* get metadata types */

    public function fetch_metadata_types($tbl) {
        $data = $this->getAllData($tbl);
        $selectMetadata = ['' => 'Select'];

        foreach ($data as $key => $value) {
            $selectMetadata[$value->id] = $value->name;
        }
        return $selectMetadata;
    }

    public function fetch_parent_metadata_types_categories($tbl) {
        $data = $this->getAllData($tbl, ['id', 'name'], '', [ 'parent' => 0 , 'status' => 1, 'delete_status' => 1 ], 'name asc');
        $selectMetadata = ['' => 'Select'];

        foreach ($data as $key => $value) {
            $selectMetadata[$value->id] = $value->name;
        }
        return $selectMetadata;
    }

    public function selectCategoryMetadata($tbl, $type) {
        $data = $this->getAllData($tbl, ['id', 'name'], '', ['type' => $type, 'delete_status' => 1], 'name asc');
        $categoryMetadata = ['' => 'Select'];
        foreach ($data as $key => $value) {
            $categoryMetadata[$value->id] = $value->name;
        }
        return $categoryMetadata;
    }

    public function getDataForDropdown($tbl, $fields, $orderField = NULL) {
        $data = $this->getAllData($tbl, $fields, '', ['status' => 1], $orderField . ' asc');
        $selectData = ['' => 'Select'];
        foreach ($data as $key => $value) {
            $selectData[$value->id] = $value->name;
        }
        return $selectData;
    }

    function InsertDataWithLastID($table, $Data) {
        $Insert = $this->db->insert($table, $Data);
        if ($Insert):
            return $this->db->insert_id();
        endif;
    }


    public function getCountProgressBar($userID,$courseId){
        $all_data = $this->getAllData('module_contain', 'course_id,module_id', '', array("course_id" =>$courseId), '', '', 'module_id');

        if(!$all_data){
            return 0;
        }
        $collectModuleID = [];
        foreach ($all_data as $check_data) {
            $collectModuleID[] =$check_data->module_id;
        }    
        $conditions = [
                "user_id" => $userID, 
                "course_id" => $courseId, 
                "lesson_id" => 0, 
                "contain_id" => 0, 
                "contain_type" => 0
        ];
       // echo sizeof($all_data);
        $where_in_conditions = $collectModuleID;
        $comp_count = $this->__get_count_activity_by_user($conditions, $where_in_conditions);
        return round(($comp_count / sizeof($all_data)) * 100); 

    }

    private function __get_count_activity_by_user($conditions = [], $where_in_conditions = []){
        
        $this->db->select('module_id');
        $this->db->from('course_activities');
        $this->db->where($conditions);
        if( $where_in_conditions){
            $this->db->where_in('course_activities.module_id', $where_in_conditions);
        }
        return $this->db->get()->num_rows();
    }
    
    public function getTags($reference_id, $type , $subtype )
    {
        $sql = "SELECT COALESCE(GROUP_CONCAT(DISTINCT tags.name ORDER BY tags.name ASC SEPARATOR '<br>'), 'NA')  as name from tag_assigned 
        JOIN tags ON tags.id = tag_assigned.tag_id
        WHERE tag_assigned.reference_id = ".$reference_id." and tag_assigned.reference_type = ".$subtype." and tag_assigned.reference_sub_type = ".$type." group by tag_assigned.reference_type";
      
      //return $sql;
      $query =  $this->db->query($sql);
       return ($query->row()->name) ? $query->row()->name : 'NA';

    }
    
    public function getCategories($reference_id, $type , $subtype){
        $sql = "SELECT COALESCE(GROUP_CONCAT(DISTINCT categories.name ORDER BY categories.name ASC SEPARATOR '<br>'), 'NA')  as name from category_assigned 
        JOIN categories ON categories.id = category_assigned.category_id
        WHERE category_assigned.reference_id = ".$reference_id." and category_assigned.reference_type = ".$subtype." and category_assigned.reference_sub_type = ".$type." group by category_assigned.reference_type";
        $query =  $this->db->query($sql);
       return ($query->row()->name) ? $query->row()->name : 'NA';
    }
}

?>
