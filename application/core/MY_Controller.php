<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends MX_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("common_model");
		//Do your magic here
		
	}

	public function my_delete($id,$table) {
		$data = array(
			'delete_status' => 0
		 );

		$result = $this->common_model->UpdateDB($table,array('id' => $id),$data);

		if ($result) 
		{
		set_flashdata('success',ucfirst($table). ' has been deleted successfully');
		redirect($_SERVER['HTTP_REFERER']);
		} 
	}

	public function change_status($id,$status,$table)
	{ 	
		$data = array(
						'status' => $status
					 );
		$result = $this->common_model->UpdateDB($table,array('id' => $id),$data);

		if ($result) 
		{
			set_flashdata('success',ucfirst($table). ' status has been changed successfully');
			redirect($_SERVER['HTTP_REFERER']);
		} 
	}

	public function get_permissions()
	{
		$user_id = $this->ion_auth->user()->row()->id;
		list($method) = explode('/', uri_string());
		if($user_id> 0){
		$this->db->select('*');
		$this->db->from('users_roles');
		$this->db->where(array('users_roles.user_id' => $user_id));
		$query 		= $this->db->get();	
		$role_ids 	= array_column($query->result(), 'role_id');
		//pr($method); exit;
		$this->db->select('*');
		$this->db->from('menus_permissions');
		$this->db->join('menus', 'menus.id = menus_permissions.menu_id', 'inner');	
		$this->db->where(['menus.slug' => $method]);
		//$this->db->like('menus.slug', $method);
		$this->db->group_start();
		$this->db->where_in('menus_permissions.role_id', $role_ids);
		$this->db->or_where('menus_permissions.user_id', $user_id);
		$this->db->group_end();
		//pr($this->db->get()->row()); exit;

		return $this->db->get()->row();	
	}
	}


		
}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */
