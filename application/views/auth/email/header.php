<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <!--[if !mso]><!-->
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <!--<![endif]-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <title></title>
</head>
<body style="Margin:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;min-width:100%;background-color:#f3f2f0; font-family: Roboto, sans-serif;">
  <center class="wrapper" style="width:100%;table-layout:fixed;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;background-color:#f3f2f0;">
    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:#f3f2f0;" bgcolor="#2659bb;">
      <tr>
        <td width="100%"><div class="webkit" style="max-width:750px;Margin:0 auto;"> 
          <table class="outer" align="center" cellpadding="0" cellspacing="0" border="0" style="border-spacing:0;Margin:0 auto;width:100%;max-width:750px;">  
            <tr>
              <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                <table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#FFFFFF"  style=" border-left:1px solid #e8e7e5; border-right:1px solid #e8e7e5">
                  <tr>
                    <td bgcolor="#2659bb" width="600" height="90" valign="top" align="center" style=" background-size: cover; padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;text-align:center;font-size:0" 
                    class="two-column">   
                    <div>
                      <div class="column" style="width:100%;display:inline-block;vertical-align:top;">
                        <table width="100%" style="border-spacing:0">
                          <tr>
                            <td class="inner" style="padding-bottom:10px; padding-right:10px;padding-left:30px;">
                            <table class="contents1" style="border-spacing:0; width:100%">
                              <tr>
                                <td align="center" valign="middle" style="padding-right:30px"><p style="font-size:30px; text-decoration:none; color:#ffffff; font-family: Roboto, sans-serif; text-align:left; margin: 10px 0 0;">
                                  <a href="'.Router::url('/', true).'" target="_blank"><img src="<?=$email_logo;?>" style="height: 60px; width: auto;"></a></p>
                                </td>
                                <td  style="border-spacing:0; width:400px; padding: 30px 0 0; display: block;">
                                 <p style="font-size:18px; text-decoration:none; color:#ffffff; font-family: Roboto, sans-serif; text-align:left; line-height:26px">TEL LIBRARY <br />
                                 </td>
                               </tr>
                             </table>
                             </td>
                           </tr>
                         </table>
                       </div>                     
                    </div>
                  </td>
                </tr>
              </table>

              <table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#FFFFFF">
                <tr>
                  <td class="" style=""> 
                    <div class="" style="">
                      <table class="contents" style="border-spacing:0; width:100%; padding:0px;" bgcolor="#FFFFFF">
                        <thead style=" color: #484848; font-size: 15px;">
                          <tr>
                            <th align="left" style="padding: 15px 5px 15px 10px; text-align: left;font-weight:normal">