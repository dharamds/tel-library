<?php
defined('BASEPATH') or exit('No direct script access allowed');

    // Helper For print_r
    function pr($var = '', $opt = false)
    {
        echo '<pre>';
        print_r($var);
        echo '</pre>';
        if ($opt) {
            exit;
        }
    }

    //Helper For base_url()
    function bs($value = '')
    {
        echo base_url($value);
    }

    //Helper for $this->load->view()
    function view($value = '', $data = array(), $output = false)
    {
        $CI = &get_instance();
        $CI->load->view($value, $data, $output);
    }

    //Helper For thsi->input->post()
    function post($value = '')
    {
        $CI = &get_instance();

        return $CI->input->post($value);
    }

    //helper for var_dump
    function dd($value = '')
    {
        echo '<pre>';
        var_dump($value);
        echo '</pre>';
        die();
    }

    //Helper for last_query()
    function vd()
    {
        $CI = &get_instance();

        return $CI->db->last_query();
    }
    function role_priviliges($value = '')
    {
        $CI = &get_instance();

        $gp_id = $CI->session->userdata('role_id');

        $gp_result = $CI->ion_auth_model->user_gp_privilegs($gp_id);

        $gp_data = array();

        foreach ($gp_result as $value) {
            //add all data to session
            $gp_data[] = $value->perm_name;
        }

        return $gp_data;
    }
    function has($val)
    {
        if ($val) {
            return true;
        }

        return false;
    }
// create hex string strToHex
    function strToHex($string){
        $hex = '';
        for ($i=0; $i<strlen($string); $i++){
            $ord = ord($string[$i]);
            $hexCode = dechex($ord);
            $hex .= substr('0'.$hexCode, -2);
        }
        return strToUpper($hex);
    }
// create hex string hexToStr    
    function hexToStr($hex){
        $string='';
        for ($i=0; $i < strlen($hex)-1; $i+=2){
            $string .= chr(hexdec($hex[$i].$hex[$i+1]));
        }
        return $string;
    }

    /*
     * Slugify Helper
     *
     * Outputs the given string as a web safe filename
     */
    
    if (!function_exists('slugify')) {
        function slugify($string, $replace = array(), $delimiter = '-', $locale = 'en_US.UTF-8', $encoding = 'UTF-8')
        {
            if (!extension_loaded('iconv')) {
                throw new Exception('iconv module not loaded');
            }
            // Save the old locale and set the new locale
            $oldLocale = setlocale(LC_ALL, '0');
            setlocale(LC_ALL, $locale);
            $clean = iconv($encoding, 'ASCII//TRANSLIT', $string);
            if (!empty($replace)) {
                $clean = str_replace((array) $replace, ' ', $clean);
            }
            $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
            $clean = strtolower($clean);
            $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
            $clean = trim($clean, $delimiter);
            // Revert back to the old locale
            // setlocale(LC_ALL, $oldLocale);
            return $clean;
        }
    }
    
    /*
     * Email Helper
     *
     * Send email throught the system
     */
    
     if (!function_exists('system_send_email')) {
        function system_send_email($data)
        {
            $CI = &get_instance();
            $CI->load->library(['email']);

            $CI->load->database();
            $result = $CI->db->select('*')
           ->from('email_templates')
           ->where(array('temp_code' => $data['template_code'], 'status' => 1))
           ->get()->row();
           
           foreach ($data['variables'] as $key => $setVars) {
                $result->message = str_replace("{".$key."}", $setVars, $result->message);
            }

            $email_template = $result->message.$CI->load->view('/auth/email/signature', '', TRUE);
            
            $from_name = $result->from_email;
                    $CI->load->library(['email']);
                    $CI->email->clear();
                    $CI->email->set_newline("\r\n");  
                                  
					$CI->email->from($from_name, $CI->config->item('site_title', 'ion_auth'));
					$CI->email->to($data['email']);
					$CI->email->subject('TEL Library - '. $result->subject);
					$CI->email->message($email_template);
                
					if ($CI->email->send())
					{
						return TRUE;
					}
                   
            return false;
        }



    }

    /*
     * Email Helper
     *
     * Send email throught the system
     */
    
    /*Array
(
    [variables] => Array
        (
            [from_email] => info@datalogysoftware.com
            [email] => ShermanVFelton@dayrep.com
            [user_name] => Sherman V Felton
            [body] => dasdasddasdasd
        )

    [template_code] => GROUP_NOTIFICATION
)*/
    
    if (!function_exists('sendgrid_email')) {
        //pr("dddd","k");
        function sendgrid_email($data)
        {   
            
            $CI = &get_instance();
            $CI->load->library(['email']);

            if($data['template_code']) { 
                $CI->load->database();
                $result = $CI->db->select('*')
               ->from('email_templates')
               ->where(array('temp_code' => $data['template_code'], 'status' => 1))
               ->get()->row();
            }else {
                $result = $data['template_data'];
            }
            
          
            foreach ($data['variables'] as $key => $setVars) {
                $result->message = str_replace("{".$key."}", $setVars, $result->message);
            }
            $data['email_logo']=base_url('public/assets/img/tel-logo.png');
            $email_template =$CI->load->view('/auth/email/header',$data , TRUE);
            $email_template .= $result->message;
            $email_template .= $CI->load->view('/auth/email/footer', '', TRUE);
                    $CI->load->library(['email']);
                    $CI->email->initialize(array(
                        'protocol' => 'smtp',
                        'smtp_host' => SMTP_HOST,
                        'smtp_user' => SMTP_USER,
                        'smtp_pass' => SMTP_PASSWORD,
                        'smtp_port' => 587,
                        'crlf' => "\r\n",
                        'newline' => "\r\n"
                    ));
                    $CI->email->clear();
                    $CI->email->set_newline("\r\n");  
                                  
					$CI->email->from($result->from_email, $result->from_name);
                    $CI->email->to($data['email']);	
                    //$CI->email->to('vaibhav@datalogysoftware.com');
					$CI->email->subject('TEL Library - '. $result->subject);
					$CI->email->message($email_template);
                if(in_array($data['email'], ['akash@datalogysoftware.com','dharamendra@datalogysoftware.com','nupendra@datalogysoftware.com','sat@yopmail.com','gaurav@datalogysoftware.com','vaibhav@datalogysoftware.com',
                'tentagil@gmail.com', 'meezymoo9@gmail.com', 'meezymoo9@yahoo.com', 'levi.p.lloyd@gmail.com', 'aubreesdad31@hotmail.com' , 'akashhedaoo2051@gmail.com'])){
                    if ($CI->email->send())
					{
						return TRUE;
					}
                   
                }
					
            return TRUE;
        }



    }

/* End of file custom_helpers.php */
/* Location: ./application/helpers/custom_helpers.php */

 function resizeImage($sourcePath,$targetPath, $width = 100, $height = 100)
        {
           /* $source_path = $_SERVER['DOCUMENT_ROOT'] . '/uploads/' . $filename;
            $target_path = $_SERVER['DOCUMENT_ROOT'] . '/uploads/thumbnail/';*/
            $CI = &get_instance();
            $source_path = FCPATH . 'uploads/' . $sourcePath;
            $target_path = FCPATH . 'uploads/' . $targetPath;

            $config_manip = array(
                'image_library' => 'gd2',
                'source_image' => $source_path,
                'new_image' => $target_path,
                'maintain_ratio' => TRUE,
                'create_thumb' => TRUE,
                'thumb_marker' => '',
                'width' => $width,
                'height' => $height
            );
            $CI->load->library('image_lib', $config_manip);
            if (!$CI->image_lib->resize()) {
                echo $CI->image_lib->display_errors();
            }
            $CI->image_lib->clear();
        }
        function siteURL() {
            $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
            $domainName = $_SERVER['HTTP_HOST'] . '/';
            return $protocol . $domainName;
        }  
        
        function currentGroup($user_id = 0) {
            $CI = &get_instance();
            $user_id = $CI->ion_auth->user()->row()->id;
            return $CI->db->select('roles.*')
                    ->from('users_roles')
                    ->join('roles', 'roles.id = users_roles.role_id', 'right')
                    ->where(['users_roles.user_id' => $user_id])
                    ->get()->row();
        }  
        function currentUser() {
            $CI = &get_instance();
            return $CI->ion_auth->user()->row();
        }  

        function get_contener_id()
        {
            $CI = &get_instance();
            return $CI->db->where(['user_id' => $CI->ion_auth->user()->row()->id])->get('containers_users_roles')->row()->container_id;
        }

        function get_role_by_id($role_id)
        {
            $CI = &get_instance();
            return $CI->db->where(['id' => $role_id])->get('roles')->row()->name;
        }

        function get_container_id()
        {
            $CI = &get_instance();
            $data = $CI->db->where(['user_id' => $CI->ion_auth->user()->row()->id])->get('containers_users_roles')->row();
            return ($data)? $data->container_id : 0;
        }

        function groupArray($arr, $group, $preserveGroupKey = false, $preserveSubArrays = false) {

            $temp = array();
            foreach($arr as $key => $value) {
                $groupValue = $value[$group];
                if(!$preserveGroupKey)
                {
                    unset($arr[$key][$group]);
                }
                if(!array_key_exists($groupValue, $temp)) {
                    $temp[$groupValue] = array();
                }

                if(!$preserveSubArrays){
                    $data = count($arr[$key]) == 1? array_pop($arr[$key]) : $arr[$key];
                } else {
                    $data = $arr[$key];
                }
                $temp[$groupValue][] = $data;
            }
            //echo "here"; exit;
            if(!$preserveSubArrays && count($arr[$key])==1){
                $temp[$groupValue][] = current($arr[$key]);
            } else {
                $temp[$groupValue][] = $arr[$key];
            }
            return $temp;
        }
        // cleare cache by specific key
        function clean_cache_by_key($my_folder = 'api', $key)
        {
                $file = APPPATH.'cache/'.$my_folder.'/'.$key;
                if(is_file($file))
                unlink($file); //delete file
        }

        // cleare cache by specific key
        function clean_cache_by_key_from_start($my_folder = 'api', $key)
        {
            $file = APPPATH.'cache/'.$my_folder.'/'.$key.'_*';
            //if(is_file($file))
            array_map('unlink', glob($file));
            // unlink($file); //delete file
        }

        // cleare cache for specific folder and pattern
        function clean_cache($my_folder = 'metadata', $pattern)
            {
                $path = APPPATH.'cache/'.$my_folder.'/';
                $files = glob($path.$pattern.'?*'); //get all file names
                foreach($files as $file){                   
                        if(is_file($file))
                        unlink($file); //delete file                     
                }
            }
                
            // logit function     
            function logit($data, $fileName){
                    $path = APPPATH."logs/".date('Y/m/d');
                    if (!file_exists($path)) {
                        mkdir($path, 0777, true);
                    }
                    
                    $myfile = fopen($path.'/'.$fileName.".txt", "a+") or die("Unable to open file!");
                   // $txt = "========================================================================================\n";
                    $txt = print_r($data, true);
                    $txt .= "========================================================================================\n";
                    fwrite($myfile, $txt);
                    fclose($myfile);
                }


