<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'users/auth'; //loading default module
$route['404_override'] = 'template/Template/error_404';
$route['translate_uri_dashes'] = FALSE;

$route['blog/post/(:any)'] = 'blog/BlogFrontend/single_post';
$route['blog/tags/(:any)'] = 'blog/BlogFrontend/tags';
$route['blog/category/(:any)'] = 'blog/BlogFrontend/categories';

$route['institutions'] = 'containers/schools/manage';
$route['institutions/add'] = 'containers/schools/setup';
$route['institutions/edit/(:any)'] = 'containers/schools/edit/$1';

$route['glossary'] = 'general_modules/glossary';
$route['glossary/add'] = 'general_modules/glossary/add';
$route['glossary/add/(:num)'] = 'general_modules/glossary/add/$1';

$route['mediasources'] = 'media_sources/MediaSources';
$route['mediasources/save'] = 'media_sources/MediaSources/save';
$route['mediasources/save/(:num)'] = 'media_sources/MediaSources/save/$1';

$route['favorites'] = 'favorites/Favorite';
$route['send_messages'] = 'messages/messages/sent_messages';
$route['received_messages'] = 'messages/messages/received_messages';

$route['graders'] = 'users/listing/6';
//$route['grading'] = 'Gradebook/reports';
$route['gradings'] = 'gradebook/gradings';

$route['ltiassessment/(:num)'] = 'general_modules/assessment/index/$1';

$route['manual_notification'] = 'notifications/Manual_Notifications';
$route['manual_notification/add'] = 'notifications/Manual_Notifications/add';
$route['manual_notification/add/(:num)'] = 'notifications/Manual_Notifications/add/$1';
