<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/*Role constants*/
defined('SUPER_ADMIN')          OR define('SUPER_ADMIN', 2);
defined('SYSTEM_ADMIN')         OR define('SYSTEM_ADMIN', 3);
defined('SYSTEM_EDITOR')        OR define('SYSTEM_EDITOR', 4);
defined('GROUP_LEADER')         OR define('GROUP_LEADER', 5);
defined('GRADER')               OR define('GRADER', 6);
defined('PARENTS')               OR define('PARENTS', 7);
defined('INSTRUCTOR')           OR define('INSTRUCTOR', 8);
defined('SITE_MANAGER')         OR define('SITE_MANAGER', 9);
defined('GRADE_ADMINISTRATOR')  OR define('GRADE_ADMINISTRATOR', 10);
defined('STUDENT')              OR define('STUDENT', 11);
/*end role constants*/
defined('DEFAULT_PASSWORD')     OR define('DEFAULT_PASSWORD', 'tel123456@');

/*Category*/
defined('DEFAULT_CAT')              OR define('DEFAULT_CAT', 1);
defined('SYSTEM_CAT')               OR define('SYSTEM_CAT', 2);
defined('DEFAULT_TAG')              OR define('DEFAULT_TAG', 1);
defined('SYSTEM_TAG')               OR define('SYSTEM_TAG', 2);

/*Mata datas*/
defined('META_SYSTEM')               OR define('META_SYSTEM', 1);
defined('META_DEFAULT')              OR define('META_DEFAULT', 2);
defined('META_INSTITUTE')            OR define('META_INSTITUTE', 3);
defined('META_COURSE')               OR define('META_COURSE', 4);
defined('META_LESSON')               OR define('META_LESSON', 5);
defined('META_MODULE')               OR define('META_MODULE', 6);
defined('META_POLL')                 OR define('META_POLL', 7);
defined('META_QUIZ')                 OR define('META_QUIZ', 8);
defined('META_ASSESSMENT')           OR define('META_ASSESSMENT', 9);
defined('META_MEDIA')                OR define('META_MEDIA', 10);
defined('META_QUESTION')             OR define('META_QUESTION', 11);
defined('META_GRADEBOOK')            OR define('META_GRADEBOOK', 12);
defined('META_GLOSSARY')             OR define('META_GLOSSARY', 13);
defined('META_LEARNING_OUTCOMES')    OR define('META_LEARNING_OUTCOMES', 14);
defined('META_LEARNING_STANDARDS')   OR define('META_LEARNING_STANDARDS', 15);
defined('META_COMPETENCIES')         OR define('META_COMPETENCIES', 16);
defined('META_SKILLS')               OR define('META_SKILLS', 17);
defined('BLOOMS_LEVEL')              OR define('BLOOMS_LEVEL', 18);
defined('QUIZ_TYPE_RESTRICTION')     OR define('QUIZ_TYPE_RESTRICTION', 19);
defined('META_NOTIFICATION')         OR define('META_NOTIFICATION', 20);
defined('QUIZ_TYPE')                 OR define('QUIZ_TYPE', 21);
defined('QUIZ_LEVEL')                OR define('QUIZ_LEVEL', 22);
defined('META_USER')                 OR define('META_USER', 23);
defined('META_SYSTEM_GLOSSARY')      OR define('META_SYSTEM_GLOSSARY', 24);
defined('META_MASTERY_POINT')        OR define('META_MASTERY_POINT', 25);

defined('CONTEXT_START')             OR define('CONTEXT_START', 1000000);
defined('RESOURCE_LINK_START')       OR define('RESOURCE_LINK_START', 8000);


/*LTI Credential*/
defined('LTI_LAUNCH_URL')            OR define('LTI_LAUNCH_URL', 'https://go.peerceptiv.com/LTIGateway');
defined('LTI_KEY')                   OR define('LTI_KEY', 'd3248ff7eaf6443bacc0409ee03111e2');
defined('LTI_SECRET')                OR define('LTI_SECRET', '8cf70e2c5a5d4b6795d9c9ae5694167b');

/* file extentions */
defined('ALLOWED_FILES_EXT')                 OR define('ALLOWED_FILES_EXT', 'png|jpg|jpeg|mp3|xlsx|xls|doc|docx|pdf|mp4');
defined('CACHE_LIFE_TIME')                 OR define('CACHE_LIFE_TIME', '30000');
defined('MAIN_DOMAIN_SITE_CREATION')                 OR define('MAIN_DOMAIN_SITE_CREATION', '.devdemo.pro');
//defined('MAIN_DOMAIN')                 OR define('MAIN_DOMAIN', 'devdemo.pro');

defined('SMTP_HOST')                    OR define('SMTP_HOST', 'smtp.sendgrid.net');
defined('SMTP_USER')                    OR define('SMTP_USER', 'apikey');
defined('SMTP_PASSWORD')                OR define('SMTP_PASSWORD', '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');
